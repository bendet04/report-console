<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function (){
    Route::get('/', 'IndexController@index');
    Route::get('/downKtp', 'IndexController@downKtp');
    Route::get('/logout', 'UserController@logout');
    Route::get('/agency/access', 'AgencyController@access');
    Route::post('/agency/access', 'AgencyController@access');
    Route::post('/agency/addAccess', 'AgencyController@addAccess');
    Route::get('/agency/accessUpdate/id/{id}', 'AgencyController@accessUpdate');
    Route::get('/agency/accessDelete/id/{id}', 'AgencyController@accessDelete');
    Route::post('/agency/updateAccessDetail', 'AgencyController@updateAccessDetail');
    Route::post('/agency/accessSoftDelete', 'AgencyController@accessSoftDelete');

    Route::post('/agency/role', 'AgencyController@role');
    Route::get('/agency/role', 'AgencyController@role');
    Route::post('/agency/addRole', 'AgencyController@addRole');
    Route::get('/agency/roleUpdate/id/{id}', 'AgencyController@roleUpdate');
    Route::get('/agency/roleDelete/id/{id}', 'AgencyController@roleDelete');
    Route::post('/agency/updateRoleDetail', 'AgencyController@updateRoleDetail');
    Route::post('/agency/roleSoftDelete', 'AgencyController@roleSoftDelete');
    Route::post('/agency/roleAssinAccessDetail', 'AgencyController@roleAssinAccessDetail');
    Route::get('/agency/roleAssignAccess/id/{id}', 'AgencyController@roleAssignAccess');
    Route::get('/agency/deleteRoleAccess/roleId/{roleid}/accessId/{id}', 'AgencyController@deleteRoleAccess');

    Route::post('/agency/admin', 'AgencyController@admin');
    Route::get('/agency/admin', 'AgencyController@admin');
    Route::post('/agency/addAdmin', 'AgencyController@addAdmin');
    Route::post('/agency/adminNameIsExist', 'AgencyController@adminNameIsExist');
    Route::get('/agency/adminUpdate/id/{id}/page/{$page}', 'AgencyController@adminUpdate');
    Route::get('/agency/adminUpdate/id/{id}', 'AgencyController@adminUpdate');
    Route::post('/agency/updateAdminDetail', 'AgencyController@updateAdminDetail');
    Route::get('/agency/adminDelete/id/{id}', 'AgencyController@adminDelete');
    Route::post('/agency/adminSoftDelete', 'AgencyController@adminSoftDelete');
    Route::get('/agency/adminAssignRole/id/{id}', 'AgencyController@adminAssignRole');
    Route::post('/agency/assignRoleDetail', 'AgencyController@assignRoleDetail');
    Route::get('/agency/cancelRoleDetail/roleId/{roleid}/adminId/{id}', 'AgencyController@cancelRoleDetail');

    Route::get('/personal/modify', 'PersonalController@modify');
    Route::post('/personal/updatePassword', 'PersonalController@updatePassword');

    Route::get('/basic/product', 'BasicController@product');
    Route::get('/basic/productUpdateRate/id/{id}', 'BasicController@productUpdateRate');
    Route::get('/basic/productUpdateDetail/id/{id}', 'BasicController@productUpdateDetail');
    Route::post('/basic/updateRate', 'BasicController@updateRate');
    Route::post('/basic/updateProduct', 'BasicController@updateProduct');
    Route::get('/basic/bank', 'BasicController@bank');
    Route::get('/basic/bankAdd', 'BasicController@bankAdd');
    Route::post('/basic/bank', 'BasicController@bank');
    Route::post('/basic/bankAddDetail', 'BasicController@bankAddDetail');
    Route::post('/basic/updatBankInfo', 'BasicController@updatBankInfo');
    Route::get('/basic/bankUpdate/id/{id}', 'BasicController@bankUpdate');

    Route::get('/switchLocale/{locale}', 'IndexController@switchLocale');
    
    #任务管理 - 催收
    Route::get('/tasktrack/track', 'TaskTrackController@track');
    Route::post('/tasktrack/track', 'TaskTrackController@track');
    Route::post('/tasktrack/taskTrackHandle', 'TaskTrackController@taskTrackHandle');
    Route::get('/tasktrack/assign', 'TaskTrackController@assign');
    Route::get('/tasktrack/reassign', 'TaskTrackController@reassign');
    Route::get('/tasktrack/taskReassign', 'TaskTrackController@taskReassign');
    Route::post('/tasktrack/autoReassigin', 'TaskTrackController@autoReassigin');
    Route::post('/tasktrack/reAssignMultiple', 'TaskTrackController@reAssignMultiple');
    Route::post('/tasktrack/reAssignMultipleHandle', 'TaskTrackController@reAssignMultipleHandle');
    
    #任务管理 - 催收提醒
    Route::get('/remind/remindtask', 'TaskRemaindController@track');
    Route::post('/remind/remindtask', 'TaskRemaindController@track');
    Route::post('/remind/taskTrackHandle', 'TaskRemaindController@taskTrackHandle');
    Route::get('/remind/assign', 'TaskRemaindController@assign');
    Route::get('/remind/reassign', 'TaskRemaindController@reassign');
    Route::get('/remind/taskReassign', 'TaskRemaindController@taskReassign');
    Route::post('/remind/reAssignMultiple', 'TaskRemaindController@reAssignMultiple');
    Route::post('/remind/reAssignMultipleHandle', 'TaskRemaindController@reAssignMultipleHandle');

    #任务管理 - 终审
    Route::get('/taskfinal/finaltrial', 'TaskFinalController@finaltrial');
    Route::post('/taskfinal/finaltrial', 'TaskFinalController@finaltrial');
    Route::get('/taskfinal/assign', 'TaskFinalController@assign');
    Route::get('/taskfinal/reassign', 'TaskFinalController@reassign');
    Route::any('/taskfinal/parts', 'TaskFinalController@parts');
    Route::any('/taskfinal/realparts', 'TaskFinalController@realparts');
    Route::get('/final/verityKtp', 'FinalController@verityKtp');



    #任务管理 - 信审
    Route::get('/taskinfo/infotrial', 'TaskInfoController@infotrial');
    Route::post('/taskinfo/infotrial', 'TaskInfoController@infotrial');
    Route::get('/taskinfo/assign', 'TaskInfoController@assign');
    Route::get('/taskinfo/reassign', 'TaskInfoController@reassign');
    Route::get('/taskinfo/taskInfoReassign', 'TaskInfoController@taskInfoReassign');
    Route::post('/taskinfo/taskInfoHandle', 'TaskInfoController@taskInfoHandle');

    #任务管理 - 电审
    Route::get('/taskphone/phonetrial', 'TaskPhoneController@phonetrial');
    Route::post('/taskphone/phonetrial', 'TaskPhoneController@phonetrial');
    Route::get('/taskphone/assign', 'TaskPhoneController@assign');
    Route::get('/taskphone/reassign', 'TaskPhoneController@reassign');
    Route::any('/taskphone/parts', 'TaskPhoneController@parts');
    Route::any('/taskphone/realparts', 'TaskPhoneController@realparts');



    #订单相关
    Route::get('/loan/loginInfo', 'LoanController@loanInfo');
    Route::post('/loan/loginInfo', 'LoanController@loanInfoTrial');
    Route::post('/loan/selectSingleOrder', 'LoanController@loanInfo');
    Route::get('/loan/add', 'LoanController@add');
    Route::get('/loan/add/orderNumber/{orderNumber}', 'LoanController@add');
    Route::get('/loan/otherOrder', 'LoanController@otherOrder');
    Route::get('/loan/otherKtpOrder', 'LoanController@otherKtpOrder');
    Route::post('/loan/otherOrder', 'LoanController@otherOrderRefuse');
    Route::get('/phone/phoneTrial', 'PhoneController@phoneTrial');
    Route::post('/phone/phoneTrial', 'PhoneController@phoneTrial');
    Route::post('/phone/phoneTrialDetail', 'PhoneController@phoneTrialDetail');
//    Route::get('/phone/workDetail/id/{id}', 'PhoneController@workDetail');
    Route::get('/phone/workDetail/id/{id}/info/{info}', 'PhoneController@workDetail');
    Route::post('/phone/addHolidayList', 'PhoneController@addHolidayList');
    Route::post('/phone/personTriailDetail', 'PhoneController@personTriailDetail');
    Route::get('/phone/call', 'PhoneController@call');

    #还款

    #订单查询
    Route::get('/order/order', 'OrderController@order');
    Route::post('/order/order', 'OrderController@order');
    Route::post('/order/bankFlowSearch', 'OrderController@bankFlowSearch');
    Route::get('/order/orderDetail/id/{id}', 'OrderController@orderDetail');
    Route::get('/order/orderDetail/id/{id}/page/{$page}', 'OrderController@orderDetail');
    Route::get('/phone/workDetail/id/{id}/info/{info}/type/{type}', 'PhoneController@workDetail');
    Route::get('/order/overdueUser', 'OrderController@overdueUser');
    Route::post('/order/overdueUser', 'OrderController@overdueUser');
    
    #风控拒绝原因查询
    Route::get('/order/riskRefuse', 'OrderRiskController@orderRisk');
    Route::post('/order/riskRefuse', 'OrderRiskController@orderRisk');

    #终审
    Route::get('/final/trialFinal', 'FinalController@trialFinal');
    Route::post('/final/trialFinal', 'FinalController@trialFinal');
    Route::get('/final/trialFinalDetail', 'FinalController@trialFinalDetail');
    Route::post('/final/trialjalksealksdfjkasdlf98qeja', 'FinalController@trialjalksealksdfjkasdlf98qeja');
    Route::get('/final/partnerInfo', 'FinalController@partnerInfo');
    Route::get('/final/selectHistory', 'FinalController@selectHistory');
    Route::get('/final/selectHistoryOld', 'FinalController@selectHistoryOld');
    Route::post('/final/riskdata', 'FinalController@curlPost');


    #放款
    Route::get('/payment/paymenttrial', 'PaymentController@paymenttrial');
    Route::post('/payment/paymenttrial', 'PaymentController@paymenttrial');
    Route::post('/payment/payment', 'PaymentController@payment');
    Route::post('/payment/cancel', 'PaymentController@cancelPayment');
    Route::get('/payment/bank', 'PaymentController@getBankInfo');
    Route::post('/payment/bank', 'PaymentController@updateBankInfo');
    Route::get('/payment/checkpaymentflow', 'PaymentController@checkpaymentflow');
    Route::post('/payment/checkpaymentflow', 'PaymentController@checkpaymentflow');
    Route::get('/payment/flow', 'PaymentController@flow');
    


    Route::get('/transation/selectTransation', 'TransationController@selectTransation');

    #还款
    Route::get('/repayment/repaymenthand', 'RepaymentController@repaymenthand');
    Route::post('/repayment/repaymenthand', 'RepaymentController@repaymenthand');
    Route::post('/repayment/closeout', 'RepaymentController@closeout');
    Route::get('/repayment/detail', 'RepaymentController@detail');
    Route::get('/repayment/repaymenthandflow', 'RepaymentController@repaymenthandflow');
    Route::post('/repayment/repaymenthandflow', 'RepaymentController@repaymenthandflow');
    Route::get('/repayment/flow', 'RepaymentController@flow');
    
    #催收
    Route::get('/track/checktrack', 'TrackController@checktrack');
    Route::post('/track/checktrack', 'TrackController@checktrack');
    Route::get('/track/detail', 'TrackController@detail');
    Route::get('/track/trackworktile', 'TrackController@trackWorktile');
    Route::post('/track/save', 'TrackController@save');
    Route::get('/track/updateStatus', 'TrackController@updateStatus');
    Route::get('/track/getVA', 'TrackController@getVA');
    Route::get('/track/detailVA', 'TrackController@detailVA');

    #信审效率
    Route::get('/efficency/voice', 'EfficencyController@voice');
    Route::get('/efficency/messageEfficency', 'EfficencyController@messageEfficency');
    Route::get('/efficency/downMessage', 'EfficencyController@downMessage');
    Route::get('/efficency/downMessageByMonth', 'EfficencyController@downMessageByMonth');
    Route::get('/efficency/downTrackByDay', 'EfficencyController@downTrackByDay');
    Route::get('/efficency/downTrackByMonth', 'EfficencyController@downTrackByMonth');
    Route::get('/efficency/downPhoneByDay', 'EfficencyController@downPhoneByDay');
    Route::get('/efficency/downPhoneByMonth', 'EfficencyController@downPhoneByMonth');
    Route::get('/efficency/countMessage', 'EfficencyController@countMessage');
    Route::post('/efficency/countMessage', 'EfficencyController@countMessage');
    Route::get('/efficency/trackAmount', 'EfficencyController@trackAmount');
    Route::post('/efficency/trackAmount', 'EfficencyController@trackAmount');
    Route::get('/efficency/channel', 'EfficencyController@channel');
    Route::post('/efficency/voice', 'EfficencyController@voice');
    Route::post('/efficency/getVoice', 'EfficencyController@getVoice');
    Route::post('/efficency/messageEfficency', 'EfficencyController@messageEfficency');
    Route::post('/efficency/selectMessageByDay', 'EfficencyController@selectMessageByDay');
    Route::post('/efficency/selectMessageByMonth', 'EfficencyController@selectMessageByMonth');
    Route::post('/efficency/selectPhoneByDay', 'EfficencyController@selectPhoneByDay');
    Route::post('/efficency/selectTrackByDay', 'EfficencyController@selectTrackByDay');
    Route::post('/efficency/selectTrackByMonth', 'EfficencyController@selectTrackByMonth');
    Route::post('/efficency/handTrackData', 'EfficencyController@handTrackData');
    Route::get('/efficency/finalCount', 'EfficencyController@finalCount');
    Route::get('/efficency/selectFinalByDate', 'EfficencyController@selectFinalByDate');
    Route::get('/efficency/selectFinalLocalWeek', 'EfficencyController@selectFinalLocalWeek');
    Route::get('/efficency/selectFinalLocalMonth', 'EfficencyController@selectFinalLocalMonth');
    Route::get('/efficency/downFinalTrialInfo/start/{condition}/end/{end}', 'EfficencyController@downFinalTrialInfo');
    Route::get('/efficency/overDue', 'EfficencyController@overDue');//催收逾期统计
    Route::post('/efficency/overDue', 'EfficencyController@overDue');//催收逾期统计
    Route::get('/efficency/willingness', 'EfficencyController@willingness');//催收逾期统计
    
    
    #渠道管理
    Route::get('/channel/channelList','ChannelController@channelList');
    Route::post('/channel/change','ChannelController@change');
    Route::post('/channel/add','ChannelController@add');
    Route::post('/channel/upname','ChannelController@upname');
    Route::get('/channel/channelCount','ChannelController@channelCount');
    Route::post('/channel/channelCount','ChannelController@channelCount');
    Route::get('/channel/down','ChannelController@down');
    Route::post('/channel/del','ChannelController@del');
    Route::get('/channel/StatisticalRrate','ChannelController@StatisticalRrate');
    Route::post('/channel/StatisticalRrate','ChannelController@StatisticalRrate');
    
    #风控相关管理
    Route::get('/risk/blackList','BlackListController@blackList');
    Route::post('/risk/getSearch','BlackListController@getSearch');
    Route::get('/risk/addBlack','BlackListController@addBlack');
    Route::post('/risk/addBlack','BlackListController@addBlack');
    Route::post('/risk/add','BlackListController@add');
    
    #投资管理
    Route::get('Investment/setting','InvestmentController@setting');
    Route::get('Investment/query','InvestmentController@query');
    Route::post('Investment/query','InvestmentController@query');
    Route::get('Investment/examine','InvestmentController@examine');
    Route::get('Investment/save','InvestmentController@save');
    Route::get('Investment/del','InvestmentController@del');
    Route::post('Investment/update','InvestmentController@update');
    Route::post('Investment/examine','InvestmentController@examine');
    Route::post('Investment/getdata','InvestmentController@getData');
    Route::post('Investment/examine_status','InvestmentController@examine_status');
    Route::post('Investment/confirm','InvestmentController@confirm');
    Route::get('/Investment/flow','InvestmentController@flow');
    Route::get('Investment/payment','InvestmentController@payment');
    Route::post('Investment/payment','InvestmentController@payment');
    Route::post('Investment/paymentAc','InvestmentController@paymentAc');
    Route::get('Investment/apply','InvestmentController@apply');
    Route::post('Investment/apply','InvestmentController@apply');
    Route::post('Investment/applyAc','InvestmentController@applyAc');
    
    
    Route::get('/switchStatus', 'IndexController@switchWorkStatus');
    Route::get('/refresh', 'IndexController@refresh');
});

Route::get('/login', [ 'as' => 'login', 'uses' => 'UserController@showLoginForm']);
Route::post('/login', 'UserController@login');