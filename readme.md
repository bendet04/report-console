## 安装

1、克隆仓库
```shell
git clone git@gitee.com:BJMiniTech/Minirupiah-console.git
```

2、复制`.env.example`重命名为`.env`或者执行下面的命令
```shell
php -r "file_exists('.env') || copy('.env.example', '.env');"
```
3、生成app_key
```shell
php artisan key:generate
```
