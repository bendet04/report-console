<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/8/14/014
 * Time: 18:33
 */
return [
    'client.name'                   => '客户名称',
    'assign.again'                  => '重新分件',
    'product.type'                  => '业务种类',
    'apply.amount'                  => '申请金额',
    'apply.time'                    => '申请时间',
    'detail'                        => '详情',
    'jump'                          => '跳转',
    'realName'                      => '真实姓名',
    'trail.fail'                    => '审核失败',
    'channel.source'                => '渠道来源',
    'married'                       => '已婚',
    'sex'                           => '性别',
    'age'                           => '年龄',
    'female'                        => '女',
    'male'                          => '男',
    'collage'                       => '学历',
    'live.period'                   => '居住时长',
    'unkown'                        => '未知',
    'living.condition'              => '生活状况',
    'start.time'                    => '开始时间',
    'end.time'                      => '结束时间',
    'deadline.time'                 => '到期时间',
    'birth.time'                    => '出生日期',
    'name'                          => '名字',
    'address'                       => '地址',
    'work.address'                  => '工作单位',
    'name.last'                     => '名',
    'name.first'                    => '姓',
    'last.apply.amount'             => '上次申请金额',
    'remain'                        => '还有',
    'already'                       => '已过',
    'payment.detail'                => '流水详情',
    'bank.detail'                   => '银行的详情',
    'request.fail'                  => '请求失败',
    'result'                        => '结果',
    'cretate.time'                  => '创建时间',
    'platform'                      => '平台',
    'info'                          => '信息',
    'search'                        => '搜索',
    'downInfo'                      => '导出',
    'up'                            => '天以上',




    'not.find.flow'                          => '银行未找到此流水交易',
    'risk.control'                           => '拒绝详情',
    'username.not.empty'                     => '管理员账号不能为空',
    'password.not.empty'                     => '密码不能为空',



    'today'                                 => '今天的日期',
    'year'                                  => '年',
    'month'                                 => '月',
    'dd'                                    => '日',
    'this.week'                             => '本周',
    'this.month'                            => '本月',
    'this.day'                              => '本日',
    'date'                                  => '日期',
    'trial.order'                           => '审核的件数',
    'pass.order'                            => '通过的件数',
    'sum.order'                             => '总件数',
    'pass.rate'                             => '通过率',
    'overdue.rate'                          => '逾期率',
    'bad.rate'                              => '坏账率',
    'down.to'                               => '导出',
    'loan.user.sum'                         => '总借款客户数量',
    'old.user.sum'                          => '老客户数量',
    'rate'                                  => '占比',
    'final.tria'                            => '终审人员',
    'track.repayment'                       => '应催款总金额',
    'track.apply'                           => '应催款总本金',
    'track.amount'                          => '催收的金额',
    'track.count'                           => '催收人员件数统计',



    'channel.list'                           => '渠道列表',
    'channel.name'                           => '渠道名称',
    'make.sure.info'                         => '确定执行该操作吗',
    'has.userd'                              => '已启用',
    'not.userd'                              => '未启用',
    'open'                                   => '开启',
    'all.platform'                           => '全部平台',
    'all.channel'                            => '全部渠道',
    'all.name'                               => '平台名称',
    'amount'                                 => '数量',
    'channel'                                => '渠道',
    'add.channel'                            => '添加渠道',
    'use'                                    => '开启',
    'register.sum'                           => '注册数量',
    'submit.sum'                             => '提交申请数量',
    'message.rate'                           => '信审通过率',
    'phone.rate'                             => '电审通过率',
    'final.rate'                             => '终审通过率',


    'flow.select'                             => '流水查询',
    'risk.reason'                             => '风控拒绝规则',
    'short.time'                              => '短期复借拒绝规则',
    'black.reason'                            => '黑产拒绝规则',
    'order.avg'                               => '平均值',
    'pay.time'                                => '缴纳时间',
    'bond.apply.time'                         => '申请提取时间',



];