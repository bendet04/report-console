<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/7/007
 * Time: 17:17
 */


return [
    "loanDeadline" => '还款截止日',
    "beforeLoanDeadline" => '还款截止日前一天',
    "suspendDeadline" => '缓期中',
    "overDueDeadline" => '逾期中',
    "partPayment" => '部分还清',
    "detail" => '详情',
    "applyDetail" => '申请详情',

    'wait.for.confirm'       => '创建待确认', // 1
    'user.cancel'            => '创建待确认', // 2
    'wind.list'              => '风控队列中', // 16
    'wind.success'           => '风控成功，待信审', //17(风控成功)
    'wind.fail'              => '风控失败', //18
    'message.success'        => '信审成功', //19
    'message.failure'        => '信审失败', //20
    'phone.success'          => '电审成功', // 21
    'phone.failure'          => '电审失败', // 22
    'final.fail'             => '终审失败', //5
    'final.success'          => '终审成功', //6
    'user.confirm.fail'      => '用户确认审核失败',//23
    'payment.not.trial'      => '放款审核未通过', //7
    'payment.proceed'        => '放款中', //8
    'loan.overdue'           => '还款己逾期', // 11
    'loan.finish'            => '还款己还清',//12
    'loan.partly'            => '还款未还清', // 13
    'wait.repayment'         => '放款成功待还款',//14(等待重新放款）
    'payment_failure'        => '放款失败',//15
    'stop.payment'           => '终止放款', //24
    'loan.deadline'          => '还款截止日', //25
    
    'status1'                => '创建待确认',//'创建待确认',
    'status2'                => '两分钟用户取消',//'两分钟用户取消',
    'status16'               => '风控队列中',//'风控队列中',
    'status17'               => '待信审',//'待信审',
    'status18'               => '风控失败',//'风控失败',
    'status19'               => '信审成功',   //'信审成功',
    'status20'               => '信审失败',       //'信审失败',
    'status21'               => '电审成功',//'电审成功',
    'status22'               => '电审失败',//'电审失败',
    'status5'                => '终审失败',//'终审失败',
    'status6'                => '终审成功',//'终审成功',
    'status23'               => '用户确认审核失败',//'用户确认审核失败',
    'status7'                => '放款审核未通过',//'放款审核未通过',
    'status8'                => '放款中',//'放款中',
    'status9'                => '还款前一天',//'还款前一天',
    'status10'               => '缓期中',//'缓期中',
    'status11'               => '已逾期',//'已逾期',
    'status12'               => '己还清',//'己还清',
    'status13'               => '部分还清',//'部分还清',
    'status14'               => '还款中',//'还款中',
    'status15'               => '放款失败',//'放款失败',//（等待重新放款）
    'status24'               => '终止放款',//'终止放款',
    'status25'               => '贷款到期',//'贷款到期',
    
    'fraud'                  => '还款意愿造假',//'贷款到期',
    'pribadi'                => '个人信息造假',//'个人信息造假',
    'sulit'                  => '催收难度大',//'催收难度大',
    'family'                 => '家人请求拉黑',//'家人请求拉黑',
    'loan.day'               => '逾期天数大于等于15天',//'逾期天数大于等于15天',
    'loan.day.sms'           => '短信逾期天数大于等于15天',//'逾期天数大于等于15天',
    'peer'                   => '同行,公检、法、军、律等行业',//'同行,公检、法、军、律等行业',
];