<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/8/14/014
 * Time: 18:25
 */
return [
    'taskInfo.manage'                           => '任务管理',
    'taskInfo.message.trial'                    => '信审管理',
    'taskInfo.trial.person'                     => '信审人',
    'taskInfo.assign.to'                        => '接收人',
    'taskInfo.assign'                           => '分配',
    'taskInfo.trial'                            => '审核人',
    'taskInfo.phone.trial'                      => '电审管理',
    'taskInfo.phone.person'                     => '电审人',
    'taskInfo.final.trial'                      => '终审任务管理',
    'taskInfo.track.trial'                      => '催收任务管理',
    'taskInfo.track.person'                     => '催收人',
    'taskInfo.trial.time'                       => '审核时间',

];