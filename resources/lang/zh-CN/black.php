<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/12/27
 * Time: 22:35
 */

return [
    'empty.uuid' => '未授权UUID',
    'uuid.has.bind' => '同UUID绑定多个KTP',
    'ktp.has.bind' => '多个账号绑定一个KTP',
    'sameNameOrder' => '同名订单',
    'sameKtpOrder' => 'KTP相似订单',
    'addSuccess' => '添加成功',
];
