<?php

return [
    //主页左边的栏目
    'agency'           => '组织机构管理',
    'operation'        => '操作员管理',
    'role'             => '角色管理',
    'access'           => '权限管理',
    
    'task'             => '任务管理',
    'infoTask'         => '信审任务分配',
    'phoneTask'        => '电审任务分配',
    'firstTask'        => '信审任务分配',
    'finalTask'        => '终审任务分配',
    'trackTask'        => '催收任务分配',
    'remindTask'       => '催收提醒任务分配',
    
    'loan'             => '贷款管理',
    'loanApply'        => '贷款申请查询',
    'firstTrial'       => '贷款信审',
    'phoneTrial'       => '贷款电审',
    'finalTrial'       => '贷款终审',
    'loanSelect'       => '借据查询',
    
    'payment' => '放款管理',
    'paymentTrial' => '放款审核',
    'paymentFlow' => '放款流水查询',
    'bond' => '保证金提取审核',
    
    
    'repayment'        => '还款管理',
    'repaymentFlow'    => '还款流水查询',
    'repaymentHand'    => '手动还款',
    
    'track'            => '催收管理',
    'trackSelect'      => '催收查询',
    
    'setting'          => '个人设置',
    'password'         => '修改密码',
    
    'basicData'        => '基础数据',
    'Product'          => '产品管理',
    'bank'             => '银行管理',
    'userList'         => '用户管理',
    
    'Order'            => '订单管理',
    'OrderSearch'      => '订单查询',
    'riskRefuse'       => '风控拒绝原因查询',
    'overdueUser'      => '坏账用户查询',
    
    'count'            => '统计数据',
    'voice'            => '外呼数据',
    'message'          => '信审数据',
    'phone'            => '电审数据',
    'trackInfo'        => '催收数据',
    'finalCount'       => '终审数据',
    
    'channel'          => '渠道管理',
    'channelList'      => '渠道列表',
    'channelCount'     => '渠道统计',
    'StatisticalRrate' =>'渠道转化率统计',
    
    'Investment'       => '投资管理',
    'setting'          => '产品参数设置',
    'examine'          => '信息审核',
    'paymentInvest'    => '放款审核',
    'query'            => '投资订单查询',
    
    'Customer-service' => '客服管理',
    'Customer-service-order-management' => '工单管理',
    'applyTrial'       => '提现审核',
    
    'risk'             => '风控管理',
    'blackList'        => '黑名单管理',
];