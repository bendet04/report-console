<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/12/21
 * Time: 12:46
 */
return [
    'no.trial'          => '综合资质不达标',
    'bank.error'        => '银行信息错误',
    'payment.success'   => '交易成功',
    'payment.failure'   => '交易失败',
    'send.payment'      => '发起转账',



    'no.repayment' => '未还款',//'未还款',
    'repayment.success' => '还款成功',//'还款成功',
    'repayment.failure'  => '还款失败',//'还款失败',
    'retrial.success' => '复核成功',//'复核成功',
    'has.overdue' => '已逾期',//'已逾期',



    'trial.pass'=> '审核通过',              //'审核通过',
    'trial.fail'=> '综合资质不达标',                    //'拒绝',


];