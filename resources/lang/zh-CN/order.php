<?php
return [
    'searchBank'           => '银行状态查询',
    'backStatus'           => '银行交易状态',
    'transactionId'        => '交易id',
    'paymentTime'          => '放款时间',
    'tranceStatus'         => '交易状态',
    'repaymentStatus'      => '还款情况',
    'repaymentStatus1'     => '全部正常还款且还款3次以上',
    'repaymentStatus2'     => '还款3次以上但有逾期',
    'repaymentStatus3'     => '全部正常还款且还款1-3次',
    'repaymentStatus4'     => '还款1-3次但有逾期',
    'overdueUser'          => '坏账用户查询',
    'track.time'           => '催收时间',
    'track.info'           => '催收信息',


    'trial.reason'         => '拒绝原因',
    'image.dim'            => '照片模糊拒绝',
    'video.ktp.different'  => '视频&KTP不一致拒绝',
    'img.invalid'          => '工作照&工资条&NPWP无效拒绝',
    'trial.refuse'         => '审核失败',
    'ktp.number.different' => 'KTP不一致拒绝',
    'name.different'       => '姓名不一致拒绝',
    'phone.invalid'        => '号码无效拒绝',
    'source'               => '自填',
    'sourceNpwp'           => 'NPWP',
    'sourceBpjs'           => 'BPJS',
    
    'no.answer'            => '无人接听',
];