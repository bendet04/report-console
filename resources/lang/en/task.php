<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/9/3
 * Time: 10:59
 */
return [
    'taskInfo.manage'                    => ' Pengaturan tugas',
    'taskInfo.message.trial'             => ' Pengaturan review',
    'taskInfo.trial.person'              => ' reviewer',
    'taskInfo.assign.to'                 => ' terbagi ke',
    'taskInfo.assign'                    => ' membagi',
    'taskInfo.trial'                     => ' pengecek',
    'taskInfo.phone.trial'               => ' pengaturan phone call',
    'taskInfo.phone.person'              => ' nama phone call',
    'taskInfo.final.trial'               => ' pengaturan tugas final review',
    'taskInfo.track.trial'               => ' pengaturan tugas desk call',
    'taskInfo.track.person'              => ' Penagih/ desk collect',
    'taskInfo.trial.time'                => ' waktu verifikasi',
];