<?php

return [
    'detail'             => 'Detail', //详情
    'apply_detail'       => 'Pengajuan Detail', //申请详情
    "loanDeadline"       => 'Tanggal pembayaran', //还款截止日
    "beforeLoanDeadline" => '1 hari sebelum tanggal pembayaran', //还款截止日前一天
    "beforeLoanDeadline1" => '2 hari sebelum tanggal pembayaran', //还款截止日前一天
    "suspendDeadline"    => 'masa tengga', //缓期中
    "overDueDeadline"    => 'keterlambatan pembayaran', //逾期中
    "partPayment"        => 'pembayaran sebagian', //部分还清
    "detail"             => 'Detail', //详情
    "applyDetail"        => 'Pengajuan Detail',  //申请详情
    
    'number'             => 'kode', //编号
    'name'               => 'nama', //名字
    'gender'             => 'jenis kelamin', //性别
    'male'               => 'laki-laki', //男
    'female'             => 'perempuan', //女
    'ktpImage'           => 'foto KTP',
    'vedio'              => 'Bukti Pinjaman',
    'workCard'           => 'foto ID Card pekerjaan',
    'applyAmount'        => ' jumlah pinjaman yg diajukan',//申请金额
    'orderRePayAmount'   => 'jumlah yg harus dibayar', //应还金额
    'paymentDate'        => 'tgl pencairan dana',
    'repaymentDate'      => 'tgl jatuh tempo',
    'phone'              => 'ponsel', //手机号
    'work_info'          => 'pekerjaan', //工作信息
    'orderBasicTitle'    => 'basic', //基本信息
    'contact'            => 'kontak person', //联系人
    'record'             => 'riwayat panggilan', //通话记录
    
    'trackStatus1One'    => 'Sehari sebelum pembayaran', //还款前一天
    'trackStatus1Two'    => 'Jatuh tempo hari ini', //今日到期
    'trackStatus1Three'  => 'Overdue 1hari', //逾期第一天
    'trackStatus1Four'   => 'Overdue 2hari', //逾期第二天
    'trackStatus1Five'   => 'Overdue < 7hari', //逾期7天内
    'trackStatus1Six'    => 'Overdue < 14hari', //逾期14天内
    'trackStatus1Seven'  => 'Overdue ≥ 15hari', //逾期15天以上
    
    'trackStatus2One'    => 'Janji bayar hari ini', //承诺今日还款
    'trackStatus2Two'    => 'Janji bayar besok', //承诺明日还款
    'trackStatus2Three'  => 'Janji bayar lusa', //承诺后天还款
    
    'callhistory'        => 'History Call', //通话历史
    'mainContact'        => 'Emergency Call', //关键联系人
    'otherContact'       => 'nomor contact lain', //其他联系人
    'add'                => 'add', //添加
    
    'reapymentDate1'     => 'tanggal payment', //还款日期
    'strong'             => 'kuat', //强
    'weak'               => 'lemah', //弱
    'previous'           => 'previous', //上一条
    'next'               => 'next', //下一条
    'today'              => 'hari ini', //今天
    'threeDays'          => 'dalam 3hari', //最近三天
    'tenDays'            => 'dalam 10hari', //最近十天
    
    'email'              => 'email', //邮箱
    'contactAndRecord'   => 'contact,record call(double)', //通讯录、通话记录重合
    'contactAndMessage'  => 'contact,SMS(double)', //通讯录、短信重合
    'recordAndMessage'   => 'contact,SMS(double)', //通话记录、短信重合
    
    'overdueDay'         => 'Overdue :dayhari', //逾期:day天
    
    'otherPlatform'      => 'Platform lainnya',
    
    'lock'               => 'Kunci',
    'unlock'             => 'mengunci',
    'lockStatus'         => 'statusKunci',
    'locked'             => 'Sudah Terkunc',
    'unlocked'           => 'Terbuka',
    'overdueDays'        => 'Hari terlambat',
    'overdueStatus1'     => 'Hari terlambat1-14',
    'overdueStatus2'     => 'Hari terlambat15-30',
    'overdueStatus3'     => 'Hari terlambat>30',
    'autoAssigin'        => 'otomatis dibantu',
    'plsChooseOverdue'   => 'Silakan pilih keterlambatan',
    'below14ContReassign'=> 'Data yang Jatuh tempo 1-14 hari , Tidak bisa di pembagian ulang',
    'below30ContReassign'=> 'Yakin pembagian ulang data jatuh tempo 15 - 30 hari ? ',
    'gt30ContReassign'   => 'Yakin pembagian ulang data jatuh tempo DIATAS 30 hari ?',
    
    'unableToContact'    => 'tidak dapat menghubungi user,ketika',
    'attitude'          =>'dapat menghubungi user tetapi sikapnya sangat buruk dan DC mengalami kesulitan untuk menagih',
    'family'           =>'user tidak memiliki dana untuk membayar, keluarga yang akan membantu untuk membayar',
    'debtDifficulties' => 'dapat menghubungi pengguna, sikapnya baik, tetapi utangnya banyak dan DC mengalami kesulitan untuk menagih',
    'noMoney'         =>'belum sampai tanggal gajian jadi tidak mempunyai dana untuk pembayaran',
    'noInternet'       =>'Tidak ada jaringan dan atm di sekitar,sehingga belum bisa melakukan pembayaran',
    'little'           =>'Pengguna menyatakan bahwa dia hanya bersedia membayar pinjaman pokok',
    'time'            =>'Pengguna menyatakan bahwa dia hanya bersedia membayar dengan angsuran',
    'tomorrow'         =>'besok',
    'after'            =>'lusa',
    'none'            =>'Tidak ada kesanggupan membayar kembali',
    
    'generate'           => 'Create', //生成
    'chooseBank'         => 'Pilih bank pengembalian dana', //选择还款银行
    'plsChooseBank'      => 'Silahkan pilih bank pengembalian dana!', //请先选择还款银行!
    'generateConfirm'    => 'Yakin ingin membuat virtual account pengembalian dana untuk user?', //确认要帮助用户生成还款虚拟账号?
    'getBankFailure'     => 'Gagal mendapatkan nomor virtual account, silahkan tunggu sebentar dan coba lagi!', //获取虚拟还款账号失败，请稍后重试!
    'amountOfRepayment'  => 'Jumlah pembayaran kembali',//还款金额
    'repaymentIntention' => ' Tidak ada keinginan untuk membayar',
    'notice'             => 'keterangan',
    'checkSize'       =>'Jumlah yang dimasukkan harus lebih besar dari 10,000',
    'subTwo'         =>'Dua yang terakhir hanya bisa 00',
    'overSize'         =>'Jumlah input tidak boleh lebih besar dari jumlah yang jatuh tempo',
    'checkMessage'         =>'Silakan periksa dengan hati-hati apakah informasinya konsisten',
    'trackAll'            =>'Jumlah total koleksi',
    'trackStatus1TwoDay'    => 'Jatuh tempo h-2'




];