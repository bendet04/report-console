<?php
/**
 * 终审语言包
 * @author  chenwr
 * @since   2018年5月30日
 * @version v1.0
 */

return [
    'order' => 'No.urut',
    'number' => 'kode', //编号
    'reason' => 'Alasan',//原因
    'clientName' => 'nama user', //客户名称
    'ktp' => 'nomor KTP',
    'phone' => 'nomor ponsel',
    'payPeriod' => 'jangka waktu pinjaman', //借款期限
    'applyAmount' => ' jumlah pinjaman yg diajukan',//申请金额
    'score' => 'score', //评分
    'applyTime' => 'waktu pengajuan permohonan', //申请时间
    'status' => 'status', //状态
    'operation' => 'operasi', //操作
    'select' => 'pencarian', //查询

    'final.trial'=> ' pengaturan final review',
    'old'=> ' user lama',
    'new' => ' user baru',
    'wait.trial'=> ' verifikasi tertunda',
    'final.success' => ' sukses final review',
    'final.failure'=> ' gagal final review',
    'user.sure' => ' User/peminjam sudah konfirm',
    'other'=> ' lain-lainnya',
    'same' => ' sama',
    'diff' => ' lain-lainnya ',
    'total.score'=> 'total nilai',
    'user.type'=> 'tipe user' ,
    'income.level'=> 'tingkat pendapatan',
    'normal' => 'normal',
    'overdue'=> 'overdue',
    'apply'=> 'mengajukan',


    'systerm.score'=> ' total nilai sistem',
    'service'=> ' Layanan pelanggan ',
    'distance.salary'=> ' jarak waktu dengan penggajian',
    'age'=> ' umur',
    'no'=> 'tidak',
    'overdue.status'=>'逾期情况',
    


    'device.data.trial' => ' Peninjauan data perangkat ',
    'message.trial'=> ' peninjauan sms',
    'message' => ' sms',
    'black.list' => ' data blacklist',
    'white.list'=> ' data whitelist',
    'phone.record.trial'=> ' peninjauan panggilan telpon',
    'total.phone.record' => ' Jumlah total rekapan',
    'same.as.phone.record' => 'penggandaan dengan no telpon',
    'same.as.phone.history' => ' penggandaan dengan panggilan',
    'same.as.phone.relation'=> ' penggandaan dengan no kontak 1 & 2 ',
    'record.trial' => ' Pengecekan buku telpon',
    'total.record.sum'=> ' Jumlah total rekapan',
    'total.download.sum'=> ' jumlah user yang unduh',
    'relative.to.loan' => ' terkait dengan pinjaman uang ',
    'repaymtn.relation' => ' terkait dengan niat pelunasan',

    'ovedue.relation' => ' terkait dengan overdue',
    'message.phone.again' => ' peninjauan ulang Loan review',


    'map.trial' => ' peninjauan peta',
    'map.location' => 'lokasi peta (daerah kota, provinsi',
    'distance' => 'jarak',
    'work.address' => ' alamat kerja',
    'home.address' => ' alamat rumah',



    'image.trial' => ' peninjauan kartu/data',
    'image.work' => ' foto ID card kerja',
    'image.other.trial' => ' foto lain-lainnya',
    'image.salary' => ' slip gaji',
    'image.credit.card' => ' kartu kredit',
    'image.social.card' => ' kartu BPJS',
    'image.driver.card' => 'Kartu SIM ',
    'image.water.card'=> ' slip pembayaran listrik, air, gas',
    'image.rent' => ' kontrak sewa rumah',
    'image.other.repayment' => ' slip pembayaran yang lain ',

    'third.data' => ' peninjauan data pihak ke 3',
    'third.check' => ' peninjauan data pihak ke 3',
    'click.select.detail' => ' klik mencari detail pengajuan',
    'real.month.income'=> ' asli pendapatan ',
    'is.submit.npwp' => ' npwp – apakah mengajukan npwp',
    'is.submit.telkomsel'=> ' telkomsel apakah mengajukan telkomsen',
    'is.same.phone'=> ' no telp sama',
    'is.same.name' => ' nama apakah sama',
    'is.same.email' => ' alamat email apakah sama',
    'ktp.same.as.npwp.name' => 'nama di NPWP dan di KTP sama ',
    'ktp.same.as.apply.phone' => ' no telp sama',
    'npwp.phone.diff'=> 'alamat email npwp beda',
    'fabk.inst'                       => 'Facebook, Instagram nomor hp dan nomor pengajuan sesuai',
    'threeDegree'                     => 'Tingkat otorisasi data third party',
    'gojek.lazada.toko'               => 'Gojek,Lazada,Tokopedia dan telepon pengajuan sama',
    'np.concat.telkomse'               => 'NPWP,Telkomsel,dan telepon pengajuan sama',

    'tell.score'=> ' layanan bisnis online',
    'social' => 'sosial ',
    'repayment.wish.alay' => ' analisa kemauan membayar ',
    'repayment.wish' => ' kemauan membayar ',
    'device.data.info' => ' verifikasi silang data Analisa perangkat',



    'user.model' => ' Analisis model pengguna ',
    'win.user.model' => '命中盈利用户模型 ',
    'win.user.sign' => '命中盈利用户特征',
    'credit.score' => ' nilai kredit',
    'sum.to.score' => ' jumlah nilai',
    'deadline.day' => ' jatuh tempo',

    'is.prioty'=> ' apakah memberi otoritas',
    'get.order.or.score'=> ' mendapatkan no order atau jumlah nilai',
    'given.address' => ' alamat sesuai/cocok',


    'work.address.match'=> ' alamat tempat kerja sesuai',
    'last.login.address'=> ' alamat sesuai dengan alamat pd saat masuk akun sebelumnya',
    'same.contact.sum'=> ' jumlah kontak yang terhubung lebih besar ',


    'other.card.big.three' => ' pemberian data pendukung lebih atau sebanyak 3jenis',
    'other.image' => ' foto yang lain',
    'apply.phone.same.bank' => 'no telpon yang melakukan pinjaman sesuai dengan no telpon yang terdaftar di bank peminjam',
    'key.word' => ' kata kunci',
    'app.relative.to' => 'kata kunci untuk aplikasi pinjaman online kurang dari 50% dari seluruh aplikasi yang di install',
    'app.not.include' => 'tidak termasuk driver, diver, poker,  dan kata sejenisnya di seluruh install aplikasi ',
    'message.keyword.same' => ' kata kunci yang menyilang di antara sms dan sms',
    'has.final' => ' telah lunas',
    'has.receive.loan' => ' telah menerima dana',
    'has.payment' => ' telah suskes menbayar',
    'not.include.keyword' => ' tidak terdapat kata kunci yang sensitif',
    'contact.record.mseg' => ' nomor kontak atau riwayat panggilan dengan kondisi buku telpon ',
    'contact.small.loan.three' => ' kata kunci mengenai pinjaman online terdapat di nomor kontak buku telpon kurang dari 3orang',
    'one.of.is.parent' => ' Contact person 1 2 diantaranya ada orang tua',
    'message.name.big.three' => ' nama kontak sms sama dengan buku telpon terjadi data silang lebih dari 3orang',
    'contact.phone.number.big' => ' nama yang dibuku telpon lebih dari 50x menelepon/berhubungan',
    'contact.phone.number.rate' => 'Kesesuaian kontak dan riwayat panggilan diatas 25%',
    'more.than.five.person' => ' menyilangi no telpon antara riwayat panggilan, buku telpon lebih dari 5orang',



    'message.phone.trial' => ' data LRO',
    'phone.retrial' => ' data verifikasi ke tempat kerja',
    'person.retrial' => ' verifikasi data user pribadi',
    'work.trial.remark' => ' keterangan phone call tempat kerja',
    'no.choice' => ' tidak memilih',


    'now.address' => ' lokasi sekarang',
    'birth.date' => ' tanggal lahir',
    'friend.sum' => ' jumlah teman ',
    'work.company' => ' nama perusahan tempat bekerja',
    'work.career' => ' posisi/jabatan ',
    'friend.info' => ' informasi teman',
    'card.info' => ' informasi bank',
    'message.info' => ' informasi',
    'work.info' => ' informasi',
    'publish.info' => ' kirim informasi',
    'publish.info' => ' kirim informasi',

    'friend.info' => ' informasi teman ',
    'card.info' => ' informasi bank',
    'message.info' => ' informasi',
    'work.info' => ' informasi',
    'publish.info' => ' kirim informasi',
    'payment.info' => ' informasi membayar',
    'education.info' => ' informasi pendidikan',
    'login.info' => ' informasi masuk akun ',
    'username' => ' nama pengguna',
    'common.friend' => ' teman yang sama',
    'card.number' => ' nomor kartu',
    'card.expire' => ' masa berlaku kartu bank/atm',
    'card.code' => ' kode post yang di tagihan',
    'card.country' => ' kartu ATM/bank berasal dari negara',
    'login.type' => ' cara masuk ke akun',
    'login.time' => ' waktu masuk ke akun',
    'view.login.time' => ' tampilan waktu masuk ke akun',
    'login.address' => ' alamat untuk masuk ke akun',
    'login.tool' => ' alat masuk ke akun',
    'college.name' => 'nama universitas',
    'college.address' => 'alamat universitas',
    'college.graduate' => ' waktu kelulusan universitas',
    'high.name' => ' nama sekolah menengah atas',
    'high.address' => ' alamat sekolah menengah atas',
    'high.graduate' => ' waktu kelulusan sekolah menengah atas',
    'get.amount' => ' jumlah uang yang di terima',
    'payment.amount' => 'jumlah uang yang di bayar',
    'tips.name' => ' nama surat',
    'tips.time' => ' waktu pengumuman surat',
    'tips.address' => ' alamat pengiriman surat',
    'score' => ' jumlah nilai',
    'money.other' => ' saldo',
    'begin' => ' mulai',
    'end' => ' ujung/akhir',
    'card.type' => ' mode kendaraan ',
    'car.card' => ' nomor kendaraaan ',
    'driver' => ' supir',
    'fatal' => ' biaya',
    'distance' => ' jarak',



    'diliver.address' => 'alamat untuk kirim',
    'diliver.type' => ' cara kirim',
    'bill.address' => ' alamat tagihan',
    'other.address' => ' alamat yang lain',
    'order.order'=> ' pengajuan',
    'like' => ' simpan',
    'payment.type' => ' cara bayar',
    'distinct' => ' daerah',
    'order.number' => ' no pengajuan ',
    'order.phone' => ' nomor telpon pengajuan',
    'sum.amount' => ' jumlah nominal',
    'order.amount' => 'jumlah pengajuan',
    'deliver.amount' => ' biaya pengiriman',
    'discount.amount' => ' nilai discount ',
    'package.info' => 'informasi paket',
    'order.info' => 'informasi pengajuan',
    'seller' => ' penjual',
    'product.name' => 'nama produk',
    'price' => ' harga',
    'count.number' => ' jumlah',
    'wish.name' => ' nama keinginan',
    'product.info' => ' informasi produk',
    'product.name' => ' nama produk',
    'product.last' => ' last stock ',
    'card.type.new' => ' jenis kartu',
    'end.time' => ' tenggat waktu',
    'expiry' => 'masa berlaku',
    'old user' => ' user lama',



    'real.name' => ' nama asli',
    'nick.name' => 'nama panggil',
    'relative.website' => ' situs web terkait ',
    'personal.discription' => 'penjelasan diri sendiri',
    'same.contact' => ' kontak person yang sama ',
    'fan.number' => 'jumlah pengemar ',
    'fan.name' => ' nama pengemar',
    'contact.name' => ' nama kontak',
    'contact.phone' => ' nomor kontak',




    'inline.email' => ' email yang terkait pajak online ',
    'career.type' => ' jenis-jenis karir ',
    'employee' => ' pengguna',
    'should.tax' => 'kantor pajak ',
    'should.tax.phone' => ' nomor kantor pajak',
    'year.tax' => ' SPT tahunan',
    'year.tax.info' => 'tahun ',
    'couple.tax.info' => ' status pajak pasangan',
    'couple.tax.num' => ' nomor NPWP pasangan',
    'country.only.income' => ' pendapatan bersih di negara ini',
    'month' => 'bulan',
    'country.other.income' => ' pendapatan lain di negara ini',
    'sea.only.income' => ' pendapatan bersih diluar negri',
    'force.religion' => ' persembahan atas keagamaan',
    'dismiss.religion' => ' pendapatan yang telah dikurangi biaya persembahan atas keagamaan',
    'discount.tax' => ' keringanan pajak ',
    'shoule.tax.income' => ' penghasilan kena pajak ',
    'shoule.tax.info' => ' pajak penghasilan (pph 21)',
    'reback.tax' => ' restitusi pajak/ pajak kembali ',
    'reback.tax.sum' => ' jumlah pajak yang harus di bayar ',
    'dismiss.tax' => ' pajak penghasilan yang dapat dipotong',
    'year.end.info' => ' asset akhir tahun',
    'small.loan.info' => '小记贷款额 ',
    'part.sum' => ' total sebagian',
    'detail.info' => ' rincian',
    'account' => 'akun',
    'local.card' => ' nomor kartu tersebut',
    'info.deadline' => ' tanggal jatuh tempo paket',
    'other.credit' => ' sisa nilai loan',



    'positive.info' => ' evaluasi positif',
    'neutral.info' => ' evaluasi normal',
    'is.certification' => ' apakah perlu verifikasi',
    'certification' => 'sertifikasi',
    'no.certification' => 'belum sertifikasi',
    'transation.info' => 'informasi transaksi',
    'shop.address' => 'tempat menerima barang',
    'shop.car' => 'keranjang belanja',



    'collage' => 'Universitas',
    'all.same' => 'Semua sama',
    'systerm.all.same' => ' sistem dapat memberikan angka yang konsisten',
    'final.trial.pass' => 'lolos verifikasi final',
    'final.trial.fail' => 'gagal verifikasi final',
    'car.shop.amount' => ' total nominal keranjang belanja ',
    'car.shop.order.amount' => ' total nominal orderan dalam keranjang belanja ',
    'car.shop.charge.amount' => ' biaya administrasi keranjang belanja ',
    'order.time' => ' tanggal order ',
    'order.status' => 'status order',
    'order.admount' => ' total nominal orderan ',
    'fate' => 'biaya pengiriman',
    'order.sum' => ' jumlah barang',
    'charge' => 'biaya administrasi',
    'recevie.name' => 'Nama penerima',
    'recevie.phone' => ' Nomor telpon penerima',
    'product.sum.price' => ' total nilai barang',
    'product.sigle.price' => 'harga satuan barang',
    'store.name' => 'Nama toko',
    'store.score' => 'Nilai toko',
    'store.address' => 'alamat toko',
    'product.name' => 'nama produk',
    'store.part.num' => ' Jumlah orang yang berpartisipasi di toko ',
    'address.type' => 'tipe alamat',
    'revicer.name' => 'nama penerima',
    'detail.address' => ' alamat lengkap',
    'email.num' => 'kode post',
    'province' => 'propinsi',
    'sum.distinct' => 'daerah',
    'all.distinct' => 'termasuk daerah',
    'extra.fate' => 'biaya tambahan',
    'insurance.fate' => 'biaya asuransi',
    'fate.sum' => 'biaya',
    'price.sum' => 'total nominal',
    'other.platform' => 'platform lain',
    'order.history' => 'pencarian riwayat orderan',
    'third.data' => 'detail data pihak ke tiga',
    'sms_concat'                      => 'SMS nomer verivikasi ke kontak 1 dan kontak 2 harus nyambung',
    'ab_concat'                      => 'record dan hubungin ke kontak 1 , 2 yang terhubung',


    'sourcePlatform'   => 'Sumber platform',
    'loanLine'         => 'Jumlah pinjaman',
    'crossScore'       => 'Cross score',
    'infoTrial'        => 'Info Trial',
    'phoneTrial'       => 'Phone Trial',
    'vidio'            => 'Video',
    'oldClientNotrial' => 'User lama, tidak ada trial',
    'income'           => 'pemasukan',
    'spouse'           => 'Kondisi pasangan',
    'callog_addressbook'=> 'Daftar kontak, riwayat panggilan dan sms kontak sesuai',
    'overdue_sms'     => 'mengandung kata kunci yang berhubungan dengan overdue dan hari overdue',
    'wealth.cash.loans'     => 'Kekayaan dan pinjaman uang tunai',
    'sms.contains.cash.loan'     => 'SMS berisi kata kunci yang berkaitan dengan pinjaman tunai',
    'loan.success'     => 'SMS pencairan berhasil',
    'sms.app.cash.loan'     => 'Platform Pinjaman Tunai via SMS dan APP',
    'income.whether.submit.wages'     => 'apakah pendapatan perlu mengajukan slip gaji',
    'income.loan.amount'     => 'Nilai pendapatan dan jumlah pinjaman',
    'oldUser.applyAmount'     => 'Penilaian User lama mengajukan pinjaman dengan jumlah yang sama',
    'amount.reminder'     => 'Penilaian jumlah pelunasan dalam SMS',
    
    'pay.day' => 'Hari Gajian',
    'jatuh.tempo' => 'Estimasi Hari Jatuh Tempo',
    
    'currentPlatform' => 'Platform saat ini',//当前平台
    'otherPlatform' => 'Platform lainnya',//其他平台
    'overdueStatus' => 'Situasi yang terlambat',//逾期情况
    'repaymentTime' => 'Situasi pembayaran kembali',//还款情况
    'normalRepayment' => 'Pembayaran normal',//正常还款

];
