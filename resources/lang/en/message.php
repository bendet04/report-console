<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/5/22/022
 * Time: 16:47
 */
return [


    'empty.uuid' => 'Belum Otorisasi UUID',
    'uuid.has.bind' => 'UUID yang sama bind banyak KTP',
    'ktp.has.bind' => 'Banyak akun bind 1 KTP',


//    common infomation
    'submit'                                  => 'mengajukan',                     /***************/
    'sure'                                    => '确定',
    'close'                                   => '关闭',
    'freeze'                                  => '冻结',
    'normal'                                  => '正常',
    'make.sure.delete.this.record'            => '确定删除该条记录',
    'update'                                  => '修改',
    'delete'                                  => '删除',
    'tips'                                    => '温馨提示',
    'null'                                    => '无',
    'number'                                  => 'No. urut',
    'create.time'                             => 'waktu pendaftaran',
    'status'                                  => 'status',
    'operation'                               => 'operasi',
    'select'                                  => 'pencarian',
    'cancel'                                  => 'Pembatalan',             //'取消',
    'reback'                                  => 'kembali',
    'choose'                                  => 'Silakan pilih',
    'vedio'                                   => 'vedio',                  /***************/
    'work.image'                              => 'foto ID Card pekerjaan',
    'ktp.number'                              => 'no.KTP',               /***************/
    'order.number'                            => 'no.pengajuan',
    'same'                                    => 'sama',                  /***************/
    'diffence'                                => 'beda',                 /***************/
    'client.name'                             => 'nama',
    'phone'                                   => 'ponsel',
    'loan.period'                             => 'jangka waktu pinjaman',
    'loan.amount'                             => 'jumlah pinjaman yg diajukan',
    'trial'                                   => 'verifikasi',
    'view'                                    => 'Query',
    'yes'                                     => 'yakin',                  /******************/
    'day'                                     => 'hari',                   /******************/
    'startWork'                               => 'mulai kerja',
    'stopWork'                                => 'selesai kerja',
    
    'amount.returned'                         => '已还金额',


//    权限管理
    'agency.access.access.name'               => '权限名称',
    'agency.access.addAccess'                 => '新增部门',
    'agency.access.lastAccess'                => '父级名称',
    'agency.access.rounter'                   => '权限路由',
    'agency.access.add.secondAccess'          => '添加二级权限',
    'agency.access.firstView'                 => '一级页面',
    'agency.access.updateAccess'              => '修改权限',
    'agency.access.deleteAccess'              => '删除权限',
    'agency.access.manage.origanize'          => '组织部门管理',
    'agency.access.access.manage'             => '权限管理',
    'agency.access.assignRole'                => '分配角色',

//    角色管理
    'agency.role.manage'                      => '角色管理',
    'agency.role.name'                        => '角色名称',
    'agency.role.description'                 => '角色描述',
    'agency.role.addRole'                     => '新增角色',
    'agency.access.updateRole'                => '修改角色',
    'agency.access.deleteRole'                => '删除角色',
    'agency.admin.assign.access'              => '分配权限',

//    管理员管理
    'agency.admin.admin.manage'               => '操作员管理',
    'agency.admin.first.password'             => '默认新建的管理员的初始密码为',
    'agency.admin.admin.number'               => '员工编号',
    'agency.admin.login.name'                 => '登录名',
    'agency.admin.real.name'                  => '真实姓名',
    'agency.admin.add.admin'                  => '新增操作员',
    'agency.admin.role'                       => '角色',
    'agency.admin.select.role'                => '请选择角色名字',
    'agency.admin.phone'                      => '手机号',
    'agency.admin.email'                      => '邮箱',
    'agency.admin.address'                    => '地址',
    'agency.admin.ceit'                       => '证件类型',
    'agency.admin.updateAdmin'                => '修改管理员',
    'agency.admin.assign.role'                => '分配角色',

//    订单页面
    'order.manage'                            => 'Mencari Orderan',  //'订单查询',
    'order.basic.info'                        => 'Data Base',       //'基本信息',
    'register.account'                        => 'Register Akun',   //'注册账号',
    'payment.data'                            => 'Tanggal Jatuh tempo',//'到账日',
    'suspend.data'                            => 'hari Suspensi',//'缓期日',
    'overdue.data'                            => 'Overdue',//'逾期日',
    'trial.info'                              => 'Data Audit',//'审核信息',
    'trial.person'                            => 'reviewer',//'审核人',
    'trial.role'                              => 'Karyawan yang audit',//'审核人角色',
    'trial.date'                              => 'tanggal audit',//'审核日期',
    'trial.result'                            => 'Hasil audit',//'审核结果',
    'role.message.trial'                      => 'LR officer',//'信审人员',
    'wait.trial.info'                         => 'Belum di review',//'未审核件',
    'role.phone.trial'                        => 'LR officer',//'电审人员',
    'role.final.trial'                        => 'Finally officer',//'终审人员',
    'payment.detail.info'                     => 'Info dana cair',//'放款信息',
    'payee.name'                              => 'Penerima dana',//'收款人',
    'payee.bank'                              => 'Bank penerima dana',//'放款银行',
    'payee.account'                           => 'No rek Bank user',//'放款账号',
    'payee.phone'                             => 'No telpon user',//'放款手机号',
    'payment.flow'                            => 'Riwayat Pencairan dana',//'放款流水',
    'payment.time'                            => 'Waktu transaksi',//'交易时间',
    'payment.amount'                          => 'Nilai nominal transaksi',//'交易金额',
    'transation.id'                           => 'No ID transaksi',//'交易id',
    'bank.callback.time'                      => 'Waktu penolakan bank',//'银行回调时间',
    'bank.callback.code'                      => 'kode penolakan bank',//'银行回调码',
    'repayment.info'                          => 'Info Lunas ',//'还款信息',
    'repayment.flow'                          => 'Riwayat pembayaran',//'还款流水',
    'repayment.bank.name'                     => 'Bank pembayaran',//'还款银行',
    'repayment.type'                          => 'cara pembayaran',//'还款方式',
    'have.already.overdue'                    => 'user yang overdue',//'已经逾期的',
    'should.payment'                          => 'dana yang harus dilunasi',//'应还金额',


//    信审的页面
    'message.trial'                           =>'LoanReview', //'信审管理',

//    电审页面
    'phone.trial'                             => 'PhoneReview',//'电审管理',
    'loan.manage'                             => 'Manajemen pinjaman',//'贷款管理',
    'phone.trial.work'                        => 'verifikasi info kerja user',//,'工作审核',
    'phone.trial.personal'                    => 'verifikasi info pribadi user',//'个人审核',
    'phone.now'                               => 'sedang diproses',         //'正在进行',
    'phone.next'                              => 'langkah selanjutnya',    //'下一步',
    'phone.company.phone'                     => 'nomor kantor',
    'phone.company.permit'                    => 'ID Card kerja lulus',
    'phone.company.vailiad'                   => 'no.hp aktif, berhasil dihubungi',//'有效电话，已接通',
    'personal.modify.ren.answer'              => 'tak diangkat, pindah ke list antrian',//'无人接听，加入队列中',
    'personal.modify.holiday'                 => 'tak diangkat krn hari libur',//'节假日无人接听',
    'phone.company.no.vailiad'                => 'nomor tidak valid',         //'此号码无效',
    'phone.company.name'                      => 'nama perusahaan',
    'phone.company.ask.same'                  => 'sama',//'问询一致',
    'phone.company.ask.diffence'              => 'beda',     //'问询不一致',
    'phone.company.career'                    => 'alamat kantor user',
    'phone.company.income'                    => 'tingkat penghasil',
    'phone.company.period'                    => 'lama bekerja',
    'phone.company.ask.new.same'              => 'sama',
    'phone.company.ask.new.difference'        => 'beda',
    'phone.company.ask.secret'                => 'rahasia',  //保密
    'phone.company.work.address'              => 'alamat kantor user',
    'phone.company.work.pay.period'           => 'tanggal penggajian',
    'phone.problem'                           => 'ketika phonereview sy ketemu hahwa···',//'通话过程中，我发现···',
    'phone.more'                              => 'opsi sblh kanan bisa pilih lebih dr satu',//'右边选项可多选',
    'phone.bottom'                            => 'opsi bawah bisa pilih lebih dr satu',//'下方选项可多选',
    'phone.ask.environment.noise'             => 'kebisingan suasana user, agak beda dgn pekerjaan user',//'环境嘈杂，与申请职位不符',
    'phone.ask.header.ear'                    => 'berbisik, sprt diajari org samping',//'交头接耳，有串通嫌疑',
    'phone.ask.take.phone.zzz'                => 'bicaranya terbata-bata',//'接电话支支吾吾',
    'phone.ask.personal.self'                 => 'yg angkat tlp bukan user sendiri',//'申请不是本人接的电话',
    'phone.ask.call.second'                   => 'hp diangkat kedua kali',//'打了两次才打通',
    'phone.ask.call.one'                      => 'hp diangkat pertama kali',//'一次就接通',
    'phone.ask.take.right.now'                => 'hp cepat diangkat',//'很快接通',
    'phone.ask.take.slowly'                   => 'hp lama diangkat',//'很慢接通',
    'phone.ask.finish.next'                   => 'selesai, lanjut ke langkah berikut',//'完成，下一步',
    'phone.ask.have.finish'                   => 'semua selesai',//'已完成',
    'phone.ask.personal.phone'                => 'phonereview user pribadi',//'个人电话',
    'phone.ask.personal.marriage'             => 'status pernikahan',
    'phone.ask.personal.pay.type'             => 'tujuan pinjaman',
    'phone.home.address'                      => 'alamat domisili',
    'phone.ask.applyer.nervous'               => 'user agak gugup',//'申请人很紧张',
    'phone.ask.other.teach'                   => 'ada org ajari disamping',//'申请人周围有人在叫他回答',
    'phone.ask.other.take'                    => 'yg angkat tlp bukan user sendiri',//'不是本人接的电话',
    'phone.ask.other.fluent'                  => 'user jawab cepat tanpa berpikir',//'申请人对答如流，不需要思考',
    'make.sure.add.holiday'                   => 'yakin dipindah ke list tak angkat tlp krn hari libur',//'确定加入假期无人接通队列',
    'make.sure.add.no.answer'                 => 'yakin dipindah ke list tak angkat tlp',//'确定加入无人接通队列',
    'make.sure.add.permit'                    => 'konfirm ID Card kerja lulu',
    'make.sure.phone.number.invalid'          => 'yakin dipindah ke list no.hp tak aktif',//'确认加入无效号码中',
    'make.sure.conpany.diffrence'             => 'yakin nama pt user beda dgn pengajuan',//'确认单位名称不一致',
    'make.sure.client.name'                   => 'yakin nama user beda dgn pengajuan',//'确认用户姓名不一致',
    'make.sure.ktp.number'                    => 'yakin KTP user beda dgn pengajuan',//'确认用户ktp不一致',
    'trial.success'                           => 'verifikasi lolos',//'审核通过',
    'trial.failure'                           => 'verifikasi gagal',//'审核失败',
    'make.sure.trial.failure'                 => 'yakin verifikasi gagal',//'确定审核失败',
    'make.sure.trial.success'                 => 'yakin verifikasi berhasil',//'确定审核成功',
    'take.phone'                              => 'yg angkat tlp',                //'接听人',
    'hr'                                      => 'HR/Aministrasi',                        //'HR/行政',
    'finance'                                 => 'Accounting',                            //'财务',
    'companyee'                               => 'Rekan kerja',                    //'同事',
    'other'                                   => 'lain/sendiri',                    //'其他/本人',
    'make.sure.title'                         => 'System prompt',                    //'系统确认',

//    个人设置
    'personal.manage'                         => '个人设置',
    'personal.modify.password'                => '修改密码',
    'personal.modify.old.password'            => '原始密码',
    'personal.modify.new.password'            => '新密码',
    'personal.modify.ren.password'            => '确认密码',


//    产品管理
    'basic.manage'                            => '基础数据',
    'basic.product.mange'                     => '产品管理',
    'basic.product.list'                      => '产品列表',
    'basic.product.type'                      => '产品类型',
    'basic.normal.rate'                       => '正常利率',
    'basic.overdue.rate'                      => '逾期利率',
    'basic.charge.rate'                       => '手续费利率',
    'basic.overdue.title'                     => '逾期表',
    'basic.product.rate'                      => '逾期利率',
    'basic.bank.manage'                       => '银行列表',
    'basic.bank.type'                         => '银行类型',
    'basic.bank.name'                         => '银行名称',
    'basic.bank.code'                         => '银行编号',
    'basic.bank.refund'                       => '还款银行',
    'basic.bank.fund'                         => '放款银行',
    'basic.bank.add'                          => '新增银行',
    
    'list_not_data_notice'                    => 'Hello, Sekarang tidak ada data list, Silakan refresh dan akan update list.',
    'refresh'                                 => 'refresh',
    
    'platform'                                => 'platform',

    'basic.manage'                            => 'data basic',
    'basic.product.mange'                     => 'pengaturan produk',
    'basic.product.list'                                                      => 'list produk',
    'basic.product.type'                                                      => 'Jenis produk',
    'basic.normal.rate'                                                       => 'tingkat bunga normal',
    'basic.overdue.rate'                                                      => 'bunga keterlambatan',
    'basic.charge.rate'                                                       => 'bunga administrasi',
    'basic.overdue.title'                                                     => 'laporan overdue',
    'basic.product.rate'                                                      => 'bunga keterlambatan',
    'basic.bank.manage'                                                       => 'list bank',
    'basic.bank.type'                                                         => 'jenis bank',
    'basic.bank.name'                                                         => 'nama bank',
    'basic.bank.code'                                                         => 'kode bank',
    'basic.bank.refund'                                                       => 'bank pembayaran',
    'basic.bank.fund'                                                         => 'bank pencairan',
    'basic.bank.add'                                                          => 'penambahan bank baru',

//            个人设置
    'personal.manage'                                                         => 'pengaturan pribadi ',
    'personal.modify.password'                                                => 'ganti password',
    'personal.modify.old.password'                                            => 'password semula',
    'personal.modify.new.password'                                            => 'password baru',
    'personal.modify.ren.password'                                            => 'konfirmasi password',


   // 权限管理 pengaturan otoritas
    'agency.access.access.name'                                               => 'nama otoritas',
    'agency.access.addAccess'                                                 => 'penambahan departemen baru',
    'agency.access.lastAccess'                                                => 'nama orang tua',
    'agency.access.rounter'                                                   => '权限路由',
    'agency.access.add.secondAccess'                                          => 'tambah level 2 otoriti',
    'agency.access.firstView'                                                 => 'tampilan level 1',
    'agency.access.updateAccess'                                              => 'perubahan otoriti',
    'agency.access.deleteAccess'                                              => 'penghapusan otoriti',
    'agency.access.manage.origanize'                                          => 'pengaturan struktur departemen ',
    'agency.access.access.manage'                                             => 'pengaturan otoritas',
    'agency.access.assignRole'                                                => 'pembagian peran',

//            角色管理
    'agency.role.manage'                                                      => 'pengarutan peran',
    'agency.role.name'                                                        => 'nama peran',
    'agency.role.description'                                                 => 'penjelasan peran',
    'agency.role.addRole'                                                     => 'penambahan peran baru',
    'agency.access.updateRole'                                                => 'perubahan peran',
    'agency.access.deleteRole'                                                => 'penghapusan peran',
    'agency.admin.assign.access'                                              => 'pembagian otoriti',

//            管理员管理
    'agency.admin.admin.manage'                                               => 'pengaturan admin',
    'agency.admin.first.password'                                             => ' password awal dari administrator yang baru dibuat adalah ',
    'agency.admin.admin.number'                                               => 'NIK',
    'agency.admin.login.name'                                                 => 'nama log in',
    'agency.admin.real.name'                                                  => 'nama asli',
    'agency.admin.add.admin'                                                  => 'penambahan admin baru',
    'agency.admin.role'                                                       => 'peran',
    'agency.admin.select.role'                                                => 'silakan pilih nama peran',
    'agency.admin.phone'                                                      => 'nomor telepon',
    'agency.admin.email'                                                      => 'alamat email',
    'agency.admin.address'                                                    => 'alamat tempat tinggal',
    'agency.admin.ceit'                                                       => 'jenis dokumen',
    'agency.admin.updateAdmin'                                                => 'perubahan admin ',
    'agency.admin.assign.role'                                                => 'pembagian peran',
    'sure'                                                                    => 'OK ',
    'close'                                                                   => 'tutup',
    'freeze'                                                                  => 'beku',
    'normal'                                                                  => 'normal',
    'make.sure.delete.this.record'                                            => 'konfirm menghapus atas record tersebut ',
    'update'                                                                  => 'revisi',
    'delete'                                                                  => 'hapus',
    'tips'                                                                    => 'peringatan ringan',
    'null'                                                                    => 'N/A',


    'sure'                                                      =>  'Pasti',
    'close'                                                     =>  'Tutup',
    'freeze'                                                   =>  'Bekukan',
    'normal'                                                   =>  'Normal',
    'make.sure.delete.this.record'                            =>  'Pasti  ingin  menghapus  catatan  ini',
    'update'                                                   =>  'Perbarui',
    'delete'                                                   =>  'Hapus',
    'tips'                                                      =>  'Peringatan',
    'null'                                                      =>  'Tidak  ada',


//      权限管理
    'agency.access.access.name'                       =>  'Nama  Akses',
    'agency.access.addAccess'                          =>  'Tambah  Department',
    'agency.access.lastAccess'                        =>  'Nama  Ayah',
    'agency.access.rounter'                             =>  'Akses  Router',
    'agency.access.add.secondAccess'                   =>  'Tambah  akses  tingkat  2',
    'agency.access.firstView'                          =>  'Halaman  tingkat  satu',
    'agency.access.updateAccess'                       =>  'Perbarui  akses',
    'agency.access.deleteAccess'                       =>  'Hapus  akses',
    'agency.access.manage.origanize'                   =>  'Organisir  pengaturan  Departemen',
    'agency.access.access.manage'                      =>  'Pengaturan  Akses',
    'agency.access.assignRole'                         =>  'Pembagian  Peran',

//      角色管理
    'agency.role.manage'                                  =>  'Pengaturan  Peran',
    'agency.role.name'                                    =>  'Nama  Peran',
    'agency.role.description'                             =>  'Detail  Peran',
    'agency.role.addRole'                                 =>  'Tambah  Peran',
    'agency.access.updateRole'                            =>  'Perbarui  Peran',
    'agency.access.deleteRole'                            =>  'Hapus  Peran',
    'agency.admin.assign.access'                          =>  'Pembagian  Peran',

//      管理员管理
    'agency.admin.admin.manage'                           =>  'Pengaturan  Operator',
    'agency.admin.first.password'                         =>  'Sandi  awal  Manager  yang  baru  dibuat',
    'agency.admin.admin.number'                           =>  'No  ID  Pekerja',
    'agency.admin.login.name'                          =>  'Nama  Login',
    'agency.admin.real.name'                           =>  'Nama  Asli',
    'agency.admin.add.admin'                           =>  'Tambah  Operator',
    'agency.admin.role'                                   =>  'Peran',
    'agency.admin.select.role'                        =>  'Silakan  pilih  nama  peran',
    'agency.admin.phone'                                 =>  'No  Hp',
    'agency.admin.email'                                 =>  'Email',
    'agency.admin.address'                              =>  'Alamat',
    'agency.admin.ceit'                                   =>  'Jenis  ID',
    'agency.admin.updateAdmin'                        =>  'Perbarui  Manager',
    'agency.admin.assign.role'                        =>  'Pembagian  Peran',


//      个人设置
    'personal.manage'                                      =>  'Pengaturan  Personal',
    'personal.modify.password'                        =>  'Perbarui  Sandi',
    'personal.modify.old.password'                  =>  'Sandi  Awal',
    'personal.mo dify.new.password'            => 'Sandi Baru',
    'personal.modify.ren.password'            => 'Pastikan Sandi',


//    产品管理
    'basic.manage'                            => 'Data Dasar',
    'basic.product.mange'                     => 'Pengaturan Produk',
    'basic.product.list'                      => 'Daftar Produk',
    'basic.product.type'                      => 'Jenis Produk',
    'basic.normal.rate'                       => 'Bunga Normal',
    'basic.overdue.rate'                      => 'Bunga Keterlambatan',
    'basic.charge.rate'                       => 'Bunga Charge',
    'basic.overdue.title'                     => 'Tabel Keterlambatan',
    'basic.product.rate'                      => 'Bunga Terlambat',
    'basic.bank.manage'                       => 'Daftar Bank',
    'basic.bank.type'                         => 'Jenis Bank',
    'basic.bank.name'                         => 'Nama Bank',
    'basic.bank.code'                         => 'ID Bank',
    'basic.bank.refund'                       => 'Bank Pengembalian',
    'basic.bank.fund'                         => 'Bank Peminjaman',
 
    
    
    
    'reset'                                   => 'Reset',
    'person.tell'                             => 'Keterangan personal',
    'overdue.interest'                        => 'Bunga keterlambatan',
    'no.detailed.reasons'                     =>'Tidak ada alasan khusus',
    
    //风控拒绝规则
    'RC_REDC'                                   => 'Data luar lainnya',
    'RC_RRAC'                                   => ' Daerah beresiko',
    'RC_RAC'                                    => 'Usia',
    'RC_RROC'                                   => 'Pekerjaan',
    'RC_RRMSC'                                  => 'Status Pernikahan',
    'RC_RRDC'                                   => 'Pendidikan',
    'RC_RCNC'                                   => ' Jumlah anak',
    'RC_RRLC'                                   => 'Lama tinggal',
    'RC_WLC'                                    => ' Lama bekerja',
    'RC_PDC'                                    => 'Hari pembagian gaji',
    'RC_ILC'                                    => 'Pemasukan ',
    'RC_AC'                                     => 'APP di hp',
    'RC_AC_ANC'                                 => 'Jumlah aplikasi',
    'RC_AC_ANIDONC'                             => 'Instal app yg berhubungan dengan driver',
    'RC_AC_ANIPONC'                             => ' Instal yg berhubungan dengan poker',
    'RC_AC_ANILONC'                             => 'Instal yg berhubungan dengan pinjaman',
    'RC_SC'                                     => 'Jumlah sms',
    'RC_SC_SILONC'                              => 'Jumlah sms pinjaman',
    'RC_SC_SMNC'                                => ' Jumlah sms (20sms)',
    'RC_SC_SNIBC'                               => ' Jumlah blacklist di no hp sms',
    'RC_CC_CNC'                                    => ' Cek jumlah panggilan (20panggilan)',
    'RC_CC_CILONC'                              => 'Jumlah pinjaman di rekaman panggilan',
    'RC_CC_CNBONC'                              => 'Jumlah no blacklist di rekaman panggilan',
    'RC_CL'                                     => 'Cek jumlah panggilan',
    'RC_CLNC'                                   => 'Jumlah panggilan',
    'RC_CL_CLIBONC'                             => 'Jumlah blacklist di rekaman panggilan',
    'RC_RBONC'                                  => ' Blacklist di kontak panggilan',
    'RC_RS_ABCARC'                              => 'Cek rekaman panggilan dan kontak',
    'RC_REFUSE_WHETHER_TO_AUTHORIZE_THIRDPARTY_DATA'  => 'Data Pihak ketiga',
    'RC_REFUSE_CHECK_RISK_AREA'  => 'Area',
    'RC_REFUSE_CHECK_AGE'  => 'Usia',
    'RC_REFUSE_CHECK_RISK_OCCUPATION'  => 'Pekerjaan',
    'RC_REFUSE_CHECK_MARITAL_STATUS'  => 'Status perkawinan',
    'RC_REFUSE_CHECK_DEGREE'  => 'Pendidikan ',
    'RC_REFUSE_CHECK_CHILD_NUM'  => 'Jumlah anak ',
    'RC_REFUSE_CHECK_RESIDENCE_LENGTH'  => 'Lama tinggal',
    'RC_REFUSE_CHECK_WORKING_LIFE'  => 'Lama pekerjaan',
    'RC_REFUSE_CHECK_PAYDAY'  => 'Tanggal pembagian gaji',
    'RC_CHECK_IMCOME_LEVEL'  => 'Pemasukan gaji',
    'RC_REFUSE_CHECK_ADDRESSBOOK_NUM'  => 'Jumlah panggilan',
    'RC_REFUSE_CHECK_SMS_NUM'  => 'Jumlah SMS',
    'RC_REFUSE_WHETHER_OR_NOT_CONTACTS_IN_BLACKLIST'  => 'Kontak termasuk dalam blacklist',
    'DF_BLACK'  => 'Black industry',
    'DF_OTHER_PLATFORM_OVERDUE'  => 'Jatuh tempo di aplikasi lain >3hari',
    'RC_REFUSE_Check_Overdue_SMS'=> 'Jumlah hari jatuh tempo di SMS',
    'RC_REFUSE_Engine_Of_Whether_Cash_Loan_Excessive'=> 'Terlalu banyak pinjaman',
    'RC_REFUSE_PULL_BLACKLIST_WHEN_OVERDUE'           => 'Jatuh tempo ≤ 15 hari',//'逾期天数大于等于15天',
    'RC_REFUSE_PULL_BLACKLIST_WHEN_RISK_OCCUPATION'                   => 'Bidang yg sama, Keamanan publik, hukum, militer, pengacara dan bidang lainnya',//
    'RC_REFUSE_INSTALLED_APPNUMBER'                  =>'Jumlah app',
    'ktp.not.null'                             => 'KTP tidak boleh kosong ',
    'add.reason'                               => 'Alasan masuk',
    'add.black'                               => 'Masuk blacklist',
    'choose.reason'                           => 'Silahkan masukkan alasan masuk',
    'all.choose'                               => 'Pilih semua',
    'add.more'                                => 'Ktp lebih dari 1 pakai tanda “;”sebagai pemisah (max 10 ktp)',
    'ktp_left'                                 => 'Yakin blacklist KTP:',
    'ktp_right'                                => 'Masuk blacklist?',
    'invalid.ktp'                               => 'no.hp tak aktif',
    'black.time'                              =>'Waktu masuk',
    'effective'                                =>'valid',
    'invalid'                                  =>'tidak valid',
    'multipletracks'                           =>'接收人数不能大于可可分配件数',
    'imgvalid'                                 =>'Buram tidak bisa di identifikasi',
    'prefix'                                   =>'',
    'idfuzzy'                                  => 'dokumen tidak jelas, informasi tidak dikenali, yakin tolak pengajuan ini, suruh user mengajukan kembali?',
    'DF_KTP_NUMBER_SIMLAR'                     => 'nama sesuai，ktp sesuai',
    'auth'                                     => 'sertifikasi resmi',
    'Authorized'                               =>'sudah otorisasi',
    'Unauthorized'                             =>'belum otorisasi',
//    'prefix'                                   =>'Foto Buram,'





];