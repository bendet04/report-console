<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/7/007
 * Time: 17:15
 */
return [
        "loanDeadline" => 'cut off tanggal pelunasan',//'还款截止日',
        "beforeLoanDeadline" => 'H-1 cut off tanggal pelunasan',//'还款截止日前一天',
        "suspendDeadline" => 'hari Suspensi',//'缓期中',
        "overDueDeadline" => 'Overdue',//'逾期中',
        "partPayment" => 'Cicilan',//'部分还清',
        "detail" => 'Rincian',//'详情',
        "applyDetail" => 'Rincian Pengajuan ',//'申请详情',

        'wait.for.confirm'       => 'tunggu konfirmasi user',//'创建待确认', // 1
        'user.cancel'            => 'Pengguna dibatalkan',//'用户取消', // 2
        'wind.list'              => 'List risk kontrol',//'风控队列中', // 16
        'wind.success'           => 'tunggu Loan review',//'风控成功，待信审', //17(风控成功)
        'wind.fail'              => 'gagal risk kontrol',//'风控失败', //18
        'message.success'        => 'Review suskes',//'信审成功', //19
        'message.failure'        => 'Review gagal',//'信审失败', //20
        'phone.success'          => 'By phone sukses',//'电审成功', // 21
        'phone.failure'          => 'by phone gagal',//'电审失败', // 22
        'final.fail'             => 'final gagal',//'终审失败', //5
        'final.success'          => 'final sukses',//'终审成功', //6
        'user.confirm.fail'      => 'User konfirm hasil verifikasi Gagal',//'用户确认审核失败',//23
        'payment.not.trial'      => 'gagal proses pencairan',//'放款审核未通过', //7
        'payment.proceed'        => 'Proses dana cair',//'放款中', //8
        'loan.overdue'           => 'dana sudah overdue',//'还款己逾期', // 11
        'loan.finish'            => 'sudah lunas',//'还款己还清',//12
        'loan.partly'            => 'belum lunas',//'还款未还清', // 13
        'wait.repayment'         => 'sukses pencairan dana',//'放款成功待还款',//14(等待重新放款）
        'payment_failure'        => 'gagal pencairan dana',//'放款失败',//15
        'stop.payment'           => 'berhenti pencairan dana',//'终止放款', //24
        'loan.deadline'          => 'batas waktu pelunasan',//'还款截止日', //25
    
        'status1'                => 'tunggu konfirmasi user',//'创建待确认',
        'status2'                => 'Pengguna dibatalkan',//'两分钟用户取消',
        'status16'               => 'List risk kontrol',//'风控队列中',
        'status17'               => 'tunggu Loan review',//'待信审',
        'status18'               => 'agal risk kontrol',//'风控失败',
        'status19'               => 'verifikasi berhasil',   //'信审成功',
        'status20'               => 'verifikasi gagal',       //'信审失败',
        'status21'               => 'By phone sukses',//'电审成功',
        'status22'               => 'by phone gagal',//'电审失败',
        'status5'                => 'final gagal',//'终审失败',
        'status6'                => 'final sukses',//'终审成功',
        'status23'               => 'User konfirm hasil verifikasi Gagal',//'用户确认审核失败',
        'status7'                => 'gagal proses pencairan',//'放款审核未通过',
        'status8'                => 'Proses dana cair',//'放款中',
        'status9'                => 'H-1 cut off tanggal pelunasan',//'还款前一天',
        'status10'               => 'hari Suspensi',//'缓期中',
        'status11'               => 'dana sudah overdue',//'已逾期',
        'status12'               => 'sudah lunas',//'己还清',
        'status13'               => 'belum lunas',//'部分还清',
        'status14'               => 'sukses pencairan dana',//'还款中',
        'status15'               => 'gagal pencairan dana',//'放款失败',//（等待重新放款）
        'status24'               => 'berhenti pencairan dana',//'终止放款',
        'status25'               => 'batas waktu pelunasan',//'贷款到期',
    
        'fraud'                  => 'Pemalsuan kesediaan untuk membayar',//'还款意愿造假',
        'pribadi'                => 'Pemalsuan informasi pribadi',//'个人信息造假',
        'sulit'                  => 'tingkat penagihan susah',//'催收难度大',
        'family'                 => 'permintaan blacklist dari keluarga',//'家人请求拉黑',
        'loan.day'               => 'Jatuh tempo ≤ 15 hari',//'逾期天数大于等于15天',
        'loan.day.sms'           => 'sms Jatuh tempo ≤ 15 hari',//'短信逾期天数大于等于15天',
        'peer'                   => 'Bidang yg sama, Keamanan publik, hukum, militer, pengacara dan bidang lainnya',//'同行,公检、法、军、律等行业',

];