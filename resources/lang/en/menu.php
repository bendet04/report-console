<?php

return [
    //主页左边的栏目 bagian kiri di halaman utama
    'agency'                    => 'pengaturan', //组织机构管理
    'operation'                 => 'operasional', //操作员管理
    'role'                      => 'peran', //角色管理
    'access'                    => 'akses', //权限管理
    
    'task'                      => 'tugas', //任务管理
    'infoTask'                  => 'infoTask', //信审任务分配
    'phoneTask'                 => 'phoneTask', //电审任务分配
    'firstTask'                 => ' pembagian tugas verifikasi pertama',//初审任务分配
    'finalTask'                 => 'pembagian tugas analis', //终审任务分配
    'trackTask'                 => 'pembagian tugas penagihan', //催收任务分配
    'remindTask'                => 'pembagian tugas pengingat', //催收提醒任务分配
    
    'loan'                      => 'Pinjaman', //贷款管理
    'loanApply'                 => 'pencarian permohonan pinjaman', //贷款申请查询
    'firstTrial'                => 'infoTrial', //贷款初审
    'phoneTrial'                => 'phoneTrial',
    'finalTrial'                => 'verifikasi pinjaman',//贷款终审
    'loanSelect'                => 'pencarian bukti pinjaman', //借据查询
    
    'payment'                   => 'pembayaran', //放款管理
    'paymentTrial'              => 'verifikasi pembayaran', //放款审核
    'paymentFlow'               => 'data pembayaran', //放款流水查询
    'bond'                      => 'margin withdrawal audit',  //保证金提取审核
    
    
    'repayment'                 => 'pelunasan', //还款管理
    'repaymentFlow'             => 'data pelunasan', //还款流水查询
    'repaymentHand'             => 'pelunasan manual', //手动还款
    
    'track'                     => 'penagihan', //催收管理
    'trackSelect'               => 'pilihan penagihan', //催收查询
    
    'setting'                   => 'setting', //个人设置
    'password'                  => 'password', //修改密码
    
    
    'basicData'                 => 'basis data',
    'Product'                   => 'pengaturan produk',
    'bank'                      => 'informasi bank',
    'userList'                  => 'data pengguna',
    
    'Order'                     => 'Daftar pemohon',
    'OrderSearch'               => 'pencarian data pemohon',
    'riskRefuse'                => 'riskRefuse',
    'overdueUser'               => 'Periksa User Macet',
    
    'count'                     => 'statistics',
    'voice'                     => 'voice',
    'message'                   => 'message',
    'phone'                     => 'phone',
    'trackInfo'                 => 'trackInfo',
    'finalCount'                => 'finalCount',
    
    'channel'                   => 'channel',
    'channelList'               => 'channelList',
    'channelCount'              =>'channelCount',
    'StatisticalRrate'          =>'StatisticalRrate',
    
    'Investment'                => 'Memberi Pinjaman',
    'setting'                   => 'Pengaturan parameter produk',
    'examine'                   => 'Review Informasi',
    'paymentInvest'             => 'paymentTrial',
    'query'                     => 'Pencarian data pendanaan',
    
    'Customer-service'          => 'Customer Service',
    'Customer-service-order-management' => 'WorkOrder',
    'applyTrial'                => 'Review Penarikan Dana',
    
    'risk'             => 'risk',
    'blackList'        => 'blackList',
];