<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/12/21
 * Time: 12:51
 */

return [
    'no.trial'          => 'Tidak memenuhi kualifikasi',
    'bank.error'        => 'Kesalahan informasi bank',
    'payment.success'   => 'Transaksi berhasil',
    'payment.failure'   => 'Kegagalan transaksi',
    'send.payment'      => 'Mulai transfer',

    'no.repayment' => 'Tidak dibayar',//'未还款',
    'repayment.success' => 'Pembayaran berhasil',//'还款成功',
    'repayment.failure'  => 'Pembayaran gagal',//'还款失败',
    'retrial.success' => 'Ulasan sukses',//'复核成功',
    'has.overdue' => 'Terlambat',//'已逾期',

    'trial.pass'=> 'Audit berlalu',              //'审核通过',
    'trial.fail'=> 'Tidak memenuhi kualifikasi',                    //'拒绝',
];