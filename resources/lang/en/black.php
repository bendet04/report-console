<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/12/27
 * Time: 22:38
 */

return [
    'empty.uuid' => 'Belum Otorisasi UUID',
    'uuid.has.bind' => 'UUID yang sama bind banyak KTP',
    'ktp.has.bind' => 'Banyak akun bind 1 KTP',
    'sameNameOrder' => 'Data nama sama',
    'sameKtpOrder' => 'KTP sesuai',
    'addSuccess' => 'Berhasil ditambahkan',
];
