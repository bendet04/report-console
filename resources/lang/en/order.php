<?php
return [
    'searchBank' => 'pencarian status bank',
    'backStatus' => 'status transaksi bank',
    'transactionId' => 'ID transaksi',
    'paymentTime'    => 'waktu pencairan',
    'tranceStatus'   => 'status transasksi',
    'repaymentStatus'  => 'Kondisi Pelunasan',
    'repaymentStatus1' => 'Semua Pelunasan Normal dan Pelunasan diatas 3kali',
    'repaymentStatus2' => 'Pelunasan diatas 3kali tapi ada keterlambatan',
    'repaymentStatus3' => 'Semua Pelunasan Normal dan Pelunasan 1-3 kali',
    'repaymentStatus4' => 'Pelunasan 1-3 kali tapi ada keterlambatan',
    'overdueUser'    => 'Periksa User Macet',

    'track.time'   => 'Waktu koleksi',
    'track.info'   => 'Informasi koleksi',

    'trial.reason'   => 'Alasan penolakan',
    'image.dim'   => 'Foto buram',
    'video.ktp.different'   => 'Video dan KTP beda',
    'img.invalid'   => 'Foto kerja,slip gaji dan NPWP tidak valid',
    'trial.refuse'   => 'Review gagal',
    'ktp.number.different'   => 'KTP tidak sesuai',
    'name.different'   => 'Nama tidak sesuai',
    'phone.invalid'   => 'Nomor tidak valid',
    
    'no.answer'            => 'tak diangkat',
];