<form id="default" class="form-horizontal" method="post">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('message.order.number')</th>
                <th>@lang('basic.taskInfo.trial')</th>
                <th>@lang('basic.realName')</th>
                <th>@lang('message.operation')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($infoUser as $key=>$avalue)
            @if ($avalue->id!=$admin_id)
            <tr class="">
                <td>{{ $key + 1 }}</td>
                <td>{{ $avalue->admin_number }}</td>
                <td>{{ $avalue->admin_username }}</td>
                <td>{{ $avalue->real_username }}</td>
                <td><a href="/taskinfo/reassign?order_id={{$order_id}}&admin_id={{ $avalue->id }}&url={{$url}}">@lang('task.taskInfo.assign')</a></td>
            </tr>
            @endif
            @endforeach
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>