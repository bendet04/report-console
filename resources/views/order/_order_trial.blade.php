@if($order['trial'])
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            @lang('message.trial.info')
        </header>
        <div class="panel-body">
            <table class="table table-striped order-table">
                <thead>
                <tr>
                    <th>@lang('message.trial.person')</th>
                    <th>@lang('message.trial.role')</th>
                    <th>@lang('message.trial.date')</th>
                    <th>@lang('message.trial.result')</th>
                </tr>
                </thead>
                <tbody>
                @if($order['trial']['messageTrial'])
                <tr>
                    <td>{{$order['trial']['messageTrial']['admin_username']}}</td>
                    <td>@lang('message.role.message.trial')</td>
                    <td>
                        @if($order['trial']['messageTrial']['trial_time'])
                             {{$order['trial']['messageTrial']['trial_time']}}
                        @else
                            @lang('message.wait.trial.info')
                        @endif
                    </td>
                    <td>{{$order['trial']['messageTrial']['status']}}</td>
                </tr>
                @endif
                @if($order['trial']['phoneTrial'])
                    <tr>
                        <td>{{$order['trial']['phoneTrial']['admin_username']}}</td>
                        <td>@lang('message.role.phone.trial')</td>
                        <td>
                            @if($order['trial']['phoneTrial']['trial_time'])
                                {{$order['trial']['phoneTrial']['trial_time']}}
                            @else
                                @lang('message.wait.trial.info')
                            @endif
                        </td>
                        <td>{{$order['trial']['phoneTrial']['status']}}</td>
                    </tr>
                @endif
                @if($order['trial']['finalTrial'])
                    <tr>
                        <td>{{$order['trial']['finalTrial']['admin_username']}}</td>
                        <td>@lang('message.role.final.trial')</td>
                        <td>
                            @if($order['trial']['finalTrial']['trial_time'])
                                {{$order['trial']['finalTrial']['trial_time']}}
                            @else
                                @lang('message.wait.trial.info')
                            @endif
                        </td>
                        <td>{{$order['trial']['finalTrial']['status']}}</td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
    </section>
</div>
@endif