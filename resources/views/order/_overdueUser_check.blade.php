<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:100px;
    }
    .col-md-4 table{
        width:105%;
    }
    .arrow{
        vertical-align:middle;
    }

</style>
<header class="panel-heading search-panel-heading" style="height: 100px;line-height: 40px;">
    <form action="/order/overdueUser" method="post">
        {{ csrf_field() }}
        {{--<label class="col-md-1 col-sm-1 control-label">@lang('basic.apply.time')</label>--}}
        <div class="col-md-4">
            <table class="ldx">
                <tr>
                    <td class="input-group-addon" >@lang('basic.date')</td>
                    <td class="arrow"><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                    <td class="arrow"><span>⇆</span></td>
                    <td class="arrow"><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">@lang('message.order.number')</td>
                    <td>
                        <input type="text" autocomplete="off" class="form-control" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">KTP</td>
                    <td>
                        <input type="text" autocomplete="off" class="form-control" name="ktp_number" @if(!empty($pageCondition['ktp_number'])) value="{{$pageCondition['ktp_number']}}" @endif>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 98px">
            <input type="hidden" name="excel" value="search">
            <button type="submit" class="btn btn-info search-btn">@lang('message.view')</button>
            <button type="submit" class="btn btn-info excel-btn" style="margin-left: 10px;">@lang('basic.downInfo')</button>
            <input type="button" class="btn btn-info" style="margin-left: 10px;" onclick="rese()" value="@lang('message.reset')">
        </div>
    </form>
</header>