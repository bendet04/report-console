<section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('message.order.number')</th>
                    <th>@lang('message.platform')</th>
                    <th class="numeric">@lang('message.client.name')</th>
                    <th class="numeric">KTP</th>
                    <th class="numeric">@lang('message.phone')</th>
                    <th class="numeric">@lang('message.loan.period')</th>
                    <th class="numeric">@lang('message.loan.amount')</th>
                    <th class="numeric">@lang('message.create.time')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                @if($orders)
                @foreach($orders as $k => $v)
                <tr>
                    <td>{{$k + 1}}</td>
                    <td>{{$v->order_number}}</td>
                    <td>{{\App\Consts\Platform::alias($v->platform)}}</td>
                    <td>{{$v->name}}</td>
                    <td>{{$v->ktp_number}}</td>
                    <td>{{$v->phone_number}}</td>
                    <td>{{$v->loan_period}}</td>
                    <td>{{$v->apply_amount}}</td>
                    <td>{{$v->order_time}}</td>
                    <td>{{$v->order_status_str}}</td>
                    <td><a href="/order/orderDetail/id/{{$v->id}}?page={{$page}}">@lang('message.view')</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{$orders->appends($pageCondition)->render()}}
            @endif

        </section>
    </div>
</section>