<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            @lang('message.order.basic.info')
        </header>
        @if($order)
        <div class="panel-body">
            <table class="table table-striped order-table">
                <thead>
                <tr>
                    <th>@lang('message.register.account')：</th>
                    <th>{{$order['user']['phone_number']}}</th>
                    <th>@lang('message.client.name')：</th>
                    <th>{{$order['user']['name']}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="4">
                        <span>@lang('message.create.time')</span>：{{$order->order_time}}&nbsp;&nbsp;
                        @if($order['date'])
                            <span>
                                @lang('message.payment.data')
                            </span>：{{$order['date']['payment']}}
                            <span>
                                @lang('message.suspend.data')
                            </span>：{{$order['date']['suspend']}}
                            <span>
                                @lang('message.overdue.data')
                            </span>：{{$order['date']['overDue']}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>@lang('message.phone.ask.personal.pay.type')：</td>
                    <td>{{$order->pay_type}}</td>
                    <td>@lang('message.loan.period')：</td>
                    <td>{{$order->loan_period}}@lang('message.day')</td>
                </tr>
                <tr>
                    <td colspan="4">
                        @lang('message.loan.amount')：{{$order->apply_amount}}
                    </td>

                </tr>
                <tr>
                    <td colspan="4">@lang('message.status')：<span style="color: red">{{$order->order_status_str}}</span> &nbsp;&nbsp;
                        @if(in_array($order->order_status,['11','13']))
                            @lang('message.have.already.overdue')
                                {{$order['overDue']['dueInfo']['day']}}
                            @lang('message.day')
                        @endif
                    </td>
                </tr>
{{--                @if($order->order_status == 11)--}}
                    <tr>
                        <td colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        @lang('message.should.payment')：
                                        <span style="color:red">{{$order['overDue']['dueInfo']['amount']}}</span>
                                         =
                                        {{$order['overDue']['dueInfo']['info']}}

                                    </td>
                                    <td>@lang('message.amount.returned')
                                    <span>@if(!empty($order->already_repayment_amount)){{$order->already_repayment_amount}}@else 0 @endif</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                {{--@endif--}}
                </tbody>
            </table>

        </div>
        @endif
    </section>
</div>