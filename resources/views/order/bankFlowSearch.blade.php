<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        @if (!empty($flow['transactionId']))
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
            </tr>
            </thead>
            <tbody>
                <tr class="">
                    <td>
                        @lang('order.transactionId')：
                    </td>
                    <td>
                        {{$flow['transactionId']}}
                    </td>
                </tr>
                <tr class="">
                    <td>
                        @lang('order.paymentTime')：
                    </td>
                    <td>
                        {{$flow['time']}}
                    </td>
                </tr>
                <tr class="">
                    <td>
                        @lang('order.tranceStatus')：
                    </td>
                    <td>
                        {{$flow['transferStatus']}}
                    </td>
                </tr>
            </tbody>
        </table>
        @else
            @lang('basic.not.find.flow')：<br /><br />
            <pre>@json($flow)</pre>
        @endif
    </fieldset>
</form>