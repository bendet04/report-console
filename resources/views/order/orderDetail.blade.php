<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @if($errors->any())
                <div class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            @include('order/_order_basic')
            @include('order/_order_trial')
            @include('order/_order_payment')
            @include('order/_order_repayment')

        </div>
        @include('public/bottom')
    </div>
</section>

@include('public/base_script')

<!-- 第三方授权的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('order.backStatus')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 第三方授权的结束 -->

<script>
    $('#paymentBank').click(function(){
        $('#myModal .modal-body').html('');
        var flow = $(this).attr('data-flow');
        var platform = $(this).attr(' date-platform');
        $.ajax({
            url:"/order/bankFlowSearch",
            type:"post",
            data:{
                'flow': flow,
                'type': 1,
                'platform': platform,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:"html",
            success:function(data){
                $('#myModal .modal-body').html(data);
            }
        });
    });
</script>

</body>
</html>


