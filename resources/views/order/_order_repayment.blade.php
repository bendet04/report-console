@if($order['repayment']['repaymentFlow']->count())
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            @lang('message.repayment.info')
        </header>
        <div class="panel-body">
            <header class="panel-heading">
                @lang('message.repayment.flow')
            </header>
            <table class="table diy-table">
                <tbody>
                @foreach($order['repayment']['repaymentFlow'] as $k => $v)
                <tr>
                    <td style="line-height: 120px">{{$k+1}}</td>
                    <td>
                        <table class="table diy-table">
                            <tr>
                                <td>@lang('message.repayment.bank.name')：</td>
                                <td>{{$v->bank_name}}</td>
                                <td>va：</td>
                                <td>{{$v->bank_number}}</td>
                                <td>@lang('message.transation.id')：</td>
                                <td>{{$v->repayment_flow}}</td>
                            </tr>
                            <tr>
                                <td>@lang('message.payment.amount')：</td>
                                <td>{{$v->repayment_amount}}</td>
                                <td>@lang('message.payment.time')：</td>
                                <td>{{$v->created_at}}</td>
                                <td>@lang('message.repayment.type')：</td>
                                <td>{{\App\Consts\RepaymentFlowType::toString($v->type)}}</td>
                            </tr>
                            <tr>
                                <td>@lang('message.payment.time')：</td>
                                <td>{{$v->repayment_time}}</td>
                                <td>@lang('message.bank.callback.code')：</td>
                                <td>{{$v->code}}</td>
                                <td>@lang('message.status')：</td>
                                <td>
                                    {{\App\Consts\RepaymentFlowStatus::toString($v->repayment_status)}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </section>
</div>
@endif