<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:145px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 210px;line-height: 50px;">
    <form id="default" class="form-horizontal" method="post" action="{{url('order/order')}}">
        {{ csrf_field() }}
        <fieldset title="Initial Info">
            <!--   <legend>Personal Information</legend> -->
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">{{__('message.platform')}}</td>
                        <td>
                            <select class="form-control" name="platform">
                                <option value="">@lang('basic.all.platform')</option>
                                @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                    <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            {{--客户名称--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.client.name')</td>
                        <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--订单编号--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.order.number')</td>
                        <td><input type="text" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <br/>
            {{--KTP--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('final.ktp')}}</td>
                        <td><input type="text" placeholder="Ktp number" name="ktp_number" @if(!empty($pageCondition['ktp_number'])) value="{{$pageCondition['ktp_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--电话号码--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.phone')</td>
                        <td><input type="text" placeholder="phone number" name="phone_number" @if(!empty($pageCondition['phone_number'])) value="{{$pageCondition['phone_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--状态--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.status')</td>
                        <td>
                            <select class="form-control" name="order_status">
                                <option value="0">@lang('message.choose')</option>
                                <option value="17" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='17') selected="selected" @endif>
                                    @lang('status.wind.success')
                                </option>
                                <option value="25" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='25') selected="selected" @endif>
                                    @lang('status.loan.deadline')
                                </option>
                                <option value="19" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='19') selected="selected" @endif>
                                    @lang('status.message.success')
                                </option>
                                <option value="20" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='20') selected="selected" @endif>
                                    @lang('status.message.failure')
                                </option>
                                <option value="21" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='21') selected="selected" @endif>
                                    @lang('status.phone.success')
                                </option>
                                <option value="22" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='22') selected="selected" @endif>
                                    @lang('status.phone.failure')
                                </option>
                                <option value="5" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='5') selected="selected" @endif>
                                    @lang('status.final.fail')
                                </option>
                                <option value="6" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='6') selected="selected" @endif>
                                    @lang('status.final.success')
                                </option>
                                <option value="8" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='8') selected="selected" @endif>
                                    @lang('status.payment.proceed')
                                </option>
                                <option value="15" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='15') selected="selected" @endif>
                                    @lang('status.payment_failure')
                                </option>
                                <option value="23" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='23') selected="selected" @endif>
                                    @lang('status.user.confirm.fail')
                                </option>
                                <option value="7" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='7') selected="selected" @endif>
                                    @lang('status.payment.not.trial')
                                </option>
                                <option value="11" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='11') selected="selected" @endif>
                                    @lang('status.loan.overdue')
                                </option>
                                <option value="12" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='12') selected="selected" @endif>
                                    @lang('status.loan.finish')
                                </option>
                                <option value="13" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='13') selected="selected" @endif>
                                    @lang('status.loan.partly')
                                </option>
                                <option value="14" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='14') selected="selected" @endif>
                                    @lang('status.wait.repayment')
                                </option>
                                <option value="24" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='24') selected="selected" @endif>
                                    @lang('status.stop.payment')
                                </option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <br/>
            {{--还款情况--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('order.repaymentStatus')}}</td>
                        <td>
                            <select class="form-control" name="repayment_status">
                                <option value="0">@lang('order.repaymentStatus')</option>
                                <option value="1" @if(!empty($pageCondition['repayment_status'])&&$pageCondition['repayment_status']=='1') selected="selected" @endif>
                                    @lang('order.repaymentStatus1')
                                </option>
                                <option value="2" @if(!empty($pageCondition['repayment_status'])&&$pageCondition['repayment_status']=='2') selected="selected" @endif>
                                    @lang('order.repaymentStatus2')
                                </option>
                                <option value="3" @if(!empty($pageCondition['repayment_status'])&&$pageCondition['repayment_status']=='3') selected="selected" @endif>
                                    @lang('order.repaymentStatus3')
                                </option>
                                <option value="4" @if(!empty($pageCondition['repayment_status'])&&$pageCondition['repayment_status']=='4') selected="selected" @endif>
                                    @lang('order.repaymentStatus4')
                                </option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
        {{--<br/>--}}
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        <div style="clear: both"> </div>
    </form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>