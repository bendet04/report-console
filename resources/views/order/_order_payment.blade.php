<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            @lang('message.payment.detail.info')
        </header>
        <div class="panel-body">
            <table class="table table-striped order-table">
                <thead>
                <tr>
                    <th>@lang('message.payee.name')：</th>
                    <th>{{$order['payment']['payment']['payee_name']}}</th>
                    <th>@lang('message.payee.bank')：</th>
                    <th>{{$order['payment']['payment']['bank_name']}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>@lang('message.payee.account')：</td>
                    <td>{{$order['payment']['payment']['bank_number']}}</td>
                    <td>@lang('message.payee.phone')：</td>
                    <td>{{$order['payment']['payment']['bank_phone']}}</td>
                </tr>
                </tbody>
            </table>

            @if($order['payment']['paymentFlow']->count())
            <header class="panel-heading">
                @lang('message.payment.flow')
            </header>
            <table class="table diy-table">
                <tbody>
                @foreach($order['payment']['paymentFlow'] as $k => $v)
                <tr>
                    <td style="line-height: 80px">{{$k + 1}}</td>
                    <td>
                        <table class="table diy-table">
                            <tr>
                                <td>@lang('message.payment.time')：</td>
                                <td>{{$v->created_at}}</td>
                                <td>@lang('message.payment.amount')：</td>
                                <td width="100">{{$v->payment_amount}}</td>
                                <td>@lang('message.transation.id')：</td>
                                <td>{{$v->payment_flow}} <a data-flow="{{$v->payment_flow}}" href="javascript:;" id="paymentBank" data-toggle="modal" date-platform="{{$v->platform}}" data-target="#myModal">@lang('order.searchBank')</a></td>
                            </tr>
                            <tr>
                                <td>@lang('message.bank.callback.time')：</td>
                                <td>{{$v->payment_time}}</td>
                                <td>@lang('message.bank.callback.code')：</td>
                                <td>{{$v->payment_code}}</td>
                                <td>@lang('message.status')：</td>
                                <td>{{\App\Consts\PaymentFlowStatus::toString($v->payment_status)}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
             @endif

        </div>
    </section>
</div>