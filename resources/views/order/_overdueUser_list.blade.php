<section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('message.order.number')</th>
                    <th class="numeric">@lang('message.client.name')</th>
                    <th class="numeric">KTP</th>
                    <th class="numeric">@lang('message.phone')</th>
                    <th class="numeric">@lang('message.create.time')</th>
                    <th class="numeric">@lang('basic.deadline.time')</th>
                    <th class="numeric">@lang('final.overdueStatus')</th>
                </tr>
                </thead>
                @if($orders)
                @foreach($orders as $k => $v)
                <tr>
                    <td>{{$k + 1}}</td>
                    <td>{{$v->order_number}}</td>
                    <td>{{$v->name}}</td>
                    <td>{{$v->ktp_number}}</td>
                    <td>{{$v->phone_number}}</td>
                    <td>{{$v->order_time}}</td>
                    <td>{{$v->loan_deadline}}</td>
                    <td>{{__('track.overdueDay', ['day' => \App\Service\DateUnit::timeToDay($v->loan_overdue_date)+1 ])}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{$orders->appends($pageCondition)->render()}}
            @endif

        </section>
    </div>
</section>