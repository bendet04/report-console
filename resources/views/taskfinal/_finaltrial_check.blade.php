<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 120px;line-height: 30px;">
    <form id="default" class="form-horizontal" action="/taskfinal/finaltrial" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset title="Initial Info">
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.order.number')</td>
                        <td><input type="text"  name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('basic.client.name')</td>
                        <td><input type="text"  name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('task.taskInfo.trial')</td>
                        <td>
                            <select class="form-control" name="admin_id">
                                <option></option>
                                @foreach($finalUser as $k=>$v)
                                    <option value="{{ $v->id }}"
                                            @if(!empty($pageCondition['admin_id'])&&$pageCondition['admin_id'] == $v->id)
                                            selected="selected"
                                            @endif

                                    >{{ $v->admin_username }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
        <br/>
        {{--<div class="col-md-9 col-sm-9" style="margin-bottom: 15px">--}}
            {{--<button type="submit" class="btn btn-info">@lang('message.view')</button>--}}
            {{--<a href="/taskfinal/parts" style="margin-left: 25px;" class="btn btn-info">@lang('basic.assign.again')</a>--}}
            {{--<input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <a href="/taskfinal/parts" data-toggle="modal" style="margin-left: 25px;" class="btn btn-info finish">@lang('basic.assign.again')</a>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        <div style="clear: both"> </div>
    </form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>