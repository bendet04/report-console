<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('taskfinal/_finaltrial_add')
        <div class="wrapper">
            @include('taskfinal/_finaltrial_check')
            @include('taskfinal/_finaltrial_list')
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 重新分配的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.assign.again')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 重新分配的结束 -->
@include('public/base_script')

<script>
    $(".track-detail").click(function(){
        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        var user = $(this).attr("data-user");
        $.ajax({
            url:'/taskfinal/assign',
            data:{
                order_id: id,
                admin_id: user
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            console.log(json);
            $('#myModal .modal-body').html(json);
        }).error(function(){
            alert("@lang('basic.trail.fail')");
        })
    });
</script>
</body>
</html>