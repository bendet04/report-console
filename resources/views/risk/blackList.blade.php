<!DOCTYPE html>
<html>
<head>
    @include('public/title')
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            <div style="width:500px;margin:0 auto;">
                <div id="default" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset title="Initial Info">
                        <div class="form-group">
                            <label class="col-md-3 col-sm-2 control-label">KTP</label>
                            <div class="col-md-7 col-sm-6">
                                <input @if(!empty($search)) value="{{$search}}" @endif onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}" type="text" id="ktp" name="ktp" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <div class="col-md-9 col-sm-9" style="margin-bottom: 15px;margin-left: 120px">
                           <a href="javascript:void(0)" id="search" class="btn btn-info">@lang('message.select')</a>
                           <a href="/risk/addBlack" class="btn btn-info">@lang('message.add.black')</a>
                           <a href="javascript:void(0)" class="btn btn-info" onclick="rese()">@lang('message.reset')</a>
                    </div>
                    <div style="clear: both"> </div>
                </div>
            </div>
            <section class="panel">
                <div class="panel-body">
                    <section id="flip-scroll">
                        <table class="table table-bordered table-striped table-condensed cf" id="table-message-trial">
                            <thead class="cf">
                            <tr>
                                <th>@lang('message.number')</th>
                                <th class="numeric">KTP</th>
                                <th class="numeric">UUID</th>
                                <th class="numeric">@lang('message.add.reason')</th>
                                <th class="numeric">@lang('message.black.time')</th>
                            </tr>
                            </thead>
                            <tbody class="mosaic">

                            </tbody>
                        </table>

                    </section>
                </div>
            </section>
        </div>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
<script src="{{ asset('/js/lazyload.min.js') }}"></script>
</body>
</html>
<script>
    //过滤内部机制
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //搜索
    $('#search').click(function () {
        var ktp=$('#ktp').val();
        if(ktp==''){
            $.alert({
                // title: '查询结果',
                content:'@lang('message.ktp.not.null')',
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });

        }else{
            $.post('/risk/getSearch', {ktp:ktp}, function (data) {
                if (data.code == 200) {
                var html=mosaic(data.data);
                $('.mosaic').html(html);
            }
            else
            {
                $.alert({
                    title: '@lang('basic.result')',
                    content: data.info,
                    buttons: {
                        okay: {
                            text: 'OK',
                            btnClass: 'btn-info'
                        }
                    }
                });
            }
        }, 'json')}

    })
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
    }
    function mosaic(data) {

        // return data;
        var html='';
        var num=0;
        $.each(data, function(index, value) {
            html+='<div ><tr >';
            html+='<td>'+index*1+1*1+'</td>';
            html+='<td>'+value.ktp_number+'</td>';
            if(value.uuid!=null){
                html+='<td>'+value.uuid+'</td>';
            }else{
                html+='<td>--</td>';
            }
            html+='<td>';
            if(value.reason==1){
                 html+='@lang('status.fraud')';
            }else if(value.resson==2){
                 html+='@lang('status.pribadi')';
            }else if(value.reason==3){
                 html+='@lang('status.sulit')';
            }else if(value.reason==4){
                 html+='@lang('status.family')';
            }else if(value.reason==5){
                 html+='@lang('status.loan.day')';
            }else if(value.reason==6){
                html+='@lang('status.loan.day.sms')';
            }else if(value.reason==7){
                 html+='@lang('status.peer')';
            }
            html+='</td>'

            html+='<td>'+value.create_at+'</td>';
            // html+='</td>';
            html+='</tr>';
            html+='</div>';
        });
        return html;
    }


</script>
