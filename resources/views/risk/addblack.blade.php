<!DOCTYPE html>
<html>
<head>
    @include('public/title')
    <meta name="csrf-token" content="{{ csrf_token() }}">
<style>

</style>
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            <div style="margin:0 auto;">
                <form id="default" action="addBlack" method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset title="Initial Info">
                        <div class="form-group">
                            <label class="col-md-1 col-sm-2 control-label">KTP</label>
                            <div class="col-md-9 col-sm-6">
                                <input @if(!empty($search)) value="{{$search}}" @endif placeholder="@lang('message.add.more')" type="text" id="ktp" name="ktp" class="form-control">
                            </div>
                            <div class="col=md-1 col-sm-2">
                                <button class="btn btn-info" type="submit">@lang('final.select')</button>
                            </div>
                        </div>
                    </fieldset>
                    <div style="clear: both"> </div>
                </form>
            </div>

            <section class="panel">

                <div class="panel-body">
                    <section id="flip-scroll">
                        <table class="table table-bordered table-striped table-condensed cf" id="table-message-trial">
                            <thead class="cf">
                            <tr>
                                <th><input type="checkbox" id='all'>@lang('message.all.choose')</th>
                                <th>@lang('message.number')</th>
                                <th class="numeric">KTP</th>
                                <th class="numeric">UUID</th>
                                {{--<th class="numeric">加入原因</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($data))
                            @foreach($data as $k=>$v)
                                <div >
                                    <tr>
                                        <td><input   type="checkbox" class="election" name="check" value="{{$v->name}}"></td>
                                        <td>{{$k+1}}</td>
                                        <td class="{{$v->platform}}{{$v->uuid}}">{{$v->ktp_number}}</td>
                                        <td>{{$v->uuid}}</td>
                                        <td style="display: none">{{$v->name}}</td>

                                    </tr>

                                </div>
                            @endforeach
                                @endif
                            </tbody>
                        </table>
                        <fieldset title="Initial Info">
                           <table>
                               <tr class="my">
                               <td>
                               @lang('message.add.reason'):
                               </td>
                               <td >
                               <select style="margin-left: 12px" id="reason" name="reason" class="form-control">
                                   <option value="0" selected="selected">@lang('message.choose')</option>
                                   <option value="1">@lang('status.fraud')</option>
                                   <option value="2">@lang('status.pribadi')</option>
                                   <option value="3">@lang('status.sulit')</option>
                                   <option value="4">@lang('status.family')</option>
                               </select>
                               </td>
                               </tr>
                           </table>
                        </fieldset>
                    </section>
                    <br/>
                    <div class="col-md-9 col-sm-9 col-md-push-2" style="margin-bottom: 15px;margin-left: 200px">
                    <a href="javascript:void(0)" id='add' class="btn btn-info">@lang('track.add')</a>
                    <a href="javascript:void(0)" class="btn btn-info" onclick="rese()">@lang('message.reset')</a>
                    </div>
                </div>
            </section>

        </div>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
<script src="{{ asset('/js/lazyload.min.js') }}"></script>
</body>
</html>
<script>
    //过滤内部机制
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#ktp").bind("input propertychange",function(event){
            var ktp = $("#ktp").val();
            var ktp1 = ktp.replace(/;/g, '');
            if (ktp1.length % 16 == 0) {
                $('#ktp').val(ktp + ';');
            }

    });

     //单选
    $('.election').click(function () {
        var allsize=$("input[name='check'][class='election']").length;
        var size=$("input[name='check'][class='election']:checked").length;
        if(size==allsize){
            $("input[type='checkbox'][id='all']").attr('checked',true);
        }else{
            $("input[type='checkbox'][id='all']").attr('checked',false);

        }
    })

    //全选
        $('#all').on('click',function(){
            if(this.checked) {
                $("input[name='check']").attr('checked',true);
            }else {
                $("input[name='check']").attr('checked',false);
            }
        });
        //添加黑名单
        $('#add').click(function () {
            //验证是否选择加入黑名单原因
            var options=$('#reason option:selected').val(); //获取选中的项
            if(options!=0){
            var post_array= new Array();
            var uri='/gather/add/blacklist';
            var str='';
            $("input[name='check']:checked").each(function(i,v){
                post_array[i]=[];
                post_array[i][i]=$(this).parent().parent().find("td:eq(2)").text();//ktp
                post_array[i][i+1]=$(this).parent().parent().find("td:eq(3)").text();//uuid
                str+=$(this).parent().parent().find("td:eq(2)").text()+',';
                post_array[i][i+2]=$(this).parent().parent().find("td:eq(4)").text();//username
                post_array[i][i+3]=$('#reason').val();//拒绝原因
            });
            if(post_array==''){
               return false
            }else {
                str = str.substring(-1);
                var message = confirm('@lang('message.ktp_left'):' + str + '@lang('message.ktp_right')');
                if (message == true) {
                    $.post('/risk/add', {post_array: post_array,uri:uri}, function (data) {
                        console.log(data.code);
                        if (data.code>0) {
                            $.alert({
                                content: data.info,
                                buttons: {
                                    okay: {
                                        text: 'OK',
                                        btnClass: 'btn-info'
                                    }
                                }
                            });
                        }
                    }, 'json');
                }
            }}else{
                $.alert({
                    title: '',
                    content: '@lang('message.choose.reason')',
                    buttons: {
                        okay: {
                            text: 'OK',
                            btnClass: 'btn-info'
                        }
                    }
                });
            }


    })

    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
    }


</script>
