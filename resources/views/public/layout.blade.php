<!DOCTYPE html>
<html>
<head>
    @include('public/title')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')

        <div class="wrapper">
            @yield('content')
        </div>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
@yield('js')
</body>
</html>