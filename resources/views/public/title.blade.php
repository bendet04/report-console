<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
{{--<link rel="shortcut icon" href="#" type="image/png">--}}
<title>ProsperiTree</title>
<!--pickers css-->
<link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-datepicker/css/datepicker-custom.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-timepicker/css/timepicker.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-colorpicker/css/colorpicker.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-datetimepicker/css/datetimepicker-custom.css') }}" />
<link rel="stylesheet" href="{{ asset('/css/css1/app.css') }}">
<script src="{{ asset('/js/jquery-1.10.2.min.js') }}"></script>
<!--dynamic table-->
<link href="{{ asset('/js/advanced-datatable/css/demo_page.css') }}" rel="stylesheet" />
<link href="{{ asset('/js/advanced-datatable/css/demo_table.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('/js/data-tables/DT_bootstrap.css') }}" />
<!--icheck-->
<link href="{{ asset('/js/iCheck/skins/minimal/minimal.css') }}" rel="stylesheet">
<link href="{{ asset('/js/iCheck/skins/square/square.css') }}" rel="stylesheet">
<link href="{{ asset('/js/iCheck/skins/square/red.css') }}" rel="stylesheet">
<link href="{{ asset('/js/iCheck/skins/square/blue.css') }}" rel="stylesheet">
<!--dashboard calendar-->
<link href="{{ asset('/css/clndr.css') }}" rel="stylesheet">
<link href="{{ asset('/css/jquery.magnify.css') }}" rel="stylesheet">
<!--Morris Chart CSS -->
<link rel="stylesheet" href="{{ asset('/js/morris-chart/morris.css') }}">
<!--common-->
<link href="{{ asset('/css/style.css?2018092905 ') }}" rel="stylesheet">
<link href="{{ asset('/css/mini.css ') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('/css/css1/app.css') }}">
<link href="{{ asset('/css/style-responsive.css') }}" rel="stylesheet">
<link href="{{ asset('/css/jquery-confirm.css') }}" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
