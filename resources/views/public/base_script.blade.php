<script src="{{ asset('/js/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('/js/jquery-ui-1.9.2.custom.min.js') }}"></script>
<script src="{{ asset('/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/modernizr.min.js') }}"></script>
<script src="{{ asset('/js/jquery.nicescroll.js') }}"></script>

<!--easy pie chart-->
<script src="{{ asset('/js/easypiechart/jquery.easypiechart.js') }}"></script>
<script src="{{ asset('/js/easypiechart/easypiechart-init.js') }}"></script>

<!--Sparkline Chart-->
<script src="{{ asset('/js/sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ asset('/js/sparkline/sparkline-init.js') }}"></script>

<!--icheck -->
<script src="{{ asset('/js/iCheck/jquery.icheck.js') }}"></script>
<script src="{{ asset('/js/icheck-init.js') }}"></script>


<script type="text/javascript" src="{{asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>

<script src="{{asset('/js/pickers-init.js')}}"></script>

<script src="{{ asset('/js/transition.js') }}"></script>
<script src="{{ asset('/js/jquery.magnify.js') }}"></script>

{{--<script src="js/transition.js"></script>--}}
<!--common scripts for all pages-->
<script src="{{ asset('/js/scripts.js') }}"></script>
<!--table表格排序-->
<!--<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">-->
<!--<script type="text/javascript" charset="utf8" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>-->
<!--<script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>-->
{{--<script src="{{ asset('/js/multilanguage.js') }}"></script>--}}
<script src="{{ asset('/js/jquery-confirm.js') }}"></script>
<script src="{{ asset('/js/orderBy.js') }}"></script>