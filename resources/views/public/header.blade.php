<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/images/logo.png" alt="" width="144" height="40">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php
                $menu = \App\Service\UserService::getUserMenu();
                foreach ($menu as $k=>$v) {
                if ($v->access_leval==0) {
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('menu.'.$v->access_name);?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php
                        foreach ($menu as $key=>$val) {
                        if ($val->access_leval==$v->id){
                        ?>
                        <li><a href="<?php echo $val->access_url;?>"><?php echo __('menu.'.$val->access_name);?></a></li>
                        <?php
                        }}
                        ?>
                    </ul>
                </li>
                <?php }} ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ asset('/images/photos/user-avatar.png') }}" alt=""> {{ Auth::user()->admin_username }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/switchLocale', ['locale'=>'zh-CN']) }}" lang="cn">中文</a></li>
                        <li><a href="{{ url('/switchLocale', ['locale'=>'en']) }}" lang="en">bhs Indonesia</a></li>
                        <li><a href="/personal/modify"><!--<i class="fa fa-user"></i>-->@lang('menu.password')</a></li>
                        <li><a href="{{ url('logout') }}"><!--<i class="fa fa-sign-out"></i>-->Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>