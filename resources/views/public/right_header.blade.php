<!-- header section start-->
<div class="header-section">
    <!--toggle button start--> <a class="toggle-btn"><i class="fa fa-bars"></i></a>

    <!--toggle button end-->
    <!--search start-->
    <form class="searchform" action="index.html" method="post">
        <input type="text" class="form-control hidden-xs" name="keyword" placeholder="Search here..." />
    </form>
    <!--search end-->
    <!--notification menu start -->
    <div class="menu-right">
        <ul class="notification-menu">
            <li style="padding: 0px;"><a href="{{ url('/switchLocale', ['locale'=>'zh-CN']) }}" lang="cn" class="btn mini-lang">中文</a></li>
            <li style="padding: 0px;"><a href="{{ url('/switchLocale', ['locale'=>'en']) }}" lang="en" class="btn mini-lang">bhs Indonesia</a></li>
            <li><a type="button" class="btn" href="{{ url('logout') }}">Login Out</a></li>
            <li>
                <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('/images/photos/user-avatar.png') }}" alt="" />{{ Auth::user()->admin_username }} <span class="caret"></span>

                </a>
                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <li><a href="/personal/modify"><i class="fa fa-user"></i>  @lang('menu.password')</a></li>
                    {{--<li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>--}}
                    <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Log Out</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--notification menu end -->
</div>
<!-- header section end-->