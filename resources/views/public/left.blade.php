<!-- left side start-->
<div class="left-side sticky-left-side">
    <!--logo and iconic logo start-->
    <div class="logo" style="background: #52a952">
        <a href="/">
            <img src="{{ asset('/images/logo.png') }}" alt="" width="144" height="40">
        </a>
    </div>
    <div class="logo-icon text-center">
        <a href="/">
            <img src="{{ asset('/images/login-logo.png') }}" width="40" height="40" alt="">
        </a>
    </div>
    <!--logo and iconic logo end-->
    <div class="left-side-inner">
        <!-- visible to small devices only -->
        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
            <!-- page heading start-->
            <?php
            $menu = \App\Service\UserService::getUserMenu();
            foreach ($menu as $k=>$v) {
            if ($v->access_leval==0) {
            ?>
            <li class="menu-list <?php echo Config::get('currentMenu')==$v['access_name'] ? 'nav-active' : '';?>"><a href="#"><i class="fa fa-home"></i> <span><?php echo __('menu.'.$v->access_name);?></span></a>
                <ul class="sub-menu-list">
                    <?php
                    foreach ($menu as $key=>$val) {
                    if ($val->access_leval==$v->id){
                    ?>
                    <li><a href="<?php echo $val->access_url;?>"><?php echo __('menu.'.$val->access_name);?></a></li>
                    <?php
                    }}
                    ?>
                </ul>
            </li>
            <?php }} ?>
            <!--sidebar nav end-->
        </ul>
    </div>
</div>
<!-- left side end-->