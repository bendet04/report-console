<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">

    <title>Login</title>

    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style-responsive.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('/js/html5shiv.js') }}"></script>
    <script src="{{ asset('/js/respond.min.js') }}"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" action="{{ url('/login') }}" method="post">
        {{ csrf_field() }}
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Sign In</h1>
            <img src="{{ asset('/images/login-logo.png') }}" width="80" alt="Welcome to MiniRupiah"/>
        </div>
        <div class="login-wrap">
            <input type="text" class="form-control" id="username" placeholder="Admin ID" name="username" required="" autofocus value="{{ old('username') }}">
            <span id="checkuse" style="color:red"></span>
            <input type="password" class="form-control" name="pwd" id="pwd" required="" placeholder="Password">
            <span id="checkpwd" style="color:red"></span>

            <button class="btn btn-lg btn-login btn-block" type="submit">
                <i class="fa fa-check"></i>
            </button>
            @if($errors->any())
                <div class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </div>
            @endif

            <!--   <div class="registration">
                  Not a member yet?
                  <a class="" href="registration.html">
                      Signup
                  </a>
              </div> -->
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>

        </div>
    </form>

    <!-- Modal -->
    <form method="post">
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>
                        <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-primary" type="button">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

    </form>

</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<!-- <script src="/static/js/jquery-1.10.2.min.js"></script> -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/modernizr.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('/js/jquery-1.11.3.min.js') }}"></script>

<script type="text/javascript">
    $('#username').on('blur',function(){
        if($(this).val() == ''){
            $('#checkuse').html("@lang('basic.username.not.empty')");
            return false;
        }
//        else {
//            var username = $(this).val();
//            $.post("/index/auth/checkusername.html", {username:username}, function(data){
//
//                var data = JSON.parse(data);
//                if(data['code']){
//                    $('#checkuse').html('');
//                } else {
//                    $('#checkuse').html('请输入正确的管理员账号');
//                    return false;
//                }
//            }, 'json');
//        };
    })

    $('#pwd').on('blur', function(){
        if($(this).val() == ''){
            $('#checkpwd').html("@lang('basic.password.not.empty')");
            return false;
        }
//        else {
//            var username = $('#username').val();
//            var password = $(this).val();
//            $.post("/index/auth/checkpwd.html", {username:username, password:password}, function(data){
//                console.log(data);
//                var data = JSON.parse(data);
//                console.log(data['code'])
//                if(data['code']){
//                    $('#checkpwd').html('');
//                } else {
//                    $('#checkpwd').html('密码不正确');
//                    return false;
//                }
//
//            }, 'JSON');
//        };
    });
</script>
