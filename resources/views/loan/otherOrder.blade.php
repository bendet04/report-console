<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            <section class="panel">
                <header class="panel-heading">
                    &nbsp;
                    <span class="tools pull-right">
                        <a href="{{$backUrl}}" class="btn btn-default btn-xs" style=" height: 30px; line-height: 10px;">@lang('message.reback')</a>
                    </span>
                </header>
                <div class="panel-body">
                    <section id="flip-scroll">
                        <table class="table table-bordered table-striped table-condensed cf" id="table-message-trial" style="width: 80%">
                            <thead class="cf">
                            <tr>
                                <th>@lang('message.number')</th>
                                <th>@lang('message.vedio')</th>
                                <th class="numeric">KTP</th>
                                <th class="numeric">@lang('message.work.image')</th>
                                <th class="numeric">@lang('final.image.salary')</th>
                                <th class="numeric">NPWP</th>
                                <th class="numeric">@lang('final.order.status')</th>
                                <th class="numeric">@lang('message.operation')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $k=>$v)
                                <div>
                                    <tr >
                                        <td>{{ $k+1 }}</td>
                                        <td>
                                            {{--@lang('message.order.number'):--}}
                                            {{$v->order_number}}
                                            <br />
                                            <video width="160px" height="120px" controls="controls" preload="none" class="custom-video" onclick="this.play();">
                                                <source src="{{ $v->video_url }}" type="video/mp4">
                                            </video>
                                        </td>
                                        <td class="numeric">
                                            {{--@lang('message.ktp.number'):--}}
                                            {{$v->ktp_number}}
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->ktp_image_url }}" data-caption="KTP:{{$v->ktp_number}} {{__('message.client.name')}}:{{$v->name}}" style="width:160px; height: 120px;">

                                        </td>
                                        <td class="numeric">
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->work_image_url }}" data-caption="@lang('message.work.image')  {{__('message.client.name')}}:{{$v->name}}" style="width:160px; height: 120px;">
                                        </td>
                                        <td class="numeric">
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->salary_image_url }}" data-caption="@lang('final.image.salary')  {{__('message.client.name')}}:{{$v->name}} {{__('final.income.level')}}:{{$v->income_level}}" style="width:160px; height: 120px;">
                                        </td>
                                        <td class="numeric">
                                            @if ($v->tax_card_number)
                                                @if ($v->npwp)
                                                    {{ $v->npwp }}
                                                @else
                                                    {{ $v->tax_card_number }}
                                                @endif
                                                @if($v->tax_card_image_url)
                                                <br />
                                                <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->tax_card_image_url }}" data-caption="NPWP:@if ($v->npwp){{ $v->npwp }}@else{{ $v->tax_card_number }}@endif" style="width:160px; height: 120px;">
                                                @endif
                                            @endif
                                        </td>
                                        <td class="numeric" style="color: red; font-weight: bold;">
                                            {{ $v->order_status_str }}
                                        </td>
                                        <td class="numeric" style="vertical-align: middle;color: red; font-weight: bold;">
                                            <button data-id="{{$v->id}}" class="btn btn-blue" style="width:97px;height: 34px;">@lang('message.add.black')</button> <br /><br />
                                        </td>
                                    </tr>
                                </div>
                            @endforeach
                            @foreach($otherOrder as $k=>$v)
                                <div>
                                    <tr >
                                        <td>{{ $k+2 }}</td>
                                        <td>
                                            {{--@lang('message.order.number'):--}}
                                            {{$v->order_number}}
                                            <br />
                                            <video width="160px" height="120px" controls="controls" preload="none" class="custom-video" onclick="this.play();">
                                                <source src="{{ $v->video_url }}" type="video/mp4">
                                            </video>
                                        </td>
                                        <td class="numeric">
                                            {{--@lang('message.ktp.number'):--}}
                                            {{$v->ktp_number}}
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->ktp_image_url }}" data-caption="KTP:{{$v->ktp_number}} {{__('message.client.name')}}:{{$v->name}}" style="width:160px; height: 120px;">

                                        </td>
                                        <td class="numeric">
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->work_image_url }}" data-caption="@lang('message.work.image')  {{__('message.client.name')}}:{{$v->name}}" style="width:160px; height: 120px;">
                                        </td>
                                        <td class="numeric">
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->salary_image_url }}" data-caption="@lang('final.image.salary')  {{__('message.client.name')}}:{{$v->name}} {{__('final.income.level')}}:{{$v->income_level}}" style="width:160px; height: 120px;">
                                        </td>
                                        <td class="numeric">
                                            @if ($v->tax_card_number)
                                                @if ($v->npwp)
                                                    {{ $v->npwp }}
                                                @else
                                                    {{ $v->tax_card_number }}
                                                @endif
                                                @if($v->tax_card_image_url)
                                                    <br />
                                                    <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->tax_card_image_url }}" data-caption="NPWP:@if ($v->npwp){{ $v->npwp }}@else{{ $v->tax_card_number }}@endif" style="width:160px; height: 120px;">
                                                @endif
                                            @endif
                                        </td>
                                        <td class="numeric" style="color: red; font-weight: bold;">
                                            {{ $v->order_status_str }}
                                        </td>
                                        <td class="numeric" style="vertical-align: middle;color: red; font-weight: bold;">
                                            <button data-id="{{$v->id}}" class="btn btn-blue" style="width:97px;height: 34px;">@lang('message.add.black')</button> <br /><br />
                                        </td>
                                    </tr>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </section>
                </div>
            </section>
        </div>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
<script src="{{ asset('/js/lazyload.min.js') }}"></script>
</body>


</html>

<script>
    //image lazy load
    var options = {
        threshold: 300, // default: 0. 另一个例子: 200
        data_attribute:'src', // default: 'original'. 另一个例子: 'url'
        effect:'fadein',
    }
    $("img.lazy").lazyload(options);


    $(document).on('click', '.btn-blue', function() {
        var type = $(this).parents().children('tr td:first').eq(0).html();
        if(type > 1) {
            $(".btn-xs").attr("href", "{{$back_url}}");
        } else {
            $(".btn-xs").attr("href", "{{$backUrl}}");
        }
        var orderId=$(this).attr('data-id');

        var that = this;
        $.ajax({
            url:"/loan/otherOrder",
            type:"POST",
            data:{
                id: '{{$id}}',
                order_id: orderId,
                _token:"{{csrf_token()}}"
            },
            dataType:"json",
            success:function(data){
                $(that).addClass('disabled');
                $.alert({
                    title: '@lang("message.make.sure.title")',
                    content: '@lang("black.addSuccess")',
                    buttons: {
                        okay: {
                            text: 'OK',
                            btnClass: 'btn-info'
                        }
                    }
                });
            },
            error:function(data){
                alert('Error!!');
            }
        });
    });

</script>