<div style="width:500px;margin:0 auto;">
    <form id="default" class="form-horizontal" action="/loan/selectSingleOrder" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset title="Initial Info">
            <div class="form-group">
                <label class="col-md-3 col-sm-2 control-label">@lang('message.order.number') </label>
                <div class="col-md-7 col-sm-6">
                    <input type="text" name="order_number" class="form-control">
                </div>
            </div>
        </fieldset>
        <div class="col-md-9 col-sm-9" style="text-align: center;margin-bottom: 15px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
        </div>
        <div style="clear: both"> </div>
    </form>
</div>