<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @include('loan/_loan_track')
            <section class="panel">
                <header class="panel-heading">
                    @lang('message.message.trial')
                    <span class="tools pull-right">
                        @if(Auth::user()->work_status==1)
                        <a href="/switchStatus?workStatus=0" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px;">@lang('message.stopWork')</a>
                        @else
                        <a href="/switchStatus?workStatus=1" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px;">@lang('message.startWork')</a>
                        @endif
                    </span>
                </header>
                <div class="panel-body">
                    <section id="flip-scroll">
                        <table class="table table-bordered table-striped table-condensed cf" id="table-message-trial" style="width: 80%">
                            <thead class="cf">
                            <tr>
                                <th>@lang('message.number')</th>
                                <th>@lang('message.vedio')</th>
                                <th class="numeric">KTP</th>
                                <th class="numeric">@lang('message.work.image')</th>
                                <th class="numeric">@lang('final.image.salary')</th>
                                <th class="numeric">NPWP</th>
                                <th class="numeric">@lang('message.operation')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $k=>$v)
                                <div>
                                    <tr >
                                        <td rowspan="3">{{ $k+1 }}</td>
                                        <td>
                                            {{--@lang('message.order.number'):--}}
                                            {{$v->order_number}}
                                            <br />
                                            <video width="160px" height="120px" controls="controls" preload="none" class="custom-video" onclick="this.play();">
                                                <source src="{{ $v->video_url }}" type="video/mp4">
                                            </video>
                                        </td>
                                        <td class="numeric">
                                            {{--@lang('message.ktp.number'):--}}
                                            {{$v->ktp_number}}
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->ktp_image_url }}" data-caption="KTP:{{$v->ktp_number}} {{__('message.client.name')}}:{{$v->name}}" style="width:160px; height: 120px;">

                                        </td>
                                        <td class="numeric">
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->work_image_url }}" data-caption="@lang('message.work.image')  {{__('message.client.name')}}:{{$v->name}}" style="width:160px; height: 120px;">
                                        </td>
                                        <td class="numeric">
                                            <br />
                                            <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->salary_image_url }}" data-caption="@lang('final.image.salary')  {{__('message.client.name')}}:{{$v->name}} {{__('final.income.level')}}:{{$v->income_level}}" style="width:160px; height: 120px;">
                                        </td>
                                        <td class="numeric">
                                            @if ($v->tax_card_number)
                                                @if ($v->npwp)
                                                    {{ $v->npwp }}
                                                @else
                                                    {{ $v->tax_card_number }}
                                                @endif
                                                @if($v->tax_card_image_url)
                                                <br />
                                                <img class="lazy" data-group="{{$v->order_number}}" data-magnify="gallery" data-src="{{ $v->tax_card_image_url }}" data-caption="NPWP:@if ($v->npwp){{ $v->npwp }}@else{{ $v->tax_card_number }}@endif" style="width:160px; height: 120px;">
                                                @endif
                                            @endif
                                        </td>
                                        @if($v->order_status == 17)
                                        <td class="numeric" rowspan="3" style="vertical-align: middle;">
                                            @if ($v['sameNameOrder']->count())
                                                <a href="/loan/otherOrder?id={{$v->id}}" class="btn btn-blue" style="width:97px;height: 34px;">@lang('black.sameNameOrder')</a> <br /><br />
                                            @endif
                                            <button class="btn btn-default active order-btn" type="button" style="width:97px;height: 34px;border: hidden;margin: 0px auto;color: #1b2128;background-color: #e1e1e1" data-order="{{$v->id}}" data-pic="" date-order-number="@if($orderNumber){{$orderNumber['order_number']}} @else 0 @endif" data-npwp="@if (empty($v->tax_card_number)) 0 @endif" data-npwp-info="{{ $v->npwp }}">@lang('message.submit')</button>
                                        </td>
                                         @endif
                                    </tr>
                                    <tr>
                                        <td align="center" class="choose">
                                            @if(Illuminate\Support\Facades\Session::get('locale')=='en')
                                                <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague1" data-val="1" data-vague1="10" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
                                            @else
                                                <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague1" data-val="1" data-vague1="10" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

                                            @endif
                                        </td>
                                        <td align="center" class="choose">
                                            @if(Illuminate\Support\Facades\Session::get('locale')=='en')
                                                <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague2" data-val="2" data-vague2="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
                                            @else
                                                <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague2" data-val="2" data-vague2="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

                                            @endif
                                        </td>
                                        <td align="center" class="choose">
                                            @if(Illuminate\Support\Facades\Session::get('locale')=='en')
                                                <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague3" data-val="3" data-vague3="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
                                            @else
                                                <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague3" data-val="3" data-vague3="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

                                            @endif
                                        </td>
                                        <td align="center" class="choose">
                                            @if(Illuminate\Support\Facades\Session::get('locale')=='en')
                                                <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague4" data-val="4" data-vague4="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
                                            @else
                                                <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague4" data-val="4" data-vague4="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

                                            @endif
                                        </td>
                                        <td align="center" class="choose">
                                            @if(Illuminate\Support\Facades\Session::get('locale')=='en')
                                                <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague5" data-val="5" data-vague5="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
                                            @else
                                                <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague5" data-val="5" data-vague5="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="choose">
                                            <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-pic" data-val="1" data-btn="btn-success" type="button">@lang('message.same')</button>
                                            <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-pic" data-val="2" data-btn="btn-danger" type="button">@lang('message.diffence')</button>
                                        </td>
                                        <td colspan="3" class="numeric choose" align="center">
                                            <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-npwp" data-val="1" data-btn="btn-success" type="button">@lang('message.effective')</button>
                                            <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-npwp" data-val="2" data-btn="btn-danger" type="button">@lang('message.invalid')</button>
                                        </td>
                                    </tr>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                        <div id="empty_data" @if($orders->count())style="display: none;"@endif>@lang('message.list_not_data_notice') <a href="/refresh?worktype=1" class="btn btn-success">@lang('message.refresh')</a></div>
                    </section>
                </div>
            </section>
        </div>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
<script src="{{ asset('/js/lazyload.min.js') }}"></script>
</body>


</html>

<script>
    //image lazy load
    var options = {
        threshold: 300, // default: 0. 另一个例子: 200
        data_attribute:'src', // default: 'original'. 另一个例子: 'url'
        effect:'fadein',
    }
    $("img.lazy").lazyload(options);

 //    $(document).on('click', '.choose span', function() {
 //
 //        $('#-message-trialtable tr').each(function(i){                   // 遍历 tr
 //            $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
 //                console.log("第"+(i+1)+"行，第"+(j+1)+"个td的值："+$(this).text()+"。");
 //            });
 //        });
 //    })

    //相关照片模糊·不清晰

    $(document).on('click', '.choose span', function() {
        var that = this;
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: '@lang("message.idfuzzy")',
            buttons: {
                confirm: {
                    text: '@lang("message.yes")',
                    action: function() {
                        $(that).parent().parent().prev().find('button').attr($(that).attr('data-input'), $(that).attr('data-val'));
                        $(that).parent().parent().prev().find('button').removeClass('active');
                        $(that).parent().parent().prev().find('button').click();
                    }
                },
                cancel: {
                    text: '@lang("message.cancel")',
                    btnClass: 'btn-info'
                }
            }
        });

        //重置按钮状态
//        $(this).parent().parent().prev().find('button').removeClass('active');
//        var td = $(this).parent().index();//当前td
//        // var td = $(this).prev().html();//当前td
//        var tr= $(this).parent().parent().index();//当前tr
//        var nextTr=tr+2;
//        if(td == 0 ){
//            var offset = 1;
//            var offset1 = 2;
//        } else if(td == 1) {
//            var offset = 0;
//            var offset1 = 2;
//        } else if(td == 2) {
//            var offset = 0;
//            var offset1 = 1;
//        } else if(td == 3) {
//            var offset =2;
//        }

//        if ($(this).hasClass('btn-danger')) {

//            $(this).removeClass('btn-danger');

//            if(td == 2 && $(this).parent().parent().find('td').eq(offset).find('span').hasClass('btn-danger') === false && $(this).parent().parent().find('td').eq(offset1).find('span').hasClass('btn-danger') === false)
//            {
//                $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(0) button").removeClass('disabled');
//
//            }
//            if(td == 1 && $(this).parent().parent().find('td').eq(offset).find('span').hasClass('btn-danger') === false && $(this).parent().parent().find('td').eq(offset1).find('span').hasClass('btn-danger') === false)
//            {
//                $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(0) button").removeClass('disabled');
//            }
//            if(td == 0 && $(this).parent().parent().find('td').eq(offset).find('span').hasClass('btn-danger') === false && $(this).parent().parent().find('td').eq(offset1).find('span').hasClass('btn-danger') === false)
//            {
//                $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(0) button").removeClass('disabled');
//            }
//
//            if($(this).parent().parent().find('td').eq(offset).find('span').hasClass('btn-danger') === false) {
//                if ( td == 3) {
//                    $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(1) button").removeClass('disabled');
//                }
//                if (td == 4) {
//                    $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(2) button").removeClass('disabled');
//                }
//            }


//        } else if(!$(this).hasClass('btn-danger')){
//            $(this).parent().parent().prev().find('button').attr($(this).attr('data-input'), $(this).attr('data-val'));

//            $(this).addClass('btn-danger');

//            if(td == 0 || td == 1 || td == 2)  {
//                $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(0) button").addClass('disabled');
//            } else if ( td == 3) {
//                $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(1) button").addClass('disabled');
//            } else if (td == 4) {
//                $("#-message-trialtable tr:eq(" + nextTr + ") td:eq(2) button").addClass('disabled');
//            }
//        }
    });

    $(document).on('click', '.choose button', function() {
        var index=$(this).parent().index();
        // console.log(index);
        if(index == 0) {
            $(this).parent().parent().prev().find('span').eq(0).addClass('disabled');
            $(this).parent().parent().prev().find('span').eq(1).addClass('disabled');

        } else if(index == 1) {
            $(this).parent().parent().prev().find('span').eq(2).addClass('disabled');
            $(this).parent().parent().prev().find('span').eq(3).addClass('disabled');
            $(this).parent().parent().prev().find('span').eq(4).addClass('disabled');
        }

        //重置按钮状态
        $(this).parent().find('button').removeClass('btn-success').removeClass('btn-danger');
        //更新被点击按钮状态
        $(this).addClass($(this).attr('data-btn'));
        //设置状态值
        $(this).parent().parent().prev().prev().find('button').attr($(this).attr('data-input'), $(this).attr('data-val'));
        //更新提交按钮状态
        if($(this).parent().parent().prev().prev().find('button').attr('data-pic') && $(this).parent().parent().prev().prev().find('button').attr('data-npwp')){
            $(this).parent().parent().prev().prev().find('button').removeClass('active');
        }
    });

//    $('.order-btn').click(function(){
    $(document).on('click', '.order-btn', function() {

        if($(this).hasClass('active')) return;

        //防止重复点击
        var oldBtn = $(this).html();
        $(this).html('loding..');
        $(this).attr('disabled', true);
        var param = new Array();

        if($(this).attr('data-order')==null){
            var orderId=0;
        }else {
            var orderId=$(this).attr('data-order');
        }
        if($(this).attr('data-pic')==null){
            var picStatus = 0;
        }else {
            var picStatus = $(this).attr('data-pic');
        }
        if($(this).attr('data-npwp')==null){
            var npwpStatus = 0;

        }else {
            var npwpStatus = $(this).attr('data-npwp');

        }
        if($(this).attr('data-salary')==null){
            var salaryStatus = 0;

        }else {
            var salaryStatus = $(this).attr('data-salary');

        }
        if($(this).attr('data-npwp-info')==null){
            var npwp = 0;

        }else {
            var npwp = $(this).attr('data-npwp-info');

        }
        if($(this).attr('date-order-number')==null){
            var orderNumber = 0;

        }else {
            var orderNumber = $(this).attr('date-order-number');

        }
        // var picStatus = $(this).attr('data-pic');
        // var npwpStatus = $(this).attr('data-npwp');
        var salaryStatus = $(this).attr('data-salary');
        var npwp = $(this).attr('data-npwp-info');
        var orderNumber = $(this).attr('date-order-number');
         param[0] = $(this).attr('data-vague1');
         param[1] = $(this).attr('data-vague2');
         param[2] = $(this).attr('data-vague3');
         param[3] = $(this).attr('data-vague4');
         param[4] = $(this).attr('data-vague5');
        var orderNumber = $(this).attr('date-order-number');
        console.log(param);
        var tr = $(this);
        $.ajax({
            url:"/loan/loginInfo",
            type:"POST",
            data:{
                order_id: orderId,
                pic_status: picStatus,
                npwp_status: npwpStatus,
                orderNumber: orderNumber,
                salary_status: salaryStatus,
                img_status:param,
                npwp: npwp,
                _token:"{{csrf_token()}}"
            },
            dataType:"json",
            success:function(data){
                console.log(data)
                if(data.code ==0){
                    window.location.replace('/loan/loginInfo');
                }
                if(data.code==11)
                {
                    $.alert({
                        title: '审核结果',
                        content: data.info,
                        buttons: {
                            okay: {
                                text: 'OK',
                                btnClass: 'btn-info'
                            }
                        }
                    });
                }
                tr.attr('date-order-number', 2);
                tr.parent().parent().next().next().remove();

                tr.parent().parent().next().remove();

                tr.parent().parent().remove();


                $("img.lazy").lazyload(options);
                getNewOrder(orderNumber);
                toggleNotice();
            },
            error:function(data){
                tr.html(oldBtn);
                tr.attr('disabled', false);
            }
        });
    });
//    getNewOrder();
    function getNewOrder(orderNumber)
    {
        //检查页面显示数量
        var orderCount = $("#table-message-trial tbody tr").length;
        if(orderCount>0){
            orderCount = orderCount/3;
        }
        $.ajax({
            url:"/loan/add",
            type:'GET',
            data: {
                orderCount: orderCount
            },
            dataType:"html",
            success:function (data) {
                console.log(data);
                if(data){
                    $("#table-message-trial tbody").append(data);
                    $("img.lazy").lazyload(options);
                } else {
                    console.log('message trial sum too small')
                }
            }
        });
    }

    /**
     * 如果列表为空，则显示提示信息及刷新按钮
     */
    function toggleNotice()
    {
        var orderCount = $("#table-message-trial tbody tr").length;
        if(orderCount==0){
            $("#empty_data").show();
        }else{
            $("#empty_data").hide();
        }
    }
</script>