@if($orders)
@foreach($orders as $order)
<tr>
    <td rowspan="3">{{$order->id}}</td>
    <td>
        {{--@lang('message.order.number'):--}}
        {{$order->order_number}}
        <br />
        <video width="160" height="120" controls="controls" preload="none" class="custom-video" onclick="this.play();">
            <source src="{{ $order->video_url }}" type="video/mp4">
        </video>
    </td>
    <td class="numeric">
        {{--@lang('message.ktp.number'):--}}
        {{$order->ktp_number}}
        <br />
        <img class="lazy" data-magnify="gallery" data-src="{{ $order->ktp_image_url }}" data-caption="KTP:{{$order->ktp_number}}" style="width:160px; height: 120px;">

    </td>
    <td class="numeric">
        <br />
        <img class="lazy" data-magnify="gallery" data-src="{{ $order->work_image_url }}" data-caption="@lang('message.work.image') {{__('message.client.name')}}:{{$order->name}}" style="width:160px; height: 120px;">
    </td>
    <td class="numeric">
        <br />
        <img class="lazy" data-group="{{$order->order_number}}" data-magnify="gallery" data-src="{{ $order->salary_image_url }}" data-caption="@lang('final.image.salary')  {{__('message.client.name')}}:{{$order->name}} {{__('final.income.level')}}:{{$order->income_level}}" style="width:160px; height: 120px;">
    </td>
    <td class="numeric">
        @if ($order->tax_card_number)
            @if ($order->npwp)
                {{ $order->npwp }}
            @else
                {{ $order->tax_card_number }}
            @endif
            <br />
            <img class="lazy" data-magnify="gallery" data-src="{{ $order->tax_card_image_url }}" data-caption="NPWP:@if ($order->npwp){{ $order->npwp }}@else{{ $order->tax_card_number }}@endif" style="width:160px; height: 120px;">
        @endif
    </td>
    <td class="numeric" rowspan="3" style="vertical-align: middle;">
        @if ($order['sameNameOrder']->count())
            <a href="/loan/otherOrder?id={{$order->id}}" class="btn btn-blue" style="width:97px;height: 34px;">@lang('black.sameNameOrder')</a> <br /><br />
        @endif
        <button class="btn btn-default active order-btn" type="button" style="width:88px;height: 34px;border: hidden;margin: 0px auto;color: #1b2128;background-color: #e1e1e1" data-order="{{$order->id}}" data-pic="" date-order-number="@if($orderNumber){{$orderNumber['order_number']}} @else 0 @endif" data-npwp="@if (empty($order->tax_card_number)) 0 @endif" data-npwp-info="{{ $order->npwp }}">@lang('message.submit')</button>
    </td>
</tr>
<tr>
    <td align="center" class="choose">
        @if(Illuminate\Support\Facades\Session::get('locale')=='en')
            <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague1" data-val="1" data-vague1="10" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
        @else
            <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague1" data-val="1" data-vague1="10" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

        @endif
    </td>
    <td align="center" class="choose">
        @if(Illuminate\Support\Facades\Session::get('locale')=='en')
            <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague2" data-val="2" data-vague2="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
        @else
            <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague2" data-val="2" data-vague2="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

        @endif
    </td>
    <td align="center" class="choose">
        @if(Illuminate\Support\Facades\Session::get('locale')=='en')
            <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague3" data-val="3" data-vague3="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
        @else
            <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague3" data-val="3" data-vague3="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

        @endif
    </td>
    <td align="center" class="choose">
        @if(Illuminate\Support\Facades\Session::get('locale')=='en')
            <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague4" data-val="4" data-vague4="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
        @else
            <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague4" data-val="4" data-vague4="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

        @endif
    </td>
    <td align="center" class="choose">
        @if(Illuminate\Support\Facades\Session::get('locale')=='en')
            <span class="btn btn-default " style="width:100%;height: 68px;border: hidden" data-input="data-vague5" data-val="5" data-vague5="0" data-btn="btn-danger" type="button">@lang('message.prefix')<br/>@lang('message.imgvalid')</span>
        @else
            <span class="btn btn-default " style="width:100%;height: 34px;border: hidden" data-input="data-vague5" data-val="5" data-vague5="0" data-btn="btn-danger" type="button">@lang('message.imgvalid')</span>

        @endif
    </td>
</tr>
<tr>
    <td colspan="2" align="center" class="choose">
        <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-pic" data-val="1" data-btn="btn-success" type="button">@lang('message.same')</button>
        <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-pic" data-val="2" data-btn="btn-danger" type="button">@lang('message.diffence')</button>
    </td>
    <td colspan="3" class="numeric choose" align="center">
        <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-npwp" data-val="1" data-btn="btn-success" type="button">@lang('message.effective')</button>
        <button class="btn btn-default active" style="width:67px;height: 34px;border: hidden" data-input="data-npwp" data-val="2" data-btn="btn-danger" type="button">@lang('message.invalid')</button>
    </td>
</tr>
@endforeach
@endif