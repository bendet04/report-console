<!--通讯录-->
@if($data['result_code']==10000&&!empty($data['data']))
    <div class="custom-section-title-square">@lang('final.record.trial')
        <span class="red">({{$data['data']['total']}})</span>
        <img src="/images/small-triangle.png.png" alt="">
    </div>
    <div class="panel-body panel-body-no-padding">
        <section class="panel section-panel-no-margin">
            <header class="panel-heading custom-tab custom-tab-20">
                <ul class="nav nav-tabs">
                    {{--<li class="active">--}}
                        {{--<a href="#contact1" data-toggle="tab" aria-expanded="true">总记录条数<br />@if(!empty($data['data']['total']))({{$data['data']['total']}}) @else (0) @endif</a>--}}
                        {{--<a href="#contact1" data-toggle="tab" aria-expanded="true">总记录条数<br /></a>--}}

                    {{--</li>--}}
                    {{--<li class="active" >--}}
                        {{--<a href="#contact2" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.record')<br />@if(!empty($data['data']['call_log'])){{count($data['data']['call_log'])}} @else 0 @endif</a>--}}
                    {{--</li>--}}
                    <li class="active">
                        <a href="#contact3" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.relation')<br />@if(!empty($data['data']['contact'])){{count($data['data']['contact'])}} @else 0 @endif</a>
                    </li>
                    <li class="">
                        <a href="#contact4" data-toggle="tab" aria-expanded="false">@lang('final.white.list')<br />@if(!empty($data['data']['whitelist'])){{count($data['data']['whitelist'])}} @else 0 @endif</a>
                    </li>
                    <li class="">
                        <a href="#contact5" data-toggle="tab" aria-expanded="false">@lang('final.black.list')<br />@if(!empty($data['data']['blacklist']))({{count($data['data']['blacklist'])}}) @else 0 @endif</a>
                    </li>

                </ul>
            </header>
            <div style="clear:both;"></div>
            <div class="panel-body custom-panel-body1">
                <div class="tab-content">
                    {{--<div class="tab-pane custom-tab-pane active" id="contact1">--}}
                        {{--@foreach($device->phone_contact as $k=>$v)--}}
                            {{--@if(!empty($v['name']))--}}
                                {{--<div class="col-lg-3">{{$v['name']}}</div>--}}
                            {{--@endif--}}
                            {{--@if(!empty($v['mobile']))--}}
                                {{--<div class="col-lg-12 tab-pane-content">{{$v['mobile']}}</div>--}}
                            {{--@endif--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane custom-tab-pane active" id="contact2" >--}}
                        {{--@foreach($data['data']['call_log'] as $k=>$v)--}}
                            {{--@if(!empty($v['name']))--}}
                                {{--<div class="col-lg-3">{{$v['name']}}</div>--}}
                            {{--@endif--}}
                            {{--@if(!empty($v['mobile']))--}}
                                {{--<div class="col-lg-12 tab-pane-content">{{$v['mobile']}}</div>--}}
                            {{--@endif--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                    <div class="tab-pane custom-tab-pane active" id="contact3" >
                        @foreach($data['data']['contact'] as $k=>$v)
                            @if(!empty($v['name']))
                                <div class="col-lg-3">{{$v['name']}}</div>
                            @endif
                            @if(!empty($v['mobile']))
                                <div class="col-lg-12 tab-pane-content">{{$v['mobile']}}</div>
                            @endif
                        @endforeach

                    </div>
                    <div class="tab-pane custom-tab-pane" id="contact4" >
                        @foreach($data['data']['whitelist'] as $k=>$v)
                            @if(!empty($v['name']))
                                <div class="col-lg-3">{{$v['name']}}</div>
                            @endif
                            @if(!empty($v['mobile']))
                                <div class="col-lg-12 tab-pane-content">{{$v['mobile']}}</div>
                            @endif
                        @endforeach

                    </div>
                    <div class="tab-pane custom-tab-pane" id="contact5" >
                        @foreach($data['data']['blacklist'] as $k=>$v)
                            @if(!empty($v['name']))
                                <div class="col-lg-3">{{$v['name']}}</div>
                            @endif
                            @if(!empty($v['mobile']))
                                <div class="col-lg-12 tab-pane-content">{{$v['mobile']}}</div>
                            @endif
                        @endforeach

                    </div>


                </div>
            </div>
        </section>
    </div>
@endif
<!--通话记录审核end-->