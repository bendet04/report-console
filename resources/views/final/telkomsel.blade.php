<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Content</th>
                </tr>
            </thead>
            <tbody>
            @if(!empty($partner))
                @if(!empty($partner['account_info']['tcash_account']))
                <tr class="">
                    <td>
                        tcash@lang('final.account')：
                    </td>
                    <td>
                        {{ $partner['account_info']['tcash_account']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['tcash_balance']))
                <tr class="">
                    <td>
                        tchash@lang('final.money.other'):
                    </td>
                    <td>
                        {{$partner['account_info']['tcash_balance']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['telkomsel_poin']))
                <tr class="">
                    <td>
                        talkomsel@lang('final.sum.to.score'):
                    </td>
                    <td>
                        {{$partner['account_info']['telkomsel_poin']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['kartu_as']))
                <tr class="">
                    <td>
                        @lang('final.local.card'):
                    </td>
                    <td>
                        {{$partner['account_info']['kartu_as']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['active_until']))
                <tr class="">
                    <td>
                        @lang('final.info.deadline'):
                    </td>
                    <td>
                        {{$partner['account_info']['active_until']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['remaining_credits']))
                <tr class="">
                    <td>
                        @lang('final.other.credit')::
                    </td>
                    <td>
                        {{$partner['account_info']['remaining_credits']}}                        {{$partner['account_info']['active_until']}}

                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['salutation']))
                <tr class="">
                    <td>
                        @lang('final.nick.name'):
                    </td>
                    <td>
                        {{$partner['account_info']['salutation']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['first_name']))
                <tr class="">
                    <td>
                        @lang('basic.name.last'):
                    </td>
                    <td>
                        {{$partner['account_info']['first_name']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['last_name']))
                <tr class="">
                    <td>
                        @lang('basic.name.first'):
                    </td>
                    <td>
                        {{$partner['account_info']['last_name']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['account_info']['puk_code']))
                <tr class="">
                <td>
               puk
                </td>
                <td>

                <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;"onclick="relativeCheck()" data-target="#relativeInfo">select</a>
                </td>
                </tr>
                @endif
            @endif
            </tbody>
        </table>

    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" style="margin-right:300px; "  id="test-close" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>


<!--详细清单展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="relativeInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Spt Data Info</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                    <tr>
                        <th>@lang('messge.number')</th>
                        <th>PUK</th>

                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($partner['account_info']['puk_code']))
                        @foreach($partner['account_info']['puk_code'] as $key => $value)
                    <tr class="">
                        <td>
                            {{$key+1}}
                        </td>
                        <td>
                            {{$value}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="detailChildData-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<script>
    //朋友的信息关闭
    function relativeCheck(obj)
    {
        $('#detailChildData-close').click(function () {
            $("#relativeInfo").modal('hide');
        })

    }
</script>
