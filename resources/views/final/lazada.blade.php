<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
            </tr>
            </thead>
            @if(!empty($partner))
            <tbody>
            @if(!empty($partner['personal_info']['phone_number']))
            <tr class="">
                <td>
                    Phone：
                </td>
                <td>
                    {{$partner['personal_info']['phone_number']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_info']['email_address']))
            <tr class="">
                <td>
                    Email：
                </td>
                <td>
                    {{$partner['personal_info']['email_address']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_info']['name']))
            <tr class="">
                <td>
                    Name：
                </td>
                <td>
                    {{$partner['personal_info']['name']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_info']['gender']))
            <tr class="">
                <td>
                    @lang('basic.sex')：
                </td>
                <td>
                    {{$partner['personal_info']['gender']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_info']['birthday']))
            <tr class="">
                <td>
                    @lang('basic.birth.time')：
                </td>
                <td>
                    {{$partner['personal_info']['birthday']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['address_book']['shipping_address']))
            <tr class="">
                <td>
                    @lang('final.diliver.address')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="shopAddressCheck()" data-target="#shopAddressInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['address_book']['billing_address']))
            <tr class="">
                <td>
                    @lang('final.bill.address')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="billAddressCheck()" data-target="#billAddressInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['address_book']['other_address']))
            <tr class="">
                <td>
                    @lang('final.other.address')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="otherAddressCheck()" data-target="#otherAddressInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['orders']))
            <tr class="">
                <td>
                    @lang('final.order.order')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="orderCheck()" data-target="#orderInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['wishlist']))
            <tr class="">
                <td>
                    @lang('final.like')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="wishlistCheck()" data-target="#wishlistInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['payment_method']))
            <tr class="">
                <td>
                    @lang('final.payment.type')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="paymentMethodCheck()" data-target="#paymentMethodInfo">select</a>
                </td>
            </tr>
            @endif

            </tbody>
            @endif
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">关闭</button>
    </div>
</form>

<!--其他地址展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="otherAddressInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Other Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>Name</th>
                        <th>@lang('message.agency.admin.address')</th>
                        <th>@lang('final.distinct')</th>
                        <th>@lang('message.agency.admin.phone')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['address_book']['other_address']))
                    @foreach($partner['address_book']['other_address'] as $key => $value)
                    <tr class="">
                        <td>{{$key+1}}</td>
                        <td>{{$value['name']}}</td>
                        <td>{{$value['address']}}</td>
                        <td>{{$value['area']}}</td>
                        <td>{{$value['phone_number']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="relative-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--收货地点展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="shopAddressInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Shop Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>Name</th>
                        <th>@lang('message.agency.admin.address')</th>
                        <th>@lang('final.distinct')</th>
                        <th>@lang('message.agency.admin.phone')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['address_book']['shipping_address']))
                    <tr class="">
                        <td>{{$partner['address_book']['shipping_address']['name']}}</td>
                        <td>{{$partner['address_book']['shipping_address']['address']}}</td>
                        <td>{{$partner['address_book']['shipping_address']['area']}}</td>
                        <td>{{$partner['address_book']['shipping_address']['phone_number']}}</td>
                    </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="shopAddress-close">关闭</button>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="billAddressInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Billing Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>Name</th>
                        <th>@lang('message.agency.admin.address')</th>
                        <th>@lang('final.distinct')</th>
                        <th>@lang('message.agency.admin.phone')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['address_book']['billing_address']))
                    <tr class="">
                        <td>{{$partner['address_book']['billing_address']['name']}}</td>
                        <td>{{$partner['address_book']['billing_address']['address']}}</td>
                        <td>{{$partner['address_book']['billing_address']['area']}}</td>
                        <td>{{$partner['address_book']['billing_address']['phone_number']}}</td>
                    </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="billAddress-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--订单展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="orderInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Info</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.order.number')</th>
                        <th>@lang('final.order.phone')</th>
                        <th>@lang('final.sum.amount')</th>
                        <th>@lang('final.order.amount')</th>
                        <th>@lang('final.deliver.amount')</th>
                        <th>@lang('final.discount.amount')</th>
                        <th>@lang('final.diliver.address')</th>
                        <th>@lang('final.bill.address')</th>
                        <th>@lang('final.package.info')</th>
                        <th>@lang('final.order.info')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['orders']))
                    @foreach($partner['orders'] as $key => $value)
                    <tr class="">
                        <td>{{$key+1}}</td>
                        <td>{{$value['order_no']}}</td>
                        <td>{{$value['order_time']}}</td>
                        <td>{{$value['total_number']}}</td>
                        <td>{{$value['sub_total']}}</td>
                        <td>{{$value['shipping_cost']}}</td>
                        <td>{{$value['promotion']}}</td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="dispatchAddressCheck()" data-target="#dispatchAddressInfo">select</a>
                        </td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="orderBillAddressCheck()" data-target="#orderBillAddressInfo">select</a>
                        </td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="packageCheck()" data-target="#packageInfo">select</a>
                        </td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="orderDetailCheck()" data-target="#orderDetailInfo">select</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="order-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--配送地址展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="dispatchAddressInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Billing Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>@lang('message.agency.admin.address')</th>
                        <th>@lang('message.agency.admin.phone')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($value['shipping_address']))
                    <tr class="">
                        <td>{{$value['shipping_address']['name']}}</td>
                        <td>{{$value['shipping_address']['address']}}</td>
                        <td>{{$value['shipping_address']['phone_number']}}</td>
                    </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="dispatchAddress-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--订单的账单展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="orderBillAddressInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Billing Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>@lang('message.agency.admin.address')</th>
                        <th>@lang('message.agency.admin.phone')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($value['billing_address']))
                    <tr class="">
                        <td>{{$value['billing_address']['name']}}</td>
                        <td>{{$value['billing_address']['address']}}</td>
                        <td>{{$value['billing_address']['phone_number']}}</td>
                    </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="orderBilladdress-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--包裹信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="packageInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Package</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.seller')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($value['packages']))
                    @foreach($value['packages'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['sold_by']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="package-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--订单商品信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="orderDetailInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.product.name')</th>
                        <th>@lang('final.price')</th>
                        <th>@lang('final.count.number')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($value['goods']))
                    @foreach($value['goods'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['name']}}</td>
                        <td>{{$value['price']}}</td>
                        <td>{{$value['amount']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="orderDetail-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--收藏展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="wishlistInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Other Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.wish.name')</th>
                        <th>@lang('final.count.number')</th>
                        <th>@lang('final.product.info')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['wishlist']))
                    @foreach($partner['wishlist'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['name']}}</td>
                        <td>{{$value['total_item']}}</td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="wishlistDetailCheck()" data-target="#wishlistDetailInfo">select</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="wishlist-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--收藏包裹信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="wishlistDetailInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Package</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.seller')</th>
                        <th>@lang('final.product.last')</th>
                        <th>@lang('final.price')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($value['items']))
                    @foreach($value['items'] as $key => $lvalue)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$lvalue['name']}}</td>
                        <td>{{$lvalue['availability']}}</td>
                        <td>{{$lvalue['price']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="wishlistDetail-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--支付展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="paymentMethodInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Package</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.card.type.new')</th>
                        <th>@lang('final.end.time')</th>
                        <th>@lang('final.expiry')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['payment_method']))
                    @foreach($partner['payment_method'] as $key => $lvalue)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$lvalue['card_type']}}</td>
                        <td>{{$lvalue['ends_with']}}</td>
                        <td>{{$lvalue['valid_util']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="paymentMethod-close">关闭</button>
            </div>
        </div>
    </div>
</div>


<script>
    //购物地址的信息关闭
    function shopAddressCheck()
    {
        $('#shopAddress-close').click(function () {
            $("#shopAddressInfo").modal('hide');
        })

    }
    //账单地址的信息关闭
    function billAddressCheck()
    {
        $('#billAddress-close').click(function () {
            $("#billAddressInfo").modal('hide');
        })

    }
    //其他地址的信息关闭
    function otherAddressCheck()
    {
        $('#otherAddress-close').click(function () {
            $("#otherAddressInfo").modal('hide');
        })

    }
    //订单的信息关闭
    function orderCheck()
    {
        $('#order-close').click(function () {
            $("#orderInfo").modal('hide');
        })

    }
    //订单的配送地址关闭
    function dispatchAddressCheck()
    {
        $('#dispatchAddress-close').click(function () {
            $("#dispatchAddressInfo").modal('hide');
        })

    }
    //订单的账单地址关闭
    function orderBillAddressCheck()
    {
        $('#orderBilladdress-close').click(function () {
            $("#orderBillAddressInfo").modal('hide');
        })

    }
    //包裹的信息关闭
    function packageCheck()
    {
        $('#package-close').click(function () {
            $("#packageInfo").modal('hide');
        })

    }
    //商品的信息关闭
    function orderDetailCheck()
    {
        $('#orderDetail-close').click(function () {
            $("#orderDetailInfo").modal('hide');
        })

    }
    //收藏的信息关闭
    function wishlistCheck()
    {
        $('#wishlist-close').click(function () {
            $("#wishlistInfo").modal('hide');
        })

    }
    //收藏商品的信息关闭
    function wishlistDetailCheck()
    {
        $('#wishlistDetail-close').click(function () {
            $("#wishlistDetailInfo").modal('hide');
        })

    }
    //支付的信息关闭
    function paymentMethodCheck()
    {
        $('#paymentMethod-close').click(function () {
            $("#paymentMethodInfo").modal('hide');
        })

    }

</script>
