<div class="page-heading">
    <div class="col-lg-12">
        <section class="custom-panel">
            <header class="custom-heading custom-heading-big">@lang('final.message.phone.trial')</header>
            <div>
                <header class="custom-heading sub-title-final">@lang('final.infoTrial')<img src="/images/big-triangle.png" alt=""></header>
                <div class="custom-section">

                    <div class="custom-section-title-square">@lang('final.image.trial')</div>
                    <div class="panel-body panel-body-no-padding">
                        <section class="panel">
                            <div class="col-lg-4 phone-ID">
                                <span><img src="{{ $idPhoto->work_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->work_image_url}}" data-caption="@lang('final.image.work')" width="313" height="200" alt="@lang('final.image.work')"></span>
                                @lang('final.image.work')
                            </div>
                            <div class="col-lg-4 phone-ID">
                                <span><img src="{{ $idPhoto->ktp_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->ktp_image_url}}" data-caption="KTP" width="313" height="200" alt="KTP"></span>
                                KTP
                                @if($userBasic->ktp_verity == 1)
                                    <a style="color: red;">
                                     @lang('final.has.verity')
                                    </a>
                                @elseif($userBasic->ktp_verity == 2)
                                    <a style="color: red;">
                                        @lang('final.has.verity.fail')
                                    </a>
                                @else

                                <a id='checkVerity' style="cursor: pointer;display:inline;" data-id="{{$idPhoto->id}}" data-url="{{$url}}">
                                    @lang('final.verity')
                                </a>
                                    <a style="color: red;" id="verityResult">
                                    </a>
                                @endif
                                @if(!empty($ktpSimlar))
                                    <br /><a href="/loan/otherKtpOrder?id={{$order->id}}" class="btn btn-blue">@lang('black.sameKtpOrder')</a>
                                @endif
                            </div>
                            @if($idPhoto->tax_card_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->tax_card_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->tax_card_image_url}}" data-caption="NPWP" width="313" height="200" alt="NPWP"></span>
                                    NPWP
                                </div>
                            @endif
                            @if(!empty($infoTrial->pic_status))
                                <div class="col-lg-8 custom-btn">@if($infoTrial->pic_status==1)<button class="btn btn-lg btn-success">@lang('final.same')</button>@else<button class="btn btn-lg btn-danger">@lang('final.diff')</button>@endif</div>
                                @if($idPhoto->tax_card_image_url)
                                    <div class="col-lg-4 custom-btn">@if($infoTrial->npwp_status==1)<button class="btn btn-lg btn-success">@lang('final.same')</button>@else<button class="btn btn-lg btn-danger">@lang('final.diff')</button>@endif</div>
                                @endif
                            @endif
                        </section>
                    </div>
                    <!--证件照审核end-->

                    <div class="custom-section-title-square">@lang('final.image.other.trial')</div>
                    <div class="panel-body panel-body-no-padding">
                        <section class="panel">
                            @if ($idPhoto->salary_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->salary_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->salary_image_url}}" data-caption="@lang('final.image.salary')" alt="@lang('final.image.salary')" width="313" height="200"></span>
                                    @lang('final.image.salary')
                                </div>
                            @endif
                            @if ($idPhoto->credit_card)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->credit_card }}" data-magnify="gallery" data-src="{{$idPhoto->credit_card}}" data-caption="@lang('final.image.credit.card')" alt="@lang('final.image.credit.card')" width="313" height="200"></span>
                                    @lang('final.image.credit.card')
                                </div>
                            @endif
                            @if ($idPhoto->social_card_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->social_card_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->social_card_image_url}}" data-caption="@lang('final.image.social.card')" alt="@lang('final.image.social.card')" width="313" height="200"></span>
                                    @lang('final.image.social.card')
                                </div>
                            @endif
                            @if ($idPhoto->driving_license_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->driving_license_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->driving_license_image_url}}" data-caption="@lang('final.image.driver.card')" alt="@lang('final.image.driver.card')" width="313" height="200"></span>
                                    @lang('final.image.driver.card')
                                </div>
                            @endif

                            @if ($idPhoto->living_fee_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->living_fee_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->living_fee_image_url}}" data-caption="@lang('final.image.water.card')" alt="@lang('ifinal.mage.water.card')" width="313" height="200"></span>
                                    @lang('final.image.water.card')
                                </div>
                            @endif
                            @if ($idPhoto->rent_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->rent_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->rent_image_url}}" data-caption="@lang('image.rent')" alt="@lang('image.rent')" width="313" height="200"></span>
                                    @lang('image.rent')
                                </div>
                            @endif
                            @if ($idPhoto->bank_flow_image_url)
                                <div class="col-lg-4 phone-ID">
                                    <span><img src="{{ $idPhoto->bank_flow_image_url }}" data-magnify="gallery" data-src="{{$idPhoto->bank_flow_image_url}}" data-caption="@lang('final.image.other.repayment')" alt="@lang('final.image.other.repayment')" width="313" height="200"></span>
                                    @lang('final.image.other.repayment')
                                </div>
                            @endif
                                <div class="col-lg-4 phone-ID">
                                    <span>
                                        <video width="336" height="200" controls="controls" preload="none" class="custom-video" onclick="this.play();">
                                            <source src="{{ $phoneTrial->user->video_url }}" type="video/mp4">
                                        </video>
                                    </span>
                                    @lang('final.vidio')
                                </div>
                        </section>
                    </div>
                    {{--<!--其他证件照审核end-->--}}
                </div>
            </div>

            <div>
                <header class="custom-heading sub-title-final top-line">@lang('final.phoneTrial')<img src="/images/big-triangle.png" alt=""><span class="pull-right red">@lang('final.all.same'): {{$rightCount}} @lang('final.systerm.all.same'): {{$normalCount}}</span></header>
                <div class="custom-section">
                    <div class="custom-section-title-square bottom-line">
                        @lang('final.phone.retrial')
                        @if(empty($phoneTrial->work['company_phone']))(@lang('final.oldClientNotrial'))@endif
                        @if($phoneTrial->work['company_phone']==4)(@lang('message.personal.modify.holiday'))@endif
                        @if($phoneTrial->work['company_phone']==5)(@lang('message.phone.company.permit'))@endif
                        {{--工作电核备注start--}}
                        @foreach(['second'=>'message.phone.ask.call.second', 'one'=>'message.phone.ask.call.one', 'now'=>'message.phone.ask.take.right.now', 'slow'=>'message.phone.ask.take.slowly'] as $k=>$v)
                        @if(isset($phoneTrial->work['work_remark'][$k]) && $phoneTrial->work['work_remark'][$k]==1)
                        <span class="normal-range" style="padding-right: 10px">@lang($v)</span>
                        @endif
                        @endforeach

                        @foreach(['env'=>'message.phone.ask.environment.noise', 'ear'=>'message.phone.ask.header.ear', 'zzz'=>'message.phone.ask.take.phone.zzz', 'other'=>'message.phone.ask.personal.self'] as $k=>$v)
                        @if(isset($phoneTrial->work['work_remark'][$k]) && $phoneTrial->work['work_remark'][$k]==1)
                        <span class="fail-range" style="padding-right: 10px">@lang($v)</span>
                        @endif
                        @endforeach
                        {{--工作电核备注end--}}
                    </div>
                    <div class="col-lg-6" style="padding-right: 0px;">
                        {{--<table class="custom-table-trial custom-table-info @if(empty($phoneTrial->work['company_phone']) || (isset($phoneTrial->work['company_phone']) && !in_array($phoneTrial->work['company_phone'], [1,2,3]))) empty-data @endif">--}}
                        <table class="custom-table-trial custom-table-info ">
                            <tbody>
                                <tr>
                                    <th>@lang('final.same')</th>
                                </tr>
                                {{--@if(empty($phoneTrial->work['company_phone']) || (isset($phoneTrial->work['company_phone']) && $phoneTrial->work['company_phone']==1))--}}
                                @if(empty($phoneTrial->work['company_phone']) || (isset($phoneTrial->work['company_phone'])))
                                <tr>
                                    <td>@lang('message.phone.company.phone') <span class="pull-right normal-range">{{ $phoneTrial->user->company_phone }}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->work['company_name']) || (isset($phoneTrial->work['company_name']) && $phoneTrial->work['company_name']==1))
                                <tr>
                                    <td>@lang('message.phone.company.name') <span class="pull-right normal-range">{{ $phoneTrial->user->company_name }}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->work['career']) || (isset($phoneTrial->work['career']) &&$phoneTrial->work['career']==1))
                                <tr>
                                    <td>@lang('message.phone.company.career') <span class="pull-right normal-range">{{$phoneTrial->user->career}}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->work['income_level']) || (isset($phoneTrial->work['income_level']) &&$phoneTrial->work['income_level']==1))
                                <tr>
                                    <td>@lang('message.phone.company.income') <span class="pull-right normal-range">{{$phoneTrial->user->income_level}}
                                        @if(isset($phoneTrial->work['levelRemark']) && $phoneTrial->work['levelRemark'])
                                            @if($phoneTrial->work['levelRemark'] && strpos($phoneTrial->work['levelRemark'], 'real')!==false)
                                                [{{\App\Consts\Level::toString(ltrim($phoneTrial->work['levelRemark'], 'real'))}}]
                                            @else
                                            [{{$phoneTrial->work['levelRemark']}}]
                                            @endif
                                        @endif
                                    </span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->work['work_period']) || (isset($phoneTrial->work['work_period']) && $phoneTrial->work['work_period']==1))
                                <tr>
                                    <td>@lang('message.phone.company.period') <span class="pull-right normal-range">{{$phoneTrial->user->work_period}}
                                        @if(isset($phoneTrial->work['periodRemark']) && $phoneTrial->work['periodRemark'])
                                            [{{$phoneTrial->work['periodRemark']}}]
                                        @endif
                                    </span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->work['company_address']) || (isset($phoneTrial->work['company_address']) && $phoneTrial->work['company_address']==1))
                                <tr>
                                    <td>@lang('message.phone.company.work.address')
                                        <span class="pull-right normal-range">
                                            {{$phoneTrial->user->company_province}}
                                            {{$phoneTrial->user->company_rengence}}
                                            {{$phoneTrial->user->company_district}}
                                            {{$phoneTrial->user->company_villages}}
                                            {{$phoneTrial->user->company_address}}
                                        </span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->work['paid_period']) || (isset($phoneTrial->work['paid_period']) && $phoneTrial->work['paid_period']==1))
                                <tr>
                                    <td>@lang('message.phone.company.work.pay.period') <span class="pull-right normal-range">{{$phoneTrial->user->paid_period}}
                                        @if(isset($phoneTrial->work['payRemark']) && $phoneTrial->work['payRemark'])
                                            [{{$phoneTrial->work['payRemark']}}]
                                        @endif
                                    </span></td>
                                </tr>
                                @endif
                                @if(isset($phoneTrial->work['taker']))
                                <tr>
                                    <td>@lang('message.take.phone')
                                        <span class="pull-right normal-range">
                                            @if($phoneTrial->work['taker']==10)
                                                @lang('message.hr')
                                            @elseif($phoneTrial->work['taker']==11)
                                                @lang('message.finance')
                                            @elseif($phoneTrial->work['taker']==12)
                                                @lang('message.companyee')
                                            @elseif($phoneTrial->work['taker']==13)
                                                @lang('message.other')
                                            @else
                                                @lang('final.no.choice')
                                            @endif
                                        </span>

                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <table class="custom-table-trial custom-table-info">
                            <tbody>
                                <tr>
                                    <th>@lang('message.diffence')</th>
                                </tr>
{{--                                @if(isset($phoneTrial->work['company_phone']) && $phoneTrial->work['company_phone']==3)--}}
                                @if(isset($phoneTrial->work['company_phone']) )
                                    <tr>
                                        <td>@lang('message.phone.company.phone') <span class="pull-right fail-range">{{ $phoneTrial->user->company_phone }}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->work['company_name']) && $phoneTrial->work['company_name']==2)
                                    <tr>
                                        <td>@lang('message.phone.company.name') <span class="pull-right fail-range">{{ $phoneTrial->user->company_name }}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->work['career']) && $phoneTrial->work['career']==2)
                                    <tr>
                                        <td>@lang('message.phone.company.career') <span class="pull-right fail-range">{{$phoneTrial->user->career}}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->work['income_level']) && $phoneTrial->work['income_level']==2)
                                    <tr>
                                        <td>@lang('message.phone.company.income') <span class="pull-right fail-range">{{$phoneTrial->user->income_level}}
                                                @if(isset($phoneTrial->work['levelRemark']) && $phoneTrial->work['levelRemark'])
                                                    @if($phoneTrial->work['levelRemark'] && strpos($phoneTrial->work['levelRemark'], 'real')!==false)
                                                        [{{\App\Consts\Level::toString(ltrim($phoneTrial->work['levelRemark'], 'real'))}}]
                                                    @else
                                                        [{{$phoneTrial->work['levelRemark']}}]
                                                    @endif
                                                @endif
                                        </span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->work['income_level']) && $phoneTrial->work['income_level']==3)
                                    <tr>
                                        <td>@lang('message.phone.company.income') <span class="pull-right error-range">{{$phoneTrial->user->income_level}}
                                                @if(isset($phoneTrial->work['levelRemark']) && $phoneTrial->work['levelRemark'])
                                                    @if($phoneTrial->work['levelRemark'] && strpos($phoneTrial->work['levelRemark'], 'real')!==false)
                                                        [{{\App\Consts\Level::toString(ltrim($phoneTrial->work['levelRemark'], 'real'))}}]
                                                    @else
                                                        [{{$phoneTrial->work['levelRemark']}}]
                                                    @endif
                                                @endif
                                        </span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->work['work_period']) && $phoneTrial->work['work_period']==2)
                                    <tr>
                                        <td>@lang('message.phone.company.period') <span class="pull-right fail-range">{{$phoneTrial->user->work_period}}
                                                @if(isset($phoneTrial->work['periodRemark']) && $phoneTrial->work['periodRemark'])
                                                    [{{$phoneTrial->work['periodRemark']}}]
                                                @endif
                                        </span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->work['work_period']) && $phoneTrial->work['work_period']==3)
                                    <tr>
                                        <td>@lang('message.phone.company.period') <span class="pull-right error-range">{{$phoneTrial->user->work_period}}
                                                @if(isset($phoneTrial->work['periodRemark']) && $phoneTrial->work['periodRemark'])
                                                    [{{$phoneTrial->work['periodRemark']}}]
                                                @endif
                                        </span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->work['company_address']) && $phoneTrial->work['company_address']==2)
                                    <tr>
                                        <td>@lang('message.phone.company.work.address')
                                            <span class="pull-right fail-range">
                                            {{$phoneTrial->user->company_province}}
                                                {{$phoneTrial->user->company_rengence}}
                                                {{$phoneTrial->user->company_district}}
                                                {{$phoneTrial->user->company_villages}}
                                                {{$phoneTrial->user->company_address}}
                                        </span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->work['paid_period']) && $phoneTrial->work['paid_period']==2)
                                    <tr>
                                        <td>@lang('message.phone.company.work.pay.period') <span class="pull-right fail-range">{{$phoneTrial->user->paid_period}}
                                                @if(isset($phoneTrial->work['payRemark']) && $phoneTrial->work['payRemark'])
                                                    [{{$phoneTrial->work['payRemark']}}]
                                                @endif
                                        </span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->work['paid_period']) && $phoneTrial->work['paid_period']==3)
                                    <tr>
                                        <td>@lang('message.phone.company.work.pay.period') <span class="pull-right error-range">{{$phoneTrial->user->paid_period}}
                                                @if(isset($phoneTrial->work['payRemark']) && $phoneTrial->work['payRemark'])
                                                    [{{$phoneTrial->work['payRemark']}}]
                                                @endif
                                        </span></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>


                    <div style="clear: both;"></div>
                    <div class="custom-section-title-square bottom-line margin-top">
                        @lang('final.person.retrial')
                        @if(empty($phoneTrial->peason['phone_number']))(@lang('final.oldClientNotrial'))@endif
                        @if($phoneTrial->peason['phone_number']==4)(@lang('message.personal.modify.holiday'))@endif

                        {{--个人电核备注start--}}
                        @foreach(['nervous'=>'message.phone.ask.applyer.nervous', 'teach'=>'message.phone.ask.other.teach', 'other'=>'message.phone.ask.other.take', 'fluent'=>'message.phone.ask.other.fluent'] as $k=>$v)
                            @if(isset($phoneTrial->peason['work_remark'][$k]) && $phoneTrial->peason['work_remark'][$k]==1)
                                <span class="error-range">@lang($v)</span>
                            @endif
                        @endforeach
                        {{--个人电核备注end--}}
                    </div>
                    <div class="col-lg-6" style="padding-right: 0px;">
                        {{--<table class="custom-table-trial custom-table-info @if(empty($phoneTrial->peason['phone_number']) || (isset($phoneTrial->peason['phone_number']) && !in_array($phoneTrial->peason['phone_number'], [1, 2, 3]))) empty-data @endif">--}}
                        <table class="custom-table-trial custom-table-info ">
                            <tbody>
                                <tr>
                                    <th>
                                        @lang('final.same')
                                    </th>
                                </tr>
{{--                                @if(empty($phoneTrial->peason['phone_number']) || (isset($phoneTrial->peason['phone_number']) && $phoneTrial->peason['phone_number']!=3))--}}
                                @if(empty($phoneTrial->peason['phone_number']) || (isset($phoneTrial->peason['phone_number'])))
                                <tr>
                                    <td>@lang('message.phone.ask.personal.phone') <span class="pull-right normal-range">{{ $phoneTrial->user->phone_number }}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['name']) || (isset($phoneTrial->peason['name']) && $phoneTrial->peason['name']==1))
                                <tr>
                                    <td>@lang('message.client.name') <span class="pull-right normal-range">{{ $phoneTrial->user->name }}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['ktp_number']) || (isset($phoneTrial->peason['ktp_number']) && $phoneTrial->peason['ktp_number']==1))
                                <tr>
                                    <td>@lang('message.ktp.number') <span class="pull-right normal-range">{{ $phoneTrial->user->ktp_number }}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['marriage']) || (isset($phoneTrial->peason['marriage']) && $phoneTrial->peason['marriage']==1))
                                <tr>
                                    <td>@lang('message.phone.ask.personal.marriage') <span class="pull-right normal-range">{{ $phoneTrial->user->marriage }}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['pay_type']) || (isset($phoneTrial->peason['pay_type']) && $phoneTrial->peason['pay_type']==1))
                                <tr>
                                    <td>@lang('message.phone.ask.personal.pay.type') <span class="pull-right normal-range">{{$order->pay_type}}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['address']) || (isset($phoneTrial->peason['address']) && $phoneTrial->peason['address']==1))
                                <tr>
                                    <td>@lang('message.phone.home.address')
                                        <span class="pull-right normal-range">
                                            {{$phoneTrial->user->province}}
                                            {{$phoneTrial->user->rengence}}
                                            {{$phoneTrial->user->district}}
                                            {{$phoneTrial->user->villages}}
                                            {{$phoneTrial->user->address}}
                                        </span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['relative_first']) || (isset($phoneTrial->peason['relative_first']) && $phoneTrial->peason['relative_first']==1))
                                <tr>
                                    <td>{{$phoneTrial->user->relative_type_first}} <span class="pull-right normal-range">{{$phoneTrial->user->relative_name_first}}: {{$phoneTrial->user->relative_phone_first}}</span></td>
                                </tr>
                                @endif
                                @if(empty($phoneTrial->peason['relative_second']) || (isset($phoneTrial->peason['relative_second']) && $phoneTrial->peason['relative_second']==1))
                                <tr>
                                    <td>{{$phoneTrial->user->relative_type_second}} <span class="pull-right normal-range">{{$phoneTrial->user->relative_name_second}}: {{$phoneTrial->user->relative_phone_second}}</span></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <table class="custom-table-trial custom-table-info">
                            <tbody>
                                <tr>
                                    <th>@lang('message.diffence')</th>
                                </tr>
{{--                                @if(isset($phoneTrial->peason['name']) && $phoneTrial->peason['phone_number']==3)--}}
                                @if(isset($phoneTrial->peason['name']))
                                    <tr>
                                        <td>@lang('message.phone.ask.personal.phone') <span class="pull-right fail-range">{{ $phoneTrial->user->phone_number }}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['name']) && $phoneTrial->peason['name']==2)
                                    <tr>
                                        <td>@lang('message.client.name') <span class="pull-right fail-range">{{ $phoneTrial->user->name }}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['ktp_number']) && $phoneTrial->peason['ktp_number']==2)
                                    <tr>
                                        <td>@lang('message.ktp.number') <span class="pull-right fail-range">{{ $phoneTrial->user->ktp_number }}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['marriage']) && $phoneTrial->peason['marriage']==2)
                                    <tr>
                                        <td>@lang('message.phone.ask.personal.marriage') <span class="pull-right fail-range">{{ $phoneTrial->user->marriage }}</span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->peason['marriage']) && $phoneTrial->peason['marriage']==3)
                                    <tr>
                                        <td>@lang('message.phone.ask.personal.marriage') <span class="pull-right error-range">{{ $phoneTrial->user->marriage }}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['pay_type']) && $phoneTrial->peason['pay_type']==2)
                                    <tr>
                                        <td>@lang('message.phone.ask.personal.pay.type') <span class="pull-right fail-range">{{$order->pay_type}}</span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->peason['pay_type']) && $phoneTrial->peason['pay_type']==3)
                                    <tr>
                                        <td>@lang('message.phone.ask.personal.pay.type') <span class="pull-right error-range">{{$order->pay_type}}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['address']) && $phoneTrial->peason['address']==2)
                                    <tr>
                                        <td>@lang('message.phone.home.address')
                                            <span class="pull-right fail-range">
                                            {{$phoneTrial->user->province}}
                                                {{$phoneTrial->user->rengence}}
                                                {{$phoneTrial->user->district}}
                                                {{$phoneTrial->user->villages}}
                                                {{$phoneTrial->user->address}}
                                        </span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->peason['address']) && $phoneTrial->peason['address']==3)
                                    <tr>
                                        <td>@lang('message.phone.home.address')
                                            <span class="pull-right error-range">
                                        {{$phoneTrial->user->province}}
                                                {{$phoneTrial->user->rengence}}
                                                {{$phoneTrial->user->district}}
                                                {{$phoneTrial->user->villages}}
                                                {{$phoneTrial->user->address}}
                                    </span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['relative_first']) && $phoneTrial->peason['relative_first']==2)
                                    <tr>
                                        <td>{{$phoneTrial->user->relative_type_first}} <span class="pull-right fail-range">{{$phoneTrial->user->relative_name_first}}: {{$phoneTrial->user->relative_phone_first}}</span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->peason['relative_first']) && $phoneTrial->peason['relative_first']==3)
                                    <tr>
                                        <td>{{$phoneTrial->user->relative_type_first}} <span class="pull-right error-range">{{$phoneTrial->user->relative_name_first}}: {{$phoneTrial->user->relative_phone_first}}</span></td>
                                    </tr>
                                @endif
                                @if(isset($phoneTrial->peason['relative_second']) && $phoneTrial->peason['relative_second']==2)
                                    <tr>
                                        <td>{{$phoneTrial->user->relative_type_second}} <span class="pull-right fail-range">{{$phoneTrial->user->relative_name_second}}: {{$phoneTrial->user->relative_phone_second}}</span></td>
                                    </tr>
                                @elseif(isset($phoneTrial->peason['relative_second']) && $phoneTrial->peason['relative_second']==3)
                                    <tr>
                                        <td>{{$phoneTrial->user->relative_type_second}} <span class="pull-right error-range">{{$phoneTrial->user->relative_name_second}}: {{$phoneTrial->user->relative_phone_second}}</span></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



            {{--确认用户收入水平--}}
            <div style="clear: both;"></div>
            <div class="custom-section-title-square bottom-line margin-top ">
                <span>@lang('final.recon.level'):</span>
                        <span class="col-md-offset-2">
                            <select id="level">
                            <option id="info"></option>
                            @foreach(App\Consts\Level::getList() as $k=>$v)
                             <option value="{{$k}}">{{$v}}</option>
                            @endforeach
                            </select>
                        </span>
                </table>
            </div>
            {{--确认用户收入水平--}}

            <div class="col-lg-12 margin-top">
                @if ($isOwner)
                    <div class="trail-textarea">
                        <textarea name="remark" id="remark" class="" cols="30" rows="10"></textarea>
                        <span class="fail-range" id="final-trial-remark" style="display: none;"></span>
                        <input type="hidden" id="order_id" name="order_id" value="{{ $order_id }}">
                    </div>

                    <div class="trail-btn">
                        <button class="btn btn-success btn-lg trial-pass-btn" type="button">@lang('final.final.trial.pass')</button>
                        <button class="btn btn-danger btn-lg trial-refuse-btn" type="button">@lang('final.final.trial.fail')</button>
                    </div>
                @endif

                <div class="trial-btn-end"></div>
            </div>
            <div style="clear: both;"></div>
        </section>
    </div>
    <div style="clear: both;"></div>
</div>