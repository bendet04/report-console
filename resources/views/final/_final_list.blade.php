<section class="panel">
    <div class="panel-body">
        <div class="col-md-3 col-sm-6 diy-search-input-box">
            <select class="form-control diy-search-input" id="searchId">
                <option value="">select</option>
                <option value="order_number">{{__('final.number')}}</option>
                <option value="name">{{__('final.clientName')}}</option>
                <option value="ktp_number">{{__('final.ktp')}}</option>
                <option value="phone_number">{{__('final.phone')}}</option>
                <option value="loan_period">{{__('final.payPeriod')}}</option>
                <option value="risk_score">{{__('final.score')}}</option>
                <option value="apply_amount">{{__('final.applyAmount')}}</option>
                <option value="created_at">{{__('final.applyTime')}}</option>
                <option value="order_status">{{__('final.status')}}</option>
            </select>
        </div>
        <span class="tools pull-right">
            @if(Auth::user()->work_status==3)
                <a href="/switchStatus?workStatus=0" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px;margin: 8px;">@lang('message.stopWork')</a>
            @else
                <a href="/switchStatus?workStatus=3" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px; margin: 8px;">@lang('message.startWork')</a>
            @endif
        </span>
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf"  id="dynamic-table">
                <thead class="cf">
                <tr>
                    <th>{{__('final.order')}}</th>
                    <th>{{__('final.number')}}</th>
                    <th>{{__('message.platform')}}</th>
                    <th class="numeric">{{__('final.clientName')}}</th>
                    <th class="numeric">{{__('final.ktp')}}</th>
                    <th class="numeric">{{__('final.phone')}}</th>
                    <th class="numeric">{{__('final.payPeriod')}}</th>
                    <th class="numeric">{{__('final.applyAmount')}}</th>
                    <th class="numeric">{{__('final.score')}}</th>
                    <th class="numeric">{{__('final.applyTime')}}</th>
                    <th class="numeric">{{__('final.status')}}</th>
                    <th class="numeric">{{__('final.operation')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $k=>$avalue)
                <tr>
                    <td>{{ $k+1 }}</td>
                    <td>{{ $avalue->order_number }}</td>
                    <td>{{ \App\Consts\Platform::alias($avalue->platform) }}</td>
                    <td class="numeric">{{ $avalue->name }}</td>
                    <td class="numeric">{{ $avalue->ktp_number }}</td>
                    <td class="numeric">{{ $avalue->phone_number }}</td>
                    <td class="numeric">{{ $avalue->loan_period }}</td>
                    <td class="numeric">{{ $avalue->apply_amount }}</td>
                    <td class="numeric">{{ empty($avalue->risk_score) ? 0 : $avalue->risk_score }}</td>
                    <td class="numeric">{{ $avalue->created_at }}</td>
                    <td class="numeric">
                        {{ $avalue->order_status_str }}
                    </td>
                    <td class="numeric">
                        <a href="/final/trialFinalDetail?order_id={{ $avalue->id }}&back_url={{$back_url}}">@if($avalue->order_status==21)@lang('message.trial')@else@lang('basic.detail')@endif</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @if($orders->count())
            <div class="diy-paginate">
                <form action="/final/trialFinal" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">{{ $orders->appends($pageCondition)->render() }}</div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
            @endif

            <div id="empty_data" @if($orders->count())style="display: none;"@endif>@lang('message.list_not_data_notice') <a href="/refresh?worktype=3" class="btn btn-success">@lang('message.refresh')</a></div>
        </section>
    </div>
</section>
