<!--通话记录-->
@if($data['result_code']==10000&&!empty($data['data']))
    <div class="custom-section-title-square" >@lang('final.phone.record.trial')
        <span class="red">({{$data['data']['total']}})</span>
        <img src="/images/small-triangle.png.png" alt="">
    </div>
    <div class="panel-body panel-body-no-padding">
        <section class="panel section-panel-no-margin">
            <header class="panel-heading custom-tab custom-tab-20">
                <ul class="nav nav-tabs">
                    {{--<li class="active">--}}
                        {{--<a href="#phone1" data-toggle="tab" aria-expanded="true">总记录条数<br />@if(!empty($data['data']['total']))({{$data['data']['total']}}) @else (0) @endif</a>--}}
                    {{--</li>--}}
                    <li class="active" >
                        <a href="#phone2" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.record')<br />@if(!empty($data['data']['addressbook'])&&count($data['data']['addressbook'])!=0){{count($data['data']['addressbook'])}} @else 0 @endif</a>
                    </li>
                    <li class="" >
                        <a href="#phone3" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.relation')<br />@if(!empty($data['data']['contact'])&&count($data['data']['contact'])!=0){{count($data['data']['contact'])}} @else 0 @endif</a>
                    </li>
                    <li class="" >
                        <a href="#phone4" data-toggle="tab" aria-expanded="false">@lang('final.white.list')<br />
                            @if(!empty($data['data']['whitelist'])&&count($data['data']['whitelist'])!=0){{count($data['data']['whitelist'])}}
                            @else 0 @endif
                        </a>
                    </li>
                    <li class="" >
                        <a href="#phone5" data-toggle="tab" aria-expanded="false">@lang('final.black.list')<br />@if(!empty($data['data']['blacklist'])&&count($data['data']['blacklist'])!=0){{count($data['data']['blacklist'])}} @else 0 @endif</a>
                    </li>

                </ul>
            </header>
            <div style="clear:both;"></div>
            <div class="panel-body custom-panel-body1">
                <div class="tab-content">
                    {{--<div class="tab-pane custom-tab-pane custom-tab-pane-phone active" id="phone1">--}}

                    {{--</div>--}}
                    <div class="tab-pane custom-tab-pane  custom-tab-pane-phone active" id="phone2">

                        @if(!empty($data['data']['addressbook']))
                            @foreach($data['data']['addressbook'] as $k=>$v)
                                @if(!empty($v['number'])&&isset($v['duration']))
                                <div class="col-lg-10">{{$v['number']}}</div>
                                <div class="col-lg-2 tab-pane-content">{{gmstrftime('%H:%M:%S',$v['duration'])}}</div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="tab-pane custom-tab-pane  custom-tab-pane-phone" id="phone3">

                        @if(!empty($data['data']['contact']))
                            @foreach($data['data']['contact'] as $k=>$v)
                                @if(!empty($v['number'])&&isset($v['duration']))
                                    <div class="col-lg-10">{{$v['number']}}</div>
                                    <div class="col-lg-2 tab-pane-content">{{gmstrftime('%H:%M:%S',$v['duration'])}}</div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="tab-pane custom-tab-pane  custom-tab-pane-phone" id="phone4">

                        @if(!empty($data['data']['whitelist']))
                            @foreach($data['data']['whitelist'] as $k=>$v)
                                @if(!empty($v['number'])&&isset($v['duration']))
                                    <div class="col-lg-10">{{$v['number']}}</div>
                                    <div class="col-lg-2 tab-pane-content">{{gmstrftime('%H:%M:%S',$v['duration'])}}</div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="tab-pane custom-tab-pane  custom-tab-pane-phone" id="phone5">

                        @if(!empty($data['data']['blacklist']))
                            @foreach($data['data']['blacklist'] as $k=>$v)
                                @if(!empty($v['number'])&&isset($v['duration']))
                                    <div class="col-lg-10">{{$v['number']}}</div>
                                    <div class="col-lg-2 tab-pane-content">{{gmstrftime('%H:%M:%S',$v['duration'])}}</div>
                                @endif
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </section>
    </div>
@endif
<!--通话记录审核end-->