<div class="page-heading">
    <div class="col-lg-12">
        <section class="custom-panel">
            <header class="custom-heading custom-heading-big">@lang('final.device.data.trial')</header>
            <div class="device_risk">
                @if(isset($device->phone_message) && !empty($device->phone_message))
                <div class="custom-section-title-square">@lang('final.message.trial') <span class="red">({{count($device->phone_message)}})</span>
                    <img src="/images/small-triangle.png.png" alt=""></div>
                <div class="panel-body panel-body-no-padding">
                    <section class="panel section-panel-no-margin">
                        <header class="panel-heading custom-tab">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#home" data-toggle="tab" aria-expanded="true">@lang('final.message')<br />({{count($device->phone_message)}})</a>
                                </li>

                                <li class="" style="display: none;">
                                    <a href="#about" data-toggle="tab" aria-expanded="false">@lang('final.black.list')<br />(50)</a>
                                </li>

                            </ul>
                        </header>
                        <div style="clear:both;"></div>
                        <div class="panel-body custom-panel-body1">
                            <div class="tab-content">
                                <div class="tab-pane custom-tab-pane active" id="home">
                                    @foreach($device->phone_message as $v)
                                        @if(!empty($v['mobile']))
                                        <div class="col-lg-3">{{$v['mobile']}}</div>
                                        @endif
                                        @if(!empty($v['date']))
                                        <div class="col-lg-9">{{date('Y-m-d H:i:s', $v['date']/1000)}}</div>
                                        @endif
                                        @if(!empty($v['body']))
                                        <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane custom-tab-pane" id="about" style="display:none;">
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-9">2018-05-08 12:34:43</div>
                                    <div class="col-lg-12 tab-pane-content">Hi~blablabla</div>

                                </div>

                            </div>
                        </div>
                    </section>
                </div>
                @endif
           </div>
            {{--<!--短信审核end-->--}}

            <div class="call">
                @if(isset($device->phone_record) && !empty($device->phone_record))
                <div class="custom-section-title-square" >@lang('final.phone.record.trial') <span class="red">({{count($device->phone_record)}})</span>
                    <img src="/images/small-triangle.png.png" alt=""></div>
                <div class="panel-body panel-body-no-padding">
                    <section class="panel section-panel-no-margin">
                        <header class="panel-heading custom-tab custom-tab-20">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#phone1" data-toggle="tab" aria-expanded="true">@lang('final.total.phone.record')<br />({{count($device->phone_record)}})</a>
                                </li>
                                <li class="" style="display: none;">
                                    <a href="#phone2" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.record')<br />(5)</a>
                                </li>

                            </ul>
                        </header>
                        <div style="clear:both;"></div>
                        <div class="panel-body custom-panel-body1">
                            <div class="tab-content">
                                <div class="tab-pane custom-tab-pane custom-tab-pane-phone active" id="phone1">
                                    @foreach($device->phone_record as $k=>$v)
                                    @if(!empty($v['number'])&&isset($v['duration']))
                                    <div class="col-lg-10">{{$v['number']}}</div>
                                    <div class="col-lg-2 tab-pane-content">{{gmstrftime('%H:%M:%S',$v['duration'])}}</div>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane custom-tab-pane  custom-tab-pane-phone" id="phone2" style="display: none">
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                    <div class="col-lg-10">+622121478342</div>
                                    <div class="col-lg-2 tab-pane-content">13</div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
                @endif
            </div>
            <!--通话记录审核end-->

            <div class="ab">
                @if(isset($device->phone_contact) && !empty($device->phone_contact))
                <div class="custom-section-title-square">@lang('final.record.trial') <span class="red">({{count($device->phone_contact)}})</span>
                    <img src="/images/small-triangle.png.png" alt=""></div>

                <div class="panel-body  panel-body-no-padding">
                    <section class="panel section-panel-no-margin">
                        <header class="panel-heading custom-tab custom-tab-20">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#contact1" data-toggle="tab" aria-expanded="true">@lang('final.total.record.sum')<br />({{count($device->phone_contact)}})</a>
                                </li>
                                <li class="" style="display: none;">
                                    <a href="#contact2" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.record')<br />(5)</a>
                                </li>

                            </ul>
                        </header>
                        <div style="clear:both;"></div>
                        <div class="panel-body custom-panel-body1">
                            <div class="tab-content">
                                <div class="tab-pane custom-tab-pane active" id="contact1">
                                    @foreach($device->phone_contact as $k=>$v)
                                        @if(!empty($v['name']))
                                        <div class="col-lg-3">{{$v['name']}}</div>
                                        @endif
                                        @if(!empty($v['mobile']))
                                        <div class="col-lg-12 tab-pane-content">{{$v['mobile']}}</div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane custom-tab-pane" id="contact2" style="display: none;">
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-12 tab-pane-content">+622121478342</div>
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-12 tab-pane-content">+622121478342</div>
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-12 tab-pane-content">+622121478342</div>
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-12 tab-pane-content">+622121478342</div>
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-12 tab-pane-content">+622121478342</div>
                                    <div class="col-lg-3">Tokiovips</div>
                                    <div class="col-lg-12 tab-pane-content">+622121478342</div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
                @endif
            </div>

            <!--通讯录审核end-->
            <div class="app">
                @if(isset($device->app_list) && !empty($device->app_list))
                <div class="custom-section-title-square">APP @lang('message.trial') <span class="red">({{count($device->app_list)}})</span>
                    <img src="/images/small-triangle.png.png" alt=""></div>
                <div class="panel-body  panel-body-no-padding">
                    <section class="panel section-panel-no-margin">
                        <header class="panel-heading custom-tab custom-tab-33">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#app1" data-toggle="tab" aria-expanded="true">@lang('final.total.download.sum')<br />({{count($device->app_list)}})</a>
                                </li>
                                <li class="" style="display: none;">
                                    <a href="#app2" data-toggle="tab" aria-expanded="false">@lang('final.relative.to.loan')<br />(5)</a>
                                </li>
                                <li class="" style="display: none;">
                                    <a href="#app3" data-toggle="tab" aria-expanded="false">@lang('final.other')<br />(5)</a>
                                </li>
                            </ul>
                        </header>
                        <div style="clear:both;"></div>
                        <div class="panel-body custom-panel-body1">
                            <div class="tab-content">
                                <div class="tab-pane custom-tab-pane active" id="app1">
                                    @foreach($device->app_list as $k=>$v)
                                        @if(!empty($v['appName']))
                                        <div class="app-content" title="{{$v['appName']}}">{{$v['appName']}}</div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane custom-tab-pane" id="app2" style="display: none;">
                                    <div class="app-content">TangBull</div>
                                    <div class="app-content">WhatsApp</div>
                                    <div class="app-content">Line</div>
                                    <div class="app-content">Abang Bintang</div>
                                    <div class="app-content">TangBull</div>
                                    <div class="app-content">WhatsApp</div>
                                    <div class="app-content">Line</div>
                                    <div class="app-content">Abang Bintang</div>
                                </div>


                            </div>
                        </div>
                    </section>
                </div>
                @endif
            </div>

                <!--APP审核end-->


            {{--<div class="custom-section-title-square">@lang('final.map.trial')</div>--}}
            {{--<div class="panel-body">--}}
                {{--<section class="panel map-section">--}}
                    {{--<div class="col-lg-3">@lang('final.map.location')</div>--}}
                    {{--<div class="col-lg-1"></div>--}}
                    {{--<div class="col-lg-6">Jawa BaratKota BekaiBeksi</div>--}}
                    {{--<div class="col-lg-2"></div>--}}

                    {{--<div class="col-lg-3">@lang('final.map.location')</div>--}}
                    {{--<div class="col-lg-1">@lang('final.distance')</div>--}}
                    {{--<div class="col-lg-6">@lang('final.work.address')（Jawa BaratKota BekaiBeksi）</div>--}}
                    {{--<div class="col-lg-2 normal-range">7KM</div>--}}

                    {{--<div class="col-lg-3">@lang('final.map.location')</div>--}}
                    {{--<div class="col-lg-1">@lang('final.distance')</div>--}}
                    {{--<div class="col-lg-6">@lang('final.home.address')（Jawa BaratKota BekaiBeksi）</div>--}}
                    {{--<div class="col-lg-2 error-range">14KM</div>--}}
                {{--</section>--}}
            {{--</div>--}}
            <!--地图审核end-->




        </section>
    </div>
    <div style="clear: both" id="info-phone-trial"></div>
</div>