<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:185px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="search-header-section">
    <form id="default" class="form-horizontal" action="/final/trialFinal" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset title="Initial Info">
            {{--平台--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">{{__('message.platform')}}</td>
                        <td>
                            <select class="form-control" name="platform">
                                <option value="">@lang('basic.all.platform')</option>
                                @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            {{--客户名称--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('final.clientName')}}</td>
                        <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--编号--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">{{__('final.number')}}</td>
                        <td><input type="text" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <br/>
            {{--KTP--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('final.ktp')}}</td>
                        <td><input type="text" placeholder="Ktp number" name="ktp_number" @if(!empty($pageCondition['ktp_number'])) value="{{$pageCondition['ktp_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--新老用户--}}

            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >user</td>
                        <td>
                            <select class="form-control" name="userType">
                                <option></option>
                                <option value="1" @if(!empty($pageCondition['userType'])&&$pageCondition['userType']==1) selected="selected" @endif >@lang('final.new')</option>
                                <option value="2" @if(!empty($pageCondition['userType'])&&$pageCondition['userType']==2) selected="selected" @endif>@lang('final.old')</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            {{--发薪日--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('message.phone.company.work.pay.period')}}</td>
                        <td>
                            <select class="form-control" name="paidperiod">
                                <option></option>
                                <option value="1" @if(!empty($pageCondition['paidperiod'])&&$pageCondition['paidperiod']==1) selected="selected" @endif >0{{__('message.day')}}</option>
                                <option value="2" @if(!empty($pageCondition['paidperiod'])&&$pageCondition['paidperiod']==2) selected="selected" @endif>1-3{{__('message.day')}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        <br/>
            {{--订单状态--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('final.status')}}</td>
                         <td>
                    <select class="form-control" name="order_status">
                        <option></option>
                        <option value="21" selected>@lang('final.wait.trial')</option>
                        <option value="6" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='6') selected="selected" @endif>@lang('final.final.success')</option>
                        <option value="5" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='5') selected="selected" @endif>@lang('final.final.failure')</option>
                        <option value="23" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='23') selected="selected" @endif>@lang('final.user.sure')</option>
                    </select>
                         </td>
                    </tr>
                </table>

            </div>
            {{--电话号码--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >{{__('final.phone')}}</td>
                        <td><input type="text" placeholder="phone number" name="phone_number" @if(!empty($pageCondition['phone_number'])) value="{{$pageCondition['phone_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
        </fieldset>
        <div class="col-md-9" style="margin-bottom: 10px;margin-left: 185px">
            <button type="submit" class="btn btn-info ">{{__('final.select')}}</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        <div style="clear: both"> </div>
    </form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>


