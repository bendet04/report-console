<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($partner))
                <tr class="">
                    @if(!empty($partner['my_account']['phone_number']))
                    <td>
                        Phone
                    </td>
                    <td>
                        {{ltrim($partner['my_account']['phone_number'],'+620')}}
                        {{--<span style="margin-left: 20px;color:red;">--}}
                            {{--@if(ltrim($partner['my_account']['phone_number'],'+620') == $compareInfo['phone_number'])--}}
                            {{--一致--}}
                            {{--@else--}}
                            {{--不一致--}}
                            {{--@endif--}}
                        {{--</span>--}}
                    </td>
                    @endif
                </tr>
                <tr class="">
                    <td>
                        Name
                    </td>
                    <td>
                        {{$partner['my_account']['name']}}
                    </td>
                </tr>
                <tr class="">
                    <td>
                        Email
                    </td>
                    <td>
                        {{$partner['my_account']['email']}}
                    </td>
                </tr>
                <tr class="">
                    <td>
                        @lang('final.score')
                    </td>
                    <td>
                        {{$partner['my_account']['go_points']}}
                    </td>
                </tr>
            <tr class="">
                <td>
                    @lang('final.money.other')
                </td>
                <td>
                    {{$partner['my_account']['go_pay']}}
                </td>
            </tr>
                <!--<tr class="">-->
                    <!--<td>-->
                        <!--historyOrder-->
                    <!--</td>-->
                    <!--<td>-->
                        <!--<a style="cursor: pointer;" data-toggle="modal"  data-target="#gojekOrder">查看</a>-->
                    <!--</td>-->
                <!--</tr>-->
                @endif
            </tbody>
        </table>
            <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                <thead>
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('final.begin')</th>
                    <th>@lang('final.end')</th>
                    <th>@lang('final.end')</th>
                    <th>@lang('final.card.type')</th>
                    <th>@lang('final.car.card')</th>
                    <th>@lang('final.driver')</th>
                    <th>@lang('final.fatal')</th>
                    <th>@lang('final.distance')</th>
                    <th>@lang('final.login.time')</th>
                    <th>@lang('message.status')</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($partner['history_order']))
                @foreach($partner['history_order'] as  $key => $value)
                <tr class="">
                    <td>{{$key+1}}</td>
                    <td>{{$value['from']}}</td>
                    <td>{{$value['to']}}</td>
                    <td>{{$value['order_no']}}</td>
                    <td>{{$value['car_model']}}</td>
                    <td>{{$value['car_no']}}</td>
                    <td>{{$value['car_driver']}}</td>
                    <td>{{$value['order_fee']}}</td>
                    <td>{{$value['distance']}}</td>
                    <td>{{$value['datetime']}}</td>
                    <td>{{$value['status']}}</td>
                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">关闭</button>
    </div>
</form>

<!--gojek订单查看-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="gojekOrder" class="modal fade">
    <div class="modal-dialog" style="width:1000px;">
        <div class="modal-content">

        </div>
    </div>
</div>