<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Content</th>
                </tr>
            </thead>
            <tbody>
            @if(!empty($partner))
                @if(!empty($partner['profile']['npwp']))
                <tr class="">
                    <td>
                        NPWP Number：
                    </td>
                    <td>
                        {{ $partner['profile']['npwp'] }}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['real_name']))
                <tr class="">
                    <td>
                        @lang('basic.name'):
                    </td>
                    <td>
                        {{$partner['profile']['real_name']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['phone']))
                <tr class="">
                    <td>
                        Phone:
                    </td>
                    <td>
                        {{$partner['profile']['phone']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['address']))
                <tr class="">
                    <td>
                        @lang('basic.address'):
                    </td>
                    <td>
                        {{$partner['profile']['address']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['email']))
                <tr class="">
                    <td>
                        email:
                    </td>
                    <td>
                        {{$partner['profile']['email']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['djponline_email']))
                <tr class="">
                    <td>
                        @lang('final.inline.email'):
                    </td>
                    <td>
                        {{$partner['profile']['djponline_email']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['job']))
                <tr class="">
                    <td>
                        @lang('basic.work.address'):
                    </td>
                    <td>
                        {{$partner['profile']['job']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['klu']))
                <tr class="">
                    <td>
                        @lang('final.career.type'):
                    </td>
                    <td>
                        {{$partner['profile']['klu']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['pkp']))
                <tr class="">
                    <td>
                        @lang('final.employee'):
                    </td>
                    <td>
                        {{$partner['profile']['pkp']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['kpp']))
                <tr class="">
                    <td>
                        @lang('final.should.tax'):
                    </td>
                    <td>
                        {{$partner['profile']['kpp']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['profile']['kpp_telephone']))
                <tr class="">
                    <td>
                        @lang('final.should.tax.phone'):
                    </td>
                    <td>
                        {{$partner['profile']['kpp_telephone']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['spt_info']))
                <tr class="">
                    <td>
                        @lang('final.year.tax'):
                    </td>
                    <td>
                        <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="sptDataCheck()" data-target="#sptDataInfo">Select</a>
                    </td>
                </tr>
                @endif
            @endif
            </tbody>
        </table>

    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" style="margin-right:300px; " data-dismiss="modal">关闭</button>
    </div>
</form>

<!--税卡信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sptDataInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 1750px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Spt Data Info</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.card.type.new')</th>
                        <th>@lang('final.year.tax.info')</th>
                        <th>@lang('final.real.month.income')</th>
                        <th>@lang('final.couple.tax.info')</th>
                        <th>@lang('final.couple.tax.num')</th>
                        <th>@lang('final.country.only.income')（@lang('final.month'))</th>
                        <th>@lang('final.country.other.income')（@lang('final.month'))</th>
                        <th>@lang('final.sea.only.income')（@lang('final.month'))</th>
                        <th>@lang('final.force.religion')（@lang('final.month'))</th>
                        <th>@lang('final.dismiss.religion')（@lang('final.month'))</th>
                        <th>@lang('final.discount.tax')（@lang('final.month'))</th>
                        <th>@lang('final.shoule.tax.income')（@lang('final.month'))</th>
                        <th>@lang('final.shoule.tax.info')（@lang('final.month'))</th>
                        <th>@lang('final.reback.tax')（@lang('final.month'))</th>
                        <th>@lang('final.reback.tax.sum')（@lang('final.month'))</th>
                        <th>@lang('final.dismiss.tax')（@lang('final.month'))</th>
                        <th>@lang('final.small.loan.info')（@lang('final.year.end.info')）</th>
                        <th>b@lang('final.part.sum')（@lang('final.year.end.info')）</th>
                        <th>@lang('final.detail.info')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if($partner)
                    @if(!empty($partner['spt_info']))
                    @foreach($partner['spt_info'] as $key => $value)
                    <tr class="">
                        <td>
                            {{$key+1}}
                        </td>
                        <td>
                            {{$value['spt_type']}}
                        </td>
                        <td>
                            {{$value['tax_year']}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['total_earning_net']/12))}}
                        </td>
                        <td>
                            {{$value['identity_info']['tax_status_spouse']}}
                        </td>
                        <td>
                            {{$value['identity_info']['spouse_npwp']}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['earning_net_work']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['earning_net_other']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['earning_net_abroad']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['religious_contribution']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['income_after_religious']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['tax_deduction']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['taxable_income']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['income_tax_payable']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['tax_refund']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['amount_tax_payable']/12))}}
                        </td>
                        <td>
                            {{number_format(ceil($value['induk_info']['income_tax_withheld']/12))}}
                        </td>
                        <td>
                            {{$value['attachment2_part_b']['sub_total']}}
                        </td>
                        <td>
                            {{$value['attachment2_part_b']['total_b']}}
                        </td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="detailChildDataCheck()" data-target="#detailChildDataInfo">查询</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-info finish" id="sptData-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--详细清单展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="detailChildDataInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Spt Data Info</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>财产代码</th>
                        <th>财产类型</th>
                        <th>获得年份</th>
                        <th>财产估值</th>
                        <th>相关信息</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($partner))
                    @if(!empty($value['attachment2_part_b']['detail_list']))
                    @foreach($value['attachment2_part_b']['detail_list'] as $key => $value)
                    <tr class="">
                        <td>
                            {{$key+1}}
                        </td>
                        <td>
                            {{$value['property_code']}}
                        </td>
                        <td>
                            {{$value['property_type']}}
                        </td>
                        <td>
                            {{$value['acquisition_year']}}
                        </td>
                        <td>
                            {{$value['acquisition_cost']}}
                        </td>
                        <td>
                            {{$value['information']}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="detailChildData-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<script>
    //spt税卡信息展示
    function sptDataCheck()
    {
        $('#sptData-close').click(function () {
            $("#sptDataInfo").modal('hide');
        })

    }
    //detail税卡信息展示
    function detailChildDataCheck()
    {
        $('#detailChildData-close').click(function () {
            $("#detailChildDataInfo").modal('hide');
        })

    }
</script>