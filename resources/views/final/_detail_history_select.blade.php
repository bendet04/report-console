<section class="final-custom-panel">
    <header class="panel-heading final-custom-tab">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#currentPlatform" data-toggle="tab" aria-expanded="true">{{__('final.currentPlatform')}}</a>
            </li>
            <li class="">
                <a href="#otherPlatform" data-toggle="tab" aria-expanded="false">{{__('final.otherPlatform')}}</a>
            </li>
        </ul>
    </header>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="currentPlatform">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('message.status')</th>
                        <th>@lang('final.reason')</th>
                        <th>@lang('basic.apply.time')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($orders))
                        @foreach($orders as  $key => $value)
                            @if($key>0)
                                <tr class="">
                                    <td>{{$key}}</td>
                                    <td>{{$value->order_status_str}}</td>
                                    <td>{{$value->refuse}}</td>
                                    <td>{{$value->order_time}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
                </div>
            </div>
            <div class="tab-pane" id="otherPlatform">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.sourcePlatform')</th>
                        <th>@lang('final.applyAmount')</th>
                        <th>@lang('message.status')</th>
                        <th>@lang('final.overdueStatus')</th>
                        <th>@lang('basic.apply.time')</th>
                        <th>@lang('track.reapymentDate1')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($otherPlatformOrder))
                        @foreach($otherPlatformOrder as  $key => $value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{\App\Consts\Platform::alias($value->platform)}}</td>
                                <td>{{$value->apply_amount}}</td>
                                <td>{{$value->order_status_str}}</td>
                                <td>{{$value->overdueStatus}}</td>
                                <td>{{$value->order_time}}</td>
                                <td>{{$value->loan_repayment_date}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
                </div>
            </div>
        </div>
    </div>
</section>