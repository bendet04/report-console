<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
            </tr>
            </thead>
            <tbody>
                @if(!empty($partner))
                @if(!empty($partner['base_info']['currentcity']))
                <tr class="">
                    <td>
                        @lang('final.now.address')
                    </td>
                    <td>
                        {{$partner['base_info']['currentcity']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['birthdate']) && !empty($partner['base_info']['birthyear']))
                <tr class="">
                    <td>
                        @lang('final.birth.date')
                    </td>
                    <td>
                        {{$partner['base_info']['birthyear']}} {{$partner['base_info']['birthdate']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['gender']))
                <tr class="">
                    <td>
                        @lang('basic.sex')
                    </td>
                    <td>
                        {{$partner['base_info']['gender']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['relationshipstatus']))
                <tr class="">
                    <td>
                        @lang('basic.living.condition')
                    </td>
                    <td>
                        {{$partner['base_info']['relationshipstatus']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['friendsnum']))
                <tr class="">
                    <td>
                        @lang('final.friend.sum')
                    </td>
                    <td>
                        {{$partner['base_info']['friendsnum']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['company']))
                <tr class="">
                    <td>
                        @lang('final.work.company')
                    </td>
                    <td>
                        {{$partner['base_info']['company']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['position']))
                <tr class="">
                    <td>
                        @lang('final.work.career')
                    </td>
                    <td>
                        {{$partner['base_info']['position']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['city']))
                <tr class="">
                    <td>
                        @lang('final.work.address')
                    </td>
                    <td>
                        {{$partner['base_info']['city']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['starttime']))
                <tr class="">
                    <td>
                        @lang('final.start.time')
                    </td>
                    <td>
                        {{$partner['base_info']['starttime']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['finishtime']))
                <tr class="">
                    <td>
                        @lang('final.deadline.time')
                    </td>
                    <td>
                        {{$partner['base_info']['finishtime']}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['base_info']['mobilephones']))
                <tr class="">
                    <td>
                        @lang('message.agency.admin.phone')
                    </td>
                    <td>
                        {{$partner['base_info']['mobilephones'][0]}}
                    </td>
                </tr>
                @endif
                @if(!empty($partner['friends_info']))
                <tr class="">
                    <td>
                        @lang('final.friend.info')
                    </td>
                    <td>
                        <a data-toggle="modal"  style="cursor: pointer" onclick="friendCheck(this)" data-target="#friendInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['bank_card_info']))
                <tr class="">
                    <td>
                        @lang('final.card.info')
                    </td>
                    <td>
                        <a data-toggle="modal" style="cursor: pointer" onclick="bankCheck()" data-target="#bankCardInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['messenger_info']))
                <tr class="">
                    <td>
                        @lang('final.message.info')
                    </td>
                    <td>
                        <a data-toggle="modal" style="cursor: pointer" onclick="messageCheck()" data-target="#messageInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['work_info']))
                <tr class="">
                    <td>
                        @lang('final.work.info')
                    </td>
                    <td>
                        <a data-toggle="modal" style="cursor: pointer" onclick="workCheck()" data-target="#workInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['publish_info']))
                <tr class="">
                    <td>
                        @lang('final.publish.info')
                    </td>
                    <td>
                        <a data-toggle="modal" style="cursor: pointer" onclick="publishCheck()" data-target="#publishInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['payments_info']))
                <tr class="">
                    <td>
                        @lang('final.payment.info')
                    </td>
                    <td>
                        <a data-toggle="modal" style="cursor: pointer" onclick="paymentCheck()" data-target="#paymentInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['historical_login_info']))
                <tr class="">
                    <td>
                        @lang('final.login.info')
                    </td>
                    <td>
                        <a data-toggle="modal" style="cursor: pointer" onclick="loginCheck()" data-target="#loginInfo">select</a>
                    </td>
                </tr>
                @endif
                @if(!empty($partner['education_info']))
                <tr class="">
                    <td>
                        @lang('final.education.info')
                    </td>
                    <td>
                        <a data-toggle="modal"  style="cursor: pointer" onclick="collegeCheck()" data-target="#collegeInfo">大学信息查询</a>
                        <a data-toggle="modal" style="margin-left:20px;cursor: pointer;" onclick="highSchoolCheck()" data-target="#highSchoolInfo">高中信息查询</a>
                    </td>
                </tr>
                @endif
                {{--<tr class="">--}}
                    {{--<td>--}}
                        {{--申请时间--}}
                    {{--</td>--}}
                    {{--<td>--}}
                        {{--{{$partner['create_time']}}--}}
                    {{--</td>--}}
                {{--</tr>--}}
                @endif
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button"  id="close" class="btn btn-info finish" data-dismiss="modal">关闭</button>
    </div>
</form>

<!--facebook中的朋友信息的查询-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="friendInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Friend</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.username')</th>
                        <th>@lang('final.friend.sum')</th>
                        <th>@lang('final.common.friend')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['friends_info']))
                    @foreach($partner['friends_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['nickname']}}</td>
                        <td>{{$value['numoffriends']}}</td>
                        <td>{{$value['mutual_friends']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="test-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--facebook中的银行信息的查询-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bankCardInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Bank</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.card.number')</th>
                        <th>@lang('final.card.expire')</th>
                        <th>@lang('final.card.code')</th>
                        <th>@lang('final.card.country')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['bank_card_info']))
                    @foreach($partner['task_data']['bank_card_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['cardnum']}}</td>
                        <td>{{$value['expiration']}}</td>
                        <td>{{$value['billingzipcode']}}</td>
                        <td>{{$value['country']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="bank-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--消息信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="messageInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Message</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>好友用户名</th>
                        <th>聊天时间</th>
                        <th>聊天内容</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['messenger_info']))
                    @foreach($partner['task_data']['messenger_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['friend_nickname']}}</td>
                        <td>{{$value['chattime']}}</td>
                        <td>{{$value['content']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="message-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--工作信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="workInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Work</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>工作公司</th>
                        <th>工作职位</th>
                        <th>开始时间</th>
                        <th>到期时间</th>
                        <th>工作地点</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['work_info']))
                    @foreach($partner['task_data']['work_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['company']}}</td>
                        <td>{{$value['position']}}</td>
                        <td>{{$value['starttime']}}</td>
                        <td>{{$value['finishtime']}}</td>
                        <td>{{$value['city']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="work-close">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--登录信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loginInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Work</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.login.type')</th>
                        <th>@lang('final.login.time')</th>
                        <th>@lang('final.view.login.time')</th>
                        <th>@lang('final.login.address')</th>
                        <th>@lang('final.ogin.tool')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['historical_login_info']))
                    @foreach($partner['task_data']['historical_login_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['way']}}</td>
                        <td>{{$value['time']}}</td>
                        <td>{{$value['page_time']}}</td>
                        <td>{{$value['locus']}}</td>
                        <td>{{$value['tool']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="login-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--大学信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="collegeInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Work</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.college.name'')</th>
                        <th>@lang('final.college.address')</th>
                        <th>@lang('final.college.graduate')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['education_info']))
                    @if(!empty($partner['task_data']['education_info']['college_info']))
                    @foreach($partner['task_data']['education_info']['college_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['college_name']}}</td>
                        <td>{{$value['college_address']}}</td>
                        <td>{{$value['college_graduation']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="college-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--高中信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="highSchoolInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Work</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.high.name')</th>
                        <th>@lang('final.high.address')</th>
                        <th>@lang('final.high.graduate')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['education_info']))
                    @if(!empty($partner['task_data']['education_info']['highschool_info']))
                    @foreach($partner['task_data']['education_info']['highschool_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['highschool_name']}}</td>
                        <td>{{$value['highschool_address']}}</td>
                        <td>{{$value['highschool_graduation']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="highSchool-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--支付信息-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="paymentInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Payment</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.login.time')</th>
                        <th>@lang('final.username')</th>
                        <th>@lang('final.get.amount')</th>
                        <th>@lang('final.payment.amount')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['payments_info']))
                    @foreach($partner['task_data']['payments_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['date']}}</td>
                        <td>{{$value['name']}}</td>
                        <td>{{$value['status']}}</td>
                        <td>{{$value['received']}}</td>
                        <td>{{$value['paid']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="payment-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<!--发布信息-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="publishInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Facebook Publish</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.tips.name')</th>
                        <th>@lang('final.tips.time')</th>
                        <th>@lang('final.tips.address')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['task_data']['publish_info']))
                    @foreach($partner['task_data']['publish_info'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['nickname']}}</td>
                        <td>{{$value['page_time']}}</td>
                        <td>{{$value['site']}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="publish-close">关闭</button>
            </div>
        </div>
    </div>
</div>
<script>
    //朋友的信息关闭
    function friendCheck()
    {
        $('#test-close').click(function () {
            $("#friendInfo").modal('hide');
        })

    }
//    银行的信息关闭
    function bankCheck()
    {
        $('#bank-close').click(function () {
            $("#bankInfo").modal('hide');
        })

    }
    //    消息的关闭
    function messageCheck()
    {
        $('#message-close').click(function () {
            $("#messageInfo").modal('hide');
        })

    }
    //    工作的关闭
    function workCheck()
    {
        $('#work-close').click(function () {
            $("#workInfo").modal('hide');
        })

    }
    //    登录的关闭
    function loginCheck()
    {
        $('#login-close').click(function () {
            $("#loginInfo").modal('hide');
        })

    }
    //    大学的关闭
    function collegeCheck()
    {
        $('#college-close').click(function () {
            $("#collegeInfo").modal('hide');
        })

    }
    //    高中的关闭
    function highSchoolCheck()
    {
        $('#highSchool-close').click(function () {
            $("#highSchoolInfo").modal('hide');
        })

    }
    //    支付的关闭
    function paymentCheck()
    {
        $('#payment-close').click(function () {
            $("#paymentInfo").modal('hide');
        })

    }
    //    发布的关闭
    function publishCheck()
    {
        $('#publish-close').click(function () {
            $("#publishInfo").modal('hide');
        })

    }
</script>

