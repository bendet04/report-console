<div class="page-heading custom-page-heading">

    <div class="col-lg-12">
        <section class="custom-panel">
            <header class="panel-heading"><a href="{{$back_url}}">< @lang('message.reback')</a></header>
            <div class="panel-body custom-panel-body col-lg-12">
                <a href="#top-trial"><div class="col-lg-4 final-title active" id="bar-title1">@lang('final.total.score') <span style="font-size:34px;color: red; position: absolute; padding-left: 10px;">0</span></div></a>
                <a href="#device-trial"><div class="col-lg-4 final-title" id="bar-title2">@lang('final.device.data.trial')</div></a>
                <a href="#info-phone-trial"><div class="col-lg-4 final-title" id="bar-title3">@lang('final.message.phone.again')</div></a>
            </div>

            <div class="panel-body custom-score-panel-body">
                <div class="col-lg-8 custom-col-lg-8">
                    <table class="table final-table">
                        <tr>
                            <th>@lang('final.loanLine')</th>
                            <th>@lang('basic.apply.amount')</th>
                            <th>@lang('basic.last.apply.amount')</th>
                            <th>@lang('final.income.level')</th>
                            <th>@lang('final.age')</th>
                        </tr>
                        <tr>
                            <td>@if($loanLimit<$order->apply_amount){{$loanLimit}}@else -- @endif</td>
                            <td>{{number_format($order->apply_amount)}}</td>
                            <td>
                                @if($lastOrder)
                                    {{number_format($lastOrder->apply_amount)}}
                                @else
                                    0
                                @endif
                            </td>
                            <td>{{$userBasicInfo->income_level}}</td>
                            <td>{{$userBasicInfo->age}}


                            </td>
                        </tr>
                        <tr>
                            <th>@lang('final.payPeriod')</th>
                            <th>@lang('basic.apply.time')</th>
                            <th>@lang('final.pay.day')</th>
                            <th>@lang('final.jatuh.tempo')</th>
                            <th>@lang('basic.channel.source')</th>
                        </tr>
                        <tr>
                            <td>{{$order->loan_period}}</td>
                            <td>{{substr($order->created_at, 0, 10)}}</td>
                            <td> {{$phoneTrial->user->paid_period}}</td>
                            <td> {{$payDay}}</td>
                            <td>
                                @if(!empty($channel[0]))
                                    {{$channel[0]->channel}}
                                @endif
                                @if(empty($channel[0]))
                                    ---
                                @endif
                            </td>
                            {{--<td colspan="5"></td>--}}
                        </tr>
                    </table>
                </div>
                <div class="col-lg-4 custom-col-lg-4">
                    <div class="system-score">
                        <table class="table system-score-table">
                            <tr>
                                <th class="system-score-td-title" style="border-top-left-radius: 10px">@lang('final.systerm.score')</th>
                                <th class="system-score-td-score">0 <span class="system-score-td-score-subtract">(-0)</span></th>
                            </tr>
                            <tr>
                                <td class="system-score-td-title">@lang('final.sourcePlatform')</td>
                                <td class="system-score-td-detail color-red">{{\App\Consts\Platform::alias($order->platform)}}</td>
                            </tr>

                            <tr>
                                <td class="system-score-td-title" style="border-bottom-left-radius: 10px">@lang('final.user.type')</td>
                                <td class="system-score-td-detail">
                                    {{$clientType['clientType']}}
                                    @if ($clientType['normalCount'])
                                        @lang('final.normal'):{{$clientType['normalCount']}}
                                    @endif
                                    @if ($clientType['overdueCount'])
                                        @lang('final.overdue'):{{$clientType['overdueCount']}}
                                    @endif
                                    @if ($clientType['applyCount'])
                                        @lang('final.apply'):{{$clientType['applyCount']}}<br />
                                    @endif
                                    {{--@if ($clientType['lastStatus'])--}}
                                        {{--{{$clientType['lastStatus']}}<br />--}}
                                    {{--@endif--}}
                                    @if (in_array($clientType['select'],[0,1]))
                                        <a id="checkHistoryOrderListInfo" style="cursor: pointer" platform="{{ $order->platform }}" basic_id="{{ $order->basic_id }}" date-type="{{$clientType['select']}}" date-uid="{{ $order->uid }}" data-toggle="modal" data-target="#historyOrderSelect">@lang('message.select')</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                @if($isHaveLoan)
                <div class="col-lg-12 custom-col-lg-8 other-platform-box">
                    <div class="table final-table">
                        @foreach($isHaveLoan as $v)
                            @if(isset($v->platform)&&($v['oldUser']||$v['overDue']||$v['order_status']==11))
                            <div class="other-platform">
                                <ul>
                                    <li style="border-top: 0px" class="color-red">{{\App\Consts\Platform::alias($v->platform)}}</li>
                                    <li class="color-red">
                                        @if(isset($v->order_status_str))
                                            {{$v->order_status_str}}
                                        @endif
                                    </li>
                                    <li>
                                        @if($v['oldUser']||$v['overDue'])
                                            @lang('final.old')<br />
                                            @lang('final.normal')：<span style="color: red">{{$v['oldUser']}}</span>,
                                            @lang('final.overdue')：<span style="color: red">{{$v['overDue']}}</span>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                            @endif
                        @endforeach
                        <div style="clear:both;"></div>
                    </div>
                </div>
                @endif

            </div>
        </section>

        <section class="custom-panel">
            <header class="custom-heading sub-title-final">@lang('final.third.data')<img src="/images/big-triangle.png" alt="">
                <span class="pull-right">@lang('final.crossScore'):
                    <i class="plus">+0</i>
                    <i>(-0)</i>
                </span>
            </header>
            <div class="custom-section three-sides" style="display: none;">
                <!--npwp start-->
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="npwp-section section-panel-no-margin">
                            <ul>
                                <li class="npwp-title">
                                    NPWP
                                    <span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                    <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="npwp" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                                </li>
                                <li>&nbsp;</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="npwp-section section-panel-no-margin">
                            <ul>
                                <li class="npwp-title">
                                    TELKOMSEL
                                    <span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                    <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="telkomsel" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                                </li>
                                <li>&nbsp;</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="npwp-section section-panel-no-margin">
                            <ul>
                                <li class="npwp-title">
                                    {{--三方数据交叉--}}
                                    @lang('final.third.check')
                                    <span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                    {{--<span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="telkomsel" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>--}}
                                </li>
                                <li>&nbsp;</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--npwp end-->


                <div class="custom-section-title-square bottom-line margin-top-10 npwp-section-large">@lang('final.tell.score') <span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span></div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                GOJEK<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="gojek" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                LAZADA<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="lazada" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                TOKOPEDIA<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="tokopedia" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!--电商end-->
                <div style="clear:both; padding-top: 10px"></div>

                <div class="custom-section-title-square bottom-line margin-top-10 npwp-section-large">@lang('final.social') <span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span></div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                            <tr>
                                <th>
                                    Facebook<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                    <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="facebook" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                Instagram<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="instagram" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!--社交end-->
                <div style="clear:both; padding-top: 10px"></div>
            </div>
        </section>
        <section class="custom-panel">
            <header class="custom-heading sub-title-final">@lang('final.repayment.wish.alay')<img src="/images/big-triangle.png" alt="">
                <span class="pull-right">@lang('final.crossScore'):
                    <i class="plus">+0</i>
                    <i>(-0)</i>
                </span>
            </header>
            <div class="custom-section three-sides" style="display: none;">
                <!--还款意愿分析start-->


                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.device.data.info')<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>
        </section>
        <section class="custom-panel">
            <header class="custom-heading sub-title-final">@lang('final.user.model')<img src="/images/big-triangle.png" alt="">
                <span class="pull-right">@lang('final.crossScore'):
                    <i class="plus">+0</i>
                    <i>(-0)</i>
                </span>
            </header>
            <div class="custom-section three-sides" style="display: none;">
                <!--用户模型分析 start-->

                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.win.user.model')<span class="pull-right"><i class="plus">+0</i> <i>(-0)</i></span></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!--用户模型分析end-->
            </div>
        </section>
    </div>
    <div style="clear: both;"></div>
</div>
<div  id="device-trial"></div>
