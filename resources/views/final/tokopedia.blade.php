<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
            </tr>
            </thead>
            @if(!empty($partner))
            <tbody>
            @if(!empty($partner['personal_profile']['name']))
            <tr class="">
                <td>
                    Name：
                </td>
                <td>
                    {{$partner['personal_profile']['name']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['mobile_phone']))
            <tr class="">
                <td>
                    Phone：
                </td>
                <td>
                    {{$partner['personal_profile']['mobile_phone']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['birthday']))
            <tr class="">
                <td>
                    @lang('basic.birth.tim')：
                </td>
                <td>
                    {{$partner['personal_profile']['birthday']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['gender']))
            <tr class="">
                <td>
                    @lang('basic,sex')：
                </td>
                <td>
                    {{$partner['personal_profile']['gender']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['email']))
            <tr class="">
                <td>
                    Email：
                </td>
                <td>
                    {{$partner['personal_profile']['email']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['address']))
            <tr class="">
                <td>
                    @lang('basic.address')：
                </td>
                <td>
                    {{$partner['personal_profile']['address']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['positive_num']))
            <tr class="">
                <td>
                    @lang('final.positive.info')：
                </td>
                <td>
                    {{$partner['personal_profile']['positive_num']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['neutral_num']))
            <tr class="">
                <td>
                    @lang('final.eutral.info')：
                </td>
                <td>
                    {{$partner['personal_profile']['neutral_num']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['verified']))
            <tr class="">
                <td>
                    @lang('final.is.certification')：
                </td>
                <td>
                    @if($partner['personal_profile']['verified']==1)
                        @lang('final.certification')
                    @else
                        @lang('final.no.certification')
                    @endif
                </td>
            </tr>
            @endif
            @if(!empty($partner['personal_profile']['saldo_tokopedia']))
            <tr class="">
                <td>
                    RP：
                </td>
                <td>
                    {{$partner['personal_profile']['saldo_tokopedia']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['transaction']))
            <tr class="">
                <td>
                    @lang('final.transation.info')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="transactionCheck()" data-target="#transactionInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['wishlist']))
            <tr class="">
                <td>
                    @lang('final.like')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="wishlistLikeCheck()" data-target="#wishlistLikeInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['delivery_address']))
            <tr class="">
                <td>
                    @lang('final.shop.address')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="reviceAddressCheck()" data-target="#reviceAddressInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['shopping_cart']))
            <tr class="">
                <td>
                    @lang('final.shop.car')：
                </td>
                <td>
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="shoppingCarCheck()" data-target="#shoppingCarInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['shopping_cart_total']['grand_total']))
            <tr class="">
                <td>
                    @lang('final.car.shop.amount')：
                </td>
                <td>
                   {{$partner['shopping_cart_total']['grand_total']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['shopping_cart_total']['subtotal']))
            <tr class="">
                <td>
                    @lang('final.car.shop.order.amount')：
                </td>
                <td>
                    {{$partner['shopping_cart_total']['subtotal']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['shopping_cart_total']['service_charge']))
            <tr class="">
                <td>
                    @lang('final.car.shop.charge.amount')：
                </td>
                <td>
                    {{$partner['shopping_cart_total']['service_charge']}}
                </td>
            </tr>
            @endif
            </tbody>
            @endif
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>

<!--交易信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="transactionInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 1350px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Transaction Info</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('message.order.number')</th>
                        <th>@lang('final.order.time')</th>
                        <th>@lang('final.order.status')</th>
                        <th>@lang('final.order.admount')</th>
                        <th>@lang('final.fate')</th>
                        <th>@lang('final.order.sum')</th>
                        <th>@lang('final.charge')</th>
                        <th>@lang('final.diliver.address')</th>
                        <th>@lang('final.diliver.type')</th>
                        <th>@lang('final.recevie.name')</th>
                        <th>@lang('final.ecevie.phone')</th>
                        <th>@lang('final.product.info')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['transaction']))
                    @foreach($partner['transaction'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['transaction_no']}}</td>
                        <td>{{$value['transaction_date']}}</td>
                        <td>{{$value['transaction_status']}}</td>
                        <td>{{$value['grand_total']}}</td>
                        <td>{{$value['shipping_cost']}}</td>
                        <td>{{$value['total_items']}}</td>
                        <td>{{$value['additional_cost']}}</td>
                        <td>{{$value['shipping_address']}}</td>
                        <td>{{$value['shipping_way']}}</td>
                        <td>{{$value['receiver_name']}}</td>
                        <td>{{$value['receiver_phone']}}</td>
                        <td>
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="productCheck()" data-target="#productInfo">select</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="transaction-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>
<!--商品信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="productInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Order Package</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('final.product.name')</th>
                        <th>@lang('final.order.sum')</th>
                        <th>@lang('final.product.sum.price')</th>
                        <th>@lang('final.product.sigle.price')</th>
                        <th>@lang('final.store.name')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($value['product']))
                    <tr class="">
                        <td>{{$value['product']['name']}}</td>
                        <td>{{$value['product']['amount']}}</td>
                        <td>{{$value['product']['price']}}</td>
                        <td>{{$value['product']['unit_price']}}</td>
                        <td>{{$value['product']['store_name']}}</td>
                    </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="product-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>

<!--收藏信息展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="wishlistLikeInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 1350px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Transaction Info</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.product.name')</th>
                        <th>@lang('final.product.sigle.price')</th>
                        <th>@lang('final.store.score')</th>
                        <th>@lang('final.store.part.num')</th>
                        <th>@lang('final.tore.name')</th>
                        <th>@lang('final.store.address')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['wishlist']))
                    @foreach($partner['wishlist'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['product_name']}}</td>
                        <td>{{$value['product_unitprice']}}</td>
                        <td>{{$value['product_score']}}</td>
                        <td>{{$value['product_rating_num']}}</td>
                        <td>{{$value['store_name']}}</td>
                        <td>{{$value['store_address']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="wishlistLike-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>
<!--收货地址展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="reviceAddressInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 1350px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Revice Address</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.address.type')</th>
                        <th>@lang('final.revicer.name')</th>
                        <th>@lang('final.detail.address')</th>
                        <th>@lang('final.email.num')</th>
                        <th>@lang('final.province')</th>
                        <th>@lang('final.sum.distinct')</th>
                        <th>@lang('final.all.distinct')</th>
                        <th>Phone</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['delivery_address']))
                    @foreach($partner.task_data.delivery_address as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['address_type']}}</td>
                        <td>{{$value['receiver_name']}}</td>
                        <td>{{$value['detailed_address']}}</td>
                        <td>{{$value['zip_code']}}</td>
                        <td>{{$value['province']}}</td>
                        <td>{{$value['regency']}}</td>
                        <td>{{$value['sub_district']}}</td>
                        <td>{{$value['phone']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="reviceAddress-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>

<!--购物车展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="shoppingCarInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 1350px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Shopping Car</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.store.name')</th>
                        <th>@lang('final.diliver.address')</th>
                        <th>@lang('final.diliver.type')</th>
                        <th>@lang('final.ecevie.name')</th>
                        <th>@lang('final.recevie.phone')</th>
                        <th>@lang('final.product.name')</th>
                        <th>@lang('final.order.sum')</th>
                        <th>@lang('final.product.sum.price')</th>
                        <th>@lang('final.product.sigle.price')</th>
                        <th>@lang('final.extra.fate')</th>
                        <th>@lang('final.insurance.fate')</th>
                        <th>@lang('final.fate.sum')</th>
                        <th>@lang('final.price.sum')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($partner['shopping_cart']))
                    @foreach($partner['shopping_cart'] as $key => $value)
                    <tr class="">
                        <td>{{$key + 1}}</td>
                        <td>{{$value['store_name']}}</td>
                        <td>{{$value['shipping_address']}}</td>
                        <td>{{$value['shipping_way']}}</td>
                        <td>{{$value['receiver_name']}}</td>
                        <td>{{$value['receiver_phone']}}</td>
                        <td>{{$value['product_name']}}</td>
                        <td>{{$value['total_items']}}</td>
                        <td>{{$value['product_price']}}</td>
                        <td>{{$value['product_unitprice']}}</td>
                        <td>{{$value['additional_cost']}}</td>
                        <td>{{$value['insurance_cost']}}</td>
                        <td>{{$value['shipping_cost']}}</td>
                        <td>{{$value['total_price']}}</td>
                    </tr>
                    @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="shoppingCar-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>


<script>
    //购物地址的信息@lang('message.close')
    function transactionCheck()
    {
        $('#transaction-close').click(function () {
            $("#transactionInfo").modal('hide');
        })

    }
//    产品的信息关闭
    function productCheck()
    {
        $('#product-close').click(function () {
            $("#productInfo").modal('hide');
        })

    }
    //    收藏关闭
    function wishlistLikeCheck()
    {
        $('#wishlistLike-close').click(function () {
            $("#wishlistLikeInfo").modal('hide');
        })

    }
    //    收货地址关闭
    function reviceAddressCheck()
    {
        $('#reviceAddress-close').click(function () {
            $("#reviceAddressInfo").modal('hide');
        })

    }
    //    购物车关闭
    function shoppingCarCheck()
    {
        $('#shoppingCar-close').click(function () {
            $("#shoppingCarInfo").modal('hide');
        })

    }


</script>
