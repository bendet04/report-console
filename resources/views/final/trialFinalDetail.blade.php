<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content" id="top-trial">
        @include('public/right_header')

        @if(!empty($riskScore)&&$riskScore['result_code']=='1000')
        @include('final/_detail_score_risk')
        @else
        @include('final/_detail_score')
        @endif

        @include('final/_detail_device')


        @include('final/_detail_trial')

    </div>
</section>



<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="historyOrderSelect" class="modal fade">
    <div class="modal-dialog" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('final.order.history')</h4>
            </div>
            <div class="modal-body scroll-box" style="max-height: 500px; overflow: auto;">
            </div>
        </div>
    </div>
</div>
<!-- 第三方授权的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('final.third.data')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 第三方授权的结束 -->

@include('public/base_script')
<script>
     $("#checkVerity").click(function () {
         var id = $(this).attr('data-id');
         var url = $(this).attr('data-url');
         $.ajax({
             url:"/final/verityKtp",
             type:'get',
             data:{
                 id:id,
                 back_url:url,
             },
             dataType: "json",
         }).done(function (json) {
             if (json.code) {
                 $("#verityResult").css('display','inline');
                 $("#verityResult").html(json.info);
                 $("#checkVerity").css('display','none');
             } else {
                 alert('try again!!!!');
             }
         });
     });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    if('{{$riskStatus}}'==1) {
        var ktp='{{$userBasicInfo->ktp_number}}';
        var username='{{$userBasicInfo->name}}';
//        var ktp='3311042509900003';
//        var username='Riswanda Wira Yudha';
//        var username='CIPTO BADILI';
      //获取短信
        $.ajax({
            url: "/final/riskdata",
            type: "post",
            data: {
                'url': '/gather/review/sms?',
                'ktp':ktp,
                'username':username,
                'type': 'sms'
            },
            dataType: "html",
            success: function (data) {
                if (!isNaN(data)) {
                    return false;
                }
                else {
                    $('.device_risk').html(data);

                }
            }
        });
//   //获取通话记录
        $.ajax({
            url: "/final/riskdata",
            type: "post",
            data: {
                'url': '/gather/review/cl?',
                'ktp':ktp,
                'username':username,
                'type': 'cl'
            },
            dataType: "html",
            success: function (data) {
                if (!isNaN(data)) {
                    return false;
                }
                else {
                    $(".call").html(data);
                }
            },

        });
//        //获取通讯录
        $.ajax({
            url: "/final/riskdata",
            type: "post",
            data: {
                'url': '/gather/review/ab?',
                'ktp':ktp,
                'username':username,
                'type':'ab'
            },
            dataType: "html",
            success: function (data) {
                if (!isNaN(data)) {
                    return false;
                }
                else {
                    $(".ab").html(data);
                }

            },

        });
        //获取app
        $.ajax({
            url: "/final/riskdata",
            type: "post",
            data: {
                'url': '/gather/review/app?',
                'ktp':ktp,
                'username':username,
                'type': 'app'
            },
            dataType: "html",
            success: function (data) {
                if (!isNaN(data)) {
                    return false;
                }
                else {
                    $(".app").html(data);
                }

            }


        });
    }
    //查看第三方授权信息
    $('.partner-detail').click(function(){
        $('#myModal .modal-body').html('');
        var uid = $(this).attr('data-uid');
        var partner = $(this).attr('data-partner');
        $.ajax({
            url:"/final/partnerInfo",
            type:"get",
            data:{
                'uid': uid,
                'partner_name': partner,
            },
            dataType:"html",
            success:function(data){
                $('#myModal .modal-body').html(data);
            }
        });
    });

    $('.tools a').click(function() {
        var item = $(this).attr('data-detail');
        if($(this).hasClass('fa-chevron-up')){
            $('#'+item).hide();
        }else{
            $('#'+item).show();
        }

        if($('#'+item).parent().parent().parent().parent().find('.col-lg-12').length==0){
            var cur_height = 0;
            $.each($('#'+item).parent().parent().parent().parent().find('.no-right-padding'), function(item){
                if($(this).height()>cur_height){
                    cur_height = $(this).height();
                }
            });
            if(cur_height>100){
                $('#'+item).parent().parent().parent().parent().find('.npwp-box').height(cur_height-1);
            }else{
                $('#'+item).parent().parent().parent().parent().find('.npwp-box').height(0);
            }
        }
    });

    @if ($isOwner)
    //终审通过
    $('.trial-pass-btn').click(function(){
        $.confirm({
            title: '系统提示',
            content: '是否确认终审通过？',
            buttons: {
                confirm: {
                    text: '确认',
                    action: function() {
                        final_trial(1);
                    }
                },
                cancel: {
                    text: '取消',
                    btnClass: 'btn-info'
                }
            }
        });
//        final_trial(1);
    });
    //终审拒绝
    $('.trial-refuse-btn').click(function(){
        $.confirm({
            title: '系统提示',
            content: '是否确认终审拒绝？',
            buttons: {
                confirm: {
                    text: '确认',
                    action: function() {
                        final_trial(2);
                    }
                },
                cancel: {
                    text: '取消',
                    btnClass: 'btn-info'
                }
            }
        });
//        final_trial(2);
    });

    function final_trial(status){
        var order_id = $('#order_id').val();
        var remark = $('#remark').val();
        var level  = $('#level').val();
        var uid  = '{{$order->uid}}';

        if((isNaN(level)||level=='') && status===1){
            $('#final-trial-remark').show().html('用户收入水平不能为空');
            return false;
        }
        if(!order_id){
            $('#final-trial-remark').show().html('数据错误');
            return false;
        }
        if(!remark){
            $('#final-trial-remark').show().html('终审意见不能为空');
            return false;
        }

        $('#final-trial-remark').hide();

        $('.trial-pass-btn').attr('disabled', true);
        $('.trial-refuse-btn').attr('disabled', true);

        $.ajax({
            url:"/final/trialjalksealksdfjkasdlf98qeja",
            type:"POST",
            data:{
                order_id: order_id,
                status: status,
                remark: remark,
                level:  level,
                uid:  uid,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:"json",
            success:function(data){
                if(data.code==1){
                    location.href='<?php echo $back_url;?>';
                }else{
                    if(data.info=='放款结果：交易处理中')
                    {
                        return true;
                    }
                    else
                    {
                        $.alert({
                            title: '审核结果',
                            content: data.info,
                            buttons: {
                                okay: {
                                    text: 'OK',
                                    btnClass: 'btn-info'
                                }
                            }
                        });
                    }

                    $('.trial-pass-btn').attr('disabled', false);
                    $('.trial-refuse-btn').attr('disabled', false);
                }
            },
            error:function(data){
                $.alert({
                    title: '审核结果',
                    content: '审核失败',
                    buttons: {
                        okay: {
                            text: 'OK',
                            btnClass: 'btn-info'
                        }
                    }
                });
                $('.trial-pass-btn').attr('disabled', false);
                $('.trial-refuse-btn').attr('disabled', false);
            }
        });
    }
    @endif
    //历史订单查询
    $("#checkHistoryOrderListInfo").click(function () {
        var uid = $(this).attr('date-uid');
        var type=　$(this).attr('date-type');
        var platform=　$(this).attr('platform');
        var basic_id=　$(this).attr('basic_id');
        // 0为老用户 1为新用户
        if(type==0){
            var url='/final/selectHistoryOld';
        }else if(type==1){
            var url='/final/selectHistory';
        }
        $('#historyOrderSelect .modal-body').html('');
        $.ajax({
            url:url,
            type:'get',
            data:{
                'uid':uid,
                'platform':platform,
                'basic_id':basic_id
            },
            dataType:'html',
            success:function (data) {
                // alert(data);
                $('#historyOrderSelect .modal-body').html(data);
            }
        });
    });

    $(window).scroll(function () {
        // 导航吸顶
        if($(window).scrollTop() > 52){
            $('.custom-panel-body').addClass('final-bar-fixed');

            if($('.sticky-header').hasClass('left-side-collapsed')){
                $('.final-bar-fixed').css('left', '52px').css('padding-right', '52px');
            }else{
                $('.final-bar-fixed').css('left', '240px').css('padding-right', '240px');
            }

            $('.final-bar-fixed a').css('width', '33%');
        }else{
            $('.final-bar-fixed').css('left', '0px').css('padding-right', '0px');;
            $('.custom-panel-body').removeClass('final-bar-fixed');
        }

        //滑动动态切换tab标签
        if($(window).scrollTop() >= 0 && $(window).scrollTop() < $('#device-trial').offset().top) {
            $('#bar-title1').parent().parent().find('div').removeClass('active');
            $('#bar-title1').addClass('active');
        }
        if($(window).scrollTop() >= $('#device-trial').offset().top && $(window).scrollTop() < $('#info-phone-trial').offset().top) {
            $('#bar-title2').parent().parent().find('div').removeClass('active');
            $('#bar-title2').addClass('active');
        }
        if($(window).scrollTop() > $('#info-phone-trial').offset().top) {
            $('#bar-title3').parent().parent().find('div').removeClass('active');
            $('#bar-title3').addClass('active');
        }
    });
    $('.toggle-btn').click(function(){
        if($('.sticky-header').hasClass('left-side-collapsed')){
            $('.final-bar-fixed').css('left', '52px').css('padding-right', '52px');
        }else{
            $('.final-bar-fixed').css('left', '240px').css('padding-right', '240px');
        }
    });
    $('.custom-panel-body div').click(function(){
        $(this).parent().parent().find('div').removeClass('active');
        $(this).addClass('active');
    });
    $('.sub-title-final').click(function(){
        if($(this).parent().find('.custom-section').css('display')=='none'){
            $(this).parent().find('.custom-section').css('display', 'block');
        }else{
            $(this).parent().find('.custom-section').css('display', 'none');
        }
    });

    $(document).on('click', '.custom-section-title-square', function() {
        if($(this).parent().find('.panel-body-no-padding').css('display')=='none'){
            $(this).parent().find('.panel-body-no-padding').css('display', 'block');
        }else{
            $(this).parent().find('.panel-body-no-padding').css('display', 'none');
        }
    });

</script>
</body>
</html>