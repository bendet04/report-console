<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
            </tr>
            </thead>
            @if(!empty($partner))
            <tbody>
            {{--有task_Data start--}}
            @if(isset($partner['task_data']))
           @if(!empty($partner['identity_code']))
            <tr class="">
                <td>
                    KTP
                </td>
                <td>
                    {{$partner['identity_code']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['user_mobile']))
            <tr class="">
                <td>
                    PHONE
                </td>
                <td>
                    {{$partner['user_mobile']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['user_name']))
            <tr class="">
                <td>
                    Name
                </td>
                <td>
                    {{$partner['user_name']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['real_name']))
            <tr class="">
                <td>
                    @lang('final.real.name')
                </td>
                <td>
                    {{$partner['real_name']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['name']))
            <tr class="">
                <td>
                    @lang('final.real.name')
                </td>
                <td>
                    {{$partner['profile_info']['name']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['user_name']))
            <tr class="">
                <td>
                    @lang('final.nick.name')
                </td>
                <td>
                    {{$partner['profile_info']['user_name']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['website']))
            <tr class="">
                <td>
                    @lang('final.relative.website')
                </td>
                <td>
                    {{$partner['profile_info']['website']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['bio']))
            <tr class="">
                <td>
                    @lang('final.personal.discription')
                </td>
                <td>
                    {{$partner['profile_info']['bio']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['email']))
            <tr class="">
                <td>
                    email
                </td>
                <td>
                    {{$partner['profile_info']['email']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['phone_number']))
            <tr class="">
                <td>
                    Phone
                </td>
                <td>
                    {{$partner['profile_info']['phone_number']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['profile_info']['gender']))
            <tr class="">
                <td>
                    @lang('basic.sex')
                </td>
                <td>
                    {{$partner['profile_info']['gender']}}
                </td>
            </tr>
            @endif
            @if(!empty($partner['synced_contacts']))
            <tr class="">
                <td>
                    @lang('final.same.contact')
                </td>
                <td>
                    {{$partner['synced_contacts']['total_contacts']}}
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="relativeCheck()" data-target="#relativeInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($partner['followers_info']))
            <tr class="">
                <td>
                    @lang('final.fan.number')
                </td>
                <td>
                    {{$partner['followers_info']['total_followers']}}
                    <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="fansCheck()" data-target="#fansInfo">select</a>
                </td>
            </tr>
            @endif
            @if(!empty($parnter['created_time']))
            <tr class="">
                <td>
                    @lang('message.create.time')
                </td>
                <td>
                    {{$partner['created_time']}}
                </td>
            </tr>
            @endif
            @endif
            {{--有task_data end--}}
            @if(!isset($partner['task_data']))
                @if(!empty($partner['profile_info']['phone_number']))
                    <tr class="">
                        <td>
                            Phone
                        </td>
                        <td>
                            {{$partner['profile_info']['phone_number']}}
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['profile_info']['name']))
                    <tr class="">
                        <td>
                            @lang('final.username')
                        </td>
                        <td>
                            {{$partner['profile_info']['name']}}
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['profile_info']['user_name']))
                    <tr class="">
                        <td>
                            @lang('final.nick.name')
                        </td>
                        <td>
                            {{$partner['profile_info']['user_name']}}
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['profile_info']['website']))
                    <tr class="">
                        <td>
                            @lang('finalrelative.website')
                        </td>
                        <td>
                            {{$partner['profile_info']['website']}}
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['profile_info']['bio']))
                    <tr class="">
                        <td>
                            @lang('finalpersonal.discription')
                        </td>
                        <td>
                            {{$partner['profile_info']['bio']}}
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['profile_info']['email']))
                    <tr class="">
                        <td>
                            email
                        </td>
                        <td>
                            {{$partner['profile_info']['email']}}
                        </td>
                    </tr>
                @endif

                @if(!empty($partner['profile_info']['gender']))
                    <tr class="">
                        <td>
                            @lang('basic.sex')
                        </td>
                        <td>
                            {{$partner['profile_info']['gender']}}
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['synced_contacts']['total_contacts']))
                    <tr class="">
                        <td>
                            @lang('final.same.contact')
                        </td>
                        <td>
                            {{$partner['synced_contacts']['total_contacts']}}
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="relativeCheck()" data-target="#relativeInfo">select</a>
                        </td>
                    </tr>
                @endif
                @if(!empty($partner['followers_info']['total_followers']))
                    <tr class="">
                        <td>
                            @lang('finalfan.number')
                        </td>
                        <td>
                            {{$partner['followers_info']['total_followers']}}
                            <a data-toggle="modal"  style="cursor: pointer;margin-left:20px;" onclick="fansCheck()" data-target="#fansInfo">select</a>
                        </td>
                    </tr>
                @endif
            @endif
            </tbody>
            @endif
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">关闭</button>
    </div>
</form>

<!--联系人展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="relativeInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Instagram Work</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.close')</th>
                        <th>@lang('final.contact.name')</th>
                        <th>@lang('final.contact.phone')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($partner['synced_contacts']['contacts_detail']))
                        @foreach($partner['synced_contacts']['contacts_detail'] as $key => $value)
                        <tr class="">
                            <td>{{$key + 1}}</td>
                            <td>{{$value['contacts_name']}}</td>
                            <td>{{$value['contacts_number']}}</td>
                        </tr>
                        @endforeach
                    @endif

                    @if(!empty($partner['task_data']['synced_contacts']['contacts_detail']))
                        @foreach($partner['task_data']['synced_contacts']['contacts_detail'] as $key => $value)
                            <tr class="">
                                <td>{{$key + 1}}</td>
                                <td>{{$value['contacts_name']}}</td>
                                <td>{{$value['contacts_number']}}</td>
                            </tr>
                        @endforeach
                    @endif
                    @if(!empty($partner['synced_contacts']['contacts_detail']))
                        @foreach($partner['synced_contacts']['contacts_detail'] as $key => $value)
                            <tr class="">
                                <td>{{$key + 1}}</td>
                                <td>{{$value['contacts_name']}}</td>
                                <td>{{$value['contacts_number']}}</td>
                            </tr>
                        @endforeach

                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="relative-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>
<!--粉丝展示-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fansInfo" class="modal  fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" style="margin-left:50px;" type="button">×</button>
                <h4 class="modal-title">Instagram Work</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-condensed cf" id="dynamic-sample">
                    <thead>
                    <tr>
                        <th>@lang('message.number')</th>
                        <th>@lang('final.fan.name')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($partner['task_data']['followers_info']['followers_detail']))
                        @foreach($partner['task_data']['followers_info']['followers_detail'] as $key => $value)
                        <tr class="">
                            <td>{{$key + 1}}</td>
                            <td>{{$value['followers_name']}}</td>
                        </tr>
                        @endforeach
                    @endif
                    {{--//区分task_data--}}
                    @if(!empty($partner['followers_info']['followers_detail']))
                        @foreach($partner['followers_info']['followers_detail'] as $key => $value)
                            <tr class="">
                                <td>{{$key + 1}}</td>
                                <td>{{$value['followers_name']}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info finish" id="fans-close">@lang('message.close')</button>
            </div>
        </div>
    </div>
</div>
<script>
    //朋友的信息
    function relativeCheck(obj)
    {
        $('#relative-close').click(function () {
            $("#relativeInfo").modal('hide');
        })

    }
    //粉丝的信息关闭
    function fansCheck(obj)
    {
        $('#fans-close').click(function () {
            $("#fansInfo").modal('hide');
        })

    }
</script>
