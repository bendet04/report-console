<div class="page-heading custom-page-heading">

    <div class="col-lg-12">
        <section class="custom-panel">
            <header class="panel-heading"><a href="{{$back_url}}">< @lang('message.reback')</a></header>
            <div class="panel-body custom-panel-body col-lg-12">
             <a href="#top-trial"><div class="col-lg-4 final-title active" id="bar-title1">@lang('final.total.score')<span style="font-size:34px;color: red; position: absolute; padding-left: 10px;"> @if($riskScore['total'])+{{$riskScore['total+']+$riskScore['total-']}}@endif</span></div></a>
                <a href="#device-trial"><div class="col-lg-4 final-title" id="bar-title2">@lang('final.device.data.trial')</div></a>
                <a href="#info-phone-trial"><div class="col-lg-4 final-title" id="bar-title3">@lang('final.message.phone.again')</div></a>
            </div>

            <div class="panel-body custom-score-panel-body">
                <div class="col-lg-8 custom-col-lg-8">
                    <table class="table final-table">
                        <tr>
                            <th>@lang('final.loanLine')</th>
                            <th>@lang('basic.apply.amount')</th>
                            <th>@lang('basic.last.apply.amount')</th>
                            <th>@lang('final.income.level')</th>
                            <th>@lang('final.age')</th>
                        </tr>
                        <tr>
                            <td>@if($loanLimit<$order->apply_amount){{$loanLimit}}@else -- @endif</td>
                            <td>{{number_format($order->apply_amount)}}</td>
                            <td>
                                @if($lastOrder)
                                    {{number_format($lastOrder->apply_amount)}}
                                @else
                                    0
                                @endif
                            </td>
                            <td>{{$userBasicInfo->income_level}}</td>
                            <td>{{$userBasicInfo->age}}


                            </td>
                        </tr>
                        <tr>
                            <th>@lang('final.payPeriod')</th>
                            <th>@lang('basic.apply.time')</th>
                            <th>@lang('final.pay.day')</th>
                            <th>@lang('final.jatuh.tempo')</th>
                            <th>@lang('basic.channel.source')</th>
                        </tr>
                        <tr>
                            <td>{{$order->loan_period}}</td>
                            <td>{{substr($order->created_at, 0, 10)}}</td>
                            <td>{{$phoneTrial->user->paid_period}}</td>
                            <td>{{$payDay}}</td>
                            <td>
                                @if(!empty($channel[0]))
                                    {{$channel[0]->channel}}
                                @endif
                                @if(empty($channel[0]))
                                    ---
                                @endif
                            </td>
                            {{--<td colspan="5"></td>--}}
                        </tr>
                    </table>
                </div>
                <div class="col-lg-4 custom-col-lg-4">
                    <div class="system-score">
                        <table class="table system-score-table">
                            <tr>
                                <th class="system-score-td-title" style="border-top-left-radius: 10px">@lang('final.systerm.score')</th>
                                <th class="system-score-td-score">@if(!empty($riskScore['total'])){{$riskScore['total+']}} @endif <span class="system-score-td-score-subtract">({{$riskScore['total-']}})</span></th>
                            </tr>
                            <tr>
                                <td class="system-score-td-title">@lang('final.sourcePlatform')</td>
                                <td class="system-score-td-detail  color-red">{{\App\Consts\Platform::alias($order->platform)}}</td>
                            </tr>

                            <tr>
                                <td class="system-score-td-title" style="border-bottom-left-radius: 10px">@lang('final.user.type')</td>
                                <td class="system-score-td-detail">
                                    {{$clientType['clientType']}}
                                    @if ($clientType['normalCount'])
                                        @lang('final.normal'):{{$clientType['normalCount']}}
                                    @endif
                                    @if ($clientType['overdueCount'])
                                        @lang('final.overdue'):{{$clientType['overdueCount']}}
                                    @endif
                                    @if ($clientType['applyCount'])
                                        @lang('final.apply'):{{$clientType['applyCount']}}<br />
                                    @endif
                                    {{--@if ($clientType['lastStatus'])--}}
                                    {{--{{$clientType['lastStatus']}}<br />--}}
                                    {{--@endif--}}
                                    @if (in_array($clientType['select'],[0,1]))
                                        <a id="checkHistoryOrderListInfo" style="cursor: pointer" date-type="{{$clientType['select']}}" basic_id="{{ $order->basic_id }}" platform="{{ $order->platform }}"  date-uid="{{ $order->uid }}" data-toggle="modal" data-target="#historyOrderSelect">@lang('message.select')</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>


                @if($isHaveLoan)
                    <div class="col-lg-12 custom-col-lg-8 other-platform-box">
                        <div class="table final-table">
                            @foreach($isHaveLoan as $v)
                                @if(isset($v->platform)&&($v['oldUser']||$v['overDue']||$v['order_status']==11))
                                    <div class="other-platform">
                                        <ul>
                                            <li style="border-top: 0px" class="color-red">{{\App\Consts\Platform::alias($v->platform)}}</li>
                                            <li class="color-red">
                                                @if(isset($v->order_status_str))
                                                    {{$v->order_status_str}}
                                                @endif
                                            </li>
                                            <li>
                                                @if($v['oldUser']||$v['overDue'])
                                                    @lang('final.old')<br />
                                                    @lang('final.normal')：<span style="color: red">{{$v['oldUser']}}</span>,
                                                    @lang('final.overdue')：<span style="color: red">{{$v['overDue']}}</span>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            @endforeach
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                @endif
            </div>


        </section>

        <section class="custom-panel">
            <header class="custom-heading sub-title-final">@lang('final.third.data')<img src="/images/big-triangle.png" alt="">
                <span class="pull-right">@lang('final.crossScore'):
                    <?php
                    $npwp = !empty($riskScore['total']) ? $riskScore['npwp_email']+$riskScore['npwp_name']+$riskScore['sub_npwp']+$riskScore['npwp_tel']+$riskScore['npwp_income'] : 0;
                    $telkomsel = !empty($riskScore['total']) ? $riskScore['sub_telkomsel']+$riskScore['telkomsel_tel']+$riskScore['telkomsel_credits']+$riskScore['telkomsel_points']+$riskScore['telkomsel_expire'] : 0;
                    $shop = !empty($riskScore['total']) ? $riskScore['sub_gojek']+$riskScore['gojek_tel']+$riskScore['gojek_email']+$riskScore['gojek_point']+$riskScore['gojek_number']+$riskScore['sub_lazada']+$riskScore['lazada_tel']+$riskScore['lazada_name']+$riskScore['lazada_address']+$riskScore['lazada_email']+$riskScore['tokopedia_name']+$riskScore['tokopedia_address']+$riskScore['tokopedia_email']+$riskScore['sub_tokopedia']+$riskScore['tokopedia_tel'] : 0;
                    $social = !empty($riskScore['total']) ? $riskScore['sub_facebook']+$riskScore['facebook_tel']+$riskScore['facebook_work']+$riskScore['facebook_last_login']+$riskScore['sub_instagram']+$riskScore['instagram _tel']+$riskScore['instagram _number'] : 0;
                    ?>
                    <i class="plus">+{{$riskScore['threeparty+']}}</i>
                <i>({{$riskScore['threeparty-']}})</i>
            </span>
            </header>
            <div class="custom-section three-sides">

                <!--npwp-telkomsel start-->
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="npwp-section section-panel-no-margin">
                            <ul>
                                <li class="npwp-title">
                                    NPWP
                                    <span class="pull-right">
                                        <i class="plus">+{{$riskScore['mpwpAdd+']}}</i>
                                        <i>({{$riskScore['mpwpAdd-']}})</i>
                                    </span>
                                    <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="npwp" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                                </li>
                                <li><span class="score-success">@if(!empty($riskScore['npwp_income'])){{$riskScore['npwp_income']}}@elseif(empty($riskScore['npwp_income'])) +1 @endif</span>@lang('final.real.month.income')</li>
                                <li><span class="score-success">@if(!empty($riskScore['sub_telkomsel'])){{$riskScore['sub_npwp']}}@elseif(empty($riskScore['sub_npwp'])) +0 @endif</span>@lang('final.is.submit.npwp')</li>
                                <li><span class="score-success">@if(!empty($riskScore['npwp_tel'])){{$riskScore['npwp_tel']}}@elseif(empty($riskScore['npwp_tel'])) +0 @endif</span>@lang('final.is.same.phone')</li>
                                <li><span class="score-success">@if(!empty($riskScore['npwp_name'])){{$riskScore['npwp_name']}}@elseif(empty($riskScore['npwp_name'])) +0 @endif</span>@lang('final.is.same.name')</li>
                                <li style="border-bottom: 0px"><span class="score-success">@if(!empty($riskScore['npwp_email'])){{$riskScore['npwp_email']}}@elseif(empty($riskScore['npwp_email'])) +0 @endif</span>@lang('final.is.same.email')</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="npwp-section section-panel-no-margin">
                            <ul>
                                <li class="npwp-title">
                                    TELKOMSEL
                                    <span class="pull-right">
                                        <i class="plus">@if($riskScore['telkomselAdd+'])+{{$riskScore['telkomselAdd+']}}@endif</i>
                                        <i>({{$riskScore['telkomselAdd-']}})</i>
                                    </span>
                                    <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="telkomsel" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                                </li>
                                <li><span class="score-success">@if(!empty($riskScore['sub_telkomsel'])){{$riskScore['sub_telkomsel']}}@elseif(empty($riskScore['sub_telkomsel'])) +0 @endif</span>@lang('final.is.submit.telkomsel')</li>
                                <li><span class="score-success">@if(!empty($riskScore['telkomsel_tel'])){{$riskScore['telkomsel_tel']}}@elseif(empty($riskScore['telkomsel_tel'])) +0 @endif</span>@lang('final.is.same.phone')</li>
                                <li><span class="score-success">@if(!empty($riskScore['telkomsel_credits'])){{$riskScore['telkomsel_credits']}}@elseif(empty($riskScore['telkomsel_credits'])) +0 @endif</span>@lang('final.credit.score')</li>
                                <li><span class="score-success">@if(!empty($riskScore['telkomsel_points'])){{$riskScore['telkomsel_points']}}@elseif(empty($riskScore['telkomsel_points'])) +0 @endif</span>@lang('final.sum.to.score')</li>
                                <li style="border-bottom: 0px"><span class="score-success">@if(!empty($riskScore['telkomsel_expire'])){{$riskScore['telkomsel_expire']}}@elseif(empty($riskScore['telkomsel_expire'])) +0 @endif</span>@lang('final.deadline.day')</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="npwp-section section-panel-no-margin">
                            <ul>
                                <li class="npwp-title">
                                    @lang('final.third.check')
                                    <span class="pull-right">
                                        <i class="plus">@if($riskScore['threePhone+'])+{{$riskScore['threePhone+']}}@endif</i>
                                        <i>({{$riskScore['threePhone-']}})</i>
                                    </span>
                                    {{--<span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="telkomsel" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>--}}
                                </li>
                                <li><span class="score-success">@if(!empty($riskScore['np_concat_telkomse'])){{$riskScore['np_concat_telkomse']}}@elseif(empty($riskScore['np_concat_telkomse'])) +0 @endif</span>@lang('final.np.concat.telkomse')</li>
                                <li><span class="score-success">@if(!empty($riskScore['gojek_lazada_toko'])){{$riskScore['gojek_lazada_toko']}}@elseif(empty($riskScore['gojek_lazada_toko'])) +0 @endif</span>@lang('final.gojek.lazada.toko')</li>
                                <li><span class="score-success">@if(!empty($riskScore['fabk_inst'])){{$riskScore['fabk_inst']}}@elseif(empty($riskScore['fabk_inst'])) +0 @endif</span>@lang('final.fabk.inst')</li>
                                <li><span class="score-success">@if(!empty($riskScore['threeDegree'])){{$riskScore['threeDegree']}}@elseif(empty($riskScore['threeDegree'])) +0 @endif</span>@lang('final.threeDegree')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--npwp-telkomsel end-->

                <!--电商 start-->
                <div class="custom-section-title-square bottom-line margin-top-10 npwp-section-large">@lang('final.tell.score')
                    <span class="pull-right">
                    <i class="plus">@if($riskScore['onlineretailers+'])+{{$riskScore['onlineretailers+']}}@else 0 @endif</i>
                    <i>({{$riskScore['onlineretailers-']}})</i>
                </span>
                </div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                GOJEK
                                <span class="pull-right">
                                    <i class="plus">@if($riskScore['gojekAdd+'])+{{$riskScore['gojekAdd+']}}@else +0 @endif</i>
                                    <i>({{$riskScore['gojekAdd-']}})</i>
                                </span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="gojek" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['sub_gojek'])){{$riskScore['sub_gojek']}}@elseif(empty($riskScore['sub_gojek'])) 0 @endif</span>@lang('final.is.prioty')gojek</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['gojek_tel'])){{$riskScore['gojek_tel']}}@endif</span>@lang('final.is.same.phone')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['gojek_email'])){{$riskScore['gojek_email']}}@endif</span>@lang('final.is.same.email')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['gojek_point'])){{$riskScore['gojek_point']}}@endif</span>@lang('final.sum.to.score')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['gojek_number'])){{$riskScore['gojek_number']}}@endif</span>@lang('final.get.order.or.score')> 0</td></tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                LAZADA
                                <span class="pull-right">
                                    <i class="plus">@if($riskScore['lazadalAdd+'])+{{$riskScore['lazadalAdd+']}}@else +0 @endif</i>
                                    <i>({{$riskScore['lazadalAdd-']}})</i>
                                </span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="lazada" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['sub_lazada'])){{$riskScore['sub_lazada']}}@endif</span>@lang('final.is.prioty')lazada</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['lazada_name'])){{$riskScore['lazada_name']}}@endif</span>@lang('final.is.same.name')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['lazada_tel'])){{$riskScore['lazada_tel']}}@endif</span>@lang('final.is.same.phone')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['lazada_address'])){{$riskScore['lazada_address']}}@endif</span>@lang('final.given.address')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['lazada_email'])){{$riskScore['lazada_email']}}@endif</span>@lang('final.is.same.email')</td></tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                TOKOPEDIA
                                <span class="pull-right">
                                    <i class="plus">@if($riskScore['tokopediaAdd+'])+{{$riskScore['tokopediaAdd+']}}@else +0 @endif</i>
                                    <i>({{$riskScore['tokopediaAdd-']}})</i>
                                </span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="tokopedia" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['tokopedia_name'])){{$riskScore['sub_tokopedia']}}@endif</span>@lang('final.is.prioty')tokopedia</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['tokopedia_email'])){{$riskScore['tokopedia_name']}}@endif</span>@lang('final.is.same.name')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['tokopedia_address'])){{$riskScore['tokopedia_tel']}}@endif</span>@lang('final.is.same.phone')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['sub_tokopedia'])){{$riskScore['tokopedia_address']}}@endif</span>@lang('final.given.address')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['tokopedia_tel'])){{$riskScore['tokopedia_email']}}@endif</span>@lang('final.is.same.email')</td></tr>
                        </tbody>
                    </table>
                </div>
                <div style="clear:both; padding-top: 10px"></div>
                <!--电商end-->

                <!--Facebook/Intagram start-->
                <div class="custom-section-title-square bottom-line margin-top-10 npwp-section-large">@lang('final.social')
                    <span class="pull-right">
                    <i class="plus">@if($riskScore['socialcontact+'])+{{$riskScore['socialcontact+']}}@else +0 @endif</i>
                    <i>({{$riskScore['socialcontact-']}})</i>
                </span>
                </div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                Facebook
                                <span class="pull-right">
                                    <i class="plus">@if($riskScore['facebookAdd+'])+{{$riskScore['facebookAdd+']}}@else 0 @endif</i>
                                    <i>({{$riskScore['facebookAdd-']}})</i>
                                </span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="facebook" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['sub_facebook'])){{$riskScore['sub_facebook']}}@endif</span>@lang('final.is.prioty')facebook</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['facebook_work'])){{$riskScore['facebook_work']}}@endif</span>@lang('final.work.address.match')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['facebook_tel'])){{$riskScore['facebook_tel']}}@endif</span>@lang('final.is.same.phone')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['facebook_last_login'])){{$riskScore['facebook_last_login']}}@endif</span>@lang('final.last.login.address')</td></tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4" style="padding-right: 0px;">
                    <table class="custom-table-trial custom-table-info">
                        <tbody>
                        <tr>
                            <th>
                                Instagram
                                <span class="pull-right">
                                    <i class="plus">@if($riskScore['instagramAdd+'])+{{$riskScore['instagramAdd+']}}@else 0 @endif</i>
                                    <i>({{$riskScore['instagramAdd-']}})</i>
                                </span>
                                <span class="pull-right"><a href="javascript:;" class="partner-detail" data-uid="{{ $order->uid }}" data-partner="instagram" data-toggle="modal" data-target="#myModal">@lang('final.click.select.detail')</a></span>
                            </th>
                        </tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['sub_instagram'])){{$riskScore['sub_instagram']}}@endif</span>@lang('final.is.prioty')instagram</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['instagram _tel'])){{$riskScore['instagram _tel']}}@endif</span>@lang('final.is.same.phone')</td></tr>
                        <tr><td><span class="score-success">@if(!empty($riskScore['instagram _number'])){{$riskScore['instagram _number']}}@endif</span>@lang('final.same.contact.sum')0</td></tr>
                        </tbody>
                    </table>
                </div>
                <!--Facebook/Intagram end-->
                <div style="clear:both; padding-top: 10px"></div>
            </div>
        </section>
        <section class="custom-panel">
            <header class="custom-heading sub-title-final">@lang('final.repayment.wish.alay')<img src="/images/big-triangle.png" alt="">
                <span class="pull-right">@lang('final.crossScore'):
                <i class="plus">@if(!empty($riskScore['willingnessAdd+']))+{{$riskScore['willingnessAdd+']}} @else 0 @endif</i>
                {{--<i class="plus">@if(!empty($riskScore['total']))+{{$riskScore['ab_keywords']+$riskScore['ab_num']+$riskScore['has_parent']+$riskScore['sms_ab']+$riskScore['person_call']+$riskScore['ab_call'] + $riskScore['sms_pay_off']+$riskScore['sms_received']+$riskScore['sms_ok_pay']+$riskScore['sms_keyword'] + $riskScore['app_loan_keyword']+$riskScore['app_not_keywords'] + $riskScore['sub_call_bank'] + $riskScore['othrt_cards']}}@endif</i>--}}
                <i>({{$riskScore['willingnessAdd-']}})</i>
            </span>
            </header>
            <!--还款意愿分析start-->
            <div class="custom-section three-sides">
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.other.card.big.three')<span class="pull-right"><i class="plus">@if(!empty($riskScore['otherCardBigthree+'])){{$riskScore['otherCardBigthree+']}}@endif </i> <i>({{$riskScore['otherCardBigthree-']}})</i></span></li>
                                <li><span class="score-success">@if(!empty($riskScore['othrt_cards'])){{$riskScore['othrt_cards']}}@endif</span>@lang('final.other.image')</li>
                            </ul>
                        </div>

                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.message.keyword.same')<span class="pull-right"><i class="plus">@if(!empty($riskScore['messageKeyWordSame+']))+{{$riskScore['messageKeyWordSame+']}}@else +0 @endif</i> <i>({{$riskScore['messageKeyWordSame-']}})</i></span></li>
                                <li><span class="score-success">@if(!empty($riskScore['sms_pay_off'])){{$riskScore['sms_pay_off']}}@endif</span>SMS @lang('final.has.final')</li>
                                <li><span class="score-success">@if(!empty($riskScore['sms_received'])){{$riskScore['sms_received']}}@endif</span>SMS @lang('final.has.receive.loan')</li>
                                <li><span class="score-success">@if(!empty($riskScore['sms_ok_pay'])){{$riskScore['sms_ok_pay']}}@endif</span>SMS @lang('final.has.payment')</li>
                                <li><span class="score-success">@if(!empty($riskScore['sms_keyword'])){{$riskScore['sms_keyword']}}@endif</span>SMS @lang('final.not.include.keyword')</li>
                                <li><span class="score-success">@if(!empty($riskScore['overdue_sms'])){{$riskScore['overdue_sms']}} @else +0 @endif</span>SMS @lang('final.overdue_sms')</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.apply.phone.same.bank')<span class="pull-right"><i class="plus">@if(!empty($riskScore['sub_call_bank'])){{$riskScore['sub_call_bank']}}@endif</i> <i>({{$riskScore['applyPhoneSameBank-']}})</i></span></li>
                                <li><span class="score-success">@if(!empty($riskScore['sub_call_bank'])){{$riskScore['sub_call_bank']}}@endif</span> @lang('final.apply.phone.same.bank')</li>
                            </ul>
                        </div>

                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.key.word')<span class="pull-right"><i class="plus">@if(!empty($riskScore['sub_call_bank']))+{{$riskScore['app_loan_keyword']+$riskScore['app_not_keywords']}}@endif</i> <i>({{$riskScore['finalKeywords-']}})</i></span></li>
                                <li><span class="score-success">@if(!empty($riskScore['app_loan_keyword'])){{$riskScore['app_loan_keyword']}}@endif</span>@lang('final.app.relative.to')</li>
                                <li><span class="score-success">@if(!empty($riskScore['app_not_keywords'])){{$riskScore['app_not_keywords']}}@endif</span>@lang('final.app.not.include')</li>
                            </ul>
                        </div>
                        {{--财富与现金贷Start--}}
                           <div class="npwp-section">
                                <ul>
                                    <li class="npwp-title">@lang('final.wealth.cash.loans')<span class="pull-right"><i class="plus">@if(!empty($riskScore['wealthCashloans+']))+{{$riskScore['wealthCashloans+']}} @else  +0 @endif </i> <i>({{$riskScore['wealthCashloans-']}})</i></span></li>
                                    <li><span class="score-success">@if(!empty($riskScore['sms_contains_cash'])){{$riskScore['sms_contains_cash']}} @else  +0 @endif</span>@lang('final.sms.contains.cash.loan')</li>
                                    <li><span class="score-success">@if(!empty($riskScore['loan_success'])){{$riskScore['loan_success']}} @else  +0 @endif</span>@lang('final.loan.success')</li>
                                    <li><span class="score-success">@if(!empty($riskScore['sa_loan'])){{$riskScore['sa_loan']}} @else  +0 @endif</span>@lang('final.sms.app.cash.loan')</li>
                                    <li><span class="score-success">@if(!empty($riskScore['inCome_whether_wages'])){{$riskScore['inCome_whether_wages']}} @else  +0 @endif</span>@lang('final.income.whether.submit.wages')</li>
                                    <li><span class="score-success">@if(!empty($riskScore['inCome_loan_amount'])){{$riskScore['inCome_loan_amount']}} @else  +0 @endif</span>@lang('final.income.loan.amount')</li>
                                    <li><span class="score-success">@if(!empty($riskScore['oldUser_applyAmount'])){{$riskScore['oldUser_applyAmount']}} @else  +0 @endif</span>@lang('final.oldUser.applyAmount')</li>
                                    <li><span class="score-success">@if(!empty($riskScore['amount_reminder'])){{$riskScore['amount_reminder']}} @else  +0 @endif</span>@lang('final.amount.reminder')</li>
                                </ul>
                            </div>
                        {{--财富与现金贷End--}}
                    </div>
                    <div class="col-md-4">
                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.contact.record.mseg')<span class="pull-right"><i class="plus">@if(!empty($riskScore['contactRecordMseg+']))+{{$riskScore['contactRecordMseg+']}}@endif</i> <i>({{$riskScore['contactRecordMseg-']}})</i></span></li>
                                <li><span class="score-success">@if(!empty($riskScore['ab_keywords'])){{$riskScore['ab_keywords']}}@endif</span>@lang('final.contact.small.loan.three')</li>
                                <li><span class="score-success">@if(!empty($riskScore['ab_num'])){{$riskScore['ab_num']}}@endif</span>@lang('final.contact.phone.number.big')</li>
                                <li><span class="score-success">@if(!empty($riskScore['has_parent'])){{$riskScore['has_parent']}}@endif</span>@lang('final.one.of.is.parent')</li>
                                <li><span class="score-success">@if(!empty($riskScore['sms_ab'])){{$riskScore['sms_ab']}}@endif</span>@lang('final.message.name.big.three')</li>
                                <li><span class="score-success">@if(!empty($riskScore['person_call'])){{$riskScore['person_call']}}@endif</span>@lang('final.contact.phone.number.rate')</li>
                                <li><span class="score-success">@if(!empty($riskScore['ab_call'])){{$riskScore['ab_call']}}@endif</span>@lang('final.more.than.five.person')</li>
                                <li><span class="score-success">@if(!empty($riskScore['sms_concat'])){{$riskScore['sms_concat']}}@endif</span>@lang('final.sms_concat')</li>
                                <li><span class="score-success">@if(!empty($riskScore['ab_concat'])){{$riskScore['ab_concat']}}@endif</span>@lang('final.ab_concat')</li>
                                <li><span class="score-success">@if(!empty($riskScore['callog_addressbook'])){{$riskScore['callog_addressbook']}}@else +0 @endif</span>@lang('final.callog_addressbook')</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--还款意愿分析end-->

        <section class="custom-panel">
            <header class="custom-heading sub-title-final">@lang('final.user.model')<img src="/images/big-triangle.png" alt="">
                <span class="pull-right">@lang('final.win.user.model'):
                <i class="plus">@if(!empty($riskScore['modelAdd+']))+{{$riskScore['modelAdd+']}}@else +0 @endif</i>
                <i>({{$riskScore['modelAdd-']}})</i>
            </span>
            </header>
            <!--用户模型分析 start-->
            <div class="custom-section three-sides">
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="npwp-section">
                            <ul>
                                <li class="npwp-title">@lang('final.win.user.model')<span class="pull-right"><i class="plus">@if(!empty($riskScore['modelAdd+']))+{{$riskScore['modelAdd+']}}@else +0 @endif</i> <i>({{$riskScore['modelAdd-']}})</i></span></li>
                                <li><span class="score-success">@if(!empty($riskScore['marry'])){{$riskScore['marry']}}@elseif(empty($riskScore['marry'])) +1 @endif</span>@lang('message.phone.ask.personal.marriage'): {{$phoneTrial->user->marriage}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['sex'])){{$riskScore['sex']}}@elseif(empty($riskScore['sex'])) +1 @endif</span>@lang('basic.sex'): @if($userBasicInfo->gender==1) @lang('basic.male') @elseif($userBasicInfo->gender==2) @lang('basic.female') @else @lang('basic.unkown') @endif</li>
                                <li><span class="score-fail">@if(!empty($riskScore['age'])){{$riskScore['age']}}@elseif(!empty($riskScore['age'])) +1 @endif</span>@lang('basic.age'): {{$userBasicInfo->birth}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['purpose'])){{$riskScore['purpose']}}@elseif(!empty($riskScore['purpose'])) +1 @endif</span>@lang('message.phone.ask.personal.pay.type'): {{$order->pay_type}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['work'])){{$riskScore['work']}}@elseif(empty($riskScore['work'])) +1 @endif</span>@lang('message.RC_REFUSE_CHECK_RISK_OCCUPATION'): {{$phoneTrial->user->career}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['degree'])){{$riskScore['degree']}}@elseif(empty($riskScore['degree'])) +1 @endif</span>@lang('basic.collage'): {{$userBasicInfo->education}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['live_lenght'])){{$riskScore['live_lenght']}}@endif</span>@lang('basic.live.period'): {{$userBasicInfo->living_period}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['work_length'])){{$riskScore['work_length']}}@endif</span>@lang('message.RC_REFUSE_CHECK_WORKING_LIFE'): {{$phoneTrial->user->work_period}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['income'])){{$riskScore['income']}}@endif</span>@lang('final.income'): {{$phoneTrial->user->income_level}}</li>
                                <li><span class="score-fail">@if(!empty($riskScore['spouse'])){{$riskScore['spouse']}}@else +0 @endif</span>@lang('final.spouse')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--用户模型分析end-->

        </section>
    </div>
    <div style="clear: both;" id="device-trial"></div>
</div>
