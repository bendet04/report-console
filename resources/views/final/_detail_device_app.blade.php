
@if($data['result_code']==10000&&!empty($data['data']))
    <div class="custom-section-title-square">APP @lang('message.trial')
        <span class="red">({{$data['data']['total']}})</span>
        <img src="/images/small-triangle.png.png" alt="">
    </div>
    <div class="panel-body  panel-body-no-padding">
        <section class="panel section-panel-no-margin">
            <header class="panel-heading custom-tab custom-tab-33">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#app1" data-toggle="tab" aria-expanded="true">@lang('final.total.download.sum')<br />@if(!empty($data['data']['total'])) {{$data['data']['total']}} @else 0 @endif </a>
                    </li>
                    <li class="">
                        <a href="#app2" data-toggle="tab" aria-expanded="false">@lang('final.relative.to.loan')<br />@if(!empty($data['data']['loan'])) {{count($data['data']['loan'])}} @else 0 @endif</a>
                    </li>
                </ul>
            </header>
            <div style="clear:both;"></div>
            <div class="panel-body custom-panel-body1">
                <div class="tab-content">
                    <div class="tab-pane custom-tab-pane active" id="app1">
                        @if(!empty($data['data']['all']))
                        @foreach($data['data']['all'] as $k=>$v)
                            @if(!empty($v['appName']))
                                <div class="app-content" title="{{$v['appName']}}">{{substr($v['appName'],0,15)}}</div>
                            @endif
                        @endforeach
                        @endif
                    </div>
                    <div class="tab-pane custom-tab-pane" id="app2">
                        @foreach($data['data']['loan'] as $k=>$v)
                            @if(!empty($v['appName']))
                                <div class="app-content" title="{{$v['appName']}}">{{substr($v['appName'],0,15)}}</div>
                            @endif
                        @endforeach
                        {{--<div class="app-content">TangBull</div>--}}
                        {{--<div class="app-content">WhatsApp</div>--}}
                        {{--<div class="app-content">Line</div>--}}
                        {{--<div class="app-content">Abang Bintang</div>--}}
                        {{--<div class="app-content">TangBull</div>--}}
                        {{--<div class="app-content">WhatsApp</div>--}}
                        {{--<div class="app-content">Line</div>--}}
                        {{--<div class="app-content">Abang Bintang</div>--}}
                    </div>


                </div>
            </div>
        </section>
    </div>
    {{--</>--}}
@endif
<!--app审核end-->