<!--短信-->
@if($data['result_code']==10000&&!empty($data['data']))
<div class="custom-section-title-square">@lang('final.message.trial')
    <span class="red">({{$data['data']['total']}})</span>
    <img src="/images/small-triangle.png.png" alt="">
</div>
<div class="panel-body panel-body-no-padding">
    <section class="panel section-panel-no-margin">
        <header class="panel-heading custom-tab">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#whitelist" data-toggle="tab" aria-expanded="true">@lang('final.white.list')<br />
                        @if(!empty($data['data']['wtitelist'])){{count($data['data']['whitelist'])}} @else 0 @endif

                    </a>
                </li>

                <li class="">
                    <a href="#blacklist" data-toggle="tab" aria-expanded="false">@lang('final.black.list')<br />
                    @if(!empty($data['data']['blacklist'])){{count($data['data']['blacklist'])}} @else 0 @endif
                    </a>
                </li>
                <li class="">
                    <a href="#refund" data-toggle="tab" aria-expanded="false">@lang('final.repaymtn.relation')<br />
                        @if(!empty($data['data']['refund'])){{count($data['data']['refund'])}} @else 0 @endif
                    </a>
                </li>
                <li class="">
                    <a href="#overdue" data-toggle="tab" aria-expanded="false">@lang('final.ovedue.relation')<br />
                        @if(!empty($data['data']['overdue'])){{count($data['data']['overdue'])}} @else 0 @endif
                    </a>
                </li>
                <li class="">
                    <a href="#concat" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.relation')<br />
                        @if(!empty($data['data']['concat'])){{count($data['data']['concat'])}} @else 0 @endif
                    </a>
                </li>

                <li class="">
                    <a href="#call_log" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.history')<br />
                        @if(!empty($data['data']['call_log'])){{count($data['data']['call_log'])}} @else 0 @endif
                    </a>
                </li>
                @if(isset($data['data']['addressbook'])&&!empty($data['data']['addressbook']))

                <li class="">
                    <a href="#addressbook" data-toggle="tab" aria-expanded="false">@lang('final.same.as.phone.record')<br />
                        @if(!empty($data['data']['addressbook'])){{count($data['data']['addressbook'])}} @else 0 @endif
                    </a>
                </li>
                @endif
                <li class="">
                    <a href="#loan" data-toggle="tab" aria-expanded="false">@lang('final.relative.to.loan')<br />
                        @if(!empty($data['data']['loan'])){{count($data['data']['loan'])}} @else 0 @endif
                    </a>
                </li>


            </ul>
        </header>
        <div style="clear:both;"></div>
        <div class="panel-body custom-panel-body1">
            <div class="tab-content">
                <div class="tab-pane custom-tab-pane active " id="blacklist">
                    @if(!empty($data['data']['blacklist']))
                        @foreach($data['data']['blacklist'] as $k=>$v)

                            <div class="col-lg-3">{{$v['mobile']}}</div>
                            <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>

                            <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                        @endforeach
                    @endif
                    {{--<div class="col-lg-3">Facebook</div>--}}
                    {{--<div class="col-lg-9">2018-03-11 00:58:29</div>--}}
                    {{--<div class="col-lg-12 tab-pane-content">365226 adalah kode Anda untuk MiniRupiah.</div>--}}
                </div>
                <div class="tab-pane custom-tab-pane " id="whitelist">
                    @if(!empty($data['data']['whitelist']))
                        @foreach($data['data']['whitelist'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))
                            <div class="col-lg-3">{{$v['mobile']}}</div>
                            <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                            <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                            @endif
                        @endforeach
                    @endif
                    {{--<div class="col-lg-3">Tokiovips</div>--}}
                    {{--<div class="col-lg-9">2018-05-08 12:34:43</div>--}}
                    {{--<div class="col-lg-12 tab-pane-content">Hi~blablabla</div>--}}

                </div>
                <div class="tab-pane custom-tab-pane " id="refund">

                    @if(!empty($data['data']['refund']))
                        @foreach($data['data']['refund'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))
                             <div class="col-lg-3">{{$v['mobile']}}</div>
                            <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                            <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                            @endif
                        @endforeach
                    @endif

                </div>
                <div class="tab-pane custom-tab-pane" id="overdue">
                    @if(!empty($data['data']['overdue']))
                        @foreach($data['data']['overdue'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))
                              <div class="col-lg-3">{{$v['mobile']}}</div>
                            <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                            <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                            @endif
                        @endforeach
                    @endif


                </div>
                <div class="tab-pane custom-tab-pane " id="concat">

                    @if(!empty($data['data']['concat']))
                        @foreach($data['data']['concat'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))
                                <div class="col-lg-3">{{$v['mobile']}}</div>
                                <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                                <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>

                            @endif
                        @endforeach
                    @endif

                </div>
                <div class="tab-pane custom-tab-pane " id="call_log">

                    @if(!empty($data['data']['call_log']))
                        @foreach($data['data']['call_log'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))
                                <div class="col-lg-3">{{$v['mobile']}}</div>
                                <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                                <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>

                            @endif
                        @endforeach
                    @endif

                </div>
                <div class="tab-pane custom-tab-pane" id="addressbook">
                    @if(!empty($data['data']['addressbook']))
                        @foreach($data['data']['addressbook'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))
                             <div class="col-lg-3">{{$v['mobile']}}</div>
                            <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                            <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                            @endif
                        @endforeach
                    @endif


                </div>
                <div class="tab-pane custom-tab-pane" id="loan">
                    @if(!empty($data['data']['loan']))
                        @foreach($data['data']['loan'] as $k=>$v)
                            @if(!empty($v['mobile'])&&!empty($v['date'])&&!empty($v['body']))

                                <div class="col-lg-3">{{$v['mobile']}}</div>
                                <div class="col-lg-9">{{ date('Y-m-d H:i:s',intval($v['date']/1000))}}</div>
                                <div class="col-lg-12 tab-pane-content">{{$v['body']}}</div>
                            @endif
                        @endforeach
                    @endif

                </div>

            </div>
        </div>
    </section>
</div>
@endif
<!--短信审核end-->