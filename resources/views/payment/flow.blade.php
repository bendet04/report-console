<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('payment.payment.flow')</th>
                <th>@lang('basic.result')</th>
                <th title="银行返回Code">CODE</th>
                <th>@lang('basic.cretate.time')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flows as $k=>$v)
            <tr class="">
                <td>{{ $k + 1 }}</td>
                <td>{{ $v->payment_flow }}</td>
                <td>{{ $v->payment_status }}</td>
                <td>{{ $v->payment_code }}</td>
                <td>{{ $v->created_at }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>