{{--<div style="width:500px;margin:0 auto;">--}}
<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 145px;line-height: 45px;">

    <form id="default" class="form-horizontal" action="/payment/paymenttrial" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset title="Initial Info">
            {{--平台名称--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('basic.platform')</td>
                        <td>
                            <select class="form-control" name="platform">
                                <option value="">@lang('basic.all.platform')</option>
                                @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                    <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            {{--序号--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.order.number')</td>
                        <td><input type="text" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--客户姓名--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('basic.client.name')</td>
                        <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <br/>
            {{--<div class="form-group">--}}
                {{--<label class="col-md-3 col-sm-2 control-label">@lang('basic.client.name') </label>--}}
                {{--<div class="col-md-7 col-sm-6">--}}
                    {{--<input type="text" placeholder="Full Name" name="name" class="form-control">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--KTP--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >KTP</td>
                        <td><input type="text" placeholder="Ktp number" name="ktp_number" @if(!empty($pageCondition['ktp_number'])) value="{{$pageCondition['ktp_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>

            {{--<div class="form-group">--}}
                {{--<label class="col-md-3 col-sm-2 control-label">KTP </label>--}}
                {{--<div class="col-md-7 col-sm-6">--}}
                    {{--<input type="text" name="ktp_number" class="form-control">--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.status')</td>
                        <td>
                            <select class="form-control" name="order_status">
                                <option></option>
                                <option value="6" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='6') selected="selected" @endif>@lang('payment.wait.for.trail')</option>
                                <option value="8" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='8') selected="selected" @endif>@lang('status.status8')</option>
                                <option value="14" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='14') selected="selected" @endif>@lang('status.wait.repayment')</option>
                                <option value="15" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='15') selected="selected" @endif>@lang('status.payment_failure')</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            {{--<div class="form-group">--}}
                {{--<label class="col-md-3 col-sm-2 control-label">@lang('message.status')</label>--}}
                {{--<div class="col-md-7 col-sm-6">--}}
                    {{--<select class="form-control m-bot15" name="order_status">--}}
                        {{--<option></option>--}}
                        {{--<option value="6">@lang('payment.wait.for.trail')</option>--}}
                        {{--<option value="8">@lang('status.status8')</option>--}}
                        {{--<option value="14">@lang('status.wait.repayment')</option>--}}
                        {{--<option value="15">@lang('status.payment_failure')</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
        </fieldset>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.view')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>

        <div style="clear: both"> </div>
    </form>
</header>
 <script>
        //重置检索条件
        function rese() {
            $(" input[ type='text' ] ").val('');
            $(" select").find('option').attr("selected", false);

        }
 </script>