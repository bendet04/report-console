<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('payment/_payment_add')
        <div class="wrapper">
            @include('payment/_checkpaymentflow_check')
            @include('payment/_checkpaymentflow_list')
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 流水的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.payment.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 流水的结束 -->

<!--银行信息查询的开始-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bankinfo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.bank.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<!--银行信息查询的结束-->

@include('public/base_script')

<script type="text/javascript">

    $('.user-bank').click(function(){
        $('#bankinfo .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/payment/bank',
            data:{
                order_id: id,
                only_view: 1
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#bankinfo .modal-body').html(json);
        }).error(function(){
            alert("@lang('basic.request.fail')");
        })
    });

    $('.flow-detail').click(function(){
        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/payment/flow',
            data:{
                order_id: id,
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#myModal .modal-body').html(json);
        }).error(function(){
            alert("@lang('basic.request.fail')");
        })
    });


</script>
</body>
</html>