<section class="panel">
    <div class="panel-body">
        <div class="col-md-3 col-sm-6 diy-search-input-box">
            <select class="form-control diy-search-input" id="searchId">
                <option value="">select</option>

                <option value="order_number">{{__('final.number')}}</option>
                {{--<option value="platform">{{__('final.platform')}}</option>--}}
                <option value="name">{{__('final.clientName')}}</option>
                <option value="ktp_number">{{__('final.ktp')}}</option>
                <option value="loan_period">{{__('final.payPeriod')}}</option>
                <option value="apply_amount">{{__('final.applyAmount')}}</option>
                <option value="created_at">{{__('final.applyTime')}}</option>
                <option value="order_status">{{__('message.status')}}</option>
            </select>
        </div>
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf"  id="dynamic-table">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>{{__('final.number')}}</th>
                    <th>@lang('basic.platform')</th>
                    <th class="numeric">{{__('final.clientName')}}</th>
                    <th class="numeric">{{__('final.applyAmount')}}</th>
                    <th class="numeric">KTP</th>
                    <th class="numeric">{{__('final.applyTime')}}</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('message.select')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $k=>$avalue)
                <tr>
                    <td>{{ $k+1 }}</td>
                    <td>{{ $avalue->order_number }}</td>
                    <td>{{ \App\Consts\Platform::alias($avalue->platform) }}</td>
                    <td class="numeric">{{ $avalue->name }}</td>
                    <td class="numeric">{{ $avalue->apply_amount }}</td>
                    <td class="numeric">{{ $avalue->ktp_number }}</td>
                    <td class="numeric">{{ $avalue->created_at }}</td>
                    <td class="numeric">
                        {{ $avalue->order_status_str }}
                    </td>
                    <td class="numeric">
                        <a data-id="{{ $avalue->id }}" class="user-bank" style="cursor: pointer" data-toggle="modal" data-target="#bankinfo">@lang('message.select')</a>
                    </td>
                    <td class="numeric">
                        <div>
                            @if ($avalue->order_status==6 || $avalue->order_status==15)
                                <a  data-id="{{ $avalue->id }}" class="trial-order" style="cursor: pointer">@lang('message.trial')</a>&nbsp;
                                <a  data-id="{{ $avalue->id }}" class="cancel-order" style="cursor: pointer">|&nbsp;@lang('payment.cancel.order')</a>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <div class="diy-paginate">
                <form action="/payment/paymenttrial" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">{{ $orders->appends($pageCondition)->render() }} </div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
        </section>
    </div>
</section>
