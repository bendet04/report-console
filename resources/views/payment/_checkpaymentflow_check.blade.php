<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:110px;
    }
    .col-md-4 table{
        width:105%;
    }
    .ldx{
        margin-top: -8px;
    }
</style>
<header class="" style="height: 150px;line-height: 45px;">
    <form id="default" class="form-horizontal" action="/payment/checkpaymentflow" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset title="Initial Info">
            {{--还款开始时间与结束时间--}}
            <div class="col-md-4">
                <table class="ldx">
                    <tr>
                        <td class="input-group-addon" >@lang('basic.date')</td>
                        <td><input type="text" placeholder="Start-time" autocomplete="off" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                        <td ><span>⇆</span></td>
                        <td><input type="text" placeholder="End-time" autocomplete="off" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                    </tr>
                </table>

                {{--</div>--}}
            </div>
            {{--平台--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('basic.platform')</td>
                        <td>
                            <select class="form-control" name="platform">
                                <option value="">@lang('basic.all.platform')</option>
                                @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                    <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            {{--订单编号--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('final.number')</td>
                        <td><input type="text" placeholder="M20180XXXXXXX" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <br/>

            {{--客户姓名--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('basic.client.name')</td>
                        <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--KTP--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >KTP</td>
                        <td><input type="text" placeholder="Ktp number" name="ktp_number" @if(!empty($pageCondition['ktp_number'])) value="{{$pageCondition['ktp_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.status')</td>
                        <td>
                            <select class="form-control" name="order_status">
                                <option></option>
                                <option value="8" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='8') selected="selected" @endif>@lang('status.status8')</option>
                                <option value="14" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='14') selected="selected" @endif>@lang('payment.wait.for.trail')</option>
                                <option value="15" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='15') selected="selected" @endif>@lang('status.payment_failure')</option>
                                <option value="12" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='12') selected="selected" @endif>@lang('status.loan.finish')</option>
                                <option value="13" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='13') selected="selected" @endif>@lang('status.loan.partly')</option>
                                <option value="11" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='11') selected="selected" @endif>@lang('status.loan.overdue')</option>
                                <option value="15" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='15') selected="selected" @endif>@lang('payment.payment.stop')</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>


        </fieldset>
        <div class="col-md-1 col-sm-1" style="margin-bottom: 15px; width:10%;margin-left:110px">
            <button type="submit" class="btn btn-info form-control">@lang('message.select')</button>
        </div>
        {{--//重置选项--}}
        <div class="col-md-1 col-xs-1 " style="tmargin-bottom: 15px">
            <input type="button" class="btn btn-info form-control" onclick="rese()" value="@lang('message.reset')">
        </div>
        <div style="clear: both"> </div>
    </form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>
