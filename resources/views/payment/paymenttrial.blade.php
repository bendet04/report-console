<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('payment/_payment_add')
        <div class="wrapper">
            @include('payment/_payment_check')
            @include('payment/_payment_list')
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 流水的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.payment.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 流水的结束 -->

<!--银行信息查询的开始-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bankinfo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.bank.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<!--银行信息查询的结束-->

@include('public/base_script')

<script type="text/javascript">

    //审核按钮
    $('.trial-order').click(function(){
        var that = this;
        $(this).parent().hide();
        $.confirm({
            title: '放款提示',
            content: '确定要放款么？',
            buttons: {
                confirm: {
                    text: '确认',
                    action: function() {
                        var orderId = $(that).attr('data-id');
                        $.ajax({
                            url:"/payment/payment",
                            type:"POST",
                            data:{
                                'order_id': orderId,
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            success:function(data){
                                if(data.info!='交易在处理中') {
                                    $.alert({
                                        title: '放款结果',
                                        content: data.info,
                                        buttons: {
                                            okay: {
                                                text: 'OK',
                                                btnClass: 'btn-info'
                                            }
                                        }
                                    });
                                }
                            },
                            error:function(){
                                $.alert({
                                    title: "@lang('payment.payment.result')",
                                    content: "@lang('basic.payment.request.fail')",
                                    buttons: {
                                        okay: {
                                            text: 'OK',
                                            btnClass: 'btn-info'
                                        }
                                    }
                                });
                            }
                        });
                    }
                },
                cancel: {
                    text: "@lang('message.cancel')",
                    btnClass: 'btn-info',
                    action: function () {
                        $(that).parent().show();
                    }
                }
            }
        });
    });

    //取消订单
    $('.cancel-order').click(function(){
        var that = this;
        $.confirm({
            title: "@lang('payment.cancel.order')",
            content: "@lang('payment.make.sure.cancel.order')",
            buttons: {
                orderFail: {
                    text: "@lang('final.final.failure')",
                    action: function() {
                        var orderId = $(that).attr('data-id');
                        $.ajax({
                            url:"/payment/cancel",
                            type:"POST",
                            data:{
                                'order_id': orderId,
                                'status':1,//1:放款审核未通过 2:终止放款
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            success:function(data){
                                $(that).parent().hide();
                            },
                            error:function(){
                                $.alert({
                                    title: "@lang('payment.cancel.order')",
                                    content: "@lang('basic.request.fail')",
                                    buttons: {
                                        okay: {
                                            text: 'OK',
                                            btnClass: 'btn-info'
                                        }
                                    }
                                });
                            }
                        });
                    }
                },
                orderEnd: {
                    text: "@lang('payment.cancel.order')",
                    action: function() {
                        var orderId = $(that).attr('data-id');
                        $.ajax({
                            url:"/payment/cancel",
                            type:"POST",
                            data:{
                                'order_id': orderId,
                                'status':2,//1:放款审核未通过 2:终止放款
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            success:function(data){
                                $(that).parent().hide();
                            },
                            error:function(){
                                $.alert({
                                    title: "@lang('basic.request.fail')",
                                    content: "@lang('basic.request.fail')",
                                    buttons: {
                                        okay: {
                                            text: 'OK',
                                            btnClass: 'btn-info'
                                        }
                                    }
                                });
                            }
                        });
                    }
                },
                cancel: {
                    text:  "@lang('message.cancel')",
                    btnClass: 'btn-info'
                }
            }
        });
    });



    $('.user-bank').click(function(){
        $('#bankinfo .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/payment/bank',
            data:{
                order_id: id
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#bankinfo .modal-body').html(json);
        }).error(function(){
            alert("@lang('payment.bank.info.failure')");
        })
    });



</script>
</body>
</html>