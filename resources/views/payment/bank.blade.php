<form id="default" class="form-horizontal" action="/payment/bank" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset title="Initial Info">
        @if(!empty($userBankInfo))
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('basic.client.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->name }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">Phone</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->phone_number }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <input type="hidden" name="id" value="{{ $userBankInfo->id }}">
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.payment.bank')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->bank_name }}" class="form-control" @if($only_view) disabled="true" @endif name="bankName">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.bank.card')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->bank_number }}" class="form-control" @if($only_view) disabled="true" @endif  name="bankNumber">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.bank.card.phone')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->bank_phone }}" class="form-control"  @if($only_view) disabled="true" @endif name="bankPhone">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.payee.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->payee_name }}" class="form-control" @if($only_view) disabled="true" @endif name="payeeName">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('basic.apply.amount')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->loan_principal_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.to.hand.amount')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $userBankInfo->reciprocal_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
            @if(empty($userBankInfo->bank_code))
            @else
            <div class="form-group">
                <label class="col-md-3 col-sm-2 control-label">@lang('payment.failure.reason')</label>
                <div class="col-md-6 col-sm-6">
                    <input type="text" value="{{ $userBankInfo->bank_code }}" class="form-control"  disabled="true" style="border:none;">
                </div>
            </div>
            @endif
        @endif
        <div class="modal-footer">
            @if(empty($only_view))<button type="submit" class="btn btn-info finish" name="sub">@lang('message.update')</button>@endif
            <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
        </div>
    </fieldset>
</form>