<section class="panel">
    <div class="panel-body">
        <div class="col-md-3 col-sm-6 diy-search-input-box">
            <select class="form-control diy-search-input" id="searchId">
                <option value="">select</option>
                <option value="order_number">@lang('message.order.number')</option>
                <option value="name">@lang('basic.client.name')</option>
                <option value="pay_type">@lang('basic.product.type')</option>
                <option value="apply_amount">@lang('basic.apply.amount')</option>
                <option value="order_time">@lang('basic.apply.time')</option>
                <option value="order_status">@lang('message.status')</option>
                <option value="admin_id">@lang('task.taskInfo.trial.person')</option>
            </select>
        </div>
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf"  id="dynamic-table">
                <thead class="cf">
                <tr>
                    <th><input type="checkbox" name="selectall"></th>
                    <th>@lang('message.number')</th>
                    <th>@lang('message.order.number')</th>
                    <th class="numeric">@lang('basic.client.name')</th>
                    <th class="numeric">@lang('message.platform')</th>
                    <th class="numeric">@lang('basic.apply.amount')</th>
                    <th class="numeric" style="width: 105px;">@lang('final.deadline.day')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('task.taskInfo.track.person')</th>
                    <th class="numeric">@lang('track.lockStatus')</th>
                    <th class="numeric">@lang('basic.detail')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $k=>$avalue)
                    <tr>
                        <td><input type="checkbox" name="selectTrack[]" value="{{$avalue->id}}"></td>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $avalue->order_number }}</td>
                        <td class="numeric">{{ $avalue->name }}</td>
                        <td class="numeric">{{ \App\Consts\Platform::alias($avalue->platform) }}</td>
                        <td class="numeric">{{ $avalue->apply_amount }}</td>
                        <td class="numeric">{{ substr($avalue->loan_deadline, 0, 10) }}</td>
                        <td class="numeric">
                            {{ $avalue->order_status_str }}
                        </td>
                        <td class="numeric">{{ $avalue->admin_user }}</td>
                        <td class="numeric">{{ $avalue->status_str }}</td>
                        <td class="numeric">
                            <a data-id="{{ $avalue->id }}" data-user="{{ $avalue->admin_id }}" class="track-detail" style="cursor: pointer" data-toggle="modal" date-url="{{$urlCondition}}" data-target="#myModal">@lang('basic.assign.again')</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="diy-paginate">
                <form action="/tasktrack/track" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">{{ $orders->appends($pageCondition)->render() }}</div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
        </section>
    </div>
</section>
