<style>
    .custom-form-group{
        margin-bottom: 0px;
    }
</style>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset title="Initial Info">
        <div class="col-md-4">
            <div class="form-group custom-form-group">
                <label class="col-md-4 control-label">@lang('message.order.number') </label>
                <div class="col-md-8">
                    <input type="text" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group custom-form-group">
                <label class="col-md-4 control-label">@lang('task.taskInfo.track.person')</label>
                <div class="col-md-8">
                    <select class="form-control m-bot15" name="admin_id">
                        <option value="">All</option>
                        @foreach($trackUser as $k=>$v)
                            <option value="{{ $v->id }}"
                                    @if(!empty($pageCondition['admin_id']))
                                    @if($pageCondition['admin_id'] == $v->id)
                                    selected="selected"
                                    @endif
                                    @endif
                            >{{ $v->admin_username }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group custom-form-group">
                <label class="col-md-4 control-label">@lang('message.platform')</label>
                <div class="col-md-8">
                    <select class="form-control m-bot15" name="platform">
                        <option value="">@lang('basic.all.platform')</option>
                        @foreach(\App\Consts\Platform::toString() as $k=>$v)
                            <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4 control-label">@lang('final.order.status')</label>
                <div class="col-md-8">
                    <select class="form-control m-bot15" name="status2">
                        <option value="">All</option>
                        <option value="4" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']==4) selected="selected" @endif>@lang('track.trackStatus1Two')</option>
                        <option value="1" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']==1) selected="selected" @endif>@lang('track.overdueStatus1')</option>
                        <option value="2" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']==2) selected="selected" @endif>@lang('track.overdueStatus2')</option>
                        <option value="3" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']==3) selected="selected" @endif>@lang('track.overdueStatus3')</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4 control-label">@lang('track.lockStatus')</label>
                <div class="col-md-8">
                    <select class="form-control m-bot15" name="status">
                        <option value="">All</option>
                        <option value="2" @if(!empty($pageCondition['status'])&&$pageCondition['status']==2) selected="selected" @endif>@lang('track.locked')</option>
                        <option value="1" @if(!empty($pageCondition['status'])&&$pageCondition['status']==1) selected="selected" @endif>@lang('track.unlocked')</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4 control-label">@lang('basic.client.name') </label>
                <div class="col-md-8">
                    <input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control">
                </div>
            </div>
        </div>


    </fieldset>
    <div class="col-md-12" style="text-align: center;margin-bottom: 15px">
        <button type="submit" class="btn btn-info">@lang('message.view')</button>
        <a href="javascript:;" style="margin-left: 50px;" class="btn btn-info reAssign">@lang('basic.assign.again')</a>
        <a href="javascript:;" style="margin-left: 50px;" class="btn btn-info autoAssigin">@lang('track.autoAssigin')</a>
        <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
    </div>
    <div style="clear: both"> </div>

<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>
