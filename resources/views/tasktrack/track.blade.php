<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('tasktrack/_track_add')
        <div class="wrapper">
            <form id="default" class="form-horizontal" action="/tasktrack/track" method="post">
            @include('tasktrack/_track_check')
            @include('tasktrack/_track_list')
            </form>
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 重新分配的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.assign.again')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 重新分配的结束 -->
@include('public/base_script')
<script src="/js/fSelect.js"></script>
<script>
    $(".track-detail").click(function(){

        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        var user = $(this).attr("data-user");
        var url = $(this).attr("date-url");

        $.ajax({
            url:'/tasktrack/assign',
            data:{
                order_id: id,
                admin_id: user,
                url: url,
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#myModal .modal-body').html(json);
        }).error(function(){
            alert("@lang('basic.trail.fail')");
        })
    });

    $('.autoAssigin').click(function(){
        var status2 = $('select[name="status2"]').val();
        if(status2==''){
            $.alert({
                title: "@lang('message.make.sure.title')",
                content: "@lang('track.plsChooseOverdue')",
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });
            return false;
        }

        if(status2<2){
            $.alert({
                title: "@lang('message.make.sure.title')",
                content: "@lang('track.below14ContReassign')",
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });
            return false;
        }

        var msg = '@lang("track.gt30ContReassign")';
        if(status2==2){
            msg = '<span style="color:red;">@lang("track.below30ContReassign")</span>';
        }

        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: msg,
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/tasktrack/autoReassigin",
                        type:"POST",
                        data:{
                            status2: status2
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:"json",
                        success:function(data){
                            $.confirm({
                                title: '@lang("message.make.sure.title")',
                                content: '@lang("message.phone.ask.have.finish")',
                                buttons: {
                                    "@lang('message.yes')": function () {
                                        location.href=location.href;
                                    },
                                    "@lang('message.cancel')": function () {
                                        location.href=location.href;

                                    }
                                }
                            })
                        }
                    });
                },
                "@lang('message.cancel')": function () {

                }
            }
        });

    });

    $('input[name="selectall"]').click(function(){
        if($(this).is(':checked')){
            $('input[name="selectTrack[]"]').each(function(){
                $(this).prop("checked",true);
            });
        }else{
            $('input[name="selectTrack[]"]').each(function(){
                $(this).removeAttr("checked",false);
            });
        }
    });

    $('.reAssign').click(function(){
        var admin_id = $('select[name="admin_id"]').val();
        if(!admin_id){
            $.alert({
                title: "@lang('message.make.sure.title')",
                content: "@lang('track.needChooseTrack')",
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });
            return false;
        }

        var formData = $("#default").serialize();

        $('#myModal').modal();
        $('.modal-body').html('');
        $.ajax({
            url:"/tasktrack/reAssignMultiple",
            type:"POST",
            data:formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:"html",
            success:function(data){
                $('.modal-body').html(data).css('height', '300px');
                $('#multipleTracks').fSelect();
            }
        });
    });

    $(document).on('click', '.reAssignConfirm', function() {
        var admin_id = $('select[name="admin_id"]').val();
        if(!admin_id){
            $.alert({
                title: "@lang('message.make.sure.title')",
                content: "@lang('track.needChooseTrack')",
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });
            return false;
        }

        var formData = $("#default").serialize();
        var multipleTracks = $('#multipleTracks').serialize();
        if(multipleTracks===''){
            $.alert({
                title: "@lang('message.make.sure.title')",
                content: "@lang('track.needChooseTrack1')",
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });
            return false;
        }

        formData = formData+'&'+multipleTracks;
        console.log(formData);
        $.ajax({
            url:"/tasktrack/reAssignMultipleHandle",
            type:"POST",
            data:formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:"json",
            success:function(data){
                if(data && data.code==1) {
                    location.href='/tasktrack/track';
                }
            }
        });
    });
</script>
</body>
</html>