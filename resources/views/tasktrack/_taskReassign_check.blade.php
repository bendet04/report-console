<a href="/tasktrack/track" style="color: #5276eb;size:14px;font-weight:bolder">
    < @lang('message.reback')
</a>
<div style="width:500px;margin:0 auto;">

    <form id="default" class="form-horizontal" action="/tasktrack/taskTrackHandle" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset title="Initial Info">
            <div class="form-group">
                <label class="col-md-3 col-sm-2 control-label">@lang('task.taskInfo.track.person')：</label>
                <div class="col-md-9 col-sm-9">
                    <select class="form-control m-bot15" name="admin_id">
                        <option></option>
                        @if($infoUser)
                            @foreach($infoUser as $k=>$v)
                                <option value="{{ $v->id }}">{{ $v->admin_username }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label col-lg-3" for="inputSuccess">@lang('task.taskInfo.assign.to')：</label>
                <div class="col-lg-9">
                    @if($realUser)
                        @foreach($realUser as $k=>$v)
                            <label class="checkbox-inline" style="margin-left: 0px;">
                                <input type="checkbox" id="inlineCheckbox1" name="needAssign[]" value="{{$v->id}}"> {{$v->admin_username}}
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>
        </fieldset>
        <div class="col-md-9 col-sm-9" style="text-align: center;margin-bottom: 30px">
            <button type="submit" class="btn btn-info">@lang('task.taskInfo.assign')</button>
        </div>
        <div style="clear: both"> </div>
    </form>
</div>