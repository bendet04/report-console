<div class="reAssignMultiple">
    <div class="col-md-6">
        <div class="col-md-5 custom-text-right">@lang('task.taskInfo.track.person')</div>
        <div class="col-md-7 highlight">{{$view['admin']}}</div>
    </div>
    <div class="col-md-6">
        <div class="col-md-5 custom-text-right">@lang('message.platform')</div>
        <div class="col-md-7 highlight">{{$view['platform']}}</div>
    </div>

    <div class="col-md-6">
        <div class="col-md-5 custom-text-right">@lang('final.order.status')</div>
        <div class="col-md-7 highlight">
            @if(!empty($pageCondition['status2'])&&$pageCondition['status2']==4)
                @lang('track.trackStatus1Two')
            @elseif(!empty($pageCondition['status2'])&&$pageCondition['status2']==1)
                @lang('track.overdueStatus1')
            @elseif(!empty($pageCondition['status2'])&&$pageCondition['status2']==2)
                @lang('track.overdueStatus2')
            @elseif(!empty($pageCondition['status2'])&&$pageCondition['status2']==3)
                @lang('track.overdueStatus3')
            @else
                All
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-5 custom-text-right">@lang('track.lockStatus')</div>
        <div class="col-md-7 highlight">
            @if(!empty($pageCondition['status'])&&$pageCondition['status']==2)
                @lang('track.locked')
            @elseif(!empty($pageCondition['status'])&&$pageCondition['status']==1)
                @lang('track.unlocked')
            @else
                All
            @endif
        </div>
    </div>

    <div class="col-md-12 trackTotal">分配案件共 <span>@if($selectTrack) {{count($selectTrack)}} @else {{$orders->total()}}@endif</span> 件</div>

    <div class="col-md-3 custom-text-right" style="width: 18.5%; padding-right: 0px">@lang('task.taskInfo.assign.to')</div>
    <div class="col-md-9">
        <select name="multipleTracks[]" id="multipleTracks" class="form-control" multiple="multiple">
            @foreach($realUser as $k=>$v)
                @if($v->admin_username!==$view['admin'])
                <option value="{{$v->id}}">{{$v->admin_username}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div style="clear: both; height: 40px"></div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-info finish reAssignConfirm">@lang('message.sure')</button>
    <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
</div>

<style>
    .fs-wrap {
        position: relative;
        display: inline-block;
        width: 100%;
        font-size: 12px;
        line-height: 1;
    }

    .fs-label-wrap {
        position: relative;
        border: 1px solid #ccc;
        cursor: default;
        border-radius:4px;
    }

    .fs-label-wrap,
    .fs-dropdown {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .fs-label-wrap .fs-label {
        padding: 4px 22px 4px 8px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        line-height: 30px;
    }

    .fs-arrow {
        width: 0;
        height: 0;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-top: 6px solid #000;
        position: absolute;
        top: 0;
        right: 5px;
        bottom: 0;
        margin: auto;
    }

    .fs-dropdown {
        position: absolute;
        background-color: #fff;
        border: 1px solid #ccc;
        margin-top: 5px;
        width: 100%;
        z-index: 1000;
    }

    .fs-dropdown .fs-options {
        max-height: 200px;
        overflow: auto;
    }

    .fs-search input {
        width: 100%;
        padding: 2px 4px;
        border: 0;
    }

    .fs-option,
    .fs-search,
    .fs-optgroup-label {
        padding: 6px 8px;
        border-bottom: 1px solid #eee;
        cursor: default;
    }

    .fs-option {
        cursor: pointer;
    }

    .fs-option.hl {
        background-color: #f5f5f5;
    }

    .fs-wrap.multiple .fs-option {
        position: relative;
        padding-left: 30px;
    }

    .fs-wrap.multiple .fs-checkbox {
        position: absolute;
        display: block;
        width: 30px;
        top: 0;
        left: 0;
        bottom: 0;
    }

    .fs-wrap.multiple .fs-option .fs-checkbox i {
        position: absolute;
        margin: auto;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        width: 14px;
        height: 14px;
        border: 1px solid #aeaeae;
        border-radius: 2px;
        background-color: #fff;
    }

    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
        background-color: rgb(17, 169, 17);
        border-color: transparent;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAGCAYAAAD+Bd/7AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAAABMSURBVAiZfc0xDkAAFIPhd2Kr1WRjcAExuIgzGUTIZ/AkImjSofnbNBAfHvzAHjOKNzhiQ42IDFXCDivaaxAJd0xYshT3QqBxqnxeHvhunpu23xnmAAAAAElFTkSuQmCC');
        background-repeat: no-repeat;
        background-position: center;
    }

    .fs-wrap .fs-option:hover {
        background-color: #f5f5f5;
    }

    .fs-optgroup-label {
        font-weight: bold;
    }

    .hidden {
        display: none;
    }
</style>