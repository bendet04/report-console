<form id="default" action="/Investment/paymentAc" method="post" class="form-horizontal">
    <input type="hidden" name="id" value="{{$investment->id}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset title="Initial Info">
        <div class="investmentInfo">
            <div class="col-md-12">@lang('investment.Application.Number'): {{$investment->apply_order}}</div>
            <div class="col-md-4">
                @lang('investment.investmentName'): {{$investment->name}}
            </div>
            <div class="col-md-4">
                @lang('final.ktp'): {{$investment->ktpNumber}}
            </div>
            <div class="col-md-4">
                @lang('user.phone'): {{$investment->mobile}}
            </div>
            <div class="col-md-4">
                @lang('investment.paymentTotalAmount'): {{$investment->amount}}
            </div>
            <div class="col-md-4">
                @lang('investment.paymentTotalCount'): {{$flows->count()}}
            </div>
        </div>

        <div style="clear:both; height: 20px"></div>
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('final.clientName')</th>
                <th>@lang('final.payPeriod')</th>
                <th>@lang('message.loan.amount')</th>
                <th>@lang('message.create.time')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flows as $k=>$v)
                <tr class="">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->loan_name }}</td>
                    <td>{{ $v->loan_period }}</td>
                    <td>{{ (int)$v->loan_amount }}</td>
                    <td>{{ $v->order_time }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div style="color:red; line-height: 30px">@lang('investment.paymentConfirm')</div>
    </fieldset>

    <div class="modal-footer">
        <button type="submit" class="btn btn-default">@lang('investment.payment')</button>
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>