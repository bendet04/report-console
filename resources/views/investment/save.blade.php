@extends('public/layout')
@section('content')
    <header class="panel-heading" style="height: 70px;line-height: 35px;">
        <form class="form-horizontal" method="post" action="{{url('Investment/update')}}">
            {{csrf_field()}}
            <input type="hidden" name="pid" value="{{$data->productId}}">
            <div class="form-group">
                <label class="control-label col-md-4">@lang('investment.cycle')</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="period" value="{{$data->period}}" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">@lang('investment.Rate')</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="rate" value="{{$data->rate}}" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-10">
                    <button class="btn btn-success" type="submit">@lang('message.submit')</button>
                    <a class="btn btn-default" href="/Investment/setting">@lang('message.reback')</a>
                </div>
            </div>
        </form>
    </header>
@endsection
