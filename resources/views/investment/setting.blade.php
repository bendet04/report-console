@extends('public/layout')
@section('content')
    <div class="page-heading">
        <ul class="breadcrumb">
            <li>
                <a href="#">@lang('menu.Investment')</a>
            </li>
            <li class="active">@lang('menu.setting')</li>
        </ul>
    </div>


    <section class="panel">
        <div class="panel-body">
            <section id="flip-scroll">
                <table class="table table-bordered table-striped table-condensed cf">
                    <thead class="cf">
                    <tr>
                        <th class="numeric">@lang('investment.Product.Id')</th>
                        <th class="numeric">@lang('investment.cycle')</th>
                        <th class="numeric">@lang('investment.Rate')</th>
                        <th class="numeric">@lang('message.operation')</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $k => $v)
                        <tr id="del">
                            <td>{{$v->productId}}</td>
                            <td><span class="id">{{$v->period}}</span></td>
                            <td><span class="id">{{$v->rate * 100}}%</span></td>
                            <td><span class="id"><a href="/Investment/save?id={{$v->productId}}">@lang('investment.Modify')</a></span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="diy-paginate">
                    <form action="/Investment/setting" method="get">
                        <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                    </form>
                </div>
                <div class="diy-page-info">Showing 1 to {{$data->count()}} of {{$data->total()}} entries</div>
            </section>
        </div>
    </section>
@endsection
@section('js')
<script>
    //过滤内部机制
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   //修改状态 启用||不启用
    function save(status,save_id) {
        var msg=confirm("确定执行该操作吗?");
        if(msg==true)
        {
            $.post('/channel/change', {status:status,save_id: save_id}, function (data) {

             if (data.code == 200) {
                   if(status==1)
                   {
                    $(".status"+save_id).html("已启用");
                       $(".status_"+save_id).html("关闭");

                   }
                   else if(status==2)
                   {
                       $(".status"+save_id).html("未启用");
                       $(".status_"+save_id).html("开启");
                   }

                }
                else
                {
                    alert(data.info);
                }
            }, 'json')
        }

        else
        {
            return false;
        }
    }
    $("#trigg").click(function () {
        $("#is_show").hide();
        $("#is_two").show();

    })


   //删除渠道
    function  del(id) {
        var msg=confirm("确定执行该操作吗?");
        if(msg==true)
        {
            $.post('/channel/del', {is_del:1,id:id}, function (data) {
                if(data.code==200)
                {
                    $("#del"+id).hide();
                }
                else
                {
                    alert(data.info);
                }

            }, 'json')
        }

    }
</script>
@endsection