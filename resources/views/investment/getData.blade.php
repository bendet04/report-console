<form id="default" class="form-horizontal"  method="post" action="{{url('/Investment/examine_status')}}">
    {{ csrf_field() }}
    <fieldset title="Initial Info">
        <div class="form-group">
            <table class="table table-bordered table-striped table-condensed cf">
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="status" value="{{$data->status}}">
                <tr>
                    <td class="apply" status="{{$data->status}}" id="{{$data->id}}">@lang('investment.Application.Number')</td>
                    <td>{{$data->apply_order}}</td>
                </tr>
                <tr>
                    <td>@lang('investment.inve.name')</td>
                    <td>{{$data->name}}</td>
                </tr>
                <tr>
                    <td>KTP</td>
                    <td>{{$data->ktpNumber}} @if($data->ktp_image_url) <a href="{{env('CDN_URL').$data->ktp_image_url}}" target="_blank"><i class="fa fa-picture-o"></i></a> @endif</td>
                </tr>
                <tr>
                    <td>@lang('user.phone')</td>
                    <td>{{$data->mobile}}</td>
                </tr>
                <tr>
                    <td>@lang('message.agency.admin.email')</td>
                    <td>{{$data->email}}</td>
                </tr>
                <tr>
                    <td>@lang('investment.bank')</td>
                    <td>{{$data->bankCode}} </td>
                </tr>
                <tr>
                    <td>@lang('investment.bankNumber')</td>
                    <td>{{$data->bankCardNumber}}</td>
                </tr>
                <tr>
                    <td>@lang('message.RC_REFUSE_CHECK_RISK_OCCUPATION')</td>
                    <td>{{empty($data->occupation) ? '' : \App\Consts\Career::toString($data->occupation)}}</td>
                </tr>
                <tr>
                    <td>@lang('message.phone.home.address')</td>
                    <td>{{$data->province}}{{$data->city}}{{$data->area}}{{$data->street}}{{$data->detailedAddress}}</td>
                </tr>
            </table>

        </div>

    </fieldset>
    <div class="modal-footer">
        <button type="submit" name="add" class="btn btn-default">
            @lang('investment.pass')
        </button>
        <button type="button" class="btn btn-info finish" onclick="confirm()"  data-dismiss="modal">@lang('investment.Not.through')</button>
    </div>
</form>
<script>
    function confirm() {
        var id=$(".apply").attr('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post('/Investment/confirm', {id:id}, function (data) {
            if (data==200) {
               location.reload()
            }else{
                $.alert({
                    title: '审核结果',
                    content: '审核失败',
                    buttons: {
                        okay: {
                            text: 'OK',
                            btnClass: 'btn-info'
                        }
                    }
                });
            }

        }, 'json')
    }
</script>

