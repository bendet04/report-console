@extends('public/layout')
@section('content')
<style>
.input-group-addon{
    border: 0px;
    background-color:#eff0f4;
    width:100px;
}
.col-md-4 table{
    width:103%;
}
.arrow{
    vertical-align:middle;
}
</style>
<div class="page-heading">
    <ul class="breadcrumb">
        <li>
            <a href="#">@lang('menu.Investment')</a>
        </li>
        <li class="active">@lang('menu.applyTrial')</li>
    </ul>
</div>


<header class="panel-heading search-panel-heading" style="height: 150px;line-height: 45px">
    <form action="{{url('Investment/apply')}}" method="post">
        {{ csrf_field() }}
        <div class="col-md-4">
            <table class="ldx">
                <tr>
                    <td class="input-group-addon" >@lang('basic.date')</td>
                    <td class="arrow"><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                    <td class="arrow"><span>⇆</span></td>
                    <td class="arrow"><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">@lang('investment.Application.Number')</td>
                    <td class="arrow"><input type="text" autocomplete="off" class="form-control" @if(!empty($pageCondition['apply_order'])) value="{{$pageCondition['apply_order']}}" @endif name="apply_order"></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">@lang('final.ktp')</td>
                    <td class="arrow"><input type="text" autocomplete="off" class="form-control" @if(!empty($pageCondition['ktpNumber'])) value="{{$pageCondition['ktpNumber']}}" @endif name="ktpNumber"></td>
                </tr>
            </table>
        </div>
        <div style="clear:both;"></div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">@lang('message.phone')</td>
                    <td class="arrow"><input type="text" autocomplete="off" class="form-control" @if(!empty($pageCondition['mobile'])) value="{{$pageCondition['mobile']}}" @endif name="mobile"></td>
                </tr>
            </table>
        </div>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 135px">
            <button type="submit" class="btn btn-info">@lang('message.view')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
    </form>
</header>
<section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th class="numeric">@lang('final.order')</th>
                    <th class="numeric">@lang('investment.Application.Number')</th>
                    <th class="numeric">@lang('investment.inve.name')</th>
                    <th class="numeric">@lang('final.ktp')</th>
                    <th class="numeric">@lang('final.phone')</th>
                    <th class="numeric">@lang('investment.Amount.of.investment')</th>
                    <th class="numeric">@lang('investment.already_interest')</th>
                    <th class="numeric">@lang('final.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $k => $v)
                    <tr>
                        <td>{{$k + 1}}</td>
                        <td><span class="id">{{$v->apply_order}}</span></td>
                        <td><span class="id">{{$v->name}}</span></td>
                        <td><span class="id">{{$v->ktpNumber}}</span></td>
                        <td><span class="id">{{$v->mobile}}</span></td>
                        <td><span class="id">{{$v->amount}}</span></td>
                        <td><span class="id">{{$v->already_interest}}</span></td>
                        <td>
                            {{\App\Consts\InvestmentStatus::toString($v->status)}}
                        </td>
                        <td>
                            @if($v->status==\App\Consts\InvestmentStatus::INVESTMENTSTATUS8)
                                <a href="#myModal" data-toggle="modal" data-id="{{$v->id}}" class="btn btn-info finish trailBtn">@lang('investment.reapplyConfirm')</a>
                            @else
                                <a href="#myModal" data-toggle="modal" data-id="{{$v->id}}" class="btn btn-info finish trailBtn">@lang('menu.applyTrial')</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="diy-paginate">
                <form action="/Investment/examine" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">Showing 1 to {{$data->count()}} of {{$data->total()}} entries</div>
        </section>
    </div>
</section>


<!-- 新增的开始 -->
<div aria-hidden="true"  aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('menu.applyTrial')</h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<!-- 新增的结束 -->

@endsection
@section('js')
<script>
    //过滤内部机制
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //获取审核信息
    $('.trailBtn').click(function(){
        var id = $(this).attr('data-id');
        $('.modal-body').html('');
        $.ajax({
            url: "/Investment/flow",
            type: "get",
            data: {
                'investId':id,
                'type': 'applypayment'
            },
            dataType: "html",
            success: function (data) {
                $('.modal-body').html(data);
            }
        });
    });
</script>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>
@endsection