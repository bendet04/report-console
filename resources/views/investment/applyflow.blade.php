<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        @lang('investment.total_apply_amount') <span style="background-color: #ddd; padding: 5px">Rp: {{$investment->total_apply_amount}}</span> @lang('investment.already_apply_amount') <span style="background-color: #ddd;padding: 5px">Rp: {{$investment->already_apply_amount}}</span>
        <br /><br />
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('final.clientName')</th>
                <th>@lang('final.payPeriod')</th>
                <th>@lang('investment.already_interest')</th>
                <th>@lang('investment.currentStatus')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flows as $k=>$v)
                <tr class="">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->loan_name }}</td>
                    <td>{{ $v->loan_period }}</td>
                    <td>{{ (int)$v->interest }}</td>
                    <td>{{ $v->status_str }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>