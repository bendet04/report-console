@extends('public/layout')
@section('content')
<style>
.input-group-addon{
    border: 0px;
    background-color:#eff0f4;
    width:85px;
}
.col-md-4 table{
    width:102%;
}
.arrow{
    vertical-align:middle;
}

</style>
<div class="page-heading">
    <ul class="breadcrumb">
        <li>
            <a href="#">@lang('menu.Investment')</a>
        </li>
        <li class="active">@lang('menu.query')</li>
    </ul>
</div>

<header class="panel-heading search-panel-heading" style="height: 110px;line-height: 40px;">
    <form action="{{url('Investment/query')}}" method="post">
        {{ csrf_field() }}
        {{--<label class="col-md-1 col-sm-1 control-label">@lang('basic.apply.time')</label>--}}
        <div class="col-md-4">
            <table class="ldx">
                <tr>
                    <td class="input-group-addon" >@lang('basic.date')</td>
                    <td class="arrow"><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                    <td class="arrow"><span>⇆</span></td>
                    <td class="arrow"><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">@lang('investment.Application.Number')</td>
                    <td>
                        <input type="text" autocomplete="off" class="form-control" name="apply_order" @if(!empty($pageCondition['apply_order'])) value="{{$pageCondition['apply_order']}}" @endif>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table class="ldx1">
                <tr>
                    <td class="input-group-addon">@lang('final.order.status')</td>
                    <td>
                        <select class="form-control" name="status">
                            <option value="">All</option>
                            @foreach(\App\Consts\InvestmentStatus::toString('') as $k=>$v)
                            <option value="{{$k}}" @if(!empty($pageCondition['status']) && $pageCondition['status']==$k) selected @endif>{{$v}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4 col-sm-4" style="clear: both; margin-bottom: 10px;margin-left: 98px">
            <button type="submit" class="btn btn-info">@lang('message.view')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        <div class="col-md-7" style="text-align: right; line-height: 50px; font-size: 16px;">
            @if(!empty($pageCondition['status']) && $pageCondition['status']==50)
                @lang('investment.totalInvestmentAmount'): {{$totalInvestmentAmount}}
            @endif
            @if(!empty($pageCondition['status']) && $pageCondition['status']==51)
                @lang('investment.alreadyRePaymentAmount'): {{$totalInvestmentAmount}}
            @endif
        </div>
    </form>
</header>
<section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th class="numeric">@lang('final.order')</th>
                    <th class="numeric">@lang('investment.Application.Number')</th>
                    <th class="numeric">@lang('investment.inve.name')</th>
                    <th class="numeric">@lang('final.ktp')</th>
                    <th class="numeric">@lang('investment.Amount.of.investment')</th>
                    <th class="numeric">@lang('investment.already_interest')</th>
                    <th class="numeric">@lang('final.phone')</th>
                    <th class="numeric">@lang('message.agency.admin.email')</th>
                    <th class="numeric">@lang('basic.apply.time')</th>
                    <th class="numeric">@lang('final.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $k => $v)
                    <tr>
                        <td>{{$k + 1}}</td>
                        <td><span class="id">{{$v->apply_order}}</span></td>
                        <td><span class="id">{{$v->name}}</span></td>
                        <td><span class="id">{{$v->ktpNumber}}</span></td>
                        <td><span class="id">{{$v->amount}}</span></td>
                        <td><span class="id">{{(int)$v->already_interest}}</span></td>
                        <td><span class="id">{{$v->mobile}}</span></td>
                        <td><span class="id">{{$v->email}}</span></td>
                        <td><span class="id">{{$v->created_at}}</span></td>
                        <td><span class="id">{{$v->status_str}}</span></td>
                        <td>
                            <a href="javascript:;" data-toggle="modal" data-target="#inventmentFlow" data-type="loan" data-id="{{$v->id}}" class="inventmentFlowBtn">@lang('investment.loanRecord')</a>
                            <a href="javascript:;" data-toggle="modal" data-target="#inventmentFlow" data-type="apply" data-id="{{$v->id}}" class="inventmentFlowBtn">@lang('investment.applyRcord')</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="diy-paginate">
                <form action="/Investment/query" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">Showing 1 to {{$data->count()}} of {{$data->total()}} entries</div>
        </section>
    </div>
</section>


<div aria-hidden="true" aria-labelledby="inventmentFlowLabel" role="dialog" tabindex="-1" id="inventmentFlow" class="modal fade">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    //过滤内部机制
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.deleteInvestment').click(function(){
        var url = $(this).attr('href');
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: '@lang("message.make.sure.delete.this.record")',
            buttons: {
                confirm: {
                    text: 'OK',
                    action: function() {
                        location.href=url;
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info'
                }
            }
        });
        return false;
    });

    $('.inventmentFlowBtn').click(function(){
        var investId = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var title = $(this).text();
        $('.modal-title').html(title);
        $('#inventmentFlow .modal-body').html('');
        $.ajax({
            url:'/Investment/flow',
            data:{
                investId: investId,
                type: type
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#inventmentFlow .modal-body').html(json);
        }).error(function(){
            alert("@lang('final.dataError')");
        })
    });

    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>

@endsection