<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        @lang('investment.Amount.of.investment') <span style="background-color: #ddd; padding: 5px">Rp: {{$investment->amount}}</span> @lang('investment.already_interest') <span style="background-color: #ddd;padding: 5px">Rp: {{(int)$investment->already_interest}}</span>
        <br /><br />
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('final.clientName')</th>
                <th>@lang('final.payPeriod')</th>
                <th>@lang('message.loan.amount')</th>
                <th>@lang('track.paymentDate')</th>
                <th>@lang('final.deadline.day')</th>
                <th>@lang('investment.currentStatus')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flows as $k=>$v)
                <tr class="">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->loan_name }}</td>
                    <td>{{ $v->loan_period }}</td>
                    <td>{{ $v->loan_amount }}</td>
                    <td>{{ $v->payment_date }}</td>
                    <td>{{ $v->loan_deadline }}</td>
                    <td>{{ $v->order_status_str }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>