<form id="default" action="/Investment/applyAc" method="post" class="form-horizontal">
    <input type="hidden" name="id" value="{{$investment->id}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset title="Initial Info">
        <div class="investmentInfo">
            <div class="col-md-6">
                @lang('investment.investmentName'): {{$investment->name}}
            </div>
            <div class="col-md-6">
                @lang('final.ktp'): {{$investment->ktpNumber}}
            </div>
            <div class="col-md-6">
                @lang('investment.Amount.of.investment'): {{$investment->amount}}
            </div>
            <div class="col-md-6">
                @lang('investment.already_interest'): {{$investment->already_interest}}
            </div>
            <div class="col-md-6">
                @lang('investment.total_apply_amount'): {{$investment->total_apply_amount}}
            </div>
            <div class="col-md-6">
                @lang('investment.already_apply_amount'): {{$investment->already_apply_amount}}
            </div>
        </div>

        <div style="clear:both; height: 20px"></div>
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('final.clientName')</th>
                <th>@lang('final.payPeriod')</th>
                <th>@lang('investment.already_interest')</th>
                <th>@lang('investment.currentStatus')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flows as $k=>$v)
                <tr class="">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->loan_name }}</td>
                    <td>{{ $v->loan_period }}</td>
                    <td>{{ (int)$v->interest }}</td>
                    <td>{{ $v->status_str }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="submit" class="btn btn-default">@lang('investment.applyConfirm')</button>
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>