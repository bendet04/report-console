<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:110px;
    }
    .col-md-4 table{
        width:100%;
        /*width:100%;*/
    }
    .ldx{
        margin-top: -8px;
    }
</style>
<header class="" style="height: 160px;line-height: 45px;">
    <form action="/order/riskRefuse" method="post">
        {{ csrf_field() }}
        {{--开始时间与结束时间--}}
        <div class="col-md-4 ">
            <table class="ldx">
                <tr>
                    <td class="input-group-addon" >@lang('basic.date')</td>
                    <td><input type="text" placeholder="Start time" autocomplete="off" class="form-control dpd1 " @if(!empty($searchArr['start'])) value="{{$searchArr['start']}}" @endif name="start"></td>
                    <td ><span>⇆</span></td>
                    <td><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($searchArr['end'])) value="{{$searchArr['end']}}" @endif name="end"></td>
                </tr>
            </table>
        </div>
        {{--订单编号--}}
        <div class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon">@lang('final.number')</td>
                    <td><input type="text" placeholder="M20180XXXXXXX" name="order_number" @if(!empty($searchArr['order_number'])) value="{{$searchArr['order_number']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        {{--电话号码--}}
        <div class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon" >@lang('message.phone')</td>
                    <td><input type="text" placeholder="phone number" name="phone_number" @if(!empty($searchArr['phone_number'])) value="{{$searchArr['phone_number']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        <br/>
        {{--KTP--}}
        <div class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon" >{{__('final.ktp')}}</td>
                    <td><input type="text" placeholder="Ktp number" name="ktp_number" @if(!empty($searchArr['ktp_number'])) value="{{$searchArr['ktp_number']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        {{--风控拒绝规则--}}
        <div class="col-md-4">
            <table>
                <tr>
                    <td class="input-group-addon">{{__('final.reason')}}</td>
                    <td>
                        <select class="form-control refuse" name="type">
                            <option value="risk" @if(!empty($searchArr['type'])&&$searchArr['type']=='risk')selected='selected' @endif>{{__('basic.risk.reason')}}</option>
                            <option value="day" @if(!empty($searchArr['type'])&&$searchArr['type']=='day')selected='selected' @endif>{{__('basic.short.time')}}</option>
                            <option value="black" @if(!empty($searchArr['type'])&&$searchArr['type']=='black')selected='selected' @endif>{{__('basic.black.reason')}}</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon">@lang('status.detail')</td>
                    <td>
                        <select name="detail" class="form-control switch" @if(!empty($searchArr['type'])&&$searchArr['type']!='risk') disabled="disabled" @endif>
                            <option value="">All</option>
                            @if(!empty($data))
                                @foreach($data as $k=>$v)
                                    <option value="{{$k}}" @if(!empty($searchArr['detail'])&&$searchArr['detail']==$k) selected="selected" @endif>@lang('message.'.$k)</option>
                                @endforeach
                            @endif
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        {{--end--}}
        <br/>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 110px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>

    </form>
</header>
{{--</head>--}}
<script>

    $(".refuse").change(function(){
        //获取已选择的拒绝原因
        var refuse=$(this).val();
        if(refuse=='risk'){
            $('.switch').attr("disabled",false);;
        }else{
            $('.switch').attr("disabled",true);;
        }
    })
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);
        var refuse=$(" input[ name='type' ] ").val();
        if(refuse=='risk'){
            $('.switch').attr("disabled",false);
        }else{
            $('.switch').attr("disabled",true);
        }
    }
</script>