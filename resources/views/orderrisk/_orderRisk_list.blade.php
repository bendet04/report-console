<section class="panel">
    <div class="wrapper" id="is_show">

        <section class="panel">
            <div class="panel-body">
                <section id="flip-scroll">
                    <table class="table table-bordered table-striped table-condensed cf">
                        <thead class="cf">
                        <tr>
                            <th>@lang('message.number')</th>
                            <th class="numeric">@lang('message.order.number')</th>
                            <th class="numeric">@lang('message.client.name')</th>
                            <th class="numeric">KTP</th>
                            <th class="numeric">@lang('message.create.time')</th>
                            <th class="numeric">@lang('final.reason')</th>
                            <th class="numeric">@lang('basic.risk.control')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $k=>$v)
                            <tr>
                                <td>{{$k + 1}}</td>
                                <td>{{$v->order_number}}</td>
                                <td >{{$v->name}}</td>
                                <td >{{$v->ktp_number}}</td>
                                <td >{{$v->created_at}}</td>
                                <td >{{__('basic.'.$v->type)}}</td>
                                <td >@if(isset($v->refuse))@lang('message.'.$v->refuse)@else @lang('message.no.detailed.reasons') @endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="diy-paginate">
                        <form action="/order/riskRefuse" method="get">
                            <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                        </form>
                    </div>
                    <div class="diy-page-info">{{$orders->appends($searchArr)->render()}}</div>
                    <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>


                </section>
            </div>
        </section>

    </div>
</section>