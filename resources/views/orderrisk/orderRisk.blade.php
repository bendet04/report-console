<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>

<body class="sticky-header">
    @include('public/left')
    <section>
        <div class="main-content">
            @include('public/right_header')
            @include('orderrisk/_orderRisk_add')
            @include('orderrisk/_orderRisk_search')
            @include('orderrisk/_orderRisk_list')
            @include('public/bottom')
        </div>
    </section>

    @include('public/base_script')
</body>
</html>

