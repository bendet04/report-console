<section class="panel">
    <div class="panel-body">
        <form method="post" action="/agency/roleAssinAccessDetail">
            {{csrf_field()}}
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th class="numeric">@lang('message.agency.access.access.name')</th>
                    <th class="numeric">@lang('message.agency.access.rounter')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                @if($accessDetail)
                    @foreach($accessDetail as $key => $value)
                        <tr>
                            <input type="hidden" name="roleId" value="{{$id}}">
                            <td>
                                <input type="checkbox" name="more[{{$value->id}}]" value="{{$value->id}}"
                                @if($roleAccessDetail)
                                    @foreach($roleAccessDetail as $v)
                                        @if($v->aid == $value->id)
                                            disabled="true"
                                        @endif
                                    @endforeach
                                @endif
                                >
                            </td>
                            <td class="numeric">{{$value->access_name}}</td>
                            <td class="numeric">
                                @empty($value->access_url)
                                    @lang('message.agency.access.firstView')
                                @endempty
                                {{$value->access_url}}
                            </td>
                            <td class="numeric">
                                @if($value->is_use == 1)
                                    @lang('message.freeze')
                                @else
                                    @lang('message.normal')
                                @endif
                            </td>
                            <td class="numeric">
                                @if($roleAccessDetail)
                                    @foreach($roleAccessDetail as $v)
                                        @if($v->aid == $value->id)
                                        <a href="/agency/deleteRoleAccess/roleId/{{$id}}/accessId/{{$value->id}}">@lang('message.cancel')</a>
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$accessDetail->render()}}
            @endif
        </section>
        <div class="modal-footer">
            <button type="submit" class="btn btn-default">@lang('message.submit')</button>
            <a type="button" class="btn btn-info finish" href="/agency/role">@lang('message.reback')   </a>
        </div>
        </form>
    </div>
</section>