<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('agency/_admin_add')
        <div class="wrapper">
            @include('agency/_admin_check')
            @include('agency/_admin_list')
        </div>
    </div>
</section>
@if($errors->any())
    <div class="list-group">
        @foreach($errors->all() as $error)
            <li class="list-group-item list-group-item-danger">{{ $error }}</li>
        @endforeach
    </div>
@endif
@include('public/base_script')
</body>
</html>
<script>
    $('#adminer').on('blur', function(){
        if($(this).val() == ''){
            $('#admin').html('管理员的名字不能为空');
            return false;
        } else {
            var username = $(this).val();
            console.log(username);
            $.post("/agency/adminNameIsExist", {_token:"{{csrf_token()}}",username:username}, function(data){
                if(data['code']){
                    $('#admin').html('该管理员已存在');
                    return false;
                } else {
                    $('#admin').html('');
                }
            }, 'json');
        }
    });
    $('#realadminer').on('blur', function(){
        if($(this).val() == ''){
            $('#realadmin').html('管理员的真实姓名不能为空');
        } else {
            $('#realadmin').html('');
        }
    })
    $("#addEmail").on('blur', function(){
        var email = $(this).val();
        var reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
        if(!reg.test(email))  {
            $('#checkEmail').html('请输入正确的邮箱地址');
            return false;
        } else {
            $('#checkEmail').html("");
            return true;
        }
    })
    function updateAdmin(obj) {
        var page = $("#currentPage").attr('value');
        var roleId = $(obj).attr('value');
        console.log(page)
        $.ajax({
//            url:'/agency/adminUpdate/id/'+roleId+'/page/'+page,
            url:'/agency/adminUpdate/id/'+roleId,
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#adminUpdateDetail .modal-body').html(json);
        }).error(function(){
            alert('修改管理员失败');
        })
    }
    function deleteAdmin(obj)
    {
        var roleId = $(obj).attr('value');
        $.ajax({
            url:'/agency/adminDelete/id/'+roleId,
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#deleteAdmin .modal-body').html(json);
        }).error(function(){
            alert('删除管理员失败');
        })
    }
</script>


