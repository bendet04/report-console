<form id="default" class="form-horizontal" action="{{url('/agency/updateAccessDetail')}}" method="post">
    {{ csrf_field() }}
    <fieldset title="Initial Info">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.access.access.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{$updateAccessDetail->access_name}}" name="name" class="form-control">
            </div>
        </div>
        <input type="hidden" name="id" value="{{$updateAccessDetail->id}}">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-labela">@lang('message.status')</label>
            <div class="col-md-6 col-sm-6">
                <select class="form-control m-bot15" name="status">
                    <option value="1"
                        @if($updateAccessDetail->is_use == 1)
                        selected
                        @endif
                    >@lang('message.freeze')</option>

                    <option value="0"
                         @if($updateAccessDetail->is_use == 0)
                         selected
                         @endif
                    >@lang('message.normal')</option>
                </select>
            </div>
        </div>
        @if($updateAccessDetail->access_url)
            <div class="form-group">
                <label class="col-md-2 col-sm-2 control-labela">@lang('message.agency.access.lastAccess')</label>
                <div class="col-md-6 col-sm-6">
                    <select class="form-control m-bot15" name="first">
                        @if($parentAccess)
                            @foreach($parentAccess as $value)
                                <option
                                    @if ($value['id'] == $updateAccessDetail['access_leval'])
                                    value="{{$value->id}}"
                                    selected
                                    @endif
                                >{{$value->access_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.access.rounter')</label>
                <div class="col-md-6 col-sm-6">
                    <input type="text" value="{{$updateAccessDetail->access_url}}" name="url" class="form-control">
                </div>
            </div>
        @endif
    </fieldset>
    <div class="modal-footer">
        <button type="submit" class="btn btn-default">@lang('message.submit')</button>
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form> 