<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="panel-heading" style="height: 120px;line-height: 30px;">
    <form id="default" class="form-horizontal" method="post" action="{{url('/agency/admin')}}">
        {{ csrf_field() }}
        <fieldset title="Initial Info">
            <!--   <legend>Personal Information</legend> -->
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.agency.admin.login.name')</td>
                        <td><input type="text"  name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.agency.admin.real.name')</td>
                        <td><input type="text"  name="realName" @if(!empty($pageCondition['realName'])) value="{{$pageCondition['realName']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
        </fieldset>
        <br/>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <a href="#addAdmin" data-toggle="modal" style="margin-left: 25px;" class="btn btn-info finish">@lang('message.agency.admin.add.admin')</a>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        <div style="clear: both"> </div>
    </form>
</header>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addAdmin" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.admin.add.admin')</h4>
            </div>
            <div class="modal-body">
                <form id="default" class="form-horizontal" action="{{url('/agency/addAdmin')}}" method="post">
                    {{csrf_field()}}
                    <fieldset title="Initial Info">
                        <!--   <legend>Personal Information</legend> -->
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.login.name')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text"  id="adminer" name="username" class="form-control" required="">
                                <span id="admin" style="color:red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.real.name')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" id="realadminer" name="realname" class="form-control">
                                <span id="realadmin" style="color:red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.role')</label>
                            <div class="col-md-6 col-sm-4">
                                <select class="form-control" name="role">
                                    <option value="0">@lang('message.agency.admin.select.role')</option>
                                    @if($roleDetail)
                                        @foreach ($roleDetail as $key => $value)
                                            <option value="{{$value->rid}}">
                                                    {{$value->role_name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.phone')</label>
                            <div class="col-lg-2">
                                <input type="text"  name="code" class="form-control">
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <input type="text"  name="phone" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.email')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text"  id="addEmail" name="email" class="form-control">
                            </div>
                            <span id="checkEmail" style="color: red;"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.address')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text"  name="address" class="form-control">
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-md-2 col-sm-2 control-label">@lang('message.status')</label>
                        <div class="col-md-6 col-sm-4">
                            <select class="form-control" name="status">
                                <option value="0" selected></option>
                                <option value="0">@lang('message.normal')</option>
                                <option value="1">@lang('message.freeze')</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">@lang('message.submit')</button>
                        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- 新增的结束 -->
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>