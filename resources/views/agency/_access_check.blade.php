<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 120px;line-height: 30px;">
    <form id="default" class="form-horizontal" method="post" action="{{url('/agency/access')}}">
        {{ csrf_field() }}
        <fieldset title="Initial Info">
            <!--   <legend>Personal Information</legend> -->
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.agency.access.access.name')</td>
                        <td><input type="text"  name="depname" @if(!empty($pageCondition['depname'])) value="{{$pageCondition['depname']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.agency.access.rounter')</td>
                        <td><input type="text"  name="rount" @if(!empty($pageCondition['rount'])) value="{{$pageCondition['rount']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
        </fieldset>
    <br/>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 15px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.view')</button>
            <a href="#myModal" data-toggle="modal" style="margin-left: 25px;" class="btn btn-info finish">@lang('message.agency.access.addAccess')</a>

            {{--<a href="/taskinfo/taskInfoReassign" style="margin-left: 25px;" class="btn btn-info">@lang('basic.assign.again')</a>--}}
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">

        </div>
        {{--<div class="col-md-9 col-sm-9" style="margin-bottom: 10px;width:5%;margin-left:50px">--}}
            {{--<button type="submit" class="btn btn-info">@lang('message.select')</button>--}}
            {{--<a href="#myModal" data-toggle="modal" style="margin-left: 25px;" class="btn btn-info finish">@lang('message.agency.access.addAccess')</a>--}}
            {{--<input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        {{--<div class="col-md-1 col-sm-1" style="margin-bottom: 10px;width:10%;margin-left:140px">--}}
            {{--<button type="submit" class="btn btn-info form-control">@lang('message.select')</button>--}}
        {{--</div>--}}
        {{--<div class="col-md-1 col-xs-1 " style="tmargin-bottom: 15px;width: 18%">--}}
            {{--<a href="#myModal" data-toggle="modal"  class="btn btn-info form-control finish">@lang('message.agency.access.addAccess')</a>--}}

        {{--</div>--}}
        {{--<div class="col-md-1 col-xs-1 " style="tmargin-bottom: 15px">--}}
            {{--<input type="button" class="btn btn-info form-control" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        {{--<div class="col-md-1 col-sm-1" style="margin-bottom: 10px;width:10%;margin-left:50px">--}}
            {{--<button type="submit" class="btn btn-info">@lang('message.select')</button>--}}
            {{--<a href="#myModal" data-toggle="modal" style="margin-left: 25px;" class="btn btn-info finish">@lang('message.agency.access.addAccess')</a>--}}
            {{--<input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        {{--<div class="col-md-1 col-sm-1" style="margin-bottom: 10px;">--}}
            {{--<a href="#myModal" data-toggle="modal"  class="btn btn-info finish">@lang('message.agency.access.addAccess')</a>--}}

        {{--</div>--}}

        <div style="clear: both"> </div>
    </form>
</header>
<!-- 新增的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.access.addAccess')</h4>
            </div>
            <div class="modal-body">
                <form id="default" class="form-horizontal"  method="post" action="{{url('/agency/addAccess')}}">
                    {{ csrf_field() }}
                    <fieldset title="Initial Info">
                        <a type="button" class="btn btn-default" id="addSecond">@lang('message.agency.access.add.secondAccess')</a>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.access.access.name')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" value="" class="form-control" name="accessName">
                            </div>
                        </div>
                        <span id="secondCome" style="display:none">
                            <div class="form-group">
                                <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.access.lastAccess')</label>
                                <div class="col-md-6 col-sm-6">
                                <select class="form-control" name="leval">

                                    <option name="null"></option>
                                    @if($parentAccess)
                                        @foreach($parentAccess as $value)
                                        <option name="leval">
                                            {{$value->access_name}}
                                        </option>
                                         @endforeach
                                     @endif
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.access.rounter')</label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" placeholder="" class="form-control" name="url">
                                </div>
                            </div>
                        </span>
                    </fieldset>
                    <div class="modal-footer">
                        <button type="submit" name="add" class="btn btn-default">@lang('message.submit')</button>
                        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 新增的结束 -->
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>