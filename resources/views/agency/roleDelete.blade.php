<form id="default" class="form-horizontal" method="post" action="{{url('agency/roleSoftDelete')}}">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{$id}}">
    <div class="modal-body">

        @lang('message.make.sure.delete.this.record')？

    </div>
    <div class="modal-footer">
        <button type="submit" name="sub" class="btn btn-default">@lang('message.sure')</button>
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>