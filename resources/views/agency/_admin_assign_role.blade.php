<section class="panel">
    <div class="panel-body">
        <form method="post" action="/agency/assignRoleDetail">
            {{csrf_field()}}
        <table class="table table-bordered table-striped table-condensed cf">
            <thead class="cf">
            <tr>
                <th>@lang('message.number')</th>
                <th class="numeric">@lang('message.agency.role.name')</th>
                <th class="numeric">@lang('message.status')</th>
                <th class="numeric">@lang('message.operation')</th>
            </tr>
            </thead>
            <tbody>
            @if($roleInfo)
                @foreach($roleInfo as $key => $value)
                    <tr>
                        <input type="hidden" name="adminId" value="{{$id}}">
                        <td>
                            <input type="checkbox" name="more[{{$value->rid}}]" value="{{$value->rid}}"
                                   @if($roleDetail)
                                   @foreach($roleDetail as $k => $v)
                                   @if($value->rid == $v->rid)
                                   disabled="true"
                                    @endif
                                    @endforeach
                                    @endif
                            >
                        </td>
                        <td>{{$value->role_name}}</td>
                        <td>
                            @if($value->status == 1)
                                @lang('message.freeze')
                            @else
                                @lang('message.normal')
                            @endif
                        </td>
                        <td>

                            @if($roleDetail)
                                @foreach($roleDetail as $k => $v)
                                    @if($value->rid == $v->rid)
                                        <a style="cursor:pointer;" href="/agency/cancelRoleDetail/roleId/{{$value->rid}}/adminId/{{$id}}">@lang('message.cancel')</a>
                                    @endif
                                @endforeach
                            @endif
                        </td>
                    </tr>
                @endforeach

            </tbody>

        </table>
        {{$roleInfo->render()}}
        @endif
        <div class="modal-footer">
            <button type="submit" class="btn btn-default">@lang('message.submit')</button>
            <a type="button" class="btn btn-info finish" href="/agency/admin">@lang('message.reback')   </a>
        </div>
    </div>
</form>
</section>