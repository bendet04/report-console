<form id="default" class="form-horizontal" action="{{url('/agency/updateAdminDetail')}}" method="post">
    <fieldset title="Initial Info">
    {{ csrf_field() }}
        <!--   <legend>Personal Information</legend> -->
        @if($adminDetailInfo)
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.admin.number')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text"  id="adminer" value="{{$adminDetailInfo->admin_number}}" name="username" class="form-control" disabled>
                <span id="admin" style="color:red"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.login.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text"  id="adminer" value="{{$adminDetailInfo->admin_username}}" name="username" class="form-control" disabled>
                <span id="admin" style="color:red"></span>
            </div>
        </div>
        <input type="hidden" name="id" value="{{$adminDetailInfo->id}}">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.real.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" id="realadminer" value="{{$adminDetailInfo->real_username}}" name="realname" class="form-control">
                <span id="realadmin" style="color:red"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.phone')</label>
            <div class="col-lg-2">
                <input type="text"
                       @if($adminDetailInfo->admin_phone)
                       value="{{$adminDetailInfo->country_code}}"
                       @endif
                       name="code" class="form-control">
            </div>
            <div class="col-md-4 col-sm-4">
                <input type="text"  name="phone"
                       @if($adminDetailInfo->admin_phone)
                       value="{{$adminDetailInfo->admin_phone}}"
                       @endif
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.email')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text"  id="addEmail" name="email"
                       @if($adminDetailInfo->admin_email)
                       value="{{$adminDetailInfo->admin_email}}"
                       @endif
                       class="form-control">
            </div>
            {{--<span id="checkEmail" style="color: red;"></span>--}}
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.admin.address')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text"  name="address"
                       @if($adminDetailInfo->admin_address)
                       value="{{$adminDetailInfo->admin_address}}"
                       @endif
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.status')</label>
            <div class="col-md-6 col-sm-4">
                <select class="form-control" name="status">
                    <option value="0"
                            @if($adminDetailInfo->admin_status == 0)
                                selected
                            @endif
                    >@lang('message.normal')</option>
                    <option value="1"
                            @if($adminDetailInfo->admin_status == 1)
                            selected
                            @endif
                    >@lang('message.freeze')</option>
                </select>
            </div>
        </div>
        @endif
    </fieldset>
        <div class="modal-footer">
            <button type="submit" class="btn btn-default">@lang('message.submit')</button>
            <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
        </div>
</form>