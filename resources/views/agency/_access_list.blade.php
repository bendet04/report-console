<section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th class="numeric">@lang('message.agency.access.access.name')</th>
                    <th class="numeric">@lang('message.agency.access.rounter')</th>
                    <th class="numeric">@lang('message.create.time')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                    @if($accessDetail)
                    @foreach($accessDetail as $key => $value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td class="numeric">{{$value->access_name}}</td>
                        <td class="numeric">
                            @empty($value->access_url)
                                @lang('message.agency.access.firstView')
                            @endempty
                            {{$value->access_url}}
                        </td>
                        <td class="numeric">{{$value->create_time}}</td>
                        <td class="numeric">
                            @if($value->is_use == 1)
                                @lang('message.freeze')
                            @else
                                @lang('message.normal')
                            @endif
                        </td>
                        <td class="numeric">
                            <a onclick="updateAccess(this)" style="cursor: pointer" id="adminDetail" value="{{$value->id}}" data-toggle="modal" data-target="#updateAccess">@lang('message.update')</a>
                            |&nbsp;
                            <a onclick="deleteAccess(this)" style="cursor: pointer" id="adminDetail" value="{{$value->id}}" data-toggle="modal" data-target="#deleteAccess">@lang('message.delete')</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$accessDetail->render()}}
            @endif
        </section>
    </div>
</section>
<!-- 修改的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="updateAccess" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.access.updateAccess')</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<!-- 修改的结束 -->
<!-- 删除的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="deleteAccess" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.access.deleteAccess')</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>