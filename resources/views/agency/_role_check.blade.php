<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 120px;line-height: 30px;">
    <form id="default" class="form-horizontal" method="post" action="{{url('/agency/role')}}">
        {{ csrf_field() }}
        <fieldset title="Initial Info">
            <!--   <legend>Personal Information</legend> -->
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.agency.role.name')</td>
                        <td><input type="text"  name="roleName" @if(!empty($pageCondition['roleName'])) value="{{$pageCondition['roleName']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.agency.role.description')</td>
                        <td><input type="text"  name="descrription" @if(!empty($pageCondition['descrription'])) value="{{$pageCondition['descrription']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
        </fieldset>
        <br/>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 15px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <a href="#addRole" data-toggle="modal" style="margin-left: 25px;" class="btn btn-info finish">@lang('message.agency.role.addRole')</a>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">

        </div>
        <div style="clear: both"> </div>
    </form>
</header>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addRole" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.role.addRole')</h4>
            </div>
            <div class="modal-body">
                <form id="default" class="form-horizontal" action="{{url('/agency/addRole')}}" method="post">
                    <fieldset title="Initial Info">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.role.name')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" placeholder="" id="roleName" name="name" class="form-control">
                            </div>
                            <span id="checkName" style="color:red;"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.status')</label>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control" name="status">
                                    <option name="null"></option>
                                    <option value="0">@lang('message.normal')</option>
                                    <option value="1">@lang('message.freeze')</option>
                                </select>
                            </div>
                            <span id="checkName" style="color:red;"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('message.agency.role.description')</label>
                            <div class="col-sm-4">
                                <textarea rows="4" name="desc" id="roleDesc" class="form-control"></textarea>
                            </div>
                            <span id="checkDesc" style="color:red;"></span>
                        </div>
                    </fieldset>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">@lang('message.submit')</button>
                        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 新增的结束 -->
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>