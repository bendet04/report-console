<section class="panel">
    <div class="panel-body">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th class="numeric">@lang('message.agency.role.name')</th>
                    <th class="numeric">@lang('message.agency.role.description')</th>
                    <th class="numeric">@lang('message.create.time')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                    @if($roleInfo)
                    @foreach($roleInfo as $key => $value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->role_name}}</td>
                        <td>{{$value->role_desc}}</td>
                        <td>{{$value->create_time}}</td>
                        <td>
                            @if($value->status == 1)
                                @lang('message.freeze')
                            @else
                                @lang('message.normal')
                            @endif
                        </td>
                        <td>
                            <a onclick="updateRole(this)" style="cursor: pointer" id="adminDetail" value="{{$value->rid}}" data-toggle="modal" data-target="#roleUpdateDetail">@lang('message.update')</a>
                            @if($value->rid == 1)
                            @else
                            &nbsp;|&nbsp;
                            <a onclick="deleteRole(this)" style="cursor: pointer" id="adminDetail" value="{{$value->rid}}" data-toggle="modal" data-target="#deleteRole">@lang('message.delete')</a>
                            |
                            <a style="cursor: pointer" href="/agency/roleAssignAccess/id/{{$value->rid}}" id="adminDetail" >@lang('message.agency.admin.assign.access')</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach

                </tbody>

            </table>
        {{$roleInfo->render()}}
        @endif
    </div>
</section>
<!-- 修改的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="roleUpdateDetail" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.access.updateRole')</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<!-- 修改的结束 -->
<!-- 删除的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="deleteRole" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.access.deleteRole')</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>