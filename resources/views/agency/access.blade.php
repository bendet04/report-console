<!DOCTYPE html>
<html>
    <head>
        @include('public/title')
    </head>
    <body class="sticky-header">
        @include('public/left')
        <section>
            <div class="main-content">
                @include('public/right_header')
                @include('agency/_access_add')
                <div class="wrapper">
                    @include('agency/_access_check')
                    @include('agency/_access_list')

                </div>
                @include('public/bottom')
            </div>
        </section>
        @if($errors->any())
            <div class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </div>
        @endif
        @include('public/base_script')
    </body>
</html>
<script>
    $(document).ready(function(){

        $("#addSecond").click(function(){
            $("#secondCome").toggle();
        });

    })
    function updateAccess(obj) {
        var accessId = $(obj).attr('value');
        $.ajax({
            url:'/agency/accessUpdate/id/'+accessId,
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#updateAccess .modal-body').html(json);
        }).error(function(){
            alert('修改权限失败');
        })
    }
    function deleteAccess(obj)
    {
        var accessId = $(obj).attr('value');
        $.ajax({
            url:'/agency/accessDelete/id/'+accessId,
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#deleteAccess .modal-body').html(json);
        }).error(function(){
            alert('修改权限失败');
        })
    }
</script>


