<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
    @include('public/left')
    <section>
        <div class="main-content">
            @include('public/right_header')
            @include('agency/_role_add')
            <div class="wrapper">
                @include('agency/_role_check')
                @include('agency/_role_list')
            </div>
        </div>
    </section>
    @if($errors->any())
        <div class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">{{ $error }}</li>
            @endforeach
        </div>
    @endif
    @include('public/base_script')
</body>
</html>
<script>
    function updateRole(obj) {
//        var page = $("#currentPage").attr();
//        console.log(page);
        var roleId = $(obj).attr('value');
        $.ajax({
            url:'/agency/roleUpdate/id/'+roleId,
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#roleUpdateDetail .modal-body').html(json);
        }).error(function(){
            alert('修改角色失败');
        })
    }
    function deleteRole(obj)
    {
        var roleId = $(obj).attr('value');
        $.ajax({
            url:'/agency/roleDelete/id/'+roleId,
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#deleteRole .modal-body').html(json);
        }).error(function(){
            alert('修改权限失败');
        })
    }
</script>


