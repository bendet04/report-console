<section class="panel">
    <header class="panel-heading">
        <a style="color:red">@lang('message.tips')：
            @lang('message.agency.admin.first.password')：<a style="color: #0000F0">123456</a></a>
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-times"></a>
         </span>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped table-condensed cf">
            <thead class="cf">
            <tr>
                <th>@lang('message.number')</th>
                <th class="numeric">@lang('message.agency.admin.admin.number')</th>
                <th class="numeric">@lang('message.agency.admin.login.name')</th>
                <th class="numeric">@lang('message.agency.admin.real.name')</th>
                <th class="numeric">@lang('message.create.time')</th>
                <th class="numeric">@lang('message.status')</th>
                <th class="numeric">@lang('message.operation')</th>
            </tr>
            </thead>
            <span id="currentPage" value="{{$page}}"></span>
            <tbody>
            @if($adminDetail)

                @foreach($adminDetail as $key => $value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->admin_number}}</td>
                        <td>{{$value->admin_username}}</td>
                        <td>{{$value->real_username}}</td>
                        <td>{{$value->create_time}}</td>
                        <td>
                            @if($value->admin_status == 1)
                                @lang('message.freeze')
                            @else
                                @lang('message.normal')
                            @endif
                        </td>
                        <td>
                            <a onclick="updateAdmin(this)" style="cursor: pointer" id="adminDetail" value="{{$value->id}}" data-toggle="modal" data-target="#adminUpdateDetail">@lang('message.update')</a>
                            @if($value->id == 1)
                            @else
                                &nbsp;|&nbsp;
                                <a onclick="deleteAdmin(this)" style="cursor: pointer" id="adminDetail" value="{{$value->id}}" data-toggle="modal" data-target="#deleteAdmin">@lang('message.delete')</a>
                                &nbsp;|&nbsp;
                                <a style="cursor: pointer" href="/agency/adminAssignRole/id/{{$value->id}}"id="adminDetail" >@lang('message.agency.admin.assign.role')</a>
                            @endif
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
        {{$adminDetail->render()}}
        @endif
    </div>
</section>
<!-- 修改的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="adminUpdateDetail" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.admin.updateAdmin')</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<!-- 修改的结束 -->
<!-- 删除的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="deleteAdmin" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('message.agency.access.deleteRole')</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
