<form id="default" class="form-horizontal" action="{{url('/agency/updateRoleDetail')}}" method="post">
    {{ csrf_field() }}
    @if($roleDetail)
    <fieldset title="Initial Info">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-label">@lang('message.agency.role.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{$roleDetail->role_name}}" name="name" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-2 control-labela">@lang('message.status')</label>
            <div class="col-md-6 col-sm-6">
                <select class="form-control m-bot15" name="status">
                    <option value="1"
                            @if($roleDetail->status == 1)
                            selected
                            @endif
                    >@lang('message.freeze')</option>
                    <option value="0"
                            @if($roleDetail->status == 0 || $roleDetail->status ==2)
                            selected
                            @endif
                    >@lang('message.normal')</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">@lang('message.agency.role.description')</label>
            <div class="col-sm-4">
                <textarea rows="4" name="desc" id="roleDesc" class="form-control">{{$roleDetail->role_desc}}</textarea>
            </div>
            <span id="checkDesc" style="color:red;"></span>
        </div>
        <input type="hidden" name="id" value="{{$roleDetail->rid}}">
        @endif
    </fieldset>
    <div class="modal-footer">
        <button type="submit" class="btn btn-default">@lang('message.submit')</button>
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>