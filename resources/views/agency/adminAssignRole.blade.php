<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('agency/_admin_add')
        <div class="wrapper">
           @include('agency/_admin_assign_role')
        </div>
    </div>
</section>
@if($errors->any())
    <div class="list-group">
        @foreach($errors->all() as $error)
            <li class="list-group-item list-group-item-danger">{{ $error }}</li>
        @endforeach
    </div>
@endif