<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('track/_worktile')
        {{--@include('public/bottom')--}}
    </div>
</section>
<!-- 详情的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('track.checkMessage')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 详情的结束 -->
@include('public/base_script')
<script src="{{asset('/js/vue.js')}}"></script>
<script>
    new Vue({
        el: '#app',
        data: {
            order_id: @json($order['id']),
            beforThreeDay: '<?php echo date('Y-m-d H:i:s', strtotime('-3 day'));?>',
            today: '<?php echo date('Y-m-d');?>',
            contacts: @json($track['contacts']),
            records: @json($track['records']),
            promise_time: @json($track['promise_time']),
            currentTab: false,
            isSubmit: false
        },
        methods: {
            //【联系电话】添加到通话历史记录
            addToRecord: function(index, contact, type){
                var record = {
                    name: contact.name,
                    phone: contact.phone,
                    isActive: false,
                    remark: '',
                    remarkList: []
                };
                if(type==1){
                    this.records.main.push(record);
                    this.contacts.main.splice(index, 1);
                }else if(type==2){
                    this.records.otherPlatform.push(record);
                    this.contacts.otherPlatform.splice(index, 1);
                }else{
                    this.records.other.push(record);
                    this.contacts.other.splice(index, 1);
                }
            },
            //点击星星【置顶】操作
            addToTop: function(index, record, type) {
                record.isActive=true;
                this.records.top.unshift(record);
                if(type==1){
                    this.records.main.splice(index, 1);
                }else if(type==2){
                    this.records.otherPlatform.splice(index, 1);
                }else{
                    this.records.other.splice(index, 1);
                }
            },
            //【今天】备注保存操作
            saveRemark: function(record){
                if(record.remarkList && record.remarkList[0] && record.remarkList[0].time==this.today) {
                    console.log(record.remark);
                    if(record.remark){
                        record.remarkList[0].remark=record.remark;
                    }else{
                        record.remarkList.shift();
                    }
                }else{
                    if(record.remark) {
                        record.remarkList.unshift({'time': this.today, 'remark': record.remark});
                    }
                }
                this.currentTab=false;
            },
            //保存数据，包括联系电话、通话历史、还款日期等所有数据
            saveTrack: function(type) {
                this.isSubmit=true;

                $.ajax({
                    url:"/track/save",
                    type:"POST",
                    data:{
                        order_id: this.order_id,
                        records: JSON.stringify(this.records),
                        promise_time: this.promise_time,
                        type: type,
                        remarks: $('#remarks').val(),
                        back_url: '{{$back_url}}',
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:"json",
                    success:function(data){
                        if(data.order_id) {
                            var url = location.href;
                            location.href = '/track/trackworktile?order_id='+data.order_id+'&back_url={{$back_url}}'
                        }else{
                            location.href='<?php echo $back_url;?>'
                        }
                    },
                    error:function(data){
                        this.isSubmit=false;
                    }
                });
            }
        }
    });

    //点击返回按钮提示用户保存信息
    $('#backBtn').click(function(){
        var that = this;
        $.confirm({
            title: 'Warning',
            content: '<span style="color: red;">Data not saved!!! Determined to leave?</span>',
            buttons: {
                confirm: {
                    text: 'ok',
                    action: function() {
                        location.href = $(that).attr('href');
                    }
                },
                cancel: {
                    text: 'cancel',
                    btnClass: 'btn-info'
                }
            }
        });
        return false;
    });

    $('#repaymentBtn').click(function(){

        var uid = $(this).attr('data-uid');
        var order_id = $(this).attr('data-order-id');
        var bank_code = $('#repaymentBank').val();
        var that = this;

        if(!bank_code){
            $.alert({
                title: '@lang("message.make.sure.title")',
                content: '@lang("track.plsChooseBank")',
                buttons: {
                    okay: {
                        text: 'OK',
                        btnClass: 'btn-info'
                    }
                }
            });
            $(that).attr('disabled', false);
            return false;
        }
        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/track/detailVA',
            data:{
                price: '{{$order->repayment_amount}}',
                uid: uid,
                order_id:order_id,
                bank_code: bank_code,
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#myModal .modal-body').html(json);
            $('#repaymentBtn').attr('disabled', true);

        }).error(function(){
            // alert('详情信息查询失败');
            $('#repaymentBtn').attr('disabled',false);

        });

     });




</script>
</body>
</html>