<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 160px;line-height: 45px;">
<form id="default" class="form-horizontal" action="/track/checktrack" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset title="Initial Info">
        {{--订单编号--}}
        <div class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon" >@lang('track.number')</td>
                    <td><input type="text"  name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        {{--姓名--}}
        <div class="col-md-4 col-xs-4 ldx">
            <table>
                <tr>
                    <td class="input-group-addon" >@lang('track.name')</td>
                    <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        {{--状态--}}
        <div  class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon">@lang('message.status')</td>
                    <td>
                        <select class="form-control" name="status1">
                            <option>All</option>
                            <option value="1" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='1') selected="selected" @endif>@lang('track.trackStatus1One')</option>
                            <option value="9" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='9') selected="selected" @endif>@lang('track.trackStatus1TwoDay')</option>
                            <option value="2" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='2') selected="selected" @endif>@lang('track.trackStatus1Two')</option>
                            <option value="8" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='8') selected="selected" @endif>@lang('track.suspendDeadline')</option>
                            <option value="3" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='3') selected="selected" @endif>@lang('track.trackStatus1Three')</option>
                            <option value="4" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='4') selected="selected" @endif>@lang('track.trackStatus1Four')</option>
                            <option value="5" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='5') selected="selected" @endif>@lang('track.trackStatus1Five')</option>
                            <option value="6" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='6') selected="selected" @endif>@lang('track.trackStatus1Six')</option>
                            <option value="7" @if(!empty($pageCondition['status1'])&&$pageCondition['status1']=='7') selected="selected" @endif>@lang('track.trackStatus1Seven')</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <br/>
        {{--状态2--}}
        <div  class="col-md-4 col-xs-4">
            <table>
                <tr>
                    <td class="input-group-addon">@lang('message.status')2</td>
                    <td>
                        <select class="form-control" name="status2">
                            <option>All</option>
                            <option value="5" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']=='5') selected="selected" @endif>@lang('track.repaymentIntention')</option>
                            <option value="1" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']=='1') selected="selected" @endif>@lang('track.trackStatus2One')</option>
                            <option value="2" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']=='2') selected="selected" @endif>@lang('track.trackStatus2Two')</option>
                            <option value="3" @if(!empty($pageCondition['status2'])&&$pageCondition['status2']=='3') selected="selected" @endif>@lang('track.trackStatus2Three')</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>
    <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 140px">
        <button type="submit" class="btn btn-info">@lang('message.select')</button>
        <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
    </div>
    <div style="clear: both"></div>
</form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>