<div class="page-heading track" style="padding-top: 15px;">
    <div class="col-lg-12">
        <section class="custom-panel" id="app" v-cloak>
            <header class="panel-heading" style="border-bottom:1px solid #e1e1e1">
                <a href="{{$back_url}}" id="backBtn">< {{__('message.reback')}}</a>
                <span>@lang('message.platform'): <span class="platform" style="color: red;font-size:30px; text-transform:none;">{{\App\Consts\Platform::alias($order['platform'])}}</span>
                (<span style="color:red;font-size: 18px;font-weight: bold">{{$clientType['clientType']}}:  @if ($clientType['normalCount'])
                        @lang('final.normal'):{{$clientType['normalCount']}}
                    @endif
                    @if ($clientType['overdueCount'])
                        @lang('final.overdue'):{{$clientType['overdueCount']}}
                    @endif
                    @if ($clientType['applyCount'])
                        @lang('final.apply'):{{$clientType['applyCount']}}
                    @endif
                    @if ($clientType['lastStatus'])
                        {{$clientType['lastStatus']}}
                    @endif</span>)
                </span>

                @if($prevAdminUser)
                <span class="pull-right" style="line-height: 40px">上次所属人：{{$prevAdminUser}}</span>
                @endif
            </header>

            <div class="track-title">
                <span>{{__('track.number')}}:{{$order->order_number}}</span>
                <span>{{__('track.name')}}:{{$user->name}}</span>
                <span>{{__('track.gender')}}:@if($user->gender==1){{__('track.male')}}@endif @if($user->gender==2){{__('track.female')}}@endif</span>
                <span>{{__('track.applyAmount')}}:{{$order->apply_amount}}</span>
                @if(Session::has('locale') && Session::get('locale')=='en')<br />@endif
                <span>{{__('track.orderRePayAmount')}}:{{$order->repayment_amount}}</span>
                <span>{{__('message.amount.returned')}}:{{$order->already_repayment_amount}}</span>
                @if(!Session::has('locale') || Session::get('locale')!='en')<br />@endif
                <span>{{__('track.paymentDate')}}:{{date('Y-m-d', strtotime($order->payment_date))}}</span>
                <span class="blue">{{__('track.repaymentDate')}}:{{date('Y-m-d', strtotime($order->loan_deadline))}}</span>
                <span class="red">
                    @if($order->order_status==14)
                        @if(date('Y-m-d', strtotime('+1 day')) == date('Y-m-d', strtotime($order->loan_deadline)))
                            {{__('track.beforeLoanDeadline')}}
                        @else
                            {{__('track.loanDeadline')}}
                        @endif
                    @endif
                    @if($order->order_status==25)
                        {{__('track.loanDeadline')}}
                    @endif
                    @if($order->order_status==10)
                        {{__('track.suspendDeadline')}}
                    @endif
                    @if($order->order_status==11)
                        {{__('track.overdueDay', ['day' => \App\Service\DateUnit::timeToDay($order->loan_overdue_date)+1 ])}}
                    @endif
                    @if($order->order_status==13)
                        {{__('track.partPayment')}}({{__('track.overdueDay', ['day' => \App\Service\DateUnit::timeToDay($order->loan_overdue_date)+1 ])}})
                    @endif
                </span>
                <span>
                    <select name="repaymentBank" id="repaymentBank" class="form-control" style="width: 150px; display: inline;">
                        <option value="">@lang('track.chooseBank')</option>
                        @foreach($repaymentBanks as $k=>$v)
                        <option value="{{$v->parameter_code}}">{{$v->parameter_code_name}}</option>
                        @endforeach
                    </select>
                    {{--<a data-id="" class="track-detail" style="cursor: pointer" data-toggle="modal" data-target="#myModal">{{ __('track.detail') }}</a>--}}
                    <button class="btn btn-success btn-sm track-detail" id="repaymentBtn" style="cursor: pointer" data-toggle="modal" data-target="#myModal" data-uid="{{$order->uid}}" data-order-id="{{$order->id}}">@lang('message.sure')</button>
                    {{--<button class="btn btn-success btn-sm" id="repaymentBtn" data-uid="{{$order->uid}}" data-order-id="{{$order->id}}">@lang('track.generate')</button>--}}
                </span>
            </div>

            <div class="track-clear"></div>
            <div class="col-lg-5 track-col-5">
                <section class="panel">
                    <header class="panel-heading custom-tab custom-track-tab">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tel" data-toggle="tab" aria-expanded="false"><span>{{__('track.phone')}}</span></a>
                            </li>
                            <li class="">
                                <a href="#profile" data-toggle="tab" aria-expanded="false"><span>{{__('track.orderBasicTitle')}}</span></a>
                            </li>
                            <li class="">
                                <a href="#workInfo" data-toggle="tab" aria-expanded="false"><span>{{__('track.work_info')}}</span></a>
                            </li>
                            <li class="">
                                <a href="#email" data-toggle="tab" aria-expanded="false"><span>{{__('track.email')}}</span></a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body custom-panel-body1">
                        <div class="tab-content custom-panel-track">
                            <!--联系电话开始-->
                            <div class="tab-pane active" id="tel">
                                <div class="track-phone-title" v-if="contacts.main.length>0">@lang('track.mainContact')</div>
                                <div class="track-table">
                                    <div class="track-tr" v-for="(contact, index) in contacts.main">
                                        <div class="col-lg-1">
                                            <span><i class="fa fa-pencil" v-on:click="contact.isEdit=(contact.isEdit==true||contact.isEdit=='true' ? false : true)"></i></span>
                                        </div>
                                        <div class="col-lg-4 right work-break">
                                            <span v-if="contact.isEdit==false || contact.isEdit=='false'">@{{contact.name}}</span>
                                            <span v-if="contact.isEdit==true || contact.isEdit=='true'"><input type="text" v-model="contact.name" v-on:blur="contact.isEdit=false" style="width: 100px"></span>
                                        </div>
                                        <div class="col-lg-5">
                                            <span>@{{contact.phone}}</span>
                                        </div>
                                        <div class="col-lg-2">
                                            <span><button class="btn btn-sm btn-success" v-on:click="addToRecord(index, contact, 1)">@lang('track.add')</button></span>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>


                                <div class="track-table">
                                    <div class="track-phone-title" v-if="contacts.otherPlatform.length>0">@lang('track.otherPlatform')</div>
                                    <div class="track-tr" v-for="(contact, index) in contacts.otherPlatform">
                                        <div class="col-lg-1">
                                            <span><i class="fa fa-pencil" v-on:click="contact.isEdit=(contact.isEdit==true||contact.isEdit=='true' ? false : true)"></i></span>
                                        </div>
                                        <div class="col-lg-4 right work-break">
                                            <span v-if="contact.isEdit==false || contact.isEdit=='false'">@{{contact.name}}</span>
                                            <span v-if="contact.isEdit==true || contact.isEdit=='true'"><input type="text" v-model="contact.name" v-on:blur="contact.isEdit=false" style="width: 100px"></span>
                                        </div>
                                        <div class="col-lg-5">
                                            <span>@{{contact.phone}}</span>
                                        </div>
                                        <div class="col-lg-2">
                                            <span><button class="btn btn-sm btn-success" v-on:click="addToRecord(index, contact, 2)">@lang('track.add')</button></span>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>

                                <div class="track-table">
                                    <div class="track-phone-title" v-if="contacts.other.length>0">@lang('track.otherContact')</div>
                                    <div class="track-tr" v-for="(contact, index) in contacts.other">
                                        <div class="col-lg-1">
                                            <span><i class="fa fa-pencil" v-on:click="contact.isEdit=!contact.isEdit"></i></span>
                                        </div>
                                        <div class="col-lg-4 right work-break">
                                            <span v-if="contact.isEdit==false">@{{contact.name}}</span>
                                            <span v-if="contact.isEdit==true"><input type="text" v-model="contact.name" v-on:blur="contact.isEdit=false" style="width: 100px"></span>
                                        </div>
                                        <div class="col-lg-5">
                                            <span>@{{contact.phone}}</span>
                                        </div>
                                        <div class="col-lg-2">
                                            <span><button class="btn btn-sm btn-success" v-on:click="addToRecord(index, contact, 3)">@lang('track.add')</button></span>
                                        </div>
                                    </div>

                                </div>

                            </div>



                            <!--联系电话结束-->
                            <!--基本信息开始-->
                            <div class="tab-pane" id="profile">
                                <div class="track-phone-title">{{__('track.orderBasicTitle')}}</div>
                                @if(!empty($user->video_url))
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('track.vedio')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <video width="200" height="160" controls="controls" preload="none" class="custom-video" onclick="this.play();">
                                                <source src="{{$user->video_url}}" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if(!empty($user->ktp_image_url))
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('track.ktpImage')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <img src="{{$user->ktp_image_url}}" data-magnify="gallery" data-src="{{$user->ktp_image_url}}" data-caption=" " alt="@lang('track.ktpImage')" width="200" height="160">
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.ask.personal.phone')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->phone_number}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.client.name')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->name}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>KTP</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->ktp_number}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.ask.personal.marriage')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->marriage}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.ask.personal.pay.type')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$order->pay_type}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.home.address')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>
                                                {{$user->province}}
                                                {{$user->rengence}}
                                                {{$user->district}}
                                                {{$user->villages}}
                                                {{$user->address}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>{{$user->relative_type_first}}</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>
                                                {{$user->relative_name_first}}<br />{{$user->relative_phone_first}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>{{$user->relative_type_second}}</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>
                                                {{$user->relative_name_second}}<br />{{$user->relative_phone_second}}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div style="clear: both;"></div>
                            </div>
                            <!--基本信息结束-->
                            <!--工作信息开始-->
                            <div class="tab-pane" id="workInfo">
                                <div class="track-phone-title">{{__('track.work_info')}}</div>
                                @if(!empty($user->work_image_url))
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('track.workCard')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <img src="{{$user->work_image_url}}" data-magnify="gallery" data-src="{{$user->work_image_url}}" data-caption=" " alt="@lang('track.workCard')" width="200" height="160">
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.phone')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->company_phone}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.name')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->company_name}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.career')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->career}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.income')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->income_level}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.period')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->work_period}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.work.address')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>
                                                {{$user->company_province}}
                                                {{$user->company_rengence}}
                                                {{$user->company_district}}
                                                {{$user->company_villages}}
                                                {{$user->company_address}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-5">
                                            <span>@lang('message.phone.company.work.pay.period')</span>
                                        </div>
                                        <div class="col-lg-7">
                                            <span>{{$user->paid_period}}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--工作信息结束-->

                            <!--邮箱信息开始-->
                            <div class="tab-pane" id="email">
                                <div class="track-phone-title">{{__('track.email')}}</div>
                                @if($user->email)
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-4">
                                            <span>@lang('track.email')</span>
                                        </div>
                                        <div class="col-lg-8">
                                            <span>{{$user->email}}</span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @foreach($email as $k=>$v)
                                <div class="track-table">
                                    <div class="track-tr">
                                        <div class="col-lg-4">
                                            <span>{{$v['name']}}</span>
                                        </div>
                                        <div class="col-lg-8">
                                            <span>{{$v['email']}}</span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                            <!--邮箱信息结束-->

                        </div>
                    </div>
                </section>

            </div>
            <div class="col-lg-7 track-content-right" style="padding: 0px 10px 0px 0px;">
                <div class="track-content-right-title">@lang('track.callhistory')</div>

                <div class="custom-panel-track track-content-right">
                    <div class="track-phone-title" v-if="records.top.length>0 || records.main.length>0">@lang('track.mainContact')</div>
                    <div class="track-table">
                        <!--置顶开始-->
                        <div class="track-tr" v-for="(record, index) in records.top">
                            <div class="col-lg-3 right work-break" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.name}}</span>
                            </div>
                            <div class="col-lg-3" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.phone}}</span>
                            </div>
                            <div class="col-lg-1" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span><i class="fa fa-star" v-on:click="record.isActive=!record.isActive"></i></span>
                            </div>
                            <div class="col-lg-5 btn-box">
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('top'+index+'1')}" v-on:click="currentTab==('top'+index+'1') ? currentTab=false : currentTab=('top'+index+'1')">@lang('track.today')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('top'+index+'2')}" v-on:click="currentTab==('top'+index+'2') ? currentTab=false : currentTab=('top'+index+'2')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.threeDays')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('top'+index+'3')}" v-on:click="currentTab==('top'+index+'3') ? currentTab=false : currentTab=('top'+index+'3')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.tenDays')</button></span>
                            </div>
                            <div class="custom-textarea" v-if="currentTab==('top'+index+'1')">
                                <textarea v-model="record.remark" v-on:blur="saveRemark(record)"></textarea>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('top'+index+'2')">
                                <template v-for="r in record.remarkList" v-if="r.time>=beforThreeDay">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('top'+index+'3')">
                                <template v-for="r in record.remarkList">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                        </div>
                        <!--置顶结束-->

                        <!--关键联系人开始-->
                        <div class="track-tr" v-for="(record, index) in records.main">
                            <div class="col-lg-3 right work-break" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.name}}</span>
                            </div>
                            <div class="col-lg-3" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.phone}}</span>
                            </div>
                            <div class="col-lg-1" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span><i class="fa fa-star" v-on:click="addToTop(index, record, 1)"></i></span>
                            </div>
                            <div class="col-lg-5 btn-box">
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('main'+index+'1')}" v-on:click="currentTab==('main'+index+'1') ? currentTab=false : currentTab=('main'+index+'1')">@lang('track.today')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('main'+index+'2')}" v-on:click="currentTab==('main'+index+'2') ? currentTab=false : currentTab=('main'+index+'2')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.threeDays')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('main'+index+'3')}" v-on:click="currentTab==('main'+index+'3') ? currentTab=false : currentTab=('main'+index+'3')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.tenDays')</button></span>
                            </div>
                            <div class="custom-textarea" v-if="currentTab==('main'+index+'1')">
                                <textarea v-model="record.remark" v-on:blur="saveRemark(record)"></textarea>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('main'+index+'2')">
                                <template v-for="r in record.remarkList" v-if="r.time>=beforThreeDay">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('main'+index+'3')">
                                <template v-for="r in record.remarkList">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                        </div>
                        <!--关键联系人结束-->

                        <div style="clear: both"></div>

                        <!--其他平台联系人开始-->
                        <div class="track-phone-title" v-if="records.otherPlatform.length>0">@lang('track.otherPlatform')</div>
                        <div class="track-tr" v-for="(record, index) in records.otherPlatform">
                            <div class="col-lg-3 right work-break" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.name}}</span>
                            </div>
                            <div class="col-lg-3" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.phone}}</span>
                            </div>
                            <div class="col-lg-1" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span><i class="fa fa-star" v-on:click="addToTop(index, record, 2)"></i></span>
                            </div>
                            <div class="col-lg-5 btn-box">
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('platform'+index+'1')}" v-on:click="currentTab==('platform'+index+'1') ? currentTab=false : currentTab=('platform'+index+'1')">@lang('track.today')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('platform'+index+'2')}" v-on:click="currentTab==('platform'+index+'2') ? currentTab=false : currentTab=('platform'+index+'2')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.threeDays')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('platform'+index+'3')}" v-on:click="currentTab==('platform'+index+'3') ? currentTab=false : currentTab=('platform'+index+'3')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.tenDays')</button></span>
                            </div>
                            <div class="custom-textarea" v-if="currentTab==('platform'+index+'1')">
                                <textarea v-model="record.remark" v-on:blur="saveRemark(record)"></textarea>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('platform'+index+'2')">
                                <template v-for="r in record.remarkList" v-if="r.time>=beforThreeDay">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('platform'+index+'3')">
                                <template v-for="r in record.remarkList">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                        </div>
                        <!--关键联系人结束-->

                        <div style="clear: both"></div>
                        <div class="track-phone-title" v-if="records.other.length>0">@lang('track.otherContact')</div>
                        <!--其他联系人开始-->
                        <div class="track-tr" v-for="(record, index) in records.other">
                            <div class="col-lg-3 right work-break" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.name}}</span>
                            </div>
                            <div class="col-lg-3" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span>@{{record.phone}}</span>
                            </div>
                            <div class="col-lg-1" v-bind:class="{active:record.isActive==true || record.isActive=='true'}">
                                <span><i class="fa fa-star" v-on:click="addToTop(index, record, 3)"></i></span>
                            </div>
                            <div class="col-lg-5 btn-box">
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('other'+index+'1')}" v-on:click="currentTab==('other'+index+'1') ? currentTab=false : currentTab=('other'+index+'1')">@lang('track.today')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('other'+index+'2')}" v-on:click="currentTab==('other'+index+'2') ? currentTab=false : currentTab=('other'+index+'2')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.threeDays')</button></span>
                                <span><button class="btn btn-sm btn-custom" v-bind:class="{'btn-blue':currentTab==('other'+index+'3')}" v-on:click="currentTab==('other'+index+'3') ? currentTab=false : currentTab=('other'+index+'3')" v-if="record.remarkList && record.remarkList.length>0">@lang('track.tenDays')</button></span>
                            </div>
                            <div class="custom-textarea" v-if="currentTab==('other'+index+'1')">
                                <textarea v-model="record.remark" v-on:blur="saveRemark(record)"></textarea>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('other'+index+'2')">
                                <template v-for="r in record.remarkList" v-if="r.time>=beforThreeDay">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                            <div class="custom-tab-pane custom-track-pane" v-if="currentTab==('other'+index+'3')">
                                <template v-for="r in record.remarkList">
                                    <div class="col-lg-3">@{{record.name}}</div>
                                    <div class="col-lg-9 custom-date-color">@{{r.time}}</div>
                                    <div class="col-lg-12 custom-bottom">@{{r.remark}}</div>
                                </template>
                            </div>
                        </div>
                        <!--其他联系人结束-->

                    </div>
                </div>

                <div class="track-content-right-title" style="margin-top: 10px">@lang('track.reapymentDate1') <select v-model="promise_time">
                        <option value=""></option>
                        <option value="暂无还款意愿">@lang('track.repaymentIntention')</option>
                        @foreach($promise_time_arr as $k=>$v)
                            <option value="{{$v}}" @if($track['promise_time']==$v) selected @endif>{{$v}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-custom-size btn-blue" v-show="promise_time && promise_time<='{{$promise_time_arr[2]}}'">@lang('track.strong')</button>
                </div>
                <div class=" track-content-right-title" >
                        @lang('track.notice') <select id="remarks" style="overflow: hidden;text-overflow: ellipsis;
                     white-space: nowrap;
                     width: 480px;">
                        <option value=""></option>
                        @foreach(\App\Consts\Remarks::toString() as $k=>$v)
                            <option value="{{$k+1}}"  @if($trackInfo['remark']==($k+1)) selected @endif>@lang('track.'.$v)</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div style="clear:both;"></div>
            <div class="track-btn">
                <button class="btn btn-default btn-lg" type="button" v-on:click="saveTrack(1)" v-if="isSubmit===false">@lang('track.previous')</button>
                <button class="btn btn-lg" type="button" v-on:click="saveTrack(1)" disabled="disabled" v-if="isSubmit===true">loding</button>
                <button class="btn btn-default btn-lg" type="button" v-on:click="saveTrack(2)" v-if="isSubmit===false">@lang('track.next')</button>
                <button class="btn btn-lg" type="button" v-on:click="saveTrack(2)" disabled="disabled" v-if="isSubmit===true">loding</button>
            </div>
        </section>
    </div>
</div>
<div style="clear:both;"></div>