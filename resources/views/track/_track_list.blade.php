<section class="panel">
    <div class="panel-body">
        <div class="col-md-3 col-sm-6 diy-search-input-box">
            <select class="form-control diy-search-input" id="searchId">
                <option value="">select</option>
                <option value="order_number">@lang('track.number')</option>
                <option value="name">@lang('message.client.name')</option>
                <option value="apply_amount">@lang('track.applyAmount')</option>
                <option value="repayment_amount">@lang('track.paymentDate')</option>
                <option value="payment_date">@lang('track.repaymentDate')</option>
                <option value="order_status">@lang('message.status')</option>
            </select>
        </div>
        <span class="tools pull-right">
            @if(Auth::user()->work_status==4)
                <a href="javascript:;" class="btn btn-xs" style="background-color: #ccc; color: #fff; height: 30px; line-height: 10px; margin: 8px;">@lang('message.startWork')</a>
            @else
                <a href="/switchStatus?workStatus=4" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px;">@lang('message.startWork')</a>
            @endif
        </span>
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf"  id="dynamic-table">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('track.number')</th>
                    <th>@lang('message.platform')</th>
                    <th class="numeric">@lang('message.client.name')</th>
                    <th class="numeric">@lang('track.applyAmount')</th>
                    <th class="numeric">@lang('track.paymentDate')</th>
                    <th class="numeric">@lang('track.repaymentDate')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('track.detail')</th>
                </tr>
                </thead>
                <tbody>
                @if(Auth::user()->work_status==4)
                @foreach($orders as $k=>$avalue)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $avalue->order_number }}</td>
                        <td>{{ \App\Consts\Platform::alias($avalue->platform) }}</td>
                        <td class="numeric">{{ $avalue->name }}</td>
                        <td class="numeric">{{ $avalue->apply_amount }}</td>
                        <td class="numeric">{{ $avalue->payment_date }}</td>
                        <td class="numeric">{{ $avalue->loan_deadline }}</td>
                        <td class="numeric">
                            @if($avalue->order_status==14)
                                @if(date('Y-m-d', strtotime('+1 day')) == date('Y-m-d', strtotime($avalue->loan_deadline)))
                                    {{__('track.beforeLoanDeadline')}}
                                @elseif(date('Y-m-d', strtotime('+2 day')) == date('Y-m-d', strtotime($avalue->loan_deadline)))
                                    {{__('track.beforeLoanDeadline1')}}
                                @else
                                    {{__('track.loanDeadline')}}
                                @endif
                            @endif
                            @if($avalue->order_status==10)
                                {{__('track.suspendDeadline')}}
                            @endif
                            @if($avalue->order_status==11)
                                {{__('track.overDueDeadline')}}
                            @endif
                            @if($avalue->order_status==13)
                                {{__('track.partPayment')}}
                            @endif
                            @if($avalue->order_status==25)
                                {{__('track.loanDeadline')}}
                            @endif
                        </td>
                        <td>
                            <a data-id="{{ $avalue->id }}" class="track-detail" style="cursor: pointer" data-toggle="modal" data-target="#myModal">{{ __('track.detail') }}</a>
                            | <a href="/track/trackworktile?order_id={{$avalue->id}}&back_url={{$back_url}}">{{__('track.applyDetail')}}</a>
                            @if($avalue->is_remind==0)
                                @if($avalue->status==1)
                                    <a class="btn btn-info" href="/track/updateStatus?order_id={{$avalue->id}}&back_url={{$back_url}}">{{__('track.lock')}}</a>
                                @else
                                    <a class="btn btn-custom" href="/track/updateStatus?order_id={{$avalue->id}}&back_url={{$back_url}}">{{__('track.unlock')}}</a>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                @endif
                </tbody>
            </table>
            <div class="diy-paginate">
                <form action="/track/checktrack" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">{{ $orders->appends($pageCondition)->render() }}</div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
        </section>
    </div>
</section>
