<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <!--   <legend>Personal Information</legend> -->
        @if (!empty($order))
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('basic.client.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $user->name }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.ktp.number')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $user->ktp_number }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.phone.ask.personal.phone')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $user->phone_number }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.deadline.date')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->payment_date }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('basic.apply.amount')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->apply_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.overdue.interest')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->overdue_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
            <div class="form-group">
                <label class="col-md-3 col-sm-2 control-label">@lang('message.amount.returned')</label>
                <div class="col-md-6 col-sm-6">
                    <input type="text" value="{{ $order->already_repayment_amount }}" class="form-control"  disabled="true" style="border:none;">
                </div>
            </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.should.payment')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->repayment_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.repayment.date')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->loan_deadline }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.suspend.data')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->loan_suspend_date }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.overdue.data')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $order->loan_overdue_date }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>

        @endif
        <div class="modal-footer">
            <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
        </div>
    </fieldset>
</form>