<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('track/_track_add')
        <div class="wrapper">
            @include('track/_track_check')
            @include('track/_track_list')
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 详情的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">详情</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 详情的结束 -->
@include('public/base_script')

<script>
    $(".track-detail").click(function(){
        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/track/detail',
            data:{
                order_id: id
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#myModal .modal-body').html(json);
        }).error(function(){
            alert('详情信息查询失败');
        })
    });
</script>
</body>
</html>