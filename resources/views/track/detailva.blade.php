<style type="text/css">
    .fill_ipt {
        position: relative;
        /*margin: 200px 0 0 200px;*/
    }

    .showbig {
        display: block;
        width: 100%;
        height: 40px;
        border: 1px solid #404040;
    }

    input {
        display: block;
        width: 100%;
        height: 100%;
        outline: none;
        border: none;
        text-indent: 20px;
    }

    .bigtx {
        height: 30px;
        min-width: 10px;
        line-height: 30px;
        padding: 0px 8px;
        border: 1px solid #C9CFDA;
        background: #f5f7fc;
        box-shadow: 0 0 10px rgba(120, 144, 156, 0.2);
        position: absolute;
        left: 0;
        top: -0px;
        font-size: 24px;
        color: #1c2b36;
        line-height: normal;
        /*display: none;*/
        opacity: 0;
        transition: all 0.2s;
    }

    .bigtx span {
        font-size: 24px;
        color: #1c2b36;
    }
</style>
<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('track.orderRePayAmount')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" id="needPrice" value="@if(!empty($price)) {{$price}} @endif" class="form-control"
                       disabled="true" style="border:none;">
            </div>
        </div>
        <br/>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('track.amountOfRepayment')</label>
            <div class="col-md-6 col-sm-6 fill_ipt">
                <input type="text" value="" onkeyup="this.value=this.value.replace(/\D/g,'')"
                       onafterpaste="this.value=this.value.replace(/\D/g,'')" id="price" class="form-control showbig">
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" id='generating' class="btn btn-info finish">@lang('track.generate')</button>
            <button type="button" id="customClose" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
        </div>
    </fieldset>
</form>
<script>
    //金额输入放大显示 Start
    $(".showbig").focus(function () {
        var _fill_ipt = $(this).parent(".fill_ipt");
        if (_fill_ipt.find('.bigtx').size() === 0) {
            var tar = "<div class='bigtx'><span></span><i></i></div>";
            _fill_ipt.append(tar)
        }
    }).bind('input propertychange', function () {
        //console.log(1)
        var tx = $(this).val();

        var _format_value = tx;
        if (_format_value !== '' && !isNaN(_format_value)) {
            var _value_array = _format_value.split('.');
            var _int = _value_array[0];
            var _decimal = '';
            if (_value_array.length > 1) {
                _decimal = _value_array[1];
            }
            var _int_str = '';
            var _count = 0;

            for (var i = _int.length - 1; i >= 0; i--) {
                _count++;
                _int_str = _int.charAt(i) + _int_str;
                if (!(_count % 3) && i !== 0) {
                    _int_str = ',' + _int_str;
                }
            }

            _format_value = _int_str;

            if (_decimal !== '') {
                _format_value += '.' + _decimal;
            }

        }

        $(this).siblings(".bigtx").css({
            "top": "-37px",
            "opacity": "1"
        });
        $(this).siblings(".bigtx").find("span").html(_format_value);
        if (_format_value === "") {
            $(this).siblings(".bigtx").css({
                "top": "0",
                "opacity": "0"
            })
        }
    }).blur(function () {

        $(this).siblings(".bigtx").css({
            "top": "0",
            "opacity": "0"
        });
        var _fill_ipt = $(this).parent(".fill_ipt");
        if (_fill_ipt.find('.bigtx').size() > 0) {
            setTimeout(function () {
                _fill_ipt.find('.bigtx').remove();
            }, 200);
        }
    });
    //金额输入放大显示 End
    //提示信息
    function message(info) {
        $.alert({
            title: '@lang("message.make.sure.title")',
            content: info,
            buttons: {
                okay: {
                    text: 'OK',
                    btnClass: 'btn-info'
                }
            }
        });
    }

    $('#generating').click(function () {
        var price = $('#price').val();//输入的还款金额
        var needPrice = $('#needPrice').val();//获取应还金额
        var sub = price.substring(price.length, price.length - 2);//截取后两位
        if (needPrice * 1 < price * 1) {
            message('@lang('track.overSize')');
            return false;
        }
        if (price * 1 < 10000 * 1) {
            message('@lang('track.checkSize')');
            return false;
        }
        if (sub != '00') {
            message('@lang('track.subTwo')');
            return false;
        }
        var uid = '{{$uid}}';
        var order_id = '{{$order_id}}';
        var bank_code = '{{$bank_code}}';
        $.ajax({
            url: "/track/getVA",
            type: "GET",
            data: {
                'uid': uid,
                'order_id': order_id,
                'bank_code': bank_code,
                'price': price,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function (data) {
                $('#repaymentBtn').attr('disabled',false);
                $('#customClose').click();
                if (data.code == 2) {
                    $.alert({
                        title: '@lang("message.make.sure.title")',
                        content: '@lang("track.getBankFailure")',
                        buttons: {
                            okay: {
                                text: 'OK',
                                btnClass: 'btn-info'
                            }
                        }
                    });
                } else {
                    $.alert({
                        title: '@lang("message.make.sure.title")',
                        content: data.data.bankName + ': <span style="color: green; font-size: 20px; font-weight: bold;">' + data.data.bankCardNumber + '</span>',
                        buttons: {
                            okay: {
                                text: 'OK',
                                btnClass: 'btn-info'
                            }
                        }
                    });
                }
            }
        });
    })
</script>