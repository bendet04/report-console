<section class="panel">
    <div class="panel-body">
        <div class="col-md-3 col-sm-6 diy-search-input-box">
            <select class="form-control diy-search-input" id="searchId">
                <option value="">select</option>
                <option value="order_number">{{__('final.number')}}</option>
                <option value="name">{{__('final.clientName')}}</option>
                <option value="ktp_number">{{__('final.ktp')}}</option>
                <option value="phone_number">{{__('final.phone')}}</option>
                <option value="loan_period">{{__('final.payPeriod')}}</option>
                <option value="apply_amount">{{__('final.applyAmount')}}</option>
                <option value="created_at">{{__('final.applyTime')}}</option>
                <option value="order_status">{{__('final.status')}}</option>
            </select>
        </div>
        <span class="tools pull-right">
            @if(Auth::user()->work_status==2)
                <a href="/switchStatus?workStatus=0" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px;margin: 8px;">@lang('message.stopWork')</a>
            @else
                <a href="/switchStatus?workStatus=2" class="btn btn-xs btn-success" style="background-color: #5cb85c; color: #fff; height: 30px; line-height: 10px; margin: 8px;">@lang('message.startWork')</a>
            @endif
        </span>
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('message.order.number')</th>
                    <th>@lang('message.platform')</th>
                    <th class="numeric">@lang('message.client.name')</th>
                    <th class="numeric">KTP</th>
                    <th class="numeric">@lang('message.phone')</th>
                    <th class="numeric">@lang('message.loan.period')</th>
                    <th class="numeric">@lang('message.loan.amount')</th>
                    <th class="numeric">@lang('message.create.time')</th>
                    <th class="numeric">@lang('message.auth')</th>
                    <th class="numeric">@lang('message.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($orders as $k => $v)
                    <tr>
                        <td>{{$k + 1}}</td>
                        <td>
                            {{$v->order_number}}
                            @if($v->work_phone_status == 2 || $v->phone_status == 2)
                                <br/>
                                <font color="red">@lang('order.no.answer')</font>
                            @endif
                        </td>
                        <td>{{\App\Consts\Platform::alias($v->platform)}}</td>
                        <td>{{$v->name}}</td>
                        <td>{{$v->ktp_number}}</td>
                        <td>{{$v->phone_number}}</td>
                        <td>{{$v->loan_period}}</td>
                        <td>{{$v->apply_amount}}</td>
                        <td>{{$v->order_time}}</td>
                        <td>
                            @if($v->auth)
                               @lang('message.Authorized')
                            @endif

                            @if(!$v->auth)
                                @lang('message.Unauthorized')
                            @endif
                        </td>
                        <td>{{$v->order_status}}</td>
                        <td><a href="/phone/workDetail/id/{{$v->id}}/info/0/type/0">@lang('message.trial')</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if($orders->count())
            <div class="diy-paginate">
                <form action="/phone/phoneTrial" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">{{ $orders->render() }}</div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
            @endif

            <div id="empty_data" @if($orders->count())style="display: none;"@endif>@lang('message.list_not_data_notice') <a href="/refresh?worktype=2" class="btn btn-success">@lang('message.refresh')</a></div>

        </section>
    </div>
</section>