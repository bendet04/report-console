<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body>
@include('public/left')
<section>
    <div class="main-content" id="reloadPhoneTrial">
        @include('public/right_header')
        <div class="wrapper">
            @if($errors->any())
                <div class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <section class="panel">
                @if($order)
                    @if($order->work_phone_status == 0 || $order->work_phone_status ==2 || $info ==1)
                        @include('phone._work_detail')
                    @else
                        @include('phone._personal_detail')
                    @endif
                @endif
            </section>
        </div>
        @include('public/bottom')
    </div>
</section>

@include('public/base_script')
</body>
</html>


