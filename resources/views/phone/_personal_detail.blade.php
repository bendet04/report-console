 <section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            @if($order)
                <table class="table table-bordered table-striped table-condensed cf personal-table-box" style="border: hidden;text-align:center;valign:center">
                    <thead class="cf">
                    <tr>
                        <th colspan="10">
                            {{--<a href="/phone/phoneTrial" style="color: #5276eb;size:14px;font-weight:bolder">--}}
                            @if($type)
                                <a href="/order/orderDetail/id/{{$id}}" style="color: #5276eb;size:14px;font-weight:bolder">
                                    < @lang('message.reback')
                                </a>
                            @else
                                <a href="/phone/phoneTrial" style="color: #5276eb;size:14px;font-weight:bolder">
                                    < @lang('message.reback')
                                </a>
                            @endif
                            <span>@lang('message.platform'): <span class="platform" style="color: red; font-size: 30px;">{{\App\Consts\Platform::alias($order['platform'])}}</span></span>
                        </th>
                    </tr>
                    <tr style="height: 60px;">
                        <th style="text-align:center;color: #999;font-weight:bolder;font-size: 24px;width:578px;height: 60px;" colspan="6">
                            <a style="" href="/phone/workDetail/id/{{$id}}/info/1/type/{{$type}}">
                                @lang('message.phone.ask.have.finish'):@lang('message.phone.trial.work')</a>
                        </th>

                        <th style="text-align:center;background: #5276eb;color: #ffffff;font-weight:bolder;font-size: 24px;width:578px;height: 60px;" colspan="4">
                            <a style="color:#FFFFFF" href="/phone/workDetail/id/{{$id}}/info/0/type/{{$type}}">
                                @lang('message.phone.now'):@lang('message.phone.trial.personal')
                            </a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 143px;vertical-align:bottom;font-size: 20px;">
                            @lang('message.phone.ask.personal.phone')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;"  >
                            @lang('order.source'): {{$order->user->phone_number}} @if(!empty($admin->phone_user) && !empty($admin->phone_pass))<i data-phone="{{$order->user->phone_number}}" data-user="{{$admin->phone_user}}" data-pass="{{$admin->phone_pass}}" class="fa fa-phone phoneBtn" style="font-size: 30px"></i>@endif
                            @if (!empty($order->npwp['profile']['phone']))
                                <br />@lang('order.sourceNpwp'): {{$order->npwp['profile']['phone']}}
                            @endif
                            @if (!empty($order->bpjs['data'][0]['user_mobile']))
                                <br />@lang('order.sourceBpjs'): {{$order->bpjs['data'][0]['user_mobile']}}
                            @endif
                        </td>
                        {{--<td style="" colspan="4">--}}
                        <td style="" >

                            {{--有效电话已接通--}}
                            <button class="btn person-valid" person-phone="1" style="width:95px;white-space:normal;font-size: 14px;padding: initial" type="button">
                                @lang('message.phone.company.vailiad')
                            </button>
                            <input type="text" style="border-radius: 4px;background-color:#fff;color:#555;border:1px solid #ccc;width:118px;height:40px;margin-top:10px"  maxlength="25"  id="phone_number_notice">
                            {{--有效电话已接通--}}
                        </td>
                        <td>
                            @if($order->phone_status !=2)
                            <button class="btn person-no-answer"  person-phone="2" style="width:95px;white-space:normal;margin-left: 15px;font-size: 14px;padding: initial" type="button">
                                @lang('message.personal.modify.ren.answer')
                            </button>
                            @endif

                        </td>
                        <td>
                            <button class="btn btn-lg person-invalid" person-phone="3" style="width:95px;white-space:normal;margin-left: 1px;font-size: 14px;padding: initial" type="button">
                                @lang('message.phone.company.no.vailiad')
                            </button>
                        </td>
                        <td>
                            {{--start 节假日无人接听--}}
                            <button class="btn person-holiday" person-phone="4" style="width:95px;white-space:normal;font-size: 14px;padding: initial" type="button">
                                @lang('message.personal.modify.holiday')
                            </button>
                            {{--end  节假日无人接听--}}
                        </td>
                        <td style="text-align: center;" colspan="3" rowspan="3" >
                            <video style="width: 230px;height: 200px;" controls>
                                <source src="{{env('CDN_URL').$order->user->video_url}}" type="video/mp4">
                            </video>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 143px;vertical-align:bottom;font-size: 20px;">
                            @lang('message.client.name')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;"  >
                            {{$order->user->name}}
                        </td>
                        <td style="width:40px;" colspan="2">
                            <button class="btn  btn-lg person-one person-name" person-name="1" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;" colspan="2">
                            <button class="btn  btn-lg person-one person-name" id="checkClientName" person-name="2" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                        </td>
                    </tr>
                    <span id="orderId" data="{{$id}}"></span>
                    <tr>
                        <td style="width: 143px;vertical-align:bottom;font-size: 20px;">
                            KTP
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;white-space:nowrap;"  >
                            {{$order->user->ktp_number}}
                        </td>
                        <td style="width:40px;" colspan="2">
                            <button class="btn  btn-lg person-one person-ktp" person-ktp="1" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;" colspan="2">
                            <button class="btn  btn-lg person-one person-ktp" id="checkKtp" person-ktp="2" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                        </td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td style="width: 143px;vertical-align:bottom;font-size: 20px;">--}}
                            {{--@lang('message.phone.ask.personal.marriage')--}}
                        {{--</td>--}}
                        {{--<td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;white-space:nowrap"  >--}}
                            {{--{{\App\Consts\Married::toString($order->user->marriage)}}--}}
                        {{--</td>--}}
                        {{--<td style="width:40px;" colspan="2">--}}
                            {{--<button class="btn  btn-lg person-one person-marriage" style="width:130px;font-size: 14px;height: 40px;padding: initial" person-marriage="1" type="button">--}}
                                {{--@lang('message.phone.company.ask.same')--}}
                            {{--</button>--}}
                        {{--</td>--}}
                        {{--<td style="width:20px;" colspan="2">--}}
                            {{--<button class="btn  btn-lg person-one person-marriage"  style="width:130px;font-size: 14px;height: 40px;padding: initial;white-space:normal;" person-marriage="2" type="button">--}}
                                {{--@lang('message.phone.company.ask.diffence')--}}
                            {{--</button>--}}
                        {{--</td>--}}
                        {{--<td style="text-align: center;white-space:normal;width:200px;" colspan="3" rowspan="2" >--}}

                            {{--<div style="font-size: 30px;font-weight: bolder;color: #333333">@lang('message.phone.problem')</div><br />--}}
                            {{--<div style="font-size: 20px;color: #333333;text-align: center">@lang('message.phone.bottom')</div>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td style="width: 143px;vertical-align:bottom;font-size: 20px;">
                            @lang('message.phone.ask.personal.pay.type')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;white-space:normal;"  >
                            {{\App\Consts\PayType::toString($order->pay_type)}}
                        </td>
                        <td style="width:40px;" colspan="2">
                            <button class="btn  btn-lg person-one person-pay" person-pay="1" style="width:130px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;" colspan="2">
                            <button class="btn  btn-lg person-one person-pay" person-pay="2" style="width:130px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                        </td>
                        <td style="text-align: center;white-space:normal;width:200px;" colspan="3" rowspan="1" >

                        <div style="font-size: 15px;font-weight: bolder;color: #333333">@lang('message.phone.problem')</div><br />
                        <div style="font-size: 10px;color: #333333;text-align: center">@lang('message.phone.bottom')</div>
                        </td>

                    </tr>
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            @lang('message.phone.home.address')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;white-space:normal"  >
                            {{$order->user->province}}
                            {{$order->user->rengence}}
                            {{$order->user->district}}
                            {{$order->user->villages}}
                            {{$order->user->address}}
                        </td>
                        <td style="width:40px;vertical-align: middle" colspan="2">
                            <button class="btn  btn-lg person-one person-address" person-address="1" style="width:130px;font-size: 14px;" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;vertical-align: middle" colspan="2">
                            <button class="btn  btn-lg person-one person-address" person-address="2" style="width:130px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                            <input type="text" style="border-radius: 4px;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-top:10px"  maxlength="50" id="home-address-notice">
                        </td>
                        <td style="text-align: center;vertical-align: middle;" colspan="4" rowspan="4" >
                            <button class="btn btn-more btn-nervous" data-nervous="1" style="width:140px;white-space:normal;font-size: 14px;height: 50px;padding: initial" type="button">
                                @lang('message.phone.ask.applyer.nervous')
                            </button>
                            <button class="btn btn-more btn-teach" data-teach="1" style="width:140px;white-space:normal;font-size: 14px;height: 50px;padding: initial" type="button">
                                @lang('message.phone.ask.other.teach')
                            </button><br /><br />
                            <button class="btn btn-more btn-other-take" data-other="1" style="width:140px;white-space:normal;font-size: 14px;height: 50px;padding: initial" type="button">
                                @lang('message.phone.ask.other.take')
                            </button>
                            <button class="btn btn-more btn-fluent" data-fluent="1" style="width:140px;white-space:normal;font-size: 14px;height: 50px;padding: initial" type="button">
                                @lang('message.phone.ask.other.fluent')
                            </button>
                        </td>
                    </tr>
                    {{--whatsapp start--}}
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            Whatsapp
                        </td>
                        <td colspan="3"  style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;"  >
                            {{$order->user->whatsapp}}
                        </td>

                        <td colspan="4" >
                            <table  style="cellspacing:0;cellpadding:0;width: 100%;height: 100%">
                                <tr>
                                    <td style="border:1px solid gainsboro" colspan="3">
                                        <button class="btn  btn-lg person-one person-whatsapp" person-whatsapp="1" style="width:130px;font-size: 14px;" type="button">
                                            @lang('message.phone.company.ask.same')
                                        </button>
                                    </td>

                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-whatsapp" person-whatsapp="2" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">
                                            @lang('message.phone.company.ask.diffence')
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <input type="text" style="border-radius: 4px;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-left:50px;margin-top:10px"  maxlength="50" id="whatsApp-notice">
                                    </td>

                                </tr>
                            </table>
                        </td>
                        {{--<td style="width:40px;" colspan="2">--}}
                            {{--<button class="btn  btn-lg person-one person-name" person-name="1" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">--}}
                                {{--@lang('message.phone.company.ask.same')--}}
                            {{--</button>--}}
                        {{--</td>--}}
                        {{--<td style="width:20px;" colspan="2">--}}
                            {{--<button class="btn  btn-lg person-one person-name" id="checkClientName" person-name="2" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">--}}
                                {{--@lang('message.phone.company.ask.diffence')--}}
                            {{--</button>--}}
                        {{--</td>--}}
                    </tr>
                    {{--whatsapp end--}}
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            {{\App\Consts\Relative::toFirstString($order->user->relative_type_first)}}
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:normal;"  >
                            {{$order->user->relative_name_first}}<br />
                            {{$order->user->relative_phone_first}}
                        </td>
                     {{--联系人1--}}
                    <td colspan="4" >
                        <table  style="cellspacing:0;cellpadding:0;width: 100%;height: 100%">
                            <tr>
                                <td style="border:1px solid gainsboro" colspan="3">
                                    <button class="btn  btn-lg person-one person-first" person-first="1" style="width:130px;font-size: 14px;" type="button">
                                    @lang('message.phone.company.ask.same')
                                    </button>
                                </td>

                                <td style="border:1px solid gainsboro">
                                    <button class="btn  btn-lg person-one person-first" person-first="2" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">
                                    @lang('message.phone.company.ask.diffence')
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <input style="border-radius: 4px;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-left:50px;margin-top:10px"   type="text" maxlength="25" id="first-person-notice">
                                </td>

                            </tr>
                        </table>
                    </td>
                        {{--<td>--}}
                            {{--<table border="1"><tr><td border="2" colspan="3">1111</td></tr></table>--}}
                        {{--</td>--}}
                        {{--<td style="width:40px;vertical-align: middle" colspan="2">--}}
                            {{--<button class="btn  btn-lg person-one person-first" person-first="1" style="width:130px;font-size: 14px;" type="button">--}}
                                {{--@lang('message.phone.company.ask.same')--}}
                            {{--</button>--}}
                        {{--</td>--}}
                        {{--<td style="width:20px;vertical-align: middle" colspan="2">--}}
                            {{--<button class="btn  btn-lg person-one person-first" person-first="2" style="width:130px;font-size: 14px;height: 40px;padding: initial" type="button">--}}
                                {{--@lang('message.phone.company.ask.diffence')--}}
                            {{--</button>--}}
                        {{--</td>--}}
                    </tr>
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            {{\App\Consts\Relative::toFirstString($order->user->relative_type_second)}}
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;white-space:normal"  >
                            {{$order->user->relative_name_second}}<br />
                            {{$order->user->relative_phone_second}}
                        </td>
                        {{--联系人2--}}
                        <td colspan="4" >
                            <table  style="cellspacing:0;cellpadding:0;width: 100%;height: 100%">
                                <tr>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-second" person-second="1" style="width:130px;font-size: 14px;" type="button">
                                            @lang('message.phone.company.ask.same')
                                        </button>
                                    </td>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-second" person-second="2" style="width:130px;font-size: 14px;height: 40px;padding: initial;" type="button">
                                            @lang('message.phone.company.ask.diffence')
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <input type="text" style="border-radius: 4px;box-shadow:none;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-left:50px;margin-top:10px"   maxlength="25" id="two-person-notice">
                                    </td>
                                </tr>

                            </table>
                        </td>
                        {{--<td style="width:40px;vertical-align: middle" colspan="2">--}}
                        {{--<button class="btn  btn-lg person-one person-second" person-second="1" style="width:130px;font-size: 14px;" type="button">--}}
                        {{--@lang('message.phone.company.ask.same')--}}
                        {{--</button>--}}
                        {{--</td>--}}
                        {{--<td style="width:20px;vertical-align: middle" colspan="2">--}}
                        {{--<button class="btn  btn-lg person-one person-second" person-second="2" style="width:130px;font-size: 14px;height: 40px;padding: initial;" type="button">--}}
                        {{--@lang('message.phone.company.ask.diffence')--}}
                        {{--</button>--}}
                        {{--</td>--}}
                    </tr>
                    @if(!empty($order->user->relative_type_third))
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            {{\App\Consts\Relative::toFirstString($order->user->relative_type_third)}}
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:normal"  >
                            {{$order->user->relative_name_third}}<br />
                            {{$order->user->relative_phone_third}}
                        </td>
                        {{--联系人2--}}
                        <td colspan="4" >
                            <table  style="cellspacing:0;cellpadding:0;width: 100%;height: 100%">
                                <tr>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-third" person-third="1" style="width:130px;font-size: 14px;" type="button">
                                            @lang('message.phone.company.ask.same')
                                        </button>
                                    </td>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-third" person-third="2" style="width:130px;font-size: 14px;height: 40px;padding: initial;" type="button">
                                            @lang('message.phone.company.ask.diffence')
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <input type="text" style="border-radius: 4px;box-shadow:none;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-left:50px;margin-top:10px"   maxlength="25" id="third-person-notice">
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    @endif
                    @if(!empty($order->user->relative_type_fourth))
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            {{\App\Consts\Relative::toFirstString($order->user->relative_type_fourth)}}
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:normal"  >
                            {{$order->user->relative_name_fourth}}<br />
                            {{$order->user->relative_phone_fourth}}
                        </td>
                        {{--联系人3--}}
                        <td colspan="4" >
                            <table  style="cellspacing:0;cellpadding:0;width: 100%;height: 100%">
                                <tr>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-fourth" person-fourth="1" style="width:130px;font-size: 14px;" type="button">
                                            @lang('message.phone.company.ask.same')
                                        </button>
                                    </td>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-fourth" person-fourth="2" style="width:130px;font-size: 14px;height: 40px;padding: initial;" type="button">
                                            @lang('message.phone.company.ask.diffence')
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <input type="text" style="border-radius: 4px;box-shadow:none;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-left:50px;margin-top:10px"   maxlength="25" id="fourth-person-notice">
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    @endif
                    @if(!empty($order->user->relative_type_fifth))
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            {{\App\Consts\Relative::toFirstString($order->user->relative_type_fifth)}}
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:normal"  >
                            {{$order->user->relative_name_fifth}}<br />
                            {{$order->user->relative_phone_fifth}}
                        </td>
                        {{--联系人2--}}
                        <td colspan="4" >
                            <table  style="cellspacing:0;cellpadding:0;width: 100%;height: 100%">
                                <tr>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-fifth" person-fifth="1" style="width:130px;font-size: 14px;" type="button">
                                            @lang('message.phone.company.ask.same')
                                        </button>
                                    </td>
                                    <td style="border:1px solid gainsboro">
                                        <button class="btn  btn-lg person-one person-fifth" person-fifth="2" style="width:130px;font-size: 14px;height: 40px;padding: initial;" type="button">
                                            @lang('message.phone.company.ask.diffence')
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <input type="text" style="border-radius: 4px;box-shadow:none;background-color:#fff;color:#555;border:1px solid #ccc;width:276px;height:40px;margin-left:50px;margin-top:10px"   maxlength="25" id="fifth-person-notice">
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    @endif
                    @if($order->order_status == 19)
                    <tr>
                        <td colspan="10" style="vertical-align: middle;text-align: center;">
                            <button class="btn  btn-lg btn-agree" style="vertical-align: middle;text-align: center;padding: initial;width:330px;height: 40px;">
                                @lang('message.trial.success')
                            </button>
                            <button class="btn  btn-lg btn-refuse" style="vertical-align: middle;text-align: center;padding: initial;width:330px;height: 40px;">
                                @lang('message.trial.failure')
                            </button>
                        </td>
                    </tr>
                     @endif
                    </tbody>
                </table>
            @endif
        </section>
    </div>
</section>
<script>
    {{--姓名不一致--}}
    $("#checkClientName").on('click', function(){

        if(!($('.person-valid').hasClass('btn-success')))
            return false;
        var orderId = $("#orderId").attr('data');
        //电审信息备注
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;
        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;
        //电审信息备注
        var thisClass = $(this);
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.client.name')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"post",
                        data:{
                            phone_number: 1,
                            name: 2,
                            ktp_number: 0,
                            pay_type: 0,
                            marriage: 0,
                            address: 0,
                            address_notice:address_notice,
                            whatsApp_notice:whatsApp_notice,
                            relative_first_notice:relative_first_notice,
                            relative_two_notice:relative_two_notice,
                            relative_third_notice:relative_third_notice,
                            relative_fourth_notice:relative_fourth_notice,
                            relative_fifth_notice:relative_fifth_notice,
                            phone_number_notice:phone_number_notice,
                            whatsApp:0,
                            relative_first: 0,
                            relative_second: 0,
                            relative_third: 0,
                            relative_fourth: 0,
                            relative_fifth: 0,
                            work_remark: {
                                nervous:0,
                                teach:0,
                                other:0,
                                fluent:0,
                            },
                            status:22,
                            order_id:orderId,
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    thisClass.removeClass('btn-danger')
                }
            }
        });

    })
//    ktp号码不一致
    $("#checkKtp").on('click', function(){

        if(!($('.person-valid').hasClass('btn-success')))
            return false;
        var orderId = $("#orderId").attr('data');
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;

        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;

        var thisClass = $(this);
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.ktp.number')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"post",
                        data:{
                            phone_number: 1,
                            name: 1,
                            ktp_number:2,
                            address_notice:address_notice,
                            whatsApp_notice:whatsApp_notice,
                            relative_first_notice:relative_first_notice,
                            relative_two_notice:relative_two_notice,
                            relative_third_notice:relative_third_notice,
                            relative_fourth_notice:relative_fourth_notice,
                            relative_fifth_notice:relative_fifth_notice,
                            phone_number_notice:phone_number_notice,
                            whatsApp:0,
                            pay_type: 0,
                            marriage: 0,
                            address: 0,
                            relative_first: 0,
                            relative_second: 0,
                            relative_third: 0,
                            relative_fourth: 0,
                            relative_fifth: 0,
                            work_remark: {
                                nervous:0,
                                teach:0,
                                other:0,
                                fluent:0,
                            },
                            status:22,
                            order_id:orderId,
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    thisClass.removeClass('btn-danger')
                }
            }
        });

    })
    {{--拨打有效电话--}}
    $(document).on('click', '.person-valid', function () {
        $(this).parent().parent().find('button').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-info').removeClass('btn-warning');
        $(this).addClass('btn-success');
    });
//    无人接通的按钮处理
    $(document).on('click', '.person-no-answer',function () {
       $(this).parent().parent().find('button').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-info').removeClass('btn-warning');
       $('.person-one').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning');
       $('.btn-more').removeClass('btn-warning');
       $('.btn-finish').removeClass('btn-info')
       $(this).addClass('btn-danger');

        var phone_number = $(this).attr('person-phone');
        var orderId = $("#orderId").attr('data');
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;
        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.add.no.answer')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"POST",
                        data:{
                            order_id:orderId,
                            phone_number: phone_number,
                            name: 0,
                            ktp_number: 0,
                            pay_type: 0,
                            marriage: 0,
                            address: 0,
                            address_notice:address_notice,
                            whatsApp_notice:whatsApp_notice,
                            relative_first_notice:relative_first_notice,
                            relative_two_notice:relative_two_notice,
                            relative_third_notice:relative_third_notice,
                            relative_fourth_notice:relative_fourth_notice,
                            relative_fifth_notice:relative_fifth_notice,
                            phone_number_notice:phone_number_notice,
                            whatsApp:0,
                            relative_first: 0,
                            relative_second: 0,
                            relative_third: 0,
                            relative_fourth: 0,
                            relative_fifth: 0,
                            work_remark: {
                                nervous:0,
                                teach:0,
                                other:0,
                                fluent:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2 || data.code == 3){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });


    });
//    节假日处理
    $(document).on('click','.person-holiday', function () {
        // 删除同级的属性
        $(this).parent().parent().find('button').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning');
        $('.person-one').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning');
        $('.btn-more').removeClass('btn-warning');
        $('.btn-finish').removeClass('btn-info')
        $(this).addClass('btn-info');
        var phone_number = $(this).attr('person-phone');
        var orderId = $("#orderId").attr('data');
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;
        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.add.holiday')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/phoneTrialDetail",
                        type:"post",
                        data:{
                            order_id:orderId,
                            phone_number: phone_number,
                            company_name: 0,
                            career: 0,
                            income_level: 0,
                            work_period: 0,
                            company_address: 0,
                            address_notice:address_notice,
                            whatsApp_notice:whatsApp_notice,
                            relative_first_notice:relative_first_notice,
                            relative_two_notice:relative_two_notice,
                            relative_third_notice:relative_third_notice,
                            relative_fourth_notice:relative_fourth_notice,
                            relative_fifth_notice:relative_fifth_notice,
                            phone_number_notice:phone_number_notice,
                            whatsApp:0,
                            paid_period: 0,
                            levelRemark:0,
                            periodRemark:0,
                            payRemark:0,
                            work_remark: {
                                env:0,
                                ear:0,
                                zzz:0,
                                other:0,
                                second:0,
                                one:0,
                                now:0,
                                slow:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
//                            var orderId = data.orderId;
//                            window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });


    });
//    号码无效
    $(document).on('click', '.person-invalid', function () {
        $(this).parent().parent().find('button').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-info').removeClass('btn-warning');
        $('.person-one').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning');
        $('.btn-more').removeClass('btn-warning');
        $('.btn-finish').removeClass('btn-info')
        $(this).addClass('btn-warning');

        var phone_number = $(this).attr('person-phone');
        var orderId = $("#orderId").attr('data');
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;
        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.phone.number.invalid')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"POST",
                        data:{
                            order_id:orderId,
                            status:22,
                            phone_number: phone_number,
                            name: 0,
                            ktp_number: 0,
                            pay_type: 0,
                            marriage: 0,
                            address: 0,
                            address_notice:address_notice,
                            whatsApp_notice:whatsApp_notice,
                            relative_first_notice:relative_first_notice,
                            relative_two_notice:relative_two_notice,
                            relative_third_notice:relative_third_notice,
                            relative_fourth_notice:relative_fourth_notice,
                            relative_fifth_notice:relative_fifth_notice,
                            phone_number_notice:phone_number_notice,
                            whatsApp:0,
                            relative_first: 0,
                            relative_second: 0,
                            relative_third: 0,
                            relative_fourth: 0,
                            relative_fifth: 0,
                            work_remark: {
                                nervous:0,
                                teach:0,
                                other:0,
                                fluent:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });
    });

//  单击按钮
    $(document).on('click', '.person-one',function () {

        if(!($('.person-valid').hasClass('btn-success')))
            return false;
//        if(!($(this).parent().parent().find('button').hasClass('person-one')))
//            return false;

        $(this).parent().parent().find('button').removeClass('btn-success').removeClass('btn-danger');
//        姓名的判断
        if($(this).attr('person-name') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-name') == 2)
            $(this).addClass('btn-danger');

//        ktp的判断
        if($(this).attr('person-ktp') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-ktp') == 2)
            $(this).addClass('btn-danger');

//        婚姻的判断
//         if($(this).attr('person-marriage') == 1)
//             $(this).addClass('btn-success');
//         else if($(this).attr('person-marriage') == 2)
//             $(this).addClass('btn-danger');

        //        婚姻的判断
         if($(this).attr('person-whatsapp') == 1)
             $(this).addClass('btn-success');
         else if($(this).attr('person-whatsapp') == 2)
             $(this).addClass('btn-danger');

//        借款用途的判断
        if($(this).attr('person-pay') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-pay') == 2)
            $(this).addClass('btn-danger');

//        用户住址的判断
        if($(this).attr('person-address') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-address') == 2)
            $(this).addClass('btn-danger');

//        联系人1的判断
        if($(this).attr('person-first') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-first') == 2)
            $(this).addClass('btn-danger');

//        联系人2的判断
        if($(this).attr('person-second') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-second') == 2)
            $(this).addClass('btn-danger');

//        联系人3的判断
        if($(this).attr('person-third') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-third') == 2)
            $(this).addClass('btn-danger');

//        联系人4的判断
        if($(this).attr('person-fourth') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-fourth') == 2)
            $(this).addClass('btn-danger');

//        联系人5的判断
        if($(this).attr('person-fifth') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-fifth') == 2)
            $(this).addClass('btn-danger');
//        var same = $('.btn-success').size();
//        var difference = $('.btn-danger').size();
//        var num = same + difference  - 1;
//        if(num == 7){
//            $('.btn-finish').addClass('btn-info')
//        }
    });

//    家庭住址的以下的单选按钮
    $(document).on('click', '.data-person-one',function () {

        if (!($('.person-valid').hasClass('btn-success')))
            return false;

        if($(this).attr('person-address') == 1)
            $(this).addClass('btn-success');
        else if($(this).attr('person-address') == 2)
            $(this).addClass('btn-danger');
    });

//    多选按钮
    $(document).on('click', '.btn-more', function () {
        if(!($('.person-valid').hasClass('btn-success')))
            return false;

        $(this).toggleClass('btn-warning');

    });

//    通过审核
    $(document).on('click', '.btn-agree', function () {

        var same = $('.btn-success').size();
        var difference = $('.btn-danger').size();
        var num = same + difference;

        var maxNum = $('.personal-table-box>tbody>tr').length - 1;
        console.log(num);
        console.log(maxNum);
        if(num < maxNum){
            return false;
        }
        $(this).addClass('btn-success');

        var phone_number = 1;
        var name = $('.person-name.btn-danger').attr('person-name') == undefined ? $('.person-name.btn-success').attr('person-name') : $('.person-name.btn-danger').attr('person-name');
        var ktp_number = $('.person-ktp.btn-danger').attr('person-ktp') == undefined ? $('.person-ktp.btn-success').attr('person-ktp') : $('.person-ktp.btn-danger').attr('person-ktp');
        // var marriage = $('.person-marriage.btn-danger').attr('person-marriage') == undefined ? $('.person-marriage.btn-success').attr('person-marriage') : $('.person-marriage.btn-danger').attr('person-marriage');
        var marriage = 0;
        var pay_type = $('.person-pay.btn-danger').attr('person-pay') == undefined ? $('.person-pay.btn-success').attr('person-pay') : $('.person-pay.btn-danger').attr('person-pay');
        var address = $('.person-address.btn-danger').attr('person-address') == undefined ? $('.person-address.btn-success').attr('person-address') : $('.person-address.btn-danger').attr('person-address');
        var relative_first = $('.person-first.btn-danger').attr('person-first') == undefined ? $('.person-first.btn-success').attr('person-first') : $('.person-first.btn-danger').attr('person-first');
        var relative_second = $('.person-second.btn-danger').attr('person-second') == undefined ? $('.person-second.btn-success').attr('person-second') : $('.person-second.btn-danger').attr('person-second');
        var relative_third = $('.person-third.btn-danger').attr('person-third') == undefined ? $('.person-third.btn-success').attr('person-third') : $('.person-third.btn-danger').attr('person-third');
        var relative_fourth = $('.person-fourth.btn-danger').attr('person-fourth') == undefined ? $('.person-fourth.btn-success').attr('person-fourth') : $('.person-fourth.btn-danger').attr('person-fourth');
        var relative_fifth = $('.person-fifth.btn-danger').attr('person-fifth') == undefined ? $('.person-fifth.btn-success').attr('person-fifth') : $('.person-fifth.btn-danger').attr('person-fifth');
        var whatsapp = $('.person-whatsapp.btn-danger').attr('person-whatsapp') == undefined ? $('.person-whatsapp.btn-success').attr('person-whatsapp') : $('.person-whatsapp.btn-danger').attr('person-whatsapp');


//        申请人很紧张
        var nervous = $('.btn-nervous.btn-warning').attr('data-nervous') == undefined ? 0 : $('.btn-nervous.btn-warning').attr('data-nervous');
//        周围有人教他回答
        var teach = $('.btn-teach.btn-warning').attr('data-teach') == undefined ? 0 : $('.btn-teach.btn-warning').attr('data-teach');
//        不是本人接的电话
        var other = $('.btn-other-take.btn-warning').attr('data-other') == undefined ? 0 : $('.btn-other-take.btn-warning').attr('data-other');
//        对答如流
        var fluent = $('.btn-fluent.btn-warning').attr('data-fluent') == undefined ? 0 : $('.btn-fluent.btn-warning').attr('data-fluent');

        var orderId = $("#orderId").attr('data');

        // 电核备注
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;
        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;

        var thisClass = $(this);

        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.trial.success')",
            buttons: {
                "@lang('message.yes')": function () {
                $.ajax({
                    url:"/phone/addHolidayList",
                    type:"POST",
                    data:{
                        order_id:orderId,
                        phone_number: phone_number,
                        name: name,
                        status:21,
                        ktp_number: ktp_number,
                        pay_type: pay_type,
                        marriage: marriage,
                        address: address,
                        address_notice:address_notice,
                        whatsApp_notice:whatsApp_notice,
                        relative_first_notice:relative_first_notice,
                        relative_two_notice:relative_two_notice,
                        relative_third_notice:relative_third_notice,
                        relative_fourth_notice:relative_fourth_notice,
                        relative_fifth_notice:relative_fifth_notice,
                        phone_number_notice:phone_number_notice,
                        whatsApp:whatsapp,
                        relative_first: relative_first,
                        relative_second: relative_second,
                        relative_third: relative_third,
                        relative_fourth: relative_fourth,
                        relative_fifth: relative_fifth,
                        work_remark: {
                            nervous:nervous,
                            teach:teach,
                            other:other,
                            fluent:fluent,
                        },
                        _token:"{{csrf_token()}}"
                    },
                    dataType:"json",
                    success:function(data){
                        console.log(data.code);
                        if(data.code == 1){
                            var orderId = data.orderId;
                            window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                        }else if(data.code==11)
                        {
                            $.alert({
                                title: '审核结果',
                                content: data.info,
                                buttons: {
                                    okay: {
                                        text: 'OK',
                                        btnClass: 'btn-info'
                                    }
                                }
                            });
//                            window.location.replace('/phone/phoneTrial');

                        } else if(data.code ==2){
                            alert(data.info);
                            window.location.replace('/phone/phoneTrial');
                        } else {
                            alert(data.info);
                        }
                    }
                });
                },
                "@lang('message.cancel')": function () {
                    thisClass.removeClass('btn-success')
                }
            }
        });
    });
//    审核拒绝
    $(document).on('click', '.btn-refuse', function () {
        var same = $('.btn-success').size();
        var difference = $('.btn-danger').size();
        var num = same + difference  - 1;
        if(num < 6){
            return false;
        }
        $(this).addClass('btn-danger');

        var phone_number = 1;
        var name = $('.person-name.btn-danger').attr('person-name') == undefined ? $('.person-name.btn-success').attr('person-name') : $('.person-name.btn-danger').attr('person-name');
        var ktp_number = $('.person-ktp.btn-danger').attr('person-ktp') == undefined ? $('.person-ktp.btn-success').attr('person-ktp') : $('.person-ktp.btn-danger').attr('person-ktp');
        // var marriage = $('.person-marriage.btn-danger').attr('person-marriage') == undefined ? $('.person-marriage.btn-success').attr('person-marriage') : $('.person-marriage.btn-danger').attr('person-marriage');
        var marriage = 0;
        var pay_type = $('.person-pay.btn-danger').attr('person-pay') == undefined ? $('.person-pay.btn-success').attr('person-pay') : $('.person-pay.btn-danger').attr('person-pay');
        var address = $('.person-address.btn-danger').attr('person-address') == undefined ? $('.person-address.btn-success').attr('person-address') : $('.person-address.btn-danger').attr('person-address');
        var relative_first = $('.person-first.btn-danger').attr('person-first') == undefined ? $('.person-first.btn-success').attr('person-first') : $('.person-first.btn-danger').attr('person-first');
        var relative_second = $('.person-second.btn-danger').attr('person-second') == undefined ? $('.person-second.btn-success').attr('person-second') : $('.person-second.btn-danger').attr('person-second');
        var relative_third = $('.person-third.btn-danger').attr('person-third') == undefined ? $('.person-third.btn-success').attr('person-third') : $('.person-third.btn-danger').attr('person-third');
        var relative_fourth = $('.person-fourth.btn-danger').attr('person-fourth') == undefined ? $('.person-fourth.btn-success').attr('person-fourth') : $('.person-fourth.btn-danger').attr('person-fourth');
        var relative_fifth = $('.person-fifth.btn-danger').attr('person-fifth') == undefined ? $('.person-fifth.btn-success').attr('person-fifth') : $('.person-fifth.btn-danger').attr('person-fifth');
        var whatsapp = $('.person-whatsapp.btn-danger').attr('person-whatsapp') == undefined ? $('.person-whatsapp.btn-success').attr('person-whatsapp') : $('.person-whatsapp.btn-danger').attr('person-whatsapp');


//        申请人很紧张
        var nervous = $('.btn-nervous.btn-warning').attr('data-nervous') == undefined ? 0 : $('.btn-nervous.btn-warning').attr('data-nervous');
//        周围有人教他回答
        var teach = $('.btn-teach.btn-warning').attr('data-teach') == undefined ? 0 : $('.btn-teach.btn-warning').attr('data-nervous');
//        不是本人接的电话
        var other = $('.btn-other-take.btn-warning').attr('data-other') == undefined ? 0 : $('.btn-other-take.btn-warning').attr('data-nervous');
//        对答如流
        var fluent = $('.btn-fluent.btn-warning').attr('data-fluent') == undefined ? 0 : $('.btn-fluent.btn-warning').attr('data-nervous');
        console.log($('.btn-nervous.btn-warning').attr('data-nervous'));

        // 电核备注
        var address_notice = $('#home-address-notice').val() ? $('#home-address-notice').val() : 0;
        var whatsApp_notice = $('#whatsApp-notice').val() ? $('#whatsApp-notice').val() : 0;
        var relative_first_notice = $('#first-person-notice').val() ? $('#first-person-notice').val() : 0;
        var relative_two_notice = $('#two-person-notice').val() ? $('#two-person-notice').val() : 0;
        var relative_third_notice = $('#third-person-notice').val() ? $('#third-person-notice').val() : 0;
        var relative_fourth_notice = $('#fourth-person-notice').val() ? $('#fourth-person-notice').val() : 0;
        var relative_fifth_notice = $('#fifth-person-notice').val() ? $('#fifth-person-notice').val() : 0;
        var phone_number_notice = $('#phone_number_notice').val() ? $('#phone_number_notice').val() : 0;
        var orderId = $("#orderId").attr('data');

        var thisClass = $(this);

        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.trial.failure')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"POST",
                        data:{
                            order_id:orderId,
                            phone_number: phone_number,
                            name: name,
                            status:22,
                            ktp_number: ktp_number,
                            pay_type: pay_type,
                            marriage: marriage,
                            address: address,
                            address_notice:address_notice,
                            whatsApp_notice:whatsApp_notice,
                            relative_first_notice:relative_first_notice,
                            relative_two_notice:relative_two_notice,
                            relative_third_notice:relative_third_notice,
                            relative_fourth_notice:relative_fourth_notice,
                            relative_fifth_notice:relative_fifth_notice,
                            phone_number_notice:phone_number_notice,
                            whatsApp:whatsapp,
                            relative_first: relative_first,
                            relative_second: relative_second,
                            relative_third: relative_third,
                            relative_fourth: relative_fourth,
                            relative_fifth: relative_fifth,
                            work_remark: {
                                nervous:nervous,
                                teach:teach,
                                other:other,
                                fluent:fluent,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            console.log(data.code);
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            }else if(data.code==11)
                            {
                                $.alert({
                                    title: '审核结果',
                                    content: data.info,
                                    buttons: {
                                        okay: {
                                            text: 'OK',
                                            btnClass: 'btn-info'
                                        }
                                    }
                                });
//                            window.location.replace('/phone/phoneTrial');

                            }
                            else if(data.code ==2){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    thisClass.removeClass('btn-danger')
                }
            }
        });



    });

    $('.phoneBtn').click(function(){
        $(this).attr('disabled', true);
        var user = $(this).attr('data-user');
        var pass = $(this).attr('data-pass');
        var phone = $(this).attr('data-phone');

        var that = this;
        $.ajax({
            url:"/phone/call",
            type:"GET",
            data:{
                username:user,
                password:pass,
                phone:phone,
            },
            dataType:"json",
            success:function(data){
                if(data.result==1){
                    $(that).removeClass('fa-phone').addClass('fa-phone-square');
                }else{
                    alert(data.msg);
                    $(this).attr('disabled', false);
                }
            },
            error:function(){
                alert(data.msg);
                $(this).attr('disabled', false);
            }
        });
        return false;
    });
</script>