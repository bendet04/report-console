<section class="panel">
    <div class="panel-body">
        <section id="flip-scroll">
            @if($order)
            <table class="table table-bordered table-striped table-condensed cf" style="border: hidden;text-align:center;valign:center;vertical-align: middle;">
                <thead class="cf">
                <tr>
                    <th colspan="10">
                        @if($type)
                            <a href="/order/orderDetail/id/{{$id}}" style="color: #5276eb;size:14px;font-weight:bolder">
                                < @lang('message.reback')
                            </a>
                        @else
                            <a href="/phone/phoneTrial" style="color: #5276eb;size:14px;font-weight:bolder">
                                < @lang('message.reback')
                            </a>
                        @endif
                        <span>@lang('message.platform'): <span class="platform" style="color: red;font-size:30px">{{\App\Consts\Platform::alias($order['platform'])}}</span></span>
                    </th>
                </tr>
                    <tr style="height: 60px;">
                        <th style="text-align:center;background: #5276eb;color: #ffffff;font-weight:bolder;font-size: 24px;width:578px;height: 60px;" colspan="6">
                            @if($info == 1)
                            <a style="color: #ffffff;text-decoration:none;" href="/phone/workDetail/id/{{$id}}/info/1/type/{{$type}}">
                                @lang('message.phone.ask.have.finish'):
                                @lang('message.phone.trial.work')
                            </a>
                            @else
                                <a style="color: #ffffff;text-decoration:none;" href="javascript:return false;">
                                     @lang('message.phone.now'):
                                    @lang('message.phone.trial.work')
                                </a>
                            @endif
                        </th>
                        <th style="text-align:center;color: #999;font-weight:bolder;font-size: 24px;width:578px;height: 60px;" colspan="4">
                            @if($info == 1)
                                <a style="" href="/phone/workDetail/id/{{$id}}/info/0/type/{{$type}}">
                                    @lang('message.phone.now'):
                                    @lang('message.phone.trial.personal')
                                </a>
                            @else
                                <a style="text-decoration:none;" href="javascript:return false;">
                                    @lang('message.phone.next'):
                                    @lang('message.phone.trial.personal')
                                </a>
                            @endif
                         </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 143px;vertical-align:bottom;font-size: 20px;">
                            @lang('message.phone.company.phone')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;"  >
                            {{$order->user->company_phone}} @if(!empty($admin->phone_user) && !empty($admin->phone_pass))<i data-phone="{{$order->user->company_phone}}" data-user="{{$admin->phone_user}}" data-pass="{{$admin->phone_pass}}" class="fa fa-phone phoneBtn" style="font-size: 30px"></i>@endif
                        </td>
                        <td style="width:40px;vertical-align:middle;">
                            <button class="btn choose-valid
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['company_phone']))
                                    @if(json_decode($workTrialDetail->work, true)['company_phone'] == 1)
                                        btn-success
                                    @endif
                                @endif
                            @endif
" data-phone="1" style="white-space:normal;width:110px;font-size: 14px;padding: initial" type="button">
                                @lang('message.phone.company.vailiad')
                            </button>
                        </td>
                        <td style="width:20px;vertical-align:middle;">
                            <button class="btn
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['company_phone']))
                                    @if(json_decode($workTrialDetail->work, true)['company_phone'] == 2)
                                        btn-danger
                                    @endif
                                 @endif
                             @endif
                            no-valid" data-phone="3" style="white-space:normal;width:100px;height: 45px;padding: initial" type="button">
                                @lang('message.phone.company.no.vailiad')
                            </button>
                        </td>
                        @if($order->work_phone_status !=2)
                        <td style="width:20px;vertical-align:middle;">

                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['company_phone']))
                                     @if(json_decode($workTrialDetail->work, true)['company_phone'] == 2)
                                        btn-warning
                                    @endif
                                @endif
                            @endif
                            add-list" data-phone="2" style="white-space:normal;width:80px;padding: initial" type="button">
                                @lang('message.personal.modify.ren.answer')
                            </button>


                        </td>
                        @endif
                        <td style="vertical-align:middle;"
                            @if($order->work_phone_status ==2)
                            colspan="2"
                            @endif>
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['company_phone']))
                                     @if(json_decode($workTrialDetail->work, true)['company_phone'] == 4)
                                        btn-info
                                    @endif
                                @endif
                            @endif
                            holiday-list" data-phone="4" style="white-space:normal;width:90px;height: 45px;padding: initial" type="button">
                                @lang('message.personal.modify.holiday')
                            </button>

                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['company_phone']))
                                    @if(json_decode($workTrialDetail->work, true)['company_phone'] == 5)
                                            btn-info
                                    @endif
                                @endif
                            @endif
                            work-permit" data-phone="5" style="white-space:normal;width:90px;height: 45px;padding: initial" type="button">
                                @lang('message.phone.company.permit')
                            </button>
                        </td>

                        <td style="" colspan="3" rowspan="7" >
                            <img data-magnify="gallery" data-src="{{env('CDN_URL').$order->user->work_image_url}}" data-caption=" " style="width: 230px;height: 200px;" src="{{env('CDN_URL').$order->user->work_image_url}}">
                            <br />
                            <span style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;">
                                {{$order->user->name}}
                            </span>
                            <br />
                            <br />
                            <br />
                            <img data-magnify="gallery" data-src="{{env('CDN_URL').$order['salary_image']}}" data-caption="{{\App\Consts\Level::toString($order->user->income_level)}} " style="width: 230px;height: 200px;" src="{{env('CDN_URL').$order['salary_image']}}">
                            <br />
                            <span style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;">
                               @lang('final.image.salary')
                            </span>

                        </td>
                    </tr>
                    <span id="orderId" data="{{$id}}"></span>

                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            @lang('message.phone.company.name')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;"  >
                            @lang('order.source'): {{$order->user->company_name}}
                            @if (!empty($order->npwp['profile']['job']))
                                <br />@lang('order.sourceNpwp'): {{$order->npwp['profile']['job']}}
                            @endif
                            @if (!empty($order->bpjs['jht_balances'][0]['member_info']['employment_unit']))
                                <br />@lang('order.sourceBpjs'): {{$order->bpjs['jht_balances'][0]['member_info']['employment_unit']}}
                            @endif
                        </td>
                        <td style="width:40px;vertical-align:middle;" colspan="2">
                            <button class="btn btn-lg
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['company_name']))
                                    @if(json_decode($workTrialDetail->work, true)['company_name'] == 1)
                                        btn-success
                                    @endif
                                @endif
                            @endif
                             work-name btn-one" data-name="1" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;vertical-align:middle;" colspan="2">
                            <button class="btn btn-lg
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['company_name']))
                                    @if(json_decode($workTrialDetail->work, true)['company_name'] == 2)
                                        btn-danger
                                    @endif
                                @endif
                            @endif
                             work-name btn-one" id="workCheckInfo" data-name="2" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                        </td>
                    </tr>


                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            @lang('message.phone.company.career')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:nowrap"  >
                            {{\App\Consts\Career::toString($order->user->career)}}
                        </td>
                        <td style="width:40px;vertical-align:middle;" colspan="2">
                            <button class="btn btn-lg
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['career']))
                                    @if(json_decode($workTrialDetail->work, true)['career'] == 1)
                                        btn-success
                                    @endif
                                @endif
                            @endif
                            work-career btn-one" data-career="1" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;vertical-align:middle;" colspan="2">
                            <button class="btn btn-lg
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['career']))
                                    @if(json_decode($workTrialDetail->work, true)['career'] == 2)
                                        btn-danger
                                    @endif
                                @endif
                            @endif
                            work-career btn-one" data-career="2" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            @lang('message.phone.company.income')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:nowrap"  >
                            @lang('order.source'): {{\App\Consts\Level::toString($order->user->income_level)}}
                            @if (!empty($order->npwp['induk_info']['total_earning_net']))
                                <br />@lang('order.sourceNpwp'): {{number_format(ceil($order->npwp['induk_info']['total_earning_net']/12, 0, '.', '.'))}}
                            @endif
                            @if (!empty($order->bpjs['jht_balances'][0]['member_info']['current_salary']))
                                <br />@lang('order.sourceBpjs'): {{number_format($order->bpjs['jht_balances'][0]['member_info']['current_salary'], 0, '.', '.')}}
                            @endif
                        </td>
                        <td style="width:40px;vertical-align:middle;" colspan="1">
                            <button class="btn btn-lg
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['income_level']))
                                     @if(json_decode($workTrialDetail->work, true)['income_level'] == 1)
                                        btn-success
                                    @endif
                                @endif
                            @endif
                            work-income btn-one" data-level="1" style="width:120px;white-space:normal;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.new.same')
                            </button>
                        </td>
                        <td style="width:200px;vertical-align:middle;" colspan="3">
                            <button class="btn btn-lg
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['income_level']))
                                    @if(json_decode($workTrialDetail->work, true)['income_level'] == 2)
                                        btn-danger
                                    @endif
                                @endif
                            @endif
                             work-income btn-one" data-level="2" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.new.difference')
                            </button>
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['income_level']))
                                    @if(json_decode($workTrialDetail->work, true)['income_level'] == 3)
                                        btn-warning
                                    @endif
                                @endif
                            @endif
                            btn-lg work-income btn-one" data-level="3" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.secret')
                            </button>
                            <br />
                            <select name="income-real-leaval" id="income-real-leaval" style="width:220px;margin-left:134px;margin-top:10px;" class="form-control">
                                <option value="">@lang('message.choose')</option>
                                <option value="real{{\App\Consts\Level::LEVER1}}">{{\App\Consts\Level::toString(1)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER5}}">{{\App\Consts\Level::toString(5)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER6}}">{{\App\Consts\Level::toString(6)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER7}}">{{\App\Consts\Level::toString(7)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER8}}">{{\App\Consts\Level::toString(8)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER9}}">{{\App\Consts\Level::toString(9)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER10}}">{{\App\Consts\Level::toString(10)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER11}}">{{\App\Consts\Level::toString(11)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER12}}">{{\App\Consts\Level::toString(12)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER13}}">{{\App\Consts\Level::toString(13)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER14}}">{{\App\Consts\Level::toString(14)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER15}}">{{\App\Consts\Level::toString(15)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER16}}">{{\App\Consts\Level::toString(16)}}</option>
                                <option value="real{{\App\Consts\Level::LEVER4}}">{{\App\Consts\Level::toString(4)}}</option>
                            </select>
                        </td>
                    </tr>


                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            @lang('message.phone.company.period')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:nowrap"  >
                            {{\App\Consts\WorkPeriod::toString($order->user->work_period)}}
                        </td>
                        <td style="width:40px;vertical-align:middle;" colspan="2">
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_period']))
                                    @if(json_decode($workTrialDetail->work, true)['work_period'] == 1)
                                        btn-success
                                    @endif
                                @endif
                            @endif
                            btn-lg btn-one btn-period" data-period="1" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;vertical-align:middle;" colspan="2">
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_period']))
                                    @if(json_decode($workTrialDetail->work, true)['work_period'] == 2)
                                        btn-danger
                                    @endif
                                @endif
                            @endif
                             btn-lg btn-one btn-period" data-period="2" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>

                            <input class="form-control" style="width:276px;height:40px;margin-left:50px;margin-top:10px;" id="income-work-period" name="confirm_password" type="text" />
                        </td>
                    </tr>

                    <span id="work-status" value="{{$info}}"></span>
                    <tr>
                        <td style="width: 143px;vertical-align:middle;font-size: 20px;">
                            @lang('message.phone.company.work.address')
                        </td>
                        <td colspan="3" style="width:240px;vertical-align:middle;font-size: 20px;font-weight: bolder;white-space:normal;"  >
                            {{$order->user->company_address }}
                        </td>
                        <td style="width:40px;vertical-align:middle;" colspan="2">
                            <button class="btn
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['company_address']))
                                    @if(json_decode($workTrialDetail->work, true)['company_address'] == 1)
                                        btn-success
                                    @endif
                                @endif
                            @endif
                            btn-lg btn-one btn-address" data-address="1" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.same')
                            </button>
                        </td>
                        <td style="width:20px;vertical-align:middle;" colspan="2">
                            <button class="btn btn-lg
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['company_address']))
                                    @if(json_decode($workTrialDetail->work, true)['company_address'] == 2)
                                        btn-danger
                                    @endif
                                @endif
                            @endif
                            btn-one btn-address"  data-address="2" style="width:210px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.diffence')
                            </button>
                            <input class="form-control" type="text" style="width:276px ;height:40px;margin-left:50px;margin-top:10px;" maxlength="50" id="work-address-notice">

                        </td>
                    </tr>


                    <tr>
                        <td style="width: 143px;vertical-align:bottom;font-size: 20px;">
                            @lang('message.phone.company.work.pay.period')
                        </td>
                        <td colspan="3" style="width:290px;vertical-align:bottom;font-size: 20px;font-weight: bolder;white-space:nowrap"  >
                            {{$order->user->paid_period}}
                        </td>
                        <td style="width:40px;" colspan="1">
                            <button class="btn
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['paid_period']))
                                    @if(json_decode($workTrialDetail->work, true)['paid_period'] == 1)
                                        btn-success
                                    @endif
                                 @endif
                             @endif
                             btn-lg btn-one btn-pay" data-pay="1" style="width:120px;white-space:normal;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.new.same')
                            </button>
                        </td>
                        <td style="width:200px;" colspan="3">
                            <button class="btn btn-lg
                             @if($workTrialDetail)
                                 @if(isset(json_decode($workTrialDetail->work, true)['paid_period']))
                                     @if(json_decode($workTrialDetail->work, true)['paid_period'] == 2)
                                        btn-danger
                                    @endif
                                 @endif
                             @endif
                             btn-one btn-pay" data-pay="2" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.new.difference')
                            </button>
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['paid_period']))
                                    @if(json_decode($workTrialDetail->work, true)['paid_period'] == 3)
                                        btn-warning
                                    @endif
                                @endif
                            @endif
                            btn-lg btn-one btn-pay" data-pay="3" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 40px;" type="button">
                                @lang('message.phone.company.ask.secret')
                            </button>
                            <input class="form-control"style="width:276px ;height:40px;margin-left:50px;margin-top:10px;" id="income-pay-period" name="confirm_password" type="text" />
                        </td>
                    </tr>


                    <tr>
                        <td colspan="4" style="white-space:nowrap"  >
                            <div style="font-size: 30px;font-weight: bolder;color: #333333;white-space:normal;">@lang('message.phone.problem')</div><br />
                            <div style="margin-left:20px;font-size: 20px;color: #333333;white-space:normal;">@lang('message.phone.more')</div>
                        </td>
                        <td style="width:200px;" colspan="6">
                            <button class="btn btn-during
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['env'])
                                            btn-danger
                                    @endif
                                @endif
                            @endif
                            btn-env"  data-env="1" style="width:150px;white-space:normal;margin-left: 30px;font-size: 14px;padding: initial" type="button">
                                @lang('message.phone.ask.environment.noise')
                            </button>
                            <button class="btn btn-during
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['ear'])
                                            btn-danger
                                    @endif
                                @endif
                            @endif
                            btn-ear" data-ear="1" style="white-space:normal;width:150px;margin-left: 30px;font-size: 14px;padding: initial" type="button">
                                @lang('message.phone.ask.header.ear')
                            </button>
                            <button class="btn btn-during
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['zzz'])
                                            btn-danger
                                    @endif
                                @endif
                            @endif
                            btn-zzz" data-zzz="1" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;padding: initial" type="button">
                                @lang('message.phone.ask.take.phone.zzz')
                            </button>
                            <button class="btn btn-during
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['other'])
                                                btn-danger
                                    @endif
                                @endif
                            @endif
                            btn-other" data-other="1" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;padding: initial" type="button">
                                @lang('message.phone.ask.personal.self')
                            </button>
                            <br /><br />
                            <button class="btn btn-during
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['second'])
                                            btn-success
                                    @endif
                                @endif
                            @endif
                            btn-second" data-second="1" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;padding: initial"type="button">
                                @lang('message.phone.ask.call.second')
                            </button>
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['one'])
                                            btn-success
                                    @endif
                                @endif
                            @endif
                            btn-during btn-first" data-first="1" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;" type="button">
                                @lang('message.phone.ask.call.one')
                            </button>
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['now'])
                                            btn-success
                                    @endif
                                @endif
                            @endif
                            btn-during btn-now" data-now="1" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;" type="button">
                                @lang('message.phone.ask.take.right.now')
                            </button>
                            <button class="btn
                            @if($workTrialDetail)
                                @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                                    @if(json_decode($workTrialDetail->work, true)['work_remark']['slow'])
                                            btn-success
                                    @endif
                                @endif
                            @endif
                            btn-during btn-slow" data-slow="1" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;" type="button">
                                @lang('message.phone.ask.take.slowly')
                            </button>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" style="white-space:nowrap"  >
                            <div style="font-size: 30px;font-weight: bolder;color: #333333;white-space:normal;">@lang('message.take.phone')</div><br />
                        </td>
                        <td style="width:200px;" colspan="6">
                            <button class="btn btn-taker
                            @if($workTrialDetail)
                            @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                            @if(json_decode($workTrialDetail->work, true)['work_remark']['env'])
                                    btn-success
@endif
                            @endif
                            @endif
                                    btn-env"  data-taker="10" style="width:150px;white-space:normal;margin-left: 30px;font-size: 14px;padding: initial;height: 45px;" type="button">
                                @lang('message.hr')
                            </button>
                            <button class="btn btn-taker
                            @if($workTrialDetail)
                            @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                            @if(json_decode($workTrialDetail->work, true)['work_remark']['ear'])
                                    btn-success
@endif
                            @endif
                            @endif
                                    btn-ear" data-taker="11" style="white-space:normal;width:150px;margin-left: 30px;font-size: 14px;padding: initial;height: 45px;" type="button">
                                @lang('message.finance')
                            </button>
                            <button class="btn btn-taker
                            @if($workTrialDetail)
                            @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                            @if(json_decode($workTrialDetail->work, true)['work_remark']['zzz'])
                                    btn-success
@endif
                            @endif
                            @endif
                                    btn-zzz" data-taker="12" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;padding: initial" type="button">
                                @lang('message.companyee')
                            </button>
                            <button class="btn btn-taker
                            @if($workTrialDetail)
                            @if(isset(json_decode($workTrialDetail->work, true)['work_remark']))
                            @if(json_decode($workTrialDetail->work, true)['work_remark']['other'])
                                    btn-success
@endif
                            @endif
                            @endif
                                    btn-other" data-taker="13" style="width:120px;white-space:normal;margin-left: 30px;font-size: 14px;height: 45px;padding: initial" type="button">
                                @lang('message.other')
                            </button>
                        </td>
                    </tr>

                    @if($info == 0)
                    <tr>
                        <td colspan="10" style="vertical-align: middle;text-align: center;">
                            <button class="btn btn-lg btn-finish" style="width:460px;height: 50px;vertical-align: middle;text-align: center;">
                                @lang('message.phone.ask.finish.next')
                            </button>
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
            @endif
        </section>
    </div>
</section>
<script>
    {{--单位名称不一致--}}
     $("#workCheckInfo").on('click', function(){

        if(!$('.choose-valid').hasClass('btn-success'))
            return false;
        if($('#work-status').attr('value') == 1)
            return false;
        $('.btn-finish').removeClass('btn-info');
        var thisClass = $(this);
        var orderId = $("#orderId").attr('data');
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.conpany.diffrence')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"post",
                        data:{
                            company_phone: 1,
                            company_name: 2,
                            career: 0,
                            income_level: 0,
                            work_period: 0,
                            company_address: 0,
                            company_address_notice: 0,
                            paid_period: 0,
                            levelRemark:0,
                            periodRemark:0,
                            payRemark:0,
                            work_remark: {
                                env:0,
                                ear:0,
                                zzz:0,
                                other:0,
                                second:0,
                                one:0,
                                now:0,
                                slow:0,
                            },
                            status:22,
                            order_id:orderId,
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2 || data.code == 3){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    thisClass.removeClass('btn-danger');
                    $('.btn-finish').removeClass('btn-info');
                }
            }
        });

    })
    {{--有效电话--}}

{{--单选按钮--}}
    $(document).on('click','.btn-one', function () {
        if(!$('.choose-valid').hasClass('btn-success'))
            return false;

        if($('#work-status').attr('value') == 1)
            return false;

        $(this).parent().parent().find('button').removeClass('btn-danger').removeClass('btn-success').removeClass('btn-warning');

        if($(this).attr('data-name') == 1){
            $(this).addClass('btn-success')
        } else if($(this).attr('data-name') == 2){
            $(this).addClass('btn-danger');
        }

        if($(this).attr('data-career') == 1){
            $(this).addClass('btn-success')
        } else if($(this).attr('data-career') == 2){
            $(this).addClass('btn-danger');
        }

        if($(this).attr('data-level') == 1){
            $(this).addClass('btn-success')
        } else if($(this).attr('data-level') == 2){
            $(this).addClass('btn-danger');
        } else if($(this).attr('data-level') == 3){
            $(this).addClass('btn-warning');
        }

        if($(this).attr('data-period') == 1){
            $(this).addClass('btn-success')
        } else if($(this).attr('data-period') == 2){
            $(this).addClass('btn-danger');
        }


        if($(this).attr('data-address') == 1){
            $(this).addClass('btn-success')
        } else if($(this).attr('data-address') == 2){
            $(this).addClass('btn-danger');
        }

        if($(this).attr('data-pay') == 1){
            $(this).addClass('btn-success')
        } else if($(this).attr('data-pay') == 2){
            $(this).addClass('btn-danger');
        } else if($(this).attr('data-pay') == 3){
            $(this).addClass('btn-warning');
        }

//        var success = $(this).parent().children('button').hasClass('btn-success').length;
        var same = $('.btn-one.btn-success').size();
        var difference = $('.btn-one.btn-danger').size();
        var secret = $('.btn-one.btn-warning').size();
        var num = same + difference + secret - 1;
//        console.log(num)
        if(num == 5){
            $('.btn-finish').addClass('btn-info')
        }

     });

//    接电话的人
    $(document).on('click','.btn-taker', function () {


        if(!$('.choose-valid').hasClass('btn-success'))
            return false;
        if($('#work-status').attr('value') == 1)
            return false;

        $(this).parent().parent().find('button').removeClass('btn-info');
        $(this).addClass('btn-info');
    });

//    多选按钮
    $(document).on('click', '.btn-during', function(){
        if(!$('.choose-valid').hasClass('btn-success'))
            return false;

        if($('#work-status').attr('value') == 1)
            return false;

        if($(this).attr('data-env') == 1 || $(this).attr('data-ear') == 1 || $(this).attr('data-zzz') == 1 || $(this).attr('data-other') == 1){
            $(this).toggleClass('btn-success');
        } else if($(this).attr('data-second') == 1 || $(this).attr('data-first') == 1 || $(this).attr('data-now') == 1 || $(this).attr('data-slow') == 1){
            $(this).toggleClass('btn-danger');
        }

    });
//    有效号码
    $(document).on('click','.choose-valid', function () {
        if($('#work-status').attr('value') == 1)
            return false;

        $(this).parent().parent().find('button').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $(this).addClass('btn-success');

    });
//    无效号码
    $(document).on('click','.no-valid', function () {

        if($('#work-status').attr('value') == 1)
            return false;

        $(this).parent().parent().find('button').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $('.btn-one').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $('.btn-finish').removeClass('btn-info')
        $('.btn-during').removeClass('btn-success').removeClass('btn-danger')
        $(this).addClass('btn-danger');

        var company_phone = $(this).attr('data-phone');

        var orderId = $("#orderId").attr('data');
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.phone.number.invalid')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"post",
                        data:{
                            order_id:orderId,
                            company_phone: company_phone,
                            company_name: 0,
                            career: 0,
                            income_level: 0,
                            work_period: 0,
                            company_address: 0,
                            company_address_notice: 0,
                            paid_period: 0,
                            levelRemark:0,
                            periodRemark:0,
                            payRemark:0,
                            status:22,
                            work_remark: {
                                env:0,
                                ear:0,
                                zzz:0,
                                other:0,
                                second:0,
                                one:0,
                                now:0,
                                slow:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2 || data.code == 3){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            } else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });



    });
//    无人接听加入队列
    $(document).on('click','.add-list', function () {

        if($('#work-status').attr('value') == 1)
            return false;

        $('.btn-finish').removeClass('btn-info')
        $(this).parent().parent().find('button').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $('.btn-one').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $('.btn-during').removeClass('btn-success').removeClass('btn-danger')
        $(this).addClass('btn-warning');

        var company_phone = $(this).attr('data-phone');

        var orderId = $("#orderId").attr('data');
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.add.no.answer')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/addHolidayList",
                        type:"post",
                        data:{
                            order_id:orderId,
                            company_phone: company_phone,
                            company_name: 0,
                            career: 0,
                            income_level: 0,
                            work_period: 0,
                            company_address: 0,
                            company_address_notice:0,
                            paid_period: 0,
                            levelRemark:0,
                            periodRemark:0,
                            payRemark:0,
                            work_remark: {
                                env:0,
                                ear:0,
                                zzz:0,
                                other:0,
                                second:0,
                                one:0,
                                now:0,
                                slow:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            if(data.code == 1){
                                var orderId = data.orderId;
                                window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                            } else if(data.code ==2){
                                alert(data.info);
                                window.location.replace('/phone/workDetail');
                            } else if(data.code == 3){
                                alert(data.info);
                                window.location.replace('/phone/phoneTrial');
                            }else {
                                alert(data.info);
                            }
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });


    });

//    节假日属性
    $(document).on('click','.holiday-list', function () {

        if($('#work-status').attr('value') == 1)
            return false;

//        删除同级的属性
        $(this).parent().parent().find('button').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
//        删除单选中的属性
        $('.btn-one').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $('.btn-finish').removeClass('btn-info')
        $('.btn-during').removeClass('btn-success').removeClass('btn-danger')
        $(this).addClass('btn-info');

        var company_phone = $(this).attr('data-phone');

        var orderId = $("#orderId").attr('data');
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.add.holiday')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/phoneTrialDetail",
                        type:"post",
                        data:{
                            order_id:orderId,
                            company_phone: company_phone,
                            company_name: 0,
                            career: 0,
                            income_level: 0,
                            work_period: 0,
                            company_address: 0,
                            company_address_notice: 0,
                            paid_period: 0,
                            levelRemark:0,
                            periodRemark:0,
                            payRemark:0,
                            work_remark: {
                                env:0,
                                ear:0,
                                zzz:0,
                                other:0,
                                second:0,
                                one:0,
                                now:0,
                                slow:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });


    });

//    工作证合格通过
    $(document).on('click','.work-permit', function () {

        if($('#work-status').attr('value') == 1)
            return false;

//        删除同级的属性
        $(this).parent().parent().find('button').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
//        删除单选中的属性
        $('.btn-one').removeClass('btn-info').removeClass('btn-success').removeClass('btn-danger').removeClass('btn-warning')
        $('.btn-finish').removeClass('btn-info')
        $('.btn-during').removeClass('btn-success').removeClass('btn-danger')
        $(this).addClass('btn-info');

        var company_phone = $(this).attr('data-phone');

        var orderId = $("#orderId").attr('data');
        $.confirm({
            title: '@lang("message.make.sure.title")',
            content: "@lang('message.make.sure.add.permit')",
            buttons: {
                "@lang('message.yes')": function () {
                    $.ajax({
                        url:"/phone/phoneTrialDetail",
                        type:"post",
                        data:{
                            order_id:orderId,
                            company_phone: company_phone,
                            company_name: 0,
                            career: 0,
                            income_level: 0,
                            work_period: 0,
                            company_address: 0,
                            company_address_notice: 0,
                            paid_period: 0,
                            levelRemark:0,
                            periodRemark:0,
                            payRemark:0,
                            work_remark: {
                                env:0,
                                ear:0,
                                zzz:0,
                                other:0,
                                second:0,
                                one:0,
                                now:0,
                                slow:0,
                            },
                            _token:"{{csrf_token()}}"
                        },
                        dataType:"json",
                        success:function(data){
                            window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                        }
                    });
                },
                "@lang('message.cancel')": function () {
                    btnClass: 'btn-info'
                }
            }
        });


    });


//    提交的属性
    $(document).on('click', '.btn-finish', function(){

        if(!$('.btn-finish').hasClass('btn-info'))
            return false;

        if($('#work-status').attr('value') == 1)
            return false;

        var orderId = $("#orderId").attr('data');
        var company_phone = 1;
        var company_name = $('.work-name.btn-danger').attr('data-name') == undefined ? $('.work-name.btn-success').attr('data-name') : $('.work-name.btn-danger').attr('data-name');
        var career = $('.work-career.btn-danger').attr('data-career') == undefined ? $('.work-career.btn-success').attr('data-career') : $('.work-career.btn-danger').attr('data-career');
        var level = $('.work-income.btn-danger').attr('data-level') == undefined ? ( $('.work-income.btn-warning').attr('data-level') == undefined ? $('.work-income.btn-success').attr('data-level') : $('.work-income.btn-warning').attr('data-level')) : $('.work-income.btn-danger').attr('data-level');
        var work_period = $('.btn-period.btn-danger').attr('data-period') == undefined ? $('.btn-period.btn-success').attr('data-period') : $('.btn-period.btn-danger').attr('data-period');
        var company_address = $('.btn-address.btn-danger').attr('data-address') == undefined ? $('.btn-address.btn-success').attr('data-address') : $('.btn-address.btn-danger').attr('data-address');
        var paid_period = $('.btn-pay.btn-danger').attr('data-pay') == undefined ? ( $('.btn-pay.btn-warning').attr('data-pay') == undefined ? $('.btn-pay.btn-success').attr('data-pay') : $('.btn-pay.btn-warning').attr('data-pay')) : $('.btn-pay.btn-danger').attr('data-pay');

//        工作备注
        var levelRemark = $('#income-real-leaval').val() ? $('#income-real-leaval').val() : "";

//        工作年限
        var periodRemark = $('#income-work-period').val() ? $('#income-work-period').val() : "";
//        发薪日
        var payRemark = $('#income-pay-period').val() ? $('#income-pay-period').val() : "";
        console.log(payRemark);

        //工作地址备注
        var company_address_notice = $('#work-address-notice').val() ? $('#work-address-notice').val() : "";
//        环境嘈杂，
        var env = $('.btn-env.btn-success').attr('data-env') == undefined ? 0 : $('.btn-env.btn-success').attr('data-env');
//        交头接耳
        var ear = $('.btn-ear.btn-success').attr('data-ear') == undefined ? 0 : $('.btn-ear.btn-success').attr('data-ear');
//        接电话支支吾吾
        var zzz = $('.btn-zzz.btn-success').attr('data-zzz') == undefined ? 0 : $('.btn-zzz.btn-success').attr('data-zzz');
//      申请不是本人接的电话
        var other = $('.btn-other.btn-success').attr('data-other') == undefined ? 0 : $('.btn-other.btn-success').attr('data-other');
//        打了两次才打通
        var second = $('.btn-second.btn-danger').attr('data-second') == undefined ? 0 : $('.btn-second.btn-danger').attr('data-second');
//        一次就接通
        var one = $('.btn-first.btn-danger').attr('data-first') == undefined ? 0 : $('.btn-first.btn-danger').attr('data-first');
//        很快就接通
        var now = $('.btn-now.btn-danger').attr('data-now') == undefined ? 0 : $('.btn-now.btn-danger').attr('data-now');
//        很慢才接通
        var slow = $('.btn-slow.btn-danger').attr('data-slow') == undefined ? 0 : $('.btn-slow.btn-danger').attr('data-slow');
//        接电话的人
        var taker = $('.btn-taker.btn-info').attr('data-taker') == undefined ? 0 : $('.btn-taker.btn-info').attr('data-taker');

        $.ajax({
            url:"/phone/addHolidayList",
            type:"POST",
            data:{
                order_id:orderId,
                company_phone: company_phone,
                company_name: company_name,
                career: career,
                income_level: level,
                work_period: work_period,
                company_address: company_address,
                company_address_notice: company_address_notice,
                paid_period: paid_period,
                taker:taker,
                levelRemark:levelRemark,
                periodRemark:periodRemark,
                payRemark:payRemark,
                work_remark: {
                    env:env,
                    ear:ear,
                    zzz:zzz,
                    other:other,
                    second:second,
                    one:one,
                    now:now,
                    slow:slow
                },
                _token:"{{csrf_token()}}"
            },
            dataType:"json",
            success:function(data){
                if(data.code == 1){
                    // var orderId = data.orderId;
                    window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
                } else if(data.code == 2 || data.code == 3){
                    alert(data.info);
                    window.location.replace('/phone/phoneTrial');
                }else {
                    alert(data.info);
                }
                // window.location.replace('/phone/workDetail/id/' + orderId + '/info/0');
            }
        });

    });

    $('.phoneBtn').click(function(){
        $(this).attr('disabled', true);
        var user = $(this).attr('data-user');
        var pass = $(this).attr('data-pass');
        var phone = $(this).attr('data-phone');

        var that = this;
        $.ajax({
            url:"/phone/call",
            type:"GET",
            data:{
                username:user,
                password:pass,
                phone:phone,
            },
            dataType:"json",
            success:function(data){
                if(data.result==1){
                    $(that).removeClass('fa-phone').addClass('fa-phone-square');
                }else{
                    alert(data.msg);
                    $(this).attr('disabled', false);
                }
            },
            error:function(){
                alert(data.msg);
                $(this).attr('disabled', false);
            }
        });
        return false;
    });
</script>