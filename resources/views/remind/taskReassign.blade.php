<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @include('remind/_taskReassign_check')
            @include('remind/_taskReassign_list')
        </div>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
</body>
</html>