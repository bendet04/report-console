<!DOCTYPE html>
<html>
    <head>
        @include('public/title')
    </head>
    <body class="sticky-header">
    @include('public/left')
        <section>
            <div class="main-content">
                @include('public/right_header')

                <div class="wrapper">
                    @if(!empty($data))

                              <div class="panel-body">
                                <section id="flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                        <tr>
                                            <th>@lang('message.agency.role.name')</th>
                                            {{--<th class="numeric">申请类型</th>--}}
                                            <th class="numeric">@lang('basic.date')</th>
                                            <th class="numeric">@lang('basic.trial.order')</th>
                                            <th class="numeric">@lang('basic.order.avg')</th>
                                            <th class="numeric">@lang('basic.pass.order')</th>
                                            <th class="numeric">@lang('basic.order.avg')</th>
                                        </tr>
                                        </thead>

                                            @foreach($data as $k => $v)
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @if($v->type==1)
                                                            @lang('message.role.message.trial')
                                                        @elseif($v->type==2)
                                                            @lang('message.role.phone.trial')

                                                        @elseif($v->type==3)
                                                            @lang('basic.final.tria')
                                                        @endif
                                                    </td>
                                                    <td>{{$v->time}}</td>
                                                    <td>{{$v->sum}}</td>
                                                    <td>{{$v->sumAvg}}</td>
                                                    <td>{{$v->myPass}}</td>
                                                    <td>{{$v->passAvg}}</td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            {{--@endif--}}
                                    </table>
                                    {{--<div class="diy-paginate">--}}
                                        {{--<form action="/channel/StatisticalRrate" method="get">--}}
                                            {{--<input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>--}}
                                        {{--</form>--}}
                                    {{--</div>--}}
                                </section>
                            </div>
                        @endif
                </div>
                    <br/>

                        <div class="wrapper">
                            @if(!empty($phone))
                                <div class="panel-body">
                                    <section id="flip-scroll">
                                        <table class="table table-bordered table-striped table-condensed cf">
                                            <thead class="cf">
                                            <tr>
                                                {{--<th>@lang('message.number')</th>--}}
                                                <th class="numeric">申请类型</th>
                                                <th class="numeric">@lang('basic.date')</th>
                                                <th class="numeric">@lang('basic.trial.order')</th>
                                                <th class="numeric">@lang('basic.order.avg')</th>
                                                <th class="numeric">@lang('basic.pass.order')</th>
                                                <th class="numeric">@lang('basic.order.avg')</th>
                                            </tr>
                                            </thead>

                                            @foreach($phone as $k => $v)
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        @if($v->type==1)
                                                            @lang('message.role.message.trial')
                                                        @elseif($v->type==2)
                                                            @lang('message.role.phone.trial')
                                                        @elseif($v->type==3)
                                                            @lang('basic.final.tria')
                                                        @endif
                                                    </td>
                                                    <td>{{$v->time}}</td>
                                                    <td>{{$v->sum}}</td>
                                                    <td>{{$v->sumAvg}}</td>
                                                    <td>{{$v->myPass}}</td>
                                                    <td>{{$v->passAvg}}</td>
                                                </tr>
                                                @endforeach
                                                </tbody>

                                        </table>
                                    </section>
                                </div>
                            @endif
                        </div>
                <br/>
                <div class="wrapper">
                    @if(!empty($final))
                        {{$phone}}
                        <div class="panel-body">
                            <section id="flip-scroll">
                                <table class="table table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                    <tr>
                                        <th class="numeric">@lang('agency.admin.role')</th>
                                        <th class="numeric">@lang('basic.date')</th>
                                        <th class="numeric">@lang('basic.trial.order')</th>
                                        <th class="numeric">@lang('basic.order.avg')</th>
                                        <th class="numeric">@lang('basic.pass.order')</th>
                                        <th class="numeric">@lang('basic.order.avg')</th>
                                    </tr>
                                    </thead>
                                    @foreach($phone as $k => $v)
                                        <tbody>
                                        <tr>
                                            <td>@if($v->type==1)
                                                    @lang('message.role.message.trial')
                                                @elseif($v->type==2)
                                                    @lang('message.role.phone.trial')

                                                @elseif($v->type==3)
                                                    @lang('basic.final.tria')
                                                @endif




                                            </td>
                                            <td>{{$v->time}}</td>
                                            <td>{{$v->sum}}</td>
                                            <td>{{$v->sumAvg}}</td>
                                            <td>{{$v->myPass}}</td>
                                            <td>{{$v->passAvg}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                </table>
                                <div class="diy-paginate">
                                    <form action="/channel/StatisticalRrate" method="get">
                                        <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                                    </form>
                                </div>
                            </section>
                        </div>
                    @endif
                </div>
                @include('public/bottom')
            </div>
        </section>
    </body>
</html>
@include('public/base_script')
