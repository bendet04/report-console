<form class="form-horizontal" method="post" action="/personal/updatePassword">
    <div class="form-group">
        {{csrf_field()}}
        <label class="control-label col-md-3">@lang('message.personal.modify.old.password')</label>
        <div class="col-md-4">
            <input type="password" class="form-control" name="old" value="" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.personal.modify.new.password')</label>
        <div class="col-md-4">
            <input type="password" class="form-control" name="new" value="" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.personal.modify.ren.password')</label>
        <div class="col-md-4">
            <input type="password" class="form-control" name="rename" value="" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <button class="btn btn-success" type="submit">@lang('message.submit')</button>
            <a class="btn btn-default" href="/personal/modify">@lang('message.reback')</a>
        </div>
    </div>
</form>