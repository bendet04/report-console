<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>

    <div class="main-content">
        @if($errors->any())
            <div class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </div>
        @endif
        @include('public/right_header')
        @include('personal/_password_add')
        <div class="wrapper">
            @include('personal/_password_list')
        </div>
    </div>
</section>

@include('public/base_script')
</body>
</html>
