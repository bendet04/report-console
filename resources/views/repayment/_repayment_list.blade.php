<section class="panel">
    <div class="panel-body">
        {{--<div class="col-md-3 col-sm-6 diy-search-input-box">--}}
            {{--<select class="form-control diy-search-input" id="searchId">--}}
                {{--<option value="">select</option>--}}
                {{--<option value="order_number">贷款编号</option>--}}
                {{--<option value="name">客户名称</option>--}}
                {{--<option value="apply_amount">借款金额</option>--}}
                {{--<option value="repayment_amount">应还金额</option>--}}
                {{--<option value="payment_date">放款日</option>--}}
                {{--<option value="order_status">还款状态</option>--}}
            {{--</select>--}}
        {{--</div>--}}
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf"  id="dynamic-table">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>{{__('final.number')}}</th>
                    <th>@lang('basic.platform')</th>
                    <th class="numeric">{{__('final.clientName')}}</th>
                    <th class="numeric">{{__('final.applyAmount')}}</th>
                    <th class="numeric">@lang('payment.repayment.amount')</th>
                    <th class="numeric">@lang('payment.repayment.date')</th>
                    <th class="numeric">@lang('payment.payment.time')</th>
                    <th class="numeric">@lang('payment.repayment.status')</th>
                    <th class="numeric">@lang('message.operation')</th>

                </tr>
                </thead>
                <tbody>
                @foreach($orders as $k=>$avalue)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td><a data-id="{{ $avalue->id }}" class="repayment-detail" style="cursor: pointer" data-toggle="modal" data-target="#myModal">{{ $avalue->order_number }}</a></td>
                        <td class="numeric">{{ \App\Consts\Platform::alias($avalue->platform) }}</td>
                        <td class="numeric">{{ $avalue->name }}</td>
                        <td class="numeric">{{ $avalue->apply_amount }}</td>
                        <td class="numeric">{{ $avalue->repayment_amount }}</td>
                        <td class="numeric">{{ $avalue->loan_repayment_date }}</td>
                        <td class="numeric">{{ $avalue->payment_date }}</td>
                        <td class="numeric">
                            {{ $avalue->order_status_str }}
                        </td>
                        <td class="numeric">
                            @if (in_array($avalue->order_status, [10, 11, 13, 14, 25]))
                            <a data-id="{{ $avalue->id }}" data-number="{{ $avalue->order_number }}" data-name="{{ $avalue->name }}" data-apply="{{ $avalue->apply_amount }}" data-repayment="{{ $avalue->repayment_amount }}" class="repayment-btn" style="cursor: pointer">@lang('payment.hand.repayment')</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="diy-paginate">
                <form action="/repayment/repaymenthand" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            @if (!empty($orders))
            <div class="diy-page-info">{{ $orders->appends($pageCondition)->render() }}</div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
            @else
                <div style="line-height: 50px; color: red;">@lang('payment.tips')</div>
            @endif
        </section>
    </div>
</section>
