<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <!--   <legend>Personal Information</legend> -->
        @if (!empty($repayment))
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('basic.client.name')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $user->name }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">KTP</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $user->ktp_number }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('message.loan.period')</label>
            <div class="col-md-6 col-sm-6">
                    <input type="text" value="{{$repayment->loan_period_str}}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('basic.apply.amount')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $repayment->apply_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.repayment.amount')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $repayment->repayment_amount }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.deadline.date')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $repayment->payment_date }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.repayment.date')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $repayment->loan_deadline }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        {{--@if (!empty($repayment->loan_suspend_date))--}}
            {{--<div class="form-group">--}}
                {{--<label class="col-md-3 col-sm-2 control-label">缓期日</label>--}}
                {{--<div class="col-md-6 col-sm-6">--}}
                    {{--<input type="text" value="{{ $repayment->loan_suspend_date }}" class="form-control"  disabled="true" style="border:none;">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}

        @if (!empty($repayment->loan_overdue_date))
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.overdue.date')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $repayment->loan_overdue_date }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        @endif
        <div class="form-group">
            <label class="col-md-3 col-sm-2 control-label">@lang('payment.repayment.status')</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" value="{{ $repayment->status }}" class="form-control"  disabled="true" style="border:none;">
            </div>
        </div>
        @endif
        <div class="modal-footer">
            <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
        </div>
    </fieldset>
</form>