<form id="default" class="form-horizontal">
    <fieldset title="Initial Info">
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
            <tr>
                <th>@lang('message.number')</th>
                <th>@lang('payment.payment.flow')</th>
                <th>@lang('payment.repayment')id</th>
                <th>@lang('basic.result')</th>
                <th>@lang('basic.cretate.time')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flows as $key=>$fvalue)
            <tr class="">
                <td>
                    {{ $key + 1 }}
                </td>
                <td>
                    {{ $fvalue->repayment_flow }}
                </td>
                <td>
                    {{ $fvalue->repayment_code }}
                </td>
                <td>
                    {{ $fvalue->status }}
                </td>
                <td>
                    {{ $fvalue->created_at }}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </fieldset>
    <div class="modal-footer">
        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
    </div>
</form>