<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
</style>
<header class="search-header-section">
    <form id="default" class="form-horizontal" action="/repayment/repaymenthand" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        {{--平台名称--}}
        <div class="col-md-4">
            <table>
                <tr>
                    <td class="input-group-addon">@lang('basic.platform')</td>
                    <td>
                        <select class="form-control" name="platform">
                            <option value="">@lang('basic.all.platform')</option>
                            @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        {{--序号--}}
        <div class="col-md-4">
            <table>
                <tr>
                    <td class="input-group-addon">@lang('message.order.number')</td>
                    <td><input type="text" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        {{--姓名--}}
        <div class="col-md-4">
            <table>
                <tr>
                    <td class="input-group-addon" >@lang('basic.client.name')</td>
                    <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                </tr>
            </table>
        </div>
        {{--ktp--}}
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >KTP</td>
                        <td><input type="text" placeholder="Ktp number" name="ktp_number" @if(!empty($pageCondition['ktp_number'])) value="{{$pageCondition['ktp_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
        <div class="col-md-9 col-sm-9 search-btn-section" style="margin-bottom: 10px; margin-left: 140px;">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        {{--<div class="col-md-1 col-sm-1" style="margin-bottom: 15px">--}}
            {{--<button type="submit" class="btn btn-info form-control">@lang('message.select')</button>--}}
        {{--</div>--}}
        {{--//重置选项--}}
        {{--<div class="col-md-1 col-xs-1 " style="tmargin-bottom: 15px">--}}
            {{--<input type="button" class="btn btn-info form-control" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        <div style="clear: both"> </div>
    </form>
    </header>

<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>