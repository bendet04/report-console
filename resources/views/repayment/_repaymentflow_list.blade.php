<section class="panel">
    <div class="panel-body">
        <div class="col-md-3 col-sm-6 diy-search-input-box">
            <select class="form-control diy-search-input" id="searchId">
                <option value="">select</option>
                <option value="order_number">{{__('final.number')}}</option>
                <option value="name">{{__('final.clientName')}}</option>
                <option value="apply_amount">{{__('final.applyAmount')}}</option>
                <option value="repayment_amount">@lang('payment.repayment.amount')</option>
                <option value="payment_date">@lang('payment.payment.time')</option>
                <option value="order_status">@lang('payment.repayment.status')</option>
            </select>
        </div>
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf"  id="dynamic-table">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>{{__('final.number')}}</th>
                    <th>@lang('basic.platform')</th>
                    <th class="numeric">{{__('final.clientName')}}</th>
                    <th class="numeric">{{__('final.applyAmount')}}</th>
                    <th class="numeric">@lang('payment.repayment.amount')</th>
                    <th class="numeric">@lang('payment.repayment.date')</th>
                    <th class="numeric">@lang('payment.payment.time')</th>
                    <th class="numeric">@lang('payment.repayment.status')</th>
                    <th class="numeric">@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $k=>$avalue)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td><a data-id="{{ $avalue->id }}" class="repayment-detail" style="cursor: pointer" data-toggle="modal" data-target="#myModal">{{ $avalue->order_number }}</a></td>
                        <td class="numeric">{{ \App\Consts\Platform::alias($avalue->platform) }}</td>
                        <td class="numeric">{{ $avalue->name }}</td>
                        <td class="numeric">{{ $avalue->apply_amount }}</td>
                        <td class="numeric">{{ $avalue->repayment_amount }}</td>
                        <td class="numeric">{{ $avalue->loan_repayment_date }}</td>
                        <td class="numeric">{{ $avalue->payment_date }}</td>
                        <td class="numeric">
                            {{ $avalue->order_status_str }}
                        </td>
                        <td class="numeric">
                            <a data-id="{{ $avalue->id }}" class="flow-btn" style="cursor: pointer" data-toggle="modal" data-target="#flowModal">@lang('basic.flow.select')</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="diy-paginate">
                <form action="/repayment/repaymenthandflow" method="get">
                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                </form>
            </div>
            <div class="diy-page-info">{{ $orders->appends($pageCondition)->render() }}</div>
            <div class="diy-page-info">Showing 1 to {{$orders->count()}} of {{$orders->total()}} entries</div>
        </section>
    </div>
</section>
