<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('repayment/_repaymentflow_add')
        <div class="wrapper">
            @include('repayment/_repaymentflow_check')
            @include('repayment/_repaymentflow_list')
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 流水的开始 -->
<div aria-hidden="true" aria-labelledby="flowModalLabel" role="dialog" tabindex="-1" id="flowModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width:800px;">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.payment.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 流水的结束 -->

<!-- 详情的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 详情的结束 -->
@include('public/base_script')

<script>
    $(".flow-btn").click(function(){
        $('#flowModal .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/repayment/flow',
            data:{
                order_id: id
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#flowModal .modal-body').html(json);
        }).error(function(){
            alert("@lang('payment.payment.select.fail')");
        })
    });

    $(".repayment-detail").click(function(){
        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/repayment/detail',
            data:{
                order_id: id
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#myModal .modal-body').html(json);
        }).error(function(){
            alert("@lang('payment.detail.select.fail')");
        })
    });
</script>
</body>
</html>