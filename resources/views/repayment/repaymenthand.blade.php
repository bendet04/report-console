<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        @include('repayment/_repayment_add')
        <div class="wrapper">
            @include('repayment/_repayment_check')
            @include('repayment/_repayment_list')
        </div>
        @include('public/bottom')
    </div>
</section>


<!-- 详情的开始 -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">@lang('basic.detail')</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- 详情的结束 -->

@include('public/base_script')

<script>
    $('.repayment-btn').click(function(){
        var that = this;

        var confirmContent = '@if($orders && $orders->count()>1)<div style="color:red;">@lang('payment.more.record')!!!</div><br />@endif';
        confirmContent += "@lang('message.order.number'):"+$(that).attr('data-number')+'<br />';
        confirmContent += "@lang('basic.client.name'):"+$(that).attr('data-name')+'<br />';
        confirmContent += "@lang('basic.apply.amount'):"+$(that).attr('data-apply')+'<br />';
        confirmContent += "@lang('payment.repayment.amount'):"+$(that).attr('data-repayment')+'<br />';

        $(that).hide();
        $.confirm({
            title: "@lang('payment.please.confirm.info'):",
            content: confirmContent,
            buttons: {
                confirm: {
                    text: "@lang('message.yes')",
                    action: function() {
                        var orderId = $(that).attr('data-id');
                        $.ajax({
                            url:"/repayment/closeout",
                            type:"POST",
                            data:{
                                'order_id': orderId,
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            success:function(data){
                                $.alert({
                                    title: "@lang('payment.repayment.result')",
                                    content: data.info,
                                    buttons: {
                                        okay: {
                                            text: 'OK',
                                            btnClass: 'btn-info'
                                        }
                                    }
                                });
                            },
                            error:function(){
                                $.alert({
                                    title: "@lang('payment.repayment.tips')",
                                    content: "@lang('basic.request.fail')",
                                    buttons: {
                                        okay: {
                                            text: 'OK',
                                            btnClass: 'btn-info'
                                        }
                                    }
                                });
                            }
                        });
                    }
                },
                cancel: {
                    text: "@lang('message.cancel')",
                    btnClass: 'btn-info',
                    action: function () {
                        $(that).show();
                    }
                }
            }
        });
    });

    $(".repayment-detail").click(function(){
        $('#myModal .modal-body').html('');
        var id = $(this).attr("data-id");
        $.ajax({
            url:'/repayment/detail',
            data:{
                order_id: id
            },
            type:"get",
            dataType:'html',
        }).done(function(json){
            $('#myModal .modal-body').html(json);
        }).error(function(){
            alert("@lang('basic.request.fail')");
        })
    });
</script>
</body>
</html>