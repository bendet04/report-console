<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:110px;
    }
    .col-md-4 table{
        width:105%;
    }
    .ldx{
        margin-top:-8px
    }
</style>
<header class="search-header-section">

    <form id="default" class="form-horizontal" action="/repayment/repaymenthandflow" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-4">
            <table  class="ldx">
                <tr>
                    <td class="input-group-addon ldx" >@lang('basic.date')</td>
                    <td><input type="text" placeholder="Start-time" autocomplete="off" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                   {{--<td>to</td>--}}
                    <td ><span>⇆</span></td>

                    <td><input type="text" placeholder="End-time" autocomplete="off" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                </tr>
            </table>

            {{--</div>--}}
        </div>
              {{--平台名称--}}
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td class="input-group-addon ldx">@lang('basic.platform')</td>
                            <td>
                                <select class="form-control" name="platform">
                                    <option value="">@lang('basic.all.platform')</option>
                                    @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                        <option value="{{$v}}" @if(!empty($pageCondition['platform'])&&$pageCondition['platform']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.order.number')</td>
                        <td><input type="text" name="order_number" @if(!empty($pageCondition['order_number'])) value="{{$pageCondition['order_number']}}" @endif class="form-control"></td>
                    </tr>
                </table>
             </div>
            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('basic.client.name')</td>
                        <td><input type="text" placeholder="Full Name" name="name" @if(!empty($pageCondition['name'])) value="{{$pageCondition['name']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>


            <div class="col-md-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.status')</td>
                        <td>
                            <select class="form-control" name="order_status">
                            <option></option>
                            <option value="14" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='14') selected="selected" @endif>@lang('status.status14')</option>
                            <option value="25" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='25') selected="selected" @endif>@lang('payment.repayment.deadline')</option>
                            <option value="13" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='13') selected="selected" @endif>@lang('payment.repayment.no.finish')</option>
                            <option value="11" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='11') selected="selected" @endif>@lang('payment.repayment.overdue')</option>
                            <option value="12" @if(!empty($pageCondition['order_status'])&&$pageCondition['order_status']=='12') selected="selected" @endif>@lang('payment.repayment.finish')</option>
                            </select>
                        </td>
                    </tr>
                </table>
                {{--<label class="col-md-3 col-sm-2 control-label">@lang('message.status')</label>--}}
                {{--<div class="col-md-7 col-sm-6">--}}
                    {{--<select class="form-control m-bot15" name="order_status">--}}
                        {{--<option></option>--}}
                        {{--<option value="14">@lang('status.status14')</option>--}}
                        {{--<option value="25">@lang('payment.repayment.deadline')</option>--}}
                        {{--<option value="13">@lang('payment.repayment.no.finish')</option>--}}
                        {{--<option value="11">@lang('payment.repayment.overdue')</option>--}}
                        {{--<option value="12">@lang('payment.repayment.finish')</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            </div>
        <br/>
        {{--</div>--}}
        {{--</fieldset>--}}
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 110px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
         {{--查询按钮--}}
        {{--<div class="col-md-1 col-sm-1 " style="tmargin-bottom: 15px">--}}
            {{--<button type="submit" class="btn btn-info form-control">@lang('message.select')</button>--}}
        {{--</div>--}}
        {{--//重置选项--}}
        {{--<div class="col-md-1 col-xs-1 " style="tmargin-bottom: 15px">--}}
            {{--<input type="button" class="btn btn-info form-control" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        <div style="clear: both"> </div>
    </form>
{{--</div>--}}
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>