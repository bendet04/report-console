<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @if($errors->any())
                <div class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            @include('efficency/_trackAmount_check')

        </div>
        @include('public/bottom')
    </div>
</section>

@include('public/base_script')
</body>
</html>

<script>
    {{--$(document).on('click', '#daySubmit', function() {--}}
{{--//    $("#daySubmit").on('click', function () {--}}
        {{--var start = $("input[name='start']").val();--}}
        {{--$.ajax({--}}
            {{--url:"/efficency/trackAmount",--}}
            {{--type:"POST",--}}
            {{--data:{--}}
                {{--start:start,--}}
                {{--_token:"{{csrf_token()}}"--}}
            {{--},--}}
            {{--dataType:"html",--}}
            {{--success:function(data){--}}
{{--//                if(data.code){--}}
                {{--$('.message-select-by-day').html(data)--}}

                {{--var checkout = $('.dpd2').datepicker({--}}
                    {{--onRender: function(date) {--}}
                        {{--return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';--}}
                    {{--}--}}
                {{--}).on('changeDate', function(ev) {--}}
                    {{--checkout.hide();--}}
                {{--}).data('datepicker');--}}
{{--//--}}

            {{--}--}}
        {{--});--}}
    {{--});--}}

    $(document).on('click','#monthSubmit', function() {
//    $("#daySubmit").on('click', function () {
        var month = $("input[name='month']").val();

        console.log(month);
        $.ajax({
            url:"/efficency/selectTrackByMonth",
            type:"POST",
            data:{
                month:month,
                _token:"{{csrf_token()}}"
            },
            dataType:"html",
            success:function(data){
//                if(data.code){
                $('.message-select-by-month').html(data)
                $('.dpMonths').datepicker({
                    autoclose: true
                });//

            }
        });
    });

    $(document).on('click', '#trackCount', function() {
//    $("#daySubmit").on('click', function () {
        $.ajax({
            url:"/efficency/handTrackData",
            type:"POST",
            data:{
                _token:"{{csrf_token()}}"
            },
            dataType:"html",
            success:function(data){
//                if(data.code){
                $('.track-admin-count-order').html(data)

            }
        });
    });
</script>


