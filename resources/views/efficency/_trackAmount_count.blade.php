<section class="panel track-admin-count-order">
    <header class="panel-heading" style="height: 50px;">
        <span style="float: left">
           @lang('basic.this.day')：{{$date}}
        </span>
        <span style="float: left;margin-left: 30px;color:red">
           @lang('basic.track.count')
        </span>
        <div class="col-md-2 col-xs-2" style="margin-left: 0px;display: inline;">
            <button class="btn btn-info" id="trackCount" name="day" style="text-align:center;height: 30px;margin-left:1px;margin-left: 100px;" type="submit">
                @lang('message.select')
            </button>
        </div>
    </header>
    <div class="panel-body">
        <section id="flip-scroll" name="">
            <table class="table table-bordered table-striped table-condensed cf">

                <thead class="cf">
                <tr>
                    <th class="numeric">@lang('task.taskInfo.track.person')</th>
                    <th class="numeric">@lang('track.beforeLoanDeadline')</th>
                    <th class="numeric">@lang('track.loanDeadline')</th>
                    <th class="numeric">@lang('track.trackStatus1Three')</th>
                    <th class="numeric">@lang('track.trackStatus1Four')</th>
                    <th class="numeric">@lang('track.trackStatus1Five')</th>
                    <th class="numeric">@lang('track.trackStatus1Six')</th>
                    <th class="numeric">@lang('track.trackStatus1Seven')</th>
                </tr>
                </thead>
                @if($trackAdmin)
                    @foreach($trackAdmin as $v)
                        <tr>
                            <td>
                                {{$v->admin_username}}
                            </td>
                            <td>
                                {{$v['before']}}
                            </td>
                            <td>
                                {{$v['deadline']}}
                            </td>
                            <td>
                                {{$v['one']}}
                            </td>
                            <td>
                                {{$v['two']}}
                            </td>
                            <td>
                                {{$v['seven']}}
                            </td>
                            <td>
                                {{$v['fifth']}}
                            </td>
                            <td>
                                {{$v['all']}}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </section>
    </div>
</section>






