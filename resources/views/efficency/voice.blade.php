<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @if($errors->any())
                <div class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            @include('efficency/_voice_check')
            @include('efficency/_voice_list')

        </div>
        @include('public/bottom')
    </div>
</section>

@include('public/base_script')
</body>
</html>


