
<section class="panel" id="select-final-info-detail">
    {{--<span class="col-md-1 col-xs-1 pull-right" style="margin-top:-50px;margin-left: -300px;">--}}
     {{--<a href="/efficency/downFinalTrialInfo/{{$condition}}">@lang('basic.down.to')</a>--}}
    {{--</span>--}}
    <div class="wrapper" id="is_show">
        <span style="margin-left: 15px;">
            @if($user)
            @lang('basic.loan.user.sum'):<span style="color:red">{{$user['total']}}</span>
            <span style="margin-left: 20px;">@lang('basic.old.user.sum'):<span style="color:red">{{$user['old']}}</span></span>
             <span style="margin-left: 20px;">@lang('basic.rate'):<span style="color:red">{{$user['oldRate']}}</span></span>
            @endif
        </span>
        <section class="panel">
            <div class="panel-body">
                <section id="flip-scroll">
                    <table class="table table-bordered table-striped table-condensed cf">
                        <thead class="cf">
                        <tr>
                            <th>@lang('message.number')</th>
                            <th class="numeric">@lang('basic.date')</th>
                            <th class="numeric">@lang('basic.final.tria')</th>
                            <th class="numeric">@lang('basic.sum.order')</th>
                            <th class="numeric">@lang('basic.pass.order')</th>
                            <th class="numeric">@lang('basic.pass.rate')</th>
                            <th class="numeric">@lang('basic.overdue.rate')</th>
                            <th class="numeric">@lang('basic.bad.rate')</th>
                        </tr>
                        </thead>
                        @if($data)
                            <tbody>
                                @foreach($data as $k => $v)
                                    <tr>
                                        <td>{{$k + 1}}</td>
                                        <td>{{$v['time']}}</td>
                                        <td>{{$v['admin']}}</td>
                                        <td><span class="name" >{{$v['total']}}</span></td>
                                        <td><span class="name" >{{$v['pass']}}</span></td>
                                        <td >{{$v['passRate']}}</td>
                                        <td >{{$v['over']}}</td>
                                        <td >{{$v['bad']}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                         @endif
                    </table>
                </section>
            </div>
        </section>
    </div>
</section>