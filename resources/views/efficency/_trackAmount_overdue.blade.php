<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @if($errors->any())
                <div class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            @include('efficency/_trackAmount_count')
        </div>
        @include('public/bottom')
    </div>
</section>

@include('public/base_script')
</body>
</html>

<script>

    $(document).on('click', '#trackCount', function() {
        $.ajax({
            url:"/efficency/handTrackData",
            type:"POST",
            data:{
                _token:"{{csrf_token()}}"
            },
            dataType:"html",
            success:function(data){
                $('.track-admin-count-order').html(data)
            }
        });
    });
</script>
