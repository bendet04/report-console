<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:100px;
    }
    .col-md-4 table{
        width:100%;
    }
    .ldx{
        margin-top: -8px;
    }
</style>
<header class="" style="height: 130px;line-height: 40px">
<span class="col-md-9 col-xs-9" style="float: left">@lang('basic.today')：{{date('Y-m-d')}}</span>
    <span class="col-md-3 col-xs-3 pull-right" style="text-align: right";>
    <a href="/efficency/downFinalTrialInfo/{{$condition}}">@lang('basic.down.to')</a>
    </span>
<form action="">
    <div class="col-md-4 col-md-offset-4">
        <table class="ldx">
            <tr>
                <td class="input-group-addon" >@lang('basic.date')</td>
                <td><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                <td ><span>⇆</span></td>
                <td><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
            </tr>
        </table>
    </div>
    <div class="row-md-5 col-xs-9 text-center col-md-offset-1" style="margin-bottom: 10px;margin-left: 185px">
        <button class="btn btn-info" id="select-final-info">
        @lang('message.select')
        </button>
        <a id="select-final-local-week"><span class="btn btn-info">@lang('basic.this.week')</span></a>
        <a id="select-final-local-month"><span class="btn btn-info">@lang('basic.this.month')</span></a>
        <input type="button" class="btn btn-info"  onclick="rese()" value="@lang('message.reset')">
    </div>
</form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>
