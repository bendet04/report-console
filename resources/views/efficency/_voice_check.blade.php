<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:100px;
    }
    .col-md-4 table{
        width:98%;
    }
    .ldx{margin-top:-8px}
</style>
{{--<div  style="width:500px;margin:0 auto;">--}}
<header class="" style="height: 80px;line-height: 40px;">

<form id="default" class="form-horizontal" method="post" action="{{url('efficency/getVoice')}}">
        {{ csrf_field() }}
        <fieldset title="Initial Info">
            <div class="col-md-4 col-md-offset-4">
                <table class="ldx">
                    <tr>
                        <td class="input-group-addon" >@lang('basic.date')</td>
                        <td><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="from"></td>
                        <td ><span>⇆</span></td>
                        <td><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="to"></td>
                    </tr>
                </table>
            </div>
            <!--   <legend>Personal Information</legend> -->
            {{--<div class="form-group">--}}
                {{--<label class="control-label col-md-3">Date Range</label>--}}
                {{--<div class="">--}}
                    {{--<div class="input-group input-large custom-date-range " data-date="13/07/2013" data-date-format="mm/dd/yyyy">--}}
                        {{--<input type="text" class="form-control dpd1 col-md-7 col-sm-6" name="from">--}}
                        {{--<span class="input-group-addon">To</span>--}}
                        {{--<input type="text" class="form-control dpd2 col-md-7 col-sm-6" name="to">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </fieldset>
        <div class="col-md-8 col-sm-8" style="text-align: center;margin-bottom: 15px;margin-left: 185px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
        </div>
        <div style="clear: both"> </div>
    </form>
{{--</div>--}}
</header>