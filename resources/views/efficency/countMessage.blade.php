<!DOCTYPE html>
<html>
<head>
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
            @if($errors->any())
            <div class="list-group">
                @foreach($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </div>
            @endif
            @include('efficency/_countMessage_check')
        </div>
        @include('public/bottom')
    </div>
</section>

@include('public/base_script')
</body>
</html>

<script>
    $(document).on('click', '#daySubmit', function() {
//    $("#daySubmit").on('click', function () {
        var date = $("input[name='date']").val();
        $.ajax({
            url:"/efficency/selectPhoneByDay",
            type:"POST",
            data:{
                date:date,
                _token:"{{csrf_token()}}"
            },
            dataType:"html",
            success:function(data){
//                if(data.code){
                $('.message-select-by-day').html(data)

                var checkout = $('.dpd2').datepicker({
                    onRender: function(date) {
                        return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                    checkout.hide();
                }).data('datepicker');
//

            }
        });
    });

    $(document).on('click', '#monthSubmit', function() {
//    $("#daySubmit").on('click', function () {
        var month = $("input[name='month']").val();
        $.ajax({
            url:"/efficency/selectPhoneByMonth",
            type:"POST",
            data:{
                month:month,
                _token:"{{csrf_token()}}"
            },
            dataType:"html",
            success:function(data){
//                if(data.code){
                $('.message-select-by-month').html(data)

                $('.dpMonths').datepicker({
                    autoclose: true
                });

            }
        });
    });
</script>


