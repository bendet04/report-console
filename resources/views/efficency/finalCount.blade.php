<!DOCTYPE html>
<html>
    <head>
        @include('public/title')
    </head>
    <body class="sticky-header">
        @include('public/left')
        <section>
            <div class="main-content">
                @include('public/right_header')
                {{--<header class="panel-heading" style="height: 70px;line-height: 35px;">--}}
                    @include('efficency/_finalCount_head')
                {{--</header>--}}
                    @include('efficency/_finalCount_list')
                    @include('public/bottom')
            </div>
        </section>
         @include('public/base_script')
    </body>
</html>
<script>
    $('#select-final-info').on('click', function () {
        var start = $("input[name='start']").val();
        var end = $("input[name='end']").val();
        $.ajax({
            url:"/efficency/selectFinalByDate",
            type:'get',
            data:{
                start:start,
                end:end
            },
            dateType:'html',
            success:function (data) {
                $('#select-final-info-detail').html(data)

            }
        })
        return false;
    })
    $('#select-final-local-week').on('click', function () {
        $.ajax({
            url:'/efficency/selectFinalLocalWeek',
            type:'get',
            dateType:'html',
            success:function (data) {
                $('#select-final-info-detail').html(data)
            }
        });

    });
    $('#select-final-local-month').on('click', function () {
        $.ajax({
            url:'/efficency/selectFinalLocalMonth',
            type:'get',
            dateType:'html',
            success:function (data) {
                $('#select-final-info-detail').html(data)
            }
        });

    });
</script>

