<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:100px;
    }
    .col-md-4 table{
        width:100%;
    }
    .ldx{
        margin-top: -8px;
    }
</style>
<header class="" style="height: 130px;line-height: 40px">
        <span style="float: left" class="col-md-9 col-xs-9">
           @lang('basic.today')：{{$date}}
        </span>
        <span class="col-md-3 col-xs-3 pull-right" style="text-align: right";>
            <a href="/channel/down/{{$str}}">
                              @lang('basic.down.to')
            </a>
        </span>
        <form action="{{url('efficency/messageEfficency')}}" method="post">
            {{ csrf_field() }}
            <div class="col-md-4 col-md-offset-4">
                <table class="ldx">
                    <tr>
                        <td class="input-group-addon" >@lang('basic.date')</td>
                        <td><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                        <td ><span>⇆</span></td>
                        <td><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                    </tr>

                </table>
                {{--</div>--}}
            </div>
            {{--<div class="row-md-5 col-xs-9 text-center col-md-offset-2" style="width:40%;margin-left: 350px">--}}
            <div class="row-md-5 col-xs-9 text-center col-md-offset-1" style="margin-bottom: 10px;margin-left: 185px">
                <button class="btn btn-info" id="daySubmit" name="day" type="submit">
                    @lang('message.select')
                </button>
                <a href="messageEfficency?week=week"><span class="btn btn-info">@lang('basic.this.week')</span></a>
                <a href="messageEfficency?month=month"><span class="btn btn-info">@lang('basic.this.month')</span></a>
                <input type="button" class="btn btn-info"  onclick="rese()" value="@lang('message.reset')">

            </div>
        </form>
</header>
<section class="panel message-select-by-day">

    <div class="panel-body">
        <section id="flip-scroll" name="">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th>@lang('basic.date')</th>
                        <th class="numeric">@lang('task.taskInfo.trial.person')</th>
                        <th class="numeric">@lang('basic.trial.order')</th>
                        <th class="numeric">@lang('basic.pass.order')</th>
                        <th class="numeric">@lang('basic.pass.rate')</th>
                        <th class="numeric">@lang('basic.overdue.rate')</th>
                        <th class="numeric">@lang('basic.bad.rate')</th>
                    </tr>
                    @if($admin)
                        @foreach($admin as $v)
                            <tr>

                                <td>{{$v->only}}</td>
                                <td>{{$v->admin}}</td>
                                <td>{{$v->totalOrder}}</td>
                                <td>{{$v->num}}</td>
                                <td>{{$v->parssRate}}</td>
                                <td>{{$v->overdue}}</td>
                                <td>{{$v->bad_debt}}</td>
                            </tr>
                        @endforeach
                    @endif
                 </thead>

            </table>
        </section>
    </div>
</section>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>

