<!DOCTYPE html>
<html>
<head>

    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>
    <div class="main-content">
        @include('public/right_header')
        <div class="wrapper">
             <span class="col-md-9 col-xs-9" style="float: left">
            @lang('basic.date')：{{$now}}
        </span>
            <br/>

                <div class="panel-body">
                    <section id="flip-scroll">
                        <table class="table table-bordered table-striped table-condensed cf">
                            <thead class="cf">
                            <tr>
                                <th class="numeric">@lang('basic.date')</th>

                                <th>@lang('task.taskInfo.track.person')</th>
                                <th>@lang('track.trackAll')</th>
                                {{--<th class="numeric">申请类型</th>--}}
                                <th class="numeric">@lang('track.today')</th>
                                <th class="numeric">@lang('track.tomorrow')</th>
                                <th class="numeric">@lang('track.after')</th>
                                <th class="numeric">@lang('track.none')</th>
                            </tr>
                            </thead>
                            @if(!empty($data))
                                @foreach($data as $k => $v)
                                <tbody>
                                <tr>
                                    <td>{{$v['time']}}</td>

                                    <td>{{$v['person']}}</td>
                                    <td>{{$v['all']}}</td>
                                    <td>{{$v['today']}}</td>
                                    <td>{{$v['tomorrow']}}</td>
                                    <td>{{$v['after']}}</td>
                                    <td>{{$v['none']}}</td>
                                </tr>
                                </tbody>
                                @endforeach
                            @endif
                        </table>
                    </section>
                </div>
        </div>
        <br/>
        @include('public/bottom')
    </div>
</section>
</body>
</html>
@include('public/base_script')
