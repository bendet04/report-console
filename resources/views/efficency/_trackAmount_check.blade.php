<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:100px;
    }
    .col-md-4 table{
        width:112%;
    }
    .ldx{
        margin-top: -8px;
    }
</style>
    <header class="" style="height: 130px;line-height: 40px">
        <span class="col-md-9 col-xs-9" style="float: left">
            @lang('basic.date')：{{$date}}
        </span>
        <span class="col-md-3 col-xs-3 pull-right" style="text-align: right";>
            <a href="/efficency/downTrackByDay?{{$flag}}">
                              @lang('basic.down.to')
            </a>
        </span>
        <form action="{{url('/efficency/trackAmount')}}" action="post">
            <div class="col-md-4 col-md-offset-4" style="width:30%;">

                <table class="ldx">
                    <tr>
                        <td class="input-group-addon" >@lang('basic.date')</td>
                        <td><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                        <td ><span>⇆</span></td>
                        <td><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                    </tr>
                </table>
            </div>

            <div class="row-md-5 col-xs-9 text-center col-md-offset-1" style="margin-bottom: 10px;margin-left: 185px">
                <button class="btn btn-info" id="daySubmit" name="day" type="submit">
                    @lang('message.select')
                </button>
                <a href="/efficency/trackAmount?week=week"><span class="btn btn-info">@lang('basic.this.week')</span></a>
                <a href="/efficency/trackAmount?month=month"><span class="btn btn-info">@lang('basic.this.month')</span></a>
                <input type="button" class="btn btn-info"  onclick="rese()" value="@lang('message.reset')">
            </div>
        <div class="col-md-3 col-xs-3" style="margin-left: 100px;display: inline;float:left">

        </div>
        </form>

    </header>
    <section class="panel message-select-by-day">

    <div class="panel-body">
        <section id="flip-scroll" name="">
            <table class="table table-bordered table-striped table-condensed cf">
                <tr>
                    <th colspan="7">
                        <span style="margin-left: 10px;color: red">@lang('basic.track.repayment')：{{$allDaySum->realCount}}</span>
                        <span style="margin-left: 10px;color: red">@lang('basic.track.apply')：{{$allDaySum->applyCount}}</span>
                    </th>
                </tr>
                {{--<thead class="cf">--}}
                <tr>
                    {{--<th>@lang('basic.year')</th>--}}
                    {{--<th>@lang('basic.month')</th>--}}
                    <th>日期</th>
                    <th class="numeric">@lang('task.taskInfo.track.person')</th>
                    <th class="numeric">@lang('basic.track.repayment')</th>
                    <th class="numeric">@lang('basic.track.apply')</th>
                    <th class="numeric">@lang('basic.track.amount')</th>
                </tr>
                {{--</thead>--}}
                @if($admin)
                    @foreach($admin as $v)
                        <tr>
                            <td>{{$v->time}}</td>
                            <td>{{$v->admin}}</td>
                            <td>@if(!empty($v->realCount)){{$v->realCount}}@else 0 @endif</td>
                            <td>@if(!empty($v->applyCount)){{$v->applyCount}}@else 0 @endif</td>
                            <td>{{$v->count}}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </section>
    </div>
</section>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>

