<section class="panel message-select-by-month">
    <header class="panel-heading" style="height: 50px;">
        <span style="float: left">
           本月：{{$month}}
        </span>
        <div class="form-group">
            <div class="col-md-2 col-xs-2" style="margin-left: 170px;display: inline;">
                <div data-date-minviewmode="months" data-date-viewmode="years" data-date-format="mm/yyyy" data-date="102/2018" name="month" class="input-append date dpMonths">
                    <input type="text"   size="16" class="form-control">
                    <span class="input-group-btn add-on">

                    </span>
                </div>
            </div>
            <button class="btn btn-info" id="monthSubmit" name="day" style="text-align:center;height: 30px;margin-left:1px;margin-top: 2px;margin-left: -20px" type="submit">
                @lang('message.select')
            </button>
        </div>
        <span style="margin-left: 770px;float: left;margin-top: -40px">
            <a href="/efficency/downPhoneByMonth">
              Excel@lang('basic.down.to')
            </a>
        </span>
    </header>
    <div class="panel-body">
        <section id="flip-scroll" name="">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('basic.year')</th>
                    <th>@lang('basic.month')</th>
                    <th class="numeric">@lang('task.taskInfo.phone.person')</th>
                    <th class="numeric">@lang('basic.pass.order')</th>
                </tr>
                </thead>
                @if($adminMonth)
                    @foreach($adminMonth as $v)
                        <tr>
                            <td>{{$v->year}}</td>
                            <td>{{$v->month}}</td>
                            <td>{{$v->admin}}</td>
                            <td>{{$v->count}}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </section>
    </div>
</section>

