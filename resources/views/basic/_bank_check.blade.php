<style>
    .input-group-addon{
        border: 0px;
        background-color:#eff0f4;
        width:140px;
    }
    .col-md-4 table{
        width:100%;
    }
</style>
<header class="" style="height: 120px;line-height: 30px;">
    <form id="default" class="form-horizontal" method="post" action="{{url('/basic/bank')}}">
        {{ csrf_field() }}
        <fieldset title="Initial Info">
            <!--   <legend>Personal Information</legend> -->
            {{--银行卡号--}}
            <div class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon" >@lang('message.basic.bank.code')</td>
                        <td><input type="text" placeholder="" name="parameter_code" @if(!empty($pageCondition['parameter_code'])) value="{{$pageCondition['parameter_code']}}" @endif class="form-control"></td>
                    </tr>
                </table>
            </div>
            {{--银行类型--}}
            <div  class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.basic.bank.type')</td>
                        <td>
                            <select class="form-control" name="parameter_code_type">
                                <option>@lang('message.choose')</option>
                                <option value="0" @if(isset($pageCondition['parameter_code_type'])&&$pageCondition['parameter_code_type']!='null'&&$pageCondition['parameter_code_type']=='0') selected="selected" @endif>@lang('message.basic.bank.fund')</option>
                                <option value="1" @if(!empty($pageCondition['parameter_code_type'])&&$pageCondition['parameter_code_type']=='1') selected="selected" @endif>@lang('message.basic.bank.refund')</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            {{--状态--}}
            <div  class="col-md-4 col-xs-4">
                <table>
                    <tr>
                        <td class="input-group-addon">@lang('message.status')</td>
                        <td>
                            <select class="form-control" name="status">
                                <option value="choose">@lang('message.choose')</option>
                                <option value="1" @if(!empty($pageCondition['status'])&&$pageCondition['status']=='1') selected="selected" @endif>@lang('message.freeze')</option>
                                <option value="2" @if(!empty($pageCondition['status'])&&$pageCondition['status']=='2') selected="selected" @endif>@lang('message.normal')</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    <br/>
        <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 140px">
            <button type="submit" class="btn btn-info">@lang('message.select')</button>
            <input type="button" class="btn btn-info" style="margin-left: 25px;" onclick="rese()" value="@lang('message.reset')">
        </div>
        {{--<div class="col-md-9 col-sm-9" >--}}
            {{--<button type="submit" class="btn btn-info ">@lang('message.select')</button>--}}
            {{--<a href="/basic/bankAdd" data-toggle="modal" style="margin-left: 15px;" class="btn btn-info finish">@lang('message.basic.bank.add')</a>--}}
            {{--<input type="button" class="btn btn-info " style="margin-left: 15px;" onclick="rese()" value="@lang('message.reset')">--}}
        {{--</div>--}}
        {{--//重置选项--}}
        <div style="clear: both"> </div>
    </form>
</header>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>

