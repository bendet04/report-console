<section class="panel">
    <header class="panel-heading">
        @lang('message.basic.product.list')
    </header>
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('message.basic.product.type')</th>
                    <th>@lang('message.basic.normal.rate')</th>
                    <th>@lang('message.basic.overdue.rate')</th>
                    <th>@lang('message.basic.charge.rate')</th>
                    <th>@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                @if($productInfo)
                    @foreach($productInfo as $v)
                <tr>
                    <th>{{$v->pid}}</th>
                    <th>
                        @if($v->loan_period == 1)
                            7
                        @else
                            14
                        @endif
                    </th>
                    <th>{{$v->normal_rate}}</th>
                    <th>{{$v->overdue_rate}}</th>
                    <th>{{$v->charge_rate}}</th>
                    <th><a href="/basic/productUpdateDetail/id/{{$v->pid}}">@lang('message.update')</a></th>
                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </section>
    </div>
</section>
