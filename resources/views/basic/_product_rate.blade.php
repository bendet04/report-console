<section class="panel">
    <header class="panel-heading">
        @lang('message.basic.overdue.title')
    </header>
    <div class="panel-body">
        <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>@lang('message.number')</th>
                    <th>@lang('message.basic.product.type')</th>
                    <th>@lang('message.basic.product.rate')</th>
                    <th>@lang('message.operation')</th>
                </tr>
                </thead>
                <tbody>
                @if($rateInfo)
                @foreach($rateInfo as $v)
                <tr>
                    <td>{{$v->id}}</td>
                    <td>
                       {{\App\Consts\LoanRate::toString($v->type)}}
                    </td>
                    <td>{{$v->rate}}</td>
                    <td><a href="/basic/productUpdateRate/id/{{$v->id}}">@lang('message.update')</a></td>
                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </section>
    </div>
</section>
