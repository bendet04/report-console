
<form class="form-horizontal" method="post" action="/basic/updateProduct">
    <div class="form-group">
        {{csrf_field()}}
        <input type="hidden" name="pid" value="{{$product->pid}}">
        <label class="control-label col-md-3">@lang('message.basic.product.type')</label>
        <div class="col-md-4">
            @if($product->pid == 1)
                7天
            @endif
            @if($product->pid == 2)
                14天
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.normal.rate')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="normal_rate" value="{{$product->normal_rate}}" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.overdue.rate')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="overdue_rate" value="{{$product->overdue_rate}}" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.charge.rate')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="charge_rate" value="{{$product->charge_rate}}" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <button class="btn btn-success" type="submit">@lang('message.submit')</button>
            <a class="btn btn-default" href="/basic/product">@lang('message.reback')</a>
        </div>
    </div>
</form>