<form class="form-horizontal" method="post" action="/basic/updateRate">
    <div class="form-group">
        {{csrf_field()}}
        <label class="control-label col-md-3">@lang('message.basic.product.type')</label>
        <div class="col-md-4">
            {{\App\Consts\LoanRate::toString($rateDetail->type)}}
        </div>
    </div>
    <input type="hidden" value="{{$rateDetail->id}}" name="id">
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.product.rate')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="rate" value="{{$rateDetail->rate}}" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <button class="btn btn-success" type="submit">@lang('message.submit')</button>
            <a class="btn btn-default" href="/basic/product">@lang('message.reback')</a>
        </div>
    </div>
</form>