<section class="panel">
    <div class="panel-body">
        <table class="table table-bordered table-striped table-condensed cf">
            <thead class="cf">
            <tr>
                <th>@lang('message.number')</th>
                <th class="numeric">@lang('message.basic.bank.type')</th>
                <th class="numeric">@lang('message.basic.bank.name')</th>
                <th class="numeric">@lang('message.basic.bank.code')</th>
                <th class="numeric">@lang('message.status')</th>
                <th class="numeric">@lang('message.operation')</th>
            </tr>
            </thead>
            <tbody>
            @if($bankInfo)
                @foreach($bankInfo as $K => $v)
                    <tr>
                        <td>{{$K + 1}}</td>
                        <td>
                            @if($v->parameter_code_type == 1)
                                @lang('message.basic.bank.refund')
                            @else
                                @lang('message.basic.bank.fund')
                            @endif
                        </td>
                        <td>
                            {{$v->parameter_code_name}}
                        </td>
                        <td>
                            {{$v->parameter_code}}
                        </td>
                        <td>
                            @if($v->status == 1)
                                @lang('message.freeze')
                            @else
                                @lang('message.normal')
                            @endif
                        </td>
                        <td>
                            <a href="/basic/bankUpdate/id/{{$v->id}}">@lang('message.update')</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$bankInfo->render()}}
        @endif
    </div>
</section>
