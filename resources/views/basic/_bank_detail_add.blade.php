<form class="form-horizontal" method="post" action="/basic/bankAddDetail">
    <div class="form-group">
        {{csrf_field()}}
        <label class="control-label col-md-3">@lang('message.basic.bank.type')</label>
        <div class="col-md-4">
            <select class="form-control" name="parameter_code_type">
                <option value="0">@lang('message.choose')</option>
                <option value="1">
                    @lang('message.basic.bank.refund')
                </option>
                <option value="0">
                    @lang('message.basic.bank.fund')
                </option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.bank.name')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="parameter_code_name" value="" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.bank.name')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="parameter_code" value="" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.status')</label>
        <div class="col-md-4">
            <select class="form-control" name="status">
                <option value="1">@lang('message.choose')</option>
                <option value="1">
                    @lang('message.freeze')
                </option>
                <option value="2">
                    @lang('message.normal')
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <button class="btn btn-success" type="submit">@lang('message.submit')</button>
            <a class="btn btn-default" href="/basic/bank">@lang('message.reback')</a>
        </div>
    </div>
</form>