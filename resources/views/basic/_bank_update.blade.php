<form class="form-horizontal" method="post" action="/basic/updatBankInfo">
    <div class="form-group">
        {{csrf_field()}}
        <label class="control-label col-md-3">@lang('message.basic.bank.type')</label>
        <div class="col-md-4">
            <select class="form-control" name="parameter_code_type">
                <option value="1"
                    @if($bankInfo->parameter_code_type == 1)
                       selected
                    @endif
                >
                    @lang('message.basic.bank.refund')
                </option>
                <option value="0"
                        @if($bankInfo->parameter_code_type == 0)
                        selected
                    @endif
                >
                    @lang('message.basic.bank.fund')
                </option>
            </select>
            {{--<input type="text" class="form-control" name="rate" value="" required>--}}
        </div>
    </div>
    <input type="hidden" value="{{$bankInfo->id}}" name="id">
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.bank.name')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="parameter_code_name" value="{{$bankInfo->parameter_code_name}}" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.basic.bank.name')</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="parameter_code" value="{{$bankInfo->parameter_code}}" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">@lang('message.status')</label>
        <div class="col-md-4">
            <select class="form-control" name="status">
                <option value="1"
                        @if($bankInfo->status == 1)
                        selected
                        @endif
                >
                    @lang('message.freeze')
                </option>
                <option value="2"
                        @if($bankInfo->status == 2)
                        selected
                        @endif
                >
                    @lang('message.normal')
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <button class="btn btn-success" type="submit">@lang('message.submit')</button>
            <a class="btn btn-default" href="/basic/bank">@lang('message.reback')</a>
        </div>
    </div>
</form>