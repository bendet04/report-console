<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')

<body class="sticky-header">
@include('public/left')
<section>
   <div class="main-content">
        @include('public/right_header')
       <style>
           .input-group-addon{
               border: 0px;
               background-color:#eff0f4;
               width:100px;
           }
           .col-md-4 table{
               width:98%;
           }
           .ldx{margin-top:-8px}
       </style>
       <header class="" style="height: 170px;line-height: 50px;">
         <span class="col-md-9 col-xs-9" style="float: left">
           @lang('basic.today')：{{date('Y-m-d')}}
        </span>
           <span class="col-md-3 col-xs-3 pull-right" style="text-align: right";>
                <a href="/channel/down/{{$str}}" style="font-size: 16px">
                  @lang('basic.down.to')
                </a>
            </span>
            <form action="{{url('channel/StatisticalRrate')}}" method="post">
                {{ csrf_field() }}
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td class="input-group-addon">{{__('message.platform')}}</td>
                            <td>
                                <select class="form-control" name="source">
                                    <option value="">@lang('basic.all.platform')</option>
                                    @foreach(\App\Consts\Platform::toString() as $k=>$v)
                                        <option value="{{$v}}" @if(!empty($pageCondition['source'])&&$pageCondition['source']==$v) selected="selected" @endif>{{\App\Consts\Platform::alias($v)}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td class="input-group-addon">@lang('basic.all.channel')</td>
                            <td>
                                <select class="form-control" name="channel">
                                    <option value="1">@lang('basic.all.channel')</option>

                                    @foreach($channel as $k => $v)
                                        <option value="{{$v->channel_name}}" @if(!empty($pageCondition['channel'])&&$pageCondition['channel']==$v->channel_name) selected="selected" @endif>{{$v->chan_name}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table class="ldx">
                        <tr>
                            <td class="input-group-addon" >@lang('basic.date')</td>
                            <td><input type="text" autocomplete="off" placeholder="Start time" class="form-control dpd1 " @if(!empty($pageCondition['start'])) value="{{$pageCondition['start']}}" @endif name="start"></td>
                            <td ><span>⇆</span></td>
                            <td><input type="text" autocomplete="off" placeholder="End time" class="form-control dpd2" @if(!empty($pageCondition['end'])) value="{{$pageCondition['end']}}" @endif name="end"></td>
                        </tr>
                    </table>
                </div>
                {{--本周--}}
                <div class="col-md-9 col-sm-9" style="margin-bottom: 10px;margin-left: 100px">
                    <button type="submit" class="btn btn-info">@lang('message.select')</button>
                    <a href="StatisticalRrate?week=week"><span class="btn btn-info">@lang('basic.this.week')</span></a>
                    <a href="StatisticalRrate?month=month"><span class="btn btn-info">@lang('basic.this.month')</span></a>
                    <input type="button" class="btn btn-info"  onclick="rese()" value="@lang('message.reset')">
                </div>
            </form>
        </header>
        </head>
        <section class="panel">
            <div class="wrapper" id="is_show">

                <section class="panel">
                    <div class="panel-body">
                        <section id="flip-scroll">
                            <table class="table table-bordered table-striped table-condensed cf">
                                <thead class="cf">
                                <tr>
                                    <th>@lang('message.number')</th>
                                    <th class="numeric">@lang('basic.date')</th>
                                    <th class="numeric">@lang('basic.all.name')</th>
                                    <th class="numeric">@lang('basic.channel.name')</th>
                                    <th class="numeric">@lang('basic.register.sum')</th>
                                    <th class="numeric">@lang('basic.submit.sum')</th>
                                    <th class="numeric">@lang('basic.message.rate')</th>
                                    <th class="numeric">@lang('basic.phone.rate')</th>
                                    <th class="numeric">@lang('basic.final.rate')</th>
                                    <th class="numeric">@lang('basic.overdue.rate')</th>
                                    <th class="numeric">@lang('basic.bad.rate')</th>
                                </tr>
                                </thead>
                                @if($data)
                                    @foreach($data as $k => $v)
                                        <tbody>
                                        <tr>
                                            <td>{{$k + 1}}</td>
                                            <td>{{$v->only}}</td>
                                            <td>{{$v->platform}}</td>
                                            <td><span class="name" >{{$v->channel}}</span></td>
                                            <td >{{$v->reg_num}}</td>
                                            <td >{{$v->sub_apply}}</td>
                                            <td >{{$v->info_pass}}</td>
                                            <td >{{$v->phone_pass}}</td>
                                            <td >{{$v->final_pass}}</td>
                                            <td >{{$v->overdue}}</td>
                                            <td >{{$v->bad_debt}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                            </table>
                            <div class="diy-paginate">
                                <form action="/channel/StatisticalRrate" method="get">
                                    <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
                                </form>
                            </div>
                            <div class="diy-page-info">Showing 1 to {{$data->count()}} of {{$data->total()}} entries</div>
                            {{$data->appends($pageCondition)->render()}}
                            @endif
                        </section>
                    </div>
                </section>
            </div>
        </section>
        @include('public/bottom')
    </div>
</section>
@include('public/base_script')
</body>
</html>
<script>
    //重置检索条件
    function rese() {
        $(" input[ type='text' ] ").val('');
        $(" select").find('option').attr("selected", false);

    }
</script>


