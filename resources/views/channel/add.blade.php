<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>


    <div class="main-content">
        @include('public/right_header')
{{--        @include('taskinfo/_infotrial_add')--}}

        <section class="panel">

        <div class="wrapper">
            <button style="margin-right:20px; float:right"><a href="/channel/add">@lang('basic.channel.list')</a></button>
            <br/>
            <section class="panel">
            <div class="panel-body">
            <section id="flip-scroll">


            <div class="diy-paginate">
            <form action="/channel/add" method="get">
                <tr>
                    <td>@lang('basic.channel.name')</td>
                    <td><input type="text" ></td>
                </tr>
            <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
            </form>
            </div>
            <div class="diy-page-info">11</div>
            <div class="diy-page-info">Showing 1 to111 entries</div>


            </section>
            </div>
            </section>
            {{--@include('taskinfo/_infotrial_check')--}}
            {{--@include('taskinfo/_infotrial_list')--}}
        </div>
        </section>
        @include('public/bottom')
    </div>
</section>



@include('public/base_script')
<script>
    //过滤内部机制
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   //修改状态
    function save(status,save_id) {
        var msg=confirm("@lang('basic.make.sure.info')?");
        if(msg==true)
        {
//            alert(status);

        $.get('/channel/change', {status:status,save_id: save_id}, function (data) {
             if (data.code == 200) {
                   if(status==1)
                   {
                    $(".status"+save_id).html("@lang('basic.has.userd')");
                       $(".status_"+save_id).html("@lang('message.close')");

                   }
                   else if(status==2)
                   {
                       $(".status"+save_id).html("@lang('basic.not.userd')");
                       $(".status_"+save_id).html("@lang('basic.open')");
                   }

                }
                else
                {
                    alert(data.info);
                }
            }, 'json')
        }

        else
        {
            return false;
        }
    }
</script>

</body>
</html>

