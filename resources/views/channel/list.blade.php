<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('public/title')
</head>
<body class="sticky-header">
@include('public/left')
<section>


    <div class="main-content">
        @include('public/right_header')


        <section class="panel">
        {{--start--}}
            <div class="modal-body" id="is_two" style="display: none">
                <form id="default" class="form-horizontal" action="{{url('/channel/add')}}" method="post">
                    <fieldset title="Initial Info">
                        {{csrf_field()}}
                        {{--渠道ID--}}
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('basic.channel')ID</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" placeholder="" id="roleName" name="channel_id" class="form-control">
                            </div>

                        </div>
                        {{--渠道ID--}}
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('basic.channel.name')</label>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" placeholder="" id="roleName" name="channel_name" class="form-control">
                            </div>
                            <span id="checkName" style="color:red;"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 control-label">@lang('message.status')</label>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control" name="status">
                                    <option name="null"></option>
                                    <option value="1">@lang('basic.has.userd')</option>
                                    <option value="2">@lang('basic.not.userd')</option>
                                </select>
                            </div>
                            <span id="checkName" style="color:red;"></span>
                        </div>

                    </fieldset>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">@lang('message.submit')</button>
                        <button type="button" class="btn btn-info finish" data-dismiss="modal">@lang('message.close')</button>
                    </div>
                </form>
            </div>
            {{--end--}}

            <div class="wrapper" id="is_show">
            <button class="btn btn-info" style="margin-right:20px; float:right" id="trigg">@lang('basic.add.channel')</button>
            <br/>
            <section class="panel">
            <div class="panel-body">
            <section id="flip-scroll">
            <table class="table table-bordered table-striped table-condensed cf">
            <thead class="cf">
            <tr>
            <th>@lang('message.number')</th>
                <th class="numeric">@lang('basic.channel')ID</th>
            {{--<th>@lang('message.order.number')</th>名称 --}} <th class="numeric">@lang('basic.name')</th>
            <th class="numeric">@lang('message.status')</th>
            <th class="numeric">@lang('basic.cretate.time')</th>

                <th class="numeric">@lang('message.operation')</th>
            </tr>
            </thead>
            @if($data)
            @foreach($data as $k => $v)
            <tbody>
            <tr id="del{{$v->id}}">
            <td>{{$k + 1}}</td>
                {{--渠道ID--}}
             <td><span class="id">{{$v->channel_name}}</span></td>
                {{--渠道名称--}}
             <td><span class="name" onclick="save_name('{{$v->id}}',$(this))" save_id="{{$v->id}}">{{$v->chan_name}}</span></td>
            <td class="status{{$v->id}}">@if($v->status==1)@lang('basic.has.userd')@elseif($v->status==2)@lang('basic.not.userd')@endif</td>
            <td>{{$v->create_at}}</td>

            <td>
               <span> @if($v->status==1)<a  class="status_{{$v->id}}" href="javascript:void(0)" onclick="save(2,'{{$v->id}}')">@lang('message.close')</a>
                @elseif($v->status==2)<a  class="status_{{$v->id}}" href="javascript:void(0)" onclick="save(1,'{{$v->id}}')">@lang('basic.use')</a>
                @endif
                </span>|<span>  <a href="javascript:void(0)" onclick="del({{$v->id}})">@lang('message.delete')</a> </span>
            </td>
            </tr>
            @endforeach
            </tbody>
            </table>
                {{$data->render()}}

                <div class="diy-paginate">

                <form action="/channel/channelList" method="get">
            <input id="page" style="width:30px;" name="page" ><button type="submit" id="jump">@lang('basic.jump')</button>
            </form>
            </div>

                <div class="diy-page-info">Showing 1 to {{$data->count()}} of {{$data->total()}} entries</div>

                @endif

            </section>
            </div>
            </section>

        </div>
        </section>
        @include('public/bottom')
    </div>
</section>



@include('public/base_script')
<script>
    //过滤内部机制
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   //修改状态 启用||不启用
    function save(status,save_id) {
        var msg=confirm("@lang('basic.make.sure.info')?");
        if(msg==true)
        {
            $.post('/channel/change', {status:status,save_id: save_id}, function (data) {

             if (data.code == 200) {
                   if(status==1)
                   {
                    $(".status"+save_id).html("@lang('basic.has.userd')");
                       $(".status_"+save_id).html("@lang('message.close')");

                   }
                   else if(status==2)
                   {
                       $(".status"+save_id).html("@lang('basic.not.userd')");
                       $(".status_"+save_id).html("@lang('basic.use')");
                   }

                }
                else
                {
                    alert(data.info);
                }
            }, 'json')
        }

        else
        {
            return false;
        }
    }
    $("#trigg").click(function () {
        $("#is_show").hide();
        $("#is_two").show();

    })
//    //修改名称
    function save_name(id,obj) {
        old_val = obj.html();
        save_id=id;
        obj.parent().html("<input type=\'text\' value=" + old_val + " class='save"+save_id+"'>");
        $(document).on('blur', '.save'+save_id, function () {
            var obj = $(this);
            var val = $(this).val(); //获取修改后的值
            $.post('/channel/upname', {chan_name: val, id: save_id}, function (data) {

                    if (data == 200) {

                        obj.parent().html("<span class='name' onclick='save_name(save_id,$(this))'>" + val + "</span>")
                    }
                    else {
                        obj.parent().html("<span class='name' onclick='save_name(save_id,$(this))' >" + old_val + "</span>")

                    }
                }
            )
        })
    }

   //删除渠道
    function  del(id) {
        var msg=confirm("@lang('basic.make.sure.info')?");
        if(msg==true)
        {
            $.post('/channel/del', {is_del:1,id:id}, function (data) {
                if(data.code==200)
                {
                    $("#del"+id).hide();
                }
                else
                {
                    alert(data.info);
                }

            }, 'json')
        }

    }
</script>

</body>
</html>

