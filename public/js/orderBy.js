function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}


$(document).ready(function(){
    var currentUrl = location.href;
    var urlArr = location.pathname.split('/');
    var nameArr = urlArr[urlArr.length-1].split('.');
    var name = nameArr[0];
    var orderBy = readCookie(name);
    $('#searchId').val(orderBy);

    //改变排序下拉框
    $('#searchId').change(function() {
        var val = $(this).val();

        if (val) {
            createCookie(name, val, 1);
        }else{
            eraseCookie(name);
        }
        location.href=currentUrl;
    });
});