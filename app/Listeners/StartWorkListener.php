<?php

namespace App\Listeners;

use App\Events\StartWork;
use App\Models\AdminUser;
use App\Models\LoanOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StartWorkListener
{
    //1信审2电审3终审（每个审核人员最多分配订单数）
    public $normal_count = [
        0 => 0,
        1 => 10,
        2 => 10,
        3 => 10,
    ];
    

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * 用户订单智能分配逻辑处理
     *
     * @param  StartWork  $event
     * @return void
     */
    public function handle(StartWork $event)
    {
        $adminId = Auth::id();
        $trialType = $event->trial_type;
        $normalCount = $this->normal_count[$trialType];
        
        //如果用户停止工作后，则不继续分配任务
        $admin = AdminUser::getOne(['id'=>$adminId]);
        if (empty($admin->work_status)) {
            return false;
        }
    
        //判断是否超级管理员，超级管理员不参与分配
        if(LoanOrder::isSuperAdmin() > 0){
            return false;
        }
    
        //检查用户任务条数
        $orderCount = 0;
        if ($trialType==1) {
            $orderCount = LoanOrder::trialinfo()->where('order_status', '17')->count();
        }
        if ($trialType==2) {
            $orderCount = LoanOrder::trialPhoneQueue()->where('order_status', '19')->count();
        }
        if ($trialType==3){
            $orderCount = LoanOrder::trialfinal()->where('order_status', '21')->count();
        }
        if ($orderCount >= $normalCount){
            return false;
        }

        //需要补齐的订单数量
        $remainingCount = $normalCount - $orderCount;
        if ($remainingCount<=0) {
            return false;
        }
        
        $now = date('Y-m-d H:i:s');
        if ($trialType == 1) {
            DB::update('update tb_loan_trial_info set admin_id=?, updated_at=? where admin_id=0 and order_id in(
                        select t.order_id from (
                        select i.order_id from tb_loan_trial_info i
                        left join tb_allocation_rule r
                        on i.order_id = r.order_id
                        where i.admin_id=0
                        order by
                        r.npwp desc,
                        r.bpjs desc,
                        r.telkomsel desc,
                        r.instagram desc,
                        r.facebook desc,
                        r.contacts_num desc,
                        r.income desc,
                        r.sex desc,
                        r.risk_score desc,
                        i.id desc
                        ) as t) limit ?', [$adminId, $now, $remainingCount]);
        }
        if ($trialType == 2) {
            DB::update('update tb_loan_trial_phone set admin_id=?, updated_at=? where admin_id=0 and order_id in(
                        select t.order_id from (
                        select i.order_id from tb_loan_trial_phone i
                        left join tb_allocation_rule r
                        on i.order_id = r.order_id
                        where i.admin_id=0
                        order by
                        r.npwp desc,
                        r.bpjs desc,
                        r.telkomsel desc,
                        r.income desc,
                        r.sex desc,
                        r.risk_score desc,
                        i.id desc
                        ) as t)
                        limit ?', [$adminId, $now, $remainingCount]);
        }
        if ($trialType == 3) {
            DB::update('update tb_loan_trial_final set admin_id=?, updated_at=? where admin_id=0 and order_id in
                        (select t.order_id from (select i.order_id from tb_loan_trial_final i
                        left join tb_allocation_rule r
                        on i.order_id = r.order_id
                        where i.admin_id=0
                        order by
                        r.npwp desc,
                        r.bpjs desc,
                        r.telkomsel desc,
                        r.income desc,
                        r.sex desc,
                        r.risk_score desc,
                        i.id desc
                        ) as t)
                        limit ?', [$adminId, $now, $remainingCount]);
        }
       
        //优先分配评分大于90分的订单
       /*
        if ($trialType==1) {
            DB::update('update tb_loan_trial_info set admin_id=?, updated_at=? where admin_id=0 and order_id in (select t.order_id from (select i.order_id from tb_loan_trial_info i left join tb_risk_control_record r on i.order_id = r.order_id  where i.admin_id=0 and r.risk_score>90 order by r.risk_score desc) as t) limit ?', [$adminId, $now, $remainingCount]);
        }
        if ($trialType==2) {
            DB::update('update tb_loan_trial_phone set admin_id=?, updated_at=? where admin_id=0 and order_id in (select t.order_id from (select i.order_id from tb_loan_trial_phone i left join tb_risk_control_record r on i.order_id = r.order_id  where i.admin_id=0 and r.risk_score>90 order by r.risk_score desc) as t) limit ?', [$adminId, $now, $remainingCount]);
        }
        if ($trialType==3) {
            DB::update('update tb_loan_trial_final set admin_id=?, updated_at=? where admin_id=0 and order_id in (select t.order_id from (select i.order_id from tb_loan_trial_final i left join tb_risk_control_record r on i.order_id = r.order_id  where i.admin_id=0 and r.risk_score>90 order by r.risk_score desc) as t) limit ?', [$adminId, $now, $remainingCount]);
        }
        
        //========如果评分大于90分配完后，条数不够会继续分配，检查用户任务条数=========
        $orderCount = 0;
        if ($trialType==1) {
            $orderCount = LoanOrder::trialinfo()->where('order_status', '17')->count();
        }
        if ($trialType==2) {
            $orderCount = LoanOrder::trialphone()->where('order_status', '19')->count();
        }
        if ($trialType==3) {
            $orderCount = LoanOrder::trialfinal()->where('order_status', '21')->count();
        }
        if ($orderCount >= $normalCount) {
            return false;
        }
        
        //需要补齐的订单数量
        $remainingCount = $normalCount - $orderCount;
        if ($remainingCount<=0) {
            return false;
        }
    
        //正常分配
        if ($trialType==1) {
            DB::update('update tb_loan_trial_info set admin_id=?,updated_at=? where admin_id=0 order by id desc limit ?', [$adminId, $now, $remainingCount]);
        }
        if ($trialType==2) {
            DB::update('update tb_loan_trial_phone set admin_id=?,updated_at=? where admin_id=0 order by id desc limit ?', [$adminId, $now, $remainingCount]);
        }
        if ($trialType==3) {
            DB::update('update tb_loan_trial_final set admin_id=?,updated_at=? where admin_id=0 order by id desc limit ?', [$adminId, $now, $remainingCount]);
        }
        */
    }
}
