<?php

namespace App\Listeners;

use App\Events\PhoneAfter;
use App\Events\StartWork;
use App\Service\Assign;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PhoneAfterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhoneAfter  $event
     * @return void
     */
    public function handle(PhoneAfter $event)
    {
        
        if ($event->status==21) {
            $assignStatus = Assign::assign($event->order_id, 'final', [['id' => 0]]);
        }
        
        //信审完一条数据后，给审核人员补充一条新的数据
        event(new StartWork(2));
    }
}
