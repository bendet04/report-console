<?php

namespace App\Listeners;

use App\Events\FinalAfter;
use App\Events\StartWork;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FinalAfterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FinalAfter  $event
     * @return void
     */
    public function handle(FinalAfter $event)
    {
        //终审完一条数据后，给审核人员补充一条新的数据
        event(new StartWork(3));
    }
}
