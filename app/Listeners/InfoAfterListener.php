<?php

namespace App\Listeners;

use App\Events\InfoAfter;
use App\Events\StartWork;
use App\Service\Assign;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InfoAfterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InfoAfter  $event
     * @return void
     */
    public function handle(InfoAfter $event)
    {
        //信审成功后分配电审任务
        if ($event->status==19) {
            $assignStatus = Assign::assign($event->order_id, 'phone', [['id' => 0]]);
        }
        //信审完成一条数据后，给审核人员补充一条新的数据
        event(new StartWork(1));
    }
}
