<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class migrationTrack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrationTrack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '催收数据迁移';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exit;
        $maxLimit = 1000;
    
        $old = DB::connection('mysql_old');
        $new = DB::connection();
    
        //放款流水
        $count = $old->selectOne("select count('id') as counts  from tb_loan_track_order");
        $trackCount = $count->counts;
    
        $batch = ceil($trackCount/$maxLimit);
    
        for ($i = 0; $i < $batch; $i++) {
            $flowArr = $old->select("select * from tb_loan_track_order order by id asc limit ".($maxLimit*$i).",".$maxLimit);
            
            foreach ($flowArr as $k=>$v) {
                $new->insert('insert into tb_loan_order_track(
                  id, admin_id, order_id, track_time, track_status, created_at, updated_at
                  ) values (
                  ?, ?, ?, ?, ?, ?, ?
                )', [
                    $v->id, $v->aid, $v->lid, null, $v->track_status, $v->create_time, $v->update_time
                ]);
            }
        }
        
        //终审审核数据
        $count = $old->selectOne("select count('id') as counts  from tb_loan_apply_trial where `type`=2");
        $finalCount = $count->counts;
    
        $batch = ceil($finalCount/$maxLimit);
    
        for ($i = 0; $i < $batch; $i++) {
            $finalArr = $old->select("select * from tb_loan_apply_trial where `type`=2 order by id asc limit ".($maxLimit*$i).",".$maxLimit);
            foreach ($finalArr as $k=>$v) {
                $new->insert('insert into tb_loan_trial_final(
                  order_id, admin_id, remark, trial_time, created_at, updated_at
                ) values (
                  ?, ?, ?, ?, ?, ?
                )', [
                    $v->apply_id, $v->admin_id, $v->description, $v->trial_time, $v->create_time, $v->update_time
                ]);
            }
        }
        
        //初审审核数据
        $count = $old->selectOne("select count('id') as counts  from tb_loan_apply_trial where `type`=1");
        $infoCount = $count->counts;
    
        $batch = ceil($infoCount/$maxLimit);
    
        for ($i = 0; $i < $batch; $i++) {
            $infoArr = $old->select("select * from tb_loan_apply_trial where `type`=1 order by id asc limit ".($maxLimit*$i).",".$maxLimit);
            foreach ($infoArr as $k=>$v) {
                $new->insert('insert into tb_loan_trial_info(
                  order_id, admin_id, trial_time, created_at, updated_at
                ) values (
                  ?, ?, ?, ?, ?
                )', [
                    $v->apply_id, $v->admin_id, $v->trial_time, $v->create_time, $v->update_time
                ]);
                
            }
        }
        
        echo 'track and trial finish';
    }
}
