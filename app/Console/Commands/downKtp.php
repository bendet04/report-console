<?php

namespace App\Console\Commands;

use App\Models\OrderUserBasicInfo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class downKtp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downKtp';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '下载ktp照片';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ktpImage = OrderUserBasicInfo::select(['id','ktp_image_url'])->orderByDesc('id')->skip(1000)->take(10)->get();
        $ktpImage->map(function($image){
            echo $image['id']."\n";
            $image['ktp_image_url'] = env('CDN_URL').($image['ktp_image_url']);
            $down = $this->getKtpImage($image['ktp_image_url']);
        });
    }
    
    /**
     * 下载图片
     * @param $url
     * @param string $path
     * @return bool|int
     */
    private function getKtpImage($url, $path='ktp/')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        $info = $this->saveAsImage($url, $file, $path);
        curl_close($ch);
        return $info;
    }
    
    /**
     * 保存图片
     * @param $url
     * @param $file
     * @param $path
     * @return bool|int
     */
    private function saveAsImage($url, $file, $path)
    {
        $filename = pathinfo($url, PATHINFO_BASENAME);
        return Storage::put($path.$filename, $file);
    }
}
