<?php

namespace App\Console\Commands;

use app\index\common\consts\OrderStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Service\Foundation\BluePay;

class updateOrderStatusTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateOrderStatusTime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update order time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BluePay $bluepay)
    {
        $this->bank = $bluepay;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = DB::connection();
        $orderDetail = $db->select('
                SELECT
                    tb_order_status.id,
                    tb_order_status.order_id,
                    tb_loan_payment_flow.payment_flow,
                    tb_order_status.status,
                    tb_order_status.created_at
                FROM
                    tb_order_status
                LEFT JOIN tb_loan_payment_flow on tb_order_status.order_id = tb_loan_payment_flow.order_id
                where tb_order_status.status = 14
                and tb_order_status.created_at > "2018-06-28 20:30:00"
                and tb_order_status.created_at < "2018-06-28  22:00:00"
                and tb_order_status.order_id <> 68647
                and tb_order_status.order_id <> 64213
                ');

        foreach ($orderDetail as $v){
            Log::info('all order id is ' . $v->order_id . 'order status id '. $v->id . ' payment flow ' . $v->payment_flow . ' order time is ' . $v->created_at);
            $result = $this->selectBluepay($v->payment_flow);
            Log::info('order time is ' . $result['time']);
            $orderStatus['created_at'] = $result['time'];
            $result = \App\Models\OrderStatus::updateOne($orderStatus, $v->id);
//            die;
        }
    }
    public function selectBluepay($tId)
    {
        $host = env('BLUE_URL') . '/charge/indonesiaFintechTransfer/queryTransfer';
        $productId = env('BLUE_PRODUCT');

        $data = "";
        $key = env('BLUE_KEY');

        $dataInfo = $this->bank->AESphp($data,$key);
        $enctypt = md5("operatorId=6&productid=$productId&t_id=$tId".$key);

        $host = $host . '?operatorId=6&productid=' . $productId .'&t_id='.$tId.'&encrypt='. $enctypt;
        header("Content-type:text/html;charset=utf-8");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $host);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        # do not verify SSL cert
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        Log::error($host);
        $data = curl_exec($curl);
        Log::error('****');
        Log::error($data);
        Log::error('****');
        curl_close($curl);
        return json_decode($data, true);
    }
}
