<?php

namespace App\Console\Commands;

use App\Service\Assign;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class phoneReAssign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'phonereassign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '电审任务重新分配';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $maxLimit = 600;
    
        $db = DB::connection();
        
        //初审审核数据
        $count = $db->selectOne("select count('id') as counts  from tb_loan_trial_phone i left join tb_loan_order o on i.order_id=o.id where o.order_status=19");
        $infoCount = $count->counts;
        
        $batch = ceil($infoCount/$maxLimit);
    
        for ($i = 0; $i < $batch; $i++) {
            $infoArr = $db->select("select i.id, i.order_id from tb_loan_trial_phone i left join tb_loan_order o on i.order_id=o.id where o.order_status=19 limit ".($maxLimit*$i).",".$maxLimit);
            foreach ($infoArr as $k=>$v) {
                Assign::assign($v->order_id, 'phone');
            }
            echo $i."\n";
            sleep(1);
        }
        
        echo 'phone reassign finished';
    }
}
