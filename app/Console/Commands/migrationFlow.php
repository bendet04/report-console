<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class migrationFlow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrationFlow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '放款、还款流水数据迁移';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exit;
        $maxLimit = 1000;
    
        $old = DB::connection('mysql_old');
        $new = DB::connection();
    
        //放款流水
        $flow = $old->selectOne("select count('id') as flow_count  from tb_loan_payment_flow");
        $flowCount = $flow->flow_count;
    
        $batch = ceil($flowCount/$maxLimit);
    
        for ($i = 0; $i < $batch; $i++) {
            $flowArr = $old->select("select * from tb_loan_payment_flow order by id asc limit ".($maxLimit*$i).",".$maxLimit);
            foreach ($flowArr as $k=>$v){
                if ($v->payment_flow_status==2) {
                    $payment_status = 1;
                }else{
                    $payment_status = $v->payment_flow_status;
                }
                
                $new->insert('insert into tb_loan_payment_flow(
                  id, order_id, payment_amount, payment_flow, payment_status, payment_code, payment_time, `code`, created_at, updated_at
                ) values (
                  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                )', [
                    $v->id, $v->lid, $v->payment_amount, $v->payment_id, $payment_status, 0, $v->create_time, $v->pay_channel_code,
                    $v->create_time, $v->update_time
                ]);
            }
        }
    
        //还款流水
        $flow = $old->selectOne("select count('id') as flow_count  from tb_loan_repayment_flow");
        $flowCount = $flow->flow_count;
    
        $batch = ceil($flowCount/$maxLimit);
    
        for ($i = 0; $i < $batch; $i++) {
            $flowArr = $old->select("select * from tb_loan_repayment_flow order by id asc limit ".($maxLimit*$i).",".$maxLimit);
            foreach ($flowArr as $k=>$v){
                
            
                $new->insert('insert into tb_loan_repayment_flow(
                  id, order_id, `type`, repayment_amount, repayment_flow, repayment_code, repayment_status, repayment_time, created_at, updated_at
                ) values (
                  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                )', [
                    $v->id, $v->oid, $v->repayment_type, $v->repayment_amount, $v->virtual_id, $v->repayment_channel_code,
                    $v->repayment_status, $v->create_time, $v->create_time, $v->update_time
                ]);
            }
        }
        
        echo 'flow finish';
    }
}
