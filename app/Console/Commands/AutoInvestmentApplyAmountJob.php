<?php

namespace App\Console\Commands;

use App\Consts\InvestmentStatus;
use App\Models\Investment_user;
use App\Models\InvestsmentRelation;
use App\Service\OrderFactory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AutoInvestmentApplyAmountJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AutoInvestmentApplyAmountJob';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Investment Withdraw Apply AutoJob';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = OrderFactory::getInstance('payment');
        Investment_user::walkInvestment('status', [InvestmentStatus::INVESTMENTSTATUS6],function($invest) use ($model) {
            $ret = $model->payment($invest->id, 'investment');
            Log::info('Investment Withdraw Apply AutoJob:'.$ret);
            $payment = json_decode($ret, true);
            if (!empty($payment['code']) && $payment['code']!=1) {
                InvestsmentRelation::updateStatus($invest->id, 4);
                Investment_user::updateStatus($invest->id, InvestmentStatus::INVESTMENTSTATUS8);
            }
        });
    }
}
