<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class migration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrationOrder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '订单数据库迁移';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exit;
        $maxLimit = 1000;
        
        $old = DB::connection('mysql_old');
        $new = DB::connection();
        
        
        $apply = $old->selectOne("select count('id') as apply_count  from tb_loan_apply where id>38182");
        $applyCount = $apply->apply_count;
        
        $applyBatch = ceil($applyCount/$maxLimit);
    
        for ($i = 0; $i < $applyBatch; $i++) {
            $applyArr = $old->select("select * from tb_loan_apply where id>38182 order by id asc limit ".($maxLimit*$i).",".$maxLimit);
            foreach ($applyArr as $k=>$v){
                $order = $old->selectOne("select * from tb_loan_order where lid={$v->id} order by id asc");
                $statusArr = $old->select("select * from tb_order_status where oid={$v->id} order by id asc");
                if (!$statusArr){
                    echo $v->id."no status---";
                    continue;
                }
    
                $v->house_type = $v->house_type==='' ? 0 : $v->house_type;
                //导入用户快照
                $new->insert("insert into tb_order_user_basic_info(
                    id, uid, device_id, pid, children_number, `number`, house_phone, email, mother_name, `name`,
                    faceBook, phone_number, ktp_number, ktp_image_url, video_url,  gender, birth, education,
                    marriage, address, province, rengence, district, career, villages,  income_level, paid_period,
                    company_type, company_name, company_phone, company_address, company_province, company_rengence, company_district,
                    company_villages, work_image_url, relative_type_first, relative_name_first, relative_phone_first, relative_type_second,
                    relative_name_second, relative_phone_second, payee_name, bank_name, bank_phone, bank_number, work_period,
                    house_type, living_period, apply_time, status, created_at, updated_at
                    ) values(
                    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?
                )", [
                    $v->id, $v->uid, $v->device_id, $v->pid, $v->children_number, $v->number, $v->house_phone, $v->email, $v->mother_name, $v->name,
                    $v->faceBook, $v->phone_number, $v->ktp_number, $v->ktp_image_url, $v->video_url, $v->gender, $v->birth, $v->education,
                    $v->marriage, $v->address, $v->province, $v->rengence, $v->district, $v->career, $v->villages, $v->income_level, $v->paid_period,
                    $v->company_type, $v->company_name, $v->company_phone, $v->company_address, $v->company_province, $v->company_rengence, $v->company_district,
                    $v->company_villages, $v->work_image_url, $v->relative_type_first, $v->relative_name_first, $v->relative_phone_first, $v->relative_type_second,
                    $v->relative_name_second, $v->relative_phone_second, $v->payee_name, $v->bank_name, $v->bank_phone, $v->bank_number, $v->work_period,
                    $v->house_type, $v->living_period, $v->apply_time, 0, $v->create_time, $v->update_time
                ]);
                
                
                
                
                //导入订单状态
                foreach ($statusArr as $key=>$val) {
                    if ($val->order_status==3){
                        $val->order_status = 21;
                    }
                    if ($val->order_status==4){
                        $val->order_status = 22;
                    }
                    $new->insert('insert into tb_order_status(order_id, status, created_at, updated_at) values(
                      ?, ?, ?, ?
                    )', [
                        $val->oid, $val->order_status, $val->create_time, $val->update_time
                    ]);
                }
    
    
                
    
                //计算新的订单状态
                $status = end($statusArr);
    
                //loan_apply 0：待初审；1：初审通过，待终审；2：初审未通过，待终审；3：终审通过；4：终审未通过;5:申请等待中；6：申请取消
                //loan_order 1：待放款审核；2：审核通过，放款中；3：放款审核未通过；4：还款中 5：还款缓期中 6：逾期中 7：部分还清 8：己还清；9：用户取消；
    
                //1:创建待确认 2:两分钟用户取消 5:终审失败 6:终审成功 7:放款审核未通过 8:放款中 11:还款己逾期 12:还款己还清 13:还款未还清；14：放款成功待还款；15：放款失败（等待重新放款）
                //16风控队列中 17 风控成功，待信审 18风控失败 19信审成功 20信审失败 21电审成功 22电审失败 23用户确认审核失败 24终止放款
                
                $apply_status = [
                    '0' => 17,
                    '1' => 21,
                    '2' => 22,
                    '3' => 6,
                    '4' => 5,
                    '5' => 1,
                    '6' => 2,
                    '7' => 23,
                ];
                $order_status = $apply_status[$v->apply_status];
    
    
    
                
                if (isset($order->order_status)){
                    $statusArr = [
                        '1' => 6,
                        '2' => 8,
                        '3' => 7,
                        '4' => 14,
                        '5' => 10,
                        '6' => 11,
                        '7' => 13,
                        '8' => 12,
                        '9' => 23,
                    ];
                    $order_status = $statusArr[$order->order_status];
                }
                
                if ($order_status!=$status->order_status){
                    $new->insert('insert into tb_order_status(order_id, status, created_at, updated_at) values(
                      ?, ?, ?, ?
                    )', [
                        $v->id, $order_status, $v->update_time, $v->update_time
                    ]);
                }
                
                
                $payment_date = isset($order->payment_date) ? $order->payment_date : null; //借款到账日
                $loan_deadline = isset($order->loan_deadline) ? $order->loan_deadline : null; //到期日
                $loan_suspend_date = isset($order->loan_suspend_date) ? $order->loan_suspend_date : null; //缓期日
                $loan_overdue_date = isset($order->loan_overdue_date) ? $order->loan_overdue_date : null; //逾期日
                $loan_repayment_date = isset($order->loan_repayment_date) ? $order->loan_repayment_date : null; //还款日
                $loan_delay_time = null; //延期时间
                $loan_charge_amount = isset($order->loan_charge_amount) ? $order->loan_charge_amount : ($v->apply_count*0.1); //手续费
                $reciprocal_amount = isset($order->reciprocal_amount) ? $order->reciprocal_amount : ($v->apply_count*0.9); //到手金额
                $overdue_amount = isset($order->overdue_amount) ? $order->overdue_amount : 0; //逾期利息
                $already_repayment_amount = isset($order->already_repayment_amount) ? $order->already_repayment_amount : 0; //已还的金额
                $remaining_repayment_amount = isset($order->remaining_repayment_amount) ? $order->remaining_repayment_amount : 0; //未还清的
                
                $day = 7;
                if ($v->pid==2){
                    $day=14;
                }
                
                $nornal_rate = 0.008;
                if ($v->create_time > '2018-06-20 00:00:00'){
                    $nornal_rate = 0.01;
                }
                
                
                $repayment_amount = $v->apply_count + $v->apply_count * $nornal_rate * $day;
                
                $repayment_amount = isset($order->repayment_amount) ? $order->repayment_amount : $repayment_amount; //应还款金额
                
                //导入订单数据
                $new->insert("insert into tb_loan_order(
                  id, order_number, uid, pid, basic_id, pay_type, loan_period, apply_amount, order_time, order_status, payment_date,
                  loan_deadline, loan_suspend_date, loan_overdue_date, loan_repayment_date, loan_delay_time, loan_charge_amount,
                  reciprocal_amount, overdue_amount, already_repayment_amount, remaining_repayment_amount, repayment_amount,
                  created_at, updated_at
                  ) values(
                  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                  ?, ?, ?, ?, ?, ?,
                  ?, ?, ?, ?, ?,
                  ?, ?
                )", [
                    $v->id, $v->number, $v->uid, $v->pid, $v->id, $v->pay_type, $v->pid, $v->apply_count, $v->apply_time, $order_status, $payment_date,
                    $loan_deadline, $loan_suspend_date, $loan_overdue_date, $loan_repayment_date, $loan_delay_time, $loan_charge_amount,
                    $reciprocal_amount, $overdue_amount, $already_repayment_amount, $remaining_repayment_amount, $repayment_amount,
                    $v->create_time, $v->update_time
                ]);
            }
        }
        
        echo 'order finish!!!';
    }
}
