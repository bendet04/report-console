<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $table = 'token';
    
    /**
     * 根据user_id获取token
     * @param $user_id
     * @return mixed
     */
    protected function getToken($user_id)
    {
        $token = Token::where('user_id', $user_id)->orderByDesc('id')->first();
        if ($token) {
            return $token->token;
        }
    }
}
