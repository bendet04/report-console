<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUserLog extends Model
{
    protected $table = 'admin_user_log';
    protected $fillable = ['username', 'ip', 'login_result'];
    
    protected function saveOne($paramArr)
    {
        return static::create($paramArr);
    }
}
