<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminCheckUser extends Model
{
    protected $table = 'admin_check_user';
    
    
    protected function getOne($uid)
    {
        return AdminCheckUser::where('user_id', $uid)->first();
    }
}
