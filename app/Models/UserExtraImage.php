<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserExtraImage extends Model
{
    protected $table = 'user_extra_image';
    
    protected function getOne($uid)
    {
        return UserExtraImage::where('user_id', $uid)->first();
    }
}
