<?php

namespace App\Models;

use App\Consts\InvestmentStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Investment_user extends Model
{
    protected $table = 'investment_user';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'uid',
        'status',
        'already_interest',
        'total_apply_amount',
    ];

    public static function getList($paramArr=[], $pageNum = 20)
    {
        $paramArr[] = ['is_del', '=', 0];
        $status = [];
        foreach ($paramArr as $k=>$v) {
            if ($v[1]=='in') {
                $status = $v;
                unset($paramArr[$k]);
            }
        }
        $query = static::where($paramArr);
        if ($status) {
            $query->whereIn($status[0], $status[2]);
        }
        
        return $query->orderByDesc('updated_at')->paginate($pageNum);
    }
    
    public static function getListCount($paramArr=[])
    {
        $paramArr[] = ['is_del', '=', 0];
        $status = [];
        foreach ($paramArr as $k=>$v) {
            if ($v[1]=='in') {
                $status = $v;
                unset($paramArr[$k]);
            }
        }
        $query = static::select([DB::raw('sum(amount) as total')])->where($paramArr);
        if ($status) {
            $query->whereIn($status[0], $status[2]);
        }

        return $query->first();
    }
    
    protected function getByPayment($where, $pageSize=20)
    {
        $query = static::whereNull('payTime');
        foreach ($where as $k=>$v) {
            if (!empty($v[2]) && is_array($v[2])) {
                $query->whereIn($v[0], $v[2]);
            } else {
                $query->where($v[0], $v[1], $v[2]);
            }
        }
        $orders = $query->paginate($pageSize);
//        $orders = $query->toSql();
        return $orders;
    }
    
    protected function walk($where, $callback)
    {
        $orders = static::whereNull('payTime')->where($where)->get();
        if (!empty($orders)) {
            foreach ($orders as $a) {
                $callback($a);
            }
        } else {
            Log::error("empty?");
        }
    }
    
    protected function walkInvestment($field, $val, $callback)
    {
        $orders = static::whereIn($field, $val)->get();
        if (!empty($orders)) {
            foreach ($orders as $a) {
                $callback($a);
            }
        } else {
            Log::error("empty?");
        }
    }
    
    public static function getOne($id)
    {
        return static::where('id', $id)->first();
    }
    
    //审核失败
    public static function updateconfirm($id)
    {
        return static::where('id', $id)->where('status', InvestmentStatus::INVESTMENTSTATUS1)->update(['status'=> InvestmentStatus::INVESTMENTSTATUS9]);
    }
    
    //修改投资状态
    public static function updateStatus($id, $status)
    {
//        return static::where(['id' => $id])->where('status', $originStatus)->update(['status' => $status]);
        return static::where(['id' => $id])->update(['status' => $status]);
    }

    //删除用户订单
    public static function del($id)
    {
        $arr1 = DB::table('investment_relation')->where(['investors_id' => $id])->delete();
        $arr = DB::table('investment_user')->where(['id' => $id])->delete();
        if($arr&&$arr1){
            return true;
        }
    }
    
    /**
     * 提现成功
     * @param $id
     * @return bool
     */
    protected function paymentSuccess($id, $invest)
    {
        if (in_array($invest->status, InvestmentStatus::paymentArr())){
            InvestsmentRelation::updateStatus($id, 5);
            $already_apply_amount = $invest->already_apply_amount + $invest->total_apply_amount;
    
            $update = ['total_apply_amount'=>0, 'already_apply_amount'=>$already_apply_amount];
            $totalOrder = InvestsmentRelation::getCountByInvestId($id);
            $totalCompleteOrder = InvestsmentRelation::getCompleteCountByInvestId($id);
            
            if ($totalOrder === $totalCompleteOrder) {
                $update['status'] = InvestmentStatus::INVESTMENTSTATUS12;
            } elseif ($totalOrder > $totalCompleteOrder) {
                $update['status'] = InvestmentStatus::INVESTMENTSTATUS5;
            }
            
            return static::where('id', $id)->update($update);
        }
        
        return false;
    }
    
    
    public function updatePaytime($paytime)
    {
        $this->payTime = $paytime;
        $this->status = InvestmentStatus::INVESTMENTSTATUS5;
        return $this->save();
    }
    
    protected function updateAmount($id, $paramArr)
    {
        return static::where('id', $id)->update($paramArr);
    }
}
