<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRole extends Model
{
//    use SoftDeletes;

    protected  $table = 'admin_role';
    protected $primaryKey = 'rid';
    protected  $fillable = ['role_name', 'status', 'role_desc'];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    public function selectRoleDetail($page)
    {
        return $this::paginate($page);
    }
    public function getRoleInfoByLike($where, $page)
    {
        return $this::where($where)
                    ->where('delete', '<>', 1)
                    ->paginate($page);
    }
    public function selectRoleDetailInfo()
    {
        return $this::where('rid', '<>', 1)
                ->where('delete', '<>', 1)
                ->get();
    }
    public function addRoleDetail($name,$data)
    {
        return $this::updateOrCreate($name, $data);
    }
    public function selectRoleNameIsExist($roleName)
    {
        return $this::where('role_name', $roleName)
                    ->first();
    }
    public function selectRoleDetailById($id)
    {
        return $this::where('rid', $id)->first();
    }
    public function updateRoleDetailById($where, $id)
    {
        return $this::where('rid', $id)->update($where);
    }
    public function softDeleteRoleById($rid)
    {
//        $info = $this::where('rid', $rid)->first();
//        $info->delete();
//        return $info->trashed();
        $data['delete'] = 1;
        return $this::where('rid', $rid)->update($data);
    }
}
