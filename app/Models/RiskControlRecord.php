<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Consts\RiskRefuse;
use Illuminate\Support\Facades\Log;


class RiskControlRecord extends Model
{
    protected $table = 'risk_control_record';
    
    protected function getList($paramArr=[], $pageNum=20)
    {
        $options = [
            ['risk_control_record.status', '=', 1],
        ];
        $options = array_merge($options ,array_filter($paramArr));
        
        $query = static::select([
            'risk_control_record.id',
            'risk_control_record.refuse_id',
            'loan_order.order_number',
            'order_user_basic_info.name',
            'order_user_basic_info.ktp_number',
            'risk_control_record.created_at',
        ]);
        $query->join('loan_order', 'loan_order.id', '=', 'risk_control_record.order_id');
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $data = $query->where($options)->paginate($pageNum);
        
        return $data;
    }
    protected function getByReason($oid)
    {
      return static::where(['order_id'=>$oid])->first(['refuse_id']);
    }
    protected function DenialList($where,$type,$detail,$pageNum=20)
    {
        //获取风控 黑产 近期重复提交的拒绝数据
        $data=DB::table('order_status as s')
              ->join('loan_order as o','o.id', '=', 's.order_id')
              ->join('order_user_basic_info as  b','b.id', '=', 'o.basic_id')
              ->join($where['table'],$where['left'],$where['flag'],$where['right'])
              ->select('s.order_id','s.created_at','o.order_time','o.order_number','b.name','b.uid as user_id','b.ktp_number')
              ->whereRaw('tb_s.status in(16,18) and tb_s.order_id!=0')
              ->whereRaw($where['param'])
              ->groupBy('s.order_id')
              ->orderBy('s.order_id','DESC')
              ->paginate($pageNum);
        $data->map(function ($item, $k)use($data,$where,$type,$detail){
                if((time()+300)<strtotime($item->created_at))
                {
                    unset($data[$k]);
                }
              //获取用户uuid
                $uuid=DB::table('user_access')->where(['user_id'=>$item->user_id])->first(['uuid']);
                if(!empty($uuid)){
                    $uuid=$uuid->uuid;
                }
                $typeArr=$this->refuse($type,$uuid,$item,$k,$data,$detail);
                $item->type=$typeArr['type'];
                $item->refuse=$typeArr['refuse'];
            return $item;

        });
        return $data;
    }
//    //根据条件获取黑产拒绝类型
    protected function refuse($type,$uuid,$item,$k,$data,$detail){
        $arr=[];
        switch($type){
            case 'day':
                //七天自动拒绝类型
                $day=DB::table("order_refuse_time")->whereRaw("ktp_number='".$item->ktp_number."'")->groupBy('ktp_number')->get(['ktp_number']);
                if(!empty($day)) {
                    $arr['type']='short.time';
                    $arr['refuse']='no.detailed.reasons';
                    return $arr;
                }else{
                    unset($data[$k]);
                }
                break;
            case 'risk':
                //风控拒绝
                $where=!empty($detail)?['status'=>1,'order_id'=>$item->order_id,'refuse_id'=>$detail]:['status'=>1,'order_id'=>$item->order_id];
                $risk=static::where($where)->first(['refuse_id']);
               if(!empty($risk)){
                    $arr['type']='risk.reason';
//                    $arr['refuse']=RiskRefuse::toString($risk->refuse_id);
                    $arr['refuse']=$risk->refuse_id;
                    return $arr;
                }else{
                    unset($data[$k]);
                }
                break;
            case 'black':
                //黑产
                $where=['status'=>1,'order_id'=>$item->order_id,'refuse_id'=>'DF_BLACK'];
                $risk=static::where($where)->first(['refuse_id']);
                unset($uuid);
                if(empty($black)){
                    $arr['type']='black.reason';
                    $arr['refuse']='no.detailed.reasons';
                    return $arr;

                }else{
                    unset($data[$k]);
                }
                break;
        }
    }
    //请求风控黑名单
    protected  function getBlack($username,$ktp_number)
    {
        $uri='/gather/find/blacklist';
        $url = env('RISK_URL') . $uri;
//        $url = 'http://10.2.3.174:4539/gather/find/blacklist';
        $username=strtolower($username);
        $post_data=[ 'ktp_number'=>$ktp_number,
                     'username'  =>$username];
        $curl = curl_init();
        //设置提交的url
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        //设置获取的信息以文件流的形式返回。而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //參数为0表示不带头文件，为1表示带头文件
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        //运行命令
        $data = curl_exec($curl);
        //获得数据并返回
        if ($data != false) {
            $data=json_decode($data,true);
            if (!empty($data['result_code']) && $data['result_code'] == 10000){
                $data['data'][0]['create_at']=date('Y-m-d: H:i:s',$data['data'][0]['create_at']);
                return json_encode(['code'=>'200','data'=>$data['data']]);
            }else{
                Log::error('RiskBlackErrorCode_:'.$data['result_code']);
                return json_encode(['code'=>'202','info'=>'Not on the blacklist.']);
            }
        } else {
            Log::error('FindRiskBlackCurlErrorCode_:'.curl_error($curl));
            return json_encode(['code'=>'203','info'=>'Request Error']);
        }
        curl_close($curl);
    }
    //请求风控黑名单
    protected  function addBlack($post_data,$uri)
    {
        $url = env('RISK_URL').$uri;
//        $url = 'http://10.2.3.174:4539/gather/add/blacklist';
        $curl = curl_init();
        //设置提交的url
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        //设置获取的信息以文件流的形式返回。而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //參数为0表示不带头文件，为1表示带头文件
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$post_data);
        //运行命令
        $data = curl_exec($curl);
        //获得数据并返回
        if ($data != false) {
            $data=json_decode($data,true);
            if (!empty($data['result_code']) && $data['result_code'] == 10000){
                if(empty($data['data']['exist'])&&empty($data['data']['notfound'])){
                    return json_encode(['code'=>'200','info'=>'success']);
                } else{
                    $info='';
                    if(!empty($data['data']['exist'])){
                        $exist=implode(',',$data['data']['exist']);
                        $info.="KTP:".$exist." Already Exist";
                    }if(!empty($data['data']['notfound'])){
                        $notfound=implode(',',$data['data']['notfound']);
                        $info.="<br/>";
                        $info.="Risk Error:".$notfound." Not Found";
                    }
                    return json_encode(['code'=>'204','info'=>$info]);
                }
             }
             else{
                Log::error('RiskBlackErrorCode_:'.$data['result_code']);
                return json_encode(['code'=>'202','info'=>'Request Error']);
            }
        } else {
            Log::error('FindRiskBlackCurlErrorCode_:'.curl_error($curl));
            return json_encode(['code'=>'203','info'=>'Add Request Error']);
        }
        curl_close($curl);
    }

}
