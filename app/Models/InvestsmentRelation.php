<?php

namespace App\Models;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Database\Eloquent\Model;

class InvestsmentRelation extends Model
{
    protected $table = 'investment_relation';
    protected $fillable = ['investors_id', 'loan_order_id', 'loan_name', 'loan_amount'];
    
    protected function getList($investId, $status=[])
    {
        $query = static::select(['investment_relation.*', 'loan_order.order_status', 'loan_order.apply_amount', 'loan_order.payment_date', 'loan_order.loan_deadline', 'loan_order.order_time']);
        $query->join('loan_order', 'loan_order.id', '=', 'investment_relation.loan_order_id');
        if ($status) {
            $query->whereIn('investment_relation.status', $status);
        }
        return $query->where('investment_relation.investors_id', $investId)->get();
    }
    
    protected function getCountByInvestId($investId)
    {
        return static::where('investors_id', $investId)->count();
    }
    
    protected function getListByApply($where, $pageSize=20)
    {
        $query = static::select('investment_user.*')->where('investment_relation.status', 3)->where($where);
        $query->join('investment_user', 'investment_user.id', '=', 'investment_relation.investors_id');
        $orders = $query->groupBy('investors_id')->paginate($pageSize);
//        $orders = $query->groupBy('investors_id')->toSql();
        return $orders;
    }
    
    protected function getListCompleteCount($where)
    {
        $query = static::select('investment_relation.*')->where('investment_relation.status', 5)->where($where);
        $query->join('investment_user', 'investment_user.id', '=', 'investment_relation.investors_id');
//        $orders = $query->groupBy('investors_id')->get();
        $orders = $query->groupBy('investors_id')->get();
    
        $total = 0;
        foreach ($orders as $order) {
            $interest = $this->getInterest($order->loan_period, $order->loan_amount);
            $total = $total + $interest + $order->loan_amount;
        }
        
        return $total;
    }
    
    protected function getListComplete($where, $pageSize=20)
    {
        $query = static::select('investment_user.*')->where('investment_relation.status', 5)->where($where);
        $query->join('investment_user', 'investment_user.id', '=', 'investment_relation.investors_id');
        $orders = $query->groupBy('investors_id')->paginate($pageSize);
//        $orders = $query->groupBy('investors_id')->toSql();
        return $orders;
    }
    
    protected function getCompleteCountByInvestId($investId)
    {
        return static::where('investors_id', $investId)->where('status', 5)->count();
    }
    
    protected function getByOrderId($order_id)
    {
        return static::where('loan_order_id', $order_id)->orderByDesc('id')->first();
    }
    
    protected function saveOne($paramArr)
    {
        return static::create($paramArr);
    }
    
    protected function delByInvestmentId($investId)
    {
        return static::where('investors_id', $investId)->delete();
    }
    
    protected function getInterest($period, $amount)
    {
        $rate = Investment::getOne($period);
        return $amount * $rate->rate / 365 * $period;
    }
    
    protected function walk($callback)
    {
        $orders = static::where('status', 3)->groupBy('investors_id')->get();
        if (!empty($orders)) {
            foreach ($orders as $a) {
                $callback($a);
            }
        } else {
            Log::error("empty?");
        }
    }
    
    protected function updateStatus($investId, $status)
    {
        return static::where('investors_id', $investId)->update(['status'=>$status]);
    }
    
    protected function updateStatusByOrderId($id)
    {
        return static::where('id', $id)->where('status', 1)->update(['status'=>2]);
    }
}
