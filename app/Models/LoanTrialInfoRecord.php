<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanTrialInfoRecord extends Model
{
    protected $table = 'loan_trial_info_record';
    
    protected $fillable = ['id', 'order_id', 'admin_id', 'npwp', 'img_status', 'pic_status', 'npwp_status', 'salary_status'];
    
    protected function saveOne($order_id, $paramArr)
    {
        return LoanTrialInfoRecord::updateOrCreate(['order_id'=>$order_id], $paramArr);
    }
    protected function getRecordOne($param)
    {
        return $this::where($param)->orderByDesc('id')->first();
    }
    
    protected function getOne($order_id)
    {
        return static::with('admin')->where('order_id', $order_id)->orderByDesc('id')->first();
    }
    
    public function admin()
    {
        return $this->hasOne('App\Models\AdminUser', 'id', 'admin_id');
    }
}
