<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Channel extends Model
{
    protected $table = 'channel_list';
    protected $fillable = ['channel_name', 'status'];
    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';
//    public $timestamps = false;
    public function updateRateById($data, $id)
    {

        return $this::where('id', $id)
            ->update($data);
    }
    //获取渠道列表
    public function getList($pageNum=20)
    {
        return $this::where('is_del',0)->paginate($pageNum);
    }
    //添加渠道
    public function insert($param)
    {
       return DB::insert('insert ignore into tb_channel_list (chan_name,channel_name,status,create_at) values (?,?,?,?)',[$param['name'],$param['id'],$param['status'],now()]);
    }

   public function updateOrCreate($param)
   {
       return $this::created($param);
   }

}
