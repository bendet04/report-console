<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ChannelCount extends Model
{
    protected $table = 'user_count_channel';
    protected $fillable = ['channel'];
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    public $timestamps = false;
    //查询渠道的安装量
    public function getCount($paramArr=[],$pageNum = 20)
    {

        $where="  1=1 ";
        if(isset($paramArr)&&count($paramArr)!=0)
        {
            if(!empty($paramArr['channel'])&&!is_numeric($paramArr['channel']))
            {
                $where.=" AND channel='".$paramArr['channel']."'";
            }
            if(!empty($paramArr['source']))
            {
                $where.=" AND source='".$paramArr['source']."'";

            }
            if(!empty($paramArr['start']))
            {
                $where.=" AND create_time>='".$paramArr['start']."'";
            }
            if(!empty($paramArr['end']))
            {
                $where.=" AND create_time<='".$paramArr['end']."'";
            }
        }
        else
        {
            $where.=" and to_days(create_time)=to_days(now()) ";
        }

        $users = DB::table('user_count_channel')
                 ->join('channel_list', 'channel', '=', 'channel_list.channel_name')
                ->select(
                DB::raw('YEAR(create_time) years'),
                DB::raw('MONTH(create_time) months'),
                DB::raw('DAY(create_time) days'),
                'source',
                DB::raw('tb_channel_list.chan_name channel'),
                DB::raw('COUNT(*) num')
            )
            ->whereRaw($where)
            ->groupBy(
                'channel',
                DB::raw('YEAR(create_time)'),
                DB::raw('MONTH(create_time)'),
                DB::raw('DAY(create_time)'),
                'source'

            );
//        dd($users->toSql());
          if($pageNum==0)
           {
               return $users->get();
           }
           else
           {
               return $users->paginate($pageNum);
           }




    }
    //获取渠道名称
    public function channel_name()
    {
     return DB::table('channel_list')->where('is_del',0)->get(['chan_name','channel_name']);
    }

}
