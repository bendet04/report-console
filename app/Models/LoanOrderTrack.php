<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LoanOrderTrack extends Model
{
    protected $table = 'loan_order_track';
    protected $fillable = ['admin_id', 'order_id', 'track_time', 'track_status', 'contacts','remark', 'records', 'prev_admin', 'promise_time', 'status', 'is_remind'];
    
    protected function saveOne($order_id, $paramArr)
    {
        return LoanOrderTrack::updateOrCreate(['order_id'=>$order_id], $paramArr);
    }
    
    protected function saveItem($paramArr)
    {
        return static::updateOrCreate($paramArr);
    }
    
    protected function getOne($order_id)
    {
        return static::where('order_id', $order_id)->first();
    }
    protected function selectTrailByAdminId($admin_id)
    {
//      return LoanTrialInfo::where('admin_id', $admin_id)->get();
        return $this::select('loan_order_track.order_id','loan_order.order_status')
            ->join('admin_user', 'loan_order_track.admin_id', 'admin_user.id')
            ->join('loan_order', 'loan_order_track.order_id', 'loan_order.id')
            ->where('loan_order_track.admin_id', $admin_id)
            ->whereIn('loan_order.order_status', [9,10,11,13,14,25])

            ->get();
    }
    protected function getWillingness($start,$end)
    {
        return $this::select(DB::raw('tb_loan_order_track.promise_time,tb_admin_user.admin_username'))
            ->join('admin_user', 'loan_order_track.admin_id', 'admin_user.id')
            ->whereRaw("track_time>='$start' and track_time<='$end'")
            ->get();
    }
    
}
