<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminAccess extends Model
{
    use SoftDeletes;

    protected $table = 'admin_access';
    protected $fillable = ['access_name', 'access_leval', 'access_url', 'is_use'];


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    //查询权限的信息
    public function selectAccessInfo($page)
    {
        return $this::paginate($page);
    }
    public function selectSecondAccessInfo($page)
    {
        return $this::where('access_leval', '<>', 0)
//                ->where('is_use', '<>', 1)
                ->paginate($page);
    }
    //通过权限的名字模糊查询
    public function getLikeInfoByAccessName($where, $page)
    {
        return $this::where($where)
                ->paginate($page);
    }
//    查询父级的权限的名称
    public function getParentAccessName()
    {
        return $this::where('access_leval', 0)
                ->where('is_use', '<>', 1)
                ->get();
    }
//    新增权限的
    public function addAccessDetail($name,$data)
    {
        return $this::updateOrCreate($name,$data);
    }
//    通过accessName查询id
    public function getIdByAccessName($accessName)
    {
        return $this::where('access_name', $accessName)->first();
    }
    public function getAccessDetailById($id)
    {
        return $this::findOrFail($id);
    }
//    更改权限的信息
    public function updateAccessInfoById($data, $id)
    {
        return $this::where('id', $id)->update($data);
    }
    public function softDeleteAccessById($id)
    {
        $info = $this::find($id);
        $info->delete();
        return $info->trashed();
    }
    
    
    protected function getList($ids)
    {
        return static::whereIn('id', $ids)->where('is_use', '<>', 1)->orderBy('create_time')->get();
    }
    
    protected function getAll()
    {
        return static::where('is_use', '<>', 1)->orderBy('create_time')->get();
    }
}
