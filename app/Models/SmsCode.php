<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
    protected $table = 'sms_code';
    protected $fillable = ['provider', 'cc', 'phone', 'code', 'type', 'sms_provider', 'message', 'create_time', 'check_time'];
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'check_time';
    
    protected function addSms($data=[])
    {
        return static::create($data);
    }
    
    protected function saveData($id, $paramArr)
    {
        return static::updateOrCreate(['id'=>$id], $paramArr);
    }
}
