<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefuseDelayRecord extends Model
{
    protected $table = 'refuse_delay_record';
    protected $fillable = ['refuse_time', 'ktp_number'];

    protected function getOne($order_id)
    {
        return $this::where(['order_id' => $order_id])->get();
    }

}
