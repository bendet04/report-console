<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LoanTrialPhoneRecord extends Model
{
    protected $table = 'loan_trial_phone_record';
    protected $fillable = ['order_id', 'admin_id', 'work', 'peason', 'work_phone_status', 'phone_status'];
    
    protected function getByOrderId($order_id)
    {
        return LoanTrialPhoneRecord::with('admin')->where('order_id', $order_id)->first();
    }
    
    public function admin()
    {
        return $this->hasOne('App\Models\AdminUser', 'id', 'admin_id');
    }
    
    protected function saveOne($order_id, $paramArr)
    {
        $paramArr['admin_id'] = Auth::id();
        $paramArr['order_id'] = $order_id;
        return LoanTrialPhoneRecord::updateOrCreate(['order_id'=>$order_id], $paramArr);
    }
    protected function unattendedNum($param)
    {
        return static::where($param)->count();
    }
}
