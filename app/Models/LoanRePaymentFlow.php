<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanRePaymentFlow extends Model
{
    protected $table = 'loan_repayment_flow';
    protected $fillable = ['order_id', 'type', 'repayment_amount', 'repayment_flow', 'repayment_code', 'repayment_status', 'repayment_time'];
    
    protected function getList($order_id)
    {
        return LoanRePaymentFlow::where('order_id', $order_id)->orderByDesc('id')->get();
    }
    
    protected function saveOne($repayment_flow, $paramArr)
    {
        return LoanRePaymentFlow::updateOrCreate(['repayment_flow'=>$repayment_flow], $paramArr);
    }
    protected function getRepaymentAmount($orderId)
    {
        return static::where(['order_id'=>$orderId,'repayment_status'=>1])->sum('repayment_amount');
    }
}
