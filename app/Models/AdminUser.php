<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class AdminUser extends Authenticatable
{
    use Notifiable;

    protected $table = 'admin_user';
    public $timestamp = true;

    //数据库不存在remember_token字段，退出登录报错
    protected $rememberTokenName = '';
    protected $fillable = ['admin_number', 'admin_username', 'real_username', 'admin_pwd', 'admin_status', 'is_distribution', 'is_super_admin', 'country_code', 'is_del', 'admin_phone', 'admin_email', 'admin_address', 'card_number', 'card_type', 'no_heared'];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    /**
     * 获取管理员
     * @param array $paramArr
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function getOne($paramArr = [])
    {
        $where = [
            'admin_status' => 0,
            'is_del' => 0,
        ];
        if ($paramArr) $where = array_merge($where, $paramArr);

        return AdminUser::where($where)->first();
    }

    public function selectAdminUserDetail($page)
    {
        return $this::where('is_del', '<>', 1)
                ->paginate($page);
    }
    public function selectAdminNameIsExist($name)
    {
        return $this::where('admin_username', $name)
                    ->where('is_del', '<>', 1)
                    ->first();
    }

    public function selectAdminDetailByLike($where, $page)
    {
        return $this::where($where)
                    ->where('is_del', '<>', 1)
                    ->paginate($page);
    }
    public function selectMaxNumber()
    {
        return $this::max('id');
    }
    public function addAdminDetail($data)
    {
        $name['admin_username'] = $data['admin_username'];
        return $this::updateOrCreate($name,$data);
    }
    public function selectAdminDetailById($id)
    {
        return $this::find($id);
    }
    public function updateAdminInfoById($where, $id)
    {
        return $this::where('id', $id)
                ->update($where);
    }
    public function softDeleteAdminById($id)
    {
        return $this::where('id', $id)
                ->update([
                    'is_del' => 1
                ]);
    }
    public function updateAdminPasswordById($data, $id)
    {
        return $this::where('id', $id)
                ->update($data);
    }
    public function getAdminById($admin_id)
    {
//        return
    }
    
    protected function saveData($id, $data)
    {
        return $this::where('id', $id)->update($data);
    }
    
    /**
     * 根据角色获取用户列表
     * @param $role_id
     * @return array
     */
    protected function getListByRole($role_id)
    {
        return $this::select('admin_user.*')->join('admin_role_info', 'admin_role_info.aid', 'admin_user.id')->where('rid', $role_id)->get();
    }
    protected function getListByRoleInfo($role_id)
    {
        return $this::select('admin_user.*')
                    ->join('admin_role_info', 'admin_role_info.aid', 'admin_user.id')
                     ->where('admin_user.is_del', 0)
                     ->where('admin_user.admin_status', 0)
                     ->where('admin_role_info.status', 0)
                     ->where('admin_role_info.rid', $role_id)
                    ->get();
    }
    
    
    protected function selectOprator($role_id)
    {
//        admin_user,admin_role
        $admin = DB::table('admin_user as user')
            ->join('admin_role_info as role','user.id','role.aid')
            ->where('user.is_del', 0)
            ->where('user.admin_status', 0)
            ->where('role.status', 0)
            ->where('role.rid', $role_id)
            ->select('user.*')
            ->get();
//        dd($admin);
        return $admin;
    }
    
    protected function getName($param)
    {
        return static::where($param)->first(['admin_username'])->admin_username;
    }
    protected function getOperationName()
    {
        return static::get(['admin_username']);
    }
    protected function getNoHeared($id)
    {
        return static::where('id', $id)->first(['no_heared'])->no_heared;
    }
}
