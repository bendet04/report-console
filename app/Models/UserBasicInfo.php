<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;
use Illuminate\Support\Facades\DB;


class UserBasicInfo extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $table = 'user_basic_info';
    protected  $fillable = ['ktp_verity'];
    
    protected function getName($ktp)
    {
       return self::where(['ktp_number'=>$ktp])->pluck('name');
    }
    protected function getInfo($ktp)
    {
//        dd($ktp);
       $data=DB::table('user_basic_info')
            ->join('user_access','uid', '=', 'user_access.user_id')
            ->select('ktp_number','name','uuid','platform')
            ->whereRaw("ktp_number in(".$ktp.")")
            ->get();
        return $data;
    }
    protected function getUid($ktp)
    {
        return self::where(['ktp_number' => $ktp])->pluck('uid')->toArray();
    }
    protected function getOne($param)
    {
        return $this::where($param)->orderByDesc('uid')->first();
    }
    protected function saveOne($where, $param)
    {
        return $this::where($where)->update($param);
    }

}
