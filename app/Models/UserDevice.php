<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class UserDevice extends Model
{
    protected $table = 'user_device';
    
    protected function getOne($id, $date)
    {
        $timePoint = env('TIMEPOINT', '2018-06-23 00:00:00');
        if ($date > $timePoint){
            $device = static::find($id);
        }else{
            $old = DB::connection('mysql_old');
            $device = $old->selectOne('select * from tb_user_device where id=?', [$id]);
        }
        
        return $device;
    }
}
