<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected  $table = 'bank_info';
    protected  $fillable = ['parameter_code_name', 'parameter_code_type', 'parameter_code', 'paramer_url', 'status'];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    public function getAllBankInfo($page)
    {
        return $this::paginate($page);
    }
    public function getBankById($id)
    {
        return $this::find($id);
    }
    public function updateBankInfoById($where, $id)
    {
        return $this::where('id', $id)
                ->update($where);
    }
    public function getBankInfoByLike($where, $page)
    {
        return $this::where($where)
                ->paginate($page);
    }
    public function addBankInfo($data)
    {
        return $this::create($data);
    }
    
    protected function getAll($bankType)
    {
        return static::where(['parameter_code_type' => 1, 'status' => 2, 'bank_type'=>$bankType])->whereNotIn('parameter_code', ['BCA'])->get();
//        return static::where(['parameter_code_type' => 1, 'status' => 2])->get();
    }
    protected function getOne($param)
    {
        return $this::where($param)
            ->first();
    }
    protected function getBankName($bank_id)
    {
        return $this::where(['parameter_code'=>$bank_id,'parameter_code_type'=>0,'status'=>2])
            ->first()->parameter_code_name;
    }
}
