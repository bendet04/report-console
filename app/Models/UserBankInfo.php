<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBankInfo extends Model
{
    protected $table = 'user_bank_info';
    protected $fillable = ['oid', 'type', 'payee_name', 'transaction_id', 'bank_name', 'bank_id', 'bank_phone', 'bank_number', 'bank_card_url', 'uid', 'bank_verify_status', 'expire_period', 'confirm_time'];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    
    protected function saveOne($paramArr)
    {
        return UserBankInfo::create($paramArr);
    }
}
