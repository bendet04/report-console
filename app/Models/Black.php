<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Black extends Model
{
    protected $table = 'black';
    protected $fillable = ['order_id', 'ktp_number', 'uuid', 'username'];
    
    protected function saveData($wheres, $data)
    {
        return static::updateOrCreate($wheres, $data);
    }
}
