<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerAccountInfo extends Model
{
    protected $table = 'partner_account_info';
    
    protected function getOne($partner, $type)
    {
        return PartnerAccountInfo::where([
            'partner' => $partner,
            'type' => $type,
            'status' => 2,
        ])->first();
    }
    
    protected function getFirst($where)
    {
        return static::where($where)->orderByDesc('id')->first();
    }
    
    protected function getListByUid($uid)
    {
        return static::where('user_id', $uid)->get();
    }
}
