<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanTrialInfo extends Model
{
    protected $table = 'loan_trial_info';
    protected $fillable = ['order_id', 'admin_id', 'status', 'trial_time'];


    protected function selectTrailByAdminId($admin_id)
    {
//      return LoanTrialInfo::where('admin_id', $admin_id)->get();
      return $this::select('loan_trial_info.order_id','loan_order.order_status')
                    ->join('admin_user', 'loan_trial_info.admin_id', 'admin_user.id')
                    ->join('loan_order', 'loan_trial_info.order_id', 'loan_order.id')
                    ->where('loan_trial_info.admin_id', $admin_id)
                    ->where('loan_order.order_status', 17)

                    ->get();
    }
    protected function updateByOrderId($order_id, $paramArr)
    {
        return LoanTrialInfo::where('order_id', $order_id)->update($paramArr);
    }
    
    protected function saveOne($paramArr)
    {
        return static::updateOrCreate($paramArr);
    }
    
    protected function saveData($order_id, $paramArr)
    {
        return static::updateOrCreate(['order_id'=>$order_id], $paramArr);
    }
}
