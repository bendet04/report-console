<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorkInfo extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    
    protected $table = 'user_work_info';
    protected $fillable = [
        'uid',
        'career',
        'work_image_url',
        'income_level',
        'work_period',
        'paid_period',//发薪时间
        'company_type',
        'company_name',
        'company_phone',
        'company_address',
        'company_province',
        'company_rengence',
        'company_district',
        'company_villages',
        'company_phone_call_times',
        'work_status',
    ];
    
    protected function getByUid($uid)
    {
        return static::where('uid', $uid)->firstOrFail();
    }
    
    protected function saveData($paramArr)
    {
        return static::create($paramArr);
    }
    
    protected function updateLevel($uid, $paramArr)
    {
        return static::where(['uid'=>$uid])->update($paramArr);
    }
}
