<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderUserBasicInfo extends Model
{
    protected $table = 'order_user_basic_info';

    
    protected function getOne($id)
    {
        return OrderUserBasicInfo::findOrFail($id);
    }
    
    protected function getByWhere($paramArr)
    {
        return static::where($paramArr)->orderByDesc('created_at')->first();
    }
    
    protected function updateOne($id, $paramArr)
    {
        return OrderUserBasicInfo::where('id', $id)->update($paramArr);
    }
    protected function getList($where)
    {
        return static::where($where)->get();
    }

}
