<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = 'order_status';
    protected $fillable = ['order_id', 'admin_id', 'status', 'status_time'];
    
    protected function saveOne($paramArr)
    {
        return OrderStatus::create($paramArr);
    }
    
    protected function updateOne($paramArr, $order_id)
    {
        return OrderStatus::where('id', $order_id)->update($paramArr);
    }
    protected function getByOrderId($order_id)
    {
        return OrderStatus::where('order_id', $order_id)->orderByDesc('created_at')->first();
    }
    protected function OverDueStatus($order_id)
    {
        return OrderStatus::where('order_id', $order_id)->pluck('status');
    }
    
}
