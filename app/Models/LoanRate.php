<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanRate extends Model
{
    protected  $table = 'loan_rate';

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    public function getAllRateInfo()
    {
        return $this::get();
    }
    public function getRateInfoById($id)
    {
        return $this::find($id);
    }
    public function updateRateById($data, $id)
    {
        return $this::where('id', $id)
                ->update($data);
    }
    public function getRateByType($type)
    {
        return $this::where('type', $type)
                ->first();
    }
    protected function getRate($type)
    {
        return $this::where('type', $type)
            ->first();
    }
}
