<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminAssign extends Model
{
    protected $table = 'admin_assign';
    
    protected $fillable = ['pos', 'role_id'];
}
