<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushToken extends Model
{
    protected $table = "push_token";
    
    
    protected function getTokenByUid($uid)
    {
        $token = PushToken::where('user_id', $uid)->first();
        if (empty($token)){
            return 'empty';
        }

        return $token['user_token'];
    }

}