<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageHistory extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    
    protected $table = 'message_history';
    
    protected $fillable = ['title', 'description', 'uid', 'message_status', 'message_type'];
    
    protected function saveOne($paramArr)
    {
        return MessageHistory::create($paramArr);
    }
}
