<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    protected  $table = 'role_access';
    protected  $fillable = ['aid', 'rid'];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    public function getAccessDetailByRole($roleId)
    {
        return $this::where('rid', $roleId)
                    ->where('status', '<>', 1)
                    ->get();
    }
    public function addRoleAccessInfo($data)
    {
        return $this::create($data);
    }
    public function cancelRoleAccessInfo($data)
    {
        return $this::where($data)
                ->update([
                    'status' => 1
                ]);
    }
    
    protected function getList($ruleIds)
    {
        return static::whereIn('rid', $ruleIds)->get();
    }
}
