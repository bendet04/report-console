<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $table = 'investment';
    protected $primaryKey = 'productId';
    protected $fillable = ['productId', 'period', 'rate'];
    
    protected function getList($pageNum=20)
    {
        return static::paginate($pageNum);
    }
    
    protected function updateOne($where,$pid)
    {
        return static::where('productId', $pid)->update($where);
    }
    
    protected function getOne($period)
    {
        return static::where('period', $period)->first();
    }
}
