<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPartnerAccount extends Model
{
    protected $table = 'user_partner_account';
    
    protected function getOne($uid)
    {
        return UserPartnerAccount::where('user_id', $uid)->first();
    }
}
