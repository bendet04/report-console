<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $table = 'product';
    protected $primaryKey = 'pid';

    public $timestamps = false;

    public function getAllProductInfo()
    {
        return $this::get();
    }
    public function getProductInfoByPid($id)
    {
        return $this::find($id);
    }
    protected function getProduct($id)
    {
        return $this::find($id);
    }
    public function updateProductByPid($where, $pid)
    {
        return $this::where('pid', $pid)
                    ->update($where);
    }
}
