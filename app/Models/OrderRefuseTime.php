<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRefuseTime extends Model
{
    protected $table = 'order_refuse_time';
    protected $fillable = ['refuse_time', 'ktp_number'];

    protected function saveOne($ktp_number, $param)
    {
        return $this::updateOrCreate($ktp_number,$param);
    }
    public function getOrderByKtpNumber($ktp_number)
    {
        return $this::where(['ktp_number' => $ktp_number])->first();
    }
}
