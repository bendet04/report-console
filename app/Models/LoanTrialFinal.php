<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanTrialFinal extends Model
{
    protected $table = 'loan_trial_final';
    protected $fillable = ['admin_id', 'order_id', 'status', 'remark', 'trial_time'];

    protected function selectTrailByAdminId($admin_id)
    {
//      return LoanTrialInfo::where('admin_id', $admin_id)->get();
        return $this::select('loan_trial_final.order_id','loan_order.order_status')
            ->join('admin_user', 'loan_trial_final.admin_id', 'admin_user.id')
            ->join('loan_order', 'loan_trial_final.order_id', 'loan_order.id')
            ->where('loan_trial_final.admin_id', $admin_id)
            ->where('loan_order.order_status',21)
            ->get();
    }
    protected function getOne($order_id)
    {
        return static::where('order_id', $order_id)->first();
    }
    
    protected function saveOne($order_id, $paramArr)
    {
        return LoanTrialFinal::updateOrCreate(['order_id'=>$order_id], $paramArr);
    }
    
    protected function saveItem($paramArr)
    {
        return static::updateOrCreate($paramArr);
    }
}
