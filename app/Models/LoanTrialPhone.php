<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanTrialPhone extends Model
{
    protected $table = 'loan_trial_phone';
    protected $fillable = ['order_id', 'admin_id', 'status', 'trial_time'];
    protected function selectTrailByAdminId($admin_id)
    {
//      return LoanTrialInfo::where('admin_id', $admin_id)->get();
        return $this::select('loan_trial_phone.order_id','loan_order.order_status')
            ->join('admin_user', 'loan_trial_phone.admin_id', 'admin_user.id')
            ->join('loan_order', 'loan_trial_phone.order_id', 'loan_order.id')
            ->where('loan_trial_phone.admin_id', $admin_id)
            ->where('loan_order.order_status',19)
            ->get();
    }
    protected function saveOne($paramArr)
    {
        return static::updateOrCreate($paramArr);
    }
    
    protected function getTrialByOrderId($order_id)
    {
        return static::where('order_id', $order_id)->first();
    }
    
    protected function saveData($order_id, $paramArr)
    {

        return static::updateOrCreate(['order_id'=>$order_id], $paramArr);


    }
}
