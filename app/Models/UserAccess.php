<?php

namespace App\Models;

use bar\baz\source_with_namespace;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserAccess extends Model
{
    protected $table = 'user_access';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    public $timestamps = false;
    //获取信审,电审，终审，通过率及逾期率坏账率
    public function Statistical($paramArr=[],$pageNum = 20)
    {
        $where="  channel!='' ";
        $flag='0';
//        区分统计数据的分组
        $add_group="channel,platform";
        
        if (isset($paramArr) && count($paramArr) != 0) {

            if (!empty($paramArr['channel'])&&!is_numeric($paramArr['channel'])&&count($paramArr)==1) {
                $where.=" AND channel='".$paramArr['channel']."' AND to_days(create_time)=to_days(now())";
                $flag='0';
            }
            if (!empty($paramArr['channel'])&&!is_numeric($paramArr['channel'])) {
                $where.=" AND channel='".$paramArr['channel']."'";
                $flag='0';
            }
            if (!empty($paramArr['source'])) {
                $where.=" AND tb_user_access.platform='".$paramArr['source']."'";
                $flag='source';
            }


            if (!empty($paramArr['start'])&&empty($paramArr['end'])) {
                $where.=" AND create_time>='".$paramArr['start']."'";
                $flag='start';
            }
            if (!empty($paramArr['end'])&&empty($paramArr['start'])) {
                $where.=" AND create_time<='".$paramArr['end']."'";
                $flag='end';
            }
            if (!empty($paramArr['start'])&&!empty($paramArr['end'])) {
                $where.=" AND create_time between '".$paramArr['start']."' AND '".$paramArr['end']."'";
                $flag='between';
            }
            if (!empty($paramArr['month'])) {
                $where.=" AND DATE_FORMAT(create_time,'%Y-%m' ) = DATE_FORMAT(now(),'%Y-%m')";
                $flag='month';
            }
            if (!empty($paramArr['week'])) {
                $where.=" AND YEARWEEK(date_format(create_time,'%Y-%m-%d')) = YEARWEEK(now())";
                $flag='week';
            }
        } else {
            $where.=" AND to_days(create_time)=to_days(now()) ";
            $flag='0';
        }

         //获取时间显示格式
        $time=$this->downTime($paramArr);
        //获取分组方式
        $group=$this->getGroup($flag, $paramArr, 'create_time', $add_group);
        //查询用户注册数及相关通过率

        $users = DB::table('user_access')
            ->join('channel_list', 'channel', '=', 'channel_list.channel_name')
            ->select(
                'platform',
                'channel_list.channel_name as channel',
                'channel_list.chan_name',
                'user_id',
                DB::raw('COUNT(*) reg_num')
            )
            ->whereRaw($where)
            ->groupBy(DB::raw($group['group']));
        
        if ($pageNum==0) {
            $data=$users->get();
        } else {
            $data=$users->paginate($pageNum);
        }
        $data->map(function ($item, $k) use ($where, $time) {
            //获取用户提交申请数
            $item->sub_apply=DB::table('loan_order')
                              ->join('user_access', 'uid', '=', 'user_access.user_id')
                              ->select(DB::raw('COUNT(*) sub_apply'))
                              ->whereRaw("tb_user_access.channel='".$item->channel."'")
                              ->whereRaw("tb_loan_order.platform='".$item->platform."'")
                              ->whereRaw($where)
                              ->first()
                              ->sub_apply;
            $item->only=$time;
            if (!$item->sub_apply) {
                $item->sub_apply = '--';
            }
            //获取该渠道下用户的order_id
            $order_id=$this->getOrderId($item->channel, $where, $item->platform);

            if ($order_id) {
                 //信审通过率
                $item->info_pass = intval(DB::table('loan_trial_info')
                                      ->select(DB::raw("COUNT(CASE WHEN STATUS='19' AND order_id IN($order_id) THEN id END)/{$item->sub_apply}*100 AS info_pass"))
                                      ->first()->info_pass).'%';
                //电审通过率
                $item->phone_pass=intval(DB::table('loan_trial_phone')
                                   ->select(DB::raw("COUNT(CASE WHEN STATUS='21' AND order_id IN($order_id) THEN id END)/{$item->sub_apply}*100 AS phone_pass"))
                                   ->first()->phone_pass).'%';
                //终审通过率
                $item->final_pass=intval(DB::table('loan_trial_final')
                        ->select(DB::raw("COUNT(CASE WHEN STATUS='1' AND order_id IN($order_id) THEN id END)/{$item->sub_apply}*100 AS final_pass"))
                        ->first()->final_pass).'%';
                 //逾期率与坏账率
                 $track=DB::table('loan_order')
                           ->select(
                           DB::raw("COUNT(CASE WHEN order_status='11' AND id IN($order_id) THEN id END)/COUNT(CASE WHEN order_status in('11','12','13','25') AND id IN($order_id) THEN id END)*100 AS overdute"),
                           DB::raw("COUNT(CASE WHEN order_status='11' AND id IN($order_id) AND NOW() > DATE_ADD(loan_deadline, INTERVAL 15 DAY) THEN id END)/COUNT(CASE WHEN order_status in('11','12','13','25') AND id IN($order_id) THEN id END)*100 AS bad_debt" ))
                           ->first();
                 $item->overdue=intval($track->overdute).'%';
                 $item->bad_debt=intval($track->bad_debt).'%';
            } else {
                //信审通过率
                $item->info_pass = '--';
                //电审通过率
                $item->phone_pass='--';
                //终审通过率
                $item->final_pass='--';
                //逾期率
                $item->overdue='--';
                //坏账率
                $item->bad_debt='--';

            }
            //提交申请数
            unset($item->user_id);
            return $item;
        });
        return $data;
    }
    //获取分组条件及搜索显示日期
    public function getGroup($flag, $paramArr, $time, $group)
    {
        switch ($flag) {
            case "week":
                $data['group']="week($time),$group";
                return $data;
                break;
            case "start":
                $data['group']="$time >'".$paramArr['start']."',$group";
                return $data;
                break;
            case "end":
                $data['group']="$time <'".$paramArr['end']."',$group";
                return $data;
                break;
            case "month":
                $data['group']="month($time),$group";
                return $data;
                break;
            case "between":
                $data['group']="$time between '".$paramArr['start']."' and '".$paramArr['end']."',$group";
                return $data;
                break;

            default:
                $data['group']="YEAR($time),MONTH($time),DAY($time),$group";
                return $data;
                break;
        }
    }
    //获取订单ID
    public function getOrderId($channel, $where, $plant)
    {
        $order_id= DB::table('loan_order')
            ->join('user_access', 'uid', '=', 'user_access.user_id')
            ->whereRaw("tb_user_access.channel='".$channel."'")
            ->whereRaw("tb_loan_order.platform='".$plant."'")
            ->whereRaw($where)
            ->pluck('loan_order.id')
            ->toArray();

        $str=",";
        
        if (!empty($order_id)) {
            foreach ($order_id as $v) {
                $str.=$v.',';
            }
            return trim($str, ",");
        } else {
            return false;
        }
    }
    public function DownTime($param)
    {
        if (!empty($param['month'])) {
            $time=date('Y/m', time());
        } elseif (!empty($param['week'])) {
            $now = time();
            $time = '1' == date('w') ? strtotime('Monday', $now) : strtotime('last Monday', $now);
            $start = date('Y-m-d', $time);
            $end = date('Y-m-d', strtotime('Sunday', $now));
            $time = date('Y/m/d', strtotime($start)).'~'.date('Y/m/d', strtotime($end));
        } elseif (!empty($param['start']) && empty($param['end'])) {
            $time=date('Y/m/d', strtotime($param['start'])).'之后';
        } elseif (!empty($param['end']) && empty($param['start'])) {
            $time=date('Y/m/d', strtotime($param['end'])).'之前';
        } elseif (isset($param['start'], $param['end']) && date('Y-m-d', strtotime($param['start'])) == date('Y-m-d', strtotime($param['end']))) {
            $time=date('Y/m/d', strtotime($param['start']));
        } elseif (!empty($param['start'])&&!empty($param['end'])) {
            if (strtotime($param['start'])>strtotime($param['end'])) {
                $center=$param['start'];
                $param['start']=$param['end'];
                $param['end']=$center;
            }
            $time=date('Y/m/d', strtotime($param['start'])).'~'.date('Y/m/d', strtotime($param['end']));
        } else {
            $time=date('Y/m/d', time());
        }
        return $time;
    }
    
    protected function getPlatform($uid)
    {
        return static::where(['user_id'=>$uid])->first(['platform'])->platform;
    }

    public static function getByUserId($user_id)
    {
        return static::where(['user_id' => $user_id])->first();
    }
}
