<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanPaymentFlow extends Model
{
    protected $table = 'loan_payment_flow';
    
    protected $fillable = ['order_id', 'payment_amount', 'payment_flow', 'payment_status', 'payment_code', 'payment_time'];
    
    protected function getList($order_id)
    {
        return LoanPaymentFlow::where('order_id', $order_id)->orderByDesc('id')->get();
    }
    
    protected function saveOne($payment_flow, $paramArr)
    {
        return LoanPaymentFlow::updateOrCreate(['payment_flow'=>$payment_flow], $paramArr);
    }
    protected function getRecentOrder($order_id)
    {
        return LoanPaymentFlow::where('order_id', $order_id)->orderByDesc('id')->first();
    }
}
