<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRoleInfo extends Model
{
    protected $table = 'admin_role_info';
    protected  $fillable = ['aid', 'rid','status'];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    public function adminAssignRole($data)
    {
//        $updateData = $data;
        $updateData['status'] = 0;
//        dd($data,$updateData);
        return $this::updateOrCreate($data,$updateData);
    }
    public function getRoleDetailByAdminId($where)
    {
        return $this::where($where)
                ->where('status', '<>', 1)
                ->get();
    }
    
    public function getRoleDetailByAdminRole($where)
    {
        return $this::where($where)
            ->whereIn('rid', [1, 19])
            ->where('status', '<>', 1)
            ->get();
    }
    
    public function cancelAssignRole($data)
    {
        return $this::where($data)
                    ->update([
                        'status' => 1
                    ]);
    }
    //获取信审，电审，终审人员总数
    public function getRoleNumber($role)
    {
        return $this::where(['rid'=>$role])->count();
    }
    public function getRoleDetail($where)
    {
        return $this::where($where)
            ->where('status', '<>', 1)
            ->pluck('rid');
    }
}
