<?php

namespace App\Models;

use App\Consts\Platform;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class LoanOrder extends Model
{
    protected $table = 'loan_order';
    
    public function order()
    {
        return $this->hasOne('App\Models\LoanOrder', 'uid', 'uid');
    }
    
    /**
     * 信审审核人员只能看到自己的订单
     * @param $query
     * @return mixed
     */
    public function scopeTrialInfo($query)
    {
        $query->join('loan_trial_info', 'loan_trial_info.order_id', '=', 'loan_order.id', 'left');
        if($this->isSuperAdmin() === 0 && Config::get('currentMenu')!=='task') {
            return $query->where('admin_id', $this->admin_id);
        }
    }
    
    /**
     * 电审审核人员只能看到自己的订单
     * @param $query
     * @return mixed
     */
    public function scopeTrialPhone($query)
    {
        $query->join('loan_trial_phone', 'loan_trial_phone.order_id', '=', 'loan_order.id', 'left');
        $query->join('loan_trial_phone_record', 'loan_trial_phone_record.order_id', '=', 'loan_order.id', 'left');
        if ($this->isSuperAdmin() === 0 && Config::get('currentMenu')!=='task') {
            return $query->where('loan_trial_phone.admin_id', $this->admin_id);
        }
    }
    
    /**
     * 电审审核人员只能看到自己的订单
     * @param $query
     * @return mixed
     */
    public function scopeTrialPhoneQueue($query)
    {
        $query->join('loan_trial_phone', 'loan_trial_phone.order_id', '=', 'loan_order.id', 'left');
        $query->join('loan_trial_phone_record', 'loan_trial_phone_record.order_id', '=', 'loan_order.id', 'left');
        if ($this->isSuperAdmin() === 0 && Config::get('currentMenu') !== 'task') {
            return $query->where('loan_trial_phone.admin_id', $this->admin_id)
                    ->where(function ($query) {
                        $query->orWhere(function ($query) {
                            $query->whereNotIn('loan_trial_phone_record.work_phone_status', [2]);
                            $query->whereNotIn('loan_trial_phone_record.phone_status', [2]);
                        });
                        $query->orWhere(function ($query) {
                            $query->whereNull('loan_trial_phone_record.phone_status');
                        });
                        $query->orWhere(function ($query) {
                            $query->whereNull('loan_trial_phone_record.work_phone_status');
                        });
                    });
        }
    }
    /**
     * 终审审核人员只能看到自己的订单
     * @param $query
     * @return mixed
     */
    public function scopeTrialFinal($query)
    {
        $query->join('loan_trial_final', 'loan_trial_final.order_id', '=', 'loan_order.id', 'left');
        if($this->isSuperAdmin() === 0 && Config::get('currentMenu')!=='task') {
            return $query->where('admin_id', $this->admin_id);
        }
    }
    
    /**
     * 催收审核人员只能看到自己的订单
     * @param $query
     * @return mixed
     */
    public function scopeTrialTrack($query)
    {
        if($this->isSuperAdmin() === 0 && Config::get('currentMenu')!=='task') {
            return $query->where('admin_id', $this->admin_id);
        }
    }
    
    /**
     * 判断当前操作人员是不是超级管理员
     * @return bool
     */
    protected function isSuperAdmin()
    {
        $this->admin_id = \Auth::id();
    
        //判断是不是超级管理员
        $roleModel = new AdminRoleInfo();
        $adminRole = $roleModel->getRoleDetailByAdminRole(['aid'=>$this->admin_id]);
        
        return $adminRole->count();
    }
    
    /**
     * 订单/信审表关联
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function info()
    {
        return $this->hasOne('App\Models\LoanTrialInfo', 'order_id');
    }
    
    protected function getOne($order_id)
    {
        return LoanOrder::findOrFail($order_id);
    }
    
    protected function getList($where)
    {
        return static::where($where)->get();
    }
    protected function getListByTime($platform, $basicId)
    {
        $ktp=OrderUserBasicInfo::getOne($basicId);
        $query = static::select(['loan_order.*']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where('loan_order.platform', '=', $platform);
        $query->where('order_user_basic_info.ktp_number', '=', $ktp['ktp_number']);
        $orders = $query->orderByDesc('loan_order.order_time')->get();
        return $orders;
    }

    protected function getByBasicId($basic_id)
    {
        return static::where('basic_id', $basic_id)->orderByDesc('order_time')->first();
    }
    protected function getByUserIdAll($user_id)
    {
        return static::where('uid', $user_id)->orderByDesc('order_time')->skip(1)->get();
    }
    protected function getByUserId($user_id)
    {
        return static::where('uid', $user_id)->orderByDesc('order_time')->skip(1)->first();
    }

    protected function getTransation($where, $orderBy = 'loan_order.id')
    {
        $query = LoanOrder::select(['loan_payment_flow.payment_flow','loan_payment_flow.payment_code','loan_payment_flow.created_at','loan_order.id','loan_order.order_status', 'loan_order.pid','loan_payment_flow.id as paymentId']);
        $query->join('loan_payment_flow', 'loan_payment_flow.order_id', '=', 'loan_order.id');
        $orders = $query->where($where)->where('loan_order.order_time', '<', '2018-06-19 00:00:00')->orderByDesc($orderBy)->get();
        return $orders;
    }
    
    protected function updateById($order_id, $paramArr)
    {
        return LoanOrder::where('id', $order_id)->update($paramArr);
    }
    protected function getChannel($order_id)
    {

        $channel = DB::table('user_access')
            ->join('channel_list', 'channel', '=', 'channel_list.channel_name')
            ->join('loan_order','user_id','=','loan_order.uid')
            ->select('channel_list.chan_name as channel')
            ->whereRaw("tb_loan_order.id=".$order_id."")->get();


        return $channel;
    }
    protected function getLastOrder($order_id, $uid)
    {
        return $this::where([
                    ['id', '<', $order_id],
                    ['order_status', '=', 12],
                    ['uid', '=', $uid]
                ])
                ->orderByDesc('id')
                ->first();
    }
    
    protected function getOtherPlatformOrder($ktp, $platform)
    {
        $query = static::select(['loan_order.*']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where('loan_order.platform', '!=', $platform);
        $query->where('order_user_basic_info.ktp_number', '=', $ktp);
        $orders = $query->orderBy('loan_order.platform')->orderByDesc('loan_order.id')->get();
//        $orders = $query->orderBy('loan_order.platform')->orderByDesc('loan_order.id')->toSql();

        return $orders;
    }
    
    protected function getUserType($platform, $basicId)
    {
        $ktp=OrderUserBasicInfo::getOne($basicId);
        $query = static::select(['loan_order.*']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where('loan_order.platform', '=', $platform);
        $query->where('loan_order.order_status', '=', 12);
        $query->where('order_user_basic_info.ktp_number', '=', $ktp['ktp_number']);
        $orders = $query->get();
        return $orders;
    }
}
