<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static payment($transactionId, $payeeBankName, $payeeName, $payeeAccount, $payeeMsisdn, $amount)
 */
class BankPayment extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\BankPayment';
    }
}
