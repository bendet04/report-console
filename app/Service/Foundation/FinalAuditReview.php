<?php

namespace App\Service\Foundation;

use App\Events\InfoAfter;
use App\Models\AdminCheckUser;
use App\Models\LoanOrder;
use App\Models\LoanTrialInfo;
use App\Models\AdminRoleInfo;
use App\Models\LoanTrialInfoRecord;
use App\Service\Assign;
use App\Service\Contracts\Order\Trial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class FinalAuditReview
{

    /**
     * 获取信审人员昨天信息
     * @param array $paramArr
     * @param string $orderBy
     * @param int $pageNum
     * @return array
     */

    public function detail($role,$admin_id)
    {
        $trialStatus=[0,1,2];
        $orderStatus=[5,6,7,8,9,10,11,12,25,14,5,23,14];
        $where="TO_DAYS(now())-TO_DAYS(tb_loan_trial_final.trial_time) <= 1";
        $Mywhere="TO_DAYS(now())-TO_DAYS(tb_loan_trial_final.trial_time) <= 1  and tb_loan_trial_final.admin_id=$admin_id";

        $roleCount=new AdminRoleInfo();
        $num=$roleCount->getRoleNumber($role);
        $result=[];
        $data=[];
        //自己审核数量
        $data=$this->sum($trialStatus,$orderStatus,$Mywhere);
        //总审核数量
        $sum=$this->sum($trialStatus,$orderStatus,$where);
        //平均审核数量
        if($sum[0]->sum>0){
            $data[0]->sumAvg=$sum[0]->sum/$num;
        }else{
            $data[0]->sumAvg=0;
        }
        //通过
        $passSum=$this->sum($trialStatus = [1], $orderStatus = [9,10,11,12,25,14],$where);
        if($passSum[0]->sum>0){
            $data[0]->passAvg=$passSum[0]->sum/$num;
        }else{
            $data[0]->passAvg=0;
        }
        $data[0]->myPass=$this->sum($trialStatus = [1], $orderStatus = [9,10,11,12,25,14],$Mywhere)[0]->sum;
        $data[0]->type=3;
        $data[0]->time=date("Y-m-d",strtotime("-1 day"));
        return $result['final']=$data;
    }
    public function sum($trialStatus,$orderStatus,$where)
    {
        $query = LoanOrder::select([
            DB::raw('count(*) as sum')
        ]);
        $query->join('loan_trial_final', 'loan_order.id', '=', 'loan_trial_final.order_id');
        $orders = $query
            ->whereIn('loan_trial_final.status', $trialStatus)
            ->whereIn('loan_order.order_status', $orderStatus)
            ->whereRaw($where)
            ->get();
        return $orders;

    }
}