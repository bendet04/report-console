<?php

namespace App\Service\Foundation;

use App\Models\AdminAccess;
use App\Models\AdminRoleInfo;
use App\Models\AdminUser;
use App\Models\AdminUserLog;
use App\Models\PartnerAccountInfo;
use App\Models\RoleAccess;
use Illuminate\Support\Facades\Auth;

class UserService
{
    /**
     * 用户登录
     * @param array $paramArr 用户名、密码
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function attemptLogin($paramArr = [])
    {
        if (!empty($paramArr['username'])) {
            $where['admin_username'] = $paramArr['username'];
        }
        
        $user = AdminUser::getOne($where);
        
        $login_type = 2;
        $result = false;
        
        if ($user) {
            $pwd = $this->encrypt($paramArr['pwd']);
            
            //验证密码是否正确
            if ($user->admin_pwd==$pwd) {
                $login_type = 1;
                $result =  $user;
            } else {
                $result = false;
            }
        }
        
        AdminUserLog::saveOne([
            'username' => $paramArr['username'],
            'ip'       => $paramArr['ip'],
            'login_result' => $login_type,
        ]);
        
        return $result;
    }
    
    /**
     * 加密
     * @param $pwd
     * @return string
     */
    public function encrypt($pwd)
    {
        return md5($pwd);
    }
    
    /**
     * 获取用户权限列表
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getUserAccess()
    {
        $user_id = Auth::id();
        
        //获取用户所有角色
        $adminModel = new AdminRoleInfo();
        $ruleList = $adminModel->getRoleDetailByAdminId(['aid'=>$user_id]);
        $ruleArr = $ruleList->pluck('rid');
        
        //超级管理员，拥有所有权限
        if ($ruleArr->contains(1)) {
            return '*';
        }
        
        //获取角色对应的权限
        $accessList = RoleAccess::getList($ruleArr);
        $accessArr = $accessList->pluck('aid');
        
        //获取权限列表
        $accessList = AdminAccess::getList($accessArr);
        return $accessList;
    }
    
    /**
     * 获取用户拥有的所有菜单
     * @param $access
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getUserMenu()
    {
        $access = $this->getUserAccess();
        
        if ($access==='*') {
            return AdminAccess::getAll();
        }
        
        $parentArr = $access->pluck('access_leval');
        $parentList = AdminAccess::getList($parentArr);
        
        return $access->merge($parentList);
    }
    
    /**
     * 获取第三方授权信息
     * @param $uid
     * @param $partner_name
     * @return mixed|null
     */
    public function partnerInfo($uid, $partner_name)
    {
        $partnerArr = [
            'lazada' => 1,
            'tokopedia'=>2,
            'facebook' => 3,
            'instagram' => 4,
            'gojek' => 5,
            'telkomsel' => 6,
            'npwp' => 7,
            'bpjs' => 8,
        ];
        
        $condition = [
            ['user_id', '=', $uid],
            ['type', '=', $partnerArr[$partner_name]],
            ['status', '=', 2],
        ];
        $partner = PartnerAccountInfo::getFirst($condition);
        if (!empty($partner) && !empty($partner['info'])){
            return json_decode($partner['info'], true);
        }
        
        return null;
    }
}