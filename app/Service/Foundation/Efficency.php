<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/25/025
 * Time: 13:27
 */

namespace App\Service\Foundation;

use App\Consts\LoanDay;
use App\Consts\OrderStatus;
use App\Models\AdminUser;
use App\Models\UserAccess;
use App\Models\LoanOrder;
use Carbon\Carbon;
use Excel;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Collection;

class Efficency
{
    public function voiceDate($from, $to, $orderBy = 'loan_order.loan_deadline')
    {
        $status = [9, 10, 11, 14];

        $from = \App\Service\DateUnit::formatTime($from);
        $to = \App\Service\DateUnit::formatTime($to);

        $query = LoanOrder::select(['name', 'order_number', 'loan_order.loan_period', 'phone_number', 'payment_date', 'loan_deadline', 'loan_suspend_date', 'loan_overdue_date', 'order_status']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');

        $orders = $query->whereBetween('loan_deadline', [$from, $to])->whereIn('order_status', $status)->orderByDesc($orderBy)->get();

        $orders = $this->format($orders);

        return $orders;

    }
    //信审信息
    public function message($paramArr=[],$pageNum = 20)
    {
        $where=" tb_t1.status=19 ";
        $flag='0';
        $add_group="tb_t1.admin_id";
        if(isset($paramArr)&&count($paramArr)!=0)
        {

            if(!empty($paramArr['start'])&&empty($paramArr['end']))
            {
                $where.=" AND tb_t1.updated_at>='".$paramArr['start']."'";
                $flag='start';
            }
            if(!empty($paramArr['end'])&&empty($paramArr['start']))
            {
                $where.=" AND tb_t1.updated_at<='".$paramArr['end']."'";
                $flag='end';
            }
            if(!empty($paramArr['start'])&&!empty($paramArr['end']))
            {
                $where.=" AND tb_t1.updated_at between '".$paramArr['start']."' AND '".$paramArr['end']."'";
                $flag='between';
            }
            if(!empty($paramArr['month']))
            {
                $where.=" AND DATE_FORMAT(tb_t1.updated_at,'%Y-%m' ) = DATE_FORMAT(now(),'%Y-%m')";
                $flag='month';
            }
            if(!empty($paramArr['week']))
            {
                $where.=" AND YEARWEEK(date_format(tb_t1.updated_at,'%Y-%m-%d')) = YEARWEEK(now())";
                $flag='week';
            }
        }
        else
        {
            $where.=" AND to_days(tb_t1.updated_at)=to_days(now()) ";
            $flag='0';
        }

        $user=new UserAccess();
        $group=$user->getGroup($flag,$paramArr,'tb_t1.updated_at',$add_group);
        $info=DB::table('loan_trial_info as t1')
            ->leftJoin('admin_user as t2', 't1.admin_id', '=', 't2.id')
            ->leftJoin('loan_order as t3', 't1.order_id', '=', 't3.id')
            ->select(DB::raw('tb_t2.admin_username admin'),'t2.id',DB::raw('COUNT(tb_t1.id) num'))
            ->whereRaw($where)
//            ->where('t1.status',19)
//            ->whereBetween('loan_order.payment_date', [$start, $end])
            ->groupBy(DB::raw($group['group']));


        if($pageNum==0)
        {
            $data=$info->get();
        }
        else
        {
            $data=$info->paginate($pageNum);
        }
        if(!isset($paramArr['start']) || !isset($paramArr['end'])){
//            echo 11;
            $date =  Carbon::now()->toDateTimeString();
            $paramArr['start'] = date('Y-m-d 00:00:00', strtotime($date));
            $paramArr['end'] = date('Y-m-d 00:00:00', strtotime($date) + 86400);
        }
        $time=$user->DownTime($paramArr);
        $data=$this->getTrailOrder($data,'loan_trial_info',$time, $paramArr, [1,2,16,17,18]);
        $data=$this->overdue($data,'loan_trial_info',$time, $paramArr['start'],$paramArr['end']);

        return $data;

    }
     //电审信息
    public function countMessage($paramArr=[],$pageNum = 20)
    {
        $where=" tb_t1.status=21";
        $flag='0';
        $add_group="tb_t1.admin_id";
        if(isset($paramArr)&&count($paramArr)!=0)
        {

            if(!empty($paramArr['start'])&&empty($paramArr['end']))
            {
                $where.=" AND tb_t1.updated_at>='".$paramArr['start']."'";
                $flag='start';
            }
            if(!empty($paramArr['end'])&&empty($paramArr['start']))
            {
                $where.=" AND tb_t1.updated_at<='".$paramArr['end']."'";
                $flag='end';
            }
            if(!empty($paramArr['start'])&&!empty($paramArr['end']))
            {
                $where.=" AND tb_t1.updated_at between '".$paramArr['start']."' AND '".$paramArr['end']."'";
                $flag='between';
            }
            if(!empty($paramArr['month']))
            {
                $where.=" AND DATE_FORMAT(tb_t1.updated_at,'%Y-%m' ) = DATE_FORMAT(now(),'%Y-%m')";
                $flag='month';
            }
            if(!empty($paramArr['week']))
            {
                $where.=" AND YEARWEEK(date_format(tb_t1.updated_at,'%Y-%m-%d')) = YEARWEEK(now())";
                $flag='week';
            }
        }
        else
        {
            $where.=" AND to_days(tb_t1.updated_at)=to_days(now()) ";
            $flag='0';
        }

        $user=new UserAccess();
        $group=$user->getGroup($flag,$paramArr,'tb_t1.updated_at',$add_group);
        $info=DB::table('loan_trial_phone as t1')
            ->leftJoin('admin_user as t2', 't1.admin_id', '=', 't2.id')
            ->leftJoin('loan_order as t3', 't1.order_id', '=', 't3.id')
            ->select(DB::raw('tb_t2.admin_username admin'),'t2.id',DB::raw('COUNT(tb_t1.id) num'))
            ->whereRaw($where)
            ->groupBy(DB::raw($group['group']));

        if($pageNum==0)
        {
            $data=$info->get();
        }
        else
        {
            $data=$info->paginate($pageNum);
        }
        if(!isset($paramArr['start']) || !isset($paramArr['end'])){
//            echo 11;
            $date =  Carbon::now()->toDateTimeString();
            $paramArr['start'] = date('Y-m-d 00:00:00', strtotime($date));
            $paramArr['end'] = date('Y-m-d 00:00:00', strtotime($date) + 86400);
        }
        $time=$user->DownTime($paramArr);
        $data=$this->getTrailOrder($data,'loan_trial_phone',$time, $paramArr,[1,2,16,17,18,19,20]);
        $data=$this->overdue($data,'loan_trial_phone',$time, $paramArr['start'],$paramArr['end']);

        return $data;
    }

    public function track($date,$endDate)
    {
//        dd($date, $endDate);
        $info = DB::select("
                    select year(loan_repayment_date) year,
                        month(loan_repayment_date) month,
                        day(loan_repayment_date) day, 
                        t2.admin_username admin,
                        sum(repayment_amount) count 
                    from tb_loan_order_track t1
                    left JOIN tb_admin_user t2 on t1.admin_id=t2.id
                    left JOIN tb_loan_order t4 on t4.id = t1.order_id
                    where  t4.order_status='12'  
                    and loan_repayment_date >= ? 
                    and loan_repayment_date < ? 
                    group by year(loan_repayment_date),
                        month(loan_repayment_date),
                        day(loan_repayment_date), 
                        t1.admin_id 
                    ORDER BY t1.created_at desc",
            [$date, $endDate]);

        return $info;
    }
    public function trackAll($where)
    {
        $info = DB::select("
                   SELECT tb_admin_user.admin_username admin,
                        sum(repayment_amount) realCount,
                        sum(reciprocal_amount) applyCount
                        FROM
                        tb_loan_order
                   left join tb_loan_order_track on tb_loan_order.id = tb_loan_order_track.order_id
                   left JOIN tb_admin_user on tb_admin_user.id = tb_loan_order_track.admin_id
                   where
                        $where
                        and order_status in (25,11,12)
                  GROUP BY tb_admin_user.id
                  ORDER BY tb_admin_user.id");

        return $info;
    }
    public function trackSumByDay($where)
    {
        $info = DB::select("
                    select
                        sum(repayment_amount) realCount,
                        sum(reciprocal_amount) applyCount
                     from tb_loan_order
                   left join tb_loan_order_track on tb_loan_order.id = tb_loan_order_track.order_id
                   left JOIN tb_admin_user on tb_admin_user.id = tb_loan_order_track.admin_id
                   where $where
                        and order_status in (25,11,12)
                   ");

        return $info;
    }
    public function trackSumByMoth($date)
    {
        $info = DB::select("
                    select
                        sum(repayment_amount) realCount,
                        sum(reciprocal_amount) applyCount
                    from  tb_loan_order
                   left join tb_loan_order_track on tb_loan_order.id = tb_loan_order_track.order_id
                   left JOIN tb_admin_user on tb_admin_user.id = tb_loan_order_track.admin_id
                   where
                       loan_deadline <= ?
                        and order_status in (25,11,12)
                   ",
            [$date]);

        return $info;
    }

    public function trackMonth($where)
    {
        $info = DB::select("
            select t2.admin_username admin,
            sum(repayment_amount) count
            from tb_loan_order_track t1
            left JOIN tb_admin_user t2 on t1.admin_id=t2.id
            left JOIN tb_order_status t3 on t1.order_id=t3.order_id 
            left JOIN tb_loan_order t4 on t4.id = t1.order_id
            where $where AND t3.status=12
            group by t1.admin_id
            ORDER BY t1.created_at desc");

        return $info;
    }
    public function trackAllMonth($date, $endDate)
    {
        $info = DB::select("
                select
                year(t3.created_at) year,
                month(t3.created_at) month,
                day(t3.created_at) day,
                t2.admin_username admin,
                sum(repayment_amount) realCount,
                sum(reciprocal_amount) applyCount
                from tb_loan_order_track t1
                left JOIN tb_admin_user t2 on t1.admin_id=t2.id
                left JOIN tb_order_status t3 on t1.order_id=t3.order_id
                left JOIN tb_loan_order t4 on t4.id = t1.order_id
                where  t3.status in (11,12)
                and t3.created_at >= ?
                and t3.created_at < ?
                group by t1.admin_id
                ORDER BY t1.created_at desc",[$date, $endDate]);

        return $info;
    }

    public function getTrackHandOrder()
    {
        $trackUser = AdminUser::getListByRoleInfo(4);
        $trackArr = $trackUser->pluck('admin_username', 'id');
//        $trackUser = $this->getTrackAdmin(4);
        return $trackUser;

    }
    public function getTrackAdmin($role_id)
    {
        $info = DB::select('
                SELECT
                    *
                FROM
                    tb_admin_user
                LEFT JOIN tb_admin_role_info on tb_admin_role_info.aid = tb_admin_user.id
                where tb_admin_user.is_del = 0
                and tb_admin_user.admin_status = 0
                and tb_admin_role_info.rid = ?
                and tb_admin_role_info.status = 0
        ', [$role_id]);
        return $info;
    }
    public function getTrackOrderByDay($period)
    {
        $now = date('Y-m-d H:i:s', time());
        $info = DB::select('SELECT 
                    tb_admin_user.id,
                    count(tb_loan_order.id) as count FROM tb_loan_order
                    left join tb_loan_order_track on tb_loan_order.id = tb_loan_order_track.order_id
                    left JOIN tb_admin_user on tb_admin_user.id = tb_loan_order_track.admin_id                
                    where
                    order_status != 12
                    and DATEDIFF( ?,loan_deadline) = ?
                    GROUP BY tb_admin_user.id',[$now, $period]
        );
        return $info;

    }
//    public function get
    public function getAllTrack($period)
    {
        $now = date('Y-m-d H:i:s', time());
        $info = DB::select('SELECT 
                    tb_admin_user.id,
                    count(tb_loan_order.id) as count FROM tb_loan_order
                    left join tb_loan_order_track on tb_loan_order.id = tb_loan_order_track.order_id
                    left JOIN tb_admin_user on tb_admin_user.id = tb_loan_order_track.admin_id                
                    where
                    order_status != 12  
                    and DATEDIFF( ?,loan_deadline) >= ?          
                    GROUP BY tb_admin_user.id',[$now, $period]
        );
        return $info;
    }
//    public function

    public function format($orders)
    {
        $orders->map(function($order) {
            $order['order_status'] = OrderStatus::toString($order['order_status']);
            $order['loan_period'] = LoanDay::toString($order['loan_period']);
            return $order;
        });

        return $orders;
    }

    public function handFinalData($start, $end)
    {
        $signEnd = date('Y/m/d', strtotime($end));
        $signStart = date('Y/m/d', strtotime($start));
        $now = Carbon::now();

        $start = date('Y-m-d 00:00:00', strtotime($start));
        $end = date('Y-m-d 00:00:00', strtotime($end) + 86400);
        $totalOrder = $this->getFinalData($start, $end,[0,1,2], [5,6,7,8,9,10,11,12,25,14,5,23,14]);
        $passOrder = $this->getFinalData($start, $end , $trialStatus = [1], $orderStatus = [9,10,11,12,25,14]);
        $allOrder = $this->getAllLoanToCardOrder($start, $end,  [9,10,11,12,25,23,14]);
        $overOrder = $this->getOverDueOrder($start, $end);
        $badOrder = $this->getOverDueOrder($start, $end, $orderStatus = [11], $overDueDay = 15);
        $adminCount = [];
        if($totalOrder->count() > 0){
            foreach ($totalOrder as $k =>$v){
                if($signStart == $signEnd){
                    $adminCount[$k]['time'] = $signStart;
                } else {
                    $adminCount[$k]['time'] = $signStart . '~' . $signEnd;
                }

                $adminCount[$k]['admin'] = '';
                $adminCount[$k]['total'] = 0;
                $adminCount[$k]['pass'] = 0;
                $adminCount[$k]['passRate'] = 0;
                $adminCount[$k]['over'] = 0;
                $adminCount[$k]['bad'] = 0;

                $adminCount[$k]['admin'] = $v->admin_username;
                $adminCount[$k]['total'] = $v->count;

                if($passOrder->count() > 0){
                    foreach ($passOrder as $pass){
                        if($pass->id == $v->id){
                            $adminCount[$k]['pass'] = $pass->count;
                            $adminCount[$k]['passRate'] = number_format($pass->count/$v->count,2)* 100 .'%';
                        }

                    }
                }

                if($allOrder->count() > 0){
                    foreach ($allOrder as $allV){
                        if($allV->id == $v->id){

                            if($overOrder->count() > 0){
                                foreach ($overOrder as $overV){
                                    if($overV->id == $v->id){
                                        $adminCount[$k]['over'] = number_format($overV->count/$allV->count,2)* 100 .'%';
                                    }

                                }
                            }

                            if($badOrder->count() > 0){
                                foreach ($badOrder as $badV){
                                    if($badV->id == $v->id){
                                        $adminCount[$k]['bad'] = number_format($badV->count/$allV->count,2)* 100 .'%';
                                    }

                                }
                            }

                        }
                    }
                }

            }
        }
        return $adminCount;

    }

    /**
     *  处理用户的信息
     * @param $start
     * @param $end
     * @return array
     */
    public function handUsereInfo($start, $end)
    {
        $start = date('Y-m-d 00:00:00', strtotime($start));
        $end = date('Y-m-d 00:00:00', strtotime($end) + 86400);
        $totalUser = [];
        $user = $this->finalUserInfo($start, $end);
        $totalUser['total'] = $user->count();

        $old = $this->getOldUser($start, $end);
        $totalUser['old'] = $old->count();
        $totalUser['oldRate'] = empty($totalUser['total'] && $totalUser['old']) ? 0 :number_format($totalUser['old']/$totalUser['total'],2) * 100 .'%';

        return $totalUser;

    }

    /**
     * 老用户的信息
     * @param $start
     * @param $end
     * @return Collection
     */
    private function getOldUser($start, $end)
    {
        $query = LoanOrder::select(DB::raw('count(*) as count'));
        $query->join('loan_trial_final', 'loan_trial_final.order_id', '=', 'loan_order.id');
        $order = $query
            ->whereIn('uid', function ($old){
                $old->select('uid')->from('loan_order')
                    ->where('order_status', '=', 12);

            })
            ->whereIn('order_status', [8,9,10,11,14,12,25])
            ->whereBetween('loan_trial_final.updated_at', [$start, $end])
            ->groupBy('uid')
            ->get();
//        dd($order);
//        dd($order->count());
        return $order;
    }

    /**
     * 查询提交订单的用户量
     * @param $start
     * @param $end
     * @return Collection
     */
    public function finalUserInfo($start, $end)
    {

        $query = LoanOrder::select(DB::raw('count(*) as count'));
        $query->join('loan_trial_final', 'loan_trial_final.order_id', '=', 'loan_order.id');
        $order = $query
            ->whereBetween('loan_trial_final.updated_at', [$start, $end])
            ->whereIn('order_status', [8,9,10,11,12,14,25])
            ->groupBy('uid')
            ->get();
//        dd($order);
        return $order;
    }

    /**
     * 终审数据通过的件，总件
     * @param $start 起始时间
     * @param $end 终止时间
     * @param array $trialStatus  审核状态
     * @param array $orderStatus 订单的状态
     * @return Collection
     */
    private function getFinalData($start, $end, $trialStatus = [0,1,2] , $orderStatus = [6,8,9,10,11,12,25,14,5,23,14])
    {

        $query = LoanOrder::select([
            'admin_user.id',
            'admin_user.admin_username',
            DB::raw('count(*) as count')
        ]);
        $query->join('loan_trial_final', 'loan_order.id', '=', 'loan_trial_final.order_id');
         $query->join('admin_user', 'loan_trial_final.admin_id', '=', 'admin_user.id');
        $orders = $query
                ->whereIn('loan_trial_final.status', $trialStatus)
                ->whereIn('loan_order.order_status', $orderStatus)
                ->whereBetween('loan_trial_final.trial_time', [$start, $end])
                ->groupBy('admin_user.id')
                ->get();
        return $orders;
    }

    /**
     *  终审审核人员的逾期件，坏账件数
     * @param $start
     * @param $end
     * @param array $orderStatus
     * @param int $overDueDay
     * @return Collection
     */
    public function getOverDueOrder($start, $end, $orderStatus = [11], $overDueDay = 0)
    {
        $query = LoanOrder::select([
            'admin_user.id',
            'admin_user.admin_username',
            DB::raw('count(*) as count')
        ]);
        $query->join('loan_trial_final', 'loan_order.id', '=', 'loan_trial_final.order_id');
        $query->join('admin_user', 'loan_trial_final.admin_id', '=', 'admin_user.id');
        $query->join('order_status', 'order_status.order_id', '=', 'loan_order.id');
        $orders = $query
                ->whereIn('order_status.status', $orderStatus)
//                ->orWhereRaw('loan_repayment_date > loan_overdue_date')
                ->whereRaw('DATEDIFF(?,loan_overdue_date) >= ?',[$end,$overDueDay])
                ->whereBetween('loan_order.payment_date', [$start, $end])
                ->groupBy('admin_user.id')
                ->get();
//        dd($orders, $start, $end, $orderStatus,$overDueDay);
        return $orders;
    }

    /**
     * 终审逾期，还清，未还清的件数和
     * @param $start
     * @param $end
     * @param array $orderStatus
     * @return Collection
     */
    public function getAllLoanToCardOrder($start, $end, $orderStatus = [25,11,12])
    {
        $query = LoanOrder::select([
            'admin_user.id',
            'admin_user.admin_username',
            DB::raw('count(*) as count')
        ]);
        $query->join('loan_trial_final', 'loan_order.id', '=', 'loan_trial_final.order_id');
        $query->join('admin_user', 'loan_trial_final.admin_id', '=', 'admin_user.id');
//        $query->join('order_status', 'loan_order.id', '=', 'order_status.order_id');
        $orders = $query
            ->whereIn('loan_order.order_status', $orderStatus)
//            ->orWhereRaw('loan_repayment_date > loan_overdue_date')
            ->whereBetween('loan_order.payment_date', [$start, $end])
            ->groupBy('admin_user.id')
            ->get();
        return $orders;
    }
    //获取该电审人员的审核过的订单id
    public function getOrderid($admin_id,$table)
    {
        $order_id= DB::table($table)
            ->whereRaw("admin_id='".$admin_id."'")
//            ->whereBetween('updated_at', [$start, $end])
            ->pluck('order_id')
            ->toArray();
        $str=",";
        if(!empty($order_id))
        {
            foreach ($order_id as $v)
            {
                $str.=$v.',';
            }

            return trim($str,",");
        }
        else
        {
            return false;
        }

    }
    public function overdue($data,$table,$time, $start, $end)
    {
        //获取逾期率以及坏账率
        $data->map(function ($item, $k) use($table,$time,$start,  $end){

            //获取该用户的审核的电审订单
            $order_id=$this->getOrderId($item->id,$table);
            $item->only=$time;
            if($order_id)
            {
                //逾期率与坏账率
                $track=DB::table('loan_order')
                    ->select(DB::raw("COUNT(CASE WHEN order_status='11' AND id IN($order_id) THEN id END)/COUNT(CASE WHEN order_status in('11','12','13','25','14') AND id IN($order_id) THEN id END)*100 AS overdute"),
                        DB::raw("COUNT(CASE WHEN order_status='11' AND id IN($order_id) AND NOW() > DATE_ADD(loan_deadline, INTERVAL 15 DAY) THEN id END)/COUNT(CASE WHEN order_status in('11','12','13','25','14') AND id IN($order_id) THEN id END)*100 AS bad_debt" ))
                    ->whereBetween('loan_order.payment_date', [$start, $end])
                    ->first();
                $item->overdue=intval($track->overdute).'%';
                $item->bad_debt=intval($track->bad_debt).'%';
            } else {

                //逾期率
                $item->overdue='--';
                //坏账率
                $item->bad_debt='--';
            }
            unset($item->id);
            return $item;
        });
        return $data;
    }
    public function getTrailOrder($data,$table,$time, $paramArr, $status)
    {
//        dd($status);

        if(isset($paramArr)&&count($paramArr)!=0)
        {

            if(!empty($paramArr['start'])&&empty($paramArr['end']))
            {
                $where="tb_$table.updated_at>='".$paramArr['start']."'";
                $flag='start';
            }
            if(!empty($paramArr['end'])&&empty($paramArr['start']))
            {
                $where=" tb_$table.updated_at<='".$paramArr['end']."'";
                $flag='end';
            }
            if(!empty($paramArr['start'])&&!empty($paramArr['end']))
            {
                $where=" tb_$table.updated_at between '".$paramArr['start']."' AND '".$paramArr['end']."'";
                $flag='between';
            }
            if(!empty($paramArr['month']))
            {
                $where="DATE_FORMAT(tb_$table.updated_at,'%Y-%m' ) = DATE_FORMAT(now(),'%Y-%m')";
                $flag='month';
            }
            if(!empty($paramArr['week']))
            {
                $where=" YEARWEEK(date_format(tb_$table.updated_at,'%Y-%m-%d')) = YEARWEEK(now())";
                $flag='week';
            }
        }
        else
        {
            $where =" to_days(tb_$table.updated_at)=to_days(now()) ";
            $flag='0';
        }

        $data->map(function($item, $k)use($table,$time,$where, $status){
//            dd($item);
//            $order_id=$this->getOrderidPeriod($item->id,$table, $start, $end);

            $track=DB::table('loan_order')
                ->select('loan_order.id')
                ->join($table, "$table.order_id", '=', 'loan_order.id')
                ->whereNotIn("loan_order.order_status",$status )
                ->where('admin_id',$item->id )
                ->whereRaw($where)
                ->get();
//            dd($track);
//            ;
            if($track->count() > 0){

                $item->totalOrder=$track->count();
                $item->parssRate=round($item->num/$track->count(), 4) * 100 . '%';

            } else {

                $item->totalOrder=0;
                $item->parssRate=0;

            }

            return $item;
        });
        return $data;
//        $order = LoanOrder::select('admin_user.id',
//            DB::raw('count(*) as count'))

    }

    public function getOrderidPeriod($admin_id,$table, $start, $end)
    {
        $order_id= DB::table($table)
            ->whereRaw("admin_id='".$admin_id."'")
            ->whereBetween('updated_at', [$start, $end])
            ->pluck('order_id')
            ->toArray();
        $str=",";
        if(!empty($order_id))
        {
            foreach ($order_id as $v)
            {
                $str.=$v.',';
            }

            return trim($str,",");
        }
        else
        {
            return false;
        }

    }
}