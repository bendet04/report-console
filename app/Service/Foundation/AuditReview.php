<?php

namespace App\Service\Foundation;

use App\Events\InfoAfter;
use App\Models\AdminCheckUser;
use App\Models\LoanOrder;
use App\Models\LoanTrialInfo;
use App\Models\AdminRoleInfo;
use App\Models\LoanTrialInfoRecord;
use App\Service\Assign;
use App\Service\Contracts\Order\Trial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AuditReview
{

    /**
     * 获取信审人员昨天信息
     * @param array $paramArr
     * @param string $orderBy
     * @param int $pageNum
     * @return array
     */

    public function detail($role,$admin_id)
    {
        $roleCount=new AdminRoleInfo();
        $num=$roleCount->getRoleNumber($role);
        $status=[1,2,16,17,18];
        $where="TO_DAYS(now()) - TO_DAYS(tb_t1.updated_at) <= 1 ";
        $Mywhere="TO_DAYS(now()) - TO_DAYS(tb_t1.updated_at) <= 1 and admin_id=$admin_id";
        $result=[];
        $data=[];
        $data=$this->sum($status,$Mywhere);
        $sum=$this->sum($status,$where);
//        dd($data,$sum);
        //所有人审核平均值
        $data[0]->sumAvg=$sum[0]->sum/$num;
        $data[0]->myPass=$this->pass($Mywhere)[0]->num;
        $passSum=$this->pass($where);
        $data[0]->passAvg=$passSum[0]->num/$num;
        $data[0]->type=1;
        $data[0]->time=date("Y-m-d",strtotime("-1 day"));
        return $result['info']=$data;
    }
    
    public function sum($status,$where)
    {
        $track=DB::table('loan_order')
            ->select(DB::RAW('count(*) as sum'))
            ->join('loan_trial_info as t1', "t1.order_id", '=', 'loan_order.id')
            ->whereNotIn("loan_order.order_status",$status )
            ->whereRaw($where)
            ->get();
        return $track;
    }
    public function pass($where)
    {
        $info = DB::table('loan_trial_info as t1')
            ->leftJoin('loan_order as t3', 't1.order_id', '=', 't3.id')
            ->select( DB::raw('COUNT(tb_t1.id) num'))
            ->whereRaw("$where  and tb_t1.status=19")->get();
        return $info;
    }
}