<?php

namespace App\Service\Foundation;

use Illuminate\Support\Facades\Log;

class BankPayment
{
    public $bank = null;
    
    public function __construct(BluePay $bluepay)
    {
        $this->bank = $bluepay;
    }
    
    /**
     * 放款操作
     * @param $transactionId 用户的交易id
     * @param $payeeBankName 银行的名字
     * @param $payeeName 持卡人的名字
     * @param $payeeAccount 银行的账号
     * @param $payeeMsisdn 手机号
     * @param $amount 金额
     * @return array
     */
    public function payment($platform, $transactionId, $payeeBankName, $payeeName, $payeeAccount, $payeeMsisdn, $amount)
    {
        if ($platform=='MiniRupiah') {
            $host = env('BLUE_URL') . '/charge/indonesiaFintechTransfer/transferBalance';
            $productId = env('BLUE_PRODUCT');
            $key = env('BLUE_KEY');
        } else {
            $host = env($platform.'_BLUE_URL') . '/charge/indonesiaFintechTransfer/transferBalance';
            $productId = env($platform.'_BLUE_PRODUCT');
            $key = env($platform.'_BLUE_KEY');
        }
        
        
        $data = "transactionId=".$transactionId."&promotionId=1000&payeeCountry=ID&payeeBankName=".$payeeBankName."&payeeName=".$payeeName."&payeeAccount=".$payeeAccount."&payeeMsisdn=" .$payeeMsisdn. "&payeeType=NORMAL&amount=".$amount."&currency=IDR";
        
        Log::info('encypt is ' .$data);
        $dataInfo = $this->bank->AESphp($data,$key);
        $enctypt = md5("productId=".$productId."&data=".$dataInfo.$key);
        
        $host = $host . '?productId=' . $productId .'&data='. $dataInfo . '&encrypt='. $enctypt;
        header("Content-type:text/html;charset=utf-8");
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $host);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        # do not verify SSL cert
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        
        Log::error($host);
        $data = curl_exec($curl);
        Log::error('****');
        Log::error($data);
        Log::error('****');
        curl_close($curl);
        return json_decode($data, true);
        
    }
}