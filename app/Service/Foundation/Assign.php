<?php

namespace App\Service\Foundation;

use App\Models\AdminAssign;
use App\Models\AdminUser;
use App\Models\LoanOrderTrack;
use App\Models\LoanTrialFinal;
use App\Models\LoanTrialInfo;
use App\Models\LoanTrialPhone;
use Carbon\Carbon;

class Assign
{
    public function assign($order_id, $type, $adminId=[])
    {
        /**
         * Warning：修改角色id请修改对应的表记录：
         * tb_admin_assign
         */
        $roleArr = [
            'info'  => 2, //信审人员
            'phone' => 10, //电审人员
            'final' => 3, //终审人员
            'track' => 4, //催收人员
        ];
        if (!isset($roleArr[$type])){
            return false;
        }
        
        //获取管理员列表
        if($adminId){
            $operator = collect($adminId);
//            $operatorCount = $operator->count();

        } else {
            $operator = AdminUser::selectOprator($roleArr[$type]);
//            $operatorCount = $operator->count();
        }
        $operatorCount = $operator->count();
        if ($operatorCount==0){
            return false;
        }

        
        //获取分配位置
        $assign = AdminAssign::where('role_id', $roleArr[$type])->first();
        $total = $assign->pos;
        $assign->increment('pos');
        $pos = $total%$operatorCount;
        
        //分配
        if($adminId){
            $admin_id = $operator[$pos]['id'];
        } else {
            $admin_id = $operator[$pos]->id;
        }

//        dd($admin_id);

        //信审任务分配
        if ($type == 'info'){
//            Log::info('info assign admin id '. $admin_id);
            return LoanTrialInfo::saveData($order_id, [
                'admin_id' => $admin_id,
                'status'   => 0,
            ]);
        }
        
        //电审任务分配
        if ($type == 'phone'){
            return LoanTrialPhone::saveData($order_id, [
                'admin_id' => $admin_id,
                'status'   => 0,
            ]);
        }

        //终审任务分配
        if ($type == 'final') {
            return LoanTrialFinal::saveOne($order_id, [
                'admin_id' => $admin_id,
                'status'   => 0,
                'remark'   => '',
            ]);
        }
        
        //催收任务分配
        if ($type == 'track'){
            return LoanOrderTrack::saveOne($order_id, [
                'admin_id' => $admin_id,
                'track_time' => Carbon::now(),
                'track_status'   => 0,
            ]);
        }
        
    }
}