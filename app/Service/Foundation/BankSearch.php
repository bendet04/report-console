<?php

namespace App\Service\Foundation;

use Illuminate\Support\Facades\Log;

class BankSearch
{
    public $bank = null;
    
    public function __construct(BluePay $bluepay)
    {
        $this->bank = $bluepay;
    }
    
    /**
     * Bluepay银行流水状态查询
     * @param $tId 交易id
     * @param $type 1放款流水查询 2收款流水查询
     * @return mixed
     */
    public function search($tId, $type=1, $platform = 'MiniRupiah')
    {
        if ($type==1){//放款查询
            $host = env('BLUE_URL') . '/charge/indonesiaFintechTransfer/queryTransfer';
        }else{//收款查询
            $host = env('BLUE_URL') . '/charge/service/queryTrans';
        }
        if($platform == "MiniRupiah"){
            $productId = env('BLUE_PRODUCT');
        } else {
            $productId = env('UangBagus_BLUE_PRODUCT');
        }

        
        $data = "";
        $key = env('BLUE_KEY');
        
        $dataInfo = $this->bank->AESphp($data,$key);
        $enctypt = md5("operatorId=6&productid=$productId&t_id=$tId".$key);
        
        $host = $host . '?operatorId=6&productid=' . $productId .'&t_id='.$tId.'&encrypt='. $enctypt;
        header("Content-type:text/html;charset=utf-8");
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $host);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        # do not verify SSL cert
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        
        Log::error($host);
        $data = curl_exec($curl);
        Log::error('****');
        Log::error($data);
        Log::error('****');
        curl_close($curl);
        return json_decode($data, true);
        
    }
}