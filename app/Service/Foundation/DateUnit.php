<?php

namespace App\Service\Foundation;

class DateUnit
{
    //两个日期转化为天数
    public function timeToDay($overdue)
    {
        $date = date('Y-m-d', strtotime($overdue));
        $now =date('Y-m-d', time());
        $second1 = strtotime($now);
        $second2 = strtotime($date);
        
        if($second1 < $second2)
            return 0;
        
        return ($second1 - $second2) / 86400;
    }
    
    //转化为一天
    public function get_days($date1)
    {
        $time1 = strtotime(date('Y-m-d', strtotime($date1)));
        $time2 = strtotime(date('Y-m-d', time()));
        if($time1 < $time2)
            return "";
        
        return ($time1-$time2)/86400;
    }
    public function formatTime($time)
    {
        return date('Y-m-d', strtotime($time));
    }
    
    //时间戳与现在的时间戳比较
    public function compareNow($date)
    {
        $date = strtotime(date('Y-m-d', strtotime($date)));
        $time2 = strtotime(date('Y-m-d', time()));
        if($time2 > $date){
            return true;
        } else {
            return false;
        }
        
    }
    public function getFormatMonth($data)
    {
        $date = explode('/', $data);
//        dd($date);/
        $date = mktime(0,0,0,$date[0],1,$date[1]);
        return $date;
    }
    public function getNextMonth($tmp_date)
    {
        $tmp_date = date('Ym', strtotime($tmp_date));
        $tmp_year=substr($tmp_date,0,4);
        //切割出月份
        $tmp_mon =substr($tmp_date,4,2);
        $tmp_nextmonth=mktime(0,0,0,$tmp_mon+1,1,$tmp_year);

            //得到当前月的下一个月
        return $fm_next_month=date("Y-m-01",$tmp_nextmonth);
    }
    
    public function getDiffDay($day1, $day2)
    {
        $date = date('Y-m-d', strtotime($day1));
        $now =date('Y-m-d', strtotime($day2));
        $second1 = strtotime($now);
        $second2 = strtotime($date);
        
        if($second1 < $second2)
            return 0;
        
        return ($second1 - $second2) / 86400;
    }
}