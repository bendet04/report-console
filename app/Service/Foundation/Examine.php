<?php

namespace App\Service\Foundation;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Examine
{
    public function getInstance($status)
    {
        switch ($status) {
            case '2': //信审
                return new AuditReview();
                break;
            case '10': //电审
                return new ElectricAuditReview();
                break;
            case '3': //终审
                return new FinalAuditReview();
                break;
           
            default:
                throw new FileException('Not found the role');
        }
    }
    public function moreRole($role,$admin_id)
    {
        $data['final']=[];
        $data['info']=[];
        $data['phone']=[];
         if(in_array('3',$role)){
             $final=new FinalAuditReview();
             $data['final']= $final->detail($role,$admin_id);
         }if(in_array('2',$role)){
             $info=new AuditReview();
             $data['info']=$info->detail($role,$admin_id);
         }if(in_array('10',$role)){
             $phone=new ElectricAuditReview();
             $data['phone']=$phone->detail($role,$admin_id);
         }
         return $data;
    }
}