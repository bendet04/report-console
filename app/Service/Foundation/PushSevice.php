<?php

namespace App\Service\Foundation;

use Illuminate\Support\Facades\Log;

class PushSevice
{
    
    //终审通过的信息
    public function pushTrialSuccess($platform, $to)
    {
        $body = 'Aplikasi pinjaman Anda telah lulus verifikasi. Mohon tunggu pinjaman cair.';
        $title = 'Aplikasi pinjaman lulus verifikasi';
        Log::info('final trial to' . $to);
        $info = $this->pushMessage($platform, $to, $body, $title);
        return $info;
    }
    
    //终审失败的信息
    public function pushTrialFailure($platform, $to)
    {
        $body = 'Mohon Maaf, informasi aplikasi pinjaman Anda tidak lulus verifikasi.';
        $title = 'Aplikasi pinjaman gagal';
        Log::info('fail trial to' . $to);
        $info = $this->pushMessage($platform, $to, $body, $title);
        return $info;
    }
    
    //放款成功的信息
    public function pushPayment($platform, $to, $card)
    {
        $body = 'Pinjaman telah dikreditkan ke kartu bank penerima yang ditunjuk dari informasi pengisian anda (**** '.$card.'),mohon dicek .';
        $title = 'Pembayaran pinjaman sukses';
        Log::info('success payment to'. $to);
        $info = $this->pushMessage($platform, $to, $body, $title);
        return $info;
    }
    
    //还款成功的信息
    public function pushRepayment($platform, $to, $total)
    {
        $body = 'Kami telah menerima pembayaran Anda, jumlah pembayaran Rp '. $total;
        $title = 'Pembayaran pinjaman sukses';
        Log::info('success repayment to'. $to);
        $info = $this->pushMessage($platform, $to, $body, $title);
        return $info;
    }
    
    //进入投资期
    public function investmentBegin($to, $platform='Modalin')
    {
        $body = 'Anda telah masuk tahap Pendanaan, Dana sudah berhasil di transfer ke peminjam, harap perhatikan situasi pelunasan peminjam.';
        $title = 'Memasuki Masa Pendanaan';
        Log::info('success investmentBegin to'. $to);
        $info = $this->pushMessage($platform, $to, $body, $title);
        return $info;
    }
    
    //投资提现
    public function investmentApplyAmount($to, $card, $platform='Modalin')
    {
        $body = 'Penarikan Dana telah berhasil di transfer ke dalam bank anda（**'.$card.'）,silahkan periksa.';
        $title = 'Penarikan Dana';
        Log::info('success investmentApplyAmount to'. $to);
        $info = $this->pushMessage($platform, $to, $body, $title);
        return $info;
    }
    
    //请求的信息
    private function pushMessage($platform, $to, $body, $title)
    {
        if ($platform=='MiniRupiah') {
            $token = env('PUSH_ACCESSTOKEN');
            $url = env('PUSH_URL');
        } else {
            $token = env($platform.'_PUSH_ACCESSTOKEN');
            $url = env($platform.'_PUSH_URL');
        }
        
        $header = array
        (
            'Authorization: key='.$token,
            'Content-Type: application/json'
        );
        
        $data = [
            'to' => $to,
            'notification' => [
                'body' => $body,
                'title' => $title
            ]
        ];
        $result = $this->curlRequest($url, $header, $data);
        return ['title'=>$title, 'body'=>$body, 'result'=>$result];
    }
    
    
    //curl的请求
    private function curlRequest($url, $header, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        
        # do not verify SSL cert
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        
        $errno = curl_errno($curl);
        $error = curl_error($curl);
        Log::info('errno ' . $errno . 'error ' .$error);
        
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }
    
}