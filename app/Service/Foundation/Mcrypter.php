<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/3/3/003
 * Time: 13:25
 */
namespace App\Service\Foundation;

class Mcrypter
{
    #加密方式
    private $cipher = '';
    #key
    private $key = '';
    #iv
    private $iv = '';
    #padding
    private $options = OPENSSL_RAW_DATA;
    
    
    public function __construct()
    {
        $this->cipher =  'DES-EDE3-CBC';
        $this->key = env('CIPHER_KEY');
        $this->iv = '01234567';
        $this->options = OPENSSL_RAW_DATA;
    }
    
    private function getCipher() {
        return $this->cipher;
    }
    
    private function getkey() {
        return $this->key;
    }
    
    private function getIv() {
        return $this->iv;
    }
    
    private function getOptions() {
        return $this->options;
    }
    
    public function encrypt($data) {
        return base64_encode(openssl_encrypt($data, $this->getCipher(), $this->getkey(), $this->getOptions(), $this->getIv()));
    }
    
    public function decrypt($data) {
//        print_r($data);
//        print_r('<br />'.$this->getCipher());
//        print_r('<br />'.$this->getKey());
//        print_r('<br />'.$this->getOptions());
//        print_r('<br />'.$this->getIv());
//        print_r('<br />');
        $ret = openssl_decrypt(base64_decode($data), $this->getCipher(), $this->getkey(), $this->getOptions(), $this->getIv());
        if (is_bool($ret))
            return "Failure";
        
        return $ret;
    }
}