<?php

namespace App\Service\Foundation;

class BluePay
{
    public function getKey($key)
    {
        if (empty($key) || $key== null) {
            $key="0000000000000000";
        }
        $len = strlen($key);
        if (strlen($key) > 16) {
            $key = substr($key, 0,16);
        }else{
            $i = 16 - $len;
            while ($i  > 0) {
                $key = $key. "0";
                $i --;
            }
        }
        return $key;
    }
    
    public function AES($data,$key)
    {
        
        $iv= "zxcvbnmk09876543";
        $module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, "");
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = $block - (strlen($data) % $block); //计算是否需要填充
        
        $data .= str_repeat(chr($pad), $pad); //不足16的倍数在末尾填充
        $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, self::getKey($key), $data, MCRYPT_MODE_CBC, $iv);
        $encrypted = bin2hex($encrypted);
        $encrypted = base64_encode($encrypted);
        $encrypted = urlencode($encrypted);
        return  $encrypted;
    }
    
    //php7.1
    public function AESphp($data,$key)
    {
        $iv= "zxcvbnmk09876543";
        $encrypted = openssl_encrypt($data, 'AES-128-CBC', $key,OPENSSL_RAW_DATA, $iv);
        $encrypted = bin2hex($encrypted);
        $encrypted = base64_encode($encrypted);
        $encrypted = urlencode($encrypted);
        return  $encrypted;
    }
    
}