<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/4/2/002
 * Time: 20:59
 */

namespace App\Service;

use App\Models\SmsCode;
use App\Service\Sms\SmsProviderFactory;
use Illuminate\Support\Facades\Log;


class OverMessage
{
    public static $from = 'ProsperiTree';
    
    //放款
    public static function repayment($mobile, $card, $user_id)
    {
        $text = 'Pinjaman sudah dicairkan ke rekening bank anda (**'.$card.'), mohon dicek kembali.[SenderId]';
        
        Log::debug('over push message ' . $text . ' userId ' . $user_id);

        $result = self::message($mobile, $text);
        Log::debug('message final result ' . $result);

    }
    
    //进入投资期
    public static function investmentBegin($mobile, $user_id)
    {
        $text = 'Anda telah masuk tahap Pendanaan, Dana sudah berhasil di transfer ke peminjam, harap perhatikan situasi pelunasan peminjam.[SenderId]';
    
        Log::debug('over push message ' . $text . ' userId ' . $user_id);
    
        $result = self::message($mobile, $text);
        Log::debug('message begin investment ' . $result);
    }
    
    //提现到账
    public static function investmentApplyAmount($mobile, $card, $user_id)
    {
        $text = 'Penarikan Dana telah berhasil di transfer ke dalam bank anda（**'.$card.'）,silahkan periksa.[SenderId]';
    
        Log::debug('over push message ' . $text . ' userId ' . $user_id);
    
        $result = self::message($mobile, $text);
        Log::debug('message investment apply ' . $result);
    }

    public static function message($mobile,$text)
    {
        $userMobile = '62' . ltrim($mobile, 0);
        $sms = SmsProviderFactory::getInstance();
        $sms->setText($text);
        $sms->setFrom(self::$from);
        $result = $sms->send($userMobile);

        SmsCode::addSms([
            'provider'     => SmsProviderFactory::getProvider(),
            'cc'           => '62',
            'phone'        => $mobile,
            'code'         => $text,
            'type'         => 10,
            'sms_provider' => $sms->getType(),
            'message'      => $result,
        ]);
        
        return $result;

    }
    private static function requestCurl($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }
}