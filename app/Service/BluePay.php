<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static getKey($key)
 * @method static AES($data,$key)
 * @method static AESphp($data,$key)
 */
class BluePay extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\BluePay';
    }
}
