<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/9/13
 * Time: 14:17
 */

namespace App\Service;


use Illuminate\Support\Facades\Facade;
/**
 * @method static getInstance(string $status)
 */
class PaymentFactory extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Pay\PaymentFactory';
    }
}