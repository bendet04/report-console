<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static timeToDay(string $overdue)
 * @method static get_days(string $date1)
 * @method static compareNow(string $date)
 */
class DateUnit extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\DateUnit';
    }
}
