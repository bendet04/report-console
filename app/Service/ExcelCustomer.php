<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/25/025
 * Time: 16:29
 */

namespace App\Service;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExcelCustomer  implements FromCollection
{
    use Exportable;

    public $arrData ;
    public function __construct($arrData)
    {
        $this->arrData = $arrData;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return collect($this->arrData);
    }
}