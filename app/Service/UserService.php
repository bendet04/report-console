<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static attemptLogin($paramArr = [])
 * @method static encrypt($pwd)
 * @method static getUserAccess()
 * @method static getUserMenu()
 * @method static partnerInfo($uid, $partner_name)
 */
class UserService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\UserService';
    }
}
