<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static search($tId, $type)
 */
class BankSearch extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\BankSearch';
    }
}
