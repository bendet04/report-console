<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/8/16/016
 * Time: 13:01
 */

namespace App\Service\Pay;

use Illuminate\Support\Facades\Log;

class DokuService extends DokuBasic
{

    public function __construct()
    {

        DokuInitiate::$mallId = env('DOKU_ID');
        DokuInitiate::$sharedKey = env('DOKU_KEY');
        $this->mallId = DokuInitiate::$mallId;
        $this->key = DokuInitiate::$sharedKey;
        $this->transation = $this->generateTransationId();
    }

    public function payment($amount,$order_id,  $cardNumber, $uid, $bankCode, $bankId, $bankName, $clientName, $phone, $transaction)
    {
        $amount = number_format($amount,2,".","");
//        $transaction = $this->generateTransationId();

//     先去duko服务器报备
//        dd($amount, $transaction, $cardNumber, $bankCode, $bankId, $bankName,$clientName);
        $inqueryResult = $this->inqueryService($amount, $transaction, $cardNumber, $bankCode, $bankId, $bankName,$clientName);
        if($inqueryResult->status != 0){
            Log::error('order_id '. $order_id .' inquery error and return status is '. $inqueryResult->status);
            return false;
        };
        $tokenId = $inqueryResult->inquiry->idToken;

//      请求放款
        $cashResult = $this->remitLoanToUser($amount,  $cardNumber, $bankCode, $bankId, $bankName, $clientName,$transaction,$tokenId,$phone);
        if($cashResult->status != 0){
            Log::error('order_id '. $order_id .' remit error and return status is '. $cashResult->status . ' idToken is ' . $tokenId);
            return false;
        };
        if(isset($cashResult->remit->transactionId) && empty($cashResult->remit->transactionId)){
            Log::error('order_id '. $order_id .' remit transactionId error and return status is '. $cashResult->status . ' idToken is ' . $tokenId);
            return false;
        }

        //        duko返回的交易id
        $remitTransaction = $cashResult->remit->transactionId;
//        $paramArr = ['duko_transaction' => $remitTransaction];
//        LoanPaymentFlow::saveOne($transaction, $paramArr);

//        查询交易的状态
        $checkResult = $this->checkTransation($transaction,$remitTransaction);
//        dd($inqueryResult, $cashResult, $checkResult);
        return $checkResult;
    }

    /**
     * 先去服务端进行备案
     * @return mixed|string
     */

    private function inqueryService($amount, $transaction, $cardNumber, $bankCode, $bankId, $bankName, $clientName)
    {
        $url = env('DOKU_PAYMENT')
        .'/cashin/inquiry';
        $header = $this->handleHeaderData($transaction);

        $data = [
            'senderCountry.Code'                    => 'ID',
            'senderCurrency.Code'                   => 'IDR',
            'beneficiaryCountry.Code'               => 'ID',
            'beneficiaryCurrency.Code'              => 'IDR',
            'channel.Code'                          => '07',
            'senderAmount'                          => $amount,
            'beneficiaryAccount.bank.code'          => $bankCode,
            'beneficiaryAccount.bank.countryCode'   => 'ID',
            'beneficiaryAccount.bank.id'            => $bankId,
            'beneficiaryAccount.bank.name'          => $bankName,
            'beneficiaryAccount.city'               => 'Jakarta',
            'beneficiaryAccount.name'               => $clientName,
            'beneficiaryAccount.number'             => $cardNumber
        ];

//        dd($url, $header, $data);
//        var_dump($header);
        $data = http_build_query($data);
        Log::info('inquery date is ' . $data);
        $result = $this->requestPostMethod($url,$header, $data);
//        dd($result);
        return $result;
    }

    /**
     * 根据之前的备案的请求token,去放款
     * @param $amount
     * @param $city
     * @param $cardNumber
     * @param $uid
     * @param $bankCode
     * @param $bankId
     * @param $bankName
     * @param $clientName
     * @param $transaction
     * @param $tokenId
     * @return mixed
     */
    private function remitLoanToUser($amount,  $cardNumber, $bankCode, $bankId, $bankName, $clientName,$transaction,$tokenId,$phone)
    {
//        dd($amount);
//        $amount = 100000.00;
        $url = env('DOKU_PAYMENT')
            .'/cashin/remit';
//        dd($url);
        $header = $this->handleHeaderData($transaction);
//        dd($header);

        $data = [
//            'sendType' => 'senderAmount',
            'inquiry.IdToken'                       => $tokenId,
            'channel.code'                          => '07',
            'senderCountry.code'                    => 'ID',
            'senderCurrency.code'                   => 'IDR',
            'beneficiaryCountry.code'               => 'ID',
            'beneficiaryCurrency.code'              => 'IDR',
            'beneficiaryCity'                       => 'Jakarta',
            'senderAmount'                          => $amount,
            'senderNote'                            => ' ',
            'sender.country.code'                   => 'ID',
            'sender.firstName'                      => 'Mini',
            'sender.lastName'                       => 'Rupiah',
            'sender.phoneNumber'                    => '81291029000',
            'sender.birthDate'                      => '2018-09-09',
            'sender.gender'                         => 'FEMALE',
            'sender.address'                        => 'Jakarta',
            'sender.personalIdType'                 => 'CITIZENID',
            'sender.personalId'                     => '1673060101880007',
            'sender.personalIdCountry.code'         => 'ID',
//            'sender.IdToken'                        => $uid,
//            'beneficiary.IdToken'                   => $uid,
            'beneficiaryAccount.bank.code'          => $bankCode,
            'beneficiaryAccount.number'             => $cardNumber,
            'beneficiaryAccount.bank.name'          => $bankName,
            'beneficiaryAccount.bank.id'            => $bankId,
            'beneficiaryAccount.name'               => $clientName,
            'beneficiaryAccount.bank.countryCode'   => 'ID',
            'beneficiaryAccount.address'            => 'Jakarta',
            'beneficiaryAccount.city'               => 'Jakarta',
            'beneficiary.country.code'              => 'ID',
            'beneficiary.firstName'                 => 'hhaha',
            'beneficiary.lastName'                  => 'hehe',
            'beneficiary.address'                   => 'Jakarta',
            'beneficiary.phoneNumber'               => $phone
        ];
//        dd($data);
//        print_r($data);
        $data1 = http_build_query($data);
        Log::info('remit data is ' . $data1);
//        $data = json_encode($data);
        $result = $this->requestPostMethod($url,$header, $data1);
//        dd($data, $result, $header);
        return $result;


    }

    private function checkTransation($transaction,$remitTransaction)
    {
        $url = env('DOKU_PAYMENT')
            .'/transaction/info';
        $key = env('DOKU_KEY');
       $header = $this->handleHeaderData($transaction);
        $token  = base64_encode(openssl_encrypt($transaction, 'AES-128-ECB', env('DUKO_ENCRYPY_KEY'), OPENSSL_RAW_DATA));
        $data = [
                'agentKey' => $key,
                'requestId' => $transaction,
                'wordsToken' => $token,
                'transactionId' => $remitTransaction
           ];
        $data = http_build_query($data);
        Log::info('check transaction data is ' . $data);
        $result = $this->requestPostMethod($url,$header, $data);
        return $result;

    }


}
