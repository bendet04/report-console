<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/9/13
 * Time: 14:01
 */

namespace App\Service\Pay;

use App\Service\Foundation\BankPayment;
use App\Service\Foundation\BluePay;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class PaymentFactory
{
    public function getInstance($type)
    {
        switch ($type){
            case 'duko' :
                return new DokuService();
                break;
            case 'bluepay':
                return new BankPayment(new BluePay());
                break;
            default:
                throw  new FileException('file not find');
        }
    }
}