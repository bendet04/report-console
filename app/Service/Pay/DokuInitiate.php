<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/8/17/017
 * Time: 11:48
 */

namespace App\Service\Pay;


class DokuInitiate
{
    const prePaymentUrl = 'https://staging.doku.com/api/payment/PrePayment';
    const paymentUrl = 'https://staging.doku.com/api/payment/paymentMip';
    const directPaymentUrl = 'https://staging.doku.com/api/payment/PaymentMIPDirect';
    const generateCodeUrl = 'https://staging.doku.com/api/payment/DoGeneratePaycodeVA';

    public static $sharedKey; //doku's merchant unique key
    public static $mallId; //doku's merchant id
}