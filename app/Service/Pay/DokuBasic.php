<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/8/16/016
 * Time: 13:43
 */

namespace App\Service\Pay;
use Illuminate\Support\Facades\Log;

abstract class DokuBasic
{
    protected $mallId ;
    protected $key;
    protected $url;
    protected $transation;


    /**
     * 生成加密的信息
     * @param $amount
     */

    private function generateEncrypt($amount)
    {
        $encrypt = $amount
            . $this->mallId
            . $this->key
            . $this->transation;

        return sha1($encrypt);
    }

    /**

     * @param $transationId
     * @return array
     */
    public function handleHeaderData($transationId)
    {
        $key = env('DOKU_PAYMENT_KEY');
//        $requestId = 'c6d33034-d374-1004-82be-58419227re92';
        $requestId = $transationId;
        $encypt = $key . $requestId;
//        $encypt = Crypt::encrypt($encypt, env('DUKO_REQUEST_ID'),'AES-128-ECB' );

        $signature = base64_encode(openssl_encrypt($encypt, 'AES-128-ECB', env('DUKO_ENCRYPY_KEY'), OPENSSL_RAW_DATA));
//
//        $agentKey = 'A41552';
//        $requstId = '2b03c404-d633-4b43-8bb9-61813f814b1f';
//        $data = $agentKey.$requstId;
//        echo base64_encode(openssl_encrypt($data, 'AES-128-ECB', $key, OPENSSL_RAW_DATA));

//        dd($url,$key, $requestId, $signature,strlen($signature));
        $header = [
            'agentKey:'. $key,
            'requestId:'.$requestId,
            'signature:'. $signature
        ];
        return $header;
    }

    /**
     * 生成交易id
     * @return string
     */
    public function generateTransationId()
    {
        $number = "";
        for($i = 0; $i < 4; $i++){
            $chr = rand(0,3);
            $str = "";
            for($j = 0; $j < 4; $j ++){
                if($j == $chr) {
                    $str .= chr(rand(65, 90));
                } else {
                    $str .= rand(0,9);
                }
            }
            $number .= (empty($number) ? $str : "-".$str);
        }

        return $number;
    }

    /**
     *      post请求方式
     * @param $url
     * @param $data
     */

    public function requestPostMethod($url,$header, $data)
    {
//        dd($url, $header,$data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$data);

//        curl_setopt( $curl, CURLOPT_POSTFIELDS, $data));

        # do not verify SSL cert
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $errno = curl_errno($curl);
        $error = curl_error($curl);
        Log::info('errno ' . $errno . 'error ' .$error);

        $data = curl_exec($curl);
        curl_close($curl);
        Log::info('return is '. $data);

        if(is_string($data)){
            return json_decode($data);
        }else{
            return $data;
        }

    }

    public function requestMethos($url,$data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$data);

//        curl_setopt( $curl, CURLOPT_POSTFIELDS, $data));

        # do not verify SSL cert
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $errno = curl_errno($curl);
        $error = curl_error($curl);
        Log::info('errno ' . $errno . 'error ' .$error);

        $data = curl_exec($curl);
        curl_close($curl);
        Log::info('return is '. $data);
        if(is_string($data)){
            return json_decode($data);
        }else{
            return $data;
        }
    }
}