<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static getInstance(string $status)
 */
class Examine extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\Examine';
    }
}
