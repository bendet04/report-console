<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/7/007
 * Time: 15:02
 */

namespace App\Service\Order;

use App\Consts\LoanDay;
use App\Consts\OrderStatus;
use App\Consts\PayType;
use App\Consts\TrialResult;
use App\Models\AdminUser;
use App\Models\LoanOrder;
use App\Models\LoanPaymentFlow;
use App\Models\LoanRePaymentFlow;
use App\Models\OrderUserBasicInfo;
use App\Models\RefuseDelayRecord;

use App\Service\DateUnit;
use App\Service\Rate\RateFactory;

class OrderDetail extends Order
{
    protected $order_status = [1,2];

    public function getOrderList($paramArr = [], $orderBy='loan_order.order_time', $pageNum = 20)
    {
        $options = [
            'order_status' => $this->order_status,
        ];

        $options = array_merge($options, array_filter($paramArr));

        $query = LoanOrder::select(['loan_order.id', 'order_number', 'loan_order.platform', 'loan_order.uid', 'name', 'ktp_number', 'phone_number', 'loan_period', 'apply_amount', 'order_time', 'order_status']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');

        if (!empty($options['repayment_status'])) {
            unset($options['order_status']);
        }
        foreach ($options as $column=>$value) {
            if (is_array($value)) {
                $query->whereNotIn($column, $value);
            } else if ($column=='platform'){
                $query->where('loan_order.'.$column,$value);
            } else if($column=='repayment_status') {
                if ($value==1) {//全部正常还款且还款3次以上
                    $query->where('order_status', 12)->groupBy('loan_order.uid')->havingRaw('count("loan_order.uid") > 3 AND max(overdue_amount)=0');
                } elseif($value==2) {//还款3次以上但有逾期
                    $query->where('order_status', 12)->groupBy('loan_order.uid')->havingRaw('count("loan_order.uid") > 3');
                    $query->whereNotIn('loan_order.uid', function($q){
                        $q->select('uid')->from('loan_order')->where('order_status', 12)->groupBy('uid')->havingRaw('count("uid") > 3 AND max(overdue_amount)=0')->get();
                    });
                } elseif($value==3) {//全部正常还款且还款1-3次
                    $query->where('order_status', 12)->groupBy('loan_order.uid')->havingRaw('count("loan_order.uid") >= 1 AND count("loan_order.uid") <= 3  AND max(overdue_amount)=0');
                } else {//还款1-3次但有逾期
                    $query->where('order_status', 12)->groupBy('loan_order.uid')->havingRaw('count("loan_order.uid") >= 1')->havingRaw('count("loan_order.uid") <= 3');
                    $query->whereNotIn('loan_order.uid', function($q){
                        $q->select('uid')->from('loan_order')->where('order_status', 12)->groupBy('uid')->havingRaw('count("loan_order.uid") >= 1 AND count("loan_order.uid") <= 3  AND max(overdue_amount)=0')->get();
                    });
                }
            } else {
                $query->where($column, 'like', "%$value%");
            }
        }

        if (!empty($options['repayment_status'])) {
            $query->orderByRaw('count("loan_order.uid") desc');
        } else {
            $query->orderByDesc($orderBy);
        }
        $orders = $query->orderByDesc($orderBy)->paginate($pageNum);
//        $orders = $query->toSql();
//        dd($orders);

        //格式化数据
        $orders = $this->formatorder($orders);

        return $orders;

    }
    public function getOrderDetail($id)
    {
        $order = LoanOrder::select(['loan_order.pid','loan_order.id','basic_id','pay_type', 'loan_period', 'apply_amount', 'order_time', 'order_status', 'payment_date', 'loan_deadline', 'loan_suspend_date', 'loan_overdue_date', 'loan_repayment_date', 'order_number','loan_charge_amount','already_repayment_amount', 'reciprocal_amount', 'overdue_amount', 'loan_order.platform', 'ktp_number']);
        $order = $order->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $order = $order->where('loan_order.id', $id)->first();

        $order['user'] = OrderUserBasicInfo::getOne($order['basic_id']);

        $order = $this->formatSingle($order);
        return $order;
    }
    public function getMessageTrialByOrderId($order_id)
    {
        $user = AdminUser::select(['admin_username', 'loan_trial_info.trial_time', 'status']);
        $user = $user->join('loan_trial_info', 'loan_trial_info.admin_id', '=', 'admin_user.id');
        $user = $user->where('loan_trial_info.order_id', $order_id)->first();

        if($user){
            $user['status'] = TrialResult::toString($user['status']);
        }
        return $user;
    }
    public function getPhoneTrialByOrderId($order_id)
    {
        $user = AdminUser::select(['admin_username', 'loan_trial_phone.trial_time', 'status']);
        $user = $user->join('loan_trial_phone', 'loan_trial_phone.admin_id', '=', 'admin_user.id');
        $user = $user->where('loan_trial_phone.order_id', $order_id)->first();

        if($user){
            $user['status'] = TrialResult::toString($user['status']);
        }
        return $user;
    }
    public function getFinalTrialByOrderId($order_id)
    {
        $user = AdminUser::select(['admin_username', 'loan_trial_final.trial_time', 'status']);
        $user = $user->join('loan_trial_final', 'loan_trial_final.admin_id', '=', 'admin_user.id');
        $user = $user->where('loan_trial_final.order_id', $order_id)->first();

        if($user){
            $user['status'] = TrialResult::toString($user['status']);
        }
        return $user;
    }
    public function paymentDetail($order_id)
    {
        $bankInfo = LoanOrder::select(['loan_order.id', 'payee_name', 'bank_name', 'bank_phone', 'bank_number']);
        $bankInfo = $bankInfo->join('order_user_basic_info', 'order_user_basic_info.id' , '=', 'loan_order.basic_id');
        $bankInfo = $bankInfo->where('loan_order.id', $order_id)->first();

        return $bankInfo;
    }

    public function paymentFlow($order_id)
    {
        return LoanPaymentFlow::getList($order_id);
    }
    public function repaymentFlow($order_id)
    {
        $bankInfo = LoanRePaymentFlow::select(['repayment_amount', 'loan_repayment_flow.created_at','repayment_flow', 'repayment_code', 'repayment_status','code', 'repayment_time', 'user_bank_info.bank_number', 'bank_name', 'loan_repayment_flow.type']);
        $bankInfo = $bankInfo->join('user_bank_info', 'user_bank_info.transaction_id' , '=', 'loan_repayment_flow.repayment_flow');
        $bankInfo = $bankInfo->where('loan_repayment_flow.order_id', $order_id)->where('user_bank_info.type', 1)->get();

        return $bankInfo;
    }

    public function calculateAmount($order)
    {
        $calculateDetail = RateFactory::getInstance('normalRate', $order['apply_amount'],$order['pid']);
        $info['info'] = $calculateDetail['fornula'];
        $info['day'] = 0;
        $info['amount'] = intval($calculateDetail['amount']);
        if(!in_array($order['order_status'],['11','13']))
            return $info;

        $day = DateUnit::timeToDay($order['loan_overdue_date']);
        $day = $day + 1;
        $info['day'] = $day;
        if($day >= 1 && $day < 4){
            $rateDetail = RateFactory::getInstance('firstRate', $order['apply_amount'],$order['pid'], $order['loan_overdue_date']);
            $info['info'] = $rateDetail['fornula'];
            $info['amount'] = intval($rateDetail['amount']);

        }
        if($day >= 4 && $day < 8){
            $rateDetail = RateFactory::getInstance('secondRate', $order['apply_amount'],$order['pid'], $order['loan_overdue_date']);
            $info['info'] = $rateDetail['fornula'];
            $info['amount'] = intval($rateDetail['amount']);

        }
        if($day > 7 && $day < 15){
            $rateDetail = RateFactory::getInstance('thirdStage', $order['apply_amount'],$order['pid'], $order['loan_overdue_date']);
            $info['info'] = $rateDetail['fornula'];
            $info['amount'] = intval($rateDetail['amount']);

        }
        if($day >= 15 && $day < 22){
            $rateDetail = RateFactory::getInstance('forthStage', $order['apply_amount'],$order['pid'],$order['loan_overdue_date']);
            $info['info'] = $rateDetail['fornula'];
            $info['amount'] = intval($rateDetail['amount']);

        }
        if($day >= 22){
            $rateDetail = RateFactory::getInstance('fifthStage', $order['apply_amount'],$order['pid']);
            $info['info'] = $rateDetail['fornula'];
            $info['amount'] = intval($rateDetail['amount']);

        }

        return $info;
    }
    public function orderIsInDelayRefuseList($order_id)
    {

//        $order = OrderRefuseTime::getOrderByKtpNumber($ktp_number);
//        $period = $order->refuse_time;
//
//        $day = DateUnit::timeToDay($period);
//        if($day && $day <= 2){
//            return true;
//        }
//        return false;
        return RefuseDelayRecord::getOne($order_id);

    }


    public function formatorder($orders)
    {
        $order['date'] = [];
        $orders->map(function($order) {
            $order['order_status_str'] = OrderStatus::toString($order['order_status']);
            $order['loan_period'] = LoanDay::toString($order['loan_period']);
            
            return $order;
        });

        return $orders;
    }

    public function formatSingle($order)
    {
        $order['date'] = [];
        $order['order_status_str'] = OrderStatus::toString($order['order_status']);
        $order['loan_period'] = LoanDay::toString($order['loan_period']);
        $order['pay_type'] = PayType::toString($order['pay_type']);

        if ($order['order_status'] == 16){
            $info = $this->orderIsInDelayRefuseList($order['id']);
            if($info){
                $order['order_status_str'] = '已被拒绝';
            }
        }

        if($order['payment_date']){
            $order['date'] = [
                'payment' => $order['payment_date'],
                'suspend' => $order['loan_suspend_date'],
                'overDue' => $order['loan_overdue_date']
            ];
        }

        $order['trial'] = [
            'messageTrial' => $this->getMessageTrialByOrderId($order['id']),
            'phoneTrial'   => $this->getPhoneTrialByOrderId($order['id']),
            'finalTrial'   => $this->getFinalTrialByOrderId($order['id'])
        ];

        $order['payment'] = [
            'payment'     => $this->paymentDetail($order['id']),
            'paymentFlow' => $this->paymentFlow($order['id']),
        ];

        $order['repayment'] = [
            'repaymentFlow' => $this->repaymentFlow($order['id']),
        ];

        $order['overDue'] = [
            'dueInfo' => $this->calculateAmount($order)
        ];

        return $order;

    }
}