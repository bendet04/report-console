<?php

namespace App\Service\Order;

use App\Consts\Level;
use App\Events\InfoAfter;
use App\Models\AdminCheckUser;
use App\Models\LoanOrder;
use App\Models\LoanTrialInfo;
use App\Models\LoanTrialInfoRecord;
use App\Models\OrderRefuseTime;
use App\Models\OrderStatus;
use App\Models\OrderUserBasicInfo;
use App\Models\PartnerAccountInfo;
use App\Models\UserExtraImage;
use App\Models\UserPartnerAccount;
use App\Service\Assign;
use App\Service\Contracts\Order\Trial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TrialInfoOrder extends Order implements Trial
{
    public $order_status = 17;
    
    /**
     * 获取信审订单列表
     * @param array $paramArr
     * @param string $orderBy
     * @param int $pageNum
     * @return array
     */
    public function getOrderList($paramArr = [], $orderBy = 'loan_trial_info.updated_at', $pageNum = 20)
    {
        $options = [
            'order_status' => $this->order_status,
        ];
        $options = array_merge($options, array_filter($paramArr));

        //获取信审订单列表
        $query = LoanOrder::select([
            'loan_order.*',
            'order_user_basic_info.video_url',
            'order_user_basic_info.work_image_url',
            'order_user_basic_info.ktp_image_url',
            'order_user_basic_info.ktp_number',
            'order_user_basic_info.name',
            'order_user_basic_info.ktp_number',
            'order_user_basic_info.phone_number',
            'order_user_basic_info.income_level',
            'loan_trial_info.admin_id',
            ]);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->trialinfo()->where($options);

        if (is_string($orderBy)) {
            $query->orderByDesc($orderBy);
        } else {
            list($orderColumn, $sort) = $orderBy;
            $query->orderBy($orderColumn, $sort);
        }
        
        $orders = $query->paginate($pageNum);

        //格式化数据
        $orders = $this->formatOrder($orders);

        return $orders;
    }
    
    public function formatOrder($orders, $getSameOrder = 1)
    {
        $orders->map(function ($order) use ($getSameOrder) {
            $order['video_url'] = env('CDN_URL').$order['video_url'];
            $order['work_image_url'] = env('CDN_URL').$order['work_image_url'];
            $order['ktp_image_url'] = env('CDN_URL').$order['ktp_image_url'];
            
            //获取ktp校验状态
            $ktp = AdminCheckUser::getOne($order['uid']);
            if ($ktp && $ktp['data_status']) {
                $order['data_status'] = $ktp['data_status'];
            }
        
            //npwp税卡卡号及照片
            $npwp = UserExtraImage::getOne($order['uid']);
            if ($npwp) {
                $order['tax_card_number'] = $npwp->tax_card_number;
                $order['tax_card_image_url'] = empty($npwp->tax_card_image_url) ? '' : env('CDN_URL').$npwp->tax_card_image_url;
                $order['salary_image_url'] = empty($npwp->salary_image_url) ? '' : env('CDN_URL').$npwp->salary_image_url;
            }
        
            //npwp认证状态
            $partner = UserPartnerAccount::getOne($order['uid']);
        
            if ($partner && $partner['npwp']) {
                $npwp = PartnerAccountInfo::getOne($partner['npwp'], 7);
                if ($npwp) {
                    $info = json_decode($npwp['info'], true);
                    if (isset($info['profile']['npwp'])) {
                        $order['npwp'] = $info['profile']['npwp'];
                    }
                }
            }
    
            $order['income_level'] = empty($order['income_level']) ? '' : Level::toString($order['income_level']);
            $order['order_status_str'] = \App\Consts\OrderStatus::toString($order['order_status']);
    
            if ($getSameOrder) {
                $order['sameNameOrder'] = $this->getSameNameOrder($order['name'], $order['id']);
            }
            return $order;
        });
        
        return $orders;
    }
    
    /**
     * 获取同名订单
     * @param $name
     * @param int $formatData
     * @return mixed
     */
    public function getSameNameOrder($name, $order_id, $formatData = 0)
    {
        $query = LoanOrder::select([
            'loan_order.*',
            'order_user_basic_info.video_url',
            'order_user_basic_info.work_image_url',
            'order_user_basic_info.ktp_image_url',
            'order_user_basic_info.ktp_number',
            'order_user_basic_info.name',
            'order_user_basic_info.ktp_number',
            'order_user_basic_info.phone_number',
            'order_user_basic_info.income_level',
        ]);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where('order_user_basic_info.name', '=', $name);
        $query->whereIn('loan_order.order_status', [3, 6, 8, 11 ,13, 14, 15, 17, 19, 21, 25]);
        $query->where('loan_order.id', '!=', $order_id);
        $orders = $query->orderByDesc('loan_order.id')->get();
        
        if ($formatData) {
            $orders = $this->formatOrder($orders, 0);
        }
        
        return $orders;
    }
    
    /**
     * 获取同KTP订单
     * @param $name
     * @param int $formatData
     * @return mixed
     */
    public function getSameKtpOrder($basicIds, $order_id, $formatData = 0)
    {
        $query = LoanOrder::select([
            'loan_order.*',
            'order_user_basic_info.video_url',
            'order_user_basic_info.work_image_url',
            'order_user_basic_info.ktp_image_url',
            'order_user_basic_info.ktp_number',
            'order_user_basic_info.name',
            'order_user_basic_info.ktp_number',
            'order_user_basic_info.phone_number',
            'order_user_basic_info.income_level',
        ]);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->whereIn('order_user_basic_info.id', $basicIds);
        $query->whereIn('loan_order.order_status', [3, 6, 8, 11 ,13, 14, 15, 17, 19, 21, 25]);
        $query->where('loan_order.id', '!=', $order_id);
        $orders = $query->orderByDesc('loan_order.id')->get();
        
        if ($formatData) {
            $orders = $this->formatOrder($orders, 0);
        }
        
        return $orders;
    }
    
    /**
     * 信审审核
     * @param array $paramArr
     * $paramArr = [
     *     'order_id' => 100,
     *     'pic_status' => 1, //1一致 2不一致
     *     'npwp_status' => 1, //1一致 2不一致
     *     'npwp' => '',
     * ];
     * @return boolean
     */
    public function trial($paramArr = [])
    {
        $order_id = $paramArr['order_id'];
        //计算初审状态
        $status = $this->getStatus($paramArr, $order_id);

        //检查订单状态，防止重复审核
        $order = LoanOrder::getOne($order_id);
        if (!$order || $order->order_status!=17) {
            $data['code']= 11;
            $data['info']= 'Audit result: this list has been audited by other personnel';
            return $data;
        }

        $paramArr['img_status']=implode(',', $paramArr['img_status']);
        $paramArr=array_filter($paramArr);
        
        //更新审核记录表
        LoanTrialInfoRecord::saveOne($order_id, $paramArr);
        


//      记录拒绝审核的时间
//        if($status == 20){
//            $this->recordRefuseTime($order_id);
//        }
        
        //不是因为照片模糊拒绝，加入七天拒绝
        if (isset($status['time'])) {
            $orderInfo = OrderUserBasicInfo::getOne($order->basic_id);
            $arr['ktp_number'] = $orderInfo->ktp_number;
            $arr['refuse_time'] = date('Y-m-d H:i:s', time());
            $ktp['ktp_number'] = $orderInfo->ktp_number;
            OrderRefuseTime::saveOne($ktp, $arr);
        }
       
        
        //更新审核表
        LoanTrialInfo::saveData($order_id, [
            'status' => $status['status'],
            'trial_time' => date('Y-m-d H:i:s')
        ]);
        
        //更新订单状态
        $this->updateOrderStatus($order_id, $status['status']);
    
        //信审完成后，触发相应事件
        event(new InfoAfter($order_id, $status['status']));
    
        return true;
    }

    /**
     * 根据图片/npwp结果，计算信审状态(19信审成功 20信审失败)
     * @param $paramArr
     * $paramArr = [
     *     'order_id' => 100,
     *     'pic_status' => 1, //1一致 2不一致
     *     'npwp_status' => 1, //1一致 2不一致
     *     'salary_status' => 1, //1有效 2无效
     *     'npwp' => '',
     * ];
     * @return int
     */
    public function getStatus($paramArr, $orderId)
    {
        $status = 0;
        if (count($paramArr['img_status']) > 0) {
            $status = 20;
          
            return ['status'=>$status];
        }        
        if ($paramArr['pic_status'] == 1 && $paramArr['npwp_status'] == 1) {
            return ['status' => 19];
        } elseif ($paramArr['pic_status'] == 1 && $paramArr['npwp_status'] == 2) {
            //当“视频&KTP”选择一致，“工作照、工资条、NPWP”选择无效时
            //校验用户是否已认证“NPWP或者BPJS”，如已认证任意一个
            //则为审核通过进入电审，如未认证，按现有逻辑，直接拒绝。
            $order = LoanOrder::getOne($paramArr['order_id']);
            $partnerAccount = UserPartnerAccount::getOne($order->uid);
            if (!empty($partnerAccount->bpjs) || !empty($partnerAccount->npwp)) {
                return ['status'=>19];
            } else {
                return ['status'=>20, 'time'=>'day'];
            }
        } else {
            return ['status'=>20, 'time'=>'day'];
        }
    }
}