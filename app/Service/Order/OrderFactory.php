<?php

namespace App\Service\Order;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class OrderFactory
{
    public function getInstance($status)
    {
        switch ($status) {
            case 'trialInfo': //信审
                return new TrialInfoOrder();
                break;
            case 'trialPhone': //电审
                return new TrialPhoneOrder();
                break;
            case 'trialFinal': //终审
                return new TrialFinalOrder();
                break;
            case 'payment': //付款
                return new PaymentOrder();
                break;
            case 'repayment': //还款
                return new RePaymentOrder();
                break;
            case 'track': //催收
                return new TrackOrder();
                break;
            case 'orderDetail': //贷款查询
                return new OrderDetail();
                break;
            default:
                throw new FileException('File not found');
        }
    }
}