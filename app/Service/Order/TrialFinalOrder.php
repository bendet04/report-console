<?php

namespace App\Service\Order;

use App\Consts\LoanDay;
use App\Consts\OrderStatus;
use App\Events\FinalAfter;
use App\Models\LoanOrder;
use App\Models\LoanTrialFinal;
use App\Models\OrderUserBasicInfo;
use App\Models\UserExtraImage;
use App\Service\Contracts\Order\Trial;
use Carbon\Carbon;

class TrialFinalOrder extends Order implements Trial
{
    public $order_status = [21, 6, 5];//电审通过待终审
    
    /**
     * 获取终审订单列表
     * @param array $paramArr
     * @param string $orderBy
     * @param int $pageNum
     * @return mixed
     */
    public function getOrderList($paramArr = [], $orderBy = 'loan_order.id', $pageNum = 20)
    {
        $options = [
            'order_status' => $this->order_status,
        ];
        $options = array_merge($options, array_filter($paramArr));
        
    
        //获取信审订单列表
        $query = LoanOrder::select(['loan_order.*', 'order_user_basic_info.name', 'order_user_basic_info.ktp_number', 'order_user_basic_info.phone_number', 'loan_trial_final.admin_id', 'risk_control_record.risk_score']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->join('risk_control_record', 'risk_control_record.order_id', '=', 'loan_order.id', 'left');
        
        //新老用户特殊处理
        if(!empty($options['userType'])){
            $userType = $options['userType'];
            unset($options['userType']);
            
            if ($userType==1){
                $query->whereDoesntHave('order', function($q) {
                    return $q->where('order_status', '=', 12);
                });
            }else{
                $query->whereHas('order', function($q) {
                    return $q->where('order_status', '=', 12);
                });
            }
        }
    
        //距离发薪日
        if (!empty($options['paidperiod'])){
            $paidperiod = $options['paidperiod'];
            unset($options['paidperiod']);
            if ($paidperiod==1){
                $query->where('order_user_basic_info.paid_datetime', '0');
            } else {
                $query->whereIn('order_user_basic_info.paid_datetime', ['-3', '-2', '-1', '1', '2', '3']);
            }
        }
    
        
        //查询条件
        foreach ($options as $column=>$value) {
            if (is_array($value)) {
                $query->whereIn($column, $value);
            } elseif(is_numeric($value)){
                $query->where($column, '=', $value);
            }else{
                $query->where($column, 'like', "%$value%");
            }
        }
        
        $orders = $query->trialfinal()->orderByDesc($orderBy)->paginate($pageNum);
//        $orders = $query->trialfinal()->toSql();
//        dd($orders);
        
        //格式化订单数据
        $orders = $this->formatorder($orders);
        
        return $orders;
    }
    
    /**
     * 格式化订单数据
     * @param $orders
     * @return mixed
     */
    public function formatorder($orders)
    {
        $orders->map(function($order) {
            $order['order_status_str'] = OrderStatus::toString($order['order_status']);
            $order['loan_period'] = LoanDay::toString($order['loan_period']);
            return $order;
        });
        
        return $orders;
    }
    
    /**
     * 终审审核
     * @param array $paramArr
     * $paramArr = [
     *      'order_id' => 1,
     *      'admin_id' => 1,
     *      'status'   => 1,//1审核通过 2拒绝
     *      'remark'   => '审核总结',
     * ]
     * @return bool
     */
    public function Trial($paramArr = [])
    {
        $order_id = $paramArr['order_id'];
        $trial_status = $paramArr['status'];
        
        //检查订单状态，防止重复审核
        $order = LoanOrder::getOne($order_id);
        if (!$order || $order->order_status!=21) {
            return json_encode(['code' => 11, 'info' => '审核结果：此单已被其他人员审核完成']);
        }
        
        //更新审核记录表
        $paramArr['trial_time'] = Carbon::now();
        LoanTrialFinal::saveOne($order_id, $paramArr);
        
        //更新订单状态(5:终审失败 6:终审成功)
        $status = $trial_status==1 ? 6 : 5;
        $this->updateOrderStatus($order_id, $status);
        
        if ($status==5){
            //记录拒绝时间
            $this->recordRefuseTime($order->basic_id);
            event(new FinalAfter($order_id));
            return json_encode(['code' => 1, 'info' => '审核成功']);
        }
        
        //放款
        $payment = \App\Service\OrderFactory::getInstance('payment');
        
        //终审完成后，触发相对事件
        event(new FinalAfter($order_id));
        
        return $payment->payment($order_id);
    }
    
    /**
     * 证件照审核
     * @param $order_id
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getIdPhoto($order_id)
    {
        $query = LoanOrder::select(['loan_order.id', 'order_number', 'loan_order.uid', 'video_url', 'work_image_url', 'ktp_image_url']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $order = $query->where('loan_order.id', $order_id)->first();
    
        $order['video_url'] = env('CDN_URL').$order['video_url'];
        $order['work_image_url'] = env('CDN_URL').$order['work_image_url'];
        $order['ktp_image_url'] = env('CDN_URL').$order['ktp_image_url'];
    
        //npwp照片
        $npwp = UserExtraImage::getOne($order['uid']);
        if ($npwp) {
            $order['tax_card_number'] = $npwp->tax_card_number;
            $order['tax_card_image_url'] = empty($npwp->tax_card_image_url) ? '' :env('CDN_URL').$npwp->tax_card_image_url;
            $order['salary_image_url'] = empty($npwp->salary_image_url) ? '' : env('CDN_URL').$npwp->salary_image_url;//工资条
            $order['credit_card'] = empty($npwp->credit_card) ? '' : env('CDN_URL').$npwp->credit_card;//信用卡
            $order['social_card_image_url'] = empty($npwp->social_card_image_url) ? '' : env('CDN_URL').$npwp->social_card_image_url;//社保卡
            $order['driving_license_image_url'] = empty($npwp->driving_license_image_url) ? '' : env('CDN_URL').$npwp->driving_license_image_url;//驾驶证
            $order['living_fee_image_url'] = empty($npwp->living_fee_image_url) ? '' : env('CDN_URL').$npwp->living_fee_image_url;//水电煤缴费单
            $order['rent_image_url'] = empty($npwp->rent_image_url) ? '' :env('CDN_URL').$npwp->rent_image_url;//租房合同
            $order['bank_flow_image_url'] = empty($npwp->bank_flow_image_url) ? '' : env('CDN_URL').$npwp->bank_flow_image_url;//其他贷款的正常还款流水
        }
        
        return $order;
    }
    
    /**
     * 计算并更新用户发薪日
     * @return int
     */
    public function updatePaidPeriod()
    {
        $atDate = date('Y-m-d 00:00:00', strtotime('-3 day'));
        
        $query = LoanOrder::select(['loan_order.loan_period','order_user_basic_info.id', 'order_user_basic_info.paid_period']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where('loan_order.order_status', 21);
//        $query->where(function($query) use ($atDate) {
//            $query->where('order_user_basic_info.paid_datetime', '');
//            $query->orWhere('order_user_basic_info.paid_datetime', '<', $atDate);
//        });
        $query->where('order_user_basic_info.paid_period', 'like', 'Bulanan%');
        $order = $query->get();
        
        foreach ($order as $k=>$v) {
            $date = $this->getPaidDate($v->paid_period, $v->loan_period);
            OrderUserBasicInfo::updateOne($v->id, ['paid_datetime' => $date]);
        }
        
        return 1;
    }
    
    public function getPaidDate($paidPeriod, $loan_period)
    {
        if (empty($paidPeriod))
            return '';
        
        $arr = explode(' ', $paidPeriod);
        
        if (empty($arr))
            return '';
        
        $deadline = LoanDay::toString($loan_period);
        $min_deadline = $deadline-3;
        $deadlineTime = strtotime("+$deadline day");
        
        $userDay = array_pop($arr);
        $strUserDay =  $userDay<9 ? '0'.$userDay : $userDay;
        $userDayTime = date("Y-m-$strUserDay 00:00:00");
        
        $day = date('Y-m-d 00:00:00', strtotime("+$min_deadline day"));
        if ($day > $userDayTime) {
            $ret = date("Y-m-$strUserDay 01:00:00", strtotime('+1 month'));
            if ($day > $ret) {
                $ret = date("Y-m-$strUserDay 01:00:00", strtotime('+2 month'));
            }
        } else {
            $ret = date("Y-m-$strUserDay 01:00:00");
        }
    
        $time1 = strtotime(date('Y-m-d', strtotime($ret)));
        $time2 = strtotime(date('Y-m-d', $deadlineTime));
        $diffDay = ($time1-$time2)/86400;
        
        return $diffDay;
    }
}