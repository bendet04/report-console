<?php

namespace App\Service\Order;

use App\Consts\InvestmentStatus;
use App\Consts\Platform;
use App\Models\LoanOrder;
use App\Models\LoanPaymentFlow;
use App\Models\OrderUserBasicInfo;
use App\Models\Investment_user;
use App\Service\BankPayment;
use App\Consts\LoanDay;
use App\Models\BankInfo;
use App\Models\Product;
use App\Models\PushToken;
use App\Service\OverMessage;
use App\Service\PaymentFactory;
use App\Service\PushSevice;
use Illuminate\Support\Facades\Log;

class PaymentOrder extends Order
{
    public function payment($order_id,$type='')
    {
        //生成放款交易id
        $transactionId = $this->generaterderNumber();
        $channel = env('PAYMENT_CHANNEL', 'bluepay');
        
        //开始放款
        if ($type=='investment') {
            return $this->investmentPayment($order_id, $channel, $transactionId);
        } else {
            return $this->loanPayment($order_id, $channel, $transactionId);
        }
    }
    
    /**
     * @param $order_id
     * @param $channel
     * @param $transactionId
     * @return bool|string
     */
    public function loanPayment($order_id, $channel, $transactionId)
    {
        //检查订单状态
        $orderStatus = [3, 6, 15]; //可进行放款的状态
        $order = LoanOrder::getOne($order_id);
        if (!in_array($order->order_status, $orderStatus)) {
            return json_encode(['code' => 2, 'info' => '该订单已放款']);
        }

        $forceDoku = Platform::doku($order->platform);
        if ($forceDoku)
            $channel = $forceDoku;

        //放款流水
        $paymentFlow = [
            'order_id' => $order_id,
            'payment_amount' => $order['reciprocal_amount'], //放款金额
            'payment_flow' => $transactionId, //放款流水
            'payment_status' => 1, //1:发起转账；2：银行信息错误；3：交易成功；4：交易失败
            'payment_code' => '',
        ];
        
        LoanPaymentFlow::saveOne($transactionId, $paymentFlow);
        
        $userInfo = OrderUserBasicInfo::getOne($order['basic_id']);
        
        if ($channel == 'duko') {
            $amount = $order['reciprocal_amount'];
            $cardNumber = $userInfo['bank_number'];
            $uid = $order['uid'];
            $bankCode = $userInfo['bank_name'];
            $bankDetail = BankInfo::getOne([
                'parameter_code' => $bankCode
            ]);
            $bankId = $bankDetail->bank_id;
            $bankName = $bankDetail->parameter_code_name;
            $clientName = $userInfo['name'];
            $phone = $userInfo['phone_number'];
            return $this->paymentByDuko($order_id, $transactionId, $order, $userInfo);
        } else {
            
            $bankName = $userInfo['bank_name'];
            $payeeName = $userInfo['payee_name'];
            $bankAccount = $userInfo['bank_number'];
            $phone = $userInfo['bank_phone'];
            $amount = $order['reciprocal_amount'];
            return $this->paymentByBluePay($order_id, $transactionId, $order->platform, $bankName, $payeeName, $bankAccount, $phone, $amount);
            
        }
    }
    
    /**
     * @param $order_id
     * @param $channel
     * @param $transactionId
     * @return bool|string
     */
    public function investmentPayment($order_id, $channel, $transactionId)
    {
        $transactionId = substr($transactionId, 0, 14).'-INVTM';
        //区分理财端与借款端数据
        $investment = Investment_user::getOne($order_id);
        
        if (!empty($investment)) {
            $order = $investment;
            $order['reciprocal_amount'] = (int)$order->total_apply_amount;
            $order->platform = 'Modalin';
            if (!in_array($order->status, InvestmentStatus::paymentArr())) {
                return json_encode(['code' => 2, 'info' => '该订单已放款']);
            }
        }


        $forceDoku = Platform::doku($order->platform);
        if ($forceDoku)
            $channel = $forceDoku;
        
        //放款流水
        $paymentFlow = [
            'order_id' => $order_id,
            'payment_amount' => $order['reciprocal_amount'], //放款金额
            'payment_flow' => $transactionId, //放款流水
            'payment_status' => 1, //1:发起转账；2：银行信息错误；3：交易成功；4：交易失败
            'payment_code' => '',
        ];
        LoanPaymentFlow::saveOne($transactionId, $paymentFlow);
        
        if ($channel == 'duko') {
            $userInfo = [
                'bank_number' => $order->bankCardNumber,
                'bank_name' => $order->bankCode,
                'name' => $order->name,
                'phone_number' => $order->mobile,
            ];
            return $this->paymentByDuko($order_id, $transactionId, $order, $userInfo);
        } else {
            $bankName = $investment->bankCode;
            $payeeName = $order->name;
            $bankAccount = $order->bankCardNumber;
            $phone = $order->mobile;
            $amount = $order['reciprocal_amount'];
            return $this->paymentByBluePay($order_id, $transactionId, $order->platform, $bankName, $payeeName, $bankAccount, $phone, $amount);
        }
    }
    
    
    
    /**
     * 通过bluepay的渠道进行放款
     * @param $order_id
     * @param $transactionId
     * @param $order
     * @return bool|string
     */
    public function paymentByBluePay($order_id,$transactionId, $platform, $bankName,$payeeName,$bankAccount,$phone,$amount)
    {
        Log::info('bluepay请求的参数:银行名字' . $bankName .'收款人的姓名' . $payeeName . '手机号' . $phone . '银行的账号'. $bankAccount);
        $paymentInfo = BankPayment::payment($platform, $transactionId, $bankName, $payeeName, $bankAccount, $phone, $amount);
        
        if (empty($paymentInfo)) {
            return json_encode(['code' => 3, 'info' => '放款失败']);
        }
        //处理银行返回结果
        
        $returnInfo = $this->updateStatus($order_id, $transactionId, $paymentInfo);
        
        return $returnInfo;
    }
    
    /**
     * 处理银行返回结果
     * @param $order_id 订单id
     * @param $transactionId 交易id
     * @param $paymentInfo bluepay返回结果
     * @return boolean
     */
    public function updateStatus($order_id, $transactionId, $paymentInfo)
    {
        $statusArr = [
            201 => ['order_status' => 8,  'payment_flow_status' => 1, 'info'=>'放款结果：交易处理中'],//发起转账成功，等待提交银行
            200 => ['order_status' => 14, 'payment_flow_status' => 3, 'info'=>'放款结果：交易处理中'],//Bluepay侧交易完成
            400 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：参数错误，缺少参数'],//参数错误，缺少参数
            401 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：签名错误'],//签名错误/加密错误
            404 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：信息未找到/交易信息未找到'],//信息未找到/交易信息未找到
            500 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：Bluepay服务内部错误'],//服务内部错误
            501 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：银行请求超时，可重新发起交易'],//银行请求超时，可重新发起交易
            506 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：IP限制'],//Ip限制
            600 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：转账失败'],//转账失败
            601 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：商户余额不足，请储值'],//商户余额不足
            646 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款结果：银行处理失败'],//银行处理失败
            649 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'放款接口：银行信息错误，账号错误'],//银行处理失败
        ];
        
        $code = $paymentInfo['transferStatus'];
        
        if (isset($statusArr[$code])) {
            if (substr($transactionId, -5) === 'INVTM') {
                if (in_array($code, [200, 201])) {
                    return Investment_user::where('id', $order_id)->update(['status' => InvestmentStatus::INVESTMENTSTATUS7]);
                } else {
                    return Investment_user::where('id', $order_id)->update(['status' => InvestmentStatus::INVESTMENTSTATUS8]);
                }
//               return $this->paymentSuccessInvestment($order_id, $code);
            } else {
                //修改放款流水状态
                LoanPaymentFlow::saveOne($transactionId, [
                    'payment_status' => $statusArr[$code]['payment_flow_status'],
                    'payment_code'   => $code,
                ]);
                //修改订单状态
                $this->updateOrderStatus($order_id, $statusArr[$code]['order_status']);
                
                if(in_array($code, [200, 201])){
                    return json_encode(['code' => 1, 'info'=>$statusArr[$code]['info']]);
                } else {
                    return json_encode(['code' => 4, 'info'=>$statusArr[$code]['info']]);
                }
            }
        } else {
            $status = $paymentInfo['transferStatus'];
            
            Log::info('BluePay放款失败错误码为:' . $status);
            
            return json_encode(['code' => 4, 'info' => '放款失败！！！']);
        }
    }
    
    /**
     * @param $date1
     * @param $date2
     * @return string
     */
    public function diff_Day($date1,$date2)
    {
        $date1=date_create($date1);
        $date2=date_create($date2);
        $diff=date_diff($date1,$date2);
        return $diff->format("%a");
    }
    
    
    /**
     * 放款请求duko
     * @param $order_id
     * @param $transactionId
     * @param $order
     * @param $userInfo
     * @return string
     */
    public function paymentByDuko($order_id, $transactionId, $order, $userInfo)
    {
        $amount = $order['reciprocal_amount'];
        $cardNumber = $userInfo['bank_number'];
        $uid = $order['uid'];
        $bankCode = $userInfo['bank_name'];
        $bankDetail = BankInfo::getOne([
            'parameter_code' => $bankCode
        ]);
        $bankId = $bankDetail->bank_id;
        $bankName = $bankDetail->parameter_code_name;
        $clientName = $userInfo['name'];
        $phone = $userInfo['phone_number'];

//        $bankCode = 'BRINIDJA';
//        $bankId = '002';
//        $bankName = 'BANK RAKYAT INDONESIA';
        
        $model = PaymentFactory::getInstance('duko');
        $paymentInfo = $model->payment($amount,$order_id,  $cardNumber, $uid, $bankCode, $bankId, $bankName, $clientName, $phone, $transactionId);
        Log::info('resultInfo:'.json_encode($paymentInfo));
        
        $resultInfo = $this->updateStatusByDuko($order_id, $transactionId, $paymentInfo, $order['pid'], $order, $userInfo);
        return $resultInfo;
    }
    
    public function updateStatusByDuko($order_id, $transactionId, $paymentInfo, $pid, $order, $userInfo)
    {
        $statusArr = [
            0  => ['order_status' => 15,  'payment_flow_status' => 4, 'info'=>'Transaction status is unknown'],
            10 => ['order_status' => 8 ,  'payment_flow_status' => 1, 'info'=>'Transaction just been created'],
            20 => ['order_status' => 15 ,  'payment_flow_status' => 4, 'info'=>'Pending unpaid transaction'],
            26 => ['order_status' => 15 ,  'payment_flow_status' => 4, 'info'=>'Transaction pending to be refund'],
            30 => ['order_status' => 15 ,  'payment_flow_status' => 4, 'info'=>'Transaction has been locked'],
            35 => ['order_status' => 15 ,  'payment_flow_status' => 4, 'info'=>'Transaction failed'],
            40 => ['order_status' => 15 ,  'payment_flow_status' => 4, 'info'=>'Transaction has been refunded'],
            41 => ['order_status' => 15 ,  'payment_flow_status' => 4, 'info'=>'Transaction has been fully refunded'],
            50 => ['order_status' => 14 ,  'payment_flow_status' => 3, 'info'=>'Transaction has been paid'],
        ];
        
        $code = $paymentInfo->transaction->status;
        if(isset($statusArr[$code])){
            //修改放款流水状态
            LoanPaymentFlow::saveOne($transactionId, [
                'payment_status' => $statusArr[$code]['payment_flow_status'],
                'trancation_id' => $paymentInfo->transaction->id,
                'payment_time' => date('Y-m-d H:i:s', time()),
                'payment_code'   => $code,
            ]);
            //修改订单状态
            if (!in_array(substr($transactionId, -5),['INVTM'])) {
                $this->updateOrderStatus($order_id, $statusArr[$code]['order_status']);
            }

//            订单的产品的信息
            Log::info('doku payment flow:'.$code);
            if(in_array($code, [50])){
                if (substr($transactionId, -5) === 'INVTM') {
                    $this->paymentSuccessInvestment($order_id, $code);
                } else {
                    $this->paymentSuccess($order_id, $pid, $order, $userInfo);
                }
                return json_encode(['code' => 1, 'info'=>$statusArr[$code]['info']]);
            } else if(in_array($code, [10])) {
                return json_encode(['code' => 1, 'info'=>$statusArr[$code]['info']]);
            } else  {
                return json_encode(['code' => 4, 'info'=>$statusArr[$code]['info']]);
            }
        } else {
            $status = $paymentInfo->status;
            
            Log::info('order_id ' .$order_id . 'Duko放款失败错误码为:' . $status);
            
            return json_encode(['code' => 4, 'info' => __('payment.bond.status7')]);
        }
        
    }
    
    public function paymentSuccess($order_id, $pid, $order, $userInfo)
    {
        $product = Product::getProduct($pid);
        $period = LoanDay::toString($product->loan_period);
        
        $deadline_days = $period-1;
        $orderParam['payment_date'] = date('Y-m-d H:i:s', time());
        $orderParam['loan_deadline'] =  date('Y-m-d 00:00:00', strtotime('+'.$deadline_days.' day'));
        $orderParam['loan_suspend_date'] =  date('Y-m-d 00:00:00', strtotime('+'.$period.' day'));
        $orderParam['loan_overdue_date'] =  date('Y-m-d 00:00:00', strtotime('+'.$period.' day'));
//       dd(LoanOrder::updateById($order_id, $orderParam));
        LoanOrder::updateById($order_id, $orderParam);
        
        $uid = $order->uid;
        $pushToken = PushToken::getTokenByUid($uid);
        $platform = $order->platform;
        $card = substr($userInfo->bank_number,-4);
        PushSevice::pushPayment($platform, $pushToken, $card);
        
        $mobile = $userInfo->phone_number;
        OverMessage::repayment($mobile, $card, $uid);
    }
    
    public function paymentSuccessInvestment($order_id, $code)
    {
        Log::info('Investment Apply Amount Success oid:'.$order_id);
        $invest = Investment_user::find($order_id);
        $ret = Investment_user::paymentSuccess($order_id, $invest, $code);
        
        if ($ret) {
            $mobile = ltrim($invest->mobile, '0');
            $card = substr($invest->bankCardNumber, -4);
            OverMessage::investmentApplyAmount($mobile, $card, $invest->uid);
            
            $uid = $invest->uid;
            $pushToken = PushToken::getTokenByUid($uid);
            PushSevice::investmentApplyAmount($pushToken, $card);
        }
        
    }
    
}