<?php

namespace App\Service\Order;

use App\Consts\OrderStatus;
use App\Models\LoanOrder;

class TrackOrder extends Order
{

    public $order_status = [10, 11, 13, 14, 25];
    
    /**
     * 催收列表
     * @param array $paramArr
     * @param int $pageNum
     * @return mixed
     */
    public function getOrderList($paramArr=[], $orderBy = 'loan_order.id', $pageNum = 20)
    {
        $options = [
            'order_status' => $this->order_status,
            'overdue90' => ['loan_overdue_date', '>', date('Y-m-d 00:00:00', strtotime('-90 day'))],
        ];
        $options = array_merge($options, array_filter($paramArr));
        
        $query = LoanOrder::select(['loan_order.*', 'order_user_basic_info.name', 'loan_order_track.admin_id', 'loan_order_track.status', 'order_user_basic_info.ktp_number', 'order_user_basic_info.phone_number']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->join('loan_order_track', 'loan_order_track.order_id', '=', 'loan_order.id', 'left');
        
        //查询条件
        foreach ($options as $column=>$value) {
            if ($column=='is_remind' && $value==100)
                $value = 0;
            
            if ($column=='type') { //上一条下一条筛选
                $query->where('loan_order.id', $value[0], $value[1]);
            } elseif ($column == 'status1' || $column == 'status11' || $column == 'status2' || $column =='overdue90') { //状态筛选
                $query->where($value[0], $value[1], $value[2]);
            } elseif (is_array($value)) {
                $query->whereIn($column, $value);
            } elseif($column=='admin'){
                $query->where('admin_id', '!=', '');
            } elseif($column=='time'){
                $query->where('loan_order.loan_deadline', '<', $value);
            } else{
                $query->where($column, 'like', "%$value%");
            }
        }
        
        //如果orderBy为字符串，则默认倒序。如果是数组，按照数组参数排序
        $sort = is_string($orderBy) ? 'desc' : $orderBy[1];
        $orderBy = is_string($orderBy) ? $orderBy : $orderBy[0];
        $orders = $query->trialtrack()->orderBy($orderBy, $sort)->paginate($pageNum);
        
        //格式化数据
        $orders = $this->formatorder($orders);
        
        
        return $orders;
    }
    
    /**
     * 格式化订单数据
     * @param $orders
     * @return mixed
     */
    public function formatorder($orders)
    {
        $orders->map(function($order) {
            $order['order_status_str'] = OrderStatus::toString($order['order_status']);
            return $order;
        });
        
        return $orders;
    }
}