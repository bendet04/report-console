<?php

namespace App\Service\Order;

use App\Consts\InvestmentStatus;
use App\Models\Investment;
use App\Models\Investment_user;
use App\Models\InvestsmentRelation;
use App\Models\LoanOrder;
use App\Models\LoanRePaymentFlow;
use App\Models\OrderStatus;
use App\Models\UserBankInfo;
use Illuminate\Support\Carbon;

class RePaymentOrder extends Order
{
    /**
     * 手动全部还款
     * @param $order_id
     * @return string
     */
    public function repayment($order_id)
    {
        $order = LoanOrder::getOne($order_id);
        if (empty($order)) {
            return json_encode(['code'=> 2, 'info'=>'订单不存在']);
        }
        
        //生成还款的交易id
        $transactionId = $this->generaterderNumber();
        
        //添加银行的信息
        $bankData['oid'] = $order_id;
        $bankData['uid'] = $order['uid'];
        $bankData['transaction_id'] = $transactionId;
        $bankData['type'] = 1;
        $bankData['confirm_time'] =  time();
        $bankDataInfo = UserBankInfo::saveOne($bankData);
//        dd($bankDataInfo);
        
        if (empty($bankDataInfo)) {
            return json_encode(['code'=> 3, 'info'=>'信息写入失败']);
        }
        
        //添加还款流水的信息
        $repaymentData['order_id'] = $order_id;
        $repaymentData['type'] = 1;
        $repaymentData['repayment_amount'] = $order['repayment_amount'];
//        $repaymentData['repayment_flow'] = $transactionId;
        $repaymentData['repayment_code'] = '0000000000000modalin';
        $repaymentData['repayment_status'] = 1;
        $repaymentData['repayment_time'] = Carbon::now();
        $flowData = LoanRePaymentFlow::saveOne($transactionId, $repaymentData);
        if (empty($flowData)) {
            return json_encode(['code'=> 3, 'info'=>'信息写入失败']);
        }
        
        $investOrder = InvestsmentRelation::getByOrderId($order_id);
        if ($investOrder) {
            $investment = Investment_user::getOne($investOrder->investors_id);
            $alreadyInterest = $investment->already_interest;
            $totalApplyAmount = $investment->total_apply_amount;
            
            $ret = InvestsmentRelation::updateStatusByOrderId($investOrder->id);
            if ($ret) {
                $interest = InvestsmentRelation::getInterest($investOrder->loan_period, $investOrder->loan_amount);
                //计算投资利息
                $alreadyInterest = $alreadyInterest + $interest;
                //计算可提现金额
                $totalApplyAmount = $totalApplyAmount + $investOrder->loan_amount + $interest;
                
                $updateParam = ['already_interest'=>$alreadyInterest, 'total_apply_amount'=>$totalApplyAmount];
                if ($investment->status == InvestmentStatus::INVESTMENTSTATUS5){
                    $updateParam['status'] = InvestmentStatus::INVESTMENTSTATUS6;
                }
                Investment_user::updateAmount($investOrder->investors_id, $updateParam);
            }
        }
        
        
        //更新订单状态
//        $this->handHanddleRepaymentOrder($order_id, 12);
        $this->updateOrderStatus($order_id, 12);
        
        return json_encode(['code'=>1, 'info'=>__('trial.repayment.success')]);
    }
    
    /**
     *     手动还款的处理
     * @param $order_id
     * @param $status
     * @return bool
     */
    public function handHanddleRepaymentOrder($order_id, $status)
    {
        OrderStatus::saveOne([
            'order_id' => $order_id,
            'admin_id' => Auth::id(),
            'status' => $status,
        ]);
        
        $orderParamArr = ['order_status' => $status, 'loan_repayment_date' => Carbon::now()];
        
        $res = LoanOrder::updateById($order_id, $orderParamArr);
        
        //推送消息
        $this->pushMessage($order_id, $status);
        
        return $res;
    }
    
}