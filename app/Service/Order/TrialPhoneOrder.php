<?php

namespace App\Service\Order;

use App\Consts\LoanDay;
use App\Consts\OrderStatus;
use App\Consts\StatusIdentifier;
use App\Events\PhoneAfter;
use App\Models\AdminUser;
use App\Models\LoanOrder;
use App\Models\LoanTrialPhone;
use App\Models\LoanTrialPhoneRecord;
use App\Models\OrderUserBasicInfo;
use App\Models\UserExtraImage;
use App\Service\Assign;
use App\Service\Contracts\Order\Trial;
use App\Service\UserService;

use Carbon\Carbon;
use App\Events\StartWork;

class TrialPhoneOrder extends Order implements Trial
{
    public $order_status = 19;
    public $work_invalid_phone_continue = false; //false无效工作电话直接拒绝 true进入个人电核
    public $pass_count = 9;// 工作电核+个人电核一致数>=9，如果工作电核无效电核可直接进入下一步，需修改此值
    
    public function getOrderList(
        $paramArr = [],
        $orderBy = 'tb_allocation_rule.auth desc, tb_loan_trial_phone_record.updated_at asc',
        $pageNum = 20
    ) {
        $admin_id = \Auth::id();
        $options = [
            'order_status' => $this->order_status,
        ];
        $options = array_merge($options, array_filter($paramArr));
        $no_heared = AdminUser::getNoHeared($admin_id);
        //获取信审订单列表
        $query = LoanOrder::select(['loan_order.id', 'order_number', 'loan_order.platform', 'loan_order.uid',
                                  'loan_order.updated_at', 'name', 'ktp_number', 'pay_type', 'phone_number',
                                  'loan_period', 'apply_amount', 'order_time', 'order_status',
                                  'loan_trial_phone.admin_id', 'allocation_rule.auth',
                                  'loan_trial_phone_record.work_phone_status',
                                  'loan_trial_phone_record.phone_status']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->leftjoin('allocation_rule', 'allocation_rule.order_id', '=', 'loan_order.id');
//        $query->leftjoin('loan_trial_phone_record', 'loan_trial_phone_record.order_id', '=', 'loan_order.id');

        foreach ($options as $column => $value) {
            if (is_array($value)) {
                $query->whereIn($column, $value);
            } else {
                $query->where($column, 'like', "%$value%");
            }
        }
        $query->trialphone();

        //如果$orderBy为字符串（默认），倒序显示，自定义可传入数组
        if (is_string($orderBy)) {
            $query->orderByRaw($orderBy);
        } else {
            list($orderColumn, $sort) = $orderBy;
            $query->orderBy($orderColumn, $sort);
        }
        $orders = $query->paginate($pageNum);
        
            //格式化数据
        $orders = $this->formatOrder($orders);
    
        return $orders;
    }
    
    public function formatOrder($orders)
    {
        $orders->map(function ($order) {
            $order['loan_period'] = LoanDay::toString($order['loan_period']);
            $order['order_status'] = OrderStatus::toString($order['order_status']);
        });
        
        return $orders;
    }
    
    /**
     * 获取审核信息
     * @param array $paramArr ['order_id'=>1]
     * @return $this|\Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getOrder($paramArr = [])
    {
        $options = array_filter($paramArr);
//        $options['order_status'] = $this->order_status;
        
        $order = LoanOrder::select('loan_order.id', 'order_number', 'loan_order.platform', 'basic_id','loan_order.uid', 'pay_type', 'order_status');
        $order = $order->join('loan_trial_phone', 'loan_trial_phone.order_id', '=', 'loan_order.id');
        $order = $order->where($options)->orderBy('loan_order.updated_at', 'DESC')->first();

        if (empty($order)) {
            return false;
        }

        $order['salary_image'] = '';
        if (isset($order['uid']) || !empty($order['uid'])) {
            $userExtra = UserExtraImage::getOne($order['uid']);
            if (isset($userExtra->salary_image_url)) {
                $order['salary_image'] = $userExtra->salary_image_url;
            }
        }
        
        if ($order) {
            //获取用户审核记录
            $order['work'] = '';
            $order['peason'] = '';
            $order['work_phone_status'] = 0;
            $order['phone_status'] = 0;
            $record = LoanTrialPhoneRecord::getByOrderId($order['id']);
            if ($record) {
                $order['work'] = $record['work'];
                $order['peason'] = $record['peason'];
                $order['work_phone_status'] = $record['work_phone_status'];
                $order['phone_status'] = $record['phone_status'];
                $order['admin'] = $record->admin->admin_username;
                $order['created_at'] = $record['created_at'];
            }
            
            //获取用户基本信息
            $order['user'] = OrderUserBasicInfo::getOne($order['basic_id']);
            $order['npwp'] = UserService::partnerInfo($order['uid'], 'npwp');
            $order['bpjs'] = UserService::partnerInfo($order['uid'], 'bpjs');
        }
        
        return $order;
    }
    
    /**
     * 电话审核
     * @param array $paramArr
     * 工作电核
     * $paramArr = [
        'status'        => 21, //21电审成功 22电审失败
        'order_id'      => 1,
        'company_phone' => 1, //1有效号码 2无人接听，加入队列 3无效号码 4节假日无人接听
        'company_name'  => 1, //公司名称1一致 2不一致
        'career'        => 1, //职位
        'work_period'   => 1, //工作年限
        'income_level'  => 1, //收入级别
        'paid_period'   => 1, //发薪日
        'company_address' => 1,
        'work_remark'   => [],
        ];
     * 个人电核
     * $paramArr = [
        'status'          => 21, //21电审成功 22电审失败
        'phone_number'    => 1, //1有效号码 2无人接听加入队列 3无效号码
        'name'            => 1,
        'ktp_number'      => 1,
        'marriage'        => 1,
        'pay_type'        => 1,
        'address'         => 1,
        'relative_first'  => 1,
        'relative_second' => 1,
        'person_remark'   => [],
        ];
     * @return int
     */
    public function Trial($paramArr = [])
    {
        $order_id = $paramArr['order_id'];
        $admin_id = \Auth::id();
        $no_heared = AdminUser::getNoHeared($admin_id);
        unset($paramArr['order_id']);
        //检查订单状态，防止重复审核
        $order = LoanOrder::getOne($order_id);
        if (!$order || $order->order_status!=19) {
              $data['code']=11;
              $data['info']='Audit result: this list has been audited by other personnel.';
              return $data;
        }
        //根据电核一致数计算结果
        $status = $this->getStatus($order_id, $paramArr);
    
        //电核拒绝则直接拒绝
        if ($status==22){

        //记录拒绝时间
            $this->recordRefuseTime($order->basic_id);
            $data['work_phone_status'] = 0;
            $data['phone_status'] = 0;
            LoanTrialPhoneRecord::saveOne($order_id, $data);
            $arr = $this->checkNum($admin_id);
            if ($no_heared != 1) {
                event(new PhoneAfter($order_id, $status));
            }

            //更新订单状态
            return $this->updateOrderStatus($order_id, $status);
        }
        
        //工作电核
        if (!empty($paramArr['company_phone'])) {
            $data['work'] = json_encode($paramArr);
            $data['work_phone_status'] = $paramArr['company_phone'];
            LoanTrialPhoneRecord::saveOne($order_id, $data);
            $arr = $this->checkNum($admin_id);
            //无人接听，加入队列19
            if ($paramArr['company_phone']==2) {
                if ($no_heared != 1) {
                    event(new StartWork(2));
                }
                $res = $this->checkNum($admin_id);
                if ($res) {
                    return $res;
                }
                $order = $this->getOrderList([], ['loan_order.updated_at', 'asc'], 1);
                $updated_at =  (!empty($order[0]) && !empty($order[0]->updated_at)) ? $order[0]->updated_at->subSeconds(1) : Carbon::now();
                return LoanOrder::updateById($order_id, ['order_status'=>19, 'updated_at'=>$updated_at]);
            }
            
            //无效号码
            if ($paramArr['company_phone']==3 && $this->work_invalid_phone_continue===false) {
                //更新订单状态
                return $this->updateOrderStatus($order_id, 22);
            }
        }
        
        //个人电核
        if (!empty($paramArr['phone_number'])) {
            $data['peason'] = json_encode($paramArr);
            $data['phone_status'] = $paramArr['phone_number'];
            LoanTrialPhoneRecord::saveOne($order_id, $data);
            $arr = $this->checkNum($admin_id);
            
            //无人接听，加入队列19
            if ($paramArr['phone_number']==2) {
                if ($no_heared != 1) {
                    event(new StartWork(2));
                }
                $res = $this->checkNum($admin_id);
                if ($res) {
                    return $res;
                }
                $order = $this->getOrderList([], ['loan_order.updated_at', 'asc'], 1);
                $updated_at =  (!empty($order[0]) && !empty($order[0]->updated_at)) ? $order[0]->updated_at->subSeconds(1) : Carbon::now();
                return LoanOrder::updateById($order_id, ['order_status'=>19, 'updated_at'=>$updated_at]);
            }
    
            //无效号码
            if ($paramArr['phone_number']==3) {
                //更新订单状态
                return $this->updateOrderStatus($order_id, 22);
            }
            
            //根据电核一致数计算结果
            $status = $this->getStatus($order_id, $paramArr);
            
            LoanTrialPhone::saveData($order_id, [
                'status' => $status,
                'trial_time' => date('Y-m-d H:i:s'),
            ]);
            
            //更新订单状态
            $this->updateOrderStatus($order_id, $status);
    
            //电审完成后，触发相应事件
            if ($no_heared != 1) {
                event(new PhoneAfter($order_id, $status));
            } else {
                $assignStatus = Assign::assign($order_id, 'final', [['id' => 0]]);
            }
        }
        
        return 1;
    }

    public function getNextOrderId($paramArr = [], $orderBy = 'loan_order.updated_at', $pageNum = 20)
    {
        //过滤条件为空的数据
        $options = array_filter($paramArr);
        $options['order_status'] = $this->order_status;

        //获取信审订单列表
        $query = LoanOrder::select(['loan_order.id', 'order_number', 'loan_order.uid', 'name', 'ktp_number', 'phone_number', 'loan_period', 'apply_amount', 'order_time', 'order_status']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        foreach ($options as $column=>$value) {
            if (is_array($value)) {
                $query->whereIn($column, $value);
            } else{
                $query->where($column, 'like', "%$value%");
            }
        }

//        $order = $query->trialphone()->orderBy($orderBy)->take(1)->first();
//        $order = $query->orderBy($orderBy)->take(1)->first();
//        $order = $query->trialphone()->orderByDesc($orderBy)->first();
        $order = $query->trialphonequeue()->orderByDesc($orderBy)->first();

        return $order;
    }
    
    /**
     * 获取订单状态21电审成功 22电审失败
     * @param $order_id
     * @param $paramArr
     * @return int
     */
    public function getStatus($order_id, $paramArr)
    {
        return isset($paramArr['status']) ? $paramArr['status'] : 21;
//        $count = 0;
//        $record = LoanTrialPhoneRecord::getByOrderId($order_id);
//
//        $work = json_decode($record['work'], true);
//        //单位名称、姓名、借款用途、ktp必须一致，否则拒绝
//        if ($work['company_name'] != 1 || $paramArr['name']!=1 || $paramArr['pay_type']!=1 || $paramArr['ktp_number']!=1) {
//            return 22;
//        }
//
//        foreach ($work as $v) {
//            if (!is_array($v) && $v == 1) $count++;
//        }
//
//        foreach ($paramArr as $v) {
//            if (!is_array($v) && $v == 1) $count++;
//        }
//
//        if ($count >= $this->pass_count) {
//            return 21;
//        } else {
//            return 22;
//        }
    }
    
    public function checkNum($admin_id)
    {
        //判断无人接听数量
        $workUnattendedNum = LoanTrialPhoneRecord::unattendedNum(['admin_id' => $admin_id, 'work_phone_status' => 2]);
        $personUnattendedNum = LoanTrialPhoneRecord::unattendedNum(['admin_id' => $admin_id, 'phone_status' => 2]);
        $unattendedNum = $workUnattendedNum + $personUnattendedNum;
        $maxUnattendedNum = env('UNATTENDEDNUM');
        if ($maxUnattendedNum <= $unattendedNum) {
             AdminUser::saveData($admin_id, ['no_heared' => 1]);
             return ['code' => StatusIdentifier::PHONE_LIST, 'info' => 'You have many unanswered orders.
                    Please check them first.'];
        } elseif ($unattendedNum == StatusIdentifier::DATA_ABNORMITY) {
             AdminUser::saveData($admin_id, ['no_heared' => 0]);
        }
        return '';
    }
}