<?php

namespace App\Service\Order;

use App\Models\LoanOrder;
use App\Models\MessageHistory;
use App\Models\OrderRefuseTime;
use App\Models\OrderStatus;
use App\Models\OrderUserBasicInfo;
use App\Models\PushToken;
use App\Models\Token;
use App\Service\PushSevice;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Consts\OrderStatus as OrderStatusConsts;
use Illuminate\Support\Facades\Log;

abstract class Order
{
    
    /**
     * 生成放款/还款交易id
     * @return string
     */
    public function generaterderNumber()
    {
        $number = "";
        for($i = 0; $i < 4; $i++){
            $chr = rand(0,3);
            $str = "";
            for($j = 0; $j < 4; $j ++){
                if($j == $chr) {
                    $str .= chr(rand(65, 90));
                } else {
                    $str .= rand(0,9);
                }
            }
            $number .= (empty($number) ? $str : "-".$str);
        }
        
        return $number;
    }
    
    /**
     * 获取订单列表
     * @param array $paramArr
     * @param string $orderBy
     * @param int $pageNum
     * @return array
     */
    public function getList($paramArr = [], $orderBy = 'loan_order.id',$time='', $pageNum = 20)
    {
        //过滤条件为空的数据
        $options = array_filter($paramArr);
        $query = LoanOrder::select(['loan_order.*', 'order_user_basic_info.name', 'order_user_basic_info.ktp_number']);
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');

   if(!empty($options['start'])&&!empty($options['end']))
    {
        $query->where('loan_order.'.$time,'>=',$options["start"]);
        $query->where('loan_order.'.$time,'<=',$options["end"]);
        unset($options['start']);
        unset($options['end']);
    }else if(empty($options['start'])&&!empty($options['end'])) {
        $query->where('loan_order.'.$time,'<=',$options['end']);
        unset($options['end']);

   }else if(empty($options['end'])&&!empty($options['start'])){
        $query->where('loan_order.'.$time,'>=',$options['start']);
        unset($options['start']);
   }
        foreach ($options as $column=>$value) {
            if (is_array($value)) {
                $query->whereIn($column, $value);
            } elseif(is_numeric($value)){
                $query->where($column, '=', $value);
            }elseif($column=='platform'){
                $query->where('loan_order.'.$column, '=', $value);
            } else
            {
                $query->where($column, 'like', "%$value%");
            }
        }
//        dd($query->toSql());
        $orders = $query->orderByDesc($orderBy)->paginate($pageNum);

        $orders->map(function($order) {
            $order->order_status_str = OrderStatusConsts::toString($order->order_status);
            return $order;
        });
        
        return $orders;
    }
    
    /**
     * 修改订单状态
     * @param $order_id
     * @param $status
     * @return bool
     */
    public function updateOrderStatus($order_id, $status)
    {
        //更新订单状态
        OrderStatus::saveOne([
            'order_id' => $order_id,
            'admin_id' => Auth::id(),
            'status' => $status,
        ]);
    
        //更新订单表
        $orderParamArr = ['order_status' => $status];
        
        //信审成功后特殊处理updated_at字段，电审根据此字段做队列
        if ($status==19){
            $orderDetail = LoanOrder::getOne($order_id);
            $orderParamArr['updated_at'] =$orderDetail->created_at;
        }
    
        if ($status==12){
            $orderParamArr['loan_repayment_date'] = Carbon::now();
        }
        
        $res = LoanOrder::updateById($order_id, $orderParamArr);
        
        //推送消息
        $this->pushMessage($order_id, $status);
        
        return $res;
    }
    
    /**
     * 推送消息
     * @param $order_id
     * @param $status
     * @return bool|mixed
     */
    public function pushMessage($order_id, $status)
    {
        $order = LoanOrder::getOne($order_id);
        $pushToken = PushToken::getTokenByUid($order['uid']);
    
        $pushInfo = [];
        
        //终审成功
        if ($status == 6) {
            $pushInfo = PushSevice::pushTrialSuccess($order['platform'], $pushToken);
        }
        
        //审核失败
        if (in_array($status, [18, 20, 22, 5])) {
            $pushInfo = PushSevice::pushTrialFailure($order['platform'], $pushToken);
        }
        
        if ($pushInfo) {
            Log::info('token ' . $pushToken . 'pushInfo ' . $pushInfo['result']);
            //保存推送记录
            return $this->savePushMessage($pushInfo, $order);
        }
        
        return false;
    }
    
    /**
     * 保存推送记录
     * @param $pushInfo
     * @param $order
     * @return mixed
     */
    public function savePushMessage($pushInfo, $order)
    {
        $result = json_decode($pushInfo['result'], true);
    
        //判断推送结果
        if (empty($result)) {
            $message_status = 2;
        } else {
            if (empty($result['success'])) {
                $message_status = 2;
            } else {
                $message_status = 1;
            }
        }
    
        //保存推送消息详情
        $res = MessageHistory::saveOne([
            'title'          => $pushInfo['title'],
            'description'    => $pushInfo['body'],
            'uid'            => $order['uid'],
            'message_status' => $message_status,//1:发送成功；2：发送失败
            'message_type'   => 2,
        ]);
        
        return $res;
    }

    public function recordRefuseTime($order_id)
    {
        $order = OrderUserBasicInfo::getOne($order_id);
        $data['ktp_number'] = $order->ktp_number;
        $data['refuse_time'] = date('Y-m-d H:i:s', time());
        $ktp['ktp_number'] = $order->ktp_number;
        $refuseRecord = OrderRefuseTime::saveOne($ktp,$data);

//        dd($ktp);
        return $refuseRecord;

    }
    
    /**
     * 获取三个月内订单，ktp六位一下相似
     *
     * @param $ktp_number
     * @return array
     */
    public function getKtpSimlarOrder($ktp_number)
    {
        $simlarKtp = [];
        
        $startDay = date('Y-m-d 00:00:00', strtotime('-90 days'));
        $query = OrderUserBasicInfo::select(['order_user_basic_info.id', 'order_user_basic_info.ktp_number']);
        $query->join('loan_order', 'loan_order.basic_id', '=', 'order_user_basic_info.id');
        $query->whereIn('loan_order.order_status', [3, 6, 8, 11 ,13, 14, 15, 17, 19, 21, 25]);
        $ktpList = $query->where('order_user_basic_info.created_at', '>=', $startDay)->get();
        
        foreach ($ktpList as $k=>$user) {
            if ($ktp_number != $user->ktp_number) {
                $simlarCount = 0;
        
                for ($i=0; $i < strlen($ktp_number); $i++) {
                    if (isset($ktp_number[$i]) && isset($user->ktp_number[$i]) && $ktp_number[$i] !== $user->ktp_number[$i]) {
                        $simlarCount++;
                    }
                }
        
                if (6 > $simlarCount) {
                    array_push($simlarKtp, $user->id);
                }
            }
        }
        
        return $simlarKtp;
    }
    
}