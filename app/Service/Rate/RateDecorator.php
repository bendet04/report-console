<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/13/013
 * Time: 13:46
 */

namespace App\Service\Rate;


use app\api\model\LoanRate;
use App\Service\DateUnit;

abstract class RateDecorator implements RateInterface
{
//    private $
    public function __construct(RateInterface $rate)
    {
        $this->rate = $rate;
    }

    public function __set($name='', $value='')
    {
        $this->$name = $value;
    }
    public function calculate()
    {
        $this->rate->calculate();
    }
    abstract public function decorateApplyAmount($applyAmount);
}