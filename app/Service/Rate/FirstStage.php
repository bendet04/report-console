<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/12/012
 * Time: 18:00
 */

namespace App\Service\Rate;


use App\Models\LoanRate;
use App\Models\Product;

class FirstStage extends RateDecorator
{
    private $applyAmount;
    private $type;
    private $pid;
    private $loanDeadline;

    public function __construct(RateInterface $rate)
    {
        $this->rate =$rate;
    }

    public function calculate()
    {
        $nornal = $this->rate ->calculate();
        $rate = $this->decorateApplyAmount($nornal['applyAmount']);

        return [
            'amount' => $nornal['amount'] + $rate['amount'] ,
            'fornula' => $nornal['fornula'] . ' + '. $rate['fornula'],
            'applyAmount' => $nornal['applyAmount'],
            'productId' =>  $nornal['productId']
        ];
    }

    public function decorateApplyAmount($applyAmount)
    {
        $rate = LoanRate::getRate(1);
        $amount = $applyAmount * $rate->rate;
        $formula = $applyAmount .' * '. $rate->rate;

        return [
            'amount' => $amount,
            'fornula' => $formula,
            'applyAmount' => $applyAmount,
            'productId' =>  $this->pid
        ];
    }

}