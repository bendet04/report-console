<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/13/013
 * Time: 18:10
 */

namespace App\Service\Rate;
use App\Service\DateUnit;
use App\Models\Product;

class OverDueDayRate extends RateDecorator
{
    private $applyAmount;
    private $type;
    private $pid;
    private $loanDeadline;

    public function __construct(RateInterface $rate,$loanDeadline)
    {
        $this->rate =$rate;
        $this->loanDeadline =$loanDeadline;
    }

    public function calculate()
    {
        $nornal = $this->rate->calculate();
//        echo 111;
//        dd($nornal);
        $this->pid = $nornal['productId'];
        $rate = $this->decorateApplyAmount($nornal['applyAmount']);

//        dd($nornal['productId']);

//        dd($this->pid);

        return [
            'amount' => $nornal['amount'] + $rate['amount'] ,
            'fornula' => $nornal['fornula'] . ' + '. $rate['fornula'],
            'applyAmount' => $nornal['applyAmount'],
            'productId' =>  $nornal['productId']
        ];
    }

    public function decorateApplyAmount($applyAmount)
    {
//        $rate = LoanRate::getRate($rateType);
//        $product = Product::getProduct($pid);
//        $amount = $applyAmount * $rate->rate + $applyAmount * $product->overdue_rate;
//        $formula = $applyAmount .' * '. $rate->rate .' + '. $applyAmount .' * '. $product->overdue_rate;
//
//        return [
//            'amount' => $amount,
//            'fornula' => $formula,
//            'applyAmount' => $applyAmount,
//            'productId' =>  $pid
//        ];
//        $rateDetail = $this->getRateType($this->loanDeadline);
        $day = DateUnit::timeToDay($this->loanDeadline);
        $day = $day+1;
        $product = Product::getProduct($this->pid);
//        dd($product);
        $amount = $applyAmount * $product->overdue_rate * $day;
        $formula = $applyAmount .' * '. $product->overdue_rate . ' * '. $day;

        return [
            'amount' => $amount,
            'fornula' => $formula,
            'applyAmount' => $applyAmount,
            'productId' =>  $this->pid
        ];
    }

}