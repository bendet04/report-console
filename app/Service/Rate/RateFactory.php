<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/14/014
 * Time: 9:52
 */

namespace App\Service\Rate;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class RateFactory
{
    public static function getInstance($type, $applyAmount, $productId, $loanOverDueDate='')
    {
        switch($type){

            case 'normalRate':  //不逾期的利率
                $normal = new NormalRate($applyAmount, $productId);
                return $normal->calculate();
                break;

            case 'firstRate':  //M0阶段利率
                $normal = new NormalRate($applyAmount, $productId);
                $firstNormal = new FirstStage($normal);
                $overDay = new OverDueDayRate($firstNormal, $loanOverDueDate);
                return $overDay->calculate();

            case 'secondRate':  //M1阶段利率
                $normal = new NormalRate($applyAmount, $productId);
                $secondNormal = new SecondStage($normal);
                $overDay = new OverDueDayRate($secondNormal, $loanOverDueDate);
                return $overDay->calculate();

            case 'thirdStage':  //M2阶段利率
                $normal = new NormalRate($applyAmount, $productId);
                $thirdNormal = new ThirdStage($normal);
                $overDay = new OverDueDayRate($thirdNormal, $loanOverDueDate);
                return $overDay->calculate();

            case 'forthStage':  //M3阶段利率
                $normal = new NormalRate($applyAmount, $productId);
                $forthNormal = new ForthStage($normal);
                $overDay = new OverDueDayRate($forthNormal, $loanOverDueDate);
                return $overDay->calculate();

            case 'fifthStage':  //M4阶段利率
                $normal = new NormalRate($applyAmount, $productId);
                $fifthNormal = new FifthStage($normal);
                return $fifthNormal->calculate();

            default:
                throw new FileException('File not found');
        }
    }
}