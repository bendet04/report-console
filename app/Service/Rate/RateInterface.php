<?php

namespace App\Service\Rate;

interface RateInterface
{
     public function calculate();
}