<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/12/012
 * Time: 17:21
 */

namespace App\Service\Rate;

use App\Consts\LoanDay;
use App\Models\Product;
use Illuminate\Contracts\Logging\Log;

class NormalRate implements  RateInterface
{
    private  $applyAmount;
    private  $pid;

    public function __construct($applyAmount,$pid)
    {
        $this->applyAmount = $applyAmount;
        $this->pid = $pid;
    }

    public function calculate()
    {
        $product = Product::getProduct($this->pid);
        $day = LoanDay::toString($product->loan_period);
        $amount = $this->applyAmount * $product->normal_rate * $day + $this->applyAmount;
        $overDue = $product->overdue_rate;
        $formula = $this->applyAmount .' * '. $product->normal_rate .' * '. $day . ' + '. $this->applyAmount;

        return [
            'amount' => $amount,
            'fornula' => $formula,
            'applyAmount' => $this->applyAmount,
            'productId' =>  $this->pid
        ];
    }

    public function loanPeriod($type)
    {
        switch ($type)
        {
            case 1:
                return 7;
                break;
            case 2:
                return 14;
                break;
            case 3:
                return 14;
                break;
            default:
                Log::info('loan period bug');
                return '';
        }
    }

}