<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/6/25/025
 * Time: 13:29
 */

namespace App\Service;

use Illuminate\Support\Facades\Facade;

class Efficency extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\Efficency';
    }
}