<?php

namespace App\Service\Sms;

use App\Consts\Platform;
use Illuminate\Support\Facades\Log;

class Paasso implements SmsProvider
{
    private $key = '';
    private $secret =  '';
    private $from = '';
    private $channel = '';
    private $text = "";
    private $type = '';
    const URL = 'https://api.paasoocn.com/json?';


    private function channelInit() {
        curl_setopt($this->channel, CURLOPT_HEADER, false);
        curl_setopt($this->channel, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->channel, CURLOPT_TIMEOUT, 5);
//        curl_setopt($this->channel, CURLOPT_RETURNTRANSFER, true);
    }
    
    /**
     * Paasso constructor.
     * @param $type 1验证通道 2营销通道
     */
    public function __construct($type)
    {
        $this->type = $type;
        $this->channel = curl_init();
        $this->channelInit();
    }

    public function setText($text) {
        $this->text = $text;
    }
    
    public function setFrom($from) {
        $this->from = env($from.'_MESSAGE_FROM');
        $this->key = env($from.'_MESSAGE_KEY');
        $this->secret = env($from.'_MESSAGE_SERECT');
        $senderId = Platform::alias($from);
        $this->text = str_replace('SenderId', $senderId, $this->text);
    }
    
    public function send($mobile)
    {
        $messageId = "";
        
        $url = self::URL
            . 'key='.$this->key
            . '&secret='.$this->secret
            . '&from=' .$this->from
            . '&to='.$mobile
            . '&text='.urlencode($this->text);

        curl_setopt($this->channel, CURLOPT_URL, $url);

        $ret = curl_exec($this->channel);
        $error = curl_errno($this->channel);
        if (0 != $error)
            Log::info("Send SMS network error: ". curl_error($this->channel));
        else {
            $ret = json_decode($ret);
            if (!empty($ret)) {
                if (0 != $ret->status)
                    Log::info("Send Paanno SMS error:". $ret->status_code);
                else
                    $messageId = $ret->messageid;
            } else {
                Log::info("Send Paanno SMS error:JSON error");
            }
        }

        curl_close($this->channel);

        return $messageId;
    }

    public function getType() {
        return $this->type;
    }

}