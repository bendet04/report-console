<?php

namespace App\Service\Sms;

class SmsProviderFactory
{
    const PAASSO = 1;
    const Nxtele = 2;
    const Chuanglan = 3;
    const INFOBIP = 4;
    
    public static function getProvider() {
        return self::INFOBIP;
    }

    public static function getInstance($type=1, $driver='InfoBip') {
        switch ($driver) {
            case 'paasso':
                return new Paasso($type);
            case 'ChuanglanWorld':
                return new ChuanglanWorld($type);
            case 'InfoBip':
                return new InfoBip($type);
        }
        
        throw new \Exception("Unsupported driver {$driver}");
    }
}