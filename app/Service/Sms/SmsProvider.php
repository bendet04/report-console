<?php

namespace App\Service\Sms;

interface SmsProvider
{
    function send($mobile);
    function getType();
}