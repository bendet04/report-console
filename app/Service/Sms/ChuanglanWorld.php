<?php
/**
 * 创蓝253短信
 * @author  chenwr
 * @since   2018年5月9日
 * @version v1.0
 */

namespace App\Service\Sms;

use App\Consts\Platform;
use Illuminate\Support\Facades\Log;


class ChuanglanWorld implements SmsProvider
{

    private $key = '';
    private $secret =  '';
    private $from = '';
    private $channel = '';
    private $text = "";
    private $type = '';
    const URL = 'http://intapi.253.com/send/json';


    private function channelInit() {
        curl_setopt($this->channel, CURLOPT_HEADER, false);
        curl_setopt($this->channel, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->channel, CURLOPT_TIMEOUT, 5);
        curl_setopt($this->channel, CURLOPT_HTTPHEADER, [
                       'Content-Type: application/json;charset=utf-8'
                   ]
               );

//        curl_setopt($this->channel, CURLOPT_RETURNTRANSFER, true);
    }

    /**
     * Paasso constructor.
     * @param $type 1验证通道 2营销通道
     */
    public function __construct($type)
    {

        $this->type = $type;
        $this->channel = curl_init();
        $this->channelInit();
    }

    public function setText($text) {
        $this->text = $text;
    }

    public function setPlatform($platform) {
        $this->from = env($platform.'_WORLD_FROM');
        $this->key = env($platform.'_WORLD_KRY');
        $this->secret = env($platform.'_WORLD_SERECT');
        $senderId = Platform::alias($platform);
        $this->text = str_replace('SenderId', $senderId, $this->text);
    }

    public function send($mobile)
    {
        $messageId = "";

        $postdata = [
            'account'  =>  $this->key,
            'password' => $this->secret,
            'msg' => $this->text,
            'mobile' => $mobile,
            'senderId' => $this->from,
        ];
        $postdata = json_encode($postdata);
        curl_setopt($this->channel, CURLOPT_URL, self::URL);
        curl_setopt($this->channel, CURLOPT_POST, true);
        curl_setopt($this->channel, CURLOPT_POSTFIELDS, $postdata);
        $ret = curl_exec($this->channel);
        $error = curl_errno($this->channel);

        if (0 != $error)
            Log::info("Send SMS network error: ". curl_error($this->channel));
        else {
            $ret = json_decode($ret);
            if (!empty($ret)) {
                if (0 != $ret->code)
                    Log::info("Send chuanglan SMS error:{$ret->error}". $ret->code);
                else
                    $messageId = $ret->msgid;
            } else {
                Log::info("Send chuanglan SMS error:JSON error");
            }
        }

        curl_close($this->channel);

        return $messageId;

    }

    public function getType() {
        return $this->type;
    }
}