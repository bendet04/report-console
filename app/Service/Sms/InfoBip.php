<?php
/**
 * Created by PhpStorm.
 * User: xuan
 * Date: 2019/1/10
 * Time: 下午1:53
 */

namespace App\Service\Sms;

use App\Consts\Platform;
use Illuminate\Support\Facades\Log;

class InfoBip implements SmsProvider
{
    /**
     * InfoBip constructor.
     * @param $type 1验证通道 2营销通道
     */
    private $key = '';
    private $secret =  '';
    private $from = '';
    private $channel = '';
    private $text = "";
    private $type = '';
    const URL = 'https://4rw8m.api.infobip.com/sms/1/text/single';
    
    
    private function channelInit()
    {
        curl_setopt($this->channel, CURLOPT_HEADER, false);
        curl_setopt($this->channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->channel, CURLOPT_TIMEOUT, 5);
    }
    
    /**
     * Paasso constructor.
     * @param $type 1验证通道 2营销通道
     */
    public function __construct($type)
    {
        
        $this->type = $type;
        $this->channel = curl_init();
        $this->channelInit();
    }
    
    public function setText($text)
    {
        $this->text = $text;
    }
    
    public function setFrom($platform)
    {
        $this->from = env($platform.'_AUTH_SENDER');
        $this->name = env($platform.'_AUTH_NAME');
        $this->pwd = env($platform.'_AUTH_PWD');
        $senderId = Platform::alias($platform);
        $this->text = str_replace('SenderId', $senderId, $this->text);
    }
    
    public function send($mobile)
    {
        $messageId  = "";
        $headers[]  =  "Accept:application/json";
        $headers[]  =  'Content-Type: application/json';
        $headers[]  =  "Authorization: Basic ". base64_encode($this->name.':'.$this->pwd);
        $postdata = [
            'from'  =>  $this->from,
            'to' => $mobile,
            'text' => $this->text,
        ];
        $postdata = json_encode($postdata);
        curl_setopt($this->channel, CURLOPT_URL, self::URL);
        curl_setopt($this->channel, CURLOPT_POST, true);
        curl_setopt($this->channel, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->channel, CURLOPT_POSTFIELDS, $postdata);
    
        $ret = curl_exec($this->channel);
        $error = curl_errno($this->channel);
        if (0 != $error) {
            Log::info("Send SMS network error: ". curl_error($this->channel));
        } else {
            $ret = json_decode($ret);
            if (!empty($ret)) {
                if (0 == $ret->messages[0]->status->groupId) {
                    Log::info("Send InfoBip SMS error:
                              {$ret->messages[0]->status->groupId}". $ret->messages[0]->status->groupId);
                } else {
                    $messageId = $ret->messages[0]->messageId;
                }
            } else {
                Log::info("Send InfoBip SMS error:JSON error");
            }
        }
        
        curl_close($this->channel);
        
        return $messageId;
    }
    
    public function getType()
    {
        return $this->type;
    }
}
