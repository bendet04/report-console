<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/8/16/016
 * Time: 20:04
 */

namespace App\Service;

use Illuminate\Support\Facades\Facade;

class DokuService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Pay\DokuService';
    }
}