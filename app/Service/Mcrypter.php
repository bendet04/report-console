<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static encrypt(string $data)
 * @method static decrypt(string $data)
 */
class Mcrypter extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\Mcrypter';
    }
}
