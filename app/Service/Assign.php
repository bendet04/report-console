<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static assign(int $order_id, string $type)
 */
class Assign extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\Assign';
    }
}
