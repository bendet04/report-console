<?php

namespace App\Service;

use Illuminate\Support\Facades\Facade;

/**
 * @method static pushTrialSuccess($to)
 * @method static pushTrialFailure($to)
 * @method static pushPayment($to, $card)
 * @method static pushRepayment($to, $total)
 */
class PushSevice extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Service\Foundation\PushSevice';
    }
}
