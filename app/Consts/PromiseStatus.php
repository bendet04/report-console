<?php
/**
 * Created by PhpStorm.
 * User: herry
 * Date: 2018/11/29
 * Time: 11:07
 */

namespace App\Consts;


class PromiseStatus
{
    const WAIT_FOR_PAY      = 1;  //未缴纳
    const ALREADY_PAY       = 2;  //已缴纳
    const WAIT_FOR_TRAIL    = 3;  //提取待审核
    const WAIT_PAYMENT      = 4;  //审核放款中
    const APPLY_CANCEL      = 5;  //取消申请
    const PAYMENT_SUCCESS   = 6;  //提现成功
    const PAYMENT_FAILURE   = 7;  //提现失败
}
