<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/3/23/023
 * Time: 13:22
 */

namespace App\Consts;


class CompanyType
{
    const LEVER1 = 1;
    const LEVER2 = 2;
    const LEVER3 = 3;
    const LEVER4 = 4;

    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }

        $arrStr = [
            self::LEVER1 => 'Milik negara',//国有
            self::LEVER2 => 'PMA',//外资
            self::LEVER3 => 'Swasta',//私营
            self::LEVER4 => 'Corporate',//合资

        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}