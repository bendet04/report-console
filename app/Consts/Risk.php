<?php
namespace App\Consts;

class Risk
{
    // NPWP
    const REFUSER1 = 'RS_SCORE_NPWP_WHETHER_OR_NOT_TO_AUTHORIZE_NPWP';
    const REFUSER2 = 'RS_SCORE_NPWP_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER3 = 'RS_SCORE_NPWP_IS_CONSISTENT_WITH_THE_NAME';
    const REFUSER4 = 'RS_SCORE_NPWP_IS_CONSISTENT_WITH_THE_EMAIL';
    const REFUSER5 = 'RS_SCORE_NPWP_REAL_INCOME';

    // TELKOMSEL
    const REFUSER11= 'RS_SCORE_NPWP_WHETHER_OR_NOT_TO_AUTHORIZE_TELKOMSEL';
    const REFUSER12= 'RS_SCORE_TELKOMSEL_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER13= 'RS_SCORE_TELKOMSEL_CREDITS';
    const REFUSER14= 'RS_SCORE_TELKOMSEL_POINTS';
    const REFUSER15 = 'RS_SCORE_TELKOMSEL_EXPIRE_DATE';

    // GOJEK
    const REFUSER51 = 'RS_SCORE_GOJEK_WHETHER_OR_NOT_TO_AUTHORIZE_GOJEK';
    const REFUSER52 = 'RS_SCORE_GOJEK_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER53 = 'RS_SCORE_GOJEK_IS_CONSISTENT_WITH_THE_EMAIL';
    const REFUSER54 = 'RS_SCORE_GOJEK_POINTS';
    const REFUSER55 = 'RS_SCORE_GOJEK_ACTIVE';


   //LAZADA
    const REFUSER21 = 'RS_SCORE_LAZADA_WHETHER_OR_NOT_TO_AUTHORIZE_LAZADA';
    const REFUSER22 = 'RS_SCORE_LAZADA_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER23 = 'RS_SCORE_LAZADA_IS_CONSISTENT_WITH_THE_NAME';
    const REFUSER24 = 'RS_SCORE_LAZADA_MATCH_ADDRESS';
    const REFUSER25 = 'RS_LAZADA_GOJEK_IS_CONSISTENT_WITH_THE_EMAIL';


//    Tokopedia


    const REFUSER31 = 'RS_SCORE_TOKOPEDIA_WHETHER_OR_NOT_TO_AUTHORIZE_TOKOPEDIA';
    const REFUSER32 = 'RS_SCORE_TOKOPEDIA_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER33 = 'RS_SCORE_TOKOPEDIA_IS_CONSISTENT_WITH_THE_NAME';
    const REFUSER34 = 'RS_SCORE_TOKOPEDIA_MATCH_ADDRESS';
    const REFUSER35 = 'RS_LAZADA_TOKOPEDIA_IS_CONSISTENT_WITH_THE_EMAIL';




//    facebook
    const REFUSER41 = 'RS_SCORE_FACEBOOK_WHETHER_OR_NOT_TO_AUTHORIZE_FACEBOOK';
    const REFUSER42 = 'RS_SCORE_FACEBOOK_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER43 = 'RS_SCORE_FACEBOOK_MATCH_WORK_ADDRESS';
    const REFUSER44 = 'RS_SCORE_FACEBOOK_MATCH_LAST_LOGIN_ADDRESS';

//    instagram 61-70
    const REFUSER61 = 'RS_SCORE_INSTAGRAM_WHETHER_OR_NOT_TO_AUTHORIZE_INSTAGRAM';
    const REFUSER62 = 'RS_SCORE_INSTAGRAM_IS_CONSISTENT_WITH_THE_PHONE_NUMBER';
    const REFUSER63 = 'RS_SCORE_INSTAGRAM_HOW_MANY_CONTACTS';

//  还款意愿分析 70
    const REFUSER71 = 'RS_SCORE_OTHER_CARDS';
    const REFUSER72 = 'RS_SCORE_PHONE_CONSISTENCY';
    const REFUSER73 = 'RS_SCORE_APP_CONTAIN_LOAN_KEYWORDS';
    const REFUSER74 = 'RS_SCORE_APP_NOT_CONTAIN_SENSTIVE_KEYWORDS';
    const REFUSER75 = 'RS_SCORE_SMS_CONTAIN_PAY_OFF';
    const REFUSER76 = 'RS_SCORE_SMS_CONTAIN_RECEIPT_OF_PAYMENT';
    const REFUSER77 = 'RS_SCORE_SMS_CONTAIN_SUCCESSFUL_PAYMENT';
    const REFUSER78 = 'RS_SCORE_SMS_NOT_CONTAIN_SENSTIVE_KEYWORDS';
    const REFUSER79 = 'RS_SCORE_ADDRESSBOOK_CONTAIN_SENSTIVE_KEYWORDS';
    const REFUSER80= 'RS_SCORE_ADDRESSBOOK_CONTACTS_NUM';
    const REFUSER81= 'RS_SCORE_CONTACT_HAS_PARENT';
    const REFUSER82= 'RS_SCORE_CROSS_SMS_ADDRESSBOOK';
    const REFUSER83= 'RS_SCORE_CROSS_CONTACT_CALLOG';
    const REFUSER84= 'RS_SCORE_CROSS_ADDRESSBOOK_CALLOG';
    const REFUSER85= 'RS_SCORE_Cross_SMS_And_Contact';//短信手机号和联系人1或2重合
    const REFUSER86= 'RS_SCORE_Cross_Addressbook_And_Contact';//通讯录与联系人1,2都重合，4分
    const REFUSER87= 'RS_SCORE_CHECK_OVERDUE_SMS';//短信中包换逾期相关关键字逾期天数评分
    const REFUSER88= 'RS_SCORE_CROSS_SMS_AND_CALLOG_AND_ADDRESSBOOK';//通讯录,通话记录，与短信联系人重合数评分

//用户模型分析
    const REFUSER91 = 'RS_SCORE_MARITAL_STATUS';
    const REFUSER92 = 'RS_SCORE_GENDER';
    const REFUSER93 = 'RS_SCORE_AGE';
    const REFUSER94 = 'RS_SCORE_PURPOSE';
    const REFUSER95 = 'RS_SCORE_DEGREE';
    const REFUSER96 = 'RS_SCORE_RESIDENCE_LENGTH';
    const REFUSER97 = 'RS_SCORE_OCCUPATION';
    const REFUSER98 = 'RS_SCORE_WORKING_LIFE';
    const REFUSER99 = 'RS_SCORE_INCOMING';
    const REFUSER100 = 'RS_SCORE_SPOUSE_SITUATION';
    
 //三方数据交叉评分
    const REFUSER110 = 'RS_SCORE_CROSS_NPWP_AND_TELKOMSEL_AND_ORDER_PHONE';
    const REFUSER111 = 'RS_SCORE_CROSS_GOJEK_AND_LAZADA_AND_TOKOPEDIA_AND_ORDER_PHONE';
    const REFUSER112 = 'RS_SCORE_CROSS_FACEBOOK_AND_INSTAGRAM_AND_ORDER_PHONE';
    const REFUSER113 = 'RS_SCORE_CROSS_EXTRA_DATA_TOKEN';
    
    //财富与现金贷
    const REFUSER120 = 'RS_SCORE_WHETHER_CASHLOAN_EXCESSIVE';
    const REFUSER121 = 'RS_SCORE_CHECK_LOAD_SMS';
    const REFUSER122 = 'RS_SCORE_PLATFORMS_IN_SMS_AND_APP';
    const REFUSER123 = 'RS_SCORE_INCOME_AND_PAYROLL';
    const REFUSER124 = 'RS_SCORE_INCOME_AND_AMOUNT';
    const REFUSER125 = 'RS_SCORE_WHITE_LIST_AMOUNT';
    const REFUSER126 = 'RS_SCORE_RECHARGE_AMOUNT';
    
    



    public static function toString($code)
    {
        $arrStr = [
//            NPWP
            self::REFUSER1 => 'sub_npwp',
            self::REFUSER2 => 'npwp_tel',
            self::REFUSER3 => 'npwp_name',
            self::REFUSER4 => 'npwp_email',
            self::REFUSER5 => 'npwp_income',

            // TELKOMSEL
            self::REFUSER11 => 'sub_telkomsel',
            self::REFUSER12 => 'telkomsel_tel',
            self::REFUSER13 => 'telkomsel_credits',
            self::REFUSER14 => 'telkomsel_points',
            self::REFUSER15 => 'telkomsel_expire',


            //LAZADA
            self::REFUSER21 => 'sub_lazada',
            self::REFUSER22 => 'lazada_tel',
            self::REFUSER23 => 'lazada_name',
            self::REFUSER24 => 'lazada_address',
            self::REFUSER25 => 'lazada_email',


            //Tokopedia
            self::REFUSER31 => 'sub_tokopedia',
            self::REFUSER32 => 'tokopedia_tel',
            self::REFUSER33 => 'tokopedia_name',
            self::REFUSER34 => 'tokopedia_address',
            self::REFUSER35 => 'tokopedia_email',

            //    facebook
            self::REFUSER41 => 'sub_facebook',
            self::REFUSER42 => 'facebook_tel',
            self::REFUSER43 => 'facebook_work',
            self::REFUSER44 => 'facebook_last_login',

            // GOJEK
            self::REFUSER51 => 'sub_gojek',
            self::REFUSER52 => 'gojek_tel',
            self::REFUSER53 => 'gojek_email',
            self::REFUSER54 => 'gojek_point',
            self::REFUSER55 => 'gojek_number',

            //    instagram 61-70
            self::REFUSER61 => 'sub_instagram',
            self::REFUSER62 => 'instagram _tel',
            self::REFUSER63 => 'instagram _number',

            //  还款意愿分析
            self::REFUSER71 => 'othrt_cards',
            self::REFUSER72 => 'sub_call_bank',
            self::REFUSER73 => 'app_loan_keyword',
            self::REFUSER74 => 'app_not_keywords',
            self::REFUSER75 => 'sms_pay_off',
            self::REFUSER76 => 'sms_received',
            self::REFUSER77 => 'sms_ok_pay',
            self::REFUSER78 => 'sms_keyword',
            self::REFUSER79 => 'ab_keywords',
            self::REFUSER80 => 'ab_num',
            self::REFUSER81 => 'has_parent',
            self::REFUSER82 => 'sms_ab',
            self::REFUSER83 => 'person_call',
            self::REFUSER84 => 'ab_call',
            self::REFUSER85 => 'sms_concat',
            self::REFUSER86 => 'ab_concat',
            self::REFUSER87 => 'overdue_sms',
            self::REFUSER88 => 'callog_addressbook',

            //用户模型分析
            self::REFUSER91 => 'marry',
            self::REFUSER92 => 'sex',
            self::REFUSER93 => 'age',
            self::REFUSER94 => 'purpose',
            self::REFUSER95 => 'degree',
            self::REFUSER96 => 'live_lenght',
            self::REFUSER97 => 'work',
            self::REFUSER98 => 'work_length',
            self::REFUSER99 => 'income',
            self::REFUSER100 => 'spouse',
        
            //三方交叉数据
            self::REFUSER110 => 'np_concat_telkomse',
            self::REFUSER111 => 'gojek_lazada_toko',
            self::REFUSER112 => 'fabk_inst',
            self::REFUSER113 => 'threeDegree',
            
            //财富与现金贷
            self::REFUSER120 => 'sms_contains_cash',
            self::REFUSER121 => 'loan_success',
            self::REFUSER122 => 'sa_loan',
            self::REFUSER123 => 'inCome_whether_wages',
            self::REFUSER124 => 'inCome_loan_amount',
            self::REFUSER125 => 'oldUser_applyAmount',
            self::REFUSER126 => 'amount_reminder',

        ];
        if(empty($arrStr[$code])){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}