<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 17:27
 */

namespace App\Consts;


class Salary
{
    const SALARY1 = 1;
    const SALARY2 = 2;

    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::SALARY1 => '周薪',
            self::SALARY2 => '月薪',

        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}