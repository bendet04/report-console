<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/5/27/027
 * Time: 21:10
 */

namespace App\Consts;

class LoanRate
{
    const RATE_M0 = 1;
    const RATE_M1 = 2;
    const RATE_M2 = 3;
    const RATE_M3 = 4;
    const RATE_M4 = 5;

    public static function toString($type)
    {
        $arrStr = [
            self::RATE_M0 => '1-3' . __('message.day'),
            self::RATE_M1 => '4-7' . __('message.day'),
            self::RATE_M2 => '8-14' . __('message.day'),
            self::RATE_M3 => '15-21' . __('message.day'),
            self::RATE_M4 => '22' . __('basic.up'),
        ];

        if(empty($type)){
            return 'Unknown';
        }
        return $arrStr[$type];
    }

}