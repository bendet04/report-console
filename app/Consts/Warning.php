<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/28/028
 * Time: 13:51
 */

namespace App\Consts;


class Warning
{
//    提醒的定义
    const WARN1 = 1;
    const WARN2 = 2;
    public static function toString($code)
    {
        $arrStr = [
            self::WARN1 => '客户婚姻状况不一致',
            self::WARN2 => '',
        ];
        if(empty($code)){
            return 'Unkown';
        }
        return $arrStr[$code];
    }
}