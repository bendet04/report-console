<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/3/13/013
 * Time: 11:46
 */

namespace App\Consts;


class PaymentFlowStatus
{
    //婚姻
    const PAYMENT0 = 0;
    const PAYMENT1 = 1;
    const PAYMENT2 = 2;
    const PAYMENT3 = 3;
    const PAYMENT4 = 4;


    public static function toString($code)
    {
        $arrStr = [
            self::PAYMENT0 => 'Tidak diaudit', //'还未审核',
            self::PAYMENT1 => 'Mulai transfer',//'发起转账',
            self::PAYMENT2 => 'Kesalahan informasi bank',//'银行信息错误',
            self::PAYMENT3 => 'Transaksi berhasil',//'交易成功',
            self::PAYMENT4 => 'Kegagalan transaksi',//'交易失败',
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}