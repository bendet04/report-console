<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 16:58
 */

namespace App\Consts;


class TrialResult
{
    const WAIT_TRIAL = 0;
    const PASS_TRIAL = 1;
    const FAIL_TRIAL = 2;
    const MESSAGE_TRIAL_SUCCESS   = 19;     // 信审成功
    const MESSAGE_TRIAL_FAILURE   = 20;     // 信审失败
    const PHONE_TRIAL_SUCCESS     = 21;     // 电审成功
    const PHONE_TRAIL_FAILURE     = 22;     // 电审失败

    public static function toString($code)
    {
        $arrStr = [
            self::WAIT_TRIAL=>   'Tidak diaudit',            //'未审核',
            self::PASS_TRIAL=> 'Audit berlalu',              //'审核通过',
            self::FAIL_TRIAL=> 'Tolak',                    //'拒绝',
            self::MESSAGE_TRIAL_SUCCESS=> 'Audit berlalu',   //'审核通过',
            self::MESSAGE_TRIAL_FAILURE=> 'Tolak',         //'拒绝',
            self::PHONE_TRIAL_SUCCESS=> 'Audit berlalu',     //'审核通过',
            self::PHONE_TRAIL_FAILURE=> 'Tolak',           //'拒绝',

        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}