<?php
namespace App\Consts;

class OrderStatus
{
 
	public static function toString($code)
	{
		$arrStr = [
            1  => __('status.status1'),//'创建待确认',
            2  => __('status.status2'),//'两分钟用户取消',
            16 => __('status.status16'),//'风控队列中',
            17 => __('status.status17'),//'待信审',
            18 => __('status.status18'),//'风控失败',
            19 => __('status.status19'),   //'信审成功',
            20 => __('status.status20'),       //'信审失败',
            21 => __('status.status21'),//'电审成功',
            22 => __('status.status22'),//'电审失败',
            5  => __('status.status5'),//'终审失败',
            6  => __('status.status6'),//'终审成功',
            23 => __('status.status23'),//'用户确认审核失败',
            7  => __('status.status7'),//'放款审核未通过',
            8  => __('status.status8'),//'放款中',
            9  => __('status.status9'),//'还款前一天',
            10 => __('status.status10'),//'缓期中',
            11 => __('status.status11'),//'已逾期',
            12 => __('status.status12'),//'己还清',
            13 => __('status.status13'),//'部分还清',
            14 => __('status.status14'),//'还款中',
            15 => __('status.status15'),//'放款失败',//（等待重新放款）
            24 => __('status.status24'),//'终止放款',
            25 => __('status.status25'),//'贷款到期',
		];
		if(empty($arrStr)){
			return 'Unknown';
		}
		return $arrStr[$code];
	}

    public static function toNumber($str)
    {
        $arrStr = [
            'WAIT_CONFIRM'        => 1,
            'USER_CANCEL'         => 2,
            'WIND_LIST'           => 16,
            'WAIT_FOR_MESSAGE'    => 17,
            'WINF_FAILURE'        => 18,
            'MESSAHE_SUCCESS'     => 19,
            'MESSAGE_FAILURE'     => 20,
            'PHONE_SUCCESS'       => 21,
            'PHONE_FAILURE'       => 22,
            'FINAL_FAILURE'       => 5,
            'FINAL_SUCCESS'       => 6,
            'USER_COMFIRM_FAILURE'=> 23,
            'PAYMENT_TRIAL_FAIL'  => 7,
            'PAYMENTING'          => 8,
            'LOAN_OVER_DUE'       => 11,
            'LOAN_FINISH'         => 12,
            'LOAN_PART_FINISH'    => 13,
            'WAIT_REPAYMENT'      => 14,
            'PAYMENT_FAILURE'     => 15,//（等待重新放款）
            'STOP_PAYMENT'        => 24,
            'LOAN_DEADLINE'       => 25,
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$str];
    }

}