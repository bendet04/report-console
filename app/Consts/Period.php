<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/25/025
 * Time: 19:25
 */
//居住市场
namespace App\Consts;

class Period
{

    //时间的类型g
    const PERIOD1 = 1;
    const PERIOD2 = 2;
    const PERIOD3 = 3;
    const PERIOD4 = 4;
    const PERIOD5 = 5;


    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::PERIOD1=> 'TIGA_BULAN',//三个月
            self::PERIOD2=> 'ENAM_BULAN',//六个月
            self::PERIOD3=> 'SATU_TAHUN',//一年以下
            self::PERIOD4=> 'DUA_TAHUN',//两年以下
            self::PERIOD5=> 'DI_ATAS_DUA_TAHUN',//两年以上
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}