<?php
namespace App\Consts;

class Refuse
{
    // 政策不符
    const REFUSER11 = 'R0101';
    const REFUSER12 = 'R0102';
    const REFUSER13 = 'R0103';
    const REFUSER14 = 'R0104';
    const REFUSER15 = 'R0105';


//信息不完整
    const REFUSER21 = 'R0201';
    const REFUSER22 = 'R0202';
    const REFUSER23 = 'R0203';
    const REFUSER24 = 'R0204';
    const REFUSER25 = 'R0205';
    const REFUSER26 = 'R0206';
    const REFUSER27 = 'R0207';
    const REFUSER28 = 'R0208';

//    欺诈
    const REFUSER31 = 'R0301';
    const REFUSER32 = 'R0302';
    const REFUSER33 = 'R0303';
    const REFUSER34 = 'R0304';



//    负面信息
    const REFUSER41 = 'R0401';
    const REFUSER42 = 'R0402';
    const REFUSER43 = 'R0403';
    const REFUSER44 = 'R0404';
    const REFUSER45 = 'R0405';
    const REFUSER46 = 'R0406';

//    **********************转换成文字******************************** //

    //            政策不符
    const CODEREFUSER11 = 'qualikasi tidak cocok';
    const CODEREFUSER12 = 'pekerjaan terbatas';
    const CODEREFUSER13 = 'tidak punya pekerjaan';
    const CODEREFUSER14 = 'informasi kantor salah';
    const CODEREFUSER15 = 'pernah menggunakan informasi palsu';

    //            信息不完整
    const CODEREFUSER21 = 'video tidak memenuhi persyaratan';
    const CODEREFUSER22 = 'KTP tidak berlaku';
    const CODEREFUSER23 = 'user tidak dapat dihubungi';
    const CODEREFUSER24 = 'kontak person tidak dapat dihubungi';
    const CODEREFUSER25 = 'nomor ponsel tidak aktif';
    const CODEREFUSER26 = 'kontak person tidak mau kerjasama';
    const CODEREFUSER27 = 'info dari kontak person tidak berfungsi';
    const CODEREFUSER28 = 'lain';

    //            欺诈
    const CODEREFUSER31 = 'KTP yang palsu';
    const CODEREFUSER32 = 'informasi pekerjaan yang palsu';
    const CODEREFUSER33 = 'bukan user sendiri yang mengajukan perohonan pinjaman';
    const CODEREFUSER34 = 'agen yang membantu pengajunan';


    //            负面信息
    const CODEREFUSER41 = 'pekerjaannya tidak dapat dikonfirmasi';
    const CODEREFUSER42 = 'user punya informasi negatif';
    const CODEREFUSER43 = 'informasi kontak person tidak benar';
    const CODEREFUSER44 = 'kontak person tidak mengenal user';
    const CODEREFUSER45 = 'pekerjaan user bagian peminjaman juga';
    const CODEREFUSER46 = 'buku kontak ponsel tidak normal';

    //    **********************转换成文字******************************** //
    public static function toString($code)
    {
        $arrStr = [
//            政策不符
            self::REFUSER11 => 'qualikasi tidak cocok',
            self::REFUSER12 => 'pekerjaan terbatas',
            self::REFUSER13 => 'tidak punya pekerjaan',
            self::REFUSER14 => 'informasi kantor salah',
            self::REFUSER15 => 'pernah menggunakan informasi palsu',

//            信息不完整
            self::REFUSER21 => 'video tidak memenuhi persyaratan',
            self::REFUSER22 => 'KTP tidak berlaku',
            self::REFUSER23 => 'user tidak dapat dihubungi',
            self::REFUSER24 => 'kontak person tidak dapat dihubungi',
            self::REFUSER25 => 'nomor ponsel tidak aktif',
            self::REFUSER26 => 'kontak person tidak mau kerjasama',
            self::REFUSER27 => 'info dari kontak person tidak berfungsi',
            self::REFUSER28 => 'lain',

//            欺诈
            self::REFUSER31 => 'KTP yang palsu',
            self::REFUSER32 => 'informasi pekerjaan yang palsu',
            self::REFUSER33 => 'bukan user sendiri yang mengajukan perohonan pinjaman',
            self::REFUSER34 => 'agen yang membantu pengajunan',


//            负面信息
            self::REFUSER41 => 'pekerjaannya tidak dapat dikonfirmasi',
            self::REFUSER42 => 'user punya informasi negatif',
            self::REFUSER43 => 'informasi kontak person tidak benar',
            self::REFUSER44 => 'kontak person tidak mengenal user',
            self::REFUSER45 => 'pekerjaan user bagian peminjaman juga',
            self::REFUSER46 => 'buku kontak ponsel tidak normal',
        ];
        if(empty($arrStr[$code])){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

    //*****************************转换成代码********************************************//
    public static function toCode($str)
    {
        $arrStr = [
//            政策不符
            self::CODEREFUSER11 => 'R0101',
            self::CODEREFUSER12 => 'R0102',
            self::CODEREFUSER13 => 'R0103',
            self::CODEREFUSER14 => 'R0104',
            self::CODEREFUSER15 => 'R0105',


        //信息不完整
            self::CODEREFUSER21 => 'R0201',
            self::CODEREFUSER22 => 'R0202',
            self::CODEREFUSER23 => 'R0203',
            self::CODEREFUSER24 => 'R0204',
            self::CODEREFUSER25 => 'R0205',
            self::CODEREFUSER26 => 'R0206',
            self::CODEREFUSER27 => 'R0207',
            self::CODEREFUSER28 => 'R0208',

        //    欺诈
            self::CODEREFUSER31 => 'R0301',
            self::CODEREFUSER32 => 'R0302',
            self::CODEREFUSER33 => 'R0303',
            self::CODEREFUSER34 => 'R0304',


        //    负面信息
            self::CODEREFUSER41 => 'R0401',
            self::CODEREFUSER42 => 'R0402',
            self::CODEREFUSER43 => 'R0403',
            self::CODEREFUSER44 => 'R0404',
            self::CODEREFUSER45 => 'R0405',
            self::CODEREFUSER46 => 'R0406',

        ];
        if(empty($arrStr[$str])){
            return 'Unknown';
        }
        return $arrStr[$str];
    }


}