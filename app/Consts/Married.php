<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 15:56
 */

namespace App\Consts;


class Married
{
    //婚姻
    const MARRIED1 = 1;
    const MARRIED2 = 2;
    const MARRIED3 = 3;
    const MARRIED4 = 4;


    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::MARRIED1 => 'Single',//未婚
            self::MARRIED2 => 'Telah menikah',//已婚
            self::MARRIED3 => 'Cerai',//离异
            self::MARRIED4 => 'Duda/janda',//丧偶
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}