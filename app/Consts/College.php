<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 15:50
 */

namespace App\Consts;

class College
{

    //学历
    const COLLEGE1 = 1;
    const COLLEGE2 = 2;
    const COLLEGE3 = 3;
    const COLLEGE4 = 4;
    const COLLEGE5 = 5;
    const COLLEGE6 = 6;
    const COLLEGE7 = 7;
    const COLLEGE8 = 8;
    const COLLEGE9 = 9;



    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::COLLEGE1 => 'Diploma 1',//一年
            self::COLLEGE2 => 'Diploma 2',//两年
            self::COLLEGE3 => 'Diploma 3',//三年
            self::COLLEGE4 => 'SD',//小学
            self::COLLEGE5 => 'SLTP',//中学
            self::COLLEGE6 => 'SLTA',//高中
            self::COLLEGE7 => 'S1',//学士
            self::COLLEGE8 => 'S2',//硕士
            self::COLLEGE9 => 'S3',//博士

        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}