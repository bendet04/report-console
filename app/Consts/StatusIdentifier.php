<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 15:50
 */

namespace App\Consts;

class StatusIdentifier
{
    //电审返回处理状态
    const DATA_ABNORMITY = 0;  //数据异常
    const NEXT_ORDER = 1;  //下一个订单
    const RETURN_LIST = 2;  //返回工作电核
    const PHONE_LIST = 3;  //返回电审列表页
    const HAS_BEEN_AUDITED = 11;  //已被审核
    
    public static $returnList = [
        self::HAS_BEEN_AUDITED,
        self::PHONE_LIST,
    ];
}
