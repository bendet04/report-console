<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 17:30
 */

namespace App\Consts;


class Relative
{
//    联系人1
    const FIRST1 = 1;
    const FIRST2 = 2;
    const FIRST3 = 3;
    const FIRST4 = 4;
    const FIRST5 = 5;

//    联系人2
    const SECOND1 = 1;
    const SECOND2 = 2;
    const SECOND3 = 3;
    const SECOND4 = 4;
    const SECOND5 = 5;
    
//    联系人3
    const CONTACT_THREE_TYPE1 = 1;
    const CONTACT_THREE_TYPE2 = 2;
    const CONTACT_THREE_TYPE3 = 3;
    const CONTACT_THREE_TYPE4 = 4;
    const CONTACT_THREE_TYPE5 = 5;
    
//    联系人4
    const CONTACT_FOURTH_TYPE1 = 1;
    const CONTACT_FOURTH_TYPE2 = 2;
    const CONTACT_FOURTH_TYPE3 = 3;
    const CONTACT_FOURTH_TYPE4 = 4;
    const CONTACT_FOURTH_TYPE5 = 5;
    
    //    联系人5
    const CONTACT_FIFTH_TYPE1 = 1;
    const CONTACT_FIFTH_TYPE2 = 2;
    const CONTACT_FIFTH_TYPE3 = 3;
    const CONTACT_FIFTH_TYPE4 = 4;
    const CONTACT_FIFTH_TYPE5 = 5;

    public static function toFirstString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::FIRST1 => 'Ayah/ibu',//父亲/母亲
            self::FIRST2 => 'Anak laki-laki/anak perempuan',//儿子/女儿
            self::FIRST3 => 'Pasangan',//配偶
            self::FIRST4 => 'Lain-lain',//其他
            self::FIRST5 => 'Saudara kandung',//兄弟姐妹


        ];
        if (empty($arrStr[$code])) {
            return 'Unknown';
        }
        return $arrStr[$code];
    }

    public static function toSecondString($code)
    {
        if (empty($code)) {
            return '未选择';
        }
        $arrStr = [
            self::SECOND1 => 'Ayah/ibu',//父亲/母亲
            self::SECOND2 => 'Anak laki-laki/anak perempuan',//儿子/女儿
            self::SECOND3 => 'Pasangan',//配偶
            self::SECOND4 => 'Lain-lain',//其他
            self::SECOND5 => 'Saudara kandung',//兄弟姐妹
        ];
        if (empty($arrStr[$code])) {
            return 'Unknown';
        }
        return $arrStr[$code];
    }
    
    public static function toThirdString($code)
    {
        if (empty($code)) {
            return '';
        }
        $arrStr = [
            self::CONTACT_THREE_TYPE1 => 'Ayah/ibu',//父亲/母亲
            self::CONTACT_THREE_TYPE2 => 'Anak laki-laki/anak perempuan',//儿子/女儿
            self::CONTACT_THREE_TYPE3 => 'Pasangan',//配偶
            self::CONTACT_THREE_TYPE4 => 'Lain-lain',//其他
            self::CONTACT_THREE_TYPE5 => 'Saudara kandung',//兄弟姐妹
        ];
        if (empty($arrStr[$code])) {
            return 'Unknown';
        }
        return $arrStr[$code];
    }
    public static function toFourthString($code)
    {
        if (empty($code)) {
            return '';
        }
        $arrStr = [
            self::CONTACT_FOURTH_TYPE1 => 'Ayah/ibu',//父亲/母亲
            self::CONTACT_FOURTH_TYPE2 => 'Anak laki-laki/anak perempuan',//儿子/女儿
            self::CONTACT_FOURTH_TYPE3 => 'Pasangan',//配偶
            self::CONTACT_FOURTH_TYPE4 => 'Lain-lain',//其他
            self::CONTACT_FOURTH_TYPE5 => 'Saudara kandung',//兄弟姐妹
        ];
        if (empty($arrStr[$code])) {
            return 'Unknown';
        }
        return $arrStr[$code];
    }
    public static function toFifthString($code)
    {
        if (empty($code)) {
            return '';
        }
        $arrStr = [
            self::CONTACT_FIFTH_TYPE1 => 'Ayah/ibu',//父亲/母亲
            self::CONTACT_FIFTH_TYPE2 => 'Anak laki-laki/anak perempuan',//儿子/女儿
            self::CONTACT_FIFTH_TYPE3 => 'Pasangan',//配偶
            self::CONTACT_FIFTH_TYPE4 => 'Lain-lain',//其他
            self::CONTACT_FIFTH_TYPE5 => 'Saudara kandung',//兄弟姐妹
        ];
        if (empty($arrStr[$code])) {
            return 'Unknown';
        }
        return $arrStr[$code];
    }
    
    
}