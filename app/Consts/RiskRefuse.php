<?php
namespace App\Consts;

class RiskRefuse
{
    
    public static function toString($code,$refuse='')
    {
        $arrStr = [
            'RC_REDC'       => '检查额外数据拒绝',
            'RC_RRAC'       => '检查风险地区拒绝',
            'RC_RAC'        => '检查年龄拒绝',
            'RC_RROC'       => '检查职业拒绝',
            'RC_RRMSC'      => '检查婚姻状态拒绝',
            'RC_RRDC'       => '检查学位拒绝',
            'RC_RCNC'       => '检查孩子数量拒绝',
            'RC_RRLC'       => '检查居住时长拒绝',
            'RC_WLC'        => '检查工作时长拒绝',
            'RC_PDC'        => '检查发薪日拒绝',
            'RC_ILC'        => '检查收入水平拒绝',
            'RC_AC'         => '检查手机APP拒绝',
            'RC_AC_ANC'     => '检查App数量拒绝',
            'RC_AC_ANIDONC' => '检查是否安装driver相关app拒绝',
            'RC_AC_ANIPONC' => '检查是否安装了Poker相关拒绝',
            'RC_AC_ANILONC' => '检查是否安装了Loan相关拒绝',
            'RC_SC'         => '检查短信数量拒绝',
            'RC_SC_SILONC'  => '检查包含loan短信数量拒绝',
            'RC_SC_SMNC'    => '检查短信数量（20条）拒绝',
            'RC_SC_SNIBC'   => '检查短信号码在黑名单中数量拒绝',
            'RC_CC_CNC'        => '检查通讯录个数(20个）拒绝',
            'RC_CC_CILONC'  => '检查通讯录包含贷款的个数拒绝',
            'RC_CC_CNBONC'  => '检查通讯录包含黑名单个数拒绝',
            'RC_CL'         => '检查通话记录数量拒绝',
            'RC_CLNC'       => '通话记录数量拒绝',
            'RC_CL_CLIBONC' => '检查通话记录中包含黑名单数量拒绝',
            'RC_RBONC'      => '检查联系人与黑名单交叉记录拒绝',
            'RC_RS_ABCARC'  => '检查通话记录与联系人拒绝',
            'RC_REFUSE_WHETHER_TO_AUTHORIZE_THIRDPARTY_DATA'  => '第三方数据',
            'RC_REFUSE_CHECK_RISK_AREA'  => '区域',
            'RC_REFUSE_CHECK_AGE'  => '年龄',
            'RC_REFUSE_CHECK_RISK_OCCUPATION'  => '职业',
            'RC_REFUSE_CHECK_MARITAL_STATUS'  => '婚姻状态',
            'RC_REFUSE_CHECK_DEGREE'  => '学位',
            'RC_REFUSE_CHECK_CHILD_NUM'  => '孩子数量',
            'RC_REFUSE_CHECK_RESIDENCE_LENGTH'  => '居住时长',
            'RC_REFUSE_CHECK_WORKING_LIFE'  => '工作时长',
            'RC_REFUSE_CHECK_PAYDAY'  => '发薪日',
            'RC_CHECK_IMCOME_LEVEL'  => '收入水平',
            'RC_REFUSE_CHECK_ADDRESSBOOK_NUM'  => '通讯录个数',
            'RC_REFUSE_CHECK_SMS_NUM'  => '短信条数',
            'RC_REFUSE_WHETHER_OR_NOT_CONTACTS_IN_BLACKLIST'  => '联系人是否在黑名单中',
            'DF_BLACK'  => '黑产拒绝',
            'DF_OTHER_PLATFORM_OVERDUE'  => '其他平台逾期超过三天',
            'RC_REFUSE_Check_Overdue_SMS'=> '短信逾期天数拒绝',
            'RC_REFUSE_Engine_Of_Whether_Cash_Loan_Excessive'=> '现金贷活跃过度',
            'RC_REFUSE_PULL_BLACKLIST_WHEN_OVERDUE'           => '短信逾期天数大于等于15天',//'逾期天数大于等于15天',
            'RC_REFUSE_PULL_BLACKLIST_WHEN_RISK_OCCUPATION'                   => '同行,公检、法、军、律等行业',//
            'RC_REFUSE_INSTALLED_APPNUMBER'                   => 'APP安装数量',//
        ];
        if($refuse=='reason'){
            return $arrStr;
        }
        if(empty($arrStr)){
            return 'Unknown';
        }
        if (!isset($arrStr[$code])){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
    
}