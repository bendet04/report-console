<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 16:02
 */

namespace App\Consts;


class LivingType
{
    //居住类型
    const LIVING1 = 1;
    const LIVING2 = 2;
    const LIVING3 = 3;
    const LIVING4 = 4;
    const LIVING5 = 5;
    const LIVING6 = 6;



    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::LIVING1=> 'RUMAH KONTRAKAN',//租整套
            self::LIVING2=> 'SEWA RUMAH',//租单间
            self::LIVING3=> 'TINGGAL DENGAN ORANGTUA/SAUDARA',//与亲人同住
            self::LIVING4=> 'RUMAH SENDIRI',//自有
            self::LIVING5=> 'KOS',//月租
            self::LIVING6=> 'RUMAH DINAS',//公司提供的
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}