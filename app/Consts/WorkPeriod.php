<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 21:47
 */

namespace App\Consts;


class WorkPeriod
{
//    工作的时限
    const WORK1 = 1;
    const WORK2 = 2;
    const WORK3 = 3;
    const WORK4 = 4;
    const WORK5 = 5;

    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::WORK1 => '1 Tahun dan 1 tahun ke bawah',//1年及以下
            self::WORK2 => '1~3 Tahun',//1~3年
            self::WORK3 => '3~5 Tahun',//3~5年
            self::WORK4 => '5~10 Tahun',//5~10年
            self::WORK5 => '10 Tahun Ke atas',//5~10年
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}