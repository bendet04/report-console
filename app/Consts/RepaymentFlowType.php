<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/3/13/013
 * Time: 16:58
 */

namespace App\Consts;


class RepaymentFlowType
{
    //婚姻
    const PAYMENT0 = 1;
    const PAYMENT1 = 2;
    const PAYMENT2 = 3;


    public static function toString($code)
    {
        $arrStr = [
            self::PAYMENT0 => '线下还款',
            self::PAYMENT1 => '线上还款',
            self::PAYMENT2 => '机构代扣',
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}