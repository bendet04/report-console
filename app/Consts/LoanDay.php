<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 16:58
 */

namespace App\Consts;


class LoanDay
{
    const DAY1  = 1;
    const DAY2  = 2;
    const DAY3  = 3;
    const DAY4  = 4;
    const DAY5  = 5;
    const DAY6  = 6;
    const DAY7  = 7;
    const DAY8  = 8;
    const DAY9  = 9;
    const DAY10 = 10;
    const DAY11 = 11;
    const DAY12 = 12;
    const DAY13 = 13;
    const DAY14 = 14;
    const DAY15 = 15;
    const DAY16 = 16;
    const DAY17 = 17;
    const DAY18 = 18;
    const DAY19 = 19;

    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::DAY1  => 7,
            self::DAY2  => 14,
            self::DAY3  => 14,
            self::DAY4  => 7,
            self::DAY5  => 8,
            self::DAY6  => 9,
            self::DAY7  => 10,
            self::DAY8  => 11,
            self::DAY9  => 12,
            self::DAY10 => 13,
            self::DAY11 => 14,
            self::DAY12 => 7,
            self::DAY13 => 7,
            self::DAY14 => 14,
            self::DAY15 => 7,
            self::DAY16 => 14,
            self::DAY17 => 8,
            self::DAY18 => 15,
            self::DAY19 => 8,

        ];
        if (empty($arrStr)) {
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}