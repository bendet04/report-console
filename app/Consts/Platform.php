<?php

namespace App\Consts;


class Platform
{

    public static function toString()
    {
        $arrStr = [
            'ProsperiTree',
        ];
        
        return $arrStr;
    }
    
    public static function alias($platform)
    {
        $arr = [
            'Modalin' => 'Saya Modalin',
        ];
        
        if (empty($arr[$platform]))
            return $platform;
        
        return $arr[$platform];
    }
    
    public static function doku($platform)
    {
//        $arr = [
//            'Modalin',
//        ];
//        if (in_array($platform, $arr))
//            return 'duko';
        
        return '';
    }
}