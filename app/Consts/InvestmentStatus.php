<?php
namespace App\Consts;

class InvestmentStatus
{
    const INVESTMENTSTATUS1 = 1;
    const INVESTMENTSTATUS2 = 2;
    const INVESTMENTSTATUS3 = 3;
    const INVESTMENTSTATUS4 = 4;
    const INVESTMENTSTATUS5 = 5;
    const INVESTMENTSTATUS6 = 6;
    const INVESTMENTSTATUS7 = 7;
    const INVESTMENTSTATUS8 = 8;
    const INVESTMENTSTATUS9 = 9;
    const INVESTMENTSTATUS10 = 10;
    const INVESTMENTSTATUS12 = 12;
    const INVESTMENTSTATUS50 = 50;
    const INVESTMENTSTATUS51 = 51;
	public static function toString($code)
	{
		$arrStr = [
            self::INVESTMENTSTATUS1  => __('investment.status1'),//待审核,
            self::INVESTMENTSTATUS2  => __('investment.status2'),//审核成功,
            self::INVESTMENTSTATUS3  => __('investment.status3'),//审核成功已确认,
            self::INVESTMENTSTATUS4  => __('investment.status4'),//投资待汇款,
            self::INVESTMENTSTATUS5  => __('investment.status5'),//投资中,
            self::INVESTMENTSTATUS6  => __('investment.status6'),//可提现,
            self::INVESTMENTSTATUS7  => __('investment.status7'),//提现中,
            self::INVESTMENTSTATUS8  => __('investment.status8'),//提现失败,
            self::INVESTMENTSTATUS9  => __('investment.status9'),//审核失败,
            self::INVESTMENTSTATUS10  => __('investment.status10'),//已取消,
            self::INVESTMENTSTATUS12  => __('investment.status12'),//已完成,
            self::INVESTMENTSTATUS50  => __('investment.alreadyPayment'),//审核成功已放款,
            self::INVESTMENTSTATUS51  => __('investment.alreadyRePayment'),//已还款,
		];
		if ($code && !isset($arrStr[$code]))
		    return '';
		
		return empty($code) ? $arrStr : $arrStr[$code];
	}
	
	public static function paymentArr()
    {
        return [
            self::INVESTMENTSTATUS6,
            self::INVESTMENTSTATUS7,
            self::INVESTMENTSTATUS8,
        ];
    }
}