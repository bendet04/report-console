<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 17:01
 */

namespace App\Consts;


class Career
{
    const CAREER1 = 1;
    const CAREER2 = 2;
    const CAREER3 = 3;
    const CAREER4 = 4;
    const CAREER5 = 5;
    const CAREER6 = 6;
    const CAREER7 = 7;
    const CAREER8 = 8;
    const CAREER9 = 9;
    const CAREER10 = 10;
    const CAREER11 = 11;
    const CAREER12 = 12;
    const CAREER13 = 13;
    const CAREER14 = 14;
    const CAREER15 = 15;
    const CAREER16 = 16;
    const CAREER17 = 17;
    const CAREER18 = 18;
    const CAREER19 = 19;
    const CAREER20 = 20;
    const CAREER21 = 21;
    const CAREER22 = 22;
    const CAREER23 = 23;
    const CAREER24 = 24;
    const CAREER25 = 25;
    const CAREER26 = 26;
    const CAREER27 = 27;
    const CAREER28 = 28;
    const CAREER29 = 29;
    const CAREER30 = 30;
    const CAREER31 = 31;
    const CAREER32 = 32;
    const CAREER33 = 33;
    const CAREER34 = 34;
    const CAREER35 = 35;
    const CAREER36 = 36;
    const CAREER37 = 37;
    const CAREER38 = 38;
    const CAREER39 = 39;
    const CAREER40 = 40;


    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::CAREER1 => ' Accounting/Finance officer',//会计／财务人员
            self::CAREER2 => 'Customer service',//客服
            self::CAREER3 => 'Engineer',//工程师
            self::CAREER4 => 'Eksecutif / executive',//行政人员
            self::CAREER5 => 'Administrasi umum / general administration',//一般行政
            self::CAREER6 => 'Teknologi informasi',//信息技术
            self::CAREER7 => 'Konsultan/Analis',//顾问／分析师
            self::CAREER8 => 'Marketing',//营销
            self::CAREER9 => 'Pengajar(Guru,Dosen)',//教师（教师，讲师）
            self::CAREER10 => 'Militer',//军人/士兵
            self::CAREER11 => 'Pelajar/Mahasiswa',//学生
            self::CAREER12 => 'Wiraswasta',//企业家
            self::CAREER13 => 'Polisi',//警察
            self::CAREER14 => 'Petani',//農民
            self::CAREER15 => 'Nelayan',//渔民
            self::CAREER16 => 'Peternak',//饲养员
            self::CAREER17 => 'Dokter',//医生
            self::CAREER18 => 'Tenaga Medis(Perawat,Bidan.dsb)',//医务人员（护士，助产）
            self::CAREER19 => 'Perhotelan & Restoran(Koki,Bartender.dsb)',//旅店&餐飲
            self::CAREER20 => 'Peneliti',//研究院
            self::CAREER21 => 'Desainer',//设计师
            self::CAREER22 => 'Arsitek',//建筑师
            self::CAREER23 => 'Pekerja Seni(Artis,Musisi,Pelukis.dsb)',//艺术工作者
            self::CAREER24 => 'Pengamanan/security',//保安員
            self::CAREER25 => 'Pialang/Broker',//经纪人
            self::CAREER26 => 'Distributor',//经销商
            self::CAREER27 => 'Transportasi Udara(Pilot,Pramugari)',//航空运输（飞行员，乘务员）
            self::CAREER28 => 'Transportasi Laut(Nahkoda,ABK)',//海上交通（船長，船員）
            self::CAREER29 => 'Transports Darat(Masons,Sopir,Kondektur)',//陸地交通（司機）
            self::CAREER30 => 'Buruh(Buruh Pabrik,Buruh,Bangunan,Buruh Tani)',//劳工（制造商，劳工，）
            self::CAREER31 => 'Pertukangan & Pengrajin(Tukang kayo, PengraJin Kulit, dll)',//匠(木，皮，農）
            self::CAREER32 => 'Ibu Rumah Tangga',//家庭主妇
            self::CAREER33 => 'Pejabat Negara/Penyelenggara Negara',//国家官员／国家组织者
            self::CAREER34 => 'Pegawai Pemerintahan/Lembaga Negara(Selain Pejabat/Penyelenggara Negara)',//政府雇员／国家机构（官员／国家干部除外）
            self::CAREER35 => 'Pekerja Informal(Asisten Rumah Tangga)',//非正式工作者（家庭助理）
            self::CAREER36 => 'Perusahaan kredit',//信贷公司
            self::CAREER37 => 'Pengacara',//律师
            self::CAREER38 => 'Koleksi',//催收
            self::CAREER39 => 'Orang religius penuh waktu',//专职宗教人士
            self::CAREER40 => 'Reporter',//记者


        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}