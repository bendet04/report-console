<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/3/13/013
 * Time: 16:58
 */

namespace App\Consts;


class RepaymentFlowStatus
{
    //婚姻
    const PAYMENT0 = 0;
    const PAYMENT1 = 1;
    const PAYMENT2 = 2;
    const PAYMENT3 = 3;
    const PAYMENT4 = 4;


    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::PAYMENT0 => 'Tidak dibayar',//'未还款',
            self::PAYMENT1 => 'Pembayaran berhasil',//'还款成功',
            self::PAYMENT2 => 'Pembayaran gagal',//'还款失败',
            self::PAYMENT3 => 'Ulasan sukses',//'复核成功',
            self::PAYMENT4 => 'Terlambat',//'已逾期',
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
}