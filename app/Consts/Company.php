<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 22:04
 */

namespace App\Consts;


class Company
{
//    公司的性质
    const  COMPANY1 = 1;
    const  COMPANY2 = 2;
    const  COMPANY3 = 3;
    const  COMPANY4 = 4;

    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }

        $arrStr = [
            self::COMPANY1 => 'Milik negara',//国有
            self::COMPANY2 => 'PMA',//外资
            self::COMPANY3 => 'Swasta',//私营
            self::COMPANY4 => 'Corporate',//合资
        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }

}