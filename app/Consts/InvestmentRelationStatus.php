<?php
namespace App\Consts;

class InvestmentRelationStatus
{
 
	public static function toString($code)
	{
		$arrStr = [
            1  => __('investment.relatinStatus1'),//未到期,
            2  => __('investment.relatinStatus2'),//到期未提,
            3  => __('investment.relatinStatus3'),//提现中,
            4  => __('investment.relatinStatus4'),//提现失败,
            5  => __('investment.relatinStatus5'),//提现完成,
		];
		if ($code && !isset($arrStr[$code]))
		    return '';
		
		return empty($code) ? $arrStr : $arrStr[$code];
	}
}