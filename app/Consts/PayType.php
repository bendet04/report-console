<?php
namespace App\Consts;

class PayType
{
	// 借款用途的15种类型
	const PAY_TYPE_FIRST = 1;
	const PAY_TYPE_SECOND = 2;
	const PAY_TYPE_THIRD = 3;
	const PAY_TYPE_FORTH = 4;
	const PAY_TYPE_FIFTH = 5;
	const PAY_TYPE_SIX = 6;
	const PAY_TYPE_SEVEN = 7;
	const PAY_TYPE_EIGHT = 8;
	const PAY_TYPE_NINE = 9;
	const PAY_TYPE_TEN = 10;
	const PAY_TYPE_TENO = 11;
	const PAY_TYPE_TENS = 12;
	const PAY_TYPE_TENH = 13;
	const PAY_TYPE_TENT = 14;
	const PAY_TYPE_TENF = 15;

	public static function toString($code)
	{
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
		$arrStr = [
			self::PAY_TYPE_FIRST => 'Cicilan kendaraan',//车辆分期付款
			self::PAY_TYPE_SECOND => 'Perbaikan kendaraan',//车辆维修
			self::PAY_TYPE_THIRD => 'Liburan',//节日
			self::PAY_TYPE_FORTH => 'Renovasi rumah',//装修房子
			self::PAY_TYPE_FIFTH => 'Biaya kos-kosan',//支付月租
			self::PAY_TYPE_SIX => 'Biaya pernikahan',//婚礼费用
			self::PAY_TYPE_SEVEN => 'Biaya transportasi',//交通費用
			self::PAY_TYPE_EIGHT => 'Lain-lain',//其他
			self::PAY_TYPE_NINE => 'Biaya medis',//医疗
			self::PAY_TYPE_TEN => 'Keperluan sehari-hari',//日用品
			self::PAY_TYPE_TENO => 'Modal bisnis',//资本业务
			self::PAY_TYPE_TENS => 'Tagihan listrik',//电费
			self::PAY_TYPE_TENH => 'Pembayaran pinjaman',//支付贷款
			self::PAY_TYPE_TENT => 'Cicilan elektronik',//电子分期付款
			self::PAY_TYPE_TENF => 'Cicilan rumah',//房贷分期付款
		];
		if(empty($arrStr)){
			return 'Unknown';
		}
		return $arrStr[$code];
	}

}