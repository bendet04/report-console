<?php

namespace App\Consts;

class Bank
{

    //银行返回状态码
    public static function toString($code)
    {
        $statusArr = [
            201 => ['order_status' => 8, 'payment_flow_status' => 1, 'info'=>'发起转账成功，等待提交银行'],//发起转账成功，等待提交银行
            200 => ['order_status' => 14, 'payment_flow_status' => 3, 'info'=>'交易成功'],//Bluepay侧交易完成
            400 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'参数错误，缺少参数'],//参数错误，缺少参数
            401 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'签名错误'],//签名错误/加密错误
            404 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'信息未找到/交易信息未找到'],//信息未找到/交易信息未找到
            500 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'Bluepay服务内部错误'],//服务内部错误
            501 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'银行请求超时，可重新发起交易'],//银行请求超时，可重新发起交易
            506 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'IP限制'],//Ip限制
            600 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'转账失败'],//转账失败
            601 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'商户余额不足，请储值'],//商户余额不足
            646 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'银行处理失败'],//银行处理失败
            649 => ['order_status' => 15, 'payment_flow_status' => 4, 'info'=>'银行信息错误，账号错误'],//银行处理失败
        ];
        if(empty($statusArr[$code])){
            return 'Unknown';
        }
        return $statusArr[$code]['info'];
    }

}