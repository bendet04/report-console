<?php

namespace App\Consts;


class Remarks
{
    
    public static function toString()
    {
        $arrStr = [
            'unableToContact',
            'attitude',
            'family',
            'debtDifficulties',
            'noMoney',
            'noInternet',
            'little',
            'time',
        ];
        
        return $arrStr;
    }
}