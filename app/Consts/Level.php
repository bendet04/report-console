<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018/2/26/026
 * Time: 17:23
 */
//收入水平
namespace App\Consts;

//:2.000.000~2.500.000<br />
//3:2.500.000~3.000.000<br />
//4:3.000.000~3.500.000<br />
//5:3.500.000~4.000.000<br />
//6:4.000.000~4.500.000<br />
//7:4.500.000~5.000.000<br />
//8:5.000.000~5.500.000<br />
//9:5.500.000~6.000.000<br />
//10:6.000.000~6.500.000<br />
//11:6.500.000~7.000.000<br />
//12:7.000.000~7.500.000<br />
//13:7.500.000~8.000.000<br />

class Level
{
    const LEVER1 = 1;
    const LEVER2 = 2;
    const LEVER3 = 3;
    const LEVER4 = 4;
    const LEVER5 = 5;
    const LEVER6 = 6;
    const LEVER7 = 7;
    const LEVER8 = 8;
    const LEVER9 = 9;
    const LEVER10 = 10;
    const LEVER11 = 11;
    const LEVER12 = 12;
    const LEVER13 = 13;
    const LEVER14 = 14;
    const LEVER15 = 15;
    const LEVER16 = 16;

    public static function toString($code)
    {
        if(!isset($code)||empty($code))
        {
            return '未选择';
        }
        $arrStr = [
            self::LEVER1 => '2.000.000 Ke bawah',
            self::LEVER2 => '2.000.000~4.000.000',
            self::LEVER3 => '4.000.000~8.000.000',
            self::LEVER4 => '8.000.000 Ke atas',
            self::LEVER5 => '2.000.000~2.500.000',
            self::LEVER6 => '2.500.000~3.000.000',
            self::LEVER7 => '3.000.000~3.500.000',
            self::LEVER8 => '3.500.000~4.000.000',
            self::LEVER9 => '4.000.000~4.500.000',
            self::LEVER10 => '4.500.000~5.000.000',
            self::LEVER11 => '5.000.000~5.500.000',
            self::LEVER12 => '5.500.000~6.000.000',
            self::LEVER13 => '6.000.000~6.500.000',
            self::LEVER14 => '6.500.000~7.000.000',
            self::LEVER15 => '7.000.000~7.500.000',
            self::LEVER16 => '7.500.000~8.000.000',


        ];
        if(empty($arrStr)){
            return 'Unknown';
        }
        return $arrStr[$code];
    }
    
    public static function getList()
    {
        $arrStr = [
            self::LEVER1 => '2.000.000 Ke bawah',
//            self::LEVER2 => '2.000.000~4.000.000',
//            self::LEVER3 => '4.000.000~8.000.000',
            self::LEVER5 => '2.000.000~2.500.000',
            self::LEVER6 => '2.500.000~3.000.000',
            self::LEVER7 => '3.000.000~3.500.000',
            self::LEVER8 => '3.500.000~4.000.000',
            self::LEVER9 => '4.000.000~4.500.000',
            self::LEVER10 => '4.500.000~5.000.000',
            self::LEVER11 => '5.000.000~5.500.000',
            self::LEVER12 => '5.500.000~6.000.000',
            self::LEVER13 => '6.000.000~6.500.000',
            self::LEVER14 => '6.500.000~7.000.000',
            self::LEVER15 => '7.000.000~7.500.000',
            self::LEVER16 => '7.500.000~8.000.000',
            self::LEVER4 => '8.000.000 Ke atas',
        ];
        return $arrStr;
    }
    
    public static function toInt($code)
    {
        if(!isset($code)||empty($code))
        {
            return 0;
        }
        $arrStr = [
            self::LEVER1 => '2000000',
            self::LEVER2 => '4000000',
            self::LEVER3 => '8000000',
            self::LEVER4 => '8000000',
            self::LEVER5 => '2500000',
            self::LEVER6 => '3000000',
            self::LEVER7 => '3500000',
            self::LEVER8 => '4000000',
            self::LEVER9 => '4500000',
            self::LEVER10 => '5000000',
            self::LEVER11 => '5500000',
            self::LEVER12 => '6000000',
            self::LEVER13 => '6500000',
            self::LEVER14 => '7000000',
            self::LEVER15 => '7500000',
            self::LEVER16 => '8000000',
        
        ];
        
        return empty($arrStr[$code]) ? 0 : $arrStr[$code];
    }

}