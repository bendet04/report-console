<?php

namespace App\Events;


class FinalAfter
{

    public $order_id;
    /**
     * 终审完成触发事件
     * 具体业务逻辑在App\Listeners\FinalAfterListener
     *
     * @return void
     */
    public function __construct($order_id)
    {
        $this->order_id = $order_id;
    }

}
