<?php

namespace App\Events;

class InfoAfter
{
    public $order_id;
    public $status;
    /**
     * 信审完成触发事件
     * 具体业务实现逻辑在App\Listeners\InfoAfterListener
     *
     * @return void
     */
    public function __construct($order_id, $status)
    {
        $this->order_id = $order_id;
        $this->status = $status;
    }

}
