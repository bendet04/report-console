<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PhoneAfter
{

    public $order_id;
    public $status;
    
    /**
     * 电审完成触发事件
     * 具体业务逻辑在App\Listeners\PhoneAfterListener
     *
     * @return void
     */
    public function __construct($order_id, $status)
    {
        $this->order_id = $order_id;
        $this->status = $status;
    }

}
