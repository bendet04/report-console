<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class StartWork
{
    use SerializesModels;

    public $trial_type;
    
    /**
     * 任务分配事件
     * 具体逻辑处理在App\Listeners\StartWorkListener
     *
     * @return void
     */
    public function __construct($trial_type)
    {
        $this->trial_type = $trial_type;
    }

}
