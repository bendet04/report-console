<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\StartWork' => [//开始工作事件
            'App\Listeners\StartWorkListener',
        ],
        'App\Events\InfoAfter' => [//信审之后事件
            'App\Listeners\InfoAfterListener',
        ],
        'App\Events\PhoneAfter' => [//电审之后事件
            'App\Listeners\PhoneAfterListener',
        ],
        'App\Events\FinalAfter' => [//终审之后事件
            'App\Listeners\FinalAfterListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
