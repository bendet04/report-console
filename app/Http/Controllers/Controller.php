<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    //获取带有查询条件及分页数据的url
    public function getUrlWithCondition($request)
    {
        $reqs = $request->all();
        foreach ($reqs as $k=>$v){
            if (empty($v)) {
                unset($reqs[$k]);
            }
            if ($k=='_token'){
                unset($reqs[$k]);
            }
            if ($k=='s'){
                unset($reqs[$k]);
            }
        }

        
        if ($reqs) {
            $result = http_build_query($reqs);
            $result = $request->url().'?'.$result;
            $result = urlencode($result);
        } else {
            $result = urlencode($request->url());
//            $result = $request->url();
        }
        
        return $result;
    }
    
    //获取搜索条件
    public function getCondition($request)
    {
        $reqs = $request->all();
        foreach ($reqs as $k=>$v){
            if (empty($v)&&!is_numeric($v)) {
                unset($reqs[$k]);
            }
            if ($k=='_token'){
                unset($reqs[$k]);
            }
            if ($k=='s'){
                unset($reqs[$k]);
            }
            if ($k=='page'){
                unset($reqs[$k]);
            }
        }
        return $reqs;
    }
}
