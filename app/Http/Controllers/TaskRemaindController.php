<?php
/**
 * 催收提醒任务重新分配
 * @author  chenwr
 * @since   2018年7月16日
 * @version v1.0
 */

namespace App\Http\Controllers;

use App\Consts\PayType;
use App\Models\AdminUser;
use App\Models\LoanOrderTrack;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Service\Assign;

class TaskRemaindController extends Controller
{
    protected $roleId = 15;
    public function __construct()
    {
        Config::set('currentMenu', 'task');
    }
    
    /**
     * 催收提醒任务分配列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function track(Request $request)
    {
        $admin_id = '';
        if($request->isMethod('get')){
            $admin_id = $request->get('admin_id');
        }
    
        //获取催收人员列表
        $trackUser = AdminUser::getListByRole($this->roleId);
        $trackArr = $trackUser->pluck('admin_username', 'id');

        $orders = $this->getOrder($request, $trackArr);
        

        return view('remind/track', [
            'orders' => $orders,
            'trackUser' => $trackUser,
            'admin_id' => $admin_id,
            'pageCondition' => $this->getCondition($request),
            'urlCondition' => $this->getUrlWithCondition($request)
        ]);
    }
    
    public function getOrder($request, $trackArr=[])
    {
        $model = OrderFactory::getInstance('track');
        
        //检索条件
        $condition['order_number'] = $request->post('order_number');
        $condition['name'] = $request->post('name');
        $condition['admin_id'] = $request->post('admin_id');
        $condition['admin'] = 1;
        $condition['is_remind'] = 1;
        $status1 = $request->post('status1');
        $condition = $this->parse_status($status1, $condition);
    
        #获取订单列表
        $orderBy = empty($_COOKIE['checktrack']) ? 'loan_order.id' : $_COOKIE['checktrack'];
        $orders = $model->getOrderList($condition, $orderBy);
        if ($trackArr) {
            $orders->map(function($order) use($trackArr) {
                $order['admin_user'] = $trackArr->get($order->admin_id);
                $order['pay_type'] = PayType::toString($order->pay_type);
                return $order;
            });
        }
        
        return $orders;
    }
    
    /**
     * 列表搜索状态特殊处理
     *
     * @param $status1
     * @param $condition
     * @param $status2
     * @return mixed
     */
    public function parse_status($status1, $condition)
    {
        if ($status1) {
            if ($status1 == 1) { //到期前一天
                $condition['status1'] = ['loan_deadline', '>=', date('Y-m-d 00:00:00', strtotime('+1 day'))];
                $condition['status11'] = ['loan_deadline', '<=', date('Y-m-d 59:59:59', strtotime('+1 day'))];
            } elseif($status1 == 9){//到期前两天
                $condition['status1'] = ['loan_deadline', '>=', date('Y-m-d 00:00:00', strtotime('+2 day'))];
                $condition['status11'] = ['loan_deadline', '<=', date('Y-m-d 59:59:59', strtotime('+2 day'))];
            }elseif ($status1 == 2) {//今日到期
                $condition['status1'] = ['loan_deadline', '>=', date('Y-m-d 00:00:00')];
                $condition['status11'] = ['loan_deadline', '<=', date('Y-m-d 59:59:59')];
            } elseif ($status1 == 3) {//逾期第一天
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 00:00:00')];
                $condition['status11'] = ['loan_overdue_date', '<=', date('Y-m-d 59:59:59')];
            } elseif ($status1 == 4) {//逾期第二天
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 00:00:00', strtotime('-1 day'))];
                $condition['status11'] = ['loan_overdue_date', '<=', date('Y-m-d 59:59:59', strtotime('-1 day'))];
            } elseif ($status1 == 5) {//逾期7天内
                $condition['order_status'] = 11;
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 59:59:59', strtotime('-7 day'))];
            } elseif ($status1 == 6) {//逾期14天内
                $condition['order_status'] = 11;
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 59:59:59', strtotime('-14 day'))];
            } elseif ($status1 == 7) {//逾期15天以上
                $condition['status1'] = ['loan_overdue_date', '<=', date('Y-m-d 00:00:00', strtotime('-15 day'))];
            } elseif ($status1 == 8) {//缓期中
                $condition['order_status'] = 10;
            }
        }
       
        return $condition;
    }
    
    /**
     * 重新分配页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assign(Request $request)
    {
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
        $url = $request->get('url');
//        dd($url);
        //获取催收人员列表
        $trackUser = AdminUser::selectOprator($this->roleId);
        
        return view('remind/assign', [
            'order_id' => $order_id,
            'admin_id' => $admin_id,
            'trackUser' => $trackUser,
            'url' => $url,
        ]);
    }
    
    public function reAssignMultiple(Request $request)
    {
        $orders = $this->getOrder($request);
        
        $selectTrack = $request->post('selectTrack');
        
        //获取催收人员列表
        $trackUser = AdminUser::getListByRole($this->roleId);
        $trackArr = $trackUser->pluck('admin_username', 'id');
        
        $view['admin'] = $trackArr[$request->post('admin_id')];
        $view['platform'] = empty($request->post('platform')) ? 'All' : Platform::toString()[$request->post('platform')];
        $view['status1'] = $request->post('status1');
        
        $realUser = AdminUser::selectOprator($this->roleId);
        
        return view('remind/reAssignMultiple', [
            'view' => $view,
            'orders' => $orders,
            'selectTrack' => $selectTrack,
            'realUser' => $realUser,
        ]);
    }
    
    public function reAssignMultipleHandle(Request $request)
    {
        $orders = $this->getOrder($request);
        $selectTrack = $request->post('selectTrack');
        
        //接收人
        $multipleTracks = $request->post('multipleTracks');
        $needAssigninfo = [];
        foreach ($multipleTracks as $k=>$v) {
            array_push($needAssigninfo,collect(['id' => $v]));
        }
        
        if (!empty($selectTrack)) {
            foreach ($selectTrack as $k=>$v) {
                Assign::assign($v, 'track', $needAssigninfo);
            }
        } else {
            foreach ($orders as $k=>$v) {
                Assign::assign($v->id, 'track', $needAssigninfo);
            }
        }
        
        return json_encode(['code'=>1]);
    }
    
    /**
     * 催收人员再分配
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reassign(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required',
            'admin_id' => 'required',
        ]);
        
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
        $url = $request->get('url');

        LoanOrderTrack::saveOne($order_id, [
            'admin_id' => $admin_id,
        ]);
        
        return redirect($url);
    }
    public function taskReassign(Request $request)
    {
        //获取催收人员列表
        $infoUser = AdminUser::getListByRole($this->roleId);
        $realUser = AdminUser::selectOprator($this->roleId);

        return view('/remind/taskReassign', [
            'infoUser' => $infoUser,
            'realUser' => $realUser,
        ]);
    }
    public function taskTrackHandle(Request $request)
    {
        $this->validate($request, [
            'admin_id' => 'required',
            'needAssign' => 'required',
        ]);

        $needAssigninfo = [];
        $needAssign = $request->input('needAssign');
        foreach ($needAssign as $v){
            array_push($needAssigninfo,collect(['id' => $v]));
        }

        $admin_id = $request->input('admin_id');

        $needAssignAdmin =LoanOrderTrack::selectTrailByAdminId($admin_id);
        if(empty($needAssignAdmin)){
            return redirect('/remind/remindtask');
        }

        foreach ($needAssignAdmin as $k => $v){

            Assign::assign($v->order_id, 'track', $needAssigninfo);
            Log::info('track assign admin id is '. $admin_id . ' order id is '.$v->order_id);

        }
        return redirect('/remind/remindtask');
    }
}
