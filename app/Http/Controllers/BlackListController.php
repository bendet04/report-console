<?php

namespace App\Http\Controllers;


use App\Models\RiskControlRecord;
use App\Models\UserBasicInfo;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;


class BlackListController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'black');
    }
    /*黑名单首页*/
    public function blackList()
    {
        $str='';
       return view('risk/blackList',
           ['date'=>date('Y-m-d',time()),
               'str'=>$str,
           ]);
    }
    /*获取搜索数据*/
    public function getSearch(Request $request)
    {
        $ktp=$request->input('ktp');
        $name=UserBasicInfo::getName($ktp);
        if(!empty($name[0])){
            $data=RiskControlRecord::getBlack($name[0],$ktp);
            return $data;
        }else{
            return json_encode(['code'=>201,'info'=>'The Ktp is invalid']);
        }
    }
    /*添加黑名单*/
    public function addBlack(Request $request)
    {
        $ktp=$request->input('ktp');
        $ktp1=$ktp;
        if(!empty($ktp)){
            $ktp=trim(str_replace(';',',',$ktp),',');
            $data=UserBasicInfo::getInfo($ktp);
        }else{
            $data='';
        }
        return view('risk/addblack',
            ['date'=>date('Y-m-d',time()),
             'data'=>$data,
             'search'=>$ktp1,
            ]);
    }
    /*向风控添加黑名单*/
    public function add(Request $request)
    {
        $post_data=$request->input('post_array');
        $uri=$request->input('uri');
        foreach ($post_data as $k=>$v)
        {
            $post['userlists'][$k]['ktp_number']=array_values(array_filter($v))['0'];
            $post['userlists'][$k]['uuid']=array_values(array_filter($v))['1'];
            $post['userlists'][$k]['username']=strtolower(array_values(array_filter($v))['2']);
            $post['userlists'][$k]['reason']=array_values(array_filter($v))['3'];
        }
        return RiskControlRecord::addBlack(['userlists'=>json_encode($post)],$uri);

    }
 
}
