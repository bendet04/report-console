<?php

namespace App\Http\Controllers;

use App\Consts\RiskRefuse;
use App\Models\RiskControlRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class OrderRiskController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'Order');
    }
    
    /**
     * 风控拒绝原因查询
     * @param Request $request
     * @return mixed
     */

    public function orderRisk(Request $request){
        $riskReason=RiskRefuse::toString("","reason");//风控拒绝详情
        $param=$this->getParam($request->input());
        $type=$request->input('type')?$request->input('type'):'risk';//拒绝类型
        $detail=$request->input('detail')?$request->input('detail'):'';//风控拒绝详情条件
        $orders=RiskControlRecord::DenialList($param,$type,$detail);
//        dd($riskReason);
//        dd($orders,$param,$type,$detail);
//        dd($this->getCondition($request));
        return view('orderrisk/orderRisk', [
            'orders' => $orders,
            'data' => $riskReason,
            'type'=>$type,
            'detail'=>$detail,
            'searchArr' => $this->getCondition($request),
        ]);
    }
    public function getParam($data){
        $param="1=1 ";
        $flag='=';
        //风控
        if((!empty($data['type'])&&$data['type']=='risk'&&empty($data['detail']))||(empty($data['type'])))
        {
            $table='risk_control_record as i';
            $left='i.order_id';
            $right='s.order_id';
            $param.=" and tb_i.status=1 ";
        }else if(!empty($data['type'])&&$data['type']=='risk'&&!empty($data['detail'])){
            $param.=" and tb_i.refuse_id='".$data['detail']."'";
            $table='risk_control_record as i';
            $left='i.order_id';
            $right='s.order_id';
            $param.=" and tb_i.status=1 ";
        }else if(!empty($data['type'])&&$data['type']=='day'){
            $table='order_refuse_time as t';
            $left='t.ktp_number';
            $right='b.ktp_number';
        }else if(!empty($data['type'])&&$data['type']=='black'){
            $param.=" and tb_i.refuse_id='DF_BLACK'";
            $table='risk_control_record as i';
            $left='i.order_id';
            $right='s.order_id';
            $param.=" and tb_i.status=1 ";
        }
        if(!empty($data['start']))
        {
            $param.=" and tb_o.order_time >='".date("Y-m-d 00:00:00",strtotime($data['start']))."'";
        }
        if(!empty($data['end']))
        {
            $param.=" and tb_o.order_time <='".date("Y-m-d 23:59:59", strtotime($data['end']))."'";
        }
        $param.=!empty($data['order_number'])?" and tb_o.order_number='".$data['order_number']."'":'';
        $param.=!empty($data['phone_number'])?" and tb_b.phone_number='".$data['phone_number']."'":'';
        $param.=!empty($data['ktp_number'])?" and tb_b.ktp_number='".$data['ktp_number']."'":'';
        $Arr['param']=$param;
        $Arr['table']=$table;
        $Arr['left']=$left;
        $Arr['flag']=$flag;
        $Arr['right']=$right;
        return $Arr;
    }
    
}
