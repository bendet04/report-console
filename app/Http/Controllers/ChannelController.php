<?php

namespace App\Http\Controllers;

use App\Consts\RiskRefuse;
use App\Models\UserAccess;
use App\Service\Efficency;
use Illuminate\Http\Request;
use App\Models\Channel;
use App\Models\ChannelCount;
use App\Service\ExcelCustomer;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\File\Exception\FileException;





class ChannelController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'channel');
    }
    /*渠道列表展示*/
    public function channelList(Request $request)
    {
        $model=new Channel();
        $data=$model->getList();
        $page=1;
        return view('channel/list')->with([
            'data' => $data,
            'pageCondition' => $this->getCondition($request),
            'page' => $page,


        ]);
   }
   /*修改修改状态*/
   public function change(Request $request)
   {
       $model=new Channel();
       $data=$request->all('status');
       $save_id=$request->all('save_id');
       $result=$model->updateRateById($data,$save_id);
       if($result){
           return json_encode(['code' => 200]);
       } else {
           return json_encode(['code' =>201, 'info' =>'更改状态失败']);
       }

   }
   //添加渠道
    function add(Request $request)
    {
        $this->validate($request, ['status' =>'required|numeric', //必填 数值
     'channel_name'=>'required|max:255', //必填 最大255位
      'channel_id'=>'required|max:255',
        ]);
        $model=new channel();
        $param['name']=$request->input('channel_name');
        $param['status'] =$request->input('status');
        $param['id'] =$request->input('channel_id');

        $param['time']=time();
       $result= $model->insert($param);


         return redirect('/channel/channelList');
     }
     //修改渠道名称
    function upname(Request $request)
    {

        $model=new channel();
        $data['chan_name']=$request->post('chan_name');
        $id=$request->post('id');
        $bool=$model->updateRateById($data,$id);
        if($bool)
        {
            return json_encode(200);
        }
        else
        {
            return json_encode(201);
        }
    }
    //渠道统计信息
    function channelCount(Request $request)
    {
        $page=1;
        $model=new channelCount();
        $param=[];
        if($request->input('channel')) {

            $param['channel'] = $request->input('channel');

        }
        if($request->input('source')) {

            $param['source'] = $request->input('source');

        }
        if($request->get('start')) {
            $param['start'] = date("Y-m-d 00:00:00", strtotime($request->get('start')));
        }
        if($request->get('end'))
        {
            $param['end'] = date('Y-m-d 23:59:59', strtotime($request->get('end')));
        }
        $data=$model->getCount($param);
        $channel=$model->channel_name();
        $str='';
        if(!empty($this->getCondition($request)))
        {
            $arr=$this->getCondition($request);
            $str.='?type=install&';
            foreach ($arr as $k=>$v)
            {
                $str.=$k.'='.$v.'&';

            }

        }

        return view('channel/count') ->with([
            'data' => $data,
            'channel'=>$channel,
            'pageCondition' => $this->getCondition($request),
            'page' => $page,
            'str'=>$str,

        ]);
    }
   //下载excel
    function down(Request $request)
    {
        $param=[];
        if($request->get('channel')) {

            $param['channel'] = $request->get('channel');
        }
        if($request->get('start')) {
            $param['start'] = date("Y-m-d 00:00:00", strtotime($request->get('start')));
        }
        if($request->get('end'))
        {
            $param['end'] = date('Y-m-d 23:59:59', strtotime($request->get('end')));
        }
        if($request->get('week'))
        {
            $param['week']=1;
        }
        if($request->get('source'))
        {
            $param['source']=$request->get('source');
        }
        if($request->get('month'))
        {
            $param['month']=1;
        }
        $type=$request->get('type');
        //获取各种导出数据的参数
        $data=$this->DownParam($type,$param);
        $arr=$data['data']->toArray();
        if(empty($arr)) {
            echo "暂无数据，无法导出!";
            exit();
        }
        $arr = array_map('get_object_vars',$arr);
        array_unshift($arr,$data['flag']);

        $time=$this->DownTime($param);
        $name = $time .$data['name'];

        $file_url =  $name .'.xls';

        return (new ExcelCustomer($arr))->download($file_url, 'Xls');
    }
    //删除渠道
    public function del(Request $request)
    {
        $model=new channel();
        $id=$request->post('id');
        $data['is_del']=$request->post('is_del');
        $result=$model->updateRateById($data,$id);
        if($result){
            return json_encode(['code' => 200]);
        } else {
            return json_encode(['code' =>201, 'info' =>'更改状态失败']);
        }

    }
//    //统计率
    public function StatisticalRrate(Request $request)
    {

        $user_obj=new UserAccess();
        $param=[];
        if($request->input('channel')) {

            $param['channel'] = $request->input('channel');

        }
        if($request->input('source'))
        {
            $param['source']=$request->input('source');
            $flag=0;
        }
        if($request->get('start')) {
            $param['start'] = date("Y-m-d 00:00:00", strtotime($request->get('start')));
        }
        if($request->get('end'))
        {
            $param['end'] = date('Y-m-d 23:59:59', strtotime($request->get('end')));
        }
        if($request->get('week'))
        {
            $param['week']=1;
        }
        if($request->get('month'))
        {
            $param['month']=1;
        }
        $data=$user_obj->Statistical($param);
        $str='?type=rate&';
        if(!empty($this->getCondition($request)))
        {
            $arr=$this->getCondition($request);
            foreach ($arr as $k=>$v)
            {
                $str.=$k.'='.$v.'&';
            }

        }
        $page=1;
        $model=new channelCount();
        $channel=$model->channel_name();
        return view('channel/rate') ->with([
            'data' => $data,
            'channel'=>$channel,
            'pageCondition' => $this->getCondition($request),
            'page' => $page,
            'str'=>$str

        ]);

    }
    public function DownParam($type,$param)
    {
        switch ($type)
        {
            //渠道统计率
            case "rate":
                $model=new UserAccess();
                $data['data']=$model->Statistical($param,0);
                $data['name']="各渠道订单通过率";
                $data['flag']=['时间','平台名称','渠道名称', '注册数量','提交申请数量','信审通过率','电审通过率','终审通过率','逾期率','坏账率'];
                return $data;
                break;

//                渠道安装量
            case "install":
                $model=new channelCount();
                $data['data']=$model->getCount($param,0);
                $data['name']="各渠道下载量";
                $data['flag']=['年','月', '日', '平台名称','渠道名称', '安装量'];
                return $data;
                break;
//                电审件数
            case "phone":
                 $data['data']=Efficency::countMessage($param,0);
                 $data['name']="电审人员审核件数";
                 $data['flag']=['时间','电审人', '审核件数','逾期率','坏账率'];
                 return $data;

            //    信审件数
            case "info":
                $data['data']=Efficency::message($param,0);
                $data['name']="信审人员审核件数";
                $data['flag']=['时间','信审人', '审核件数','逾期率','坏账率'];
                return $data;


        }
    }
    public function DownTime($param)
    {
        if(!empty($param['month']))
        {
            $time=date('Y-m',time());
        }else if(!empty($param['week'])) {
            $now = time();
            $time = '1' == date('w') ? strtotime('Monday', $now) : strtotime('last Monday', $now);
            $start = date('Y-m-d', $time);
            $end = date('Y-m-d', strtotime('Sunday', $now));
            $time = date('Y-m-d',strtotime($start)).'~'.date('Y-m-d',strtotime($end));

        }else if(!empty($param['start'])&&empty($param['end'])) {
            $time=date('Y-m-d',strtotime($param)).'之后';
        }else if(!empty($param['end'])&&empty($param['start'])) {
            $time=date('Y-m-d',strtotime($param['end'])).'之前';
        }else if(isset($param['start'],$param['end'])&&date('Y-m-d',strtotime($param['start']))==date('Y-m-d',strtotime($param['end']))){
            $time=date('Y-m-d',strtotime($param['start']));
        }else if(!empty($param['start'])&&!empty($param['end'])) {
            $time=date('Y-m-d',strtotime($param['start'])).'~'.date('Y-m-d',strtotime($param['end']));
        }else{
            $time=date('Y-m-d', time());
        }
        return $time;
    }



}
