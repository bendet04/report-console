<?php

namespace App\Http\Controllers;

use App\Consts\Bank;
use App\Service\BankSearch;
use App\Service\OrderFactory;
use App\Service\Rate\FirstStage;
use App\Service\Rate\NormalRate;
use App\Service\Rate\OverDueDayRate;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'Order');
    }
    
    public function order(Request $request)
    {
        $model = OrderFactory::getInstance('orderDetail');
        $page = 1;


        if($request->input()){

            $data = $request->input();
            $page = $request->input('page');
            unset($data['_token']);
            unset($data['s']);
            unset($data['page']);
            $orders = $model->getOrderList($data);

        } else{
            $orders = $model->getOrderList();
        }
        return view('/order/order')
            ->with([
                'orders' => $orders,
                'page' => $page,
                'pageCondition' => $this->getCondition($request),
            ])
            ->withInput($request->all());
    }
    public function orderDetail(Request $request, $id)
    {
        $model = OrderFactory::getInstance('orderDetail');
        $order = $model->getOrderDetail($id);
        return view('/order/orderDetail')
                ->with([
                    'order' => $order
                ])
               ->withInput($request->all());
    }
    
    
    public function bankFlowSearch(Request $request)
    {
        $flow = $request->input('flow');
        $type = $request->input('type');
        $platform = $request->input('platform');

        $flow = BankSearch::search($flow, $type, $platform);
        
        if (!empty($flow['transferStatus'])){
            $flow['transferStatus'] = Bank::toString($flow['transferStatus']);
        }
        
        return view('/order/bankFlowSearch', [
            'flow' => $flow,
        ]);
    }
    
    public function overdueUser(Request $request)
    {
        $model = OrderFactory::getInstance('track');
        $excel = $request->post('excel');
        
        //检索条件
        $condition['order_number'] = $request->post('order_number');
        $condition['ktp_number'] = $request->post('ktp_number');
        $condition['overdue90'] = ['loan_overdue_date', '<=', date('Y-m-d 23:59:59', strtotime('-90 day'))];
        
        if ($request->post('start')) {
            $condition['status1'] = ['order_time', '>=', date('Y-m-d 00:00:00', strtotime($request->post('start')))];
        }
        if ($request->post('end')) {
            $condition['status11'] = ['order_time', '<=', date('Y-m-d 23:50:59', strtotime($request->post('end')))];
        }
        $orderBy = empty($_COOKIE['overdueUser']) ? 'loan_order.id' : $_COOKIE['overdueUser'];
        $pageSize = $excel=='excel' ? 1000 : 20;
        $orders = $model->getOrderList($condition, $orderBy, $pageSize);
        
        if ($excel == 'excel') {
            return $this->overdueUserExcel($orders);
        }
        
        return view('order/overdueUser', [
            'orders' => $orders,
            'pageCondition' => $this->getCondition($request),
            'back_url' => $this->getUrlWithCondition($request),
        ]);
    }
    
    public function overdueUserExcel($orders)
    {
        $excelArr = [[
            __('message.number'),
            __('message.order.number'),
            __('message.client.name'),
            __('KTP'),
            __('message.phone'),
            __('message.create.time'),
            __('basic.deadline.time'),
            __('final.overdueStatus'),
        ]];
        foreach ($orders as $k => $v) {
            array_push($excelArr, [
                $k + 1,
                $v->order_number,
                $v->name,
                ' '.$v->ktp_number,
                ' '.$v->phone_number,
                $v->order_time,
                $v->loan_deadline,
                __('track.overdueDay', ['day' => \App\Service\DateUnit::timeToDay($v->loan_overdue_date)+1 ]),
            ]);
        }
        
        $name = date('Y-m-d', time()) . __('order.overdueUser');
        $file_url =  $name .'.xls';
        
        return (new ExcelCustomer($excelArr))->download($file_url, 'Xls');
    }
}
