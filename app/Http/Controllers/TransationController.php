<?php

namespace App\Http\Controllers;

use App\Consts\LoanDay;
use App\Consts\OrderStatus;
use App\Models\LoanOrder;
use App\Models\LoanPaymentFlow;
use App\Service\Foundation\BluePay;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class TransationController extends Controller
{
    public $bank = null;

    public function __construct(BluePay $bluepay)
    {
        $this->bank = $bluepay;
    }
    public function selectTransation()
    {
        $where['order_status'] = 8 ;
//        $where['']
//        Log::info('transation info');

        $payment_id = "3S46-821Q-0X37-T881";
        $arr = [
            '75D6-9O41-A063-26Y8',
            '3S46-821Q-0X37-T881',
            '9U40-76M1-D092-T827',
            '9U40-76M1-D092-T827',
            'G099-092N-M405-98X9',
            '8I61-H395-F593-K652',
            'P203-8K88-3G29-42W4',
            '514J-I739-4L65-61C1',
            'Z891-977V-836I-063S',
            'T945-954H-05Y7-62V8',
            '112Q-316H-705B-90A7',
            'Y868-541L-61X8-5K19',
            '7S43-2B62-1L99-2D08',
            'A988-E670-F394-632M',
            '00L1-Q681-4O33-I729',
            '4W44-0W72-47U4-Y320',
            '31X8-0N42-27I3-0L37',
            '2O65-21U9-048T-2Z10',
            'B338-85X6-929K-L746',

        ];
        foreach ($arr as $v){
            $result = $this->selectBluepay($v);
            print_r('payment flow is '.$v.' status is' . $result['transferStatus']. 'code ' . $result['code'] .'<br />');
        }
        dd(222);


        $orders = LoanOrder::getTransation($where);
//        dd($orders);
        foreach ($orders as $v){
            $payment_id = $v->payment_flow;
            $payment_id = "7J05-593J-C835-082V";
            $result = $this->selectBluepay($payment_id);
            dd($result);
            Log::info('blue pay code is ' .$result['transferStatus'] . 'order_id is ' . $v->id);

            print_r('payment flow is '.$v->payment_flow.' order is ' . $v->id . ' status is' . $result['transferStatus']. 'code ' . $result['code'] .'<br />');
            if($result['transferStatus'] != '600')
            {
                Log::info('blue pay error code is ' .$result['transferStatus'] . 'order_id is ' . $v->id);
                continue;
            }

            $date['payment_date'] = $result['time'];
            $period =strtotime($result['time']) + 86400 * LoanDay::toString($v->pid);
            $date['loan_deadline'] = date('Y-m-d H:i:s', $period);
            $date['loan_suspend_date'] = date('Y-m-d H:i:s', $period + 86400);
            $date['loan_overdue_date'] = date('Y-m-d H:i:s', $period + 86400);
            $date['order_status'] = $this->checkOrderCode($result['transferStatus']);

//            $orderDetail = LoanOrder::updateById($v->id, $date);

            $paymentData['payment_code'] = $result['transferStatus'];
            $paymentData['payment_status'] = $this->checkCode($result['transferStatus']);
            $paymentData['code'] = 'minirupiah20180628';
            $paymentData['payment_time'] = $result['time'];

//            $paymentDetail = LoanPaymentFlow::saveOne($v->payment_flow, $paymentData);

            $OrderStatusData['status'] = $this->checkOrderCode($result['transferStatus']);
            $OrderStatusData['order_id'] = $v->id;

//            $orderStatusDetail = \App\Models\OrderStatus::saveOne($OrderStatusData);

//            dd($v->id);
//            break;
//            dd($date, $paymentData, $OrderStatusData);
//            dd(LoanDay::toString($v->pid),$date);

        }


    }

//    1:发起转账；2：银行信息错误；3：交易成功；4：交易失败
    public function checkCode($code)
    {
        switch ($code){
            case '200':
                return 3;
                break;
            case '646':
                return 4;
                break;
            case '649':
                return 2;
                break;
            case '600':
                return 4;
                break;
            default:
                Log::info('payment code error' . $code);
        }

    }
    public function checkOrderCode($code)
    {
        switch ($code){
            case '200':
                return 14;
                break;
            case '646':
                return 15;
            case '649':
                return 15;
                break;
            case '600':
                return 15;
                break;
            default:
                Log::info('order code error' . $code);
        }
    }
    public function selectBluepay($tId)
    {
        $host = env('BLUE_URL') . '/charge/indonesiaFintechTransfer/queryTransfer';
        $productId = env('BLUE_PRODUCT');

        $data = "";
        $key = env('BLUE_KEY');

        $dataInfo = $this->bank->AESphp($data,$key);
        $enctypt = md5("operatorId=6&productid=$productId&t_id=$tId".$key);

        $host = $host . '?operatorId=6&productid=' . $productId .'&t_id='.$tId.'&encrypt='. $enctypt;
        header("Content-type:text/html;charset=utf-8");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $host);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        # do not verify SSL cert
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        Log::error($host);
        $data = curl_exec($curl);
        Log::error('****');
        Log::error($data);
        Log::error('****');
        curl_close($curl);
        return json_decode($data, true);
    }
}
