<?php

namespace App\Http\Controllers;

use App\Events\StartWork;
use App\Models\AdminRoleInfo;
use App\Models\AdminUser;
use App\Service\Examine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class IndexController extends Controller
{
    public function index()
    {
        $user_id=Auth::id();
        $RoleInfo= new AdminRoleInfo();
        $role=$RoleInfo->getRoleDetail(['aid'=>$user_id])->toArray();//获取角色
        $role=array_intersect($role,array('2','10','3'));
        $role=array_values($role);
    
        if(!empty($role)&&count($role)==1){
        
            $data=Examine::getInstance($role[0]);
            $data=$data->detail($role[0],$user_id);
            return view('index/index',['data'=>$data]);
        
        }else if(!empty($role)&&count($role)>1){
        
            $data=Examine::moreRole($role,$user_id);
            return view('index/index',['data'=>$data['info'],'phone'=>$data['phone'],'final'=>$data['final']]);
        
        }
        return view('index/index');
    }


    /**
     * 选择语言
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function switchLocale(Request $request, $locale)
    {

        Session::put('locale', $locale);
        
        return redirect(URL::previous());
    }
    
    /**
     * 切换工作状态
     * @param Request $request
     * $workStatus: 停止工作 1开始信审 2开始电审 3开始终审
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function switchWorkStatus(Request $request)
    {
        $workStatus = $request->get('workStatus');
        $res = AdminUser::saveData(\Auth::id(), ['work_status' => $workStatus]);
        
        //开始工作 $workStatus 1信审 2电审 3终审 4催收（催收为定时脚本自动分配）
        if ($res && in_array($workStatus, [1, 2, 3])) {
            event(new StartWork($workStatus));
        }
    
        return redirect(URL::previous());
    }
    
    /**
     * 刷新工作任务
     * @param Request $request
     * $worktype: 1信审 2电审 3终审
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function refresh(Request $request)
    {
        $worktype = $request->get('worktype');
        
        //开始工作 $worktype 1信审 2电审 3终审
        if (in_array($worktype, [1, 2, 3])) {
            event(new StartWork($worktype));
        }
        
        return redirect(URL::previous());
    }
    
}
