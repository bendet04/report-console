<?php
/**
 * 终审任务重新分配
 * @author  chenwr
 * @since   2018年5月31日
 * @version v1.0
 */

namespace App\Http\Controllers;

use App\Consts\PayType;
use App\Models\AdminUser;
use App\Models\LoanTrialFinal;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Service\Assign;

class TaskFinalController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'task');
    }
    
    /**
     * 终审任务分配列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function finaltrial(Request $request)
    {
        $model = OrderFactory::getInstance('trialFinal');
    
        //获取终审人员列表
        $finalUser = AdminUser::getListByRole(3);
    
        $finalArr = $finalUser->pluck('admin_username', 'id');
        
        //检索条件
        $condition['order_number'] = $request->post('order_number');
        $condition['name'] = $request->post('name');
        $condition['admin_id'] = $request->post('admin_id');
        $condition['order_status'] = 21;
    
        #获取订单列表
        $orderBy = empty($_COOKIE['finaltrial']) ? 'loan_order.id' : $_COOKIE['finaltrial'];
        $orders = $model->getOrderList($condition, $orderBy);
        $orders->map(function($order) use($finalArr) {
            $order['admin_user'] = $finalArr->get($order->admin_id);
            $order['pay_type'] = PayType::toString($order->pay_type);
            return $order;
        });
        
        
        return view('taskfinal/finaltrial', [
            'orders' => $orders,
            'finalUser' => $finalUser,
            'pageCondition' => $this->getCondition($request),
        ]);
    }
    /**
     * 终审重新分件页面
     * @param Request $request
     * @return View
     * @date 18-7-9
     */
    public function parts()
    {

        //获取电审人员列表
        $infoUser = AdminUser::getListByRole(3);
        $realUser = AdminUser::selectOprator(3);

        return view('/taskfinal/parts', [
            'infoUser' => $infoUser,
            'realUser' => $realUser,
        ]);
    }
    /**
     * 终审重新分配
     * @param Request $request
     * @return View
     * @date 18-7-9
     */
    public function realparts(Request $request)
    {

        $this->validate($request, [
            'admin_id' => 'required',
            'needAssign' => 'required',
        ]);

        $needAssigninfo = [];
        $needAssign = $request->input('needAssign');

        foreach ($needAssign as $v){
            array_push($needAssigninfo,collect(['id' => $v]));
        }
        $admin_id = $request->input('admin_id');

        $needAssignAdmin =LoanTrialFinal::selectTrailByAdminId($admin_id);


        foreach ($needAssignAdmin as $k => $v){
            Assign::assign($v->order_id,'final',$needAssigninfo);
            Log::info('assign admin id is '. $admin_id . ' order id is '.$v->order_id);

        }

        return redirect('/taskfinal/finaltrial');

    }
    /**
     * 重新分配页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assign(Request $request)
    {
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
    
        //获取终审人员列表
        $finalUser = AdminUser::selectOprator(3);
        
        return view('taskfinal/assign', [
            'order_id' => $order_id,
            'admin_id' => $admin_id,
            'finalUser' => $finalUser,
        ]);
    }
    
    /**
     * 终审人员再分配
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reassign(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required',
            'admin_id' => 'required',
        ]);
        
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
    
        LoanTrialFinal::saveOne($order_id, [
            'admin_id' => $admin_id,
        ]);
        
        return redirect('/taskfinal/finaltrial');
    }
}
