<?php

namespace App\Http\Controllers;

use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    
    public function showLoginForm()
    {
        return view('user/login');
    }
    
    public function login(Request $request)
    {
        $this->validate($request, [
            'username'  => 'required',
            'pwd'  => 'required',
        ]);
    
        $paramArr = $request->all();
        $paramArr['ip'] = $request->getClientIp();
//        dd($paramArr);
        $user = UserService::attemptLogin($paramArr);
        
        if (empty($user)) {
            return back()->withInput($request->all())->withErrors(['password'=>'用户名或密码错误']);
        }
        
        Auth::loginUsingId($user->id);
        return redirect('/');
    }
    
    public function logout()
    {
        Auth::logout();
        
        return redirect('/login');
    }
}
