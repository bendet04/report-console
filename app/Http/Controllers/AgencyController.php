<?php

namespace App\Http\Controllers;

use App\Models\AdminRoleInfo;
use App\Models\AdminUser;
use Illuminate\Http\Request;

use App\Models\AdminAccess as AccessModel;
use App\Models\AdminRole as RoleModel;
use App\Models\AdminUser as AdminModel;
use App\Models\RoleAccess as RoleAccessModel;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

class AgencyController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'agency');
    }
    
    public function access(Request $request)
    {
        $access = new AccessModel();
        $page = 20;

        $parentAccess = $access->getParentAccessName();
        if($request->input()){

            $accessName = $request->input('depname');
            $rountName = $request->input('rount');

            $where = [];
            if($accessName){
                $where[] = ['access_name', 'like', "%$accessName%"];
            }
            if($rountName){
                $where[] = ['access_url', 'like', "%$rountName%"];
            }
//            $accessName && $access->where('access_name', 'like', "%$accessName%");
//            $rountName && $access->where('access_url', 'like', "%$rountName%");
            $accessDetail = $access->getLikeInfoByAccessName($where, $page);

        } else {
            $accessDetail = $access->selectAccessInfo($page);
        }

        return view("agency/access")
                ->with([
                        'accessDetail' => $accessDetail,
                         'pageCondition' => $this->getCondition($request),
                         'parentAccess' => $parentAccess
                        ])
                ->withInput($request->all());
    }
//    新增权限
    public function addAccess(Request $request)
    {
        $access = new AccessModel();

        $this->validate($request, [
            'accessName'  => 'required',
        ]);
        $where['access_name'] = $request->input('accessName');
        $accessName['access_name'] = $request->input('accessName');

        $where['access_url'] = '';
        $where['access_leval'] = 0;

        if($request->input('leval')){

            $accessId = $access->getIdByAccessName($request->input('leval'));
            $request->offsetSet('levalId', $accessId->id);

            $this->validate($request,[
                'leval'   => 'required',
                'url'     => 'required',
                'levalId' => 'required'
            ]);
            $where['access_leval'] = $request->input('levalId');
            $where['access_url'] = $request->input('url');
        }

        $addAccessInfo = $access->addAccessDetail($accessName,$where);

        if($addAccessInfo){
            return redirect('/agency/access');
        } else {
            return back()->withInput()->with('mes','添加权限失败');
        }
    }
//    修改权限
    public function accessUpdate(Request $request, $id)
    {
        $access = new AccessModel();

        $updateAccessDetail = $access->getAccessDetailById($id);

        $parentAccess = $access->getParentAccessName();

        return view('agency/accessUpdate')
                ->with([
                    'updateAccessDetail' => $updateAccessDetail,
                    'parentAccess'       => $parentAccess
                ])
                ->withInput($request->all());
    }
//    修改信息的详细信息
    public function updateAccessDetail(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);

        $id = $request->input('id');
        $data['access_name']  = $request->input('name');
        if($request->input('first')){
            $data['access_leval'] = $request->input('first');
            $data['access_url']   = $request->input('url');
        }
        $data['is_use'] = $request->input('status');

        $access = new AccessModel();
        $updateInfo = $access->updateAccessInfoById($data, $id);

        if($updateInfo){
            return redirect('/agency/access');
        } else {
            return back()->withInput()->with('mes', '更改权限的信息失败');
        }
    }
//    删除权限
    public function accessDelete(Request $request, $id)
    {
        return view('agency/accessDelete')
            ->with([
                'id' => $id
            ])
            ->withInput($request->all());
    }
    public function accessSoftDelete(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);
        $id = $request->input('id');

        $access = new AccessModel();
        $deleteInfo = $access->softDeleteAccessById($id);
        if($deleteInfo){
            return redirect('/agency/access');
        } else {
            return back()
                ->withInput()
                ->with('mes', '删除权限信息失败');
        }
    }



    public function role(Request $request)
    {
        $page = 20;

        $role = new RoleModel();
        $roleInfo = $role->selectRoleDetail($page);

        if($request->all()){

            $roleName = $request->input('roleName');
            $description = $request->input('descrription');
            $where = [];
            if($roleName){
                $where[] = ['role_name', 'like', "%$roleName%"];
            }
            if($description){
                $where[] = ['role_desc', 'like', "%$description%"];
            }
            $roleInfo = $role->getRoleInfoByLike($where, $page);

        } else {
            $roleInfo = $role->selectRoleDetail($page);
        }

        return view('/agency/role')
            ->with([
                'pageCondition' => $this->getCondition($request),
                'roleInfo' => $roleInfo
            ])
            ->withInput($request->all());
    }
    public function addRole(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required'
        ]);
        $name['role_name'] = $request->input('name');
        $data['role_name'] = $request->input('name');
        $data['role_desc'] = $request->input('desc');
        $data['status'] = $request->input('status');

        $role = new RoleModel();
//        $roleIsExist = $role->selectRoleNameIsExist($data['role_name']);
//        if($roleIsExist){
//            return back()
//                    ->withInput()
//                    ->with('mes', '该角色已存在');
//        }
        $addRoleInfo = $role->addRoleDetail($name,$data);

        if($addRoleInfo){
            return redirect('/agency/role');
        } else {
            return back()
                    ->withInput()
                    ->with('mes', '添加角色信息失败');
        }

    }
    public function roleUpdate(Request $request, $id)
    {
        $role = new RoleModel();
        $roleDetail = $role->selectRoleDetailById($id);

        return view('/agency/roleUpdate')
                ->with([
                    'roleDetail' => $roleDetail
                ]);
    }
    public function updateRoleDetail(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);
        $where['role_name'] = $request->input('name');
        $where['role_desc'] = $request->input('desc');
        $where['status'] = $request->input('status');
        $id = $request->input('id');

        $role = new RoleModel();
        $updateRole = $role->updateRoleDetailById($where, $id);
        if($updateRole){
           return redirect('/agency/role')
               ->withInput($request->all());
        } else {
            return back()->with('mes', '修改角色信息失败');
        }
    }
    public function roleDelete(Request $request, $id)
    {
        return view('/agency/roleDelete')
                ->with([
                    'id' => $id
                ]);
    }
    public function roleSoftDelete(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);

        $id = $request->input('id');
        $role = new RoleModel();
        $softDelete = $role->softDeleteRoleById($id);
        if($softDelete){
            return redirect('/agency/role');
        } else {
            return back()
                        ->with('mes', '删除角色信息失败');
        }
    }
    public function roleAssignAccess(Request $request, $id)
    {
        $page = 20;

        $access = new AccessModel();
        $accessDetail = $access->selectSecondAccessInfo($page);

        $roleAccess = new RoleAccessModel();
        $roleAccessDetail = $roleAccess->getAccessDetailByRole($id);

        return view('/agency/roleAssignAccess')
                ->with([
                    'accessDetail' => $accessDetail,
                    'roleAccessDetail' => $roleAccessDetail,
                    'id' => $id
                ]);
    }
    public function roleAssinAccessDetail(Request $request)
    {
        $this->validate($request,[
            'roleId' => 'required',
            'more' => 'required'
        ]);

        $roleId = $request->input('roleId');
        foreach($request->input('more') as $k => $v)
        {
//            dd($roleId);
            $data['rid'] = $roleId;
            $data['aid'] = $v;

            $roleAccess = new RoleAccessModel();
            $accessInfo = $roleAccess->addRoleAccessInfo($data);
            if(empty($accessInfo)){
                return back()
                        ->with('mes', '角色分配权限失败');
            }

        }

        return redirect("/agency/roleAssignAccess/id/$roleId");
    }
    public function deleteRoleAccess(Request $request, $roleId, $accessId)
    {
//        dd($roleId, $accessId);
        $data['rid'] = $roleId;
        $data['aid'] = $accessId;

        $roleAccess = new RoleAccessModel();
        $deleteInfo = $roleAccess->cancelRoleAccessInfo($data);

        if($deleteInfo){
            return redirect("/agency/roleAssignAccess/id/$roleId");
        } else {
            return back()
                ->with('mes', '删除权限失败');
        }

    }


    public function admin(Request $request)
    {
        $page = 20;
//        var_dump($request->all());
        $currentPage = 1;
        if($request->all('page')){
            $currentPage = $request->get('page');
        }
        $role = new RoleModel();
        $roleDetail = $role->selectRoleDetailInfo();
        $admin = new AdminModel();
        if($request->all()){
            $loginName = $request->input('name');
            $realName = $request->input('realName');

            $where = [];
            if($loginName){
                $where[] = ['admin_username', 'like', "%$loginName%"];
            }
            if($realName){
                $where[] = ['real_username', 'like', "%$realName%"];
            }
            $adminDetail = $admin->selectAdminDetailByLike($where, $page);
        } else {
            $adminDetail = $admin->selectAdminUserDetail($page);
        }


        return view('/agency/admin')
                ->with([
                    'adminDetail' => $adminDetail,
                    'roleDetail' => $roleDetail,
                    'pageCondition' => $this->getCondition($request),
                    'page'  => $currentPage
                ])
                ->withInput($request->all());
    }
    public function  adminNameIsExist(Request $request)
    {
        $this->validate($request, [
            'username' => 'required'
        ]);
        $adminName = $request->input('username');

        $admin = new AdminModel();
        $adminIsExist = $admin->selectAdminNameIsExist($adminName);
        if($adminIsExist){
            return json_encode(['code' => 1, 'info' => '已存在']);
        } else {
            return json_encode(['code' => 0, 'info' => '不存在']);
        }
    }
    public function addAdmin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required'
        ]);

        $admin = new AdminModel();
        $max = $admin->selectMaxNumber();
        $max = empty($max) ? 0 : $max + 1;

        $data['admin_number'] = 'M' . str_pad($max, 6, 0, STR_PAD_LEFT);
        $data['admin_username'] = $request->input('username');
        $name['admin_username'] = $request->input('username');
        $adminIsExist = $admin->selectAdminNameIsExist($data['admin_username']);
        if($adminIsExist){
            return back()
                        ->with('mes', '该管理员已存在');
        }
        $data['real_username'] = empty($request->input('realname')) ? '' : $request->input('realname');
        $data['country_code'] = empty($request->input('code')) ? '+62' : $request->input('code');
        $data['admin_phone']  = empty($request->input('phone')) ? '' : $request->input('phone');
        $data['admin_email'] = empty($request->input('email')) ? '' : $request->input('email');
        $data['admin_address'] = empty($request->input('address')) ? '' : $request->input('address');
        $data['admin_status'] = empty($request->input('status')) ? 0 : 1;
        $data['admin_pwd'] = md5(123456);

        $adminNum = $admin->addAdminDetail($data);

        if($adminNum && ($request->input('role'))){
            $roleData['aid'] = $adminNum->id;
            $roleData['rid'] = $request->input('role');
//            dd($request->all());
            $adminRole = new AdminRoleInfo();
            $assignInfo = $adminRole->adminAssignRole($roleData);
            if($assignInfo){
                return redirect('/agency/admin');
            } else {
                return Redirect::back()
                    ->with('mes', '分配角色是失败');
            }
        } else {
            return Redirect::back()
                        ->with('mes', '添加管理员信息失败');
        }
    }
    public function adminUpdate(Request $request, $id)
    {
        $admin = new AdminModel();
        $adminDetailInfo = $admin->selectAdminDetailById($id);

        $role = new RoleModel();
        $roleDetail = $role->selectRoleDetailInfo();

        return view('agency/adminUpdate')
                ->with([
                    'adminDetailInfo' => $adminDetailInfo,
                    'roleDetail' => $roleDetail,
//                    'page' => $page
                ]);
    }

    public function updateAdminDetail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $id = $request->input('id');
        $where['real_username'] = empty($request->input('realname')) ? '' : $request->input('realname');
        $where['country_code'] = empty($request->input('code')) ? '+62' : $request->input('code');
        $where['admin_phone']  = empty($request->input('phone')) ? '' : $request->input('phone');
        $where['admin_email'] = empty($request->input('email')) ? '' : $request->input('email');
        $where['admin_address'] = empty($request->input('address')) ? '' : $request->input('address');
        $where['admin_status'] = $request->input('status');

        $admin = new AdminModel();
        $updateAdminInfo = $admin->updateAdminInfoById($where, $id);
        if($updateAdminInfo){
            return redirect('/agency/admin');
        } else {
            return back()
                    ->with('mes', '修改管理员失败');
        }
    }
    public function adminDelete(Request $request, $id)
    {
        return view('/agency/adminDelete')
                ->with([
                    'id' => $id
                ]);

    }
    public function adminSoftDelete(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);

        $id = $request->input('id');
        $admin = new AdminModel();
        $softDelete = $admin->softDeleteAdminById($id);
        if($softDelete){
            return redirect('/agency/admin');
        } else {
            return back()
                ->with('mes', '删除角色信息失败');
        }
    }
    public function adminAssignRole(Request $request, $id)
    {
        $page = 20;

        $role = new RoleModel();
        $roleInfo = $role->selectRoleDetail($page);

        $where = [];
        $adminRole = new AdminRoleInfo();
        $where['aid'] = $id;
        $roleDetail = $adminRole->getRoleDetailByAdminId($where);
//        var_dump();
//        dd($roleDetail,$roleInfo);
        return view('/agency/adminAssignRole')
                ->with([
                    'roleInfo' => $roleInfo,
                    'roleDetail' => $roleDetail,
                    'id' => $id
                ]);
    }
    public function assignRoleDetail(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[
            'adminId' => 'required',
            'more' => 'required'
        ]);
        $adminRole = new AdminRoleInfo();

        foreach ($request->input('more') as $key => $value)
        {
            $data['aid'] = $request->input('adminId');
            $data['rid'] = $value;
//            $data['status'] = 0;
            $assinRoleDetail = $adminRole->adminAssignRole($data);
//            dd($assinRoleDetail);
            if(empty($assinRoleDetail)){
                return back()
                    ->with('mes', '分配角色失败');
            }
        }

        return redirect('agency/admin');
    }
    public function cancelRoleDetail(Request $request, $roleId, $adminId)
    {
//        dd($roleId, $adminId);
        $adminRole = new AdminRoleInfo();

        $data['aid'] = $adminId;
        $data['rid'] = $roleId;
        $assinRole = $adminRole->cancelAssignRole($data);
//        dd($assinRole);

        if($assinRole){
            return redirect("agency/adminAssignRole/id/$adminId");
        } else {
            return back()
                ->with('mes', '删除角色失败');
        }
    }


}
