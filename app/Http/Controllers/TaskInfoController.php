<?php
/**
 * 信审任务重新分配
 * @author  chenwr
 * @since   2018年6月12日
 * @version v1.0
 */

namespace App\Http\Controllers;

use App\Consts\PayType;
use App\Models\AdminUser;
use App\Models\LoanTrialInfo;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Service\Assign;

class TaskInfoController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'task');
    }

    /**
     * 信审任务分配列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function infotrial(Request $request)
    {
//        dd($request);
        $admin_id = '';
        if($request->isMethod('get')){
            $admin_id = $request->get('admin_id');
        }
        $model = OrderFactory::getInstance('trialInfo');

        //获取信审人员列表
        $infoUser = AdminUser::getListByRole(2);

        $infoArr = $infoUser->pluck('admin_username', 'id');

//        $condition = [];
//        if($request->isMethod('post')){
            //检索条件
            $condition['order_number'] = $request->post('order_number');
            $condition['name'] = $request->post('name');
            $condition['admin_id'] = $request->post('admin_id');
//            $admin_id = empty($request->post('name')) ? $admin_id : $request->post('name');
//        }


        #获取订单列表
        $orderBy = empty($_COOKIE['infotrial']) ? 'loan_order.id' : $_COOKIE['infotrial'];
        $orders = $model->getOrderList($condition, $orderBy);
        $orders->map(function ($order) use ($infoArr) {
            $order['admin_user'] = $infoArr->get($order->admin_id);
            $order['pay_type'] = PayType::toString($order->pay_type);
            return $order;
        });

        return view('taskinfo/infotrial', [
            'orders' => $orders,
            'infoUser' => $infoUser,
            'admin_id' => $admin_id,
            'pageCondition' => $this->getCondition($request),
            'urlCondition' => $this->getUrlWithCondition($request),
        ]);
    }

    /**
     * 重新分配页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assign(Request $request)
    {
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
        $url = $request->get('url');
//        dd($url);
        //获取终审人员列表
        $infoUser = AdminUser::selectOprator(2);

        return view('taskinfo/assign', [
            'order_id' => $order_id,
            'admin_id' => $admin_id,
            'infoUser' => $infoUser,
            'url' => $url,
        ]);
    }

    public function taskInfoReassign()
    {
        //获取信审人员列表
        $infoUser = AdminUser::getListByRole(2);
        $realUser = AdminUser::selectOprator(2);

        return view('/taskinfo/taskInfoReassign', [
            'infoUser' => $infoUser,
            'realUser' => $realUser,
        ]);
    }

    public function taskInfoHandle(Request $request)
    {

        $this->validate($request, [
            'admin_id' => 'required',
            'needAssign' => 'required',
        ]);

        $needAssigninfo = [];
        $needAssign = $request->input('needAssign');
        foreach ($needAssign as $v){
            array_push($needAssigninfo,collect(['id' => $v]));
        }

        $admin_id = $request->input('admin_id');

        $needAssignAdmin =LoanTrialInfo::selectTrailByAdminId($admin_id);
//        dd($admin_id, $needAssignAdmin);
        foreach ($needAssignAdmin as $k => $v){

            Assign::assign($v->order_id, 'info', $needAssigninfo);
            Log::info('assign admin id is '. $admin_id . ' order id is '.$v->order_id);

        }
        return redirect('/taskinfo/infotrial');

    }
    
    /**
     * 信审人员再分配
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reassign(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required',
            'admin_id' => 'required',
        ]);
        
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
        $url = $request->get('url');

        $res = LoanTrialInfo::saveData($order_id, [
            'admin_id' => $admin_id,
        ]);
//        dd($url);
        return redirect($url);
    }
}
