<?php

namespace App\Http\Controllers;

use App\Consts\LoanDay;
use App\Consts\OrderStatus;
use App\Consts\RepaymentFlowStatus;
use App\Models\LoanOrder;
use App\Models\LoanRePaymentFlow;
use App\Models\OrderUserBasicInfo;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class RepaymentController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'repayment');
    }
    
    /**
     * 还款列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function repaymenthand(Request $request)
    {
        $model = OrderFactory::getInstance('repayment');
        $orders = [];
        
        if ($request->isMethod('post')){
            //检索条件
            $condition['order_number'] = $request->post('order_number');
            $condition['name'] = $request->post('name');
            $condition['platform'] = $request->post('platform');
            $condition['ktp_number'] = $request->post('ktp_number');
            $condition['order_status'] = [9, 10, 11, 12, 13, 14, 25];
        
            $orderBy = empty($_COOKIE['repaymenthand']) ? 'loan_order.id' : $_COOKIE['repaymenthand'];
            $orders = $model->getList($condition, $orderBy);
        }
        return view('repayment/repaymenthand', [
            'orders' => $orders,
            'pageCondition' => $this->getCondition($request),
        ]);
    }
    
    /**
     * 还款流水列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function repaymenthandflow(Request $request)
    {
        $model = OrderFactory::getInstance('repayment');
    
        //检索条件
        $condition['order_number'] = $request->post('order_number');
        $condition['name'] = $request->post('name');
        $condition['platform'] = $request->post('platform');
        $condition['start'] = !empty($request->input("start"))?date('Y-m-d 00:00:00',strtotime($request->input("start"))):null;
        $condition['end'] = !empty($request->input("end"))?date('Y-m-d 23:59:59',strtotime($request->input("end"))):null;
        $condition['platform'] = $request->post('platform');
        $condition['order_status'] = $request->post('order_status', [12, 13]);
//         dd($condition);
        $orderBy = empty($_COOKIE['repaymenthandflow']) ? 'loan_order.id' : $_COOKIE['repaymenthandflow'];
        $orders = $model->getList($condition, $orderBy,'loan_deadline');

        return view('repayment/repaymenthandflow', [
            'orders' => $orders,
            'pageCondition' => $this->getCondition($request),
        ]);
    }
    
    /**
     * 手动全额还款
     * @param Request $request
     * @return mixed
     */
    public function closeout(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required',
        ]);
        
        $order_id = $request->post('order_id');
        $model = OrderFactory::getInstance('repayment');
        return $model->repayment($order_id);
    }
    
    /**
     * 还款详情
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail(Request $request)
    {
        $order_id = $request->post('order_id');

        $repayment = LoanOrder::getOne($order_id);
        $repayment['status'] = OrderStatus::toString($repayment['order_status']);
        $repayment['loan_period_str'] = LoanDay::toString($repayment['loan_period']);
        $user = OrderUserBasicInfo::getOne($repayment->basic_id);
        
        return view('repayment/detail', [
            'repayment' => $repayment,
            'user'      => $user,
        ]);
    }
    
    /**
     * 还款流水查询
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function flow(Request $request)
    {
        $order_id = $request->post('order_id');
    
        $flows = LoanRePaymentFlow::getList($order_id);
        $flows->map(function($flow){
           $flow['status'] = RepaymentFlowStatus::toString($flow->repayment_status);
           return $flow;
        });
        
        return view('/repayment/flow', [
            'flows' => $flows,
        ]);
    }
}
