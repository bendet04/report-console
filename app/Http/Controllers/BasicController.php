<?php

namespace App\Http\Controllers;


use App\Models\BankInfo;
use App\Models\LoanRate;
use App\Models\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class BasicController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'basicData');
    }
    
    public function product(Request $request)
    {
        $product = new Product();
        $productInfo = $product->getAllProductInfo();

        $rate = new LoanRate();
        $rateInfo = $rate->getAllRateInfo();

        return view('/basic/product')
                ->with([
                    'productInfo' => $productInfo,
                    'rateInfo' => $rateInfo
                ]);
    }
    public function productUpdateRate(Request $request, $id)
    {
        $rate = new LoanRate();
        $rateDetail = $rate->getRateInfoById($id);

        return view('/basic/productUpdateRate')
                ->with([
                    'rateDetail' => $rateDetail
                ]);
    }
    public function updateRate(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
        ]);

        $id = $request->input('id');
        $where['rate'] = $request->input('rate');

        $rate = new LoanRate();
        $updateRate = $rate->updateRateById($where, $id);

        if($updateRate){
            return redirect("/basic/product");
        } else {
            return back()
                    ->with('mes', '修改利息失败');
        }
    }
    public function productUpdateDetail(Request $request, $id)
    {
        $product = new Product();
        $productInfo = $product->getProductInfoByPid($id);

        return view('/basic/productUpdateDetail')
                ->with([
                   'product' => $productInfo
                ]);
    }
    public function updateProduct(Request $request)
    {
        $this->validate($request, [
            'pid' => 'required'
        ]);

        $pid = $request->input('pid');
        $where['normal_rate'] = $request->input('normal_rate');
        $where['overdue_rate'] = $request->input('overdue_rate');
        $where['charge_rate'] = $request->input('charge_rate');

        $product = new Product();
        $productInfo = $product->updateProductByPid($where, $pid);
        if ($productInfo) {
            return redirect("/basic/product");
        } else {
            return back()
                ->with('mes', '修改利息失败');
        }
    }
     public function bank(Request $request)
    {
        $page = 20;

        $bank = new BankInfo();
        if($request->all()){

            $where = [];
            if($request->input('parameter_code')){
                $code = $request->input('parameter_code');
                $where[] = ['parameter_code', 'like', "%$code%"];
            }
            if(is_numeric($request->input('parameter_code_type'))){
                $where['parameter_code_type'] = $request->input('parameter_code_type');
            }
            if(is_numeric($request->input('status'))){
                $where['status'] = $request->input('status');
            }
            $bankInfo = $bank->getBankInfoByLike($where, $page);
        } else {
            $bankInfo = $bank->getAllBankInfo($page);
        }

        return view('/basic/bank')
                ->with([
                    'pageCondition' => $this->getCondition($request),
                    'bankInfo' => $bankInfo
                ]);
    }
    public function bankUpdate(Request $request, $id)
    {
        $bank = new BankInfo();
        $bankInfo = $bank->getBankById($id);

        return view('/basic/bankUpdate')
                ->with([
                    'pageCondition' => $this->getCondition($request),
                    'bankInfo' => $bankInfo
                ]);
    }
    public function updatBankInfo(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $where = $request->all();
        unset($where['_token'], $where['id']);
        $id = $request->input('id');

        $bank = new BankInfo();
        $updateInfo = $bank->updateBankInfoById($where, $id);

        if($updateInfo){
            return redirect('/basic/bank');
        } else {
            return back()
                    ->withErrors('修改银行信息失败');
        }
    }
    public function bankAdd(Request $request)
    {
        return view('/basic/bankAdd')
                ->withInput($request->all());
    }
    public function bankAddDetail(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $data['paramer_url'] = " ";

        $bank = new BankInfo();
        $bankInfo = $bank->addBankInfo($data);
        if($bankInfo){
            return redirect('/basic/bank');
        } else {
            return back()
                    ->withErrors('添加银行信息失败');
        }
    }


}
