<?php

namespace App\Http\Controllers;

use App\Consts\Career;
use App\Consts\Level;
use App\Consts\Married;
use App\Consts\OrderStatus;
use App\Consts\PayType;
use App\Consts\Platform;
use App\Consts\Relative;
use App\Consts\WorkPeriod;
use App\Models\AdminUser;
use App\Models\BankInfo;
use App\Models\LoanOrder;
use App\Models\LoanOrderTrack;
use App\Models\OrderUserBasicInfo;
use App\Models\PartnerAccountInfo;
use App\Models\Token;
use App\Models\UserDevice;
use App\Service\Mcrypter;
use App\Service\OrderFactory;
use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class TrackController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'track');
    }
    
    public function checktrack(Request $request)
    {
        $model = OrderFactory::getInstance('track');
        
        //检索条件
        $condition['order_number'] = $request->post('order_number');
        $condition['name'] = $request->post('name');
        $condition['time'] = date('Y-m-d 00:00:00', strtotime('+3 day'));
    
        $status1 = $request->post('status1');
        $status2 = $request->post('status2');
        $condition = $this->parse_status($status1, $status2, $condition);
    
        $orderBy = empty($_COOKIE['checktrack']) ? 'loan_order.id' : $_COOKIE['checktrack'];
        $orders = $model->getOrderList($condition, $orderBy);
        
        return view('track/checktrack', [
            'orders' => $orders,
            'pageCondition' => $this->getCondition($request),
            'back_url' => $this->getUrlWithCondition($request),
        ]);
    }
    
    /**
     * 列表搜索状态特殊处理
     *
     * @param $status1
     * @param $condition
     * @param $status2
     * @return mixed
     */
    public function parse_status($status1, $status2, $condition)
    {
        if ($status1) {
            if ($status1 == 1) { //到期前一天
                $condition['status1'] = ['loan_deadline', '>=', date('Y-m-d 00:00:00', strtotime('+1 day'))];
                $condition['status11'] = ['loan_deadline', '<=', date('Y-m-d 23:59:59', strtotime('+1 day'))];
            } elseif ($status1==9){ //到期前两天
                $condition['status1'] = ['loan_deadline', '>=', date('Y-m-d 00:00:00', strtotime('+2 day'))];
                $condition['status11'] = ['loan_deadline', '<=', date('Y-m-d 23:59:59', strtotime('+2 day'))];
            }elseif ($status1 == 2) {//今日到期
                $condition['status1'] = ['loan_deadline', '>=', date('Y-m-d 00:00:00')];
                $condition['status11'] = ['loan_deadline', '<=', date('Y-m-d 23:59:59')];
            } elseif ($status1 == 3) {//逾期第一天
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 00:00:00')];
                $condition['status11'] = ['loan_overdue_date', '<=', date('Y-m-d 23:59:59')];
            } elseif ($status1 == 4) {//逾期第二天
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 00:00:00', strtotime('-1 day'))];
                $condition['status11'] = ['loan_overdue_date', '<=', date('Y-m-d 23:59:59', strtotime('-1 day'))];
            } elseif ($status1 == 5) {//逾期7天内

                $condition['order_status'] = 11;
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 59:59:59', strtotime('-7 day'))];
            } elseif ($status1 == 6) {//逾期14天内
                $condition['order_status'] = 11;
                $condition['status1'] = ['loan_overdue_date', '>=', date('Y-m-d 59:59:59', strtotime('-14 day'))];
            } elseif ($status1 == 7) {//逾期15天以上
                $condition['status1'] = ['loan_overdue_date', '<=', date('Y-m-d 00:00:00', strtotime('-15 day'))];
            } elseif ($status1 == 8) {//缓期中
                $condition['order_status'] = 10;

            }
        }
        
        if ($status2) {
            if ($status2 == 1) { //承诺今日还款
                $condition['status2'] = ['promise_time', '=', date('Y-m-d')];
            } elseif ($status2 == 2) { //承诺明日还款
                $condition['status2'] = ['promise_time', '=', date('Y-m-d', strtotime('+1 day'))];
            } elseif ($status2 == 3) { //承诺后天还款
                $condition['status2'] = ['promise_time', '=', date('Y-m-d', strtotime('+2 day'))];
            } elseif ($status2 == 4) { //还款意愿弱
                $condition['status2'] = ['promise_time', '=', 'x'];
            } elseif($status2 ==5){ //暂无还款意愿
                $condition['status2'] = ['promise_time', '=', '暂无还款意愿'];
    
            }
        }
        return $condition;
    }
    
    /**
     * 锁定/解锁
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateStatus(Request $request)
    {
        $order_id = $request->input('order_id');
        $back_url = $request->input('back_url');
        
        $track = LoanOrderTrack::getOne($order_id);
//        if (!empty($track) && $track->admin_id==\Auth::id()){
        if (!empty($track)){
            if ($track->status==1){
                $track->status=2;
            }else {
                $track->status=1;
            }
            $track->save();
        }
        
        return redirect($back_url);
    }
    
    /**
     * 详情 - 弹窗页
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail(Request $request)
    {
        $order_id = $request->get('order_id');
        
        $order = LoanOrder::getOne($order_id);
        $order['status'] = OrderStatus::toString($order['order_status']);
        $user = OrderUserBasicInfo::getOne($order->basic_id);
    
        return view('track/detail', [
            'order' => $order,
            'user'  => $user,
        ]);
    }
    /**
     * 催收详情 - 部分还款弹窗页
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detailVA(Request $request)
    {
        $price=$request->get('price',0);
        $bank_code=$request->get('bank_code');
        $order_id=$request->get('order_id');
        $uid=$request->get('uid');
        return view('track/detailva', [
            'price'=>$price,
            'bank_code'=>$bank_code,
            'order_id'=>$order_id,
            'uid'=>$uid,
            ]);
    }
    /**
     * 申请详情页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trackWorktile(Request $request)
    {
        $order_id = $request->get('order_id');
        $back_url = $request->get('back_url');
        $order = LoanOrder::getOne($order_id);
        
        $trackInfo = LoanOrderTrack::getOne($order_id);
        $prevAdminUser = '';
        if (!empty($trackInfo->prev_admin)){
            $prevAdmin = AdminUser::find($trackInfo->prev_admin);
            $prevAdminUser = $prevAdmin->admin_username;
        }
        
//        dd($order);

        //新老用户判断
        $orders = LoanOrder::getList([['uid', '=', $order->uid], ['order_status', '=', 12], ['platform', '=', $order->platform]]);
        $clientType = $this->isOldUserHandDetail($orders, $order->uid);
        
        $user = $order->basic_id ? OrderUserBasicInfo::getOne($order->basic_id) : '';
        $user->career = $user->career ? Career::toString($user->career) : '';
        $user->income_level = $user->income_level ? Level::toString($user->income_level) : '';
        $user->work_period = $user->work_period ? WorkPeriod::toString($user->work_period) : '';
        $user->work_image_url = env('CDN_URL').$user->work_image_url;
        $user->video_url = env('CDN_URL').$user->video_url;
        $user->ktp_image_url = env('CDN_URL').$user->ktp_image_url;
        $user->marriage = $user->marriage? Married::toString($user->marriage) : '';
        $user->relative_type_first = $user->relative_type_first ? Relative::toFirstString($user->relative_type_first) : '';
        $user->relative_type_second = $user->relative_type_second ? Relative::toSecondString($user->relative_type_second) : '';
    
        $order->pay_type = PayType::toString($order->pay_type);
    
        //获取用户邮箱列表
        $email = $this->getEmail($user->uid);
        
        
        //获取当前订单的设备信息
        $device = $this->getDevice($user->device_id, $user->created_at);
        
        //获取其他平台的设备信息
        $deviceNew = '';
        if (!empty($user->uid)) {
            $where[] = ['ktp_number', '=', $user->ktp_number];
            $where[] = ['platform', '!=', $user->platform];
            $otherPlatformOrder = OrderUserBasicInfo::getByWhere($where);
            if (!empty($otherPlatformOrder)) {
                $deviceNew = $this->getDevice($otherPlatformOrder->device_id, $otherPlatformOrder->created_at);
            }
        }
    
        //获取催收通话历史
        $trackDetail = $this->getContactsFromDb($order_id);
    
        
        //获取催收联系电话
        $contacts = $this->getContacts($order_id, $user, $device, $deviceNew);
        $trackDetail['contacts'] = $contacts;
        
        //催收联系电话去除催收通话记录
        $trackDetail = $this->getUniqueContact($trackDetail);
        
        //还款日期列表
        $promise_time_arr = [
            date('Y-m-d'), date('Y-m-d', strtotime('+1 day')), date('Y-m-d', strtotime('+2 day')),
        ];
        
        //获取还款银行列表
        $bankType = $this->getBankTypeByEnv($order->platform);
        $repaymentBanks = BankInfo::getAll($bankType);
        
        
        return view('track/worktile', [
            'order'    => $order,
            'user'     => $user,
            'back_url' => $back_url,
            'track'    => $trackDetail,
            'trackInfo'    => $trackInfo,
            'email'    => $email,
            'promise_time_arr' => $promise_time_arr,
            'clientType' => $clientType,
            'prevAdminUser' => $prevAdminUser,
            'repaymentBanks' => $repaymentBanks,
        ]);
    }
    
    
    public function getBankTypeByEnv($platform)
    {
        $type = env('PAYMENT_CHANNEL','bluepay');
        $forceDoku = Platform::doku($platform);
        if ($forceDoku)
            $type = $forceDoku;
        switch ($type){
            case 'duko':
                return 2;
                break;
            case 'bluepay':
                return 1;
                break;
            default :
                Log::error('bank error');
                return false;
        }
        
    }
    
    public function getVA(Request $request)
    {
        $uid = $request->input('uid');
        $order_id = $request->input('order_id');
        $bank_code = $request->input('bank_code');
        $price= $request->input('price');
        $token = Token::getToken($uid);
        $url = env('API_URL').'/019.minirupiah';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['token'=>$token, 'orderId'=>$order_id,'price'=>$price, 'bankCode'=>$bank_code]);
        $output = curl_exec($ch);
        curl_close($ch);
        $ret = json_decode($output, true);
        if ($ret['resultCode']===10000 && !empty($ret['data'])){
            $vaArr = Mcrypter::decrypt($ret['data']);
            if ($vaArr){
                $vaArr = json_decode($vaArr, true);
                return json_encode(['code'=>1, 'data'=>$vaArr]);
            }else{
                return json_encode(['code'=>2]);
            }
        }else{
            return json_encode(['code'=>2]);
        }
    }
    
    /**
     * 催收联系电话去除催收通话历史的数据
     * @param $track
     * @return mixed
     */
    public function getUniqueContact($track)
    {
        foreach ($track['contacts']['main'] as $k=>$v) {
            foreach ($track['records']['top'] as $key=>$val) {
                if ($v['phone'] == $val['phone']) {
                    unset($track['contacts']['main'][$k]);
                }
            }
            
            foreach ($track['records']['main'] as $key=>$val) {
                if ($v['phone'] == $val['phone']) {
                    unset($track['contacts']['main'][$k]);
                }
            }
        }
        $track['contacts']['main'] = collect($track['contacts']['main'])->unique('phone')->toArray();
        $track['contacts']['main'] = array_values($track['contacts']['main']);
    
        foreach ($track['contacts']['otherPlatform'] as $k=>$v) {
            foreach ($track['records']['top'] as $key=>$val) {
                if ($v['phone'] == $val['phone']) {
                    unset($track['contacts']['otherPlatform'][$k]);
                }
            }
            foreach ($track['records']['otherPlatform'] as $key=>$val) {
                if ($v['phone'] == $val['phone']) {
                    unset($track['contacts']['otherPlatform'][$k]);
                }
            }
        }
        $track['contacts']['otherPlatform'] = collect($track['contacts']['otherPlatform'])->unique('phone')->toArray();
        $track['contacts']['otherPlatform'] = array_values($track['contacts']['otherPlatform']);
    
        foreach ($track['contacts']['other'] as $k=>$v) {
            foreach ($track['records']['top'] as $key=>$val) {
                if ($v['phone'] == $val['phone']) {
                    unset($track['contacts']['other'][$k]);
                }
            }
            foreach ($track['records']['other'] as $key=>$val) {
                if ($v['phone'] == $val['phone']) {
                    unset($track['contacts']['other'][$k]);
                }
            }
        }
        $track['contacts']['other'] = collect($track['contacts']['other'])->unique('phone')->toArray();
        $track['contacts']['other'] = array_values($track['contacts']['other']);

        return $track;
    }
    
    /**
     * 获取设备信息
     * @param $deviceId
     * @param $time
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null|static|static[]
     */
    public function getDevice($deviceId, $time)
    {
        $device = UserDevice::getOne($deviceId, $time);
//        $device = UserDevice::getOne(11, '2018-06-23 12:00:01');
        if(!empty($device->phone_contact)){
            $device->phone_contact = json_decode(Mcrypter::decrypt($device->phone_contact), true);
        }
        if(!empty($device->phone_record)){
            $device->phone_record = json_decode(Mcrypter::decrypt($device->phone_record), true);
        }
        if(!empty($device->phone_message)){
            $device->phone_message = json_decode(Mcrypter::decrypt($device->phone_message), true);
        }
        
        return $device;
    }
    
    /**
     * 获取用户邮箱
     * @param $uid
     * @return array
     */
    public function getEmail($uid)
    {
        $email = [];
        //1:lazada; 2:tokopedia; 3:facebook; 4:instagram; 5:gojek; 6:telkomsel
        $partner = PartnerAccountInfo::getListByUid($uid);
        foreach ($partner as $k=>$v) {
            $info = json_decode($v['info'], true);
            if ($v['type'] == 7 && !empty($info['profile']['email'])) {
                array_push($email, ['name'=>'NPWP', 'email'=>$info['profile']['email']]);
            }
            if ($v['type'] == 5 && !empty($info['my_account']['email'])) {
                array_push($email, ['name'=>'Gojek', 'email'=>$info['my_account']['email']]);
            }
            if ($v['type'] == 1 && !empty($info['task_data']['personal_info']['email_address'])) {
                array_push($email, ['name'=>'Lazada', 'email'=>$info['task_data']['personal_info']['email_address']]);
            }
            if ($v['type'] == 2 && !empty($info['task_data']['personal_profile']['email'])) {
                array_push($email, ['name'=>'Tokopedia', 'email'=>$info['task_data']['personal_profile']['email']]);
            }
            if ($v['type'] == 3 && !empty($info['task_data']['base_info']['emails'][0])) {
                array_push($email, ['name'=>'Facebook', 'email'=>$info['task_data']['base_info']['emails'][0]]);
            }
            if ($v['type'] == 4 && !empty($info['task_data']['profile_info']['email'])) {
                array_push($email, ['name'=>'Instagram', 'email'=>$info['task_data']['profile_info']['email']]);
            }
            if ($v['type'] == 6 && !empty($info['task_data']['account_info']['email'])) {
                array_push($email, ['name'=>'Telkomsel', 'email'=>$info['task_data']['account_info']['email']]);
            }
            
        }
        return $email;
    }
    
    /**
     * 催收联系电话
     * @param $order_id
     * @param $user
     * @return array
     */
    public function getContacts($order_id, $user, $device, $deviceNew)
    {

        $contacts = [
            'main' => [
                ['name' => __('message.phone.ask.personal.phone'), 'phone' => $this->getPhone($user->phone_number), 'isEdit' => false], //本人电话
                ['name' => __('message.phone.company.phone'), 'phone' => $this->getPhone($user->company_phone), 'isEdit' => false], //联系人1
                ['name' => $user->relative_type_first, 'phone' => $this->getPhone($user->relative_phone_first), 'isEdit' => false], //联系人1
                ['name' => $user->relative_type_second, 'phone' => $this->getPhone($user->relative_phone_second), 'isEdit' => false], //联系人2
            ],
            'otherPlatform' => [],
            'other' => [],
        ];

        if(isset($user->whatsapp) && !empty($user->whatsapp)){
            array_unshift($contacts['main'],['name' => 'WhatsApp', 'phone' => $this->getPhone($user->whatsapp), 'isEdit' => false]); //WhatsApp
        }
        
        //获取其他平台差异
        $contacts['otherPlatform'] = $this->getPhoneByDiff($device, $deviceNew);
        
        //获取其他联系人
        $partner = PartnerAccountInfo::getListByUid($user->uid);
        foreach ($partner as $k=>$v) {
            $info = json_decode($v['info'], true);
            if ($v['type'] == 7 && !empty($info['profile']['phone'])) {
                $contacts['other'][] = ['name' => 'NPWP', 'phone' => $this->getPhone($info['profile']['phone']), 'isEdit' => false];
            }
            if ($v['type'] == 5 && !empty($info['my_account']['phone_number'])) {
                $contacts['other'][] = ['name' => 'Gojek', 'phone' => $this->getPhone($info['my_account']['phone_number']), 'isEdit' => false];
            }
            if ($v['type'] == 1 && !empty($info['task_data']['personal_info']['phone_number'])) {
                $contacts['other'][] = ['name' => 'Lazada', 'phone' => $this->getPhone($info['task_data']['personal_info']['phone_number']), 'isEdit' => false];
            }
            if ($v['type'] == 2 && !empty($info['task_data']['personal_profile']['mobile_phone'])) {
                $contacts['other'][] = ['name' => 'Tokopedia', 'phone' => $this->getPhone($info['task_data']['personal_profile']['mobile_phone']), 'isEdit' => false];
            }
            if ($v['type'] == 3 && !empty($info['task_data']['base_info']['mobilephones'][0])) {
                $contacts['other'][] = ['name' => 'Facebook', 'phone' => $this->getPhone($info['task_data']['base_info']['mobilephones'][0]), 'isEdit' => false];
            }
            if ($v['type'] == 4 && !empty($info['task_data']['profile_info']['phone_number'])) {
                $contacts['other'][] = ['name' => 'Instagram', 'phone' => $this->getPhone($info['task_data']['profile_info']['phone_number']), 'isEdit' => false];
            }
        
        }
        //联系人、通话历史添加到其他联系人
        //默认获取通讯录通话最长的、通话次数最多的各20条
        //如果没有，显示手机联系人
        if (!empty($device->phone_record)) {
            //通讯录通话最长的
            $collettion = collect($device->phone_record);
            $record = $collettion->sortByDesc('duration');
            $record->splice(20);
            foreach ($record as $k=>$v){
                if (!empty($v['number']) && !empty($v['name'])){
                    $contacts['other'][] = ['name' => $v['name'], 'phone' => $this->getPhone($v['number']), 'isEdit' => false];
                }
            }
    
            //通话次数最多的各20条
            $record = $collettion->groupBy('number')->sortByDesc(function ($item, $key) {
                return count($item);
            });
            $i = 0;
            foreach ($record as $k=>$v){
                $v = $v[0];
                if (!empty($v['number']) && !empty($v['name'])){
                    $contacts['other'][] = ['name' => $v['name'], 'phone' => $this->getPhone($v['number']), 'isEdit' => false];
                }
                $i++;
                if ($i>=20)
                    break;
            }
        } else {
            if (!empty($device->phone_contact)){
                foreach ($device->phone_contact as $k=>$v){
                    if(!empty($v['mobile']) && !empty($v['name'])) {
                        $contacts['other'][] = ['name' => $v['name'], 'phone' => $this->getPhone($v['mobile']), 'isEdit' => false];
                    }
                }
            }
        }
        
        //如果通话记录太少，增加通讯录列表
        if (count($contacts['other'])<20 && !empty($device->phone_contact)) {
            foreach ($device->phone_contact as $k=>$v){
                if(!empty($v['mobile']) && !empty($v['name'])) {
                    $contacts['other'][] = ['name' => $v['name'], 'phone' => $this->getPhone($v['mobile']), 'isEdit' => false];
                }
            }
        }
        
        //通讯录、通话记录、短信两两重合
        $contacts = $this->getPhoneByIntersect($contacts, $device);
        return $contacts;
    }
    
    /**
     * 获取其他平台差异
     * @param $device
     * @param $deviceNew
     * @return array
     */
    public function getPhoneByDiff($device, $deviceNew)
    {
        $arr = [];
        if(!empty($device->phone_contact) && !empty($deviceNew->phone_contact)){
            $contactArr = [];
            foreach ($device->phone_contact as $k=>$v){
                if(!empty($v['mobile']) && !empty($v['name'])) {
                    $contactArr[$this->getPhone($v['mobile'])] = $v['name'];
                }
            }
    
            $newContactArr = [];
            foreach ($deviceNew->phone_contact as $k=>$v){
                if(!empty($v['mobile']) && !empty($v['name'])) {
                    $newContactArr[$this->getPhone($v['mobile'])] = $v['name'];
                }
            }
            
            $contactAndRecord = array_diff_key($newContactArr, $contactArr);
            foreach ($contactAndRecord as $k=>$v) {
                array_push($arr, ['name'=> $v, 'phone'=>$k, 'isEdit' => false]);
            }
            
        }
    
        if(!empty($device->phone_record) && !empty($deviceNew->phone_record)){
            $recordArr = [];
            foreach ($device->phone_record as $k=>$v){
                if (!empty($v['number']) && !empty($v['name'])){
                    $mobile = $this->getPhone($v['number']);
                    $recordArr[$mobile] = $v['name'];
                }
            }
    
            $newRecordArr = [];
            foreach ($deviceNew->phone_record as $k=>$v){
                if (!empty($v['number']) && !empty($v['name'])){
                    $mobile = $this->getPhone($v['number']);
                    $newRecordArr[$mobile] = $v['name'];
                }
            }
    
            $contactAndRecord = array_diff_key($newRecordArr, $recordArr);
            foreach ($contactAndRecord as $k=>$v) {
                array_push($arr, ['name'=> $v, 'phone'=>$k, 'isEdit' => false]);
            }
        }
    
        return $arr;
    }
    
    
    /**
     * 通讯录、通话记录、短信两两重合
     * @param $contacts
     * @param $device
     * @return mixed
     */
    public function getPhoneByIntersect($contacts, $device)
    {
        //==============通讯录与通话记录、短信重合===========
        if(!empty($device->phone_contact)){
            $contactArr = [];
            foreach ($device->phone_contact as $k=>$v){
                if(!empty($v['mobile']) && !empty($v['name'])) {
                    $contactArr[$this->getPhone($v['mobile'])] = $v['name'];
                }
            }
        
            //通讯录与通话记录重合
            $recordArr = [];
            if (!empty($device->phone_record)) {
                foreach ($device->phone_record as $k=>$v){
                    if(!empty($v['number'])) {
                        $mobile = $this->getPhone($v['number']);
                        $recordArr[$mobile] = 1;
                    }
                }
                $contactAndRecord = array_intersect_key($contactArr, $recordArr);
                foreach ($contactAndRecord as $k=>$v) {
                    array_push($contacts['main'], ['name'=> __('track.contactAndRecord').'-'.$v, 'phone'=>$k, 'isEdit' => false]);
                }
            }
        
            //短信
            $messageArr = [];
            if(!empty($device->phone_message)){
                foreach ($device->phone_message as $k=>$v){
                    if(!empty($v['mobile'])){
                    $mobile = $this->getPhone($v['mobile']);
                    $recordArr[$mobile] = 1;
                    }
                }
                $contactAndMessage = array_intersect_key($contactArr, $messageArr);
                foreach ($contactAndMessage as $k=>$v) {
                    array_push($contacts['main'], ['name'=> __('track.contactAndMessage').'-'.$v, 'phone'=>$k, 'isEdit' => false]);
                }
            }
        }
    
        //==================通话记录与短信重合=============
        if (!empty($device->phone_record)){
            $recordArr = [];
            foreach ($device->phone_record as $k=>$v){
                if (!empty($v['number']) && !empty($v['name'])){
                $mobile = $this->getPhone($v['number']);
                $recordArr[$mobile] = $v['name'];
                }
            }
        
            //短信
            $messageArr = [];
            if(!empty($device->phone_message)){
                foreach ($device->phone_message as $k=>$v){
                    if (!empty($v['mobile'])){
                    $mobile = $this->getPhone($v['mobile']);
                    $recordArr[$mobile] = 1;
                    }
                }
                $recordAndMessage = array_intersect_key($recordArr, $messageArr);
                foreach ($recordAndMessage as $k=>$v) {
                    array_push($contacts['main'], ['name'=> __('track.recordAndMessage').'-'.$v, 'phone'=>$k, 'isEdit' => false]);
                }
            }
        }
        
        return $contacts;
    }
    
    
    /**
     * 尝试从数据库获取
     * @param $order_id
     * @return array|null
     */
    public function getContactsFromDb($order_id)
    {
        $track = LoanOrderTrack::getOne($order_id);
        if (!empty($track['records'])) {
            $records = json_decode($track['records'], true);
        
            //处理输入框默认值
            if (!empty($records['top'])){
                foreach ($records['top'] as $k=>$v){
                    if (!empty($v['remarkList'][0]) && $v['remarkList'][0]['time']==date('Y-m-d')) {
                        $records['top'][$k]['remark'] = $v['remarkList'][0]['remark'];
                    } else {
                        $records['top'][$k]['remark'] = '';
                    }
                    if (empty($v['remarkList'])){
                        $records['top'][$k]['remarkList'] = [];
                    }
                }
            }
        
            if (!empty($records['main'])){
                foreach ($records['main'] as $k=>$v){
                    if (!empty($v['remarkList'][0]) && $v['remarkList'][0]['time']==date('Y-m-d')) {
                        $records['main'][$k]['remark'] = $v['remarkList'][0]['remark'];
                    } else {
                        $records['main'][$k]['remark'] = '';
                    }
                    if (empty($v['remarkList'])){
                        $records['main'][$k]['remarkList'] = [];
                    }
                }
            }
        
            if (!empty($records['other'])){
                foreach ($records['other'] as $k=>$v){
                    if (!empty($v['remarkList'][0]) && $v['remarkList'][0]['time']==date('Y-m-d')) {
                        $records['other'][$k]['remark'] = $v['remarkList'][0]['remark'];
                    } else {
                        $records['other'][$k]['remark'] = '';
                    }
                    if (empty($v['remarkList'])){
                        $records['other'][$k]['remarkList'] = [];
                    }
                }
            }
        
            return [
                'records' => [
                    'top' => isset($records['top']) ? $records['top'] : [],
                    'main' => isset($records['main']) ? $records['main'] : [],
                    'otherPlatform' => isset($records['otherPlatform']) ? $records['otherPlatform'] : [],
                    'other' => isset($records['other']) ? $records['other'] : [],
                ],
                'promise_time' => $track['promise_time']
            ];
        }
    
        return [
            'records' => [
                'top' => [],
                'main' => [],
                'otherPlatform' => [],
                'other' => [],
            ],
            'promise_time' => ''
        ];
    }
    
    /**
     * 保存催收信息
     * @param Request $request
     * @return json
     */
    public function save(Request $request)
    {
        $remarks = $request->post('remarks');
        $order_id = $request->post('order_id');
        $records = $request->post('records');
        $promise_time = $request->post('promise_time');
        $type = $request->post('type'); //1上一条 2下一条
        $back_url = $request->post('back_url');
        $result = LoanOrderTrack::saveOne($order_id, [
            'admin_id' => \Auth::id(),
            'contacts' => '',
            'records' => $records ? $records : '',
            'remark' => $remarks,
            'promise_time' => $promise_time ? $promise_time : '',
        ]);
        
        //获取上一条、下一条
        $model = OrderFactory::getInstance('track');
    
        //检索条件
        $condition['time'] = date('Y-m-d 00:00:00', strtotime('+2 day'));
        $condition['type'] = [$type==1 ? '>' : '<', $order_id];
        
        //分析backurl的参数
        $urlArr = parse_url($back_url);
        if (!empty($urlArr['query'])) {
            parse_str($urlArr['query'], $uriArr);
            foreach ($uriArr as $k=>$v) {
                if ($k==='page'){
                    continue;
                }
                $condition[$k] = $v;
            }
    
            if (isset($condition['status1'])) {
                $status1 = $condition['status1'];
                unset($condition['status1']);
            } else {
                $status1 = 0;
            }
            if (isset($condition['status2'])) {
                $status2 = $condition['status2'];
                unset($condition['status2']);
            } else {
                $status2 = 0;
            }
            
            $condition = $this->parse_status($status1, $status2, $condition);
        }
    
        //上一页是正序，下一页倒序
        if ($type == 1){
            $orderBy = empty($_COOKIE['checktrack']) ? ['loan_order.id', 'asc'] : [$_COOKIE['checktrack'], 'asc'];
        }else{
            $orderBy = empty($_COOKIE['checktrack']) ? 'loan_order.id' : $_COOKIE['checktrack'];
        }
        $orders = $model->getOrderList($condition, $orderBy, 1);
        
        return ['result' => 'success', 'order_id' => !empty($orders[0]) ? $orders[0]->id : 0];
    }
    
    public function getPhone($mobile)
    {
        $mobile = ltrim($mobile, '+62');
        $mobile = ltrim($mobile, '0');
        $mobile = str_replace(' ', '', $mobile);
        return $mobile;
    }

    public function isOldUserHandDetail($orders, $userId)
    {
        $clientType['clientType'] = ($orders->count() > 0) ? '老客户' : '新客户';
        $clientType['normalCount'] = 0;
        $clientType['overdueCount'] = 0;
        $clientType['applyCount'] = 0;
        $clientType['lastStatus'] = 0;
        $clientType['select'] = 0;
        if ($orders->count() > 0) {
            foreach ($orders as $ko => $vo) {
                if ($vo['overdue_amount'] > 0) {
                    $clientType['overdueCount']++;
                } else {
                    $clientType['normalCount']++;
                }
            }
        } else {
            $newOrder = LoanOrder::getList(['uid' => $userId]);
            if ($newOrder->count() > 0) {
                $clientType['applyCount'] = $newOrder->count() - 1;
                $recentOrder = LoanOrder::getByUserId($userId);
                if ($recentOrder) {
                    $status = \App\Models\OrderStatus::getByOrderId($recentOrder->id);
                    $clientType['lastStatus'] = OrderStatus::toString($status->status);
                    $clientType['select'] = 1;
                }

            }

        }
        return $clientType;
    }
    
}
