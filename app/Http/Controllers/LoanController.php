<?php

namespace App\Http\Controllers;

use App\Consts\OrderStatus;
use App\Models\Black;
use App\Models\LoanOrder;
use App\Models\LoanTrialInfoRecord;
use App\Models\OrderUserBasicInfo;
use App\Models\RiskControlRecord;
use App\Models\UserAccess;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'loan');
    }
    
    /**
     * 信审列表页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loanInfo(Request $request)
    {
        $model = OrderFactory::getInstance('trialInfo');
        $orderNumber = 0;

        if($request->isMethod('post')){

            $orderNumber = $request->all('order_number');
            $paramArr['order_number'] = $request->all('order_number');
            $orders = $model->getOrderList($paramArr);

        } else {

            $orders = $model->getOrderList();

        }

        return view('loan/loanInfo', [
            'orders' => $orders,
            'orderNumber'  => $orderNumber
        ]);
    }
    
    public function otherOrder(Request $request)
    {
        $id = $request->input('id');
        $model = OrderFactory::getInstance('trialInfo');
        $paramArr['loan_order.id'] = $id;
        $orders = $model->getOrderList($paramArr);
    
        $otherOrder = [];
        if (!empty($orders[0]['name'])) {
            $otherOrder = $model->getSameNameOrder($orders[0]['name'], $id, 1);
        }
        
        return view('loan/otherOrder', [
            'id' => $id,
            'orders' => $orders,
            'otherOrder' => $otherOrder,
            'backUrl' => '/loan/loginInfo',
        ]);
    }
    
    public function otherKtpOrder(Request $request)
    {
        $id = $request->input('id');
        $model = OrderFactory::getInstance('trialInfo');
        $paramArr['loan_order.id'] = $id;
        $paramArr['order_status'] = 21;
        $orders = $model->getOrderList($paramArr);
        $back_url   = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '/final/trialFinal';
        $otherOrder = [];
        if (!empty($orders[0]['ktp_number'])) {
            $basicIds = $model->getKtpSimlarOrder($orders[0]['ktp_number']);
            $otherOrder = $model->getSameKtpOrder($basicIds, $id, 1);
        }
        return view('loan/otherOrder', [
            'id' => $id,
            'orders' => $orders,
            'otherOrder' => $otherOrder,
            'backUrl' => '/final/trialFinal',
            'back_url' => $back_url,
        ]);
    }
    
    public function otherOrderRefuse(Request $request)
    {
        $order_id = $request->input('order_id');
        $order = LoanOrder::getOne($order_id);
        
        $model = OrderFactory::getInstance('trialInfo');
        if ($order->order_status==17) {
            $model->trial([
                'order_id' => $order_id,
                'pic_status' => 2,
                'npwp_status' => 2,
                'img_status' => [],
                'npwp' => '',
            ]);
        } elseif ($order->order_status==19) {
            $model->updateOrderStatus($order_id, 20);
        } elseif ($order->order_status==21) {
            $model->updateOrderStatus($order_id, 5);
        }
        
        $orderBasic = OrderUserBasicInfo::getOne($order->basic_id);
        $userAccess = UserAccess::getByUserId($order['uid']);
        Black::saveData(['order_id' => $order_id], [
            'order_id' => $order_id,
            'ktp_number' => $orderBasic['ktp_number'],
            'uuid' => $userAccess['uuid'],
            'username' => $orderBasic['name'],
        ]);
        
        $post = [];
        $post['userlists'][0]['ktp_number'] = $orderBasic['ktp_number'];
        $post['userlists'][0]['uuid'] = $userAccess['uuid'];
        $post['userlists'][0]['username'] = $orderBasic['name'];
        $post['userlists'][0]['reason'] = 'info black';
        $ret = RiskControlRecord::addBlack(['userlists' => json_encode($post)], '/gather/add/blacklist');
        
        return 10000;
    }
    
    public function loanInfoTrial(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        unset($data['orderNumber']);
       
        $data['img_status'] =isset($data['img_status'])?array_filter($data['img_status']):[];
        $data['admin_id'] = Auth::id();
        $trial = OrderFactory::getInstance('trialInfo');
        $trialInfo = $trial->trial($data);


        if(!empty($request->all('orderNumber')['orderNumber'])){
            return json_encode(['code' => 2, 'info' => 'select by order number finish']);
        }


        if($trialInfo&&$trialInfo['code']!=11){
            return json_encode(['code' => 1, 'info' => 'trial success']);
        } elseif ($trialInfo&&$trialInfo['code']==11) {
            return json_encode(['code' =>11, 'info' => $trialInfo['info']]);
        } else {
           return json_encode(['code' => 0, 'info' =>'trial failure']);
        }

    }
    public function selectSingleOrder(Request $request)
    {
      $model = OrderFactory::getInstance('trialInfo');

      $orderNumber = $request->all('order_number');
      $paramArr['order_number'] = $request->all('order_number');
      $orders = $model->getOrderList($paramArr);


        return json_encode(['code' => 2, info => 'select by choose finish' ]);
    }
    public function add(Request $request)
    {
        $model = OrderFactory::getInstance('trialInfo');
        $orders = $model->getOrderList();
        
        $orderCount = $request->get('orderCount', 0);

        if($orders->count() <= $orderCount){
            $remainingOrder = "";
        } else {
            $remainingCount = $orders->count() - $orderCount;
            $remainingOrder = $orders->take($remainingCount);
        }

        $orderNumber = "";
        
        return view('/loan/add', [
            'orders' => $remainingOrder,
            'orderNumber'  => $orderNumber
        ]);
    }
}
