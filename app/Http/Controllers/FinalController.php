<?php

namespace App\Http\Controllers;

use App\Consts\Career;
use App\Consts\College;
use App\Consts\Level;
use App\Consts\LoanDay;
use App\Consts\Married;
use App\Consts\OrderStatus;
use App\Consts\PayType;
use App\Consts\Period;
use App\Consts\Platform;
use App\Consts\Relative;
use App\Consts\Risk;
use App\Consts\WorkPeriod;
use App\Models\AdminRoleInfo;
use App\Models\LoanOrder;
use App\Models\LoanTrialFinal;
use App\Models\LoanTrialInfoRecord;
use App\Models\LoanTrialPhone;
use App\Models\OrderUserBasicInfo;
use App\Models\RiskControlRecord;
use App\Models\UserBasicInfo;
use App\Models\UserDevice;
use App\Models\UserWorkInfo;
use App\Service\face\CurlClient;
use App\Service\Foundation\DateUnit;
use App\Service\Mcrypter;
use App\Service\OrderFactory;
use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FinalController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'loan');
    }

    /**
     * 终审列表页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trialFinal(Request $request)
    {
        $model = OrderFactory::getInstance('trialFinal');
        $condition['loan_order.platform'] = $request->input('platform');
        $condition['name'] = $request->input('name');
        $condition['order_number'] = $request->input('order_number');
        $condition['ktp_number'] = $request->input('ktp_number');
        $condition['userType'] = $request->input('userType');//1新用户 2老用户
        $condition['order_status'] = $request->input('order_status', 21);//电审通过待终审
        $condition['phone_number'] = $request->input('phone_number');
        $condition['paidperiod'] = $request->input('paidperiod');

        $orderBy = empty($_COOKIE['trialFinal']) ? 'loan_order.id' : $_COOKIE['trialFinal'];
        $orders = $model->getOrderList($condition, $orderBy);


        //如果是超级管理员，计算并更新用户发薪日
        if (LoanOrder::isSuperAdmin()>0 && $request->isMethod('get')) {
            $model->updatePaidPeriod();
        }


        return view('final/trialFinal', ['orders' => $orders, 'back_url' => $this->getUrlWithCondition($request), 'pageCondition' => $this->getCondition($request),]);
    }

    /**
     * 终审审核页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trialFinalDetail(Request $request)
    {
//        $this->validate($request, ['order_id' => 'required',]);
        $order_id = $request->get('order_id');
//        $order_id ='93470';
        $back_url = $request->get('back_url');
        $order = LoanOrder::getOne($order_id);
        $uid = $order->uid;
        $userBasic = UserBasicInfo::getOne(['uid' => $uid]);
        $order->loan_period = LoanDay::toString($order->loan_period);
        $riskScore = null;
        //新老用户判断
        $orders=LoanOrder::getUserType($order->platform, $order->basic_id);
        //初始化风控
        $riskStatus = env('RISK_STATUS');
        //获取风控评分
        if ($riskStatus == 1) {
            $riskScore = $this->curlGet($order->order_number);
        }
        $clientType = $this->isOldUserHandDetail($orders, $order->uid);
        //获取用户信息快照
        $userBasicInfo = OrderUserBasicInfo::getOne($order->basic_id);
        $incomeLevel = Level::toInt($userBasicInfo->income_level);
        $userBasicInfo->income_level = Level::toString($userBasicInfo->income_level);
        $userBasicInfo->education = College::toString($userBasicInfo->education);
        $userBasicInfo->birth = date('Y', time()) - date('Y', strtotime($userBasicInfo->birth));
        $userBasicInfo->living_period = Period::toString($userBasicInfo->living_period);
        $userBasicInfo->age = $this->getAge($userBasicInfo->ktp_number);


        //根据ktp查询其他平台并行订单
        $isHaveLoan = $this->isHaveLoanInprogess($userBasicInfo->platform, $userBasicInfo->ktp_number, $userBasicInfo->bank_number);
        foreach ($isHaveLoan as $k=>$v){
            if (isset($v->platform)&&($v['oldUser']||$v['overDue']||$v['order_status']==11)) {

            }else{
                unset($isHaveLoan[$k]);
            }
        }

//        dd($isHaveLoan);
//        $oldPlatfromIsOldUser = $this->anotherPlatformIsOldUser($userBasicInfo->platform, $userBasicInfo->ktp_number);
//        $anotherPlatformIsOverDue = $this->anotherPlatformIsOverDue($userBasicInfo->platform, $userBasicInfo->ktp_number);
//        dd($isHaveLoan);

//        获得与发薪日的差别
//        $userBasicInfo->paid_period = 'Bulanan  5';
        $diffDay = $this->getDiffGetSalaryPeriod($userBasicInfo->paid_period, $order->loan_period );
//        dd($diffDay);
        $lastOrder = $this->getLastOrderInfo($order_id,$order->uid);
//
        //证件照审核
        $model = OrderFactory::getInstance('trialFinal');
        $idPhoto = $model->getIdPhoto($order_id);

        //获取用户订单渠道来源
        $channel = LoanOrder::getChannel($order_id);

        //信审复核
        $infoTrial = LoanTrialInfoRecord::getOne($order_id);

        //电审复核
        $phoneModel = OrderFactory::getInstance('trialPhone');
        $phoneTrial = $phoneModel->getOrder(['order_id' => $order_id,]);
//        dd($phoneTrial);
        //用户信息
        if (!isset($phoneTrial->user)) {
            $phoneTrial = new LoanTrialPhone();
            $phoneTrial->user = OrderUserBasicInfo::getOne($order->basic_id);
        }
        $phoneTrial->user->career = Career::toString($phoneTrial->user->career);
        $phoneTrial->user->income_level = Level::toString($phoneTrial->user->income_level);
        $phoneTrial->user->work_period = WorkPeriod::toString($phoneTrial->user->work_period);
        $phoneTrial->user->work_image_url = env('CDN_URL') . $phoneTrial->user->work_image_url;
        $phoneTrial->user->video_url = env('CDN_URL') . $phoneTrial->user->video_url;
        $phoneTrial->user->marriage = Married::toString($phoneTrial->user->marriage);
        $phoneTrial->user->relative_type_first = Relative::toFirstString($phoneTrial->user->relative_type_first);
        $phoneTrial->user->relative_type_second = Relative::toSecondString($phoneTrial->user->relative_type_second);
        $order->pay_type = PayType::toString($order->pay_type);
        $pay_day = 'Tanggal '.date('d', strtotime('+'.$order->loan_period.' days'));

        //总一致数判断
        list($rightCount, $normalCount) = $this->getCount($phoneModel->pass_count, $phoneTrial, $infoTrial);

        //判断是否是当前审核人或超级管理员
        $isOwner = $this->isOwner($order_id, $order->order_status);

//         dd($userBasicInfo->ktp_number,$userBasicInfo->name,$order->order_number);
        //设备数据获取
        $device = $this->getDevice($userBasicInfo->device_id, $userBasicInfo->created_at);

        //计算贷款额度
        $loanLimit = $this->getLoanLimit($order->uid, $incomeLevel, $phoneTrial, $isHaveLoan);
        $ktpSimlar = $model->getKtpSimlarOrder($userBasicInfo->ktp_number);
//        $device = $this->getDevice(11, '2018-06-23 12:00:01');
        return view('final/trialFinalDetail', [
            'order_id' => $order_id,
            'riskStatus' => $riskStatus,
            'riskScore' => $riskScore,
            'order' => $order,
            'clientType' => $clientType,
            'userBasicInfo' => $userBasicInfo,
            'idPhoto' => $idPhoto,
            'infoTrial' => $infoTrial,
            'phoneTrial' => $phoneTrial,
            'rightCount' => $rightCount,
            'normalCount' => $normalCount,
            'isOwner' => $isOwner,
            'back_url' => $back_url,
            'diffDay' => $diffDay,
            'device' => $device,
            'channel' => $channel,
            'isHaveLoan' => $isHaveLoan,
//            'oldPlatfromIsOldUser' => $oldPlatfromIsOldUser,
//            'anotherPlatformIsOverDue' => $anotherPlatformIsOverDue,
            'lastOrder' => $lastOrder,
            'loanLimit' => $loanLimit,
            'payDay' => $pay_day,
            'url' => $this->getUrlWithCondition($request),
            'ktpSimlar' => $ktpSimlar,
            'userBasic'    => $userBasic,
        ]);
    }
    public function verityKtp(Request $request)
    {
        $back_url = $request->get('back_url');
        $host = env('KTP_HOST');
        $key = env('KTP_KEY');
        $secret = env('KTP_SECRET');
        $apiName = '/openapi/anti-fraud/v4/identity-check';


        $id = $request->get('id');
        $order = LoanOrder::getOne($id);
        $uid = $order->uid;
        $basic = UserBasicInfo::getOne(['uid' => $uid]);

        $data['name']     = $basic->name;
        $data['idNumber'] = $basic->ktp_number;
        $client = new CurlClient($host,$key, $secret);
        $result = $client->request($apiName, $data);
        $result = json_decode($result);
        if (isset($result->code) && $result->code == 'SUCCESS') {

            UserBasicInfo::saveOne(['uid' => $uid], ['ktp_verity' => 1]);

            return json_encode(['code' => 1,'info' =>  __('final.has.verity')]);
        } else if (isset($result->code) && ($result->code == 'INVALID_ID_NUMBER' || $result->code == 'PERSON_NOT_FOUND')) {
//            dd(111);
            UserBasicInfo::saveOne(['uid' => $uid], ['ktp_verity' => 2]);
            return json_encode(['code' => 2,'info' =>  __('final.has.verity.fail')]);
        }

        return json_encode(['code' => 0,'info' =>  "try again!!!!!"]);


    }

    public function diffBasicInfo($basic1, $basic2)
    {
        if (empty($basic1) || empty($basic2))
            return '';
        
        $ret = '';
        $arr = [
            __('final.card.info') => [
                'payee_name',
                'bank_name',
                'bank_phone',
                'bank_number',
            ],
        ];
        
        foreach ($arr as $k=>$v) {
            foreach ($v as $key=>$val) {
                if ($basic1[$val] !== $basic2[$val]) {
                    $ret = $ret.$k.' ';
                    break;
                }
            }
        }
        
        return $ret;
    }
    
    /**
     * 计算贷款额度
     * @param $uid
     * @param $incomeLevel
     * @param $isHaveLoan
     * @return int
     */
    public function getLoanLimit($uid, $incomeLevel, $phoneTrial, $isHaveLoan)
    {
        $totalRepayment = 1;

        if(isset($phoneTrial->work['levelRemark']) && $phoneTrial->work['levelRemark']){
            if($phoneTrial->work['levelRemark'] && strpos($phoneTrial->work['levelRemark'], 'real')!==false){
                $incomeLevel = Level::toInt(ltrim($phoneTrial->work['levelRemark'], 'real'));
            }
        }

        $npwp = UserService::partnerInfo($uid, 'npwp');
        if (!empty($npwp) && !empty($npwp['spt_info'][0]['induk_info']['total_earning_net'])) {
            $incomeLevel = (int)$npwp['spt_info'][0]['induk_info']['total_earning_net'] / 12;
        }

        foreach ($isHaveLoan as $k=>$v){
            if (!empty($v) && !empty($v->order_status) && in_array($v->order_status, [11, 13, 14]))
                $totalRepayment += $v->repayment_amount;
        }

        $loanLimit = ($incomeLevel/$totalRepayment) * 1.2;
        return (int)$loanLimit;
    }

    /**
     * 得到用户上个订单的信息
     * @param $order_id
     * @param $uid
     * @return bool|\Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getLastOrderInfo($order_id, $uid)
    {
        $order = LoanOrder::getLastOrder($order_id, $uid);

        if(empty($order) || $order->count() == 0){
            return false;
        }
        return $order;
    }

    /**
     * 与发薪日相差的时间
     * @param $salary
     * @param $period
     * @return array|bool
     */
    public function getDiffGetSalaryPeriod($salary, $period)
    {
//        $period = 20;
       $now = date('d',strtotime("+$period day"));
//       dd($now);
       $day = $this->handleSalary($salary);
       if($day){
            return [
                'diff' => abs($day - $now),
                'now'  => $now,
                'sal'  => $day,
            ];
       }
       return false;
    }

    /**
     *  匹配发薪日的数字
     * @param $salary
     * @return bool
     */
    public function handleSalary($salary)
    {
        if(strstr($salary, 'Bulanan')){
            $reg='/\d+/';//匹配数字的正则表达式
            preg_match_all($reg,$salary,$result);
            if(is_array($result)){
                return $result[0][0];
            }
            return false;
        }
        return false;
    }

    /**
     * 针对新老用户做的处理
     * @param $orders
     * @param $userId
     * @return mixed
     */
    public function isOldUserHandDetail($orders, $userId)
    {
        $clientType['clientType'] = ($orders->count() > 0) ? '老客户' : '新客户';
        $clientType['normalCount'] = 0;
        $clientType['overdueCount'] = 0;
        $clientType['applyCount'] = 0;
        $clientType['lastStatus'] = 0;
        $clientType['select'] = 0;
        if ($orders->count() > 0) {
            foreach ($orders as $ko => $vo) {
                if ($vo['loan_repayment_date'] < $vo['loan_overdue_date']) {
                    $clientType['normalCount']++;
                } else {
                    $clientType['overdueCount']++;
                }
            }
        } else {
            $newOrder = LoanOrder::getList(['uid' => $userId]);
            if ($newOrder->count() > 0) {
                $clientType['applyCount'] = $newOrder->count() - 1;
                $recentOrder = LoanOrder::getByUserId($userId);
                if ($recentOrder) {
                    $status = \App\Models\OrderStatus::getByOrderId($recentOrder->id);
                    $clientType['lastStatus'] = OrderStatus::toString($status->status);
                    $clientType['select'] = 1;
                }

            }

        }
        return $clientType;
    }

    /**
     * 查看用户在其他平台是否存在并行订单
     * @param $platform
     * @param $ktp_number
     * @param $user_bank
     * @return bool
     */
    public function isHaveLoanInprogess($platform, $ktp_number, $user_bank)
    {
        $ret = [];
        $otherPlatfrom = $this->handFilterPlatform($platform);

        $ret = $this->checkKtpIsHaveLoan($ktp_number,$otherPlatfrom);
        if($ret)
            return $ret;

        $ret = $this->checkBankNumberIsHaveLoan($user_bank, $otherPlatfrom);
        if($ret)
            return $ret;

        return $ret;
    }

    /**
     *  过滤平台
     * @param $platfrom
     * @return array
     */
    public function handFilterPlatform($platfrom)
    {

        $arr = Platform::toString();
        array_walk($arr, function($v, $k) use(&$platfrom, &$arr){
//            dd($v, $platfrom);
            if($v == $platfrom)
                unset($arr[$k]);
        });

        return $arr;
//        return $info;
    }

    /**
     * 查看ktp是否有存在的订单
     * @param $otherPlatfrom
     * @return array|bool
     */
    public function checkKtpIsHaveLoan($ktp_number,$otherPlatfrom)
    {
        $ret = [];
        foreach ($otherPlatfrom as $v){
            $basic = OrderUserBasicInfo::getByWhere([
                ['ktp_number', '=', $ktp_number],
                ['platform', '=', $v]
            ]);
            if(empty($basic)){
                $ret[] = '';
            } else {
//                dd(1, $this->checkUserHasLoanInprogress($basic->id, $v, $ktp_number));
                $ret[] = $this->checkUserHasLoanInprogress($basic->id, $v, $ktp_number);
            }

        }
//        dd($ret);
        return $ret;
    }
    public function checkBankNumberIsHaveLoan($bank_number,$otherPlatfrom)
    {
        $ret = [];
        foreach ($otherPlatfrom as $v){
            $basic = OrderUserBasicInfo::getByWhere([
                ['bank_number', '=', $bank_number],
                ['platform', '=', $v]
            ]);

            if(empty($basic)){
                $ret[] = '';
            } else {
//                dd(1, $this->checkUserHasLoanInprogress($basic->id, $v, $ktp_number));
                $ret[] = $this->checkUserHasLoanInprogress($basic->id, $v);
            }
        }
        return $ret;
    }


    /**
     * 判断该用户在另一个平台是否是老用户
     * @param $platform
     * @param $ktp_number
     * @return int
     */
    public function anotherPlatformIsOldUser($platform, $ktp_number)
    {
        $where = [
            ['order_user_basic_info.platform', '=', $platform],
            ['order_status', '=', 12],
            ['ktp_number', '=', $ktp_number],
            ['overdue_amount', '=', 0],
        ];
        $query = LoanOrder::select('loan_order.uid');
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where($where);
        $orders = $query->get();

        return  count($orders);
    }

    /**
     * 判断该用户在另一个平台的正在进行中的订单是否逾期
     * @param $platform
     * @param $ktp_number
     * @return int
     */
    public function anotherPlatformIsOverDue($platform, $ktp_number)
    {
        $where = [
            ['order_user_basic_info.platform', '=', $platform],
            ['order_status', '=', 12],
            ['ktp_number', '=', $ktp_number],
            ['overdue_amount', '>', 0],
        ];
        $query = LoanOrder::select('loan_order.uid');
        $query->join('order_user_basic_info', 'order_user_basic_info.id', '=', 'loan_order.basic_id');
        $query->where($where);
        $orders = $query->get();

        return count($orders);

    }
    /**
     * 检查用户订单是否已完结
     * @param $basic_id
     * @return bool
     */
    public function checkUserHasLoanInprogress($basic_id, $platorm = '', $ktp_number='')
    {
        $order = LoanOrder::getByBasicId($basic_id);
        $order['oldUser'] = '';
        $order['overDue'] = '';
        if($ktp_number){
            $order['oldUser'] = $this->anotherPlatformIsOldUser($platorm, $ktp_number);
            $order['overDue'] = $this->anotherPlatformIsOverDue($platorm, $ktp_number);
        }
        if(empty($order))
            return $order;

        $status = $order->order_status;
        $loanDead = $order->loan_overdue_date;
        $cancelStatus = [2, 5, 23, 12, 26, 27];
        if (in_array($status, $cancelStatus)) {
            return $order;
        }

        $order->order_status_str = OrderStatus::toString($status);
        $time = new DateUnit();
        $day = $time->timeToDay($loanDead);
        if ($status == 11) {
            $order->order_status_str = '逾期' . $day . '天';
        }


        return $order;
    }

    /**
     * 终审审核操作
     * @param Request $request
     * @return array
     */
    public function trialjalksealksdfjkasdlf98qeja(Request $request)
    {
        $this->validate($request, ['order_id' => 'required', 'status' => 'required', 'remark' => 'required',]);
        $paramArr['order_id'] = $request->post('order_id');
        $paramArr['status'] = $request->post('status');
        $paramArr['remark'] = $request->post('remark');//1审核通过 2拒绝
        $paramArr['admin_id'] = Auth::id();
        $userWork['income_audit'] = $request->post('level');
        $uid = $request->post('uid');
        if (isset($uid, $userWork['income_audit'])) {
            UserWorkInfo::updateLevel($uid, $userWork);
        }
        
        $model = OrderFactory::getInstance('trialFinal');
        $res = $model->trial($paramArr);

        return $res;
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function selectHistory(Request $request)
    {
        $uid = $request->get('uid');
        $platform = $request->get('platform');
        $basic_id = $request->get('basic_id');
        $orders = LoanOrder::getListByTime($platform, $basic_id);

        foreach ($orders as $k => $v) {
            $orders[$k]->refuse='---';
            //获取拒绝原因
            if($v->order_status==18) {
                $refuse = RiskControlRecord::getByReason($v->id);
                $orders[$k]->refuse= __('message.'.$refuse->refuse_id);
            }
            $status = \App\Models\OrderStatus::getByOrderId($v->id);
            $orders[$k]->order_status_str = OrderStatus::toString($status->status);
        }

        $otherPlatformOrder = $this->getOtherPlatformOrder($uid);

        return view("final/_detail_history_select", [
            'orders' => $orders,
            'otherPlatformOrder'=> $otherPlatformOrder,
        ]);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function selectHistoryOld(Request $request)
    {
        $uid = $request->get('uid');
        $platform = $request->get('platform');
        $basic_id = $request->get('basic_id');
        $orders = LoanOrder::getListByTime($platform, $basic_id);
        foreach ($orders as $k => $v) {
            //获取风控拒绝原因
            if($v->order_status==18) {
                $refuse = RiskControlRecord::getByReason($v->id);
                $orders[$k]->refuse= __('message.'.$refuse->refuse_id);
            }
            //逾期
            if($v->order_status==11){
                $orders[$k]->overdue=$this->diffBetweenTwoDays(date('Y-m-d H:i:s'),$v->loan_deadline);
            }
            if($v->order_status==12){
                $check=\App\Models\OrderStatus::OverDueStatus($v->id)->toArray();
                if(in_array(11,$check)){
                    if(!empty($v->loan_repayment_date)){
                        $orders[$k]->overdue=$this->diffBetweenTwoDays($v->loan_repayment_date,$v->loan_deadline);
                    }else{
                        $orders[$k]->overdue=$this->diffBetweenTwoDays(date('Y-m-d H:i:s',time()),$v->loan_deadline);
                    }
                }
                $orders[$k]->loan_repayment_date=date('m-d H时',strtotime($v->loan_repayment_date));

            }

            $status = \App\Models\OrderStatus::getByOrderId($v->id);
            $orders[$k]->order_status_str = OrderStatus::toString($status->status);
        }

        $otherPlatformOrder = $this->getOtherPlatformOrder($uid);

        return view("final/_detail_history_old_select", [
            'orders' => $orders,
            'otherPlatformOrder'=> $otherPlatformOrder,
        ]);
    }

    public function getOtherPlatformOrder($uid)
    {
        $order = LoanOrder::where('uid', $uid)->first();
        $user = UserBasicInfo::where('uid', $uid)->first();
        $orders = LoanOrder::getOtherPlatformOrder($user->ktp_number, $order->platform);

        foreach ($orders as $k => $v) {
            $status = \App\Models\OrderStatus::getByOrderId($v->id);
            $orders[$k]->order_status_str = OrderStatus::toString($status->status);

            if ($v->order_status==12){
                if (substr($v['loan_deadline'], 0, 10) === substr($v['loan_repayment_date'], 0, 10)) {
                    $orders[$k]->overdueStatus = __('final.normalRepayment');
                } else{
                    $days = \App\Service\DateUnit::getDiffDay($v['loan_deadline'], $v['loan_repayment_date']);
                    $orders[$k]->overdueStatus = __('track.overdueDay', ['day'=>$days]);
                }
                $orders[$k]->loan_repayment_date = date('m-d H时',strtotime($v['loan_repayment_date']));
            }else{
                $orders[$k]->overdueStatus = '--';
            }
        }

        return $orders;
    }

    /**
     * 获取第三方授权信息详情页
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function partnerInfo(Request $request)
    {
        $uid = $request->get('uid');
        $partnerName = $request->get('partner_name');
        $partner = UserService::partnerInfo($uid, $partnerName);

        if (!$partner) {
            return '用户未授权！';
        }
//        return $partner;
        return view("final/{$partnerName}", ['partner' => $partner,]);
    }

    /**
     * 总一致数、建议通过数计算
     * @param $phoneModel
     * @param $phoneTrial
     * @param $infoTrial
     * @return array
     */
    public function getCount($pass_count, $phoneTrial, $infoTrial)
    {
        $rightCount = 0;
        $normalCount = $pass_count;
        if (!empty($phoneTrial->work)) {
            $phoneTrial->work = json_decode($phoneTrial->work, true);
            foreach ($phoneTrial->work as $k => $v) {
                if (!is_array($v) && $k != 'company_phone') {
                    if ($v === "1") $rightCount++;
                }
            }
        }
        if (!empty($phoneTrial->peason)) {
            $phoneTrial->peason = json_decode($phoneTrial->peason, true);
            foreach ($phoneTrial->peason as $k => $v) {
                if (!is_array($v) && $k != 'phone_number') {
                    if ($v === "1") $rightCount++;
                }
            }
        }

        if (!empty($infoTrial)) {
            if ($infoTrial->pic_status == 1) {
                $rightCount++;
            }
            if ($infoTrial->npwp_status == 1) {
                $rightCount++;
            }
            //如果npwp是从同盾获取+2，否则+1
            if ($infoTrial->npwp) {
                $normalCount = $normalCount + 2;
            } else {
                $normalCount = $normalCount + 1;
            }
        }

        return array($rightCount, $normalCount);
    }

    /**
     * 判断是否是当前审核人或超级管理员
     * @param $order_id
     * @param $order
     * @return bool
     */
    public function isOwner($order_id, $order_status)
    {
        $isOwner = false;

        //判断终审人是不是当前用户
        $finalTrial = LoanTrialFinal::getOne($order_id);

        //判断是不是超级管理员
        $isSuperAdmin = LoanOrder::isSuperAdmin();
        if ($order_status == 21 && ($finalTrial->admin_id == Auth::id() || $isSuperAdmin)) {
            $isOwner = true;
        }

        return $isOwner;
    }

    /**
     * 获取设备数据
     * @param $device_id
     * @param $date
     */
    public function getDevice($device_id, $date)
    {
        $device = UserDevice::getOne($device_id, $date);

        //通讯录
        if (!empty($device->phone_contact)) {
            $device->phone_contact = json_decode(Mcrypter::decrypt($device->phone_contact), true);
        }

        //通话记录
        if (!empty($device->phone_record)) {
            $device->phone_record = json_decode(Mcrypter::decrypt($device->phone_record), true);
        }

        //短信
        if (!empty($device->phone_message)) {
            $device->phone_message = json_decode(Mcrypter::decrypt($device->phone_message), true);
        }

        //手机app
        if (!empty($device->app_list)) {
            $device->app_list = json_decode(Mcrypter::decrypt($device->app_list), true);
        }

        return $device;
    }

    public function curlGet($order_id)
    {
        $testurl = env('RISK_URL') . "/gather/score?order_id=" . $order_id;
//        $testurl = "http://10.2.3.174:4539/gather/score?order_id=".$order_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $testurl);
        //參数为1表示数据传输。为0表示直接输出显示。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //參数为0表示不带头文件，为1表示带头文件
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
        $output = curl_exec($ch);
        $output = json_decode($output, true);
        curl_close($ch);
        $arr = [];

        if ($output['result_code'] == 10000) {
            foreach ($output['data']['detail'] as $k => $v) {
                if(!isset($v['RS_SCORE_Cross_SMS_And_Contact']))
                {
                    $arr[Risk::toString('RS_SCORE_Cross_SMS_And_Contact')]='+0';
                }
                if(!isset($v['RS_SCORE_Cross_Addressbook_And_Contact']))
                {
                  $arr[Risk::toString('RS_SCORE_Cross_Addressbook_And_Contact')]='+0';
                }
                if(!substr_count($v['score'],'-')){
                   $arr[Risk::toString($v['id'])] = '+' . $v['score'];
                }else{
                    $arr[Risk::toString($v['id'])] =$v['score'];
                }
            }
//               dd($arr);
               $arr=$this->negative($arr,'+');
               $arr= $this->negative($arr,'-');


            $arr['result_code'] = 1000;
            $arr['total'] = $output['data']['total'];
            return $arr;
        } else {
            return $output;
        }
    }

    public function curlPost(Request $request)
    {

        $type = $request->post('type');
        $post_data = $this->getParam($type);
        $uri = $request->post('url');
        $ktp_number = $request->post('ktp');
        $username = rawurlencode($request->post('username'));
        $url = env('RISK_URL') . $uri . 'ktp_number=' . $ktp_number . '&username=' . $username;
        $curl = curl_init();
        //设置提交的url
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        //设置获取的信息以文件流的形式返回。而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //參数为0表示不带头文件，为1表示带头文件
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        //运行命令
        $data = curl_exec($curl);
        //获得数据并返回
        if ($data != false) {
            $data = json_decode($data, true);
//            dd($data);
            if (!empty($data['result_code']) && $data['result_code'] == 10000 && ($data['data']['total'] != 0 || !empty($data['data']['total']))) {
                $blade = $this->getblade($type);
                return view($blade, ['data' => $data]);
            } else {
//                 Log::error('risk_return_code'.$data['result_code']);
                Log::error('risk_return_code201' . $data['result_code']);
                return '201';
            }

        } else {
            Log::error('risk202' . curl_error($curl));
            return '202';
        }
        curl_close($curl);

    }

    public function getParam($type)
    {
        switch ($type) {
            case "sms":
                $data['phone'][] = 'whiteList';
                $data['phone'][] = 'blacklist';
                $data['phone'][] = 'contact';
                $data['phone'][] = 'call_log';
                $data['phone'][] = 'addressbook';
                $keywords['refund'][] = 'telah dilunasi';
                $keywords['refund'][] = 'pinjaman anda telah lunas';
                $keywords['refund'][] = 'anda telah berhasil mengembalikan';
                $keywords['refund'][] = 'Pinjaman Anda berhasil dilunasi';
                $keywords['refund'][] = 'Pembayaran kembali Anda telah diterima';
                $keywords['refund'][] = 'telah lunas';
                $keywords['refund'][] = 'tlh lunas';
                $keywords['refund'][] = 'sdh lunas';
                $keywords['refund'][] = 'Pinjaman kali ini telah lunas';
//                $keywords['refund'][] = 'telah berhasil melakukan pembayaran';
//                $keywords['refund'][] = 'pembayaran anda berhasil';
//                 $keywords['overdue'][]='melewati';
                $keywords['overdue'][] = 'telah terlambat';
                $keywords['overdue'][] = 'telah terlambat selama';
                $keywords['overdue'][] = 'telah perpanjang';
                $keywords['overdue'][] = 'telah melewati';
                $keywords['overdue'][] = 'denda';
                $keywords['overdue'][] = 'telah biaya';
                $keywords['overdue'][] = 'jumlah tunggakan';
                $keywords['overdue'][] = 'biaya fee keterlambatan';
                $keywords['overdue'][] = 'lewat jatuh tempo';
                $keywords['overdue'][] = 'lwt jth tempo';
                $keywords['overdue'][] = 'Anda telah terlambat melunasi pinjaman selama';
                $keywords['overdue'][] = 'anda telah melewati tangal jatuh tempo， pinjaman anda telah';
                $keywords['loan'][] = 'loan';
                $keywords['loan'][] = 'pinjam';
                $keywords['loan'][] = 'dana';
                $keywords['loan'][] = 'cash';
                $keywords['loan'][] = 'rupiah';
                $keywords['loan'][] = 'uang';
                $keywords['loan'][] = 'kredit';
                $keywords['loan'][] = 'tunai';
                $post_data = [
                    'phone' => json_encode($data['phone']),
                    'keywords' => json_encode($keywords)
                ];
                return $post_data;
                break;
            case "cl":
                $data['phone'][] = 'whiteList';
                $data['phone'][] = 'total';
                $data['phone'][] = 'blacklist';
                $data['phone'][] = 'contact';
                $data['phone'][] = 'addressbook';
                $keywords['call_log'][] = 'call_log';
                $keywords['contact'][] = 'contact';
                $keywords['whiteList'][] = 'whiteList';
                $keywords['blacklist'][] = 'blacklist';
//                $keywords['loan'][]='rupiah';
                $post_data = [
                    'phone' => json_encode($data['phone']),
                    'keywords' => json_encode($keywords)
                ];
                return $post_data;
                break;
            case "ab":
                $data['phone'][] = 'whiteList';
                $data['phone'][] = 'total';
                $data['phone'][] = 'blacklist';
                $data['phone'][] = 'contact';
                $data['phone'][] = 'addressbook';
                $keywords['call_log'][] = 'call_log';
                $keywords['contact'][] = 'contact';
                $keywords['whiteList'][] = 'whiteList';
                $keywords['blacklist'][] = 'blacklist';
                $post_data = [
                    'phone' => json_encode($data['phone']),
                    'keywords' => json_encode($keywords)
                ];
                return $post_data;
                break;

            case "app":
                $keywords['loan'][] = 'loan';
                $keywords['loan'][] = 'pinjam';
                $keywords['loan'][] = 'dana';
                $keywords['loan'][] = 'cash';
                $keywords['loan'][] = 'rupiah';
                $keywords['loan'][] = 'uang';
                $keywords['loan'][] = 'kredi';
                $keywords['loan'][] = 'tunai';
                $post_data = [
                    'keywords' => json_encode($keywords)
                ];
                return $post_data;
                break;
            default:
                break;
        }

    }

    public function getblade($type)
    {
        switch ($type) {
            case 'sms':
                return 'final/_detail_device_risk';
                break;
            case 'cl':
                return 'final/_detail_device_cl';
                break;

            case 'ab':
                return 'final/_detail_device_ab';
                break;

            case 'app':
                return 'final/_detail_device_app';
                break;
            default:
                break;

        }
    }

    /**
     * 根据ktp获取年龄
     * @param $ktp_number
     * @return false|string
     */
    public function getAge($ktp_number)
    {
        $age = '';
        if ($ktp_number) {
            $birthYear = substr($ktp_number, 10, 2);
            if (!$birthYear)
                return $age;

            if ($birthYear>11) {
                $birthYear = '19'.$birthYear;
            } else {
                $birthYear = '20'.$birthYear;
            }

            $age = date('Y')-$birthYear;
        }

        return $age;
    }
   function negative($data,$flag)
   {
       $center=$data;
       $arr['total'.$flag]=0;
       foreach ($data as $k=>$v)
       {
           if(!substr_count($data[$k],$flag))
          {
           $data[$k]=0;
          }
       }
       foreach ($data as $k=>$v)
       {
           $arr['total'.$flag]+=$data[$k];//总加分
           if(!isset($data['overdue_sms'],$data['callog_addressbook'],$data['spouse'])){
               $data['overdue_sms']=0;
               $data['callog_addressbook']=0;
               $data['spouse']=0;
           }
           if(isset($data['np_concat_telkomse'],$data['gojek_lazada_toko'],$data['fabk_inst'],$data['threeDegree'])) {
               $arr['threePhone' . $flag] = $data['np_concat_telkomse'] + $data['gojek_lazada_toko'] + $data['fabk_inst'] + $data['threeDegree'];
           }else{
               $arr['threePhone'.$flag]=0;
               $data['np_concat_telkomse']=0;
               $data['gojek_lazada_toko']=0;
               $data['fabk_inst']=0;
               $data['threeDegree']=0;
           }//三方数据交叉评分规则
           if(isset($data['sms_contains_cash'],$data['loan_success'],$data['sa_loan'],$data['inCome_whether_wages'],$data['inCome_loan_amount'],$data['oldUser_applyAmount'],$data['amount_reminder'])) {
               $arr['wealthCashloans' . $flag] = $data['sms_contains_cash'] + $data['loan_success'] + $data['sa_loan'] + $data['inCome_whether_wages']+$data['inCome_loan_amount']+$data['oldUser_applyAmount']+$data['amount_reminder'];
           }else{
               $arr['wealthCashloans' . $flag]=0;
               $data['sms_contains_cash']=0;
               $data['loan_success']=0;
               $data['sa_loan']=0;
               $data['inCome_whether_wages']=0;
               $data['inCome_loan_amount']=0;
               $data['oldUser_applyAmount']=0;
               $data['amount_reminder']=0;
           }//财富与现金贷
           $arr['mpwpAdd'.$flag]=$data['sub_npwp']+$data['npwp_tel']+$data['npwp_name']+$data['npwp_email']+$data['npwp_income'];
           $arr['telkomselAdd'.$flag]=$data['sub_telkomsel']+$data['telkomsel_tel']+$data['telkomsel_credits']+$data['telkomsel_points']+$data['telkomsel_expire'];
           $arr['lazadalAdd'.$flag]=$data['sub_lazada']+$data['lazada_tel']+$data['lazada_name']+$data['lazada_address']+$data['lazada_email'];
           $arr['tokopediaAdd'.$flag]=$data['sub_tokopedia']+$data['tokopedia_tel']+$data['tokopedia_name']+$data['tokopedia_address']+$data['tokopedia_email'];
           $arr['facebookAdd'.$flag]=$data['sub_facebook']+$data['facebook_tel']+$data['facebook_work']+$data['facebook_last_login'];
           $arr['gojekAdd'.$flag]=$data['sub_gojek']+$data['gojek_tel']+$data['gojek_email']+$data['gojek_point']+$data['gojek_number'];
           $arr['onlineretailers'.$flag]=$arr['gojekAdd'.$flag]+ $arr['lazadalAdd'.$flag]+$arr['tokopediaAdd'.$flag];//电商
           $arr['instagramAdd'.$flag]=$data['sub_instagram']+$data['instagram _tel']+$data['instagram _number'];
           $arr['socialcontact'.$flag]=$arr['facebookAdd'.$flag]+  $arr['instagramAdd'.$flag];//社交
           $arr['otherCardBigthree'.$flag]=$data['othrt_cards'];//其他证件上传大于等于三张
           $arr['applyPhoneSameBank'.$flag]=$data['sub_call_bank'];//申请的手机号与银行预留手机号一致
           $arr['finalKeywords'.$flag]=$data['app_loan_keyword']+$data['app_not_keywords'];//关键字
           $arr['contactRecordMseg'.$flag]=$data['ab_keywords']+$data['ab_num']+$data['has_parent']+$data['sms_ab']+$data['person_call']+$data['ab_call']+$data['sms_concat']+$data['ab_concat']+$data['callog_addressbook'];//联系人及通话记录与通讯录情况交叉
           $arr['messageKeyWordSame'.$flag]=$data['sms_pay_off']+$data['sms_received']+$data['sms_ok_pay']+$data['sms_keyword']+$data['overdue_sms'];//短信及短信、关键词交叉
           $arr['threeparty'.$flag]=$arr['mpwpAdd'.$flag]+$arr['telkomselAdd'.$flag]+$arr['lazadalAdd'.$flag]+$arr['tokopediaAdd'.$flag]+$arr['facebookAdd'.$flag]+$arr['gojekAdd'.$flag]+ $arr['instagramAdd'.$flag]+$arr['threePhone'.$flag];//交叉评分
           $arr['modelAdd'.$flag]=$data['marry']+$data['sex']+$data['age']+$data['purpose']+$data['degree']+$data['live_lenght']+$data['work']+$data['work_length']+$data['income']+$data['spouse'];//用户模型
           $arr['willingnessAdd'.$flag]=$arr['finalKeywords'.$flag]+$arr['applyPhoneSameBank'.$flag]+$arr['otherCardBigthree'.$flag]+$arr['messageKeyWordSame'.$flag]
               +$arr['contactRecordMseg'.$flag]+$arr['wealthCashloans' . $flag];//还款意愿
       }
      $result=array_merge($center,$arr);
       return $result;
   }
    /**
     * 求两个日期之间相差的天数
     * (针对1970年1月1日之后，求之前可以采用泰勒公式)
     * @param string $day1
     * @param string $day2
     * @return number
     */
    function diffBetweenTwoDays ($day1, $day2)
    {
        $second1 = strtotime($day1);
        $second2 = strtotime($day2);

        if ($second1 < $second2) {
            $tmp = $second2;
            $second2 = $second1;
            $second1 = $tmp;
        }
        $date=($second1 - $second2)/86400;
        $day=(int)round($date);
        if($day==0){
            return 1;
        }else{
            return $day;
        }
    }
}
