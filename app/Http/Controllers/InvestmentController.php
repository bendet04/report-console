<?php

namespace App\Http\Controllers;

use App\Consts\InvestmentRelationStatus;
use App\Consts\InvestmentStatus;
use App\Consts\OrderStatus;
use App\Models\Investment_user;
use App\Models\InvestsmentRelation;
use App\Models\PushToken;
use App\Service\OrderFactory;
use App\Service\OverMessage;
use App\Service\PushSevice;
use Illuminate\Http\Request;
use App\Models\Investment;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class InvestmentController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'Investment');
    }
    
    //设置产品参数
    public function setting(Request $request)
    {
        $data=Investment::getList();
        return view('investment/setting')->with([
            'data' => $data,
        ]);
    }
    
    //投资审核
    public function examine(Request $request)
    {
        $param=[];
        if($request->input('start')) {
            $param[] = ['created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->input('start')))];
        }
        if($request->get('end'))
        {
            $param[] = ['created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->get('end')))];
        }
        if($request->input('apply_order'))
        {
            $param[] = ['apply_order', '=', $request->input('apply_order')];
        }
        if($request->input('ktpNumber'))
        {
            $param[] = ['ktpNumber', '=', $request->input('ktpNumber')];
        }
        if($request->input('mobile'))
        {
            $param[] = ['mobile', '=', $request->input('mobile')];
        }
        $param[] = ['status', '=', InvestmentStatus::INVESTMENTSTATUS1];
        
        $data = Investment_user::getList($param);
        $data->map(function($investment){
            $investment->totalLoanCount = InvestsmentRelation::getCountByInvestId($investment->id);
            return $investment;
        });
        
        return view('investment/examine', [
            'data' => $data,
            'pageCondition' => $this->getCondition($request),

        ]);

    }
    
    //放款审核
    public function payment(Request $request)
    {
        $param=[];
        if($request->input('start')) {
            $param[] = ['created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->input('start')))];
        }
        if($request->get('end'))
        {
            $param[] = ['created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->get('end')))];
        }
        if($request->input('apply_order'))
        {
            $param[] = ['apply_order', '=', $request->input('apply_order')];
        }
        if($request->input('ktpNumber'))
        {
            $param[] = ['ktpNumber', '=', $request->input('ktpNumber')];
        }
        if($request->input('mobile'))
        {
            $param[] = ['mobile', '=', $request->input('mobile')];
        }
        $param[] = ['status', 'in', [InvestmentStatus::INVESTMENTSTATUS4, InvestmentStatus::INVESTMENTSTATUS5]];
        
        $data = Investment_user::getByPayment($param);
        $data->map(function($investment){
            $investment->totalLoanCount = InvestsmentRelation::getCountByInvestId($investment->id);
            return $investment;
        });
        
        return view('investment/payment', [
            'data' => $data,
            'pageCondition' => $this->getCondition($request),
        ]);
    }
    
    //提现审核
    public function apply(Request $request)
    {
        $param=[];
        if($request->input('start')) {
            $param[] = ['created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->input('start')))];
        }
        if($request->get('end'))
        {
            $param[] = ['created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->get('end')))];
        }
        if($request->input('apply_order'))
        {
            $param[] = ['apply_order', '=', $request->input('apply_order')];
        }
        if($request->input('ktpNumber'))
        {
            $param[] = ['ktpNumber', '=', $request->input('ktpNumber')];
        }
        if($request->input('mobile'))
        {
            $param[] = ['mobile', '=', $request->input('mobile')];
        }
        $param[] = ['status', 'in', [InvestmentStatus::INVESTMENTSTATUS7, InvestmentStatus::INVESTMENTSTATUS8]];
        $data = Investment_user::getList($param);
        
        $data->map(function($investment){
            $investment->totalLoanCount = InvestsmentRelation::getCountByInvestId($investment->id);
            return $investment;
        });
        
        return view('investment/apply', [
            'data' => $data,
            'pageCondition' => $this->getCondition($request),
        ]);
    }
    
    //投资订单查询
    public function query(Request $request)
    {
        $param = [];
        if ($request->input('start')) {
            $param[] = ['created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->input('start')))];
        }
        
        if ($request->get('end'))
        {
            $param[] = ['created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->get('end')))];
        }
        
        if ($request->input('apply_order')) {
            $param[] = ['apply_order', '=', $request->input('apply_order')];
        }
    
        $totalInvestmentAmount = 0;
        if ($request->input('status')) {
            $status = $request->input('status');
            $dateStart = $request->input('start') ? $request->input('start') : date('Y-m-d H:i:s');
            $dataEnd = $request->input('end') ? $request->input('end') : date('Y-m-d H:i:s');
            
            if ($status<50){
                $param[] = ['status', '=', $status];
            } elseif($status==50) {
                $param = [];
                $param[] = ['payTime', '>=', date('Y-m-d 00:00:00', strtotime($dateStart))];
                $param[] = ['payTime', '<=', date('Y-m-d 23:59:59', strtotime($dataEnd))];
                $total = Investment_user::getListCount($param);
                if ($total->total)
                    $totalInvestmentAmount = $total->total;
                
            } else if($status==51) {
                $param = [];
                $param[] = ['investment_relation.updated_at', '>=', date('Y-m-d 00:00:00', strtotime($dateStart))];
                $param[] = ['investment_relation.updated_at', '<=', date('Y-m-d 23:59:59', strtotime($dataEnd))];
                $totalInvestmentAmount = InvestsmentRelation::getListCompleteCount($param);
            }
        }
    
    
        if ($request->input('status') && $request->input('status')==51) {
            $data = InvestsmentRelation::getListComplete($param);
        } else {
            $data = Investment_user::getList($param);
        }
        $data->map(function($investment){
            $investment->status_str = InvestmentStatus::toString($investment->status);
            return $investment;
        });
        return view('investment/query', [
            'data' => $data,
            'pageCondition' => $this->getCondition($request),
            'totalInvestmentAmount' => (int)$totalInvestmentAmount,
        ]);
    }
    
    //修改产品页面
    public function save(Request $request){
        $id = $request->get('id');
        $data = Investment::find($id);
        
        return view('investment/save', ['data' => $data]);
    }
    
    //修改产品信息
    public function update(Request $request){
        $pid = $request->input('pid');
        $where['period'] = $request->input('period');
        $where['rate'] = $request->input('rate');

        $data = Investment::updateOne($where, $pid);
        if($data){
            return redirect("/Investment/setting");
        } else {
            return back()->with('mes', '修改失败');
        }
    }
    
    public function del(Request $request){
        $id = $request->get('id');
        $data = Investment_user::del($id);
        if($data){
            return redirect("/Investment/query");
        }else{
            return back()->with('mes', '删除失败');
        }
    }
    
    //获取审核信息
    public function getData(Request $request)
    {
        $id = $request->post('id');
        $data = Investment_user::getOne($id);
        if ($data) {
            $data->totalLoanCount = InvestsmentRelation::getCountByInvestId($data->id);
        }
        return view('investment/getData', ['data' => $data]);

    }
    
    //投资申请审核通过
    public function examine_status(Request $request)
    {
        $id = $request->input('id');
        
        $result = Investment_user::updateStatus($id, InvestmentStatus::INVESTMENTSTATUS2);
        
        return redirect('Investment/examine');
    }
    
    //投资申请审核不通过
    public function confirm(Request $request)
    {
        $id=$request->post('id');
        InvestsmentRelation::delByInvestmentId($id);
        $result=Investment_user::updateconfirm($id);
        
        if($result) {
            return $data['code']=200;
        }else{
            return $data['code']=201;
        }
    }
    
    
    public function flow(Request $request)
    {
        $investId = $request->input('investId');
        $type = $request->input('type');
        $investment = Investment_user::find($investId);
        
        $statusArr = [
            'loan'         => [],
            'apply'        => [2, 3, 4, 5],
            'payment'      => [],
            'applypayment' => [2, 3, 4, 5],
        ];
        $status = $statusArr[$type];
        $flow = InvestsmentRelation::getList($investId, $status);

        $flow->map(function($order) {
            $order['order_status_str'] = OrderStatus::toString($order['order_status']);
            //已产生利息金额
            if ($order->status>1) {
                $order->interest = InvestsmentRelation::getInterest($order->loan_period, $order->loan_amount);
            }
            $order->status_str = InvestmentRelationStatus::toString($order->status);
            return $order;
        });
        
        $templateArr = [
            'loan'         => 'loanflow',
            'apply'        => 'applyflow',
            'payment'      => 'paymentflow',
            'applypayment' => 'applypaymentflow',
        ];
        $template = $templateArr[$type];
        return view('investment/'.$template, [
            'investment' => $investment,
            'flows' => $flow,
        ]);
    }
    
    /**
     * 投资确认放款
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function paymentAc(Request $request)
    {
        $id = $request->input('id');
        $model = OrderFactory::getInstance('payment');
        $invest = Investment_user::getOne($id);
        $orders = InvestsmentRelation::getList($id);
        
        foreach ($orders as $k=>$v) {
            $ret = $model->payment($v->loan_order_id);
            Log::info('Investment Payment result:'.$ret);
        }
        $now = date('Y-m-d H:i:s');
        $invest->updatePaytime($now);
        
        //进入投资期
        $mobile = ltrim($invest->mobile, '0');
        OverMessage::investmentBegin($mobile, $invest->uid);
        
        $uid = $invest->uid;
        $pushToken = PushToken::getTokenByUid($uid);
        PushSevice::investmentBegin($pushToken);
        return redirect('/Investment/payment');
    }
    
    /**
     * 确认投资提现
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function applyAc(Request $request)
    {
        $id = $request->input('id');
        $model = OrderFactory::getInstance('payment');
        $ret = $model->payment($id, 'investment');
        Log::info('Investment Apply Amount result:'.$ret);
        $payment = json_decode($ret, true);
        if (!empty($payment['code']) && $payment['code']!=1) {
            InvestsmentRelation::updateStatus($id, 4);
            Investment_user::updateStatus($id, InvestmentStatus::INVESTMENTSTATUS8);
        }
        return redirect('/Investment/apply');
    }
}
