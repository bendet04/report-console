<?php
/**
 * 电审任务重新分配
 * @author  chenwr
 * @since   2018年6月12日
 * @version v1.0
 */

namespace App\Http\Controllers;

use App\Consts\PayType;
use App\Models\AdminUser;
use App\Models\LoanTrialPhone;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Service\Assign;

class TaskPhoneController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'task');
    }
    
    /**
     * 电审任务分配列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function phonetrial(Request $request)
    {
        $admin_id = '';
        if($request->isMethod('get')){
            $admin_id = $request->get('admin_id');
        }
        $model = OrderFactory::getInstance('trialPhone');
    
        //获取电审人员列表
        $phoneUser = AdminUser::getListByRole(10);
    
        $phoneArr = $phoneUser->pluck('admin_username', 'id');

        //检索条件
        $condition['order_number'] = $request->post('order_number');
        $condition['name'] = $request->post('name');
        $condition['loan_trial_phone.admin_id'] = $request->post('admin_id');
        $condition['loan_order.platform'] = $request->post('platform');
    
        #获取订单列表
        $orderBy = empty($_COOKIE['phonetrial']) ? 'tb_loan_order.id' : $_COOKIE['phonetrial'];
        $orders = $model->getOrderList($condition, $orderBy);
        $orders->map(function($order) use($phoneArr) {
            $order['admin_user'] = $phoneArr->get($order->admin_id);
            $order['pay_type'] = PayType::toString($order->pay_type);
            return $order;
        });
        
        return view('taskphone/phonetrial', [
            'orders' => $orders,
            'phoneUser' => $phoneUser,
            'admin_id' => $admin_id,
            'pageCondition' => $this->getCondition($request),
            'urlCondition' => $this->getUrlWithCondition($request)
        ]);
    }
    
    /**
     * 重新分配页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assign(Request $request)
    {
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
        $url = $request->get('url');

        //获取终审人员列表
        $phoneUser = AdminUser::selectOprator(10);
        
        return view('taskphone/assign', [
            'order_id' => $order_id,
            'admin_id' => $admin_id,
            'phoneUser' => $phoneUser,
            'url' => $url,
        ]);
    }
    /**
     * 电审重新分件页面
     * @param Request $request
     * @return View
     * @date 18-7-9
     */
    public function parts()
    {

        //获取电审人员列表
        $infoUser = AdminUser::getListByRole(10);
        $realUser = AdminUser::selectOprator(10);

        return view('/taskphone/parts', [
            'infoUser' => $infoUser,
            'realUser' => $realUser,
        ]);
    }
    /**
     * 电审重新分配
     * @param Request $request
     * @return View
     * @date 18-7-9
     */
    public function realparts(Request $request)
    {

        $this->validate($request, [
            'admin_id' => 'required',
            'needAssign' => 'required',
        ]);

        $needAssigninfo = [];
        $needAssign = $request->input('needAssign');

        foreach ($needAssign as $v){
            array_push($needAssigninfo,collect(['id' => $v]));
        }
        $admin_id = $request->input('admin_id');

        $needAssignAdmin =LoanTrialPhone::selectTrailByAdminId($admin_id);


        foreach ($needAssignAdmin as $k => $v){
            Assign::assign($v->order_id,'phone',$needAssigninfo);
            Log::info('assign admin id is '. $admin_id . ' order id is '.$v->order_id);

        }

        return redirect('/taskphone/phonetrial');

    }
    /**
     * 电审人员再分配
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reassign(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required',
            'admin_id' => 'required',
        ]);
        
        $order_id = $request->get('order_id');
        $admin_id = $request->get('admin_id');
        $url = $request->get('url');

        $res = LoanTrialPhone::saveData($order_id, [
            'admin_id' => $admin_id,
        ]);
        
        return redirect($url);
    }
}
