<?php

namespace App\Http\Controllers;

use App\Models\AdminUser;
use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PersonalController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'setting');
    }
    
    public function modify(Request $request)
    {
        return view('/personal/modify');
    }
    
    public function updatePassword(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'old' => 'required',
            'new' => 'required',
            'rename' => 'required',
        ]);

        $oldPassword = UserService::encrypt($request->input('old'));

        if($oldPassword == Auth::user()->admin_pwd){

            $new = $request->input('new');
            $renew = $request->input('rename');

            if($new == $renew){

                $id = Auth::id();

                $admin = new AdminUser();
                $data['admin_pwd'] = UserService::encrypt($new);
                $updateInfo = $admin->updateAdminPasswordById($data, $id);
                if($updateInfo){
                    return redirect('/personal/modify');
                } else {
                    return redirect()->back()
                            ->withErrors('修改密码失败');
                }

            } else {
                return back()
                        ->withErrors('两次密码输入不一致');
            }


        } else {
            return back()
                    ->withErrors('原始密码不正确');
        }

    }
    
    
}
