<?php

namespace App\Http\Controllers;

use App\Consts\OrderStatus;
use App\Consts\StatusIdentifier;
use App\Models\AdminUser;
use App\Models\LoanOrder;
use App\Models\LoanTrialInfoRecord;
use App\Models\LoanTrialPhoneRecord;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Auth;

class PhoneController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'loan');
    }
    
    public function phoneTrial(Request $request)
    {

        $model = OrderFactory::getInstance('trialPhone');
        if($request->all()){

            $where = [];
            if($request->input('name')){
                $where['name'] = $request->input('name');
            }
            if($request->input('order_number')){
                $where['order_number'] = $request->input('order_number');
            }
            if($request->input('ktp_number')){
                $where['ktp_number'] = $request->input('ktp_number');
            }
            $orders = $model->getOrderList($where);

        } else {

            $orders = $model->getOrderList();

        }

        return view('/phone/phoneTrial')
            ->with([
                'orders' => $orders,
                'pageCondition' => $this->getCondition($request),
                'back_url' => $this->getUrlWithCondition($request)
            ])
            ->withInput($request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @param $info 为了兼容电审页面中的跳转
     * @param string $type 为了兼容订单表中的返回按钮
     * @return mixed
     */
    public function workDetail(Request $request, $id, $info, $type = '')
    {
        $model = OrderFactory::getInstance('trialPhone');
        $order = $model->getOrder(['order_id' => $id]);

        $workTrialDetail = "";
        if($info){
            $workTrialDetail = LoanTrialPhoneRecord::getByOrderId($id);
        }
        
        $adminModel = new AdminUser();
        $admin = $adminModel->selectAdminDetailById(Auth::id());
        
        return view('/phone/workDetail')
                ->with([
                    'order' => $order,
                    'id' => $id,
                    'type' => $type,
                    'info' => $info,
                    'workTrialDetail' => $workTrialDetail,
                    'admin' => $admin,
                    'back_url' => $this->getUrlWithCondition($request)
                ])
                ->withInput($request->all());
    }

    public function phoneTrialDetail(Request $request)
    {

        $admin_id = Auth::id();
        $model = OrderFactory::getInstance('trialPhone');
        $data = $request->all();
        unset($data['_token']);
        $no_hearad = AdminUser::getNoHeared($admin_id);
        $trialInfo = $model->Trial($data);
        $order = $model->getNextOrderId();
        if (empty($order) && $no_hearad != 1) {
            return json_encode(['code' => StatusIdentifier::RETURN_LIST, 'info' => 'not find next order']);
        }
        if (($trialInfo && !in_array($trialInfo['code'], StatusIdentifier::$returnList)) && $no_hearad != 1) {
            return json_encode(['code' => StatusIdentifier::NEXT_ORDER, 'orderId' => $order->id, 'info' => 'trial success']);
        } elseif ($trialInfo&&$trialInfo['code'] == StatusIdentifier::HAS_BEEN_AUDITED && $no_hearad != 1) {
            return json_encode(['code' => StatusIdentifier::HAS_BEEN_AUDITED,'info' =>$trialInfo['info']]);
        } elseif ($trialInfo && $trialInfo['code'] == StatusIdentifier::PHONE_LIST) {
            $trialInfo['orderId'] = $order->id;
            return json_encode($trialInfo);
        } elseif ($no_hearad == StatusIdentifier::NEXT_ORDER) {
            return json_encode(['code' => StatusIdentifier::RETURN_LIST, 'orderId' => $order->id, 'info' => 'You have many unanswered orders.
                    Please check them first.']);
        } else {
            return json_encode(['code' => 0, 'info' => 'trial fail']);
        }
    }
    
    public function call(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $phone = $request->input('phone');
    
        $url = "https://wss.wanlaowo.com/Api/Call/call?username={$username}&password={$password}&phone={$phone}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //參数为1表示数据传输。为0表示直接输出显示。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //參数为0表示不带头文件，为1表示带头文件
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
        $output = curl_exec($ch);
        $output = json_decode($output, true);
        curl_close($ch);
        
        
        if ($output && !empty($output['result']) && $output['result']==1){
            return json_encode($output);
        }elseif($output){
            return json_encode($output);
        }else {
            return json_encode(['result'=>0, 'msg'=>'Call failure']);
        }
    }
    
    public function  addHolidayList(Request $request)
    {
        $model = OrderFactory::getInstance('trialPhone');
        $data = $request->all();
        $admin_id = Auth::id();
        unset($data['_token']);


        $trialInfo = $model->Trial($data);

        $order = $model->getNextOrderId();
        $no_hearad = AdminUser::getNoHeared($admin_id);
        if (empty($order) && $no_hearad != 1) {
            return json_encode(['code' => StatusIdentifier::RETURN_LIST, 'info' => 'not find next order']);
        }
        if (($trialInfo && !in_array($trialInfo['code'], StatusIdentifier::$returnList)) && $no_hearad != 1) {
            return json_encode(['code' => StatusIdentifier::NEXT_ORDER, 'orderId' => $order->id, 'info' => 'trial success']);
        } elseif ($trialInfo&&$trialInfo['code'] == StatusIdentifier::HAS_BEEN_AUDITED && $no_hearad != 1) {
            return json_encode(['code' => StatusIdentifier::HAS_BEEN_AUDITED,'info' =>$trialInfo['info']]);
        } elseif ($trialInfo && $trialInfo['code'] == StatusIdentifier::PHONE_LIST) {
            return json_encode($trialInfo);
        } elseif ($no_hearad == StatusIdentifier::NEXT_ORDER) {
            return json_encode(['code' => StatusIdentifier::RETURN_LIST, 'info' => 'You have many unanswered orders.
                    Please check them first.']);
        } else {
            return json_encode(['code' => StatusIdentifier::DATA_ABNORMITY, 'info' => 'trial fail']);
        }
    }


}
