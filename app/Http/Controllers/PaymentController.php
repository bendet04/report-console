<?php

namespace App\Http\Controllers;

use App\Consts\Career;
use App\Consts\Level;
use App\Consts\LoanDay;
use App\Consts\PaymentFlowStatus;
use App\Consts\PayType;
use App\Consts\Period;
use App\Models\LoanOrder;
use App\Models\LoanPaymentFlow;
use App\Models\LoanPaymetFlow;
use App\Models\LoanRePaymentFlow;
use App\Models\OrderStatus;
use App\Models\OrderUserBasicInfo;
use App\Service\OrderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use App\Consts\OrderStatus as OrderStatusConsts;

class PaymentController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'payment');
    }
    
    /**
     * 放款管理列表页
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paymenttrial(Request $request)
    {
        $model = OrderFactory::getInstance('payment');
        
        $condition['order_number'] = $request->post('order_number');
        $condition['name'] = $request->post('name');
        $condition['ktp_number'] = $request->post('ktp_number');
        $condition['platform'] = $request->post('platform');
        $condition['order_status'] = $request->post('order_status', [6,15]);
    
        $orderBy = empty($_COOKIE['paymenttrial']) ? 'loan_order.id' : $_COOKIE['paymenttrial'];
        $orders = $model->getList($condition, $orderBy);
        if($request->post('order_status')){
            $orders = $this->HandleOrderStatus($orders);
        }


        
        return view('payment/paymenttrial', [
            'orders' => $orders,
            'pageCondition' => $this->getCondition($request),
        ]);
    }
    
    /**
     * 放款流水列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkpaymentflow(Request $request)
    {
        $model = OrderFactory::getInstance('payment');
    
        $condition['order_number'] = $request->post('order_number');
        $condition['platform'] = $request->post('platform');
        $condition['name'] = $request->post('name');
        $condition['ktp_number'] = $request->post('ktp_number');
        $condition['start'] = !empty($request->input("start"))?date('Y-m-d 00:00:00',strtotime($request->input("start"))):null;
        $condition['end'] = !empty($request->input("end"))?date('Y-m-d 23:59:59',strtotime($request->input("end"))):null;
        $condition['order_status'] = $request->post('order_status', [8, 11, 12, 13, 14, 15, 24, 25]);
        
        $orderBy = empty($_COOKIE['checkpaymentflow']) ? 'loan_order.id' : $_COOKIE['checkpaymentflow'];
        $orders = $model->getList($condition, $orderBy,'payment_date');
//    dd($orders);
        return view('payment/checkpaymentflow', [
            'orders' => $orders,
            'pageCondition' => $this->getCondition($request),
        ]);
    }

    
    /**
     * 确认放款
     * @param $order_id
     * @return bool|string
     */
    public function payment(Request $request)
    {
        $order_id = $request->post('order_id');
        $model = OrderFactory::getInstance('payment');
        return $model->payment($order_id);
    }
    
    /**
     * 取消放款
     * @param Request $request
     * @return string
     */
    public function cancelPayment(Request $request)
    {
        $order_id = $request->post('order_id');
        $status = $request->post('status', 1); //1:放款审核未通过 2:终止放款
        //修改订单状态
        $order_status = $status == 1 ? 7 : 24;
        
        $model = OrderFactory::getInstance('payment');
        $res = $model->updateOrderStatus($order_id, $order_status);

        return json_encode(['code' => $res, 'info' => '修改成功']);
    }
    
    /**
     * 获取银行信息
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBankInfo(Request $request)
    {
        $order_id = $request->get('order_id');
        $only_view = $request->get('only_view');
        
        $order = LoanOrder::getOne($order_id);
        
        //获取用户基本信息
        $bank = OrderUserBasicInfo::getOne($order['basic_id']);
        $bank['career'] = Career::toString($bank['career']);
        $bank['pay_type'] = PayType::toString($order['pay_type']);
        $bank['income_level'] = Level::toString($bank['income_level']);
        $bank['day'] = LoanDay::toString($order['loan_period']);
        $bank['loan_principal_amount'] = $order['apply_amount'];
        $bank['reciprocal_amount'] = $order['reciprocal_amount'];

        $bank['bank_code'] = " ";
        if($order->order_status == 6 || $order->order_status == 15){

            $payment = LoanPaymentFlow::getRecentOrder($order_id);
//        dd($payment);
            if($payment){
                $bank['bank_code'] = $this->handleBankCode($payment->payment_code);
            } else {
                $bank['bank_code'] = "未知错误，可重新放款或联系技术人员";
            }

        }


        
        return view('payment/bank', [
            'userBankInfo' => $bank,
            'only_view'    => $only_view,
        ]);
    }
    
    /**
     * 修改银行信息
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateBankInfo(Request $request)
    {
        $this->validate($request, [
            'id'  => 'required',
            'bankName'  => 'required',
            'bankNumber'  => 'required',
            'bankPhone'  => 'required',
            'payeeName'  => 'required',
        ]);
        
        $id = $request->post('id');
        $dataArr['bank_name'] = $request->post('bankName');
        $dataArr['bank_number'] = $request->post('bankNumber');
        $dataArr['bank_phone'] = $request->post('bankPhone');
        $dataArr['payee_name'] = $request->post('payeeName');
        $payment = OrderUserBasicInfo::updateOne($id, $dataArr);

        
        return redirect('/payment/paymenttrial');
    }
    
    /**
     * 放款流水
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function flow(Request $request)
    {
        $order_id = $request->get('order_id');
        $flows = LoanPaymentFlow::getList($order_id);
        $flows->map(function($flow){
            $flow['payment_status'] = PaymentFlowStatus::toString($flow->payment_status);
        });
        
        return view('payment/flow', [
            'flows' => $flows
        ]);
    }

    /**
     * 银行信息的处理
     * @param $code
     * @return string
     */
    public function handleBankCode($code)
    {
        if($code == 600 || $code == 401 || $code == 506 || $code == 404){
            return "系统异常，可重新放款";
        } else if($code == 400){
            return '信息有误，请联系客户修改';
        } else if($code == 501 || $code == 500 || $code == 646){
            return "内部错误，可重新放款";
        } else if($code == 601 ){
            return "账户余额不足";
        } else if($code == 649){
            return "银行账户信息错误，请联系客户修改";
        } else {
            return '未知错误，可重新放款或联系技术人员。';
        }
    }
    public function HandleOrderStatus($orders)
    {
        $orders->map(function($order) {
            if($order->order_status == 6){
                $order->order_status_str = "放款失败";
            } else {
                $order->order_status_str = OrderStatusConsts::toString($order->order_status);
            }

            return $order;
        });
        return $orders;
    }
}
