<?php

namespace App\Http\Controllers;

use App\Models\LoanOrderTrack;
use App\Models\UserAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Service\Efficency;
use App\Service\DateUnit;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

use App\Service\ExcelCustomer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Excel;

class EfficencyController extends Controller
{
    public function __construct()
    {
        Config::set('currentMenu', 'count');
    }

   public function voice(Request $request)
   {
       $orders = [];

       return view('/efficency/voice')
               ->with([
                   'orders' => $orders,
               ]);
   }
   public function getVoice(Request $request)
   {
       if($request->isMethod('post')){

           $this->validate($request,[
               'from' => 'required',
               'to'   => 'required'
           ]);

           $from = $request->input('from');
           $to = $request->input('to');
           $orders = Efficency::voiceDate($from, $to);
           $orders = $orders->toArray();
           array_unshift($orders, ['用户名','订单编号', '借款期限', '电话号码', '放款日', '到期日', '缓期日', '逾期日', '订单状态']);

           $name = date('Y-m-d', time()) . '外呼数据';
           $file_url =  $name .'.xls';

           return (new ExcelCustomer($orders))->download($file_url, 'Xls');

       }
   }

//   信审审核的件数
   public function messageEfficency(Request $request)
   {
       $date = date('Y-m-d', time());
       $param=[];
       if($request->post('start')) {
           $param['start'] = date("Y-m-d 00:00:00", strtotime($request->get('start')));
       }
       if($request->post('end'))
       {
           $param['end'] = date('Y-m-d 23:59:59', strtotime($request->get('end')));
       }
       if($request->get('week'))
       {
           $param['week']=1;
       }
       if($request->get('month'))
       {
           $param['month']=1;
       }
       $admin = Efficency::message($param);
       $str='?type=info&';
       $page=1;
       if(!empty($this->getCondition($request)))
       {
           $arr=$this->getCondition($request);
           foreach ($arr as $k=>$v)
           {
               $str.=$k.'='.$v.'&';
           }

       }
       return view('/efficency/messageEfficency')
                ->with([
                    'admin' => $admin,
                    'date'  => $date,
                    'pageCondition' => $this->getCondition($request),
                    'page' => $page,
                    'str'   =>  $str,
                ]);
   }



//   电审的通过件数
   public function countMessage(Request $request)
   {
       $date = date('Y-m-d', time());
       $param=[];
       if($request->post('start')) {
           $param['start'] = date("Y-m-d 00:00:00", strtotime($request->get('start')));
       }
       if($request->post('end'))
       {
           $param['end'] = date('Y-m-d 23:59:59', strtotime($request->get('end')));
       }
       if($request->get('week'))
       {
           $param['week']=1;
       }
       if($request->get('month'))
       {
           $param['month']=1;
       }
       $admin = Efficency::countMessage($param);
       $str='?type=phone&';
       $page=1;
       if(!empty($this->getCondition($request)))
       {
           $arr=$this->getCondition($request);
           foreach ($arr as $k=>$v)
           {
               $str.=$k.'='.$v.'&';
           }

       }

       return view('/efficency/countMessage')
           ->with([
               'admin' => $admin,
               'date'  => $date,
               'pageCondition' => $this->getCondition($request),
               'page' => $page,
               'str'   =>  $str,
            ]);
   }


//   催收金额
    public function trackAmount(Request $request)
    {
        $date=date('Y-m-d',time());
        $obj=new UserAccess();
        $time=$obj->DownTime($request->all());
        $where=$this->getParam($request->all(),'t3.created_at');
        $all=$this->getParam($request->all(),'loan_deadline');
        $admin = $this->getTrackAmountSum($where,$all,$time);
        $admin = $this->handleTrackRepeatNameData($admin);
        $allDaySum = Efficency::trackSumByDay($all)[0];
        $flag=null;
        if(!empty($this->getCondition($request)))
        {
            $arr=$this->getCondition($request);
            foreach ($arr as $k=>$v)
            {
                $flag.=$k.'='.$v.'&';
            }
        
        }
        $trackAdmin = "";

        return view('/efficency/trackAmount')
            ->with([
                'admin' => $admin,
                'date'=>$date,
                'trackAdmin' => $trackAdmin,
                'pageCondition' => $this->getCondition($request),
                'allDaySum' => $allDaySum,
                'flag'=>$flag,

            ]);
    }
    public function getTrackAmountSum($where,$all,$time)
    {
       
        $admin = Efficency::trackMonth($where);
        $adminAll = Efficency::trackAll($all);
        if($admin){
            foreach ($admin as $k => $v) {
                $adminName=$v->admin;
                unset($v->admin);
                $admin[$k]->time=$time;
                $admin[$k]->admin=$adminName;
                $count=$v->count;
                unset($v->count);
                $admin[$k]->realCount = 0;
                $admin[$k]->applyCount = 0;
                if($adminAll){
                    foreach ($adminAll as $dayK => $dayV){
                        if($dayV ->admin && $v ->admin == $dayV ->admin){
                            $admin[$k]->realCount = $dayV->realCount;
                            $admin[$k]->applyCount = $dayV->applyCount;
                        }
                    }
                }
                $admin[$k]->count = $count;

            }
        }
        return $admin;
    }
    public function getTrackAmountMonthSum($date,$endDate)
    {
        $admin = Efficency::trackMonth($date,$endDate);
        $adminAll = Efficency::trackAll($date,$endDate);
        if($admin){
            foreach ($admin as $k => $v) {
                $count=$v->count;
                unset($v->count);
                $admin[$k]->realCount = 0;
                $admin[$k]->applyCount = 0;
                if($adminAll){
                    foreach ($adminAll as $dayK => $dayV){
                        if($dayV ->admin && $v ->admin == $dayV ->admin){
                            $admin[$k]->realCount = $dayV->realCount;
                            $admin[$k]->applyCount = $dayV->applyCount;
                        }
                    }
                }
                $admin[$k]->count = $count;

            }
        }
        return $admin;
    }
    public function overDue()
    {
        $date = date('Y-m-d', time());
        //催收人员
        $trackAdmin = Efficency::getTrackHandOrder();
//        dd($trackAdmin);
        $beforeTrack = Efficency::getTrackOrderByDay(-1);

        $todayTrack = Efficency::getTrackOrderByDay(0);
        $overDueOneDay = Efficency::getTrackOrderByDay(1);
        $overDueTwoDay = Efficency::getTrackOrderByDay(2);
        $overDueServenDay = Efficency::getTrackOrderByDay(7);
        $overDueFifthDay = Efficency::getTrackOrderByDay(14);
        $overDueAllDay = Efficency::getAllTrack(15);
//        dd($trackAdmin,$overDueServenDay);
        if($trackAdmin){
            foreach ($trackAdmin as $k => $v) {
                $trackAdmin[$k]['before'] = 0;
                $trackAdmin[$k]['deadline'] = 0;
                $trackAdmin[$k]['one'] = 0;
                $trackAdmin[$k]['two'] = 0;
                $trackAdmin[$k]['seven'] = 0;
                $trackAdmin[$k]['fifth'] = 0;
                $trackAdmin[$k]['all'] = 0;
                if($beforeTrack){
                    foreach ($beforeTrack as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['before'] = $dayV->count;
                        }
                    }
                }
                if($todayTrack){
                    foreach ($todayTrack as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['deadline'] = $dayV->count;
                        }
                    }
                }
                if($overDueOneDay){
                    foreach ($overDueOneDay as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['one'] = $dayV->count;
                        }
                    }
                }
                if($overDueTwoDay){
                    foreach ($overDueTwoDay as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['two'] = $dayV->count;
                        }
                    }
                }
                if($overDueServenDay){
                    foreach ($overDueServenDay as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['seven'] = $dayV->count;
                        }
                    }
                }
                if($overDueFifthDay){
                    foreach ($overDueFifthDay as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['fifth'] = $dayV->count;
                        }
                    }
                }
                if($overDueAllDay){
                    foreach ($overDueAllDay as $dayK => $dayV){
                        if($dayV ->id && $v ->id == $dayV ->id){
                            $trackAdmin[$k]['all'] = $dayV->count;
                        }
                    }
                }

            }
        }
//        return view('/efficency/_trackAmount_count')
        return view('/efficency/_trackAmount_overdue')
            ->with([
                'trackAdmin' => $trackAdmin,
                'date' => $date,
            ]);
    }
    public function selectTrackByDay(Request $request)
    {
        $date = date('Y-m-d', time());
        $admin = "";
        $allDaySum = "";
        $data = $request->all();
        unset($data['_token']);
        unset($data['s']);
        $flag=null;

        if(!empty($data['date'])){

            $data = date('Y-m-d',strtotime($data['date']));
            $endDate = date('Y-m-d',strtotime("+1 day",strtotime($data)));
            $admin = $this->getTrackAmountSum($data, $endDate);
            $allDaySum = Efficency::trackSumByDay($data, $endDate)[0];

            if($admin){
                $flag.="?date=".$data."";
                return view('/efficency/_trackAmount_check')
                    ->with([
                        'admin' => $admin,
                        'date'  => $date,
                        'flag'  => $flag,
                        'allDaySum' => $allDaySum
                    ]);

                return json_encode(['code' => 1, 'info' => $detail]);
            } else {

                $admin = "";
                return view('/efficency/_trackAmount_check')
                    ->with([
                        'admin' => $admin,
                        'date'  => $date,
                        'flag'  => $flag,
                        'allDaySum' => $allDaySum
                    ]);
            }

        } else {
            return view('/efficency/_trackAmount_check')
                ->with([
                    'admin' => $admin,
                    'date'  => $date,
                    'flag'  => $flag,
                    'allDaySum' => $allDaySum
                ]);
        }
    }
    public function selectTrackByMonth(Request $request)
    {
        $month = date('Y-m', time());;
        $adminMonth = "";
        $allMonthSum = "";
        $data = $request->all();
        unset($data['_token']);
        unset($data['s']);
        $flag=null;
        if(!empty($data['month'])){
//            dd($data['month'], strtotime($data['month']));
            $formatMonth = DateUnit::getFormatMonth($data['month']);
            $data = date('Y-m-d',$formatMonth);
            $nextMonth = DateUnit::getNextMonth($data);
//            dd();
            $adminMonth = $this->getTrackAmountMonthSum($data, $nextMonth);
//            dd($adminMonth);
            $allMonthSum = Efficency::trackSumByMoth($data)[0];
            if($adminMonth){
                $flag.="?date=".$data."";
                return view('/efficency/_trackAmount_list')
                    ->with([
                        'month' => $month,
                        'flag' =>$flag,
                        'adminMonth' => $adminMonth,
                        'allMonthSum' => $allMonthSum,
                    ]);

                return json_encode(['code' => 1, 'info' => $detail]);
            } else {
                $adminMonth = "";
                return view('/efficency/_trackAmount_list')
                    ->with([
                        'month' => $month,
                        'flag'=>$flag,
                        'adminMonth' => $adminMonth,
                        'allMonthSum' => $allMonthSum,
                    ]);
            }

        } else {
            return view('/efficency/_trackAmount_list')
                ->with([
                    'month' => $month,
                    'flag'  => $flag,
                    'adminMonth' => $adminMonth,
                    'allMonthSum' => $allMonthSum,
                ]);
        }
    }

    public function downTrackByDay(Request $request)
    {
        $obj=new UserAccess();
        $time=$obj->DownTime($request->all());
        $time=str_replace('/','-',$time);
        $where=$this->getParam($request->all(),'t3.created_at');
        $all=$this->getParam($request->all(),'loan_deadline');
        $admin = $this->getTrackAmountSum($where,$all,$time);
        if(empty($admin))
            throw new FileException('message trial info empty');

        $admin = array_map('get_object_vars', $admin);
        array_unshift($admin,['日期','催收人员','应催收总金额','应催收总本金','催收金额']);
        $name = $time.'催收数据';
        $file_url =  $name .'.xls';

        return (new ExcelCustomer($admin))->download($file_url, 'Xls');
    }

    public function downTrackByMonth(Request $request)
    {
        $param=$request->get('date');
        if(!empty($param))
        {
            $month=$param;
        }
        else
        {
            $month = date('Y-m', time());

        }
        $nextMonth = DateUnit::getNextMonth($month);
        $adminMonth = $this->getTrackAmountMonthSum($month, $nextMonth);
        $adminMonth = $this->handleTrackRepeatNameData($adminMonth);
        if(empty($adminMonth))
            throw new FileException('message trial info empty');

        $admin = array_map('get_object_vars', $adminMonth);
        $admin=$this->array_remove_key($admin,array('day'));//删除day这个字段
        array_unshift($admin, ['年','月',  '催收人员', '应催收总金额',' 应催收总本金', '催收金额']);
        $name = date('Y-m', time()) . '催收数据';
        $file_url =  $name .'.xls';

        return (new ExcelCustomer($admin))->download($file_url, 'Xls');
    }
    public function handleTrackRepeatNameData($admin)
    {
        $signPan = '';
        $amountPan = 0;
        $signRiz = '';
        $amountRiz = 0;

       foreach ($admin as $k => $v)
       {
           if($v->admin == "cahya panji prisna"){
               $signPan = $k;
               $amountPan = $v->count;
           }
           if($v->admin == "rizky trestyar"){
               $signRiz = $k;
               $amountRiz = $v->count;
           }
       }
        foreach ($admin as $k => $v)
        {
            if($v->admin == 'panji'){
                $admin[$k]->count = $v->count + $amountPan;
            }
            if($v->admin == 'rizky'){
                $admin[$k]->count = $v->count + $amountRiz;
            }
        }
       if($signPan){
           unset($admin[$signPan]);
       }
        if($signRiz){
            unset($admin[$signRiz]);
        }
        return $admin;

    }

    public function finalCount(Request $request)
    {
        $time = Carbon::today()->toDateTimeString();
        $data = Efficency::handFinalData($time, $time);
        $user = Efficency::handUsereInfo($time, $time);

        $time = date('Y-m-d', time());

        return view('/efficency/finalCount')
            ->with([
                'data' => $data,
                'user' => $user,
                'condition' => 'start\\'.$time . '\\end\\' . $time
            ]);
    }
    public function selectFinalByDate(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        if(empty($end)){
            $end = Carbon::now()->toDateTimeString();
        }

        $data = Efficency::handFinalData($start, $end);
        $user = Efficency::handUsereInfo($start, $end);

        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end) + 86400);

        return view('/efficency/_finalCount_list')
            ->with([
                'data' => $data,
                'user' => $user,
                'condition' => 'start\\'.$start . '\\end\\' . $end
            ]);

    }
    public function selectFinalLocalWeek(Request $request)
    {
        $start = date('Y-m-d', (time() - ((date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600));
        $end = date('Y-m-d', (time() + (7 - (date('w') == 0 ? 7 : date('w'))) * 24 * 3600));

        $data = Efficency::handFinalData($start, $end);
        $user = Efficency::handUsereInfo($start, $end);


        return view('/efficency/_finalCount_list')
            ->with([
                'data' => $data,
                'user' => $user,
                'condition' => 'start\\'.$start . '\\end\\' . $end
            ]);
    }
    public function selectFinalLocalMonth(Request $request)
    {
        $start = date('Y-m-d', strtotime(date('Y-m', time()) . '-01 00:00:00'));
        $end = date('Y-m-d', strtotime(date('Y-m', time()) . '-' . date('t', time()) . ' 00:00:00'));

        $data = Efficency::handFinalData($start, $end);
        $user = Efficency::handUsereInfo($start, $end);

        return view('/efficency/_finalCount_list')
            ->with([
                'data' => $data,
                'user' => $user,
                'condition' => 'start\\'.$start . '\\end\\' . $end
            ]);
    }
    public function downFinalTrialInfo(Request $request, $start, $end)
    {
        $name = $start. '~' . $end;
        $data = Efficency::handFinalData($start, $end);
//        dd($data);
        if(empty($data))
            throw new FileException('message trial info empty');
//        dd($data);
//        $admin = array_map('get_object_vars', $data);
        array_unshift($data, ['日期',  '终审人员', '总件数', '通过件数', '通过率','逾期率','坏账率']);

        $name = $name . '终审数据';
        $file_url =  $name .'.xls';

        return (new ExcelCustomer($data))->download($file_url, 'Xls');
    }
    function array_remove_key($array,$keys){

        if (!is_array($array) || !is_array($keys)){
            return false;
        }
        foreach($array as $t){
            foreach($keys as $k){
                unset($t[$k]);
            }
            $doc[]=$t;
        }
        return $doc;
    }
    function getParam($paramArr="",$diff="")
    {
        $where="1=1 ";
        $paramArr=array_filter($paramArr);
        if(isset($paramArr['s'])&&!empty($paramArr['s']))
            unset($paramArr['s']);
        
        if(isset($paramArr)&&count($paramArr)!=0) {
           
            if (!empty($paramArr['start']) && empty($paramArr['end']))
            {   $where .= " AND $diff>='" . date("Y-m-d 00:00:00", strtotime($paramArr['start'])) . "'";
                $flag = 'start';
            }
            if (!empty($paramArr['end']) && empty($paramArr['start'])) {
                $where .= " AND $diff<='" . date("Y-m-d 00:00:00", strtotime($paramArr['end'])) . "'";
                $flag = 'end';
            }
            if (!empty($paramArr['start']) && !empty($paramArr['end'])) {
                if(strtotime($paramArr['start'])>strtotime($paramArr['end']))
                {
                    $center=$paramArr['start'];
                    $paramArr['start']=$paramArr['end'];
                    $paramArr['end']=$center;
                }
                $where .= " AND $diff between '" . date("Y-m-d 00:00:00", strtotime($paramArr['start'])) . "' AND '" . date("Y-m-d 23:59:59", strtotime($paramArr['end'])) . "'";
                $flag = 'between';
            }
            if (!empty($paramArr['month'])) {
                $where .= " AND DATE_FORMAT($diff,'%Y-%m' ) = DATE_FORMAT(now(),'%Y-%m')";
                $flag = 'month';
            }
            if (!empty($paramArr['week'])) {
                $where .= " AND YEARWEEK(date_format($diff,'%Y-%m-%d')) = YEARWEEK(now())";
                $flag = 'week';
            }
        }
        else
        {
            $where.=" AND to_days($diff)=to_days(now())";
            $flag='0';
        }
        return $where;
    }
    function willingness(Request $request)
    {
        $start=date('Y-m-d 00:00:00',time());
        $end=date('Y-m-d 23:59:59',time());
       $data= LoanOrderTrack::getWillingness($start,$end)->toArray();
       $data=$this->handleWillingness($data);
       return view('/efficency/willingness')
        ->with([
            'data' => $data,
            'now'  =>date('Y-m-d',time()),
        ]);
     
    }
    function handleWillingness($data)
    {
        $result=[];
        $arr=[];
        foreach ($data as $k=>$v){
          $result[$v['admin_username']][]=$v;
        }
        foreach ($result as $k1=>$v1){
           $today=0;
           $tomorrow=0;
           $after=0;
           $none=0;
         foreach ($v1 as $k=>$v) {
             $arr[$k1]['all']=count($v1);
             $arr[$k1]['person'] = $v['admin_username'];
             if (substr_count($v['promise_time'], '-')) {
                 if (date("Y-m-d") == $v['promise_time']) {
                     $today += 1;
                 } else if (date("Y-m-d", strtotime("+1 day")) == $v['promise_time']) {
                     $tomorrow += 1;
                 } else if (date("Y-m-d", strtotime("+2 day")) == $v['promise_time']) {
                     $after += 1;
                 }
             } else if (substr_count($v['promise_time'], '暂无')) {
                 $none += 1;
             }
         }
         $arr[$k1]['today']=$today;
         $arr[$k1]['tomorrow']=$tomorrow;
         $arr[$k1]['after']=$after;
         $arr[$k1]['none']=$none;
         $arr[$k1]['time']=date('Y-m-d',time());
       }
       return $arr;
    }

}
