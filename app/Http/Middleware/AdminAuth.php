<?php

namespace App\Http\Middleware;

use App\Models\AdminAccess;
use App\Service\UserService;
use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class AdminAuth
{
    /**
     * 管理员权限控制模块
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access = UserService::getUserAccess();
        if ($access !== '*') {
            //获取当前uri
            $uri = Request::getRequestUri();
            $uriArr = explode('?', $uri);
            $currentUri = $uriArr[0];
            $currentShort = preg_replace('/(\/[a-zA-Z]*)\/{1}(.*)/i', '$1', $currentUri);
            
            //获取所有uri
            $accessAll = AdminAccess::getAll();
            $accessAllUri = $accessAll->pluck('access_url');
            $accessAllUri = $accessAllUri->filter();
            $accessAllShort = $accessAllUri->map(function($uri){
                return preg_replace('/(.*)\/{1}([^\/]*)/i', '$1', $uri);
            });
            
            //获取用户拥有权限的uri
            $accessUri = $access->pluck('access_url');
            $accessShort = $accessUri->map(function($uri){
                return preg_replace('/(.*)\/{1}([^\/]*)/i', '$1', $uri);
            });
            
            $passUri = ['/channel/down/'];
            if (!in_array($currentUri, $passUri)){
                if ($accessAllUri->contains($currentUri) && !$accessUri->contains($currentUri)) {
                    dd('permission denied!');
                } elseif($accessAllShort->contains($currentShort) && !$accessShort->contains($currentShort)) {
                    dd('permission denied!!');
                }
            }
            
        }
        return $next($request);
    }
}
